/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

/**
 * Key class for identifying a single task send to a worker.
 */
public class WorkKey {

  private final String queueName;
  private final String workId;
  private final int calculationId;
  private final int sectorId;

  public WorkKey(final String queueName, final String workId, final int calculationId, final int sectorId) {
    this.queueName = queueName;
    this.workId = workId;
    this.calculationId = calculationId;
    this.sectorId = sectorId;
  }

  public String getQueueName() {
    return queueName;
  }

  /**
   * Returns the unique id identifying the whole work job.
   * @return
   */
  public String getWorkId() {
    return workId;
  }

  public int getCalculationId() {
    return calculationId;
  }

  public int getSectorId() {
    return sectorId;
  }
}
