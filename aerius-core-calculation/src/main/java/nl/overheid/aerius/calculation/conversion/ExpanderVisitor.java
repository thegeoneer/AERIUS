/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource.PlanEmission;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Expands {@link EmissionSource} objects into point objects using the visitor pattern.
 *
 * <p>This visitor uses a {@link SourceCollector} to pass the converted results and therefore only traverses the sources lists.
 * The collecting via the interface design pattern is used because network sources are nested and need to be handled differently for different
 * profiles.
 */
class ExpanderVisitor implements EmissionSourceVisitor<Void> {

  /**
   * Interface to which converted engine sources are passed by the visitor. The implementation should collect the given engines source list.
   */
  public interface SourceCollector {
    /**
     * Method called with the list of engine sources. The emissionSource is the source they were converted from.
     *
     * @param engineSources list of converted engine sources
     * @param emissionSource source the engine sources are converted from
     */
    void collect(List<EngineSource> engineSources, EmissionSource emissionSource);
  }

  private static final Logger LOGGER = LoggerFactory.getLogger(ExpanderVisitor.class);

  private final Connection con;
  private final List<EmissionValueKey> keys;
  private final SourceConverter gse;
  private final InlandShipExpander shipSourceExpander;
  private final MaritimeShipExpander maritimeShipExpander;
  private final SourceCollector sourceCollector;

  /**
   * Constructor.
   *
   * @param con Connection to use for queries.
   * @param sources The emission sources to convert to engine sources.
   * @param keys emissions keys to convert
   * @throws SQLException throws SQLException in case of database problems
   */
  public ExpanderVisitor(final Connection con, final SourceConverter sourceConverter, final List<EmissionValueKey> keys,
      final SourceCollector sourceCollector) throws SQLException {
    this.con = con;
    this.sourceCollector = sourceCollector;
    gse = sourceConverter;
    this.keys = keys;
    shipSourceExpander = new InlandShipExpander(con, gse);
    maritimeShipExpander = new MaritimeShipExpander(con, gse);
  }

  @Override
  public Void visit(final FarmEmissionSource emissionSource) throws AeriusException {
    final List<EngineSource> expandedSources = new ArrayList<>();
    gse.convert(con, expandedSources, emissionSource, keys);
    sourceCollector.collect(expandedSources, emissionSource);
    return null;
  }

  @Override
  public Void visit(final GenericEmissionSource emissionSource) throws AeriusException {
    final List<EngineSource> expandedSources = new ArrayList<>();
    gse.convert(con, expandedSources, emissionSource, keys);
    sourceCollector.collect(expandedSources, emissionSource);
    return null;
  }

  @Override
  public Void visit(final InlandMooringEmissionSource emissionSource) throws AeriusException {
    final List<EngineSource> expandedSources = new ArrayList<>();
    try {
      shipSourceExpander.inlandMooringToPoints(expandedSources, emissionSource, keys);
      sourceCollector.collect(expandedSources, emissionSource);
      return null;
    } catch (final SQLException e) {
      LOGGER.error("SQLException in ExpanderVisitor#InlandMooringEmissionSource", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public Void visit(final InlandRouteEmissionSource emissionSource) throws AeriusException {
    final List<EngineSource> expandedSources = new ArrayList<>();
    try {
      shipSourceExpander.inlandRouteToPoints(expandedSources, emissionSource, keys);
      sourceCollector.collect(expandedSources, emissionSource);
      return null;
    } catch (final SQLException e) {
      LOGGER.error("SQLException in ExpanderVisitor#InlandRouteEmissionSource", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public Void visit(final MaritimeMooringEmissionSource emissionSource) throws AeriusException {
    final List<EngineSource> expandedSources = new ArrayList<>();
    try {
      maritimeShipExpander.maritimeMooringShipsToPoints(expandedSources, emissionSource, keys);
      sourceCollector.collect(expandedSources, emissionSource);
      return null;
    } catch (final SQLException e) {
      LOGGER.error("SQLException in ExpanderVisitor#MaritimeMooringEmissionSource", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public Void visit(final MaritimeRouteEmissionSource emissionSource) throws AeriusException {
    final List<EngineSource> expandedSources = new ArrayList<>();
    if (emissionSource.getGeometry().getType() == TYPE.LINE) {
      try {
        maritimeShipExpander.maritimeRouteToPoints(expandedSources, emissionSource, keys);
      } catch (final SQLException e) {
        LOGGER.error("SQLException in ExpanderVisitor#MaritimeRouteEmissionSource", e);
        throw new AeriusException(Reason.SQL_ERROR);
      }
    } else {
      gse.convert(con, expandedSources, emissionSource, keys);
    }
    sourceCollector.collect(expandedSources, emissionSource);
    return null;
  }

  /**
   * Converts a {@link OffRoadMobileEmissionValues} object to points object.
   * The default and custom categories are split, where custom categories will result in a
   * emission source each, due to the differences in OPS characteristics.
   *
   * @param emissionSource EmissionSource to convert
   * @throws AeriusException throws AeriusException in case of other problems
   */
  @Override
  public Void visit(final OffRoadMobileEmissionSource emissionSource) throws AeriusException {
    final List<EngineSource> expandedSources = new ArrayList<>();
    //split the source: customs get their own source (own characteristics).
    final ArrayList<OffRoadVehicleEmissionSubSource> defaultVehicles = new ArrayList<>();
    final ArrayList<OffRoadVehicleEmissionSubSource> customVehicles = new ArrayList<>();
    for (final OffRoadVehicleEmissionSubSource vehicle : emissionSource.getEmissionSubSources()) {
      if (vehicle.isCustomCategory()) {
        customVehicles.add(vehicle);
      } else {
        defaultVehicles.add(vehicle);
      }
    }
    final OffRoadMobileEmissionSource clearedEmissionSource = emissionSource.copy();
    clearedEmissionSource.getEmissionSubSources().clear();
    clearedEmissionSource.setSourceCharacteristics(emissionSource.getSector().getDefaultCharacteristics());

    //handle the 'custom' vehicles
    for (final OffRoadVehicleEmissionSubSource vehicle : customVehicles) {
      final OffRoadMobileEmissionSource customVehicleSource = clearedEmissionSource.copy();
      customVehicleSource.getEmissionSubSources().add(vehicle);
      final OPSSourceCharacteristics characteristics = customVehicleSource.getSourceCharacteristics();
      characteristics.setEmissionHeight(vehicle.getEmissionHeight());
      characteristics.setSpread(vehicle.getSpread());
      characteristics.setHeatContent(vehicle.getHeatContent());
      //add to the expanded list. Source can be all geometries.
      gse.convert(con, expandedSources, customVehicleSource, keys);
    }
    //handle the 'default' vehicles.
    final OffRoadMobileEmissionSource defaultVehiclesSource = clearedEmissionSource.copy();
    defaultVehiclesSource.getEmissionSubSources().addAll(defaultVehicles);
    //add to the expanded list. Source can be all geometries.
    gse.convert(con, expandedSources, defaultVehiclesSource, keys);
    sourceCollector.collect(expandedSources, emissionSource);
    return null;
  }

  /**
   * Converts a {@link PlanEmissionValues} object to points object.
   * The categories are split, where each categories will result in a
   * emission source each, due to the differences in OPS characteristics.
   *
   * @param emissionSource EmissionSource to convert
   * @throws AeriusException throws AeriusException in case of other problems
   */
  @Override
  public Void visit(final PlanEmissionSource emissionSource) throws AeriusException {
    final List<EngineSource> expandedSources = new ArrayList<>();
    //split the source: each plan type get their own source (own characteristics based on sector).
    for (final PlanEmission planEmission : emissionSource.getEmissionSubSources()) {
      final GenericEmissionSource planEmissionSource = emissionSource.copyTo(new GenericEmissionSource());
      for (final EmissionValueKey key : keys) {
        planEmissionSource.setEmission(key, emissionSource.getEmission(planEmission, key));
      }
      planEmissionSource.setSourceCharacteristics(planEmission.getCategory().getCharacteristics());
      //add to the expanded list. Source can be all geometries.
      gse.convert(con, expandedSources, planEmissionSource, keys);
    }
    sourceCollector.collect(expandedSources, emissionSource);
    return null;
  }

  @Override
  public Void visit(final SRM2EmissionSource emissionSource) throws AeriusException {
    final List<EngineSource> expandedSources = new ArrayList<>();

    gse.convert(con, expandedSources, emissionSource, keys);
    sourceCollector.collect(expandedSources, emissionSource);
    return null;
  }

  /**
   * Converts a {@link SRM2NetworkEmissionValues} object to points object.
   * The network is split into individual roads, after which each road is split into points.
   * For actual SRM2 calculations the network isn't split like this, but the calculation still needs it (to determine receptors).
   *
   * @param emissionSource EmissionSource to convert
   * @throws AeriusException throws AeriusException in case of other problems
   */
  @Override
  public Void visit(final SRM2NetworkEmissionSource emissionSource) throws AeriusException {
    //split the source: each plan type get their own source (own characteristics based on sector).
    for (final EmissionSource srm2RoadSource : emissionSource.getEmissionSources()) {
      final List<EngineSource> expandedSources = new ArrayList<>();

      gse.convert(con, expandedSources, srm2RoadSource, keys);
      sourceCollector.collect(expandedSources, srm2RoadSource);
    }
    return null;
  }
}
