/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.WorkSemaphore;
import nl.overheid.aerius.taskmanager.client.mq.QueueUpdateHandler;
import nl.overheid.aerius.taskmanager.client.mq.RabbitMQQueueMonitor;

/**
 * {@link WorkSemaphore} implementation that scales with more or less capacity depending on the load on the queue. For this queue the worker type
 * queue to the taskmanager is monitored. If the queue is empty (no ready messages) then it might be possible to handle more tasks.
 *
 */
class DynamicWorkSemaphore implements WorkSemaphore {

  private static final Logger LOGGER = LoggerFactory.getLogger(DynamicWorkSemaphore.class);

  /**
   * Step size for each increase of the dynamic size.
   */
  private static final int STEP_SIZE = 4;

  /**
   * If the current number of tasks running exceeds this value the step size is doubled. This to get faster claiming of resources when a very large
   * calculation is done.
   */
  private static final int MAGIC_THRESHOLD_FOR_LARGER_STEPS = 100;

  private final RabbitMQQueueMonitor rabbitMQQueueMonitor;
  private final int initialTaskSize;
  private final Map<String, DynamicWorkQueueSemaphore> map = new HashMap<>();

  public DynamicWorkSemaphore(final RabbitMQQueueMonitor rabbitMQQueueMonitor, final int initialTaskSize) {
    this.rabbitMQQueueMonitor = rabbitMQQueueMonitor;
    this.initialTaskSize = initialTaskSize;
  }

  @Override
  public void drain() {
    map.forEach((k, v) -> v.drain());
    map.forEach((k, v) -> {
      if (StringUtils.isNotEmpty(k)) {
        rabbitMQQueueMonitor.removeQueueUpdateHandler(k, v);
      }
    });
  }

  @Override
  public void acquire(final String queue) throws InterruptedException {
    getMonitor(queue).acquire();
  }

  @Override
  public void release(final String queue) {
    getMonitor(queue).release();
  }

  /**
   * @return Number of available permits that can be acquired. Method for testing only
   */
  int availablePermits(final String queue) {
    synchronized (this) {
      return getMonitor(queue).semaphore.availablePermits();
    }
  }


  private DynamicWorkQueueSemaphore getMonitor(final String queueName) {
    if (!map.containsKey(queueName)) {
      addHandler(queueName, new DynamicWorkQueueSemaphore(initialTaskSize));
    }
    return map.get(queueName);
  }

  private void addHandler(final String queueName, final DynamicWorkQueueSemaphore monitor) {
    if (StringUtils.isNotEmpty(queueName)) {
      rabbitMQQueueMonitor.addQueueUpdateHandler(queueName, monitor);
    }
    map.put(queueName, monitor);
  }

  /**
   * Specific {@link WorkSemaphore} implementation for a specific queue.
   */
  private static class DynamicWorkQueueSemaphore implements QueueUpdateHandler {
    /**
     * The semaphore used to acquire and release slots.
     */
    private final Semaphore semaphore;
    /**
     * Keeps track of the number of tasks that have acquired a slot.
     */
    private final AtomicInteger numberOfTasksRunning = new AtomicInteger();
    /**
     * Keeps track of the number of slots that should be available.
     */
    private int dynamicSize;
    /**
     * Counter that delays acting on onQueueUpdate until a threshold is exceeded.
     */
    private int delayCounter;

    public DynamicWorkQueueSemaphore(final int concurrentTasksSize) {
      dynamicSize = concurrentTasksSize;
      semaphore = new Semaphore(concurrentTasksSize);
    }

    @Override
    public void onQueueUpdate(final String queueName, final int numberOfWorkers, final int numberOfMessages, final int numberOfMessagesReady) {
      synchronized (this) {
        final boolean messagesWaiting = numberOfMessagesReady > 0;
        final int olddynamicSize = dynamicSize;

        // allowUpscale() has a side-effect that it counts the number of times it's called, therefore it should be called first.
        if (allowUpscale() && semaphore.availablePermits() == 0 && !messagesWaiting) {
          final int increaseSize = computeIncreaseSize(dynamicSize);
          semaphore.release(increaseSize);
          dynamicSize += increaseSize;
        } else if (messagesWaiting && dynamicSize > 1) {
          dynamicSize--;
          semaphore.tryAcquire();
        }
        if (dynamicSize != olddynamicSize && LOGGER.isTraceEnabled()) {
          LOGGER.trace("QueueUpdate: tasks:#{}, dynamicSize: #{}({}), messages:#{}", numberOfTasksRunning.get(), dynamicSize, olddynamicSize,
              numberOfMessagesReady);
        }
      }
    }

    /**
     * Dynamically compute the size with which the number of tasks should be increased. If the size becomes larger it takes longer to increase the
     * number of workers because it takes longer until the threshold counter exceeds. Therefore if the current size is large we take larger steps to
     * reach maximal optimal usage quicker.
     *
     * @param currentSize current size of the tasks running
     * @return increase step to increase the number of tasks with
     */
    public int computeIncreaseSize(final int currentSize) {
      return (currentSize > MAGIC_THRESHOLD_FOR_LARGER_STEPS ? STEP_SIZE : 0) + STEP_SIZE;
    }

    public void acquire() throws InterruptedException {
      semaphore.acquire();
      final int newNumberOfTasksRunning = numberOfTasksRunning.incrementAndGet();
      LOGGER.trace("Acquire: tasks:#{}", newNumberOfTasksRunning);
    }

    /**
     * Releases a slot, but only if there are fewer tasks running then the dynamic size.
     */
    public void release() {
      synchronized (this) {
        final int newNumberOfTasksRunning = numberOfTasksRunning.decrementAndGet();

        LOGGER.trace("Release: tasks:#{}, dynamicSize: #{}", newNumberOfTasksRunning, dynamicSize);
        if (dynamicSize > numberOfTasksRunning.get()) {
          semaphore.release();
        }
      }
    }

    /**
     * Draining will release all tasks still waiting on acquire.
     */
    public void drain() {
      synchronized (this) {
        LOGGER.trace("Drain all tasks({}) remaining", semaphore.getQueueLength());
        while (semaphore.hasQueuedThreads()) {
          semaphore.release();
        }
        dynamicSize = 0;
      }
    }

    /**
     * When a job want to claim more slots it is preferred this claiming is somewhat fair between multiple jobs. Because there is no centralized
     * mechanism that knows of all jobs how many slots are claimed it's not possible to implement a centralized fair scheduling algorithm. To
     * get some fair algorithm it's implemented to make it harder to claim a slot the more slots are already claimed. This way jobs we fewer slots
     * should be able to claim a slot faster then other jobs. This should result in a general fair algorithm. The fairness is simply implemented by
     * maintaining a counter each time a measurement is done that when exceeded the actual size of allowed slots triggers. Since when the size grows
     * this will result in more times before the counter reaches this number.
     *
     * @return true if allowed to claim a new slot
     */
    private boolean allowUpscale() {
      LOGGER.trace("Allow: tasks: #:{}, delayCounter: #{}", dynamicSize, delayCounter);
      if (delayCounter >= dynamicSize) {
        delayCounter = 0;
        return true;
      } else {
        delayCounter++;
        return false;
      }
    }
  }
}
