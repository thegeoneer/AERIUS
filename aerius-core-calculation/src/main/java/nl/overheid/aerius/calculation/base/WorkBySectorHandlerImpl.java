/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link WorkHandler} implementation that splits the work into smaller tasks grouped by by sector.
 */
public class WorkBySectorHandlerImpl implements WorkHandler<AeriusPoint> {
  private static final Logger LOGGER = LoggerFactory.getLogger(WorkBySectorHandlerImpl.class);

  private final CalculationTaskFactory taskFactory;
  private final CalculationTaskHandler calculationTaskHandler;
  private final CalculationEngineProvider provider;

  public WorkBySectorHandlerImpl(final CalculationEngineProvider provider, final CalculationTaskFactory taskFactory,
      final CalculationTaskHandler calculationTaskHandler) {
    this.provider = provider;
    this.taskFactory = taskFactory;
    this.calculationTaskHandler = calculationTaskHandler;
  }

  /**
   * Do work for a given calculation and given set of receptors.
   */
  @Override
  public void work(final JobCalculation job, final CalculationJob calculationJob, final int cellId, final Collection<AeriusPoint> calculationPoints)
      throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final Map<Sector, Collection<EngineSource>> sourceBySector = job.getSourcesFor(cellId);
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("[calculationId:{}], calculate cellId:{}, #receptors:{}", job.getCalculationId(), cellId, calculationPoints.size());
    }
    for (final Entry<Sector, Collection<EngineSource>> entry : sourceBySector.entrySet()) {
      final CalculationTask<EngineSource, AeriusPoint, EngineInputData<EngineSource, AeriusPoint>> task =
          taskFactory.createTask(provider.getCalculationEngine(entry.getKey()));
      final CalculationSetOptions cso = calculationJob.getCalculationSetOptions();

      task.setOptions(cso.getSubstances(), cso.getEmissionResultKeys(), calculationJob.getYear());
      task.setSources(entry.getValue());
      final WorkKey workKey = new WorkKey(task.getWorkerType().getTaskQueueName(calculationJob.getQueueName()), calculationJob.getWorkId(),
          job.getCalculationId(), entry.getKey().getSectorId());
      calculationTaskHandler.work(workKey, task, calculationPoints);
    }
  }
}
