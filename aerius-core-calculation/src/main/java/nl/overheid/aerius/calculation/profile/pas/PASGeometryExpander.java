/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.conversion.OPSSourceConverter;
import nl.overheid.aerius.calculation.conversion.SRMSourceConverter;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.ShippingEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

public class PASGeometryExpander implements SourceConverter {

  private final SRMSourceConverter srmGeometryExpander = new SRMSourceConverter();
  private final OPSSourceConverter opsGeometryExpander;
  private final CalculationEngineProvider provider;

  public PASGeometryExpander(final Connection con, final CalculationEngineProvider provider) throws SQLException {
    this.provider = provider;
    opsGeometryExpander = new OPSSourceConverter(con);
  }

  @Override
  public boolean convert(Connection con, final List<EngineSource> expanded, final EmissionSource originalSource, final List<EmissionValueKey> keys)
      throws AeriusException {
    if (provider.getCalculationEngine(originalSource.getSector()) == CalculationEngine.ASRM2) {
      return srmGeometryExpander.convert(expanded, originalSource, keys);
    } else {
      return opsGeometryExpander.convert(con, expanded, originalSource, keys);
    }
  }

  @Override
  public Sector mapToSector(final EmissionSource es) {
    return es.getSector();
  }

  /**
   * Some emission source objects (the shipping sources) are always converted to OPS sources. If they would be assigned an sector
   * that is to be calculated with another srm2 this would result in sending ops sources to the srm2 worker. Subsequently this will
   * crash the srm2 worker. Therefore this guard will throw an exception when such a combination would be used as input.
   *
   * @param emissionSource source to check
   * @throws AeriusException exception in case the sector for the source doesn't use OPS as calculation engine.
   */
  @Override
  public void validate(final EmissionSource emissionSource) throws AeriusException {
    final Sector sector = emissionSource.getSector();

    if (emissionSource instanceof ShippingEmissionSource && provider.getCalculationEngine(sector) != CalculationEngine.OPS) {
      throw new AeriusException(Reason.SHIPPING_INVALID_SECTOR, emissionSource.getLabel(), sector.getName(), String.valueOf(sector.getSectorId()));
    }
  }
}
