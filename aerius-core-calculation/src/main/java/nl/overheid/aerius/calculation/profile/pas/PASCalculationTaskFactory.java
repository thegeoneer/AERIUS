/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.base.CalculationTaskFactory;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.ops.OPSVersion;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.srm2.domain.SRM2InputData;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Implementation of {@link CalculationTaskFactory} specific for PAS.
 */
public class PASCalculationTaskFactory implements CalculationTaskFactory {
  private static final Logger LOGGER = LoggerFactory.getLogger(PASCalculationTaskFactory.class);

  private final int surfaceZoomLevel1;

  public PASCalculationTaskFactory(final ReceptorGridSettings rgs) {
    surfaceZoomLevel1 = rgs.getZoomLevel1().getSurfaceLevel1();
  }

  @Override
  public <E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> CalculationTask<E, R, T> createTask(
      final CalculationEngine calculationEngine) throws AeriusException {
    final CalculationTask<E, R, T> calculationTask;
    switch (calculationEngine) {
    case ASRM2:
      calculationTask = new CalculationTask<E, R, T>(WorkerType.ASRM2, (T) new SRM2InputData(Profile.PAS));
      break;
    case OPS:
      calculationTask = new CalculationTask<E, R, T>(WorkerType.OPS, (T) new OPSInputData(OPSVersion.VERSION, surfaceZoomLevel1));
      break;
    default:
      LOGGER.error("Calculation engine {} not supported for WETNB profile.", calculationEngine);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    return calculationTask;
  }
}
