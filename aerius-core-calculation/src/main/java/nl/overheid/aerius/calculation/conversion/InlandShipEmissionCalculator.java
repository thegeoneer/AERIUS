/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.db.common.sector.InlandCategoryEmissionFactorKey;
import nl.overheid.aerius.db.common.sector.InlandShippingRoutePoint;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.HasShippingRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.NavigationDirection;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.ShippingLaden;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Util class to calculate Inland shipping route or mooring sources to single emission points.
 */
final class InlandShipEmissionCalculator {

  private static final int MAX_PERCENTAGE = 100;

  private InlandShipEmissionCalculator() {
    // Util class
  }

  /**
   * @param con Database connection.
   * @param vgev The VesselGroupEmissionValues to determine the total emissions for.
   * @param geometry
   * @param keys
   * @return The total emissions (per substance, year) for this particular vesselgroup.
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case of other problems.
   */
  public static EmissionValues getEmissions(final Connection con, final InlandMooringVesselGroup vgev, final List<EmissionValueKey> keys)
      throws SQLException, AeriusException {
    //number of ships is arriving/departing, so /2 (assume UI has done the job of assuring there's equal amounts going out as in).
    final EmissionValues emissions = setInlandMooringDockedEmissionValues(con, vgev,
        vgev.getShipsResidenceTimeLadenPerYear() + vgev.getShipsResidenceTimeUnLadenPerYear(), keys.get(0).getYear());
    //next add all the route emissions.
    for (final InlandMooringRoute route : vgev.getRoutes()) {
      emissions.addEmissions(getInlandMooringRoutesEmissionPoints(con, vgev, route, keys));
    }
    return emissions;
  }

  /**
   * @param con Database connection.
   * @param vrev The VesselGroupEmissionValues to determine the total emissions for.
   * @param keys
   * @return The total emissions (per substance, year) for this particular vesselgroup.
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException
   */
  public static EmissionValues getEmissions(final Connection con, final RouteInlandVesselGroup vrev,
      final WKTGeometry geometry, final List<EmissionValueKey> keys, final HasShippingRoute parentSource)
          throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints =
        InlandRouteEmissionsCalculator.getRoutePoints(con, geometry, parentSource.getInlandWaterwayType());
    return calculateInlandRouteEmissions(con, routePoints, vrev, keys);
  }

  private static EmissionValues setInlandMooringDockedEmissionValues(final Connection con, final InlandMooringVesselGroup vgev,
      final double residenceTime, final int year) throws SQLException {
    final EmissionValues emissionValues = new EmissionValues();
    final Map<EmissionValueKey, Double> emissionFactors =
        ShippingCategoryRepository.findInlandCategoryMooringEmissionFactors(con, vgev.getCategory(), year);
    for (final Entry<EmissionValueKey, Double> entry : emissionFactors.entrySet()) {
      emissionValues.setEmission(entry.getKey(), residenceTime * entry.getValue());
    }
    return emissionValues;
  }

  private static EmissionValues getInlandMooringRoutesEmissionPoints(final Connection con, final InlandMooringVesselGroup imveg,
      final InlandMooringRoute route, final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    final EmissionValues emissionValues;
    if (route == null || route.getRoute() == null || route.getShipMovementsPerTimeUnit() == 0) {
      emissionValues = new EmissionValues();
    } else {
      final List<InlandShippingRoutePoint> routePoints =
          InlandRouteEmissionsCalculator.getRoutePoints(con, route.getRoute().getGeometry(), route.getRoute().getInlandWaterwayType());
      emissionValues = getInlandMooringRouteEmissionSources(con, routePoints, imveg, route, keys);
    }
    return emissionValues;
  }

  private static EmissionValues getInlandMooringRouteEmissionSources(final Connection con,
      final List<InlandShippingRoutePoint> routePoints, final InlandMooringVesselGroup imvg, final InlandMooringRoute route,
      final List<EmissionValueKey> keys) throws SQLException {
    //use the normal RouteInlandVesselGroup way at this point.
    final RouteInlandVesselGroup ivg = new RouteInlandVesselGroup();
    ivg.setCategory(imvg.getCategory());

    //the route is coded from dock, so arrive is from B to A, departure is from A to B.
    if (route.getDirection() == NavigationDirection.ARRIVE) {
      ivg.setNumberOfShipsBtoAperTimeUnit(route.getShipMovementsPerTimeUnit());
      ivg.setTimeUnitShipsBtoA(route.getTimeUnit());
      ivg.setPercentageLadenBtoA(route.getPercentageLaden());
    } else {
      ivg.setNumberOfShipsAtoBperTimeUnit(route.getShipMovementsPerTimeUnit());
      ivg.setTimeUnitShipsAtoB(route.getTimeUnit());
      ivg.setPercentageLadenAtoB(route.getPercentageLaden());
    }
    return calculateInlandRouteEmissions(con, routePoints, ivg, keys);
  }

  private static EmissionValues calculateInlandRouteEmissions(final Connection con, final List<InlandShippingRoutePoint> routePoints,
      final RouteInlandVesselGroup ivg, final List<EmissionValueKey> keys) throws SQLException {
    final EmissionValues emissionValues = new EmissionValues();
    final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors =
        ShippingCategoryRepository.findInlandCategoryRouteEmissionFactors(con, ivg.getCategory(), keys.get(0).getYear());

    for (final InlandShippingRoutePoint routePoint : routePoints) {
      //add laden A to B emissionsources
      if (ivg.getPercentageLadenAtoB() > 0) {
        calculateInlandRoutePointEmissions(emissionValues, emissionFactors, keys, routePoint, ivg, false, ShippingLaden.LADEN);
      }
      //add laden B to A emissionsources
      if (ivg.getPercentageLadenBtoA() > 0) {
        calculateInlandRoutePointEmissions(emissionValues, emissionFactors, keys, routePoint, ivg, true, ShippingLaden.LADEN);
      }
      //add unladen A to B emissionsources
      if (ivg.getPercentageLadenAtoB() < MAX_PERCENTAGE) {
        calculateInlandRoutePointEmissions(emissionValues, emissionFactors, keys, routePoint, ivg, false, ShippingLaden.UNLADEN);
      }
      //add unladen B to A emissionsources
      if (ivg.getPercentageLadenBtoA() < MAX_PERCENTAGE) {
        calculateInlandRoutePointEmissions(emissionValues, emissionFactors, keys, routePoint, ivg, true, ShippingLaden.UNLADEN);
      }
    }
    return emissionValues;
  }

  private static void calculateInlandRoutePointEmissions(final EmissionValues emissionValues,
      final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors,
      final List<EmissionValueKey> keys, final InlandShippingRoutePoint routePoint, final RouteInlandVesselGroup ivg,
      final boolean reverse, final ShippingLaden laden) {
    //determine the right direction and # ships per year based on reverse & laden
    final WaterwayDirection d = routePoint.getDirection();
    final WaterwayDirection direction = d != null && reverse ? d.getOpposite() : d;
    final double shipsPerYear = reverse ? ivg.getShipsBtoAPerYear(laden) : ivg.getShipsAtoBPerYear(laden);

    if (shipsPerYear > 0) {
      //emission values and characteristics can be different based on direction and laden/unladen.
      //this causes quite some sources, but these can be (and are) aggregated with the EmissionSourceAggregator.
      emissionValues.addEmissions(getInlandRouteEmissionValues(emissionFactors, keys, direction, laden, routePoint, shipsPerYear));
    }
  }

  private static EmissionValues getInlandRouteEmissionValues(final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors,
      final List<EmissionValueKey> keys, final WaterwayDirection direction, final ShippingLaden laden,
      final InlandShippingRoutePoint routePoint, final double shipsPerYear) {
    final EmissionValues emissionValues = new EmissionValues();
    for (final EmissionValueKey key : keys) {
      emissionValues.setEmission(key, InlandRouteEmissionsCalculator.getInlandRouteEmission(emissionFactors, routePoint,
          new InlandCategoryEmissionFactorKey(key, direction, laden, routePoint.getWaterwayCategoryId()), shipsPerYear));
    }
    return emissionValues;
  }
}
