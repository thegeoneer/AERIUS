/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.road.OPSRoadRunner;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Split the sources into 2 groups. 1 group has no receptors for all the sources in the group that are within 3km of any source. The other group is
 * the rest. The latter are called the nearby sources.
 *
 * <p>For more information @see {@link OPSRoadRunner}.
 */
class OpsRoadCalculationTaskHandler implements CalculationTaskHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(OpsRoadCalculationTaskHandler.class);

  private static final int ROAD_RELEVANT_DISTANCE = 5000;
  private static final int ROAD_INCLUDED_DISTANCE = 5000;

  private final CalculationTaskHandler wrappedHandler;
  private final EmissionSourceListSTRTree sourcesTree;

  public OpsRoadCalculationTaskHandler(final EmissionSourceListSTRTree sourcesTree, final CalculationTaskHandler wrappedHandler) {
    this.sourcesTree = sourcesTree;
    this.wrappedHandler = wrappedHandler;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <E extends EngineSource, T extends CalculationTask<E, ?, ?>> void work(final WorkKey workKey, final T task,
      final Collection<AeriusPoint> points) throws AeriusException, InterruptedException, TaskCancelledException {
    final Collection<E> nearbyPoints = new HashSet<>();
    Collection<E> sources = task.getTaskInput().getEmissionSources();
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Start with sources #{}", sources.size());
    }

    for (final AeriusPoint aeriusPoint : points) {
      if (sourcesTree.findShortestDistance(aeriusPoint) <= ROAD_RELEVANT_DISTANCE) {
        final Map<Boolean, List<EngineSource>> collect = sources.stream()
            .collect(Collectors.partitioningBy(s -> aeriusPoint.distance(((OPSSource) s).getPoint()) <= ROAD_INCLUDED_DISTANCE));
        sources = (Collection<E>) collect.get(Boolean.FALSE);
        nearbyPoints.addAll((Collection<E>) collect.get(Boolean.TRUE));
      }
    }
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Found sources #{}, nearby #{}", sources.size(), nearbyPoints.size());
    }
    ((OPSInputData) task.getTaskInput()).setNearbySources((Collection<OPSSource>) nearbyPoints);
    task.getTaskInput().setEmissionSources(sources);
    wrappedHandler.work(workKey, task, points);
  }

}
