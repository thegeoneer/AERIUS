/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Store emission calculation results organized by point and with each point the result per sector.
 */
class SectorResults implements Serializable {

  private static final long serialVersionUID = -2389750026684234828L;

  private final Set<Integer> sectors;
  private final Map<AeriusPoint, SectorsResultPoint> results = new ConcurrentHashMap<>();
  private final int sectorsCount;

  /**
   * Creates a results storage expecting the given sectors to be stored results for
   * @param sectors sectors to store results for
   */
  public SectorResults(final Set<Integer> sectors) {
    this.sectors = sectors;
    sectorsCount = sectors.size();
  }

  /**
   * Initializes the storage for the give point.
   * @param ap point to store
   * @return returns the new point
   */
  public SectorsResultPoint put(final AeriusPoint ap) {
    final SectorsResultPoint p = new SectorsResultPoint(ap);
    results.put(p, p);
    return p;
  }

  /**
   * Puts the result for the given result point at the given point for the given sector
   * @param sectorId sector to relate the results with
   * @param resultPoint point and results to get emission results from
   * @return point results are stored in.
   */
  public SectorsResultPoint put(final int sectorId, final AeriusResultPoint resultPoint) {
    if (sectors.contains(sectorId)) {
      synchronized (this) {
        final SectorsResultPoint srp = results.containsKey(resultPoint) ? results.get(resultPoint) : put(resultPoint);
        srp.put(sectorId, resultPoint.getEmissionResults());
        return srp.size() == sectorsCount ? srp : null;
      }
    } else {
      throw new IllegalArgumentException("Results for sector " + sectorId + " retrieved but sector not in sectors list");
    }
  }

  @Override
  public String toString() {
    return "SectorResults [sectors=" + sectors + ", results=" + results + ", sectorsCount=" + sectorsCount + "]";
  }
}
