/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.base.AggregationProfilePicker;
import nl.overheid.aerius.calculation.base.EngineSourceAggregator;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.EngineSourceExpander;
import nl.overheid.aerius.calculation.domain.AutoFillingHashMap;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.domain.LevelStore;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * Takes a calculationjob, which contains job calculations, which contains sources.
 *
 * For each job calculation, segregate into sectors.
 *
 * For each segregated set, convert to engine sources.
 *
 * For each sector, stick in grid.
 *
 * For each grid cell, aggregate.
 */
public class GridEmissionSourceDigestor {
  private final class LevelizedGridMap extends AutoFillingHashMap<AggregationDistanceProfile, Map<Sector, LevelStore<GridZoomLevel, EngineSource>>> {
    private static final long serialVersionUID = 9043629118140613446L;

    @Override
    protected SectorMap getEmptyObject() {
      return new SectorMap();
    }
  }

  private final class SectorMap extends AutoFillingHashMap<Sector, LevelStore<GridZoomLevel, EngineSource>> {
    private static final long serialVersionUID = 4954101953510638025L;

    @Override
    protected LevelStore<GridZoomLevel, EngineSource> getEmptyObject() {
      return new GridLevelStore<>();
    }
  }

  private final PMF pmf;
  private final GridUtil gridUtil;
  private final AggregationProfilePicker<AggregationDistanceProfile> profilePicker;
  private final EngineSourceAggregator<EngineSource, Substance> aggregator;

  public GridEmissionSourceDigestor(final PMF pmf, final GridUtil gridUtil, final EngineSourceAggregator<EngineSource, Substance> aggregator,
      final AggregationProfilePicker<AggregationDistanceProfile> aggregationProfilePicker) {
    this.pmf = pmf;
    this.gridUtil = gridUtil;
    this.aggregator = aggregator;
    this.profilePicker = aggregationProfilePicker;
  }

  /**
   * transform Levelize Aggregate
   *
   * @param key
   * @throws AeriusException
   * @throws SQLException
   */
  public void digest(final SourceConverter geometryExpander, final CalculationJob calculationJob, final List<EmissionValueKey> keys)
      throws SQLException, AeriusException {
    for (final Calculation job : calculationJob.getCalculatedScenario().getCalculations()) {
      final CalculationSetOptions cso = calculationJob.getCalculationSetOptions();
      final Map<Sector, List<EngineSource>> expanded;

      try (Connection con = pmf.getConnection()) {
        expanded = EngineSourceExpander.toEngineSourcesBySector(con, geometryExpander, job.getSources(), keys);
      }
      final LevelizedGridMap originals = new LevelizedGridMap();
      final LevelizedGridMap aggregated = new LevelizedGridMap();

      getEmissionSourceStore(originals, aggregated, expanded, cso.getSubstances(), cso.isForceAggregation());

      final SourcesStoreImpl store = new SourcesStoreImpl(gridUtil, originals, aggregated);
      final JobCalculation jobCalculation = calculationJob.findJobCalculation(job.getCalculationId());
      jobCalculation.setSourcesStore(store);
    }
  }

  private void getEmissionSourceStore(final LevelizedGridMap originals, final LevelizedGridMap aggregated,
      final Map<Sector, List<EngineSource>> unprocessedOriginals, final List<Substance> substances, final boolean forceAggregation) {
    for (final Entry<Sector, List<EngineSource>> entry : unprocessedOriginals.entrySet()) {
      final Sector sector = entry.getKey();
      final AggregationDistanceProfile profile = profilePicker.getAggregationProfile(sector.getSectorId(), forceAggregation);

      final LevelStore<GridZoomLevel, EngineSource> aggregatedLevelStore = aggregated.get(profile).get(sector);
      final LevelStore<GridZoomLevel, EngineSource> originalLevelStore = originals.get(profile).get(sector);
      final Iterable<EngineSource> sectorSources = entry.getValue();

      // Insert sources into original set.
      insertSourcesIntoLevels(originalLevelStore, sectorSources);
      insertSourcesAggregatedLevels(substances, aggregatedLevelStore, originalLevelStore);
    }
  }

  private void insertSourcesIntoLevels(final LevelStore<GridZoomLevel, EngineSource> originalLevelStore, final Iterable<EngineSource> sectorSources) {
    for (final EngineSource src : sectorSources) {
      final Point pointFromSource = getPointFromSource(src);
      for (final GridZoomLevel level : gridUtil.getGridSettings().getZoomLevels()) {
        originalLevelStore.add(level, gridUtil.getCellFromPosition(pointFromSource, level), src);
      }
    }
  }

  private Point getPointFromSource(final EngineSource src) {
    if (src instanceof OPSSource) {
      return ((OPSSource) src).getPoint();
    } else if (src instanceof SRM2RoadSegment) {
      final SRM2RoadSegment seg = (SRM2RoadSegment) src;
      return new Point((seg.getStartX() + seg.getEndX()) / 2, (seg.getStartY() + seg.getEndY()) / 2);
    } else {
      throw new IllegalArgumentException("Unknown EngineSource encountered - cannot handle this. " + src);
    }
  }

  private void insertSourcesAggregatedLevels(final List<Substance> substances, final LevelStore<GridZoomLevel, EngineSource> aggregatedLevelStore,
      final LevelStore<GridZoomLevel, EngineSource> originalLevelStore) {
    for (final GridZoomLevel level : gridUtil.getGridSettings().getZoomLevels()) {
      // Aggregate sources for each cell in the original store, for this level
      for (final Entry<Integer, Collection<EngineSource>> originalEntry : originalLevelStore.getLevel(level).entrySet()) {
        aggregatedLevelStore.addAll(level, originalEntry.getKey(), aggregator.aggregate(originalEntry.getValue(), substances));
      }
    }
  }
}
