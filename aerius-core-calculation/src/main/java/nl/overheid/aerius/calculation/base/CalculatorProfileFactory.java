/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface to handle separate logic needed for calculation for different profiles.
 */
public interface CalculatorProfileFactory {

  /**
   * Called on initialization of a calculation and can be used to force certain options.
   *
   * @param scenario scenario to being calculated
   */
  void prepareOptions(CalculatedScenario scenario);

  /**
   * Maps an emission source to the sector it should be grouped by for calculation.
   */
  Sector mapToSector(EmissionSource source);

  /**
   * Returns the {@link CalculationEngineProvider} related to this profile.
   */
  CalculationEngineProvider getProvider(CalculationJob calculationJob);

  /**
   * Returns the {@link SourceConverter} related to this profile.
   */
  SourceConverter getSourceConverter(Connection con, CalculationJob calculationJob) throws SQLException;

  /**
   * Returns a filter that will be used to filter out intermediate results.
   */
  IncludeResultsFilter getIncludeResultFilter(CalculationSetOptions calculationSetOptions);

  /**
   * Returns additional handlers for profile specific actions. Should return an empty list if no handlers available to return.
   */
  List<CalculationResultHandler> getCalculationResultHandler(CalculationJob calculationJob);

  /**
   * Digest all sources and potentially store them in the {@link CalculationJob} to be used by later on.
   */
  void digest(CalculationJob calculationJob, List<EmissionValueKey> keys) throws SQLException, AeriusException;

  /**
   * Creates a profile specific work handler that can be used to process the input before it is send to be calculated.
   *
   * @param calculationJob data object with input data
   * @param workHandler handler to which the processed calculation should be passed on
   * @param resultHandler the handler to which calculation results or errors should be send to
   * @return a profile specific work handler
   * @throws AeriusException
   */
  WorkHandler<AeriusPoint> createWorkHandler(CalculationJob calculationJob, CalculationTaskHandler workHandler, ResultHandler resultHandler)
      throws AeriusException;

  /**
   * Creates a profile specific {@link WorkDistributor} object.
   */
  WorkDistributor<AeriusPoint> createWorkDistributor(CalculationJob calculationJob) throws AeriusException, SQLException;
}
