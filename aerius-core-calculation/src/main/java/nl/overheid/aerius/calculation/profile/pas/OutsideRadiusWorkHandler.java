/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link WorkHandler} that sets all results to 0.0. This is used for sources that lay outside the range to have effect.
 * This handler doesn't check if there is a range condition it just passes back the results as zero values.
 */
class OutsideRadiusWorkHandler implements WorkHandler<AeriusPoint> {

  private final ResultHandler resultHandler;

  public OutsideRadiusWorkHandler(final ResultHandler resultHandler) {
    this.resultHandler = resultHandler;
  }

  @Override
  public void work(final JobCalculation job, final CalculationJob calculation, final int cellId, final Collection<AeriusPoint> points)
      throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final List<AeriusResultPoint> emptyResults = createEmptyResults(points, calculation.getCalculationSetOptions().getEmissionResultKeys());

    for (final Sector sector: job.getSectors()) {
      final WorkKey workKey = new WorkKey("", calculation.getWorkId(), job.getCalculationId(), sector.getSectorId());
      final String taskId = resultHandler.registerTask(workKey);
      try {
        resultHandler.onResult(taskId, emptyResults);
      } catch (final Exception e) {
        resultHandler.onFailure(taskId, e);
      }
    }
  }

  private List<AeriusResultPoint> createEmptyResults(final Collection<AeriusPoint> points, final Set<EmissionResultKey> keys) {
    final List<AeriusResultPoint> results = new ArrayList<>();
    for (final AeriusPoint receptor : points) {
      final AeriusResultPoint arp = new  AeriusResultPoint(receptor.getId(), receptor.getPointType(), receptor.getX(), receptor.getY());
      for (final EmissionResultKey key : keys) {
        arp.setEmissionResult(key, 0.0);
      }
      results.add(arp);
    }
    return results;
  }
}
