/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.SQLException;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.base.CalculationJobWorkHandler;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
class ResearchAreaWorkDistributor implements WorkDistributor<AeriusPoint> {

  private final Natura2kGridPointStore pointStore;

  public ResearchAreaWorkDistributor(final Natura2kGridPointStore pointStore) {
    this.pointStore = pointStore;
  }

  @Override
  public void distribute(final CalculationJob calculationJob, final CalculationJobWorkHandler<AeriusPoint> workHandler)
      throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final int radius = calculationJob.getCalculationSetOptions().getPermitCalculationRadiusType().getRadius();
    final double range = Math.ceil(radius + pointStore.getGridDiameter());
    final EmissionSourceListSTRTree researchAreaTree = calculationJob.getResearchAreaTree();

    for (final Entry<Integer, Point> cell : pointStore.getGridPoints()) {
      if (researchAreaTree.findShortestDistance(cell.getValue()) < range) {
        workHandler.work(calculationJob, cell.getKey(), pointStore.getGridCell(cell.getKey()));
      }
    }
  }
}
