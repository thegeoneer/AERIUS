/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.mutable.MutableDouble;

import nl.overheid.aerius.calculation.base.CalculatorProfileFactory;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.UnitsPerSectorPerEngineAccumulator;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;

/**
 * Collect summary results of a calculation and store the results in a file.
 */
public class SummaryCollector {

  private static final String HEADER =
      "result_type;sector;substance;result_min;result_max;result_sum;result_average;source_count;total_emission;OPS;SRM2;result_filename";
  private static final String FILENAME_FORMAT = "%s.summary";
  private static final String DELIMITER = ";";

  private final Map<String, SectorSummary> sectorSummaries = new TreeMap<>();
  private final CalculatorProfileFactory profileFactory;
  private final CalculationJob calculationJob;
  private final ArrayList<EmissionValueKey> emissionValueKeys;
  private final long startTime = System.currentTimeMillis();

  public SummaryCollector(final CalculatorProfileFactory profileFactory, final CalculationJob calculationJob) {
    this.profileFactory = profileFactory;
    this.calculationJob = calculationJob;
    emissionValueKeys = EmissionValueKey.getEmissionValueKeys(calculationJob.getYear(), calculationJob.getCalculationSetOptions().getSubstances());
  }

  /**
   * Initialize summary for a calculation id, sector id and {@link EmissionResultType} combination.
   *
   * @param calculationId calculation id
   * @param ert emission result type
   * @param filename name of the file results for calculation are stored in
   */
  public void addSummary(final int calculationId, final EmissionResultType ert, final String filename) {
    final Map<Integer, List<EmissionSource>> sectorSources = countSourcesBySector(calculationId);

    sectorSources.forEach((sectorId, sources) ->
    sectorSummaries.put(key(calculationId, sectorId, ert),
        new SectorSummary(sectorId, filename, sources.size(), summarizeEmissions(sources), calculationJob.getUnitsAccumulator())));
  }

  private Map<Integer, List<EmissionSource>> countSourcesBySector(final int calculationId) {
    final List<EmissionSource> esl = new ArrayList<>();
    for (final EmissionSource es : calculationJob.getCalculatedScenario().getSources(calculationId)) {
      if (es instanceof SRM2NetworkEmissionSource) {
        esl.addAll(((SRM2NetworkEmissionSource)es).getEmissionSources());
      } else {
        esl.add(es);
      }
    }
    return esl.stream().collect(Collectors.groupingBy(s -> profileFactory.mapToSector(s).getSectorId()));
  }

  private Map<Substance, MutableDouble> summarizeEmissions(final List<EmissionSource> sectorSources) {
    final Map<Substance, MutableDouble> emissions = new EnumMap<>(Substance.class);

    emissionValueKeys.forEach(evk -> emissions.put(evk.getSubstance(), new MutableDouble()));
    sectorSources.forEach(s ->
    emissionValueKeys.forEach(evk ->
    emissions.get(evk.getSubstance()).add(s.getEmission(evk))));
    return emissions;
  }

  /**
   * Processes a single result.
   *
   * @param calculationId id of the calculation
   * @param sectorId sector this result is for
   * @param erk result type of the result
   * @param value result value
   */
  public void addResult(final int calculationId, final int sectorId, final EmissionResultKey erk, final Double value) {
    sectorSummaries.get(key(calculationId, sectorId, erk.getEmissionResultType())).addResult(erk, value);
  }

  private String key(final int calculationId, final int sectorId, final EmissionResultType ert) {
    return calculationId + "-" + sectorId + "-" + ert.toDatabaseString();
  }

  /**
   * Writes the summary results to a file.
   *
   * @param directory directory to write the file to
   * @param name name of the file to write
   */
  public void writeSummary(final String directory, final String name) throws IOException {
    final Path openFile = Paths.get(directory, String.format(FILENAME_FORMAT, name));

    try (final PrintWriter writer = new PrintWriter(Files.newBufferedWriter(openFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE))) {
      writer.println(HEADER);
      sectorSummaries.forEach((s, ss) -> ss.write(writer));
    }
  }

  public String getDuration() {
    return String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));
  }

  /**
   * Data class to collect summary results for a sector.
   */
  private static class SectorSummary {
    private final int sectorIdInt;
    private final String sectorId;
    private final String fileName;
    private final int sourceCount;
    private final Map<Substance, MutableDouble> emissions;
    private final Map<EmissionResultKey, ResultStats> results = new EnumMap<>(EmissionResultKey.class);
    private final UnitsPerSectorPerEngineAccumulator unitsAccumulator;

    public SectorSummary(final Integer sectorId, final String fileName, final int sourceCount, final Map<Substance, MutableDouble> emissions,
        final UnitsPerSectorPerEngineAccumulator unitsAccumulator) {
      this.sourceCount = sourceCount;
      this.emissions = emissions;
      this.sectorIdInt = sectorId;
      this.unitsAccumulator = unitsAccumulator;
      this.sectorId = String.valueOf(sectorId);
      this.fileName = fileName;
    }

    /**
     * Adds a new result for the given result key and increments the number of results with 1.
     * @param key result key
     * @param value value to add
     */
    public void addResult(final EmissionResultKey key, final double value) {
      if (!results.containsKey(key)) {
        results.put(key, new ResultStats());
      }
      results.get(key).add(value);
    }

    /**
     * Writes the summary results as rows to the given writer.
     * @param writer writer to append rows with summary results.
     */
    public void write(final PrintWriter writer) {
      results.forEach((k, v) -> writer.println(collect(k, v, emissions.get(k.getSubstance()).doubleValue())));
    }


    private String collect(final EmissionResultKey erk, final ResultStats results, final double emissionValue) {
      final List<String> values = new ArrayList<>();
      values.add(erk.getEmissionResultType().toDatabaseString());
      values.add(sectorId);
      values.add(erk.getSubstance().toDatabaseString());
      values.add(String.valueOf(results.min));
      values.add(String.valueOf(results.max));
      values.add(String.valueOf(results.sum));
      values.add(String.valueOf(results.getAverage()));
      values.add(String.valueOf(sourceCount));
      values.add(String.valueOf(emissionValue));
      values.add(String.valueOf(unitsAccumulator.getUnits(sectorIdInt, CalculationEngine.OPS)));
      values.add(String.valueOf(unitsAccumulator.getUnits(sectorIdInt, CalculationEngine.ASRM2)));
      values.add(fileName);
      return values.stream().collect(Collectors.joining(DELIMITER));
    }

    @Override
    public String toString() {
      return "SectorSummary [sectorId=" + sectorId + ", fileName=" + fileName + ", sourceCount=" + sourceCount + ", emissions=" + emissions
          + ", results=" + results + "]";
    }
  }

  private static class ResultStats {
    double min = Double.MAX_VALUE;
    double max;
    double sum;
    int count;

    public void add(final double value) {
      min = Math.min(value, min);
      max = Math.max(value, max);
      sum += value;
      count++;
    }

    public double getAverage() {
      return sum / count;
    }

    @Override
    public String toString() {
      return "ResultStats [min=" + min + ", max=" + max + ", sum=" + sum + ", count=" + count + "]";
    }
  }
}
