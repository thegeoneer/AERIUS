/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.export.ExportType;

/**
 * Data object with options to perform a calculation.
 */
public class CalculationInputData extends ExportData implements Serializable {

  private static final long serialVersionUID = 6029201784844430165L;

  private String name;
  private CalculatedScenario scenario;
  private String calculationWorkerQueue;
  private ExportType exportType;

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public CalculatedScenario getScenario() {
    return scenario;
  }

  public void setScenario(final CalculatedScenario scenario) {
    this.scenario = scenario;
  }

  public ExportType getExportType() {
    return exportType;
  }

  public void setExportType(final ExportType exportType) {
    this.exportType = exportType;
  }

  public boolean isSectorOutput() {
    return exportType == ExportType.GML_WITH_SECTORS_RESULTS;
  }

  public boolean isCsvOutput() {
    return exportType == ExportType.CSV;
  }

  public String getQueueName() {
    return calculationWorkerQueue;
  }

  public void setQueueName(final String queueName) {
    this.calculationWorkerQueue = queueName;
  }

  public boolean isNameEmpty() {
    return name == null || name.isEmpty();
  }
}
