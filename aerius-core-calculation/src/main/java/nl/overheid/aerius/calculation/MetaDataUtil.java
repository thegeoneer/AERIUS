/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.ops.OPSVersion;

/**
 * Collector to store system meta data about versions in a metadata.json file.
 */
final class MetaDataUtil {

  private static final Gson GSON = new Gson();

  private static final String FILENAME_FORMAT = "%s_resulttype-metadata.json";

  private  MetaDataUtil() {
    // Util class
  }

  public static void writeMetadata(final PMF pmf, final CalculationJob calculationJob, final String directory, final String name,
      final String duration) throws IOException, JsonIOException, SQLException {
    final Path openFile = Paths.get(directory, String.format(FILENAME_FORMAT, name));
    try (final PrintWriter writer = new PrintWriter(
        Files.newBufferedWriter(openFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE))) {
      GSON.toJson(setMetaData(pmf, calculationJob, duration), writer);
    }
  }


  private static Map<String, String> setMetaData(final PMF pmf, final CalculationJob calculationJob, final String duration) throws SQLException {
    final Map<String, String> metadataMap = new LinkedHashMap<>();
    metadataMap.put("aerius_version", AeriusVersion.getVersionNumber());
    metadataMap.put("database_version", pmf.getDatabaseVersion());
    metadataMap.put("ops_version", OPSVersion.VERSION);
    metadataMap.put("calculation_year", String.valueOf(calculationJob.getYear()));
    metadataMap.put("receptor_set", calculationJob.getCalculatedScenario().getReceptorSetName());
    metadataMap.put("duration_seconds", duration);
    metadataMap.put("receptor_count", String.valueOf(calculationJob.getCalculatedScenario().getCalculationPoints().size()));
    return metadataMap;
  }


}
