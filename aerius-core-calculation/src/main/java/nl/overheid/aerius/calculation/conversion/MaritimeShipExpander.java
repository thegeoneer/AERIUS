/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.sector.MaritimeShippingRoutePoint;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.ops.domain.OPSCopyFactory;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Util class to expand {@link MaritimeMooringEmissionSource} or {@link MaritimeRouteEmissionSource} to single emission points.
 */
final class MaritimeShipExpander {

  private final Connection con;
  private final SourceConverter gse;
  private final double lineSegmentSize;

  public MaritimeShipExpander(final Connection con, final SourceConverter gse) throws SQLException {
    this.con = con;
    this.gse = gse;
    lineSegmentSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_LINE_TO_POINTS_SEGMENT_SIZE, Double.class);
  }

  /**
   * Converts a {@link MaritimeMooringEmissionSource} object to points object. If a ship is docked the emission is based on the docked emission, the
   * transfer route emission and the emission on the main route. For ships on a route the emission is the emission on the route.
   *
   * @param expanded Add new emission sources to this list
   * @param sev Maritime Emission Source
   * @param keys
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case of other problems
   */
  public void maritimeMooringShipsToPoints(final List<EngineSource> expanded,
      final MaritimeMooringEmissionSource sev, final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    // Calculate the emission points on the geometry. For docked ships this
    // is the emission of stationary vessels over the length of the dock.
    for (final MooringMaritimeVesselGroup vgev : sev.getEmissionSubSources()) {
      //if there aren't any ships, don't even bother to handle them. The operations are relatively expensive.
      if (vgev.getNumberOfShipsPerTimeUnit() > 0) {
        // For docked ships emission is calculated differently for the dock-part and the routes-part.
        expanded.addAll(getMaritimeMooringDockedEmissionSources(con, sev, vgev, keys));
        if (vgev.getInlandRoute() == null) {
          //Emission calculations for a custom shipping route (no dock) is not supported.
          throw new IllegalArgumentException("No route for vesselgroup: " + vgev);
        } else {
          expanded.addAll(getMaritimeMooringRoutesEmissionPoints(con, vgev, lineSegmentSize, keys));
        }
      }
    }
  }

  /**
   * Expands maritime ships on a route to points.
   * @param con
   * @param expanded
   * @param serv
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  public void maritimeRouteToPoints(final List<EngineSource> expanded, final MaritimeRouteEmissionSource serv, final List<EmissionValueKey> keys)
      throws SQLException, AeriusException {
    // This is a shipping route.
    // The emissionsource itself represents the route, use its geometry to determine all route points
    final List<MaritimeShippingRoutePoint> routePoints = ShippingRepository.getMaritimeShippingRoutePoints(con, serv.getGeometry(), lineSegmentSize);
    for (final RouteMaritimeVesselGroup verv : serv.getEmissionSubSources()) {
      //The emissionsource itself represents the route, use its geometry and the right movement type.
      expanded.addAll(
          convertMaritimePointsToEmissionSources(con, routePoints, verv.getCategory(), verv.getShipMovementsPerYear(), verv.getMovementType(), keys));
    }
  }

  private static List<EngineSource> getMaritimeMooringRoutesEmissionPoints(final Connection con,
      final MooringMaritimeVesselGroup vgev, final double maxSegmentSize, final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    final List<EngineSource> pointSources = new ArrayList<>();
    //determine the emission points on the inland part of the route.
    final List<MaritimeShippingRoutePoint> routePoints = ShippingRepository.getMaritimeMooringShippingInlandRoute(con, vgev, maxSegmentSize);
    pointSources.addAll(
        convertMaritimePointsToEmissionSources(con, routePoints, vgev.getCategory(), vgev.getNumberOfShipsPerYear() * 2, ShippingMovementType.INLAND, keys));
    //determine the emission points on the maritime part of the route.
    for (final MaritimeRoute mtr : vgev.getMaritimeRoutes()) {
      if (mtr.getRoute() != null && mtr.getShipMovementsPerTimeUnit() > 0) {
        //The route contains the geometry that should be used, use it to determine all route points
        final List<MaritimeShippingRoutePoint> maritimeRoutePoints =
            ShippingRepository.getMaritimeShippingRoutePoints(con, mtr.getRoute().getGeometry(), maxSegmentSize);
        pointSources.addAll(convertMaritimePointsToEmissionSources(con, maritimeRoutePoints, vgev.getCategory(), mtr.getShipMovementsPerYear(),
            ShippingMovementType.MARITIME, keys));
      }
    }
    return pointSources;
  }

  /**
   * Expands the ship emission values at the dock to points.
   * @param con
   * @param emissionSource
   * @param vgev
   * @param keys
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  private List<EngineSource> getMaritimeMooringDockedEmissionSources(final Connection con, final EmissionSource emissionSource,
      final MooringMaritimeVesselGroup vgev, final List<EmissionValueKey> keys)
          throws SQLException, AeriusException {
    final List<EngineSource> pointSources = new ArrayList<>();
    //Dock can be a line, a polygon or a point. Ensure it's converted no matter what.
    //ensure only the emission for this vesselgroup is used
    final GenericEmissionSource copy = getMaritimeMooringDockedEmissionValues(con, emissionSource, vgev, keys);
    gse.convert(con, pointSources, copy, keys);
    final OPSSourceCharacteristics characteristicsAtDock =
        ShippingCategoryRepository.getCharacteristics(con, vgev.getCategory(), ShippingMovementType.DOCK, keys.get(0).getYear());

    for (final EngineSource pointES : pointSources) {
      //query DB for right characteristics: heatcontent/height for movement type = DOCK.
      OPSCopyFactory.toOpsSource(characteristicsAtDock, (OPSSource) pointES);
    }
    return pointSources;
  }

  private static GenericEmissionSource getMaritimeMooringDockedEmissionValues(final Connection con, final EmissionSource emissionSource,
      final MooringMaritimeVesselGroup vgev, final List<EmissionValueKey> keys) throws SQLException {
    final GenericEmissionSource copy = emissionSource.copyTo(new GenericEmissionSource());
    copy.setEmissionPerUnit(false);
    copy.setYearDependent(true);
    final HashMap<EmissionValueKey, Double> emissionFactors =
        ShippingCategoryRepository.findMaritimeCategoryEmissionFactors(con, vgev.getCategory(), ShippingMovementType.DOCK, keys.get(0).getYear());
    for (final EmissionValueKey key : keys) {
      if (emissionFactors.containsKey(key)) {
        copy.setEmission(key, vgev.getNumberOfShipsPerYear() * vgev.getResidenceTime() * emissionFactors.get(key));
      }
    }
    return copy;
  }

  private static List<EngineSource> convertMaritimePointsToEmissionSources(final Connection con,
      final List<MaritimeShippingRoutePoint> routePoints, final MaritimeShippingCategory category, final int numberOfShips,
      final ShippingMovementType movementType, final List<EmissionValueKey> keys) throws SQLException {
    final List<EngineSource> expanded = new ArrayList<>();
    //query DB for right characteristics: heatcontent/height for the right movement type.
    final OPSSourceCharacteristics characteristics =
        ShippingCategoryRepository.getCharacteristics(con, category, movementType, keys.get(0).getYear());
    //get emissionfactor (kg per meter) per ship from DB for each year, multiply times ships and set it.
    final HashMap<EmissionValueKey, Double> emissionFactors = ShippingCategoryRepository
        .findMaritimeCategoryEmissionFactors(con, category, movementType, keys.get(0).getYear());

    for (final MaritimeShippingRoutePoint routePoint : routePoints) {
      final EngineSource pointES = getMaritimeRouteEmissionSource(emissionFactors, characteristics, routePoint, numberOfShips, keys);
      expanded.add(pointES);
    }
    return expanded;
  }

  private static EngineSource getMaritimeRouteEmissionSource(final Map<EmissionValueKey, Double> emissionFactors,
      final OPSSourceCharacteristics emissionCharacteristics, final MaritimeShippingRoutePoint routePoint, final int numberOfShips,
      final List<EmissionValueKey> keys) {
    final OPSSource source = new OPSSource(0, routePoint.getSrid(), routePoint.getX(), routePoint.getY());
    setEmissionValuesPerPointForRoute(source, emissionFactors, routePoint.getMeasure(), routePoint.getManeuverFactor(), numberOfShips, keys);
    OPSCopyFactory.toOpsSource(emissionCharacteristics, source);
    return source;
  }

  private static void setEmissionValuesPerPointForRoute(final EngineSource gev, final Map<EmissionValueKey, Double> emissionFactors,
      final double measure, final double maneuverFactor, final int numberOfMovements, final List<EmissionValueKey> keys) {
    for (final EmissionValueKey key : keys) {
      if (emissionFactors.containsKey(key)) {
        gev.setEmission(key.getSubstance(), emissionFactors.get(key) * maneuverFactor * measure * numberOfMovements);
      }
    }
  }
}
