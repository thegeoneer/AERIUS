/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.common.sector.InlandCategoryEmissionFactorKey;
import nl.overheid.aerius.db.common.sector.InlandShippingRoutePoint;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Helper class calculation emissions on inland shippin routes.
 */
final class InlandRouteEmissionsCalculator {

  private InlandRouteEmissionsCalculator() {
    // util class
  }

  /**
   * Calculates the emission for a specific route.
   * @param emissionFactors
   * @param routePoint
   * @param efk
   * @param shipsPerYear
   * @return
   */
  public static double getInlandRouteEmission(final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors,
      final InlandShippingRoutePoint routePoint, final InlandCategoryEmissionFactorKey efk, final double shipsPerYear) {
    return emissionFactors.get(efk) == null ? 0 : (emissionFactors.get(efk) * routePoint.getMeasure() * routePoint.getLockFactor() * shipsPerYear);
  }

  /**
   * Queries the points on the route and sets water way type ofn each point.
   * @param con
   * @param geometry geometry of the route
   * @param waterwayType determined water way type of the route. If null it calculates the type for each point.
   * @return
   * @throws SQLException
   * @throws AeriusException
   */
  public static List<InlandShippingRoutePoint> getRoutePoints(final Connection con, final WKTGeometry geometry,
      final InlandWaterwayType waterwayType) throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints;
    if (waterwayType == null || waterwayType.getWaterwayCategory() == null) {
      //old situation where waterways are determined by DB.
      routePoints = ShippingRepository.getInlandShippingRoutePointsWithWaterways(con, geometry);
    } else {
      //new situation where waterways are determined by user.
      routePoints = ShippingRepository.getInlandShippingRoutePoints(con, geometry);
      for (final InlandShippingRoutePoint routePoint : routePoints) {
        routePoint.setWaterwayCategoryId(waterwayType.getWaterwayCategory().getId());
        routePoint.setWaterwayCategoryCode(waterwayType.getWaterwayCategory().getCode());
        routePoint.setDirection(waterwayType.getWaterwayDirection());
      }
    }
    return routePoints;
  }
}
