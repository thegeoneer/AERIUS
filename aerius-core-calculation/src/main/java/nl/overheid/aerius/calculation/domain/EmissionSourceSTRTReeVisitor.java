/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.Set;

import com.vividsolutions.jts.geom.Geometry;

import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Visitor for handling specific {@link EmissionSource} objects. This object instance keeps the state and should only be used once for every list.
 */
class EmissionSourceSTRTReeVisitor implements EmissionSourceVisitor<Void> {

  private final Set<Geometry> geometries;

  public EmissionSourceSTRTReeVisitor(final Set<Geometry> geometries) {
    this.geometries = geometries;
  }

  @Override
  public Void visit(final FarmEmissionSource emissionSource) throws AeriusException {
    visitOther(emissionSource);
    return null;
  }

  @Override
  public Void visit(final GenericEmissionSource emissionSource) throws AeriusException {
    visitOther(emissionSource);
    return null;
  }

  @Override
  public Void visit(final InlandMooringEmissionSource emissionSource) throws AeriusException {
    mooringEmissionSource2Geometries(emissionSource, geometries);
    return null;
  }

  @Override
  public Void visit(final MaritimeMooringEmissionSource emissionSource) throws AeriusException {
    mooringEmissionSource2Geometries(emissionSource, geometries);
    return null;
  }


  private void mooringEmissionSource2Geometries(final MooringEmissionSource<?> emissionSource, final Set<Geometry> geometries)
      throws AeriusException {
    for (final ShippingRoute route : emissionSource.getInlandRoutes()) {
      geometries.add(GeometryUtil.getGeometry(route.getGeometry()));
    }
    geometries.add(GeometryUtil.getGeometry(emissionSource.getGeometry()));
  }

  @Override
  public Void visit(final InlandRouteEmissionSource emissionSource) throws AeriusException {
    visitOther(emissionSource);
    return null;
  }

  @Override
  public Void visit(final MaritimeRouteEmissionSource emissionSource) throws AeriusException {
    visitOther(emissionSource);
    return null;
  }

  @Override
  public Void visit(final OffRoadMobileEmissionSource emissionSource) throws AeriusException {
    visitOther(emissionSource);
    return null;
  }

  @Override
  public Void visit(final PlanEmissionSource emissionSource) throws AeriusException {
    visitOther(emissionSource);
    return null;
  }

  @Override
  public Void visit(final SRM2EmissionSource emissionSource) throws AeriusException {
    visitOther(emissionSource);
    return null;
  }

  @Override
  public Void visit(final SRM2NetworkEmissionSource emissionSource) throws AeriusException {
    for (final EmissionSource srm2RoadSource : emissionSource.getEmissionSources()) {
      srm2RoadSource.accept(this);
    }
    return null;
  }

  private void visitOther(final EmissionSource emissionSource) throws AeriusException {
    geometries.add(GeometryUtil.getGeometry(emissionSource.getGeometry()));
  }
}
