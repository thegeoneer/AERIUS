/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.Collection;
import java.util.Map;

import nl.overheid.aerius.calculation.grid.GridZoomLevel;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.sector.Sector;

/**
 * Data class to store temporary data for 1 situation created during a calculation.
 */
public class JobCalculation {

  private final int calculationId;
  private SourcesStore sourcesStore;
  private final Iterable<Sector> sectors;

  public JobCalculation(final int calculationId, final Iterable<Sector> sectors) {
    this.calculationId = calculationId;
    this.sectors = sectors;
  }

  public int getCalculationId() {
    return calculationId;
  }

  public Map<Sector, Collection<EngineSource>> getSourcesFor(final int cellId) {
    return sourcesStore.getSourcesFor(cellId);
  }

  public Iterable<Sector> getSectors() {
    return sectors;
  }

  public void setSourcesStore(final SourcesStore sourcesStore) {
    this.sourcesStore = sourcesStore;
  }

  public Iterable<Integer> getSourceGridIds(final GridZoomLevel zoomLevel) {
    return sourcesStore.getSourceGridIds(zoomLevel);
  }
}
