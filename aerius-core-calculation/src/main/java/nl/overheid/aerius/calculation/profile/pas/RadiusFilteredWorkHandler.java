/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Proxy {@link WorkHandler} to split tasks into tasks to be send to worker and tasks that can be handled directly because calculation
 * points are outside the scope.
 */
class RadiusFilteredWorkHandler implements WorkHandler<AeriusPoint> {

  private final WorkHandler<AeriusPoint> remoteWorkHandler;
  private final WorkHandler<AeriusPoint> outsideRangeHandler;
  private final ReceptorFilter filter;


  public RadiusFilteredWorkHandler(final WorkHandler<AeriusPoint> remoteWorkHandler, final WorkHandler<AeriusPoint> outsideRangeHandler,
      final EmissionSourceListSTRTree sourcesTree, final int radius) {
    this.remoteWorkHandler = remoteWorkHandler;
    this.outsideRangeHandler = outsideRangeHandler;
    filter = receptor -> sourcesTree.findShortestDistance(receptor) <= radius;
  }

  @Override
  public void work(final JobCalculation job, final CalculationJob calculation, final int cellId, final Collection<AeriusPoint> points)
      throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final List<AeriusPoint> remoteReceptors = new ArrayList<>();
    final List<AeriusPoint> outsideScopeReceptors = new ArrayList<>();

    for (final AeriusPoint receptor : points) {
      if (filter.needsCalculation(receptor)) {
        remoteReceptors.add(receptor);
      } else {
        outsideScopeReceptors.add(receptor);
      }
    }
    if (!remoteReceptors.isEmpty()) {
      remoteWorkHandler.work(job, calculation, cellId, remoteReceptors);
    }
    if (!outsideScopeReceptors.isEmpty()) {
      outsideRangeHandler.work(job, calculation, cellId, outsideScopeReceptors);
    }
  }

  /**
   * Filters receptors based on certain criteria, like distance to the source.
   */
  interface ReceptorFilter {

    /**
     * Returns true if the given point needs to be calculated or if should return with 0 value results.
     * @param resultPoint the point to test
     * @return true if should be calculated
     * @throws AeriusException
     */
    boolean needsCalculation(AeriusPoint resultPoint) throws AeriusException;
  }

}
