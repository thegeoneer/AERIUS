/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Test class for {@link SummaryCollector}.
 */
public class SummaryCollectorTest extends CalculationTestBase {

  private static final String FILENAME = "filename";
  private CalculationJob calculationJob;
  private SummaryCollector collector;

  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    calculationJob = getExampleCalculationJob();
    calculationJob.setUnitsAccumulator();
    collector = new SummaryCollector(profileFactory, calculationJob);
  }

  @Test
  public void testWritingSummaryFile() throws IOException {
    final JobCalculation jobCalculation = calculationJob.getCalculations().get(0);
    final int calculationId = jobCalculation.getCalculationId();

    collector.addSummary(calculationId, EmissionResultType.CONCENTRATION, FILENAME);
    jobCalculation.getSectors().forEach(s -> {
      final int sectorId = s.getSectorId();
      for (int i = 0; i < 100; i++) {
        collector.addResult(calculationId, sectorId, EmissionResultKey.NH3_CONCENTRATION, 10.0);
        collector.addResult(calculationId, sectorId, EmissionResultKey.NOX_CONCENTRATION, 5.0);
      }
    });
    collector.writeSummary(folder.getRoot().getAbsolutePath(), calculationJob.getName());
    final File summaryFile = new File(folder.getRoot(), calculationJob.getName() + ".summary");
    assertTrue("Summary file should exist", summaryFile.exists());
    final List<String> lines = Files.readAllLines(summaryFile.toPath());
    assertEquals("Summary file should have expected nr. of files", 7, lines.size());
    // Because lines are created based on Map variables the order can be arbitrary so we try to find the line with the nox value.
    final int noxRow = lines.get(1).contains("nox") ? 1 : 2;
    assertEquals("Content of a line should be as expected", "concentration;1800;nox;5.0;5.0;500.0;5.0;50;50000.0;0;0;" + FILENAME,
        lines.get(noxRow));
  }

  private Map<String, String> getExampleMetadataMap() {
    final Map<String, String> metadataMap = new HashMap<>();
    return metadataMap;
  }
}
