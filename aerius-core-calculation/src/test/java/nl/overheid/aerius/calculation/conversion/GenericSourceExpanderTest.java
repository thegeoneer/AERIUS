/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.postgis.LineString;
import org.postgis.LinearRing;
import org.postgis.Polygon;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link GenericSourceExpander}.
 */
public class GenericSourceExpanderTest extends BaseDBTest {

  private static final EmissionValueKey KEY_NH3 = new EmissionValueKey(Substance.NH3);

  private static SourceConverter geometryExpander;

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    geometryExpander = new OPSSourceConverter(getCalcPMF().getConnection());
  }

  @Test
  public void testLineToPoints() throws SQLException, AeriusException {
    final GenericEmissionSource emissionSource = new GenericEmissionSource(1, new Point());

    emissionSource.setGeometry(getExampleLineWKTGeometry());
    TestDomain.getGenericEmissionSource(emissionSource);

    final List<EmissionSource> sources = new ArrayList<>();
    sources.add(emissionSource);
    final List<EngineSource> pointSources = EngineSourceExpander.toEngineSources(getCalcConnection(), geometryExpander, sources, KEY_NH3.hatch());
    double calculatedEmission = 0d;
    for (final EngineSource es : pointSources) {
      calculatedEmission += es.getEmission(Substance.NH3);
    }
    assertEquals("Compare emission of # points:" + pointSources.size() + ", emission per point:" + pointSources.get(0).getEmission(Substance.NH3),
    emissionSource.getEmission(KEY_NH3), calculatedEmission, 0.0001d);
  }

  @Test
  public void testPolygonToPoints() throws SQLException, AeriusException {
    final EmissionSource emissionSource = TestDomain.getGenericEmissionSource();
    emissionSource.setId(1);
    emissionSource.setGeometry(getExamplePolygonWKTGeometry());

    final List<EmissionSource> sources = new ArrayList<>();
    sources.add(emissionSource);
    final List<EngineSource> pointSources = EngineSourceExpander.toEngineSources(getCalcConnection(), geometryExpander, sources, KEY_NH3.hatch());
    double calculatedEmission = 0d;
    for (final EngineSource es : pointSources) {
      calculatedEmission += es.getEmission(Substance.NH3);
    }
    assertEquals("Compare emission of # points:" + pointSources.size() + ", emission per point:" + pointSources.get(0).getEmission(Substance.NH3),
    emissionSource.getEmission(KEY_NH3), calculatedEmission, 0.0001d);
    pointSources.forEach(p -> assertTrue("Diameter should be set", ((OPSSource) p).getDiameter() > 0));
  }

  public static WKTGeometry getExampleLineWKTGeometry() {
    final ArrayList<org.postgis.Point> linePoints = new ArrayList<>();

    linePoints.add(new org.postgis.Point(166430, 471689));
    linePoints.add(new org.postgis.Point(167214, 464592));
    final LineString lineString = new LineString(linePoints.toArray(new org.postgis.Point[linePoints.size()]));
    return new WKTGeometry(lineString.toString(), (int) Math.round(lineString.length()));
  }

  private WKTGeometry getExamplePolygonWKTGeometry() {
    final ArrayList<org.postgis.Point> polygonPoints = new ArrayList<>();

    polygonPoints.add(new org.postgis.Point(166430, 471689));
    polygonPoints.add(new org.postgis.Point(167214, 464592));
    polygonPoints.add(new org.postgis.Point(167284, 469592));
    polygonPoints.add(new org.postgis.Point(166430, 471689));
    final ArrayList<LinearRing> linearRings = new ArrayList<>();
    linearRings.add(new LinearRing(polygonPoints.toArray(new org.postgis.Point[polygonPoints.size()])));
    final Polygon polygon = new Polygon(linearRings.toArray(new LinearRing[linearRings.size()]));
    return new WKTGeometry(polygon.toString(), 100);
  }
}
