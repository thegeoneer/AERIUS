/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * Test class for {@link TemporaryProjectResultsHandler}.
 */
public class TemporaryProjectResultsHandlerTest {
  private static final int TEMPORARY_YEARS = 1;

  @Test(expected = IllegalArgumentException.class)
  public void testInstantiation() {
    new TemporaryProjectResultsHandler(TEMPORARY_YEARS);
    fail("Instantiation should not succeed.");
  }
}
