/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Random;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link EmissionSourceListSTRTree}.
 */
public class EmissionSourceListSTRTreeTest {

  private static final String LINESTRING = "LINESTRING(1000 1000, 1000 2000)";
  private static final WKTGeometry LINE_GEOMETRY = new WKTGeometry(LINESTRING);
  private static final Random RANDOM = new Random();

  @Test
  public void testFindClosestDistancePoint() throws AeriusException {
    final GenericEmissionSource source = new GenericEmissionSource();
    source.setX(10000);
    source.setY(10000);
    source.setGeometry(new WKTGeometry(source.toWKT()));
    final EmissionSourceListSTRTree tree = creatTreeWithOneSource(source);
    final AeriusPoint point = new AeriusPoint(10000, 20000);
    assertEquals("Distance should be 20000", 10000, tree.findShortestDistance(point), 0);
  }

  @Test
  public void testFindClosestDistanceLine() throws AeriusException {
    final GenericEmissionSource source = new GenericEmissionSource();
    source.setGeometry(LINE_GEOMETRY);
    final EmissionSourceListSTRTree tree = creatTreeWithOneSource(source);
    final AeriusPoint point = new AeriusPoint(1500, 1500);
    assertEquals("Distance should be 500", 500, tree.findShortestDistance(point), 0);
  }

  @Test
  public void testFindClosestDistanceShippingRoute() throws AeriusException {
    final ShippingRoute route = new ShippingRoute();
    route.setGeometry(LINE_GEOMETRY);
    final EmissionSourceList list = new EmissionSourceList();
    list.getMaritimeRoutes().add(route);
    final EmissionSourceListSTRTree tree = createTree(list);
    final AeriusPoint point = new AeriusPoint(1600, 1600);
    assertEquals("Distance route should be 600", 600, tree.findShortestDistance(point), 0);
  }

  @Test
  public void testFindClosestDistanceMooringShipping() throws AeriusException {
    final InlandMooringEmissionSource source = new InlandMooringEmissionSource();
    final ShippingRoute route = new ShippingRoute();
    final WKTGeometry geometry = LINE_GEOMETRY;
    route.setGeometry(geometry);
    source.setGeometry(geometry);
    source.getInlandRoutes().add(route);
    final EmissionSourceListSTRTree tree = creatTreeWithOneSource(source);
    final AeriusPoint point = new AeriusPoint(1700, 1700);
    assertEquals("Distance route should be 700", 700, tree.findShortestDistance(point), 0);
  }

  @Test
  public void testFindClosestDistanceSRM2Network() throws AeriusException {
    final SRM2NetworkEmissionSource network = new SRM2NetworkEmissionSource();
    final GenericEmissionSource source = new GenericEmissionSource();
    source.setGeometry(LINE_GEOMETRY);
    network.getEmissionSources().add(source);
    final EmissionSourceListSTRTree tree = creatTreeWithOneSource(network);
    final AeriusPoint point = new AeriusPoint(1800, 1800);
    assertEquals("Distance route should be 800", 800, tree.findShortestDistance(point), 0);
  }

  @Test
  public void testFindClosestDistancePolygon() throws AeriusException {
    final GenericEmissionSource source = new GenericEmissionSource();
    source.setGeometry(new WKTGeometry("POLYGON((100 100, 100 200, 200 200, 200 100, 100 100))"));
    final EmissionSourceListSTRTree tree = creatTreeWithOneSource(source);
    final AeriusPoint point1 = new AeriusPoint(50, 150);
    assertEquals("Distance outside polygon should be 50", 50, tree.findShortestDistance(point1), 0);
    final AeriusPoint point2 = new AeriusPoint(150, 150);
    assertEquals("Distance inside polygon should be 0", 0, tree.findShortestDistance(point2), 0);
  }

  @Test
  public void testFindClosestDistanceRandomPoints() throws AeriusException {
    final int numberOfReceptors = 100;
    final int range = 1000;
    final int x1 = 10000;
    final int y1 = 10000;
    final EmissionSourceList list = createPoints(numberOfReceptors, range, x1, y1);
    final EmissionSourceListSTRTree tree = createTree(list);
    final AeriusPoint point = new AeriusPoint(x1, y1);
    assertNotEquals("Distance should be 0", 0, tree.findShortestDistance(point), 0.1);
  }

  private EmissionSourceListSTRTree creatTreeWithOneSource(final EmissionSource source) throws AeriusException {
    final EmissionSourceList list = new EmissionSourceList();
    list.add(source);
    return createTree(list);
  }

  private EmissionSourceListSTRTree createTree(final EmissionSourceList list) throws AeriusException {
    final EmissionSourceListSTRTree tree = new EmissionSourceListSTRTree();
    tree.add(list);
    tree.build();
    return tree;
  }

  private EmissionSourceList createPoints(final int count, final int maxRange, final int offsetX, final int offsetY) {
    final EmissionSourceList esl = new EmissionSourceList();
    for (int i = 0; i < count; i++) {
      final GenericEmissionSource source = new GenericEmissionSource();
      source.setX(offsetX - (offsetX / 2) + RANDOM.nextInt(maxRange));
      source.setY(offsetY - (offsetY / 2) + RANDOM.nextInt(maxRange));
      source.setGeometry(new WKTGeometry(source.toWKT()));
      esl.add(source);
    }
    return esl;
  }
}
