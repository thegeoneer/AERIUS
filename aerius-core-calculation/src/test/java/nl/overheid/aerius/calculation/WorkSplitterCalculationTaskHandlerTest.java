/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.internal.ArrayComparisonFailure;

import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test class for {@link WorkSplitterCalculationTaskHandler}.
 */
public class WorkSplitterCalculationTaskHandlerTest extends BaseDBTest {

  private static final int MAX_OPS_UNITS = 50;

  private static final WorkKey WORK_KEY = new WorkKey("", "1", 1, 1);

  private List<Integer> counter;
  private WorkSplitterCalculationTaskHandler handler;

  @Before
  public void before() {
    counter = new ArrayList<>();
    setHandler(1);
  }

  private void setHandler(final int minReceptors) {
    final CalculationTaskHandler wrappedHandler = new CalculationTaskHandler() {
      @Override
      public <E extends EngineSource, T extends CalculationTask<E, ?, ?>> void work(final WorkKey workKey, final T task,
          final Collection<AeriusPoint> points) throws AeriusException, InterruptedException, TaskCancelledException {
        counter.add(points.size());
      }
    };
    handler = new WorkSplitterCalculationTaskHandler(wrappedHandler, MAX_OPS_UNITS, minReceptors);
  }

  @Test
  public void testSplitLess() throws AeriusException, InterruptedException, TaskCancelledException {
    assertSplitter(9, 10, 2, 5, 4);
  }

  @Test
  public void testSplitExact1() throws AeriusException, InterruptedException, TaskCancelledException {
    assertSplitter(8, 10, 2, 4, 4);
  }

  @Test
  public void testSplitExact2() throws AeriusException, InterruptedException, TaskCancelledException {
    assertSplitter(10, 10, 2, 5, 5);
  }

  @Test
  public void testSplitMore() throws AeriusException, InterruptedException, TaskCancelledException {
    assertSplitter(11, 10, 3, 4, 4, 3);
  }

  @Test
  public void testSplitLotsSources() throws AeriusException, InterruptedException, TaskCancelledException {
    assertSplitter(10, 30, 5, 2, 2, 2, 2, 2);
  }

  @Test
  public void testSplitOverTheTopSources() throws AeriusException, InterruptedException, TaskCancelledException {
    assertSplitter(2356710, 168748, 2356710);
  }

  @Test
  public void testSplitOverTheTopSourcesWithMin() throws AeriusException, InterruptedException, TaskCancelledException {
    setHandler(2);
    assertSplitter(10, 100, 5, 2, 2, 2, 2, 2);
  }

  private void assertSplitter(final int numberOfPoints, final int nrOfSources, final int size, final Integer... expected)
      throws AeriusException, InterruptedException, TaskCancelledException, ArrayComparisonFailure {
    handler.work(WORK_KEY, createSources(nrOfSources), createReceptorPoints(numberOfPoints));
    assertEquals("We should have " + size + " task", size, counter.size());
    if (size < 1000) { // Ignore the really large over the top input
      assertArrayEquals("Size of task should be same as input", expected, counter.toArray());
    }
  }

  private CalculationTask<OPSSource, OPSReceptor, OPSInputData> createSources(final int numberOfSources) throws AeriusException {
    final CalculationTask<OPSSource, OPSReceptor, OPSInputData> task = new CalculationTask<>(WorkerType.OPS, new OPSInputData("1", 1));
    final Collection<OPSSource> sources = new ArrayList<>();
    for (int i = 0; i < numberOfSources; i++) {
      sources.add(new OPSSource());
    }
    task.getTaskInput().setEmissionSources(sources);
    return task;
  }

  private List<AeriusPoint> createReceptorPoints(final int numberOfPoints) {
    final List<AeriusPoint> list = new ArrayList<>();
    for (int i = 0; i < numberOfPoints; i++) {
      list.add(new AeriusPoint(i));
    }
    return list;
  }
}
