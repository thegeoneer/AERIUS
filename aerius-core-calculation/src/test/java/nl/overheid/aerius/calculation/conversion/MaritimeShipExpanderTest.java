/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.ExpanderVisitor.SourceCollector;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link MaritimeShipExpander}
 */
public class MaritimeShipExpanderTest extends BaseDBTest {

  private static SourceConverter geometryExpander;
  private static TestDomain testDomain;

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    testDomain = new TestDomain(getCalcPMF());
    geometryExpander = new OPSSourceConverter(getCalcPMF().getConnection());
  }

  @Test
  public void testMaritimeShipsToPoints() throws SQLException, AeriusException {
    final int dockPointSources = 286;
    final MaritimeMooringEmissionSource emissionSource = new MaritimeMooringEmissionSource();
    emissionSource.setId(1);
    final EmissionValueKey evk = TestDomain.EVK_WITH_YEAR_NOX;

    emissionSource.setGeometry(GenericSourceExpanderTest.getExampleLineWKTGeometry());
    testDomain.getMooringMaritimeShipEmissionSource(emissionSource);
    //ensure the 2nd boat has a maneuver part on the inland route.
    final MaritimeShippingCategory category = new MaritimeShippingCategory();
    //category with code "OO10000", maritime category ID 5, has a maneuver_factor != 1.
    category.setId(5);
    category.setCode("OO10000");
    emissionSource.getEmissionSubSources().get(1).setCategory(category);
    final List<EngineSource> pointSources = new ArrayList<>();
    final SourceCollector sourceCollector = (expandedSources, es) -> pointSources.addAll(expandedSources);
    final ExpanderVisitor visitor = new ExpanderVisitor(getCalcConnection(), geometryExpander, evk.hatch(), sourceCollector);
    emissionSource.accept(visitor);
    assertTrue("Number of points should be over 572 (dock = 286, 2 types of ships)", pointSources.size() > dockPointSources * 2);
    int pointNr = 0;
    final OPSSource engineSource0 = (OPSSource) pointSources.get(0);
    assertNotEquals("Characteristics shouldn't have 0 heat content", 0.0, engineSource0.getHeatContent(), 1E-3);
    assertNotEquals("Characteristics shouldn't have 0 height", 0.0, engineSource0.getEmissionHeight(), 1E-3);
    assertNotEquals("Characteristics shouldn't have 0 spread", 0.0, engineSource0.getSpread(), 1E-3);
    boolean puntertje = false;
    boolean motorboot = false;
    for (final EngineSource s : pointSources) {
      final OPSSource es = (OPSSource) s;
      final double emission = es.getEmission(evk.getSubstance());
      final double x = es.getPoint().getX();
      final double y = es.getPoint().getY();

      if ((int) emission == 80965) {
        puntertje = true;
      } else if ((int) emission == 5552) {
        motorboot = true;
      }
      assertNotEquals("X coordinate of (point:" + pointNr + ", xy:" + x + " " + y + ", emission of point:" + emission + "):", 0, x, 0.0001d);
      assertNotEquals("Y coordinate of (point:" + pointNr + ", xy:" + x + " " + y + ", emission of point:" + emission + "):", 0, y, 0.0001d);
      assertNotEquals("emission of (point:" + pointNr + ", xy:" + x + " " + y + ", emission of point:" + emission + "):", 0, emission, 0.0001d);
      pointNr++;
    }
    assertTrue("Puntertje found", puntertje);
    assertTrue("Motorboot found", motorboot);
  }
}
