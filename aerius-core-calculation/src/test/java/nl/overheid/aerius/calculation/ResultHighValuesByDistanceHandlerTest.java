/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTreeTestUtil;
import nl.overheid.aerius.shared.domain.calculation.ResultHighValuesByDistances;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test for {@link ResultHighValuesByDistanceHandler}.
 */
public class ResultHighValuesByDistanceHandlerTest {

  private static final int CALCULATION_ID = 1;

  private EmissionSourceListSTRTree tree;

  @Before
  public void before() throws AeriusException {
    tree = buildTreeWithSource();
  }

  @Test
  public void testWithNoFilter() throws AeriusException {
    final ResultHighValuesByDistanceHandler handler = new ResultHighValuesByDistanceHandler(tree, new IncludeAllResultsFilter());
    final ResultHighValuesByDistances result = addAndGetResults(handler);
    assertEquals("Higest distance with no filter", 3, result.getHighestDistance(CALCULATION_ID));
    assertEquals("Size of distances with no filter", 2, result.getCalculationIdMap(CALCULATION_ID).size());
    assertEquals("Size of value for distance 2 with no filter", 1, result.getCalculationIdMap(CALCULATION_ID).get(2).entrySet().size());
    assertEquals("Size of value for distance 3 with no filter", 1, result.getCalculationIdMap(CALCULATION_ID).get(3).entrySet().size());
  }

  @Test
  public void testWithFilter() throws AeriusException {
    final ResultHighValuesByDistanceHandler handler = new ResultHighValuesByDistanceHandler(tree,
        r -> r.getEmissionResult(IntermediateResultHandlerUtil.KEY) > 1.0);
    final ResultHighValuesByDistances result = addAndGetResults(handler);
    assertEquals("Higest distance with filter", 2, result.getHighestDistance(CALCULATION_ID));
    assertEquals("Size of distances with filter", 1, result.getCalculationIdMap(CALCULATION_ID).size());
    assertEquals("Size of value for distance 2 with filter", 1, result.getCalculationIdMap(CALCULATION_ID).get(2).entrySet().size());
    assertNull("Size of value for distance 3 should be empty with filter", result.getCalculationIdMap(CALCULATION_ID).get(3));
  }

  private ResultHighValuesByDistances addAndGetResults(final ResultHighValuesByDistanceHandler handler) throws AeriusException {
    handler.onTotalResults(IntermediateResultHandlerUtil.createResults(), CALCULATION_ID);
    final ResultHighValuesByDistances result = (ResultHighValuesByDistances) handler.get();
    return result;
  }

  private EmissionSourceListSTRTree buildTreeWithSource() throws AeriusException {
    final EmissionSourceList esl = new EmissionSourceList();
    EmissionSourceListSTRTreeTestUtil.addGenericSource(esl, 10000, 10000);
    return EmissionSourceListSTRTreeTestUtil.buildTree(esl);
  }
}
