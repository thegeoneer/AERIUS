/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.lang.Thread.State;
import java.util.concurrent.Semaphore;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;

/**
 * Test class for {@link WorkTrackerImpl}.
 */
public class WorkTrackerImplTest {

  private WorkTracker workTracker;
  private Semaphore startSync;
  private Semaphore released;
  private CalculationState finishState;

  @Before
  public void before() {
    startSync = new Semaphore(0);
    released = new Semaphore(0);
    workTracker = new WorkTrackerImpl();
  }

  /**
   * Test normal flow.
   */
  @Test(timeout = 1000)
  public void testWorkFinished() throws InterruptedException {
    final Thread thread = startNewThread();
    while (thread.getState() != State.WAITING) {}
    assertEquals("Still waiting to finish", 0, released.availablePermits());
    workTracker.decrement();
    released.acquire();
    assertEquals("Should be finished", 0, released.availablePermits());
    assertSame("Should have COMPLETED state", CalculationState.COMPLETED, finishState);
  }

  /**
   * Tests if correctly releases waitForFinished() when all tasks finish before waitForFinished is called.
   */
  @Test(timeout = 1000)
  public void testWorkFinishedBeforeWait() throws InterruptedException {
    createNewThread().start();
    released.acquire();
    assertEquals("Still waiting to finish", 0, released.availablePermits());
    assertSame("Should have COMPLETED state", CalculationState.COMPLETED, finishState);
  }

  /**
   * Tests if correctly releases waitForFinished() when all tasks finish before waitForFinished is called.
   */
  @Test(timeout = 1000)
  public void testWorkFinishedBeforeWait2() throws InterruptedException {
    workTracker.increment();
    final Thread thread = startNewThread();
    while(thread.getState() != State.WAITING) {}
    workTracker.decrement();
    assertTrue("Should still be waiting", ((WorkTrackerImpl) workTracker).isWaiting());
    assertEquals("Still waiting to finish", 0, released.availablePermits());
    workTracker.decrement();
    released.acquire();
    assertEquals("Should be finished", 0, released.availablePermits());
    assertSame("Should have COMPLETED state", CalculationState.COMPLETED, finishState);
  }

  /**
   * Test when exception is given before waitForFinished() is called.
   */
  @Test(timeout = 1000)
  public void testExceptionBeforeWait() throws InterruptedException {
    final IllegalArgumentException theException = new IllegalArgumentException("");
    workTracker.cancel(theException);
    createNewThread().start();
    assertExceptionOnRelease(theException);
  }

  /**
   * Test when exception is given after waitForFinished() is called.
   */
  @Test(timeout = 1000)
  public void testException() throws InterruptedException {
    startNewThread();
    assertEquals("Still waiting to finish", 0, released.availablePermits());
    final IllegalArgumentException theException = new IllegalArgumentException("1");
    workTracker.cancel(theException);
    workTracker.cancel(new TaskCancelledException(theException)); // this exception should not be reported.
    assertExceptionOnRelease(theException);
  }

  /**
   * Tests if waitForFinish is correctly handled when after an exception an onCompleted is called.
   */
  @Test(timeout = 1000)
  public void testUpdateAfterException() throws InterruptedException {
    startNewThread();
    final IllegalArgumentException theException = new IllegalArgumentException("");
    workTracker.cancel(theException);
    workTracker.decrement();
    assertExceptionOnRelease(theException);
  }

  private Thread startNewThread() throws InterruptedException {
    workTracker.increment();
    final Thread thread = createNewThread();
    thread.start();
    startSync.acquire();
    assertEquals("Nothing done should not finished", 0, released.availablePermits());
    return thread;
  }

  private Thread createNewThread() {
    return new Thread(new Runnable() {

   @Override
      public void run() {
        try {
          startSync.release();
          finishState = workTracker.waitForCompletion();
          released.release();
        } catch (final InterruptedException e) {
        }
      }
    });
  }

  private void assertExceptionOnRelease(final IllegalArgumentException theException) throws InterruptedException {
    released.acquire();
    assertEquals("Should finish on Exception ", 0, released.availablePermits());
    assertSame("Should have CANCELLED state", CalculationState.CANCELLED, finishState);
    assertSame("Should have exception", theException, workTracker.getException());
  }
}
