/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.calculation.CalculationTestBase;
import nl.overheid.aerius.calculation.Calculator;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculatorProfileFactory;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;
import nl.overheid.aerius.shared.domain.calculation.ResearchAreaCalculationScenario;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.MockChannel;
import nl.overheid.aerius.taskmanager.client.MockConnection;
import nl.overheid.aerius.taskmanager.client.configuration.ConnectionConfigurationBean;
import nl.overheid.aerius.taskmanager.client.util.QueueHelper;

/**
 * Test class for {@link ResearchAreaWorkDistributor}.
 */
public class ResearchAreaWorkDistributorTest extends CalculationTestBase {

  private static final String DUMMY = "DUMMY";

  private static final int XCOORD_1 = 203312;
  private static final int YCOORD_1 = 513541;

  private static final int RADIUS = 1000;
  private static final int POINTS_IN_RADIUS_OF_SOURCE = 4; // 3 in area 36, 1 in area 37

  private static final List<AeriusPoint> POINTS_AREA_36 = Arrays.asList(new AeriusPoint[] {
      new OPSReceptor(1, 203219, 513594), // one hexagon away
      new OPSReceptor(2, 202940, 513541), // 372 meters away
      new OPSReceptor(3, 202381, 513541), // 931 meters away
      new OPSReceptor(4, 202195, 513541) // 1117 meters away
  });

  private static final List<AeriusPoint> POINTS_AREA_37 = Arrays.asList(new AeriusPoint[] {
      new OPSReceptor(5, 204242, 513541), // 930 meters away
      new OPSReceptor(6, 204429, 513541), // 1117 meters away
      new OPSReceptor(7, 204615, 541078), // 1,5+ km away
      new OPSReceptor(8, 205359, 514293) // 2+ km away
  });

  private CalculatorBuildDirector director;

  @Before
  @Override
  public void setUp() throws Exception {
    super.setUp();
    final PASCalculatorFactory cf = new PASCalculatorFactory(getCalcPMF()) {
      @Override
      protected Natura2kGridPointStore loadNatura2kGridPointStore(final PMF pmf, final ReceptorUtil receptorUtil) throws SQLException {
        final GridSettings gs = new GridSettings(RECEPTOR_GRID_SETTINGS);
        return getPointStore(gs);
      }
    };
    final BrokerConnectionFactory factory = new BrokerConnectionFactory(EXECUTOR, new ConnectionConfigurationBean(DUMMY, 0, DUMMY, DUMMY)) {
      @Override
      protected com.rabbitmq.client.Connection createNewConnection() throws IOException {
        return new MockConnection() {
          @Override
          public Channel createChannel() throws IOException {
            return new MockChannel() {
              @Override
              protected byte[] mockResults(final BasicProperties properties, final byte[] body) throws Exception {
                final OPSInputData input = (OPSInputData) QueueHelper.bytesToObject(body);
                final CalculationResult result = new CalculationResult();
                final List<AeriusResultPoint> results = input.getReceptors().stream()
                    .map(x -> new AeriusResultPoint(x.getX(), x.getY())).collect(Collectors.toList());
                result.setResults(new ArrayList<>(results));
                return QueueHelper.objectToBytes(result);

              };
            };
          };
        };
      }
    };
    director = new CalculatorBuildDirector(getCalcPMF(), factory, true) {
      @Override
      protected CalculatorOptions getCalculatorOptions(final Connection con, final boolean calculatorOptionsUI) throws SQLException {
        final CalculatorOptions options = super.getCalculatorOptions(con, calculatorOptionsUI);
        options.setMaxConcurrentChunks(1);
        return options;
      }

      @Override
      protected CalculatorProfileFactory getCalculatorProfileFactory(final CalculationSetOptions calculationSetOptions) {
        return cf;
      }
    };
    director.shutdown();
  }

  @After
  public void after() {
    director.shutdown();
  }

  @Test //(timeout = 60000)
  @Ignore // radius not allowed at this time.. Disable test
  public void testDistribute() throws Exception {
    assertTest(RADIUS);
  }

  @Test(expected = AeriusException.class)
  public void testDistributeWithoutRadius() throws Exception {
    assertTest(null);
  }

  private void assertTest(final Integer radius) throws Exception {
    final AtomicInteger counter = new AtomicInteger();

    final Calculator calculator = createExampleResearchAreaCalculator(radius);
    calculator.addCalculationResultHandler(new CalculationResultHandler() {
      @Override
      public void onTotalResults(final List<AeriusResultPoint> result, final int calculationId) throws AeriusException {
        counter.addAndGet(result.size());
      }
    });
    calculator.calculate();
    assertEquals("The amount of points that are calculated differs from the expected amount", POINTS_IN_RADIUS_OF_SOURCE, counter.get());
  }

  private Calculator createExampleResearchAreaCalculator(final Integer radius) throws SQLException, AeriusException {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setScenario(getExampleResearchScenario(radius));
    inputData.setQueueName(DUMMY);
    return director.construct(inputData, "1", null);
  }

  private ResearchAreaCalculationScenario getExampleResearchScenario(final Integer radius) {
    final ResearchAreaCalculationScenario researchScenario = new ResearchAreaCalculationScenario();
    researchScenario.setOptions(getCalculationSetOptions(radius));
    researchScenario.setYear(TestDomain.YEAR);
    final EmissionSourceList researchSources = new EmissionSourceList();
    final Point point1 = new Point(XCOORD_1, YCOORD_1);
    researchSources.add(TestDomain.getSource(1, new WKTGeometry(point1.toWKT(), 1), "Herderinnetje", TestDomain.getGenericEmissionSource()));
    researchScenario.setResearchAreaSources(researchSources);
    researchScenario.setSources(researchSources);
    return researchScenario;
  }

  private CalculationSetOptions getCalculationSetOptions(final Integer radius) {
    final CalculationSetOptions options = new CalculationSetOptions();
    options.setCalculationType(CalculationType.PAS);
    options.getSubstances().add(Substance.NH3);
    options.getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
    if (radius != null) {
      options.setPermitCalculationRadiusType(new PermitCalculationRadiusType());
      options.getPermitCalculationRadiusType().setRadius(radius);
    }
    return options;
  }

  private Natura2kGridPointStore getPointStore(final GridSettings gs) {
    final Natura2kGridPointStore pointStore = new Natura2kGridPointStore(RECEPTOR_UTIL, gs);

    pointStore.addAll(36, POINTS_AREA_36);
    pointStore.addAll(37, POINTS_AREA_37);

    pointStore.sortPointStore();

    return pointStore;
  }
}