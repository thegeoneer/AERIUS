/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;

import nl.overheid.aerius.db.common.CalculationInfoRepository;
import nl.overheid.aerius.db.common.CalculationRepositoryTestBase;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Test class for {@link TotalResultDBUnsafeHandler}.
 */
public class TotalResultDBUnsafeHandlerTest extends CalculationRepositoryTestBase {

  @Test
  public void testOnSuccess() throws Exception {
    final PartialCalculationResult cr = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    final TotalResultDBUnsafeHandler handler = new TotalResultDBUnsafeHandler(getCalcPMF(), null);
    init(cr, handler);
    assertEquals("Should have inserted results", cr.getResults().size(),
        CalculationInfoRepository.getCalculationResults(getCalcConnection(), calculation.getCalculationId()).getResults().size());
  }

  public TotalResultDBUnsafeHandler init(final PartialCalculationResult cr, final TotalResultDBUnsafeHandler handler) throws Exception {
    handler.onTotalResults(cr.getResults(), calculation.getCalculationId());
    return handler;
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }
}
