/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTreeTestUtil;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link RadiusFilteredWorkHandler}.
 */
public class RadiusFilteredWorkHandlerTest {

  final AtomicInteger insideCount = new AtomicInteger();
  final AtomicInteger outsideCount = new AtomicInteger();
  final WorkHandler<AeriusPoint> in = (j, cj, id, s) -> insideCount.incrementAndGet();
  final WorkHandler<AeriusPoint> out = (j, cj, id, s) -> outsideCount.incrementAndGet();

  @Before
  public void setUp() {
    insideCount.set(0);
    outsideCount.set(0);
  }

  @Test
  public void testNoFilter() throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final EmissionSourceList esl = new EmissionSourceList();
    EmissionSourceListSTRTreeTestUtil.addGenericSource(esl, 100, 100);
    final CalculationJob createJob = createJob(esl);
    final RadiusFilteredWorkHandler handler = new RadiusFilteredWorkHandler(in, out, createJob.getTree(), 5000);

    handler.work(null, createJob, 1, Collections.singletonList(new AeriusPoint(200, 200)));
    assertEquals("point within 5km needs calculation", 1, insideCount.get());
    handler.work(null, createJob, 1, Collections.singletonList(new AeriusPoint(100, 5101)));
    assertEquals("point outside 5km needs calculation", 1, outsideCount.get());
  }

  @Test
  public void testShippingFilter() throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final EmissionSourceList esl = new EmissionSourceList();
    EmissionSourceListSTRTreeTestUtil.addGenericSource(esl, 100, 100);
    final CalculationJob createJob = createJob(esl);
    final RadiusFilteredWorkHandler handler = new RadiusFilteredWorkHandler(in, out, createJob.getTree(), 5000);

    handler.work(null, createJob, 1, Collections.singletonList(new AeriusPoint(200, 200)));
    assertEquals("point within 5km needs calculation", 1, insideCount.get());
    handler.work(null, createJob, 1, Collections.singletonList(new AeriusPoint(100, 5101)));
    assertEquals("point outside 5km needs calculation", 1, outsideCount.get());
  }

  @Test
  public void testRoadFilter() throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final EmissionSourceList esl = new EmissionSourceList();
    EmissionSourceListSTRTreeTestUtil.addGenericSource(esl, 100, 100);
    final CalculationJob createJob = createJob(esl);
    final RadiusFilteredWorkHandler handler = new RadiusFilteredWorkHandler(in, out, createJob.getTree(), 3000);

    handler.work(null, createJob, 1, Collections.singletonList(new AeriusPoint(200, 200)));
    assertEquals("point within 5km needs calculation", 1, insideCount.get());
    handler.work(null, createJob, 1, Collections.singletonList(new AeriusPoint(100, 3101)));
    assertEquals("point outside 5km needs calculation", 1, outsideCount.get());

  }

  private CalculationJob createJob(final EmissionSourceList esl) throws AeriusException {
    final CalculatedScenario cs = new CalculatedSingle();
    cs.getScenario().getSourceLists().add(esl);
    final CalculationJob calculationJob = new CalculationJob(null, cs, "");
    return calculationJob;
  }
}
