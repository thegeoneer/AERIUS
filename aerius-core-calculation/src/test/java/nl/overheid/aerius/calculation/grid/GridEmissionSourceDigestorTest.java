/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.calculation.base.EngineSourceAggregator;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.profile.pas.PASGeometryExpander;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Test class for {@link GridEmissionSourceDigestor}.
 */
public class GridEmissionSourceDigestorTest extends BaseDBTest {

  private static final int XCOORD = 114051;
  private static final int YCOORD = 395806;
  private static final String LINESTRING_PREFIX = "LINESTRING(";

  private final GridUtil gridUtil = new GridUtil(new GridSettings(RECEPTOR_GRID_SETTINGS));
  private final AggregationDistanceProfilePicker aggregationProfilePicker = new AggregationDistanceProfilePicker();
  private final EngineSourceAggregator<EngineSource, Substance> aggregator =
      new EngineSourceAggregator<>(new GridEngineSourceAggregatable(RDNew.SRID));

  private GridEmissionSourceDigestor sourceDigestor;

  @Before
  public void before() {
    sourceDigestor = new GridEmissionSourceDigestor(getCalcPMF(), gridUtil, aggregator, aggregationProfilePicker);
  }

  @Test
  public void testDigest() throws SQLException, AeriusException {
    final CalculationJob job = createCalculationJob();
    final List<EmissionValueKey> keys = createEmissionValueKeyList();

    sourceDigestor.digest(new PASGeometryExpander(getCalcConnection(), new DefaultCalculationEngineProvider()), job, keys);
    final int cell0 = getCellIdFromSource(job, 0);
    final Map<Sector, Collection<EngineSource>> sourceGridIds = job.getCalculations().get(0).getSourcesFor(cell0);
    assertEquals("Should contain 2 sectors", 2, sourceGridIds.size());
    int count = 0;
    for (final Entry<Sector, Collection<EngineSource>> entry : sourceGridIds.entrySet()) {
      count += entry.getValue().size();
    }
    assertEquals("Should contain 3 engine sources", 3, count);
  }

  private int getCellIdFromSource(final CalculationJob job, final int idx) {
    return gridUtil.getCellFromPosition(job.getCalculatedScenario().getSources(1).get(idx), gridUtil.getGridSettings().getZoomLevelMax());
  }

  private List<EmissionValueKey> createEmissionValueKeyList() {
    final List<EmissionValueKey> keys = new ArrayList<>();
    keys.add(new EmissionValueKey(Substance.NH3));
    keys.add(new EmissionValueKey(Substance.NOX));
    keys.add(new EmissionValueKey(Substance.NO2));
    return keys;
  }

  private CalculationJob createCalculationJob() throws SQLException, AeriusException {
    final CalculatedSingle cs = new CalculatedSingle();
    final TestDomain testDomain = new TestDomain(getCalcPMF());
    final EmissionSourceList sources = new EmissionSourceList();
    sources.add(getSource(1, new WKTGeometry("POINT(" + XCOORD + " " + YCOORD + ")", 1), "SomeFarmSource", testDomain.getFarmEmissionSource()));
    sources.add(getSource(2, someLineString(), "SomeRoadSource", testDomain.getSRM2EmissionSource()));
    cs.setSources(sources);
    cs.setCalculationId(1);
    final CalculationSetOptions options = new CalculationSetOptions();
    options.getSubstances().add(Substance.NH3);
    options.getSubstances().add(Substance.NOX);
    options.getSubstances().add(Substance.NO2);
    final Collection<Sector> sectors = new HashSet<>();
    for (final EmissionSource emissionSource : sources) {
      sectors.add(emissionSource.getSector());
    }
    final JobCalculation jc = new JobCalculation(1, sectors);
    cs.setOptions(options);
    final CalculationJob cj = new CalculationJob(UUID.randomUUID(), cs, "queue");
    cj.setProvider(new DefaultCalculationEngineProvider());
    cj.getCalculations().add(jc);
    return cj;
  }

  private WKTGeometry someLineString() {
    final int yCoord1 = YCOORD + 100;
    final WKTGeometry geometrySource3 = new WKTGeometry(LINESTRING_PREFIX
        + XCOORD + " " + YCOORD + ","
        + (XCOORD + 100) + " " + yCoord1 + ","
        + XCOORD + " " + YCOORD + ")", 5000);
    return geometrySource3;
  }


  private <E extends EmissionSource> E getSource(final int id, final WKTGeometry geometry, final String label, final E source)
      throws AeriusException {
    source.setId(id);
    source.setGeometry(geometry);
    source.setLabel(label);
    final Point midPoint = GeometryUtil.middleOfGeometry(geometry);
    source.setX(midPoint.getX());
    source.setY(midPoint.getY());
    return source;
  }
}
