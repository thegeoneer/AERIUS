/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.EngineSourceAggregator;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.domain.OPSSource.OPSSourceOutflowDirection;
import nl.overheid.aerius.ops.io.BrnConstants;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Test class for {@link GridEngineSourceAggregatable}. It uses {@link EngineSourceAggregator} as the entry point for class to the test.
 */
public class GridEngineSourceAggregatableTest {
  private static final List<Substance> SUBSTANCES = Substance.NOXNH3.hatch();
  // Aggregable heat content value
  private static final double HC_A = 0.2;
  // Non aggregable heat content value
  private static final double HC_NA = 0.5;
  // Aggregable height value
  private static final double H_A = 20;
  // Non aggregable height value
  private static final double H_NA = 25;
  // Diameter
  private static final int DIA = 10;
  // Diurnal variation value 1
  private static final int DV_1 = 1;
  // Spread value
  private static final double SPRD = 2.5;


  private static final Logger LOG = LoggerFactory.getLogger(GridEngineSourceAggregatableTest.class);

  private final EngineSourceAggregator<EngineSource, Substance> aggregator =
      new EngineSourceAggregator<>(new GridEngineSourceAggregatable(RDNew.SRID));

  @Test
  public void testSingleSource() {
    final ArrayList<EngineSource> sources = new ArrayList<>();
    final OPSSource src1 = createSource(1, 50, 150, HC_NA, H_NA, DIA, DV_1, SPRD, 500, 0);
    sources.add(src1);

    final Collection<EngineSource> aggregatedSources = aggregator.aggregate(sources, SUBSTANCES);
    final OPSSource aggregated = (OPSSource) aggregatedSources.stream().findFirst().get();

    Assert.assertSame("Set must contain third source identically", src1, aggregated);
    Assert.assertEquals("Height should be 25", 25, aggregated.getEmissionHeight(), 0);
    Assert.assertEquals("x should be 50", 50, aggregated.getPoint().getX(), 0);
    Assert.assertEquals("x should be 150", 150, aggregated.getPoint().getY(), 0);
  }

  @Test
  public void testAggregatorHeatContent() {
    final ArrayList<EngineSource> sources = new ArrayList<>();

    sources.add(createSource(1, 50, 150, HC_A, H_A, DIA, DV_1, SPRD, 500, 0));
    sources.add(createSource(2, 90, 190, HC_A, H_A, DIA, DV_1, SPRD, 1000, 0));
    final OPSSource src3 = createSource(3, 111, 111, HC_A, H_NA, DIA, DV_1, SPRD, 1000, 0);
    sources.add(src3);

    final Collection<EngineSource> aggregatedSources = aggregator.aggregate(sources, SUBSTANCES);

    Assert.assertEquals("There should be 2 aggregated sources", 2, aggregatedSources.size());
    Assert.assertTrue("Set must contain third source identically", aggregatedSources.contains(src3));
  }

  @Test
  public void testDuplicateSubstance() {
    final ArrayList<EngineSource> sources = new ArrayList<>();

    sources.add(createSource(1, 50, 150, HC_A, H_A, DIA, DV_1, SPRD, 500, 0));
    sources.add(createSource(2, 50, 150, HC_A, H_A, DIA, DV_1, SPRD, 500, 500));

    final Collection<EngineSource> aggregatedSources = aggregator.aggregate(sources, SUBSTANCES);
    Assert.assertEquals("There should be 2 aggregated sources", 2, aggregatedSources.size());
    final double totalNH3 = aggregatedSources.stream().mapToDouble(es -> es.getEmission(Substance.NH3)).sum();
    final double totalNOx = aggregatedSources.stream().mapToDouble(es -> es.getEmission(Substance.NOX)).sum();

    Assert.assertEquals("Sum NH3 should be the same", 500, totalNH3, 0.001);
    Assert.assertEquals("Sum NOx should be the same", 1000, totalNOx, 0.001);
  }

  @Test
  public void testAggregatorLargeSet() {
    final OPSSource src1 = createSource(1, 50, 150, HC_A, H_A, DIA, DV_1, SPRD, 500, 0);
    final OPSSource src2 = createSource(2, 90, 190, HC_A, H_A, DIA, DV_1, SPRD, 1000, 0);
    final OPSSource src3 = createSource(3, 111, 111, HC_NA, H_NA, DIA, DV_1, SPRD, 1000, 0);
    final OPSSource src4 = createSource(4, 222, 222, HC_NA, H_NA, DIA, DV_1, SPRD, 1000, 0);
    final OPSSource src5 = createSource(5, 222, 222, HC_NA, H_NA, DIA, DV_1, SPRD, 1000, 0);
    final OPSSource src6 = createSource(6, 50, 150, HC_A, H_A, DIA, DV_1, SPRD, 500, 0);
    final ArrayList<EngineSource> sources = new ArrayList<>();

    sources.add(src1);
    sources.add(src2);
    sources.add(src3);
    sources.add(src4);
    sources.add(src5);
    sources.add(src6);

    final Collection<EngineSource> aggregatedSources = aggregator.aggregate(sources, SUBSTANCES);

    Assert.assertEquals("There should be 4 aggregated sources", 4, aggregatedSources.size());

    Assert.assertFalse("Set must NOT contain first source identically", aggregatedSources.contains(src1));
    Assert.assertFalse("Set must NOT contain second source identically", aggregatedSources.contains(src2));
    Assert.assertTrue("Set must contain third source identically", aggregatedSources.contains(src3));
    Assert.assertTrue("Set must contain fourth source identically", aggregatedSources.contains(src4));
    Assert.assertTrue("Set must contain fifth source identically", aggregatedSources.contains(src5));
    Assert.assertFalse("Set must NOT contain sixth source identically", aggregatedSources.contains(src6));
  }

  /**
   * Test if weighing a large set of coordinates doesn't have some rounding problems due to large coordinate values.
   */
  @Test
  public void testAggregatorLargeSetPoints() {
    final ArrayList<EngineSource> sources = new ArrayList<>();

    for (int i = 0; i < 100; i++) {
      final int offset = i * 1000;
      sources.add(createSource(i, 240000 + offset, 600000 + offset, HC_A, H_A, DIA, DV_1, SPRD, 5000, 0));
    }
    final Collection<EngineSource> aggregatedSources = aggregator.aggregate(sources, SUBSTANCES);

    Assert.assertEquals("There should be 1 aggregated source", 1, aggregatedSources.size());
    final OPSSource opsSource = (OPSSource) aggregatedSources.stream().findFirst().get();
    final Point point = opsSource.getPoint();
    Assert.assertEquals("X should averaged", 289500, point.getX(), 0);
    Assert.assertEquals("Y should averaged", 649500, point.getY(), 0);
    Assert.assertEquals("Emission should be summed", 100 * 5000, opsSource.getEmission(Substance.NOX), 0);
  }

  @Test
  public void testAggregatorDuplicate() {
    final OPSSource src1 = createSource(1, 50, 150, HC_A, H_A, DIA, DV_1, SPRD, 500, 0);
    final OPSSource src2 = createSource(2, 50, 150, HC_A, H_A, DIA, DV_1, SPRD, 500, 0);

    final ArrayList<EngineSource> sources = new ArrayList<>();
    sources.add(src1);
    sources.add(src2);

    final Collection<EngineSource> aggregatedSources = aggregator.aggregate(sources, SUBSTANCES);

    for (final EngineSource src : aggregatedSources) {
      LOG.info("Source aggregated: {}", src);
    }

    Assert.assertEquals("There should be 1 aggregated sources", 1, aggregatedSources.size());

    Assert.assertFalse("Set should NOT contain src1", aggregatedSources.contains(src1));
    Assert.assertFalse("Set should NOT contain src2", aggregatedSources.contains(src2));

    final OPSSource srcTest = createSource(1, 50, 150, HC_A, H_A, DIA, DV_1, 0, 1000, 0);

    Assert.assertTrue("Set should contain srcTest - aggregated fabrication on id 1", aggregatedSources.contains(srcTest));

    final OPSSource aggregated = (OPSSource) aggregatedSources.iterator().next();

    Assert.assertEquals("Height should be same", H_A, aggregated.getEmissionHeight(), 0);
    Assert.assertEquals("X should be same", 50, aggregated.getPoint().getX(), 0);
    Assert.assertEquals("Y should be same", 150, aggregated.getPoint().getY(), 0);

    Assert.assertEquals("emission should be 1000 (2x500)", 1000, aggregated.getEmission(Substance.NOX), 0);
  }

  @Test
  public void testAggregator() {
    final OPSSource src1 = createSource(1, 50, 150, 0.2, 10, 10, DV_1, SPRD, 500, 0);
    final OPSSource src2 = createSource(2, 90, 190, 0.4, 20, 20, DV_1, SPRD, 1000, 0);

    final ArrayList<EngineSource> sources = new ArrayList<>();
    sources.add(src1);
    sources.add(src2);

    final Collection<EngineSource> aggregatedSources = aggregator.aggregate(sources, SUBSTANCES);

    Assert.assertEquals("There should be 1 aggregated source", 1, aggregatedSources.size());
    final OPSSource aggregated = (OPSSource) aggregatedSources.iterator().next();

    // Standard deviation / summed emission
    Assert.assertEquals("Spread should be 5", 5, aggregated.getSpread(), 0.01D);

    // Sum
    Assert.assertEquals("Emission should be 1500", 1500, aggregated.getEmission(Substance.NOX), 0);

    // Emission weighted average
    Assert.assertEquals("Height should be 16.66", 16.66, aggregated.getEmissionHeight(), 0.01D);

    // Average
    Assert.assertEquals("X should be 77", 77, aggregated.getPoint().getX(), 0);

    // Average
    Assert.assertEquals("Y should be 170", 177, aggregated.getPoint().getY(), 0);

    // Diameter is max difference
    Assert.assertEquals("Diameter should be weighted", 17, aggregated.getDiameter());

    // Emission weighted average
    Assert.assertEquals("Heat content should be 0.33", 0.33, aggregated.getHeatContent(), 0.01D);
  }

  @Test
  public void testNoAggregatorForForcedOutFlow() {
    final OPSSource src1 = createSource(1, 50, 150, 0.2, 10, 10, DV_1, SPRD, 500, 0);
    final OPSSource src2 = createSource(2, 90, 190, 0.4, 20, 20, DV_1, SPRD, 1000, 0);

    addForcedOutflowCharacteristics(src1, 0.6, 6.5, 200.0, OPSSourceOutflowDirection.VERTICAL);
    addForcedOutflowCharacteristics(src2, 0.8, 8.2, 75.0, OPSSourceOutflowDirection.HORIZONTAL);

    final ArrayList<EngineSource> sources = new ArrayList<>();
    sources.add(src1);
    sources.add(src2);

    final Collection<EngineSource> aggregatedSources = aggregator.aggregate(sources, SUBSTANCES);

    Assert.assertEquals("There should be 2 aggregated sources", 2, aggregatedSources.size());

  }

  @Test
  public void testSegregation() {
    final ArrayList<EngineSource> sources = new ArrayList<>();
    sources.add(createSource(1, 50, 150, HC_A, H_A, DIA, DV_1, SPRD, 500, 0));
    sources.add(createSource(2, 90, 190, HC_A, H_A, DIA, DV_1, SPRD, 500, 0));

    final Collection<EngineSource> aggregatedSources = aggregator.aggregate(sources, SUBSTANCES);

    Assert.assertEquals("There should be 2 aggregated sources", 1, aggregatedSources.size());
  }

  private void addForcedOutflowCharacteristics(final OPSSource source, final double outflowDiameter,
      final double outflowVelocity, final double emissionTemperature, final OPSSourceOutflowDirection outflowDirection) {;
      source.setHeatContent(BrnConstants.HEAT_CONTENT_NOT_APPLICABLE);
      source.setOutflowDiameter(outflowDiameter);
      source.setOutflowVelocity(outflowVelocity);
      source.setEmissionTemperature(emissionTemperature);
      source.setOutflowDirection(outflowDirection);
  }

  private OPSSource createSource(final int id, final double x, final double y, final double heatContent, final double height,
      final int diameter, final int diurnalVariation, final double spread, final double emNOx, final double emNH3) {
    final OPSSource opss = new OPSSource(id, RDNew.SRID, x, y);
    opss.setHeatContent(heatContent);
    opss.setEmissionHeight(height);
    opss.setDiameter(diameter);
    opss.setDiurnalVariation(diurnalVariation);
    opss.setSpread(spread);
    if (emNOx > 0) {
      opss.setEmission(Substance.NOX, emNOx);
    }
    if (emNH3 > 0) {
      opss.setEmission(Substance.NH3, emNH3);
    }
    return opss;
  }
}