# Use Cases Register
This page describes the main use cases for the Register product.
The use cases give a global overview of what happens internally. 
For specific details see the source code.

## Login

Register is a web application restricted to certain users linked to authorities.
Authentication is done through a NetIQ authentication service (/aerius-wui-server/src/main/java/nl/overheid/aerius/server/auth/NetIQFilter.java), which will be redirected to by Shiro.
Authorization is done based on permissions within AERIUS. 
See [UserService](/aerius-wui-server/src/main/java/nl/overheid/aerius/server/service/UserService.java) and [UserProfileUtil](/aerius-shared/src/main/java/nl/overheid/aerius/shared/util/UserProfileUtil.java).

## Dashboard

The dashboard shows global statistics on nature area level. 
The data is dynamically queried up on opening the page. 
It is not automatically refreshed.
See [DashboardActivity](/aerius-wui-register-client/src/main/java/nl/overheid/aerius/wui/register/ui/DashboardActivity.java)

## Viewing and editting requests

[Request](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/register/Request.java) is the generic domain object for Register.
It encompases all details with respect to a change in deposition due to economic activity.

There are 4 kinds of requests as far as Register is concerned:
* [PriorityProject](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/register/PriorityProject.java)
* [PrioritySubProject](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/register/PrioritySubProject.java)
* [Permit](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/register/Permit.java)
* [Notice](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/register/Notice.java) or 'Melding'

There is a more or less generic flow for these requests:

* A user adds a request file to the system, usually through an upload.
* The file is imported and the required data is used to persist a new request in the database. This request gets the [RequestState](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/register/RequestState.java) ***INITIAL***.
* The system ensures all other required data is linked to this request. For instance files and calculations. For permits and priority subprojects this is partly done by a continuous worker [RequestCalculationWorker] (/aerius-tasks/src/main/java/nl/overheid/aerius/register/RequestCalculationWorker.java).
* When all of this is done, the status of the request is changed to  ***QUEUED*** by the system. This means there can be a longer period of time between the moment of upload and the moment the request is ready to be changed by the user.
* After this, it all depends on the type of request what is done/needed.

For more information about the database part of this, see [AERIUS_db_implementatie_keuzes.md](/doc/database/AERIUS_db_implementatie_keuzes.md).

### Priority projects and priority subprojects

A priority project can be viewed as a kind of super request.
These projects are limited in number as it's a (more or less) fixed list of projects that the authorities have deemed critical.
A portion of the development space available is reserved for a priority project when it's added to Register.
A priority project can have multiple subprojects, each of which can use part of this reservation. 
The total demand of the subprojects should not exceed the initial reservation.

The priority projects page shows a filtered [PriorityProjectFilter](/aerius-shared/src/main/java/nl/overheid/aerius/shared/register/PriorityProjectFilter.java) list.

A priority project can be added by a user with the proper permissions.
This is done by uploading a GML with results (or a ZIP containing such GMLs) through [RegisterPriorityProjectUploadServlet](/aerius-wui-register-server/src/main/java/nl/overheid/aerius/server/servlet/RegisterPriorityProjectUploadServlet.java).
The GML is checked for required metadata fields.
When the import is succesful the results are added to the database along with other project data.
These results in turn will be used to make a reservation of the development spaces through a database function call in [PriorityProjectModifyRepository](/aerius-core-repository/src/main/java/nl/overheid/aerius/db/register/PriorityProjectModifyRepository.java).

Clicking on a project will take the user to the [PriorityProjectProjectDossierDetailActivity](/aerius-wui-register-client/src/main/java/nl/overheid/aerius/wui/register/ui/PriorityProjectProjectDossierDetailActivity.java).
Here the user can see the basic information about the project and edit some of the data (with the correct permissions).

The user can upload some extra files like an extra fact sheet or an actualisation of the reservation. 
This actualisation does not change the reservation right away, it's purpose is to update the reservation in the next priority-projects-update round (how/what/when is unclear at the moment).
The status can be changed as well to indicate no extra subprojects will be added/assigned.

On the 'Project' tab the user can upload new priority subprojects [RegisterPPSubProjectUploadServlet](/aerius-wui-register-server/src/main/java/nl/overheid/aerius/server/servlet/RegisterPPSubProjectUploadServlet.java).
This is done with a (Wnb) PDF or a GML and are added to the system similar to Permits.
The user can also click one of the existing priority subprojects, which will redirect to the specific part for that subproject [PriorityProjectProjectDossierDetailActivity](/aerius-wui-register-client/src/main/java/nl/overheid/aerius/wui/register/ui/PriorityProjectProjectDossierDetailActivity.java).
Here the user can view information about the subproject and review the effects before changing the status of the subproject.

### Permits

Permits are the 'normal' requests.

The permits page shows a filtered [PermitFilter](/aerius-shared/src/main/java/nl/overheid/aerius/shared/register/PermitFilter.java) list. 

A new permit can be added here by the user with the right permissions through [RegisterPermitUploadServlet](/aerius-wui-register-server/src/main/java/nl/overheid/aerius/server/servlet/RegisterPermitUploadServlet.java).
The file has to be a (Wnb) PDF.
The request is persisted along with the supplied file by the webserver.
The user can't change status right away, first the effects of the emission sources have to be calculated.
The continuous worker will handle the request at some point in time, which will take care of the needed calculations.

When the user clicks on one of the permits shown on the permit page, he or she will be redirected to the detail page [PermitOverviewActivity](/aerius-wui-register-client/src/main/java/nl/overheid/aerius/wui/register/ui/PermitOverviewActivity.java).
The detail page shows information about the request through a number of tabs.
Some of this information can be changed as well, the [DossierMetaData](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/register/DossierMetaData.java).

There is a so-called Queueing Mechanism for permits:
Based on the received date (which is the date the user received the initial request from the actual project owner, not the date the file was uploaded), the state of the permit will be changed automatically from ***QUEUED***.
This is done by the [DepositionSpaceWorker](/aerius-tasks/src/main/java/nl/overheid/aerius/register/DepositionSpaceWorker.java) through some database calls.
The status will be changed to either ***PENDING_WITH_SPACE***, indicating the request fits in the current development space, or ***PENDING_WITHOUT_SPACE***, indicating there isn't currently enough space.
This does look at the order permits were received.
A user can now change the status of the permit to one of the ***ASSIGNED*** or ***REJECTED*** states based on the review part.
There is a certain order to the states, and some state changes are not possible at all. See [AERIUS_status_flow.eap](/doc/AERIUS_status_flow.eap).

### 'Meldingen'

'Meldingen' are the requests with low enough demands along with some other requirements.

The 'Meldingen' page shows a filtered [NoticeFilter](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/register/NoticeFilter.java) list.
Next to the list a map is shown containing a [NoticeMarkerLayerWrapper](/aerius-wui-register-client/src/main/java/nl/overheid/aerius/wui/register/geo/NoticeMarkerLayerWrapper.java).
This layer should show the same requests as those shown in the list (hovering in the list highlights the marker).

'Meldingen' are added to Register through Calculator, see the [use case Calculator] (use_case_calculator.md).
It's not possible to add 'Meldingen' directly.

Clicking on an item in the list will take the user to the [NoticeActivity](/aerius-wui-register-client/src/main/java/nl/overheid/aerius/wui/register/ui/NoticeActivity.java).
The first tab shows some basic information about the 'melding'.
The second tab shows a list and map with the requests near the selected 'melding'.

## User administration

With the proper rights, the user can add or change users with the user page [UserManagementActivity](/aerius-wui-admin-client/src/main/java/nl/overheid/aerius/wui/admin/ui/UserManagementActivity.java).
On this page, a filtered list of the users is shown [UserManagementFilter](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/admin/UserManagementFilter.java).

The roles of users and which permissions are linked to those roles can be seen on the roles page [RoleManagementActivity](/aerius-wui-admin-client/src/main/java/nl/overheid/aerius/wui/admin/ui/RoleManagementActivity.java).
