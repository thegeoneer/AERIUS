# Use Case AERIUS UK

This describes the differences needed to create an AERIUS UK edition of the application.

## Database
The database must be generated from the [uk branch](../../tree/uk). This will generate a database with UK specific configuration and UK specific geographic data. This database has the same schemas and table structure as the default database and therefore can be used as a replacement of the default database. It can be used in conjunction with other AERIUS modules (webapp, geoserver, taskmanager and workers).

## Geoserver
GeoServer contains layers of the application specific data. These layers consist of several configuration files in the the GeoServer application projects (e.g. `aerius-geoserver-calculator`). These layers contain configurations specific to the spatial reference system used. AERIUS UK uses a different spatial reference system. Because of this a GeoServer application that is specific to AERIUS UK must be build. This works via a Maven functionality known as *filtering* which can replace placeholders with content specified as properties in filter files. These files can be found in the following toplevel directory: [/src/main/filters](/src/main/filters). Inside this directory filter files use the file naming convention `geoserver-[srid]-filter.properties`. The SRID defaults to 28992. To generate using a different SRID, e.g. using SRID 27700 for AERIUS UK, running maven with the option `-Dgeo.srid=27700` should suffice. A file matching this SRID should be present in the aforementioned directory. It's also possible to use a property file not matching the file naming convention by using `-Dgeoserver.filter.properties=[properties file]`. Do note though that this is ill advised.

## User interface
The user interface is not specific for AERIUS UK. All specific data is obtained from the database.
