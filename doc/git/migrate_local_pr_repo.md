# Switching from organisation master to local fork

This guide shows how to track your own fork rather than the AERIUS main repo, starting from a situation where you're tracking the main repo, rather than starting from scratch.

Outline:
- Fork the project on github to your own user space
- Rename existing origin to upstream - as is convention
- Track your own remote branch into origin
- Switch to your own master branch
- Test

Useful:

Show branches: 

```
git branch -v
```

Show remotes:
```
git remote -v
```


## Configuration

```
git remote rename origin upstream
git remote add origin https://github.com/USERNAME/AERIUS-II.git
git fetch origin
git branch -u origin/master
```

Done.

## Testing

```
touch test.txt
echo "Test" >> test.txt
git add .
git commit -a -m "Test"
git push
```

Should work with no error.

```
git reset --hard HEAD^1
git push -f
```

Test commit should be reset now.
