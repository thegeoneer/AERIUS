# AERIUS database implementatie keuzes

## Receptoren en hexagonen
Receptoren zijn de rekenpunten waar AERIUS standaard voor rekent. Deze rekenpunten liggen in een hexagonaal grid waarbij elke hexagon 1 hectare aan oppervlakte bevat. Het gebruikte grid is in RD-coordinaten van 3604, 296800 tot 287959, 629300.

De database bevat een subset van deze receptoren. Deze receptoren worden bepaald op basis van de ***geometry_of_interests*** tabel.

De ***geometry_of_interests*** tabel wordt gegenereerd (***setup.ae_build_geometry_of_interests***) door te kijken naar de landgrenzen in de database (***setup.province_land_borders***). 
Deze bevat naast Nederland ook België en Duitsland.
De intersectie van een toetsgebied in de database met de landgrenzen zorgt voor het eerste gedeelte van de geometry of interests.
Het tweede deel wordt bepaald op basis van het overblijfsel van het toetsgebied: alle stikstofgevoelige habitats die binnen dit verschil met de landgrenzen vallen (en dus op water liggen) wordt meegenomen inclusief een buffer.
Over deze twee delen wordt wederom een buffer getrokken en hiermee wordt de geometry of interests gevuld.

Vervolgens wordt met een functie (***setup.ae_build_receptors***) bepaald welke receptoren binnen de union van alle geometry of interests liggen, hiermee wordt de ***receptors*** tabel gevuld. 
De gebieden op het water waar geen habitat onder ligt zijn niet stikstofgevoelig en zijn daarom niet opgenomen in het rekengrid.

Op basis van deze receptors wordt de ***hexagons*** tabel gevuld (***setup.ae_build_hexagons***):
Alle receptoren komen met de hexagon representatie op zoom level 1 (1 hectare) in ***hexagons***.
Vervolgens wordt gekeken voor zoom levels 2 t/m 5 welke receptors het middelpunt zijn van een hexagon op dat zoom level. Voor al die receptoren wordt de hexagon op dat zoom level voor die receptor ook toegevoegd.

## Relevante habitats
Habitat gebieden (***habitat_areas***) zijn de gebieden in een toetsgebied (***assessment_areas***) waarbinnen soorten (leefgebied) of typen natuurgebieden (habitat) voorkomt. Beiden zijn opgenomen in ***habitat_types***.
Deze gebieden hebben een bepaalde dekkingsgraad van het habitat type.

Relevantie van een habitat gebied betekent dat het gebied een habitat type heeft die stikstofgevoelig is en:

* Het gebied binnen een habitat-richtlijn gebied valt en het type aangewezen is of een H9999 code heeft, of
* Een aangewezen diersoort aangewezen is voor het habitat type binnen het natura2000 gebied. Hierbij wordt voor vogelsoorten gekeken binnen vogel-richtlijn gebieden en voor overige soorten naar de habitat-richtlijn gebieden.

Uit de union van de geometrieen van deze definitie komt de ***relevant_habitat_areas*** (zie ook ***setup.build_relevant_habitat_areas_view***).

De ***relevant_habitats*** tabel is vervolgens de ***relevant_habitat_areas*** geagreggeerd per toetsgebied (***assessment_area***) en habitat type (***habitat_type***), via ***setup.build_relevant_habitats_view***.
De dekkingsgraad van deze aggregatie wordt bepaald door een gewogen gemiddelde op basis van oppervlakte.
De ***habitats*** tabel is op eenzelfde manier geaggregeerd vanuit ***habitat_areas***, via ***setup.build_habitats_view***.

Op basis van de relevante habitats wordt bijvoorbeeld de ***included_receptors*** bepaald, de receptoren die meegenomen moeten worden bij bepaalde overzichten en berekeningen. Dit gebeurd via ***setup.build_included_receptors_view***.

De ***critical_depositions*** tabel, waarin per receptor de laagste kritische depositie waarde (KDW) wordt bijgehouden wordt gegenereerd op basis van de relevante habitats, via ***setup.build_critical_depositions***.

## Berekeningen

Berekeningen komen in de database onder ***calculations***. Deze bevatten een status van de berekening (wordt beheerd vanuit Java) en het jaar waarvoor gerekend is.
Optioneel bevat de berekening een verwijzing naar ***calculation_point_sets*** waarin door de gebruiker gedefinieerde punten vastgelegd worden. Meerdere berekeningen kunnen eenzelfde set gebruiken.

Resultaten worden vastgelegd onder ***calculation_result_sets***. 
Hierbij is vastgelegd voor welke stof de resultaten zijn, om wat voor type resultaten het gaat (bijvoorbeeld concentratie of depositie), en wat voor soort set de resultaten zijn (totale resultaten of resultaten voor een bepaalde sector).
Een result set hoort altijd bij 1 berekening, een berekening kan wel meerdere result sets bevatten.

De daadwerkelijke resultaten liggen dan vast in ***calculation_results*** voor receptoren en ***calculation_point_results*** voor door de gebruiker gedefinieerde punten.
Bij ***calculation_results*** is verder geen gebruik gemaakt van een foreign key op de ***receptors*** tabel omdat in principe alle mogelijke receptoren berekent kunnen worden.
Resultaten in ***calculation_point_results*** moeten in principe dezelfde ***calculation_point_id***'s gebruiken als in de ***calculation_point_set*** die hoort bij de berekening.

Om ervoor te zorgen dat bij het vergelijken van berekeningen er nog enigszins tijdig een resultaat terugkomt kan de functie ***ae_calculation_demands*** gebruikt worden. 
Deze zorgt voor een optimalisatie waarmee totale deposities opgehaald kunnen worden. Ook worden hierin de resultaten onder de grenswaarde (***PRONOUNCEMENT_THRESHOLD_VALUE***) teruggegeven alsof er 0 depositie is.
Deze functie wordt onder andere gebruikt in ***calculation_combination_demands_view*** en ***request_demands_view***.

De ***calculation_results*** tabel kan vrij groot in omvang worden. Het ophalen van resultaten uit deze database kan dus 'traag' zijn.

## Ontwikkelingsruimte
Ontwikkelingsruimte bestaat uit een gereserveerd deel (***reserved_development_spaces***) en een uitgegeven deel (***development_spaces***).
Deze ruimtes zijn gespecificeerd per segment: projecten/vergunningen, prioritaire projecten en meldingen.

Zodra er een wijziging gebeurd aan de uitgegeven ontwikkelingsruimte, dan dient de ***development_spaces*** tabel gelocked te worden totdat de wijziging voltooid is.
Dit voorkomt dat een andere proces de gedane wijzigingen overschrijft. In het algemeen wordt dit locking gedaan in functies in de database, bijvoorbeeld bij status wijzigingen van requests.

## Requests

### Algemeen
***Requests*** zijn de aanvragen in Register. Er zijn een paar verschillende types:

* Vergunningsaanvraag (***permit***) 
* Melding (***pronouncement***)
* Prioritair subproject (***priority_subproject***)
* Reservering voor een prioritair project (***priority_project***)

Alle requests hebben een record in de ***requests*** tabel, met daarnaast een record in de type-specifieke tabel.
Elk request heeft een ***status*** waarmee wordt vastgelegd in welke fase deze zich bevindt.
Elk request heeft ook een audit trail (***request_audit_trail_items***, ***request_audit_trail_item_changes***) waarin belangrijke wijzigingen worden vastgelegd. Denk hierbij aan wijziging status, wijziging behandelaar of wijziging dossier nummer. Bij het aanmaken van een aanvraag worden voor het begin van de audit trail al wat initiele waardes hierin geplaatst.
Elk request heeft een of twee berekeningen: Altijd een berekening voor de toekomstige situatie (***proposed***) en eventueel een berekening voor de huidige situatie (***current***).

### Generieke flow
Voor alle aanvragen geldt eigenlijk dat er een generieke flow is.
Deze flow is als volgt:

* Gebruiker voert op een of andere manier een aanvraag in: de request wordt aangemaakt en staat op ***INITIAL***.
* Het systeem zorgt dat alle informatie die benodigd is aan het request worden gekoppeld. Denk aan bestanden en berekeningen. Berekeningen kunnen tijdens deze status nog uitgevoerd worden. 
* Wanneer dit gebeurd is wordt de request op de status ***QUEUED*** gezet.
* Hierna is het afhankelijk van het type aanvraag wat er verder gebeurd.

### Vergunningsaanvragen
De vergunningsaanvragen worden door (bevoegde) gebruikers van Register opgevoerd en volgen de generieke flow. Op het moment dat de aanvraag op ***QUEUED*** staat volgt de wachtrij.

De wachtrij werkt als volgt:

* Gedurende een periode, 4 weken, gedefinieerd met behulp van de constante ***PERMIT_RECEIVED_DATE_TERM***, kan de gebruiker alleen detail gegevens van de aanvraag aanpassen, zoals opmerkingen, behandelaar en postkamer datum. Deze periode wordt berekend ten opzichte van de postkamer datum (***received_date***) die dus ook aangepast kan worden.
* Elke dag draait een proces (***ae_dequeue_permits***), die alle aanvragen waarvan de periode is verstreken en die nog niet op een toegekend/afgewezen status staan op de status ***QUEUED*** zet (***ae_reset_pending_permits***). 
* Dit proces gaat vervolgens elk van die aanvragen op volgorde van postkamer datum af, om te kijken of deze binnen de beschikbare OR van dat moment passen. Past deze wel, dan wordt de status ***PENDING_WITH_SPACE***. Past het niet, dan wordt de status ***PENDING_WITHOUT_SPACE***. Onafhankelijk welke status het wordt, de demand van de aanvraag wordt opgeteld bij de pending ontwikkelingsruimte. Deze pending ontwikkelingsruimte wordt alleen in dit proces gebruikt om de aanvragen op volgorde mee te laten tellen. Bij een wijziging van status wordt dit gelogged in de audit trail: was deze bijvoorbeeld al ***PENDING_WITH_SPACE***, en is deze na het proces nog steeds ***PENDING_WITH_SPACE***, dan wordt niks gelogged.

De gebruiker van Register kan vervolgens de aanvraag toekennen of afwijzen. Afhankelijk van de ***PENDING*** status krijgt deze dan een andere status:

* Toekennen met ***PENDING_WITH_SPACE*** -> ***ASSIGNED***. Nu is de ontwikkelingsruimte gereserveerd.
* Afwijzen met ***PENDING_WITH_SPACE*** -> ***REJECTED_WITH_SPACE***. Hierbij is de ontwikkelingsruimte WEL gereserveerd.
* Afwijzen met ***PENDING_WITHOUT_SPACE*** -> ***REJECTED_WITHOUT_SPACE***. Hierbij is er geen ontwikkelingsruimte gereserveerd.

Vanaf de ***ASSIGNED*** status kan verder aangepast worden naar ***ASSIGNED_FINAL*** (na de bezwaarperiode bijvoorbeeld). Vanaf de ***REJECTED_WITH_SPACE*** kan aangepast worden naar ***ASSIGNED***. Voor alle statussen geldt dat de aanvraag verwijderd mag worden uit het systeem, waarna de eventueel gebruikte OR wordt vrijgegeven. Dit kan gezien worden als een aanpassing naar de status ***REJECTED_FINAL***, maar deze status bestaat in de database niet.

Deze status wijzigingen door de gebruiker verlopen via een functie (***ae_change_permit_state***), die controleert of de status wijziging überhaupt wel mag. Indien de wijziging er ook voor zorgt dat er OR gereserveerd wordt voor de aanvraag terwijl dat voor de wijziging niet zo was, dan wordt ook gecontroleerd of er wel ruimte is. De demand van de gebruiker wordt bij de uitgegeven ontwikkelingsruimte van permits (in de ***development_spaces*** tabel) toegevoegd. Deze controles en aanpassingen gebeuren binnen een transactie waarin de juiste tabellen gelocked worden, om te voorkomen dat er tussentijdse wijzigingen gedaan worden.

### Meldingen
Meldingen kom in het systeem doordat een gebruiker via de Calculator en Melding webapps een melding doet. Dit gebeurd dus niet direct via Register.
De generieke flow geldt wel, maar wordt in zijn geheel door een worker gedaan. Deze worker zorgt er ook voor dat de melding wordt toegekend, waarbij de uitgegeven ontwikkelingsruimte van pronouncements wordt aangepast. Dit gebeurd via een soortgelijke functie als voor vergunningen (***ae_assign_pronouncement***). Indien er geen ruimte is voor de melding, dan wordt de melding niet gedaan. De request wordt verwijdert uit het systeem en de worker zal de gebruiker via een bericht hierover informeren.

Bij meldingen geldt een extra check voor gebieden waarvan meer dan een bepaald percentage (op het moment 95%) aan ontwikkelingsruimte is uitgegeven (***permit_threshold_values***, ***ae_update_permit_threshold_values***). Deze gebieden zijn dan 'op slot' en als de melding meer dan de drempelwaarde (0.05 mol/ha/j) aan demand heeft op zo'n gebied dan wordt de melding niet toegestaan.

Voor meldingen geldt dat ze eigenlijk geen melding zijn als ze meer dan 1.00 mol/ha/j aan demand hebben, maar die controle wordt NIET door het systeem gedaan. Deze meldingen worden door bevoegde gebruikers beoordeeld en verwijderd indien nodig.

Bij meldingen is het verder mogelijk om een vorige melding met behulp van het kenmerk op te geven, met het doel deze te vervangen. Gebeurd dat, dan controleert het systeem of deze melding bestaat en is toegekend. Zo niet, dan wordt de melding niet gedaan. Bestaat de vorige melding wel, dan wordt deze verwijderd waardoor de ontwikkelingsruimte weer vrij komt, waarna de nieuwe melding wordt gedaan. Dit wordt binnen 1 transactie gedaan: Mocht de nieuwe melding niet passen dan wordt alles weer teruggedraaid. De audit trail van de vorige melding wordt toegevoegd aan de nieuwe melding zodat te volgen is door welke melding iets is vervangen.

### Prioritaire projecten
Prioritaire projecten bestaan eigenlijk uit 2 afzonderlijke types aanvragen: de reservering (***priority_projects***) en de (deel)projecten (***priority_subprojects***).
De reservering is eigenlijk het overkoepelend project, waarbinnen een gedeelte van de totale ontwikkelingsruimte van prioritaire projecten beschikbaar is.
Voor beide types geldt dat alleen bevoegde gebruikers van Register deze mogen aanmaken.

Bij het opvoeren van een nieuw prioritair project kan gecontroleerd worden of dit wel past binnen de totale pot ontwikkelingsruimte voor prioritaire projecten. De functie waarmee het potje voor het prioritair project wordt gemaakt (***ae_insert_priority_project_development_spaces***) bevat hier functionaliteit voor. Op het moment wordt op basis van een gebruikersrecht bepaald of dit wel of niet moet worden gecontroleerd. Prioritaire projecten worden verder als uitzondering op het moment niet naar ***QUEUED*** gezet wanneer de reservering klaar staat in het systeem, maar gaat gelijk door naar ***ASSIGNED***.

Een prioritair project kan verder een actualisatie bestand krijgen van de bevoegd gebruiker, waarin een update van de reservering vastgelegd wordt. Dit bestand wordt op het moment nog niet gebruikt, maar het is de bedoeling om 1 maal per jaar alle actualisaties te verwerken in de reserveringen. Hoe dit gaat gebeuren is nog niet bekend. In de huidige situatie is het in ieder geval niet mogelijk om een reservering aan te passen, naast het compleet verwijderen en opnieuw aanmaken van het prioritaire project in Register.

Prioritaire subprojecten zijn de daadwerkelijke vergunningen die uit worden gegeven binnen een prioritair project. Het proces lijkt hier veel op die van normale vergunningen. Er wordt echter geen wachtrij gehanteerd aangezien de ontwikkelingsruimte in principe al gereserveerd is. De subprojecten kennen dus ook geen ***PENDING*** status. Subprojecten kunnen alleen naar ***ASSIGNED***, ***ASSIGNED_FINAL*** en ***REJECTED_WITHOUT_SPACE*** statussen.

De status wijziging moet gebeuren via een functie (***ae_change_priority_subproject_state***). Bij het toekennen wordt altijd gecontroleerd of de aanvraag past binnen de reservering, waarbij andere subprojecten die reeds zijn toegekend worden meegenomen. Er is ook een optie om te controleren of de aanvraag past binnen de totale ruimte voor prioritaire projecten, dit wordt ook gedaan op basis van een gebruikersrecht (in de Java code).   
