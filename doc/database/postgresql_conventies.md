# *AERIUS PostgreSQL Conventies*


# Stijlconventies

## Hoofdlettergebruik

* Gebruik HOOFDLETTERS voor statement namen en keywords.
  
  ```sql
  SELECT, CREATE TABLE abc, NULL::integer AS column, WHERE a = 1 AND b IS NULL
  ```

* Gebruik kleine letters voor tabel- en kolomnamen, aliassen, kolomtypes en andere identifiers.

* Voor bestaande (aggregate) functies, gebruik de kapitalisatie zoals getoond in de handleiding. Bijvoorbeeld: ```MIN```, ```MAX```, ```SUM```, ```array_agg```, ```ST_Intersects```, etc.

  ```sql
  SELECT MAX(a), array_agg(b), ST_Union(c), FIRST(d)
  ```

## Witruimte

### Newlines

* Aanbevolen: Zet elke van de volgende op een nieuwe regel:

    * elke ```SELECT``` kolom

    * elke ```FROM``` tabel

    * elke ```WHERE``` conditie

    * elke expressie binnen een ```WHERE``` conditie, beginnende met ```AND```, ```OR```, etc.

    * ```GROUP BY``` regel

    * ```ORDER BY``` regel

    * ```LIMIT```, ```UNION```, etc.

    * elk element van een lange ```CASE``` statement

* **Eenvoudige** ```SELECT``` kolommen en ```WHERE``` condities mogen echter op dezelfde regel worden geplaatst om ruimte te besparen.

* Aanbevolen: Lege regels na de volgende:

    * laatste ```SELECT``` kolom

    * laatste ```FROM``` tabel

    * laatste ```WHERE``` conditie

    * ```GROUP BY``` regel

    * ```ORDER BY``` regel

### Inspringing

* Aanbevolen: alle regels één positie inspringen behalve het eerste ```SELECT``` statement.

* Extra inspringing na de eerste ```FROM``` join (opvolgende joins op het zelfde niveau).

* Subqueries verder inspringen. Terugspringen na het afsluitende haakje van de subquery.

* **Niet** inspringen bij ```UNIONS``` etc. Dit suggereert een parent-child relatie die er niet is: de queries worden namelijk op volgorde uitgevoerd.

### Witruimte binnen een regel

* Zet spaties rondom operators en na komma’s.

* Spaties binnen ronde haakjes is niet nodig.

    * Alleen indien de inhoud tussen de haakjes complex is (zoals bij subqueries en tabeldefinities), kun je de inhoud op alleenstaande regels zetten en inspringen, zoals in de vorige paragraaf is genoemd.

## Joins

Gebruik voor inner joins de "```INNER JOIN``` met ```USING```/```ON```" syntaxis in plaats van de “komma-met-```WHERE```” syntaxis. Idem voor de andere typen joins (```LEFT```/```FULL OUTER```/```CROSS```).

```sql
SELECT *
    FROM table1, table2, table3
    WHERE
         table1.columnname = table2.columnname
         AND table1.column_x = table3.column_y
         AND table1.column_z = table2.column_w
```

wordt:

```sql
SELECT *
    FROM table1
        INNER JOIN table2 USING (columnname)
        INNER JOIN table3 ON (table1.column_x = table3.column_y 
                          AND table1.column_z = table2.column_w)
```

Schrijf queries liever zo dat ```RIGHT JOIN``` niet nodig is.

## Overige

* Gebruik altijd het ```AS``` keyword voor kolom en tabel aliassen, ook al is dit optioneel in SQL.

* Hoewel beide geldig zijn, gebruik het keyword ```integer``` in plaats van ```int```, en ```double precision``` in plaats van ```float```.

* Wees voorzichtig met het gebruik van serials. Vermijd deze indien mogelijk: ze introduceren problemen tijdens backup/restore (en inserten), waardoor id’s uit de pas kunnen gaan lopen.
Niet iedere tabel hoeft een kolom met id’s te hebben voor zijn elementen. Het is vaak beter om records te identificeren met grotere primary keys. Deze kunnen bestaan uit (multi-kolom) foreign keys.
Serials kunnen echter wel waarde hebben voor user-generated data.

# Naamgevingsconventies

Engels heeft altijd de voorkeur.

Gebruik nooit spaties in namen van identifiers, maar underscores. Dan hoeven er nooit "quotes" gebruikt te worden.

In de volgende paragraaf worden conventies benoemd voor de identifiers van de verschillende soorten database objecten. De naamgeving voor tabellen (en views) is in de daarop volgende paragraaf specifieker uitgewerkt aan de hand van de inhoud van die tabellen.

## Naamgeving van identifiers

* Suffix views met ```_view``` (om ze van tabellen te onderscheiden)

    * Prefix views die door geoserver gebruikt worden met ```wms_```

* Prefix functies met een eigen gekozen term (om ze te onderscheiden van PostgreSQL en PostGIS functies)

    * Bij AERIUS kiezen we voor: ```ae_```

* Veldnamen welke id’s voorstellen (dus verwijzen naar zichzelf of andere tabellen) moeten gesuffixed worden met ```_id``` en verder gelijk zijn aan de tabelnaam waar ze naar verwijzen, in enkelvoud.
Voorbeelden: ```receptor_id```, ```hexagon_id```, ```habitat_area_id```, ```habitat_type_id```, ```natura2000_area_id```

* Primary keys moeten benoemd worden als ```table_name_pkey```

* Foreign keys moeten benoemd worden als ```table_name_fkey_other_table_name```, bijvoorbeeld:

  ```sql
  CONSTRAINT habitat_areas_fkey_habitat_types FOREIGN KEY (habitat_type_id) REFERENCES habitat_types
  ```

* Andere constraints en indexes moeten soortgelijke beschrijvende namen krijgen. Tabel naam, kolom naam, en indextype indien afwijkend van de standaard. Bijvoorbeeld:

  ```sql
  CONSTRAINT jurisdictions_name_unique UNIQUE (name);
  CREATE INDEX natura2000_areas_geometry_gist ON natura2000_areas USING GIST (geometry);
  CREATE INDEX natura2000_areas_name ON natura2000_areas(name);
  CREATE INDEX variants_user_id ON variants(user_id);
  ```

* Variabelen gebruikt binnen PostgreSQL-functies moeten dezelfde regels volgen, overweeg echter een vaste prefix voor functie parameter variabelen (en eventueel gedeclareerde variabelen). Het komt vaak voor dat sommige van zulke variabelen dezelfde naam hebben als een kolomnaam welke wordt gebruikt in de query. In dit geval is het prettiger om onderscheid te maken met behulp van een prefix, dan overal verplicht tabelnamen te specificeren.

    * Bij AERIUS kiezen we voor: ```v_```

## Naamgeving van tabellen

Voor de naamgeving van tabellen wordt goed gekeken naar de inhoud van die tabel. Aan de hand hiervan kan bepaald worden wat de tabelsoort is. De naamgevingsregels per tabelsoort staan in het overzicht hieronder. De uitleg van de verschillende tabelsoorten staan verderop.

Meestal kunnen deze regels ook worden toegepast op views. 

### Tabelnaamgeving per tabelsoort

<table>
  <tr>
    <td>Objecttabellen</td>
    <td>De naam van het object in meervoud.<br>
Bijvoorbeeld: <tt>natura2000_areas</tt>, <tt>sources</tt>, <tt>sectors</tt>, <tt>sites</tt>, <tt>habitat_types</tt></td>
  </tr>
  <tr>
    <td>Objecttabellen extra keys</td>
    <td>Dit komt niet terug in de naam.</td>
  </tr>
  <tr>
    <td>Aanvullende data</td>
    <td>Object in enkelvoud + beschrijvende suffix (of <tt>_properties</tt> voor algemeen).<br>
Bijvoorbeeld: <tt>source_emissions</tt>, <tt>farm_animal_category_economic_growths</tt>, <tt>habitat_type_relations</tt>, <tt>natura_2000_area_properties</tt></td>
  </tr>
  <tr>
    <td>Aanvullende data extra keys</td>
    <td>Dit hoeft niet perse in de naam aangegeven te worden en heeft vaak ook geen toegevoegde waarde. Wil je dit vanwege verduidelijking toch doen, gebruik dan de suffix <tt>_per_xxxx</tt>.<br>
Maar in principe geldt: Houdt de namen simpel en duidelijk, je hoeft niet de hele tabelstructuur in de naam te beschrijven.<br>
Ook kun je tabellen groeperen per soort, zoals de <tt>farm_</tt> tabellen.</td>
  </tr>
  <tr>
    <td>Subset objecttabellen</td>
    <td>De objecttabel naam met een beschrijvende prefix.<br>
Bijvoorbeeld: <tt>report_assessment_areas</tt>, <tt>farm_sites</tt>, <tt>designated_habitats</tt></td>
  </tr>
  <tr>
    <td>Subset objecttabellen extra keys</td>
    <td>Dit hoeft niet perse in de naam aangegeven te worden. Wil je dit vanwege verduidelijking toch doen, gebruik dan de suffix <tt>_per_xxxx</tt>.</td>
  </tr>
  <tr>
    <td>Koppeltabel N:N</td>
    <td>De naam van het ene object + <tt>_to_</tt> + de naam van het andere object.<br>
Gebruik voor beide kanten meervoud want het is N:N.<br>
Als er extra velden zijn hoeft dit niet in de naam terug te komen.<br>
Bijvoorbeeld: <tt>receptors_to_critical_deposition_areas</tt></td>
  </tr>
  <tr>
    <td>Koppeltabel N:1</td>
    <td>De naam van het ene object + <tt>_</tt> + de naam van het andere object.<br>
Gebruik meervoud voor de linkerkant en enkelvoud voor de rechterkant want het is N:1.<br>
Op deze manier lijkt het bijna een gewone "aanvullende data" tabelnaam, alleen wordt er meervoud gebruikt.<br>
Als er extra velden zijn hoeft dit niet in de naam terug te komen.<br>
Bijvoorbeeld: <tt>sources_gcn_sector</tt>, <tt>sectors_main_gcn_sector</tt></td>
  </tr>
  <tr>
    <td>Koppeltabel 1:N</td>
    <td>Dit mag niet, herschrijf als N:1.</td>
  </tr>
  <tr>
    <td>Koppeltabel 1:1</td>
    <td>De naam van het ene object + <tt>_</tt> + de naam van het andere object.<br>
Gebruik voor beide kanten enkelvoud want het is 1:1.<br>
Op deze manier wordt het een gewone "aanvullende data" tabelnaam.<br>
Als er extra velden zijn hoeft dit niet in de naam terug te komen.</td>
  </tr>
  <tr>
    <td>Koppeltabel extra keys</td>
    <td>Bij voorkeur niet, anders weer <tt>_per_</tt>.</td>
  </tr>
  <tr>
    <td>Receptordata</td>
    <td>Alleen bij receptordata wordt de objectnaam (<tt>receptor</tt>) niet geprefixed.<br>
De receptordata moet te herkennen zijn aan de soort (<tt>depositions</tt>, <tt>desires</tt>, etc).<br>
Net zoals bij aanvullende data, kunnen er additionele beschrijvende suffixen zijn.<br>
Bijvoorbeeld: <tt>depositions_global_policies</tt>, <tt>terrain_properties</tt>, <tt>deposition_spaces_divided</tt></td>
  </tr>
  <tr>
    <td>Uitgesplitste receptordata</td>
    <td>Het object waarop wordt uitgesplitst wordt als prefix toegevoegd (enkelvoud).<br>
Let op dat <tt>year</tt>, <tt>substance_id</tt>, <tt>zoom_level</tt> niet mee doen in de naamgeving.<br>
Bijvoorbeeld: <tt>other_depositions</tt>, <tt>sector_depositions_global_policies</tt></td>
  </tr>
  <tr>
    <td>Geaggregeerde receptordata</td>
    <td>Idem als bij uitsplitsing. Aan het type prefix kun je wel zien of er geaggregeerd wordt of uitgesplitst. Normaliter wordt op habitat en andere ruimtelijke objecten altijd geaggregeerd, en op (bijvoorbeeld) sector wordt altijd uitgesplitst.<br>
Bijvoorbeeld: <tt>assessment_area_deposition_spaces_view</tt>, <tt>relevant_habitat_depositions_view</tt>, <tt>critical_deposition_area_deviation_ranges_view</tt><br><br> 
In de gevallen dat er wordt "uitgesplitst" op een object waarop normaal geaggregeerd wordt, is dat te herkennen aan het feit dat <tt>receptor_id</tt> nog steeds wordt teruggeven als kolom. Laat dit dan in de naam terugkomen.<br>
Bijvoorbeeld: <tt>critical_deposition_area_receptor_depositions_view</tt>, <tt>wms_assessment_area_receptor_delta_depositions_view</tt></td>
  </tr>
  <tr>
    <td>Uitgesplitste geaggregeerde receptordata</td>
    <td>Eerst de aggregatie prefix, dan de uitsplitsing prefix.<br>
Bijvoorbeeld: <tt>assessment_area_sector_economic_desires_view</tt></td>
  </tr>
</table>


### Extra opmerkingen bij tabelnaamgeving

* De velden ```year```, ```substance```, ```zoom_level``` etc. worden genegeerd in de naamgeving. Receptordata heeft heel vaak die velden als key, maar dat hoeven we niet telkens te noemen. Ook ```jurisdiction_id``` wordt vaak niet genoemd in de naam want ```jurisdiction_policies``` suggereert dit al.

* De lijst van soorten receptordata is altijd op ```receptor_id```, tenzij er ```_by_``` in de naam staat. Dan is er geaggregeerd. Andere soorten data, zoals bron data, habitat data, en farm data, worden eigenlijk nooit geaggregeerd op assessment area, habitat etc.

* Als we alleen naar ```habitat_type_id``` verwijzen, gebruiken we ```habitat_type``` in de naam. Als we naar ```[assessment_area_id, habitat_type_id]``` verwijzen, gebruiken we ```habitat``` in de naam. De reden is dat deze combinatie erg vaak voorkomt, en ```assessment_area_habitat_type_``` te lang is als prefix.

* Met soortgelijke redenering komt een ```critical_deposition_area_id``` nooit voor zonder ```assement_area_id```, dus het is voldoende om te zeggen ```critical_deposition_area_```.

### Uitleg tabelsoorten

#### Objecttabel

Een objecttabel is zoals de naam al aangeeft een hoofdtabel van een object. Een object is iets wat een id heeft, bijvoorbeeld ```natura2000_area_id``` of ```farm_animal_category_id```. Ieder object heeft maar één "hoofd" objecttabel met daarin één key column. Er kunnen optioneel kolommen zijn voor eigenschappen of data.

Er zijn enkele uitzonderingen waarbij de hoofd objecttabel een extra key heeft. Bijvoorbeeld ```zoom_level``` in ```hexagons```.

#### Aanvullende data tabel

Alle eigenschappen en data kun je vaak niet in de hoofdtabel kwijt, dus zijn er ook aanvullende tabellen. Soms is deze data uitgebreider gespecificeerd op extra key(s).

Het gros van de tabellen zijn aanvullende data tabellen.

#### Receptordata tabel

De grens tussen eigenschappen, data, oppervlakten, factoren en cijfers is soms vaag. Uiteindelijk noemen we dit allemaal maar "aanvullende data bij een object". De enige uitzondering is “receptordata”, omdat we hier het meeste mee doen in de database en deze data op allerlei manieren wordt geaggregeerd, gecombineerd en uitgesplitst. Per definitie is dit data die (oorspronkelijk) van het receptor object vandaan komt.

Receptordata is in de basis een waarde bij een ```receptor_id```. Daarnaast kan de data worden uitgesplitst, je krijgt dan meer detail. Bijvoorbeeld sector deposities per receptor. De kolom ```receptor_id``` blijft in dit geval bestaan, maar ```sector_id``` is extra.

Data kan ook worden geaggregeerd, je krijgt dan minder detail. Bijvoorbeeld deposities per toetsgebied. De kolom ```assessment_area_id``` is dan extra, maar ```receptor_id``` is weg geaggregeerd.

Een combinatie van beide kan ook, bijvoorbeeld sector deposities per toetsgebied. De kolom ```receptor_id``` is dan weer weg.

Voorbeelden van receptor data: ```depositions```, ```(deposition) corrections```, ```deposition_space```, ```deposition_space_corrections```, ```(economic_)growths```, ```(economic_)growth_corrections```, ```(economic_)desires```, ```demands```, ```reductions```, ```terrain_properties```.

#### Koppeltabel

Om een relatie tussen twee objecten aan te geven wordt een koppeltabel gebruikt. Met een kolom voor het ene object en voor het andere object. Eventueel bevat deze extra informatie over de koppeling.

Veel van de koppeltabellen gaan over ruimtelijke (geometrische) relaties, bijvoorbeeld hexagonen die in een toetsgebied liggen. Denk dan aan een tabel met ```receptor_id``` en ```assessment_area_id``` en eventueel een veld ```surface``` voor het intersectie deel.

Ook hier zijn extra keys mogelijk. Zo horen habitat types vaak bij een toetsgebied, en deze combinatie kan weer ruimtelijk gekoppeld worden aan receptors.

Relaties kunnen N:N, N:1 of 1:1 zijn. Als het een N:1 of 1:1 relatie betreft, wordt deze hetzelfde behandeld als een aanvullende data tabel. Immers is het dan een eigenschap die geldt op een object met een id. Enkel bij N:N is het een echte koppeltabel.

* **N:N** is many-to-many: ieder object kan aan meerdere andere objecten gekoppeld worden en andersom. Bijvoorbeeld een herstelmaatregel kan voor meerdere habitattypes gelden, en tegelijkertijd kan een habitattype onder meerdere maatregelen vallen (oftewel er kan overlap zijn). De primary key is de combinatie van de twee object id’s.

* **1:N** is one-to-many: een object is gekoppeld aan meerdere andere objecten. Bijvoorbeeld een GCN-sector omvat meerdere bronnen, maar bronnen hebben altijd één GCN-sector.

   ***Een 1:N relatie moet worden omgedraaid naar N:1***, vervolgens is de primary key is alleen op de eerste kolom (het N-object). Dus in dit voorbeeld is er een aanvullende data tabel voor bronnen, met daarin de GCN-sector van iedere bron.

* **1:1** is one-to-one: zoals N:1 maar de tweede kolom (het 1-object) krijgt tevens een unique-constraint mee. Beide objecten kunnen dus maar éénmalig gekoppeld worden. We hebben hier geen praktijkvoorbeeld van. Als "object" A is gekoppeld aan “ding” B, dan zal object A niet gekoppeld zijn aan andere “dingen”, en ding B niet aan andere “objecten”.

#### Subset tabel

Een speciale aanvullende data tabel is de subset tabel. Deze bevat een subselectie van objecten om een bepaalde filterslag te kunnen doen, bijvoorbeeld ```farm_sites```. In de basis bevat deze daarom alleen het object id. Optioneel bevat deze extra kolommen met eigenschappen specifiek voor de gefilterde objecten. Ook deze tabellen kunnen extra key(s) hebben, zoals bijvoorbeeld ```designated_species```.

# Voorbeelden

## Typen

```sql
CREATE TYPE export_group_by_habitat_type AS ENUM
	('none', 'type', 'area');
```

```sql
CREATE DOMAIN posint AS integer
	CHECK (VALUE >= 0::integer);
```

## Tabellen

```sql
/*
 * habitats
 * --------
 * Habitatgebieden samengevoegd naar toetsgebied met habitattype niveau.
 * De geometrie is de combinatie van alle uitsnedes van dat type.
 */
CREATE TABLE habitats
(
	assessment_area_id integer NOT NULL,
	habitat_type_id integer NOT NULL,
	coverage fraction NOT NULL,
	geometry geometry(MultiPolygon, 28992),

	CONSTRAINT habitats_pkey PRIMARY KEY (assessment_area_id, habitat_type_id),
	CONSTRAINT habitats_fkey_habitat_types FOREIGN KEY (habitat_type_id) REFERENCES habitat_types
);

CREATE INDEX idx_habitats_geometry_gist ON habitats USING GIST (geometry);
CREATE INDEX idx_habitats_habitat_type_id ON habitats (habitat_type_id);
```

## Views

```sql
/*
 * industry_site_geometry_view
 * ---------------------------
 * Geeft van alle industriebronnen op een site het zwaartepunt terug.
 */
CREATE OR REPLACE VIEW industry_site_geometry_view AS
SELECT
	site_id,
	ST_Centroid(ST_Union(sources.geometry)) AS geometry

	FROM sites
		INNER JOIN industry_sources USING (site_id)
		INNER JOIN sources USING (source_id)

	GROUP BY site_id

	LIMIT 25
;
```

```sql
CREATE OR REPLACE VIEW average_deposition_reductions_per_habitat_view AS
SELECT
	scenario_id,
	base_year,
	year,
	habitat_name,
	habitat_description,
	COALESCE(SUM(reduction * surface * coverage) / NULLIF(SUM(surface * coverage), 0), 0)::real AS avg_reduction
	
	FROM
		(SELECT
			scenario_id,
			base_year,
			year,
			hexagons.receptor_id,
			habitat_types.name AS habitat_name,
			habitat_types.description AS habitat_description,
			habitat_areas.coverage,
			reduction,
			ST_Area(ST_Intersection(ST_Intersection(hexagons.geometry, variants.geometry), habitat_areas.geometry)) AS surface
			
			FROM receptor_deposition_reductions_view
				INNER JOIN scenarios USING (scenario_id)
				INNER JOIN variants USING (variant_id)
				INNER JOIN hexagons USING (receptor_id)
				INNER JOIN habitat_areas ON ST_Intersects(hexagons.geometry, habitat_areas.geometry)
				INNER JOIN habitat_types USING (habitat_type_id)
			
			WHERE hexagons.zoom_level = 1
		) AS reductions_per_receptor
		
	GROUP BY scenario_id, base_year, year, habitat_name, habitat_description
;
```

```sql
CREATE OR REPLACE VIEW distribution_habitats_surface_view AS
SELECT
	scenarios.scenario_id,
	habitat_types.name AS habitat_name,
	habitat_types.description AS habitat_description,
	SUM(ST_Area(ST_Intersection(habitat_areas.geometry, variants.geometry)) * habitat_areas.coverage)::real AS total_surface
	
	FROM scenarios
		INNER JOIN variants USING (variant_id)
		INNER JOIN habitat_areas ON ST_Intersects(habitat_areas.geometry, variants.geometry)
		INNER JOIN habitat_types USING (habitat_type_id)
	
	GROUP BY scenario_id, habitat_name, habitat_description
UNION ALL
SELECT
	scenarios.scenario_id,
	'H0000' AS habitat_name,
	FIRST(variants.name) AS habitat_description,
	(ST_Area(FIRST(variants.geometry))
		- SUM(ST_Area(ST_Intersection(habitat_areas.geometry, variants.geometry)) * habitat_areas.coverage))::real AS total_surface
	
	FROM scenarios
		INNER JOIN variants USING (variant_id)
		INNER JOIN habitat_areas ON ST_Intersects(habitat_areas.geometry, variants.geometry)
	
	GROUP BY scenario_id
;
```

```sql
CREATE OR REPLACE VIEW characterization.habitat_type_states_per_natura2000_area_view AS
SELECT
	natura2000_area_id,
	management_period,
	habitat_type_id,
	habitat_state,
	habitat_quality_type,
	rehabilitation_strategy_types,
	goal,
	landscape_type

	FROM characterization.habitat_type_properties
		INNER JOIN characterization.characterization_to_natura2000_areas USING (natura2000_area_id)
		INNER JOIN characterization.natura2000_area_properties USING (natura2000_area_id)
		CROSS JOIN
			(SELECT DISTINCT management_period -- Ensure all 3 management periods are included in the returnset at least
				
				FROM
					(SELECT management_period FROM characterization.rehabilitation_strategy_costs
					 UNION
					 SELECT management_period FROM characterization.rehabilitation_strategy_area_expands
					) AS rehabilitation_strategy_management_periods
			) AS management_periods
		LEFT JOIN
			(SELECT
				characterization_id,
				management_period,
				habitat_type_id,
				array_to_string(array_agg(rehabilitation_strategy_type), ', ') AS rehabilitation_strategy_types

				FROM
					(SELECT DISTINCT
						characterization_id,
						management_period,
						habitat_type_id,
						rehabilitation_strategy_type
						
						FROM characterization.rehabilitation_strategies
							INNER JOIN characterization.rehabilitation_strategy_habitat_types USING (rehabilitation_strategy_id)
							LEFT JOIN
								(SELECT rehabilitation_strategy_id, management_period FROM characterization.rehabilitation_strategy_costs
								 UNION
								 SELECT rehabilitation_strategy_id, management_period FROM characterization.rehabilitation_strategy_area_expands
								) AS rehabilitation_strategy_cost_or_area_expand USING (rehabilitation_strategy_id)
				
				ORDER BY characterization_id, management_period, habitat_type_id, rehabilitation_strategy_type
				
			) AS distinct_rehabilitation_strategy_types
		
		GROUP BY characterization_id, management_period, habitat_type_id
		
	) AS rehabilitation_strategy_types_accum USING (characterization_id, management_period, habitat_type_id)
;
```

## Functies

```plpgsql
CREATE OR REPLACE FUNCTION ae_collect_receptors(v_scenario_id integer, v_zoom_level integer)
	RETURNS SETOF receptor_rs AS
$BODY$
DECLARE
	v_geometry geometry;
	rs receptor_rs%rowtype;
BEGIN
	-- Get variant_geometry of scenario with a buffer of 250
	SELECT ST_Buffer(geometry, 250) INTO v_geometry	
		FROM scenarios
			INNER JOIN variants USING (variant_id)
		WHERE scenario_id = v_scenario_id;
		
	-- Return result
	FOR rs IN
	SELECT
		receptors.receptor_id,
		receptors.geometry

		FROM receptors
			INNER JOIN hexagons USING (receptor_id)

		WHERE
			hexagons.zoom_level = v_zoom_level
			AND ST_Within(hexagons.geometry, v_geometry)
	LOOP
		RETURN NEXT rs;
	END LOOP;
	
	RETURN;
END;
$BODY$
LANGUAGE plpgsql STABLE;
```

```sql
CREATE AGGREGATE ae_median(numeric) (
	SFUNC = array_append,
	STYPE = numeric[],
	FINALFUNC = ae_median_agg,
	INITCOND = '{}'
);
```
