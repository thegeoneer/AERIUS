# UI styling en CSS flexbox

Het zal je misschien zijn opgevallen dat driekwart van alle UI de afgelopen weken is aangepast om gebruik te maken van flexbox / display: flex;

Flex(box) is een css3 feature waarmee je elementen honderd keer makkelijker dan met display:block/inline-block; kan distribueren/inrichten. Tastbaar resultaat in AERIUS is dat voor het eerst ooit alle UI volgens de oorspronkelijke regeltjes van Taco zijn ingericht: alle elementen lijnen perfecto uit, marges zijn in orde en overal hetzelfde, etc

In het vervolg wanneer iemand met de UI bezig gaat gaarne ook de boel adhv flexbox te implementeren:

##### Complete guide:

https://css-tricks.com/snippets/css/a-guide-to-flexbox/

##### Snelle concrete regeltjes voor aerius:
- {res.css.flex} om een element flex te maken

- {res.css.grow} om een child van een flex item te laten groeien tot aan max of groeien relatief aan siblings

- {res.css.sourceDetailCol1} en {res.css.sourceDetailCol2} voor de 2-kolommen structuur die erg vaak voorkomt (met parent .flex)

- {res.css.flex} op een parent element is hetzelfde als dat alle children display: inline-block; en vertical-align: top; hebben

- expliciete breedtes vermijden alsof het hete olie is, als een breedte wel nodig is (zelden), liever flex-basis: ..px gebruiken -- width: ..px is alleen nodig voor panelen die geen flexbox zijn.

- voor marges {res.css.calculatorRow} gebruiken, alle andere vormen van marges zijn slechts zelden nodig (als in: UI opnieuw inrichten als je ze wel nodig hebt)

- copy/paste css rules in een uiBinder zijn vaak niet nodig / te vermijden: rules uit strict.css/R.java voldoen
