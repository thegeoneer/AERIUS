/*
 * background_cell_results_view
 * ----------------------------
 * Achtergrond concentratie, depositie en overschrijdingsdagen informatie.
 * Gebruik 'year', 'substance_id', 'result_type' en ST_Intersects(.., geometry) in de WHERE-clause.
 * Voorbeeld:
 *     WHERE year = 2018 AND substance_id = 10 AND result_type = 'concentration'
 *     AND ST_Intersects(ST_SetSRID(ST_Point(218928, 486793), ae_get_srid()), geometry)
 */
CREATE OR REPLACE VIEW background_cell_results_view AS
SELECT
	background_cell_id,
	year,
	substance_id,
	result_type,
	result,
	geometry

	FROM background_cells
		INNER JOIN background_cell_results USING (background_cell_id)
;
