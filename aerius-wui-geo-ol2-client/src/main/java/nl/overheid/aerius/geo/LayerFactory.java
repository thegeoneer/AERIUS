/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.gwtopenmaps.openlayers.client.format.WMSCapabilities;
import org.gwtopenmaps.openlayers.client.format.WMTMSCapabilities;
import org.gwtopenmaps.openlayers.client.format.WMTMSCapabilities.Layer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.core.client.JsArray;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.AeriusWMSMapLayer.WMSLayerEventBinder;
import nl.overheid.aerius.geo.MapLayoutPanel.SimpleLayerFactory;
import nl.overheid.aerius.geo.shared.LabeledMultiLayerProps;
import nl.overheid.aerius.geo.shared.LayerAeriusMultiWMSProps;
import nl.overheid.aerius.geo.shared.LayerAeriusWMSProps;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerTMSProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.geo.shared.MultiLayerProps;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.NotificationUtil;

/**
 * Helper interface to create custom layers.
 */
@Singleton
public class LayerFactory implements SimpleLayerFactory {
  private static final Logger LOGGER = Logger.getLogger("LayerFactory");

  private final UserContext userContext;

  @Inject
  public LayerFactory(final UserContext uc) {
    this.userContext = uc;
  }

  /**
   * Creates a MapLayer based on the LayerItem information.
   *
   * @param eventBus EventBus to listen to layer change events
   * @param map Map to which the new layer will be attached.
   * @param li Input data to create layer from
   * @return A new MapLayer
   *
   */
  @Override
  public MapLayer createLayer(final EventBus eventBus, final MapLayoutPanel map, final LayerProps li) {
    MapLayer ml = null;
    if (li instanceof LabeledMultiLayerProps) {
      ml = new LabeledMultiMapLayer(eventBus, map, (LabeledMultiLayerProps) li);
    } else if (li instanceof MultiLayerProps) {
       ml = new MultiMapLayer(eventBus, map, (MultiLayerProps) li);
    } else if ((li instanceof LayerAeriusMultiWMSProps<?>) || (li instanceof LayerAeriusWMSProps)) {
      ml = createWMSLayer(map, (LayerWMSProps) li, userContext.getEmissionValueKey(), eventBus);
    } else if (li instanceof LayerWMSProps) {
      ml = createWMSLayer(map, (LayerWMSProps) li);
    } else if (li instanceof LayerTMSProps) {
      ml = TMSMapLayer.create(map, (LayerTMSProps) li);
    }

    if (ml == null) {
      if (LOGGER.isLoggable(Level.WARNING)) {
        LOGGER.warning("Could not create a Layer, this may not be a problem, but here's the name of the layer: " + li.getName());
      }
      NotificationUtil.broadcastError(eventBus, M.messages().errorLayerCouldNotBeLoaded(li.getTitle() == null ? li.getName() : li.getTitle()));
    }

    return ml;
  }

  public static AeriusWMSMapLayer createWMSLayer(final MapLayoutPanel map, final LayerWMSProps props, final EmissionValueKey emissionValueKey,
      final EventBus eventBus) {
    final AeriusWMSMapLayer layer = (AeriusWMSMapLayer) createWMSLayer(map, props);
    layer.setEventBus(eventBus);
    layer.setYear(emissionValueKey.getYear());

    final WMSLayerEventBinder eventBinder = GWT.create(WMSLayerEventBinder.class);
    eventBinder.bindEventHandlers(layer, eventBus);

    return layer;
  }

  public static MapLayer createWMSLayer(final MapLayoutPanel map, final LayerWMSProps props) {
    final String capabilitiesXML = props.getCapabilities().getCapabilitiesXML();
    final String[] names = props.getName().split(",");

    final String url;
    final String layerTitle;
    MapLayer mapLayer = null;
    if (capabilitiesXML != null) {
      mapLayer = createCapabilitiesLayer(map, capabilitiesXML, names, props);
    }
    if (mapLayer == null) {
      url = props.getCapabilities().getUrl();
      layerTitle = names.length == 0 ? "" : names[0];
      mapLayer = create(map, url, layerTitle, props);
    }
    return mapLayer;
  }

  private static MapLayer createCapabilitiesLayer(final MapLayoutPanel map, final String capabilitiesXML, final String[] names,
      final LayerWMSProps props) {
    try {
      final WMSCapabilities wmsc = new WMSCapabilities();
      final WMTMSCapabilities caps = wmsc.read(capabilitiesXML);
      if (caps == null || caps.getMap().getPropertyNames().isEmpty()) {
        if (LOGGER.isLoggable(Level.WARNING)) {
          final String capXML = capabilitiesXML == null || capabilitiesXML.isEmpty()
              ? "<empty>" : capabilitiesXML.substring(0, Math.min(capabilitiesXML.length(), 400));
          LOGGER.warning("Could not parse capabilities XML: " + capXML);
        }
      } else {
        return findLayer(map, caps.getLayers(), names, props, caps.getMap().getHref());
      }
    } catch (final JavaScriptException e) {
      if (LOGGER.isLoggable(Level.SEVERE)) {
        LOGGER.log(Level.SEVERE, "Exception on read capabilities:" + capabilitiesXML, e);
      }
    }
    return null;
  }

  private static MapLayer findLayer(final MapLayoutPanel map, final JsArray<Layer> layers, final String[] names, final LayerWMSProps props,
      final String url) {
    for (int i = 0; i < layers.length(); i++) {
      final Layer layer = layers.get(i);
      if (nameExists(names, layer)) {
        return create(map, url, layer.getTitle(), props);
      }
    }
    if (LOGGER.isLoggable(Level.SEVERE)) {
      LOGGER.log(Level.SEVERE, "Layer could not be created (names doesn't exist?): " + names);
    }
    return null;
  }

  private static boolean nameExists(final String[] names, final Layer layer) {
    for (final String name : names) {
      if (name.equals(layer.getName())) {
        return true;
      }
    }
    return false;
  }

  private static MapLayer create(final MapLayoutPanel map, final String url, final String layerTitle, final LayerWMSProps props) {
    final String title = (props.getTitle() == null) || props.getTitle().isEmpty() ? layerTitle : props.getTitle();

    if ((props instanceof LayerAeriusMultiWMSProps) || (props instanceof LayerAeriusWMSProps)) {
      return new AeriusWMSMapLayer(title, map, url, props);
    } else {
      return new WMSMapLayer(title, map, url, props);
    }
  }
}
