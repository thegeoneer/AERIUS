/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.layer.TMS;
import org.gwtopenmaps.openlayers.client.layer.TMSOptions;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.geo.shared.LayerTMSProps;

/**
 * AERIUS extended TMS layer.
 */
public class TMSMapLayer extends TMS implements MapLayer {

  private final MapLayoutPanel map;

  private final LayerTMSProps li;
  private final LayerVisibility visibility;

  public TMSMapLayer(final MapLayoutPanel map, final LayerTMSProps li) {
    this(map, li, li.getTitle(), li.getBaseUrl(), defaultTMSOptions(li, li.getName(), map.getReceptorGridSettings().getEpsg().getEpsgCode()),
        new LayerVisibility());
  }

  private TMSMapLayer(final MapLayoutPanel map, final LayerTMSProps li, final String name, final String url, final TMSOptions tmsOptions,
      final LayerVisibility visibility) {
    super(name, url, tmsOptions);
    this.map = map;
    this.li = li;
    this.visibility = visibility;

    visibility.setLayer(this);
  }

  public static MapLayer create(final MapLayoutPanel map, final LayerTMSProps lp) {
    return new TMSMapLayer(map, lp);
  }

  public static TMSOptions defaultTMSOptions(final LayerTMSProps lp, final String layerName, final String epsgCode) {
    final TMSOptions tmsOptions = new TMSOptions();

    tmsOptions.setType(lp.getType());
    tmsOptions.getJSObject().setProperty("serviceVersion", lp.getServiceVersion() == null ? "" : lp.getServiceVersion());
    tmsOptions.getJSObject().setProperty("layername", layerName);
    tmsOptions.setVisibility(lp.isEnabled());
    tmsOptions.setProjection(epsgCode);
    if (lp.getMaxScale() > 0) {
      tmsOptions.setMaxScale(lp.getMaxScale());
    }
    tmsOptions.setMinScale(lp.getMinScale());
    tmsOptions.setAttribution(lp.getAttribution());
    return tmsOptions;
  }


  @Override
  public void attachLayer() {
    map.addLayerToMap(this);
  }

  @Override
  public void destroy() {
    getMap().removeLayer(this);
    super.destroy();
  }

  @Override
  public int getLayerIndex() {
    return getMap().getLayerIndex(this);
  }

  @Override
  public LayerProps getLayerProps() {
    return li;
  }

  @Override
  public Legend getLegend() {
    return li.getLegend();
  }

  @Override
  public Map getMap() {
    return map.getMap();
  }

  @Override
  public String getTitle() {
    return li.getTitle();
  }

  @Override
  public void setTitle(final String name) {
    li.setTitle(name);
  }

  @Override
  public String getNotVisibleReason() {
    return visibility.getNotVisibleReason();
  }

  @Override
  public LayerInZoom isLayerInZoom() {
    return visibility.isLayerInZoom();
  }

  @Override
  public void raiseLayer(final int delta) {
    getMap().raiseLayer(this, delta);
  }

  @Override
  public void refreshLayer() {
    map.setVisible(this, li.isEnabled());
  }

  /**
   * TMS layers ignores parameters.
   */
  public void setParameters(final String parameters) {
    // No-op.
  }

  @Override
  public boolean setVisible(final boolean isVisible) {
    return visibility.setIsVisible(isVisible);
  }

  @Override
  public void zoomToMinScale() {
    getMap().zoomToScale(li.getMinScale(), true);
  }

  @Override
  public void zoomToMaxScale() {
    getMap().zoomToScale(li.getMaxScale(), true);
  }
}
