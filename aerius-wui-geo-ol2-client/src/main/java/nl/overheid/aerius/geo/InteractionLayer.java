/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.event.MapClickListener;
import org.gwtopenmaps.openlayers.client.layer.Layer;

public interface InteractionLayer<L extends Layer> {

  /**
   * Add a {@link MapClickListener} to the {@link InteractionLayer}.
   *
   * @param listener Listener to add
   */
  void addMapClickListener(MapClickListener listener);

  /**
   * Returns the associated {@link Layer}.
   * 
   * TODO: If interaction layer is based on Control do we need the layer, since a Control also contains a layer???
   * 
   * @return The {@link Layer} of this {@link InteractionLayer}
   */
  L asLayer();

  /**
   * Activates the interaction layer.
   */
  void activate();

  /**
   * If this layer is currently the active interaction layer the layer will be
   * reset to the default interaction layer, if it's not the active
   * interaction layer nothing will be done.
   */
  void deactivate();

  /**
   * Deactivates the current interaction layer. If setDefault is true, the
   * default interaction layer will be set in place of current removed
   * interaction layer.
   *
   * @param setDefault if true sets the new active interaction layer to the default interaction layer
   * 
   */
  void deactivate(boolean setDefault);

  /**
   * Destroys the interaction layer. After this call the layer should be
   * initialized again before it can be used.
   */
  void destroy();
}
