/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import com.google.inject.Inject;

import nl.overheid.aerius.geo.shared.LayerProps.ColorRangesLegend;
import nl.overheid.aerius.wui.geo.LegendTitleFactory;
import nl.overheid.aerius.wui.main.context.AppContext;
import nl.overheid.aerius.wui.main.i18n.M;

public class SimpleLegendTitleFactory implements LegendTitleFactory {
  private final AppContext<?, ?> appContext;

  @Inject
  public SimpleLegendTitleFactory(final AppContext<?, ?> appContext) {
    this.appContext = appContext;
  }

  @Override
  public String getLegendTitle(final MapLayer layer) {
    if (!((layer.getLegend() instanceof ColorRangesLegend) && ((ColorRangesLegend) layer.getLegend()).isHexagon())) {
      return DEFAULT_TITLE;
    }

    return M.messages().unitDepositionSingularHaY(appContext.getUserContext().getEmissionResultValueDisplaySettings().getDisplayType());
  }
}
