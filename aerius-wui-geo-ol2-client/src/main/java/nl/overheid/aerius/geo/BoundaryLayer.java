/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;

import nl.overheid.aerius.wui.main.i18n.M;

/**
 * Layer to display the boundary for the calculator.
 */
public class BoundaryLayer extends AbstractVectorLayer {

  private static final String FILL_COLOR = "#ffffff";
  private static final double FILL_OPACITY = 0.5;
  private static final String BORDER_COLOR = "#000000";
  private static final double BORDER_OPACITY = 1.0;

  private static final Style LIMIT_STYLE = new Style();
  {
    LIMIT_STYLE.setFillColor(FILL_COLOR);
    LIMIT_STYLE.setFillOpacity(FILL_OPACITY);
    LIMIT_STYLE.setStroke(true);
    LIMIT_STYLE.setStrokeColor(BORDER_COLOR);
    LIMIT_STYLE.setStrokeDashstyle("dot");
    LIMIT_STYLE.setStrokeOpacity(BORDER_OPACITY);
  }

  private final String boundaryWKT;

  /**
   * Layer to display the boundary for the calculator.
   * @param map Map to use this layer with.
   * @param boundaryWKT The WKT for the boundary.
   */
  public BoundaryLayer(final MapLayoutPanel map, final String boundaryWKT) {
    super(M.messages().calculatorBoundaryLayerTitle(), map);
    this.boundaryWKT = boundaryWKT;
  }

  @Override
  public void attachLayer() {
    final VectorFeature vectorFeature = new VectorFeature(Geometry.fromWKT(boundaryWKT));
    vectorFeature.setStyle(LIMIT_STYLE);
    addFeature(vectorFeature);

    super.attachLayer();
  }

}
