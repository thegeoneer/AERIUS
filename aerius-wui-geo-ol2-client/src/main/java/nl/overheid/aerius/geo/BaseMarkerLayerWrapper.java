/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.util.HashSet;
import java.util.Set;

import org.gwtopenmaps.openlayers.client.Bounds;
import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.event.MapMoveEndListener;
import org.gwtopenmaps.openlayers.client.event.MapZoomListener;
import org.gwtopenmaps.openlayers.client.layer.Markers;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.ClusteredReceptorPoint;

/**
 * Baser Wrapper for marker layers.
 * @param <E> Type of point representing the marker
 */
public abstract class BaseMarkerLayerWrapper<E extends Point> {
  private static final int MIN_SCALE_TO_CLUSTER = 15000;
  private static final int SCALE_FACTOR = 60;

  protected final MapLayoutPanel map;

  // Source and CalculationPoint markers.
  private final Markers markersLayer;

  private Set<ClusteredReceptorPoint<E>> drawn = new HashSet<ClusteredReceptorPoint<E>>();

  protected double lastDrawnScale = Double.MAX_VALUE;

  private final MapMoveEndListener moveListener = new MapMoveEndListener() {
    @Override
    public void onMapMoveEnd(final MapMoveEndEvent eventObject) {
      redrawVisible();
    }
  };

  private final MapZoomListener zoomListener = new MapZoomListener() {
    @Override
    public void onMapZoom(final MapZoomEvent eventObject) {
      redraw(true);
    }
  };

  public BaseMarkerLayerWrapper(final MapLayoutPanel map, final String name) {
    this.map = map;
    markersLayer = new Markers(name);

    map.getMap().addMapZoomListener(zoomListener);
    map.getMap().addMapMoveEndListener(moveListener);
  }

  protected final void redraw(final boolean soft) {
    if (soft && Math.max(lastDrawnScale, map.getMap().getScale()) <= MIN_SCALE_TO_CLUSTER) {
      return;
    }

    markersLayer.clearMarkers();

    final double distanceForScale = getDistanceForScale(map.getMap().getScale());
    drawn = draw(createClusters(distanceForScale));

    lastDrawnScale = map.getMap().getScale();
  }

  /**
   * Draw the markers on the map in cluster points on the map.
   * @param clusterPoints
   * @return
   */
  protected HashSet<ClusteredReceptorPoint<E>> draw(final Set<ClusteredReceptorPoint<E>> clusterPoints) {
    final HashSet<ClusteredReceptorPoint<E>> drawn = new HashSet<ClusteredReceptorPoint<E>>();

    // Iterate over the clusters and add them
    for (final ClusteredReceptorPoint<E> p : clusterPoints) {
      // Redraws only that which is in the bounds
      final Bounds bounds = map.getMap().getExtent();
      if (bounds != null && bounds.getJSObject() != null && !isInBounds(p, bounds)) {
        continue;
      }

      drawn.add(p);

      final Marker marker = createMarker(p);

      markersLayer.addMarker(marker);
    }

    return drawn;
  }

  private boolean isInBounds(final Point p, final Bounds b) {
    return b.getLowerLeftX() <= p.getX() && b.getUpperRightX() >= p.getX() && b.getLowerLeftY() <= p.getY() && b.getUpperRightY() >= p.getY();
  }

  private void redrawVisible() {
    final double distanceForScale = getDistanceForScale(map.getMap().getScale());
    final Set<ClusteredReceptorPoint<E>> clusterPoints = createClusters(distanceForScale);

    // Filter clusters that have already been drawn.
    clusterPoints.removeAll(drawn);
    drawn.addAll(draw(clusterPoints));
  }

  protected abstract Marker createMarker(ClusteredReceptorPoint<E> p);

  protected abstract Set<ClusteredReceptorPoint<E>> createClusters(double scale);

  /**
   * Get the cluster distance for the given map scale.
   *
   * This used to be temporary guesswork, although it's been in here for months and it seems to be
   * working exactly perfect. However, should we ever want to do something intelligent with the values
   * here, these are the laws to follow:
   *
   * - It should always show the exact location when the map is below a specific scale level
   * - The cluster distance should be linear to / a function of the map's current scale level
   */
  protected double getDistanceForScale(final double scale) {
    if (scale < MIN_SCALE_TO_CLUSTER) {
      return 0;
    } else {
      return scale / SCALE_FACTOR;
    }
  }

  public void attach() {
    map.addLayer(markersLayer);
  }

  public void detach() {
    map.removeLayer(markersLayer);
  }
}
