/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo.util.development;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.client.events.LayerChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger;

public abstract class SuperNitroTurboGeoLogger extends SuperNitroTurboLogger {
  interface SuperTurboCalculatorBinder extends EventBinder<SuperNitroTurboGeoJuggernaut> {}

  public static class SuperNitroTurboGeoJuggernaut extends SuperNitroTurboJuggernaut {
    @EventHandler
    public void onEmissionSourceChange(final EmissionSourceChangeEvent e) {
      logger.log("Emission source changed (" + e.getChange() + "): " + e.getValue());
    }

    @EventHandler
    public void onLayerChange(final LayerChangeEvent e) {
      switch (e.getChange()) {
      case ADDED:
        logger.log("Layer added: " + e.getLayer().getTitle());
        break;
      case LEGEND_CHANGED:
        if (e.getLayer() != null) {
          logger.log("Layer legend changed: " + e.getLayer().getTitle() + " > " + parseSimpleClassName(e.getLayer().getLegend().getClass()));
        } else {
          logger.logWeirdness("Legend changed for layer that is null. (which could be considered weird)");
        }
        break;
      case VISIBLE:
        logger.log("Layer turned on: " + e.getLayer().getTitle());
        break;
      case HIDDEN:
        logger.log("Layer turned off: " + e.getLayer().getTitle());
        break;
      default:
        logger.verbose("Layer changed: " + e.getLayer().getTitle() + " - " + e.getChange());
        break;
      }
    }
  }

  public SuperNitroTurboGeoLogger(final EventBus eventBus, final SuperNitroTurboGeoJuggernaut juggernaut) {
    super(eventBus, juggernaut);
    final SuperTurboCalculatorBinder playground = GWT.create(SuperTurboCalculatorBinder.class);
    playground.bindEventHandlers(juggernaut, eventBus);
    log("SuperNitroTurboCalculatorJuggernaut attached.");
  }
}
