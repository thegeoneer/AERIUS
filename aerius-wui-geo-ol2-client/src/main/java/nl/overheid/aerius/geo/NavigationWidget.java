/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.control.Pan;
import org.gwtopenmaps.openlayers.client.control.Pan.Direction;
import org.gwtopenmaps.openlayers.client.control.ZoomIn;
import org.gwtopenmaps.openlayers.client.control.ZoomOut;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Navigation Widget.
 */
public class NavigationWidget extends Composite {

  private static NavigationWidgetUiBinder uiBinder = GWT
      .create(NavigationWidgetUiBinder.class);

  interface NavigationWidgetUiBinder extends
  UiBinder<Widget, NavigationWidget> {
  }

  interface GeoCSSResource extends CssResource {
    String panButtonNorth();
    String panButtonEast();
    String panButtonSouth();
    String panButtonWest();
  }

  @UiField Button panButton;
  @UiField PushButton zoomInButton;
  @UiField PushButton zoomOutButton;
  @UiField GeoImageResources img;
  @UiField GeoCSSResource style;

  private final Pan panNorth = new Pan(Pan.Direction.North);
  private final Pan panEast = new Pan(Pan.Direction.East);
  private final Pan panSouth = new Pan(Pan.Direction.South);
  private final Pan panWest = new Pan(Pan.Direction.West);
  private final ZoomIn zoomIn = new ZoomIn();
  private final ZoomOut zoomOut = new ZoomOut();

  public NavigationWidget(final MapLayoutPanel aeriusMap, final HelpPopupController hpC) {
    this(aeriusMap, (GeoImageResources) GWT.create(GeoImageResources.class), hpC);
  }

  public NavigationWidget(final MapLayoutPanel aeriusMap, final GeoImageResources img, final HelpPopupController hpC) {
    this.img = img;
    initWidget(uiBinder.createAndBindUi(this));
    final Map map = aeriusMap.getMap();

    map.addControl(panNorth);
    map.addControl(panEast);
    map.addControl(panSouth);
    map.addControl(panWest);
    map.addControl(zoomIn);
    map.addControl(zoomOut);

    zoomInButton.ensureDebugId(TestID.BUTTON_ZOOM_IN);
    zoomOutButton.ensureDebugId(TestID.BUTTON_ZOOM_OUT);
    panButton.ensureDebugId(TestID.BUTTON_PAN);

    hpC.addWidget(panButton, hpC.tt().ttNavigateControle());
    hpC.addWidget(zoomInButton, hpC.tt().ttNavigateControle());
    hpC.addWidget(zoomOutButton, hpC.tt().ttNavigateControle());

  }

  @UiHandler("panButton")
  void onMouseMovePanButton(final MouseMoveEvent e) {
    final Direction direction = panPosition(e.getNativeEvent());
    switch (direction) {
    case North:
      panButton.setStyleName(style.panButtonNorth());
      break;
    case East:
      panButton.setStyleName(style.panButtonEast());
      break;
    case South:
      panButton.setStyleName(style.panButtonSouth());
      break;
    case West:
      panButton.setStyleName(style.panButtonWest());
      break;
    default:
      throw new IllegalStateException("This direction (" + direction + ") insn't supported by onMouseMovePanButton.");
    }
  }

  @UiHandler("panButton")
  void onClickPanButton(final ClickEvent e) {
    final Direction direction = panPosition(e.getNativeEvent());
    switch (direction) {
    case North:
      panNorth.trigger();
      break;
    case South:
      panSouth.trigger();
      break;
    case East:
      panEast.trigger();
      break;
    case West:
      panWest.trigger();
      break;
    default:
      throw new IllegalStateException("This direction (" + direction + ") insn't supported by onClickPanButton.");
    }
  }

  @UiHandler("zoomInButton")
  void onClickZoomIn(final ClickEvent e) {
    zoomIn.trigger();
  }

  @UiHandler("zoomOutButton")
  void onClickZoomOut(final ClickEvent e) {
    zoomOut.trigger();
  }

  private Pan.Direction panPosition(final NativeEvent event) {
    final float x = event.getClientX() - (panButton.getAbsoluteLeft() + panButton.getOffsetWidth() / 2);
    final float y = event.getClientY() - (panButton.getAbsoluteTop() + panButton.getOffsetHeight() / 2);

    return Math.abs(x) < Math.abs(y) ? y < 0 ? Pan.Direction.North : Pan.Direction.South
        : x < 0 ? Pan.Direction.West : Pan.Direction.East;
  }
}
