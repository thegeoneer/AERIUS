/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.control.ModifyFeature;
import org.gwtopenmaps.openlayers.client.control.ModifyFeature.OnModificationListener;
import org.gwtopenmaps.openlayers.client.control.ModifyFeatureOptions;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.handler.Handler;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.util.JSObject;

import com.google.gwt.user.client.DOM;

/**
 * Interaction action layer to modify a vector feature.
 */
public class ModifyFeatureInteractionLayer extends AbstractInteractionLayer<Vector> {

  private final ModifyFeature modifyFeature;

  public ModifyFeatureInteractionLayer(final MapLayoutPanel map, final Handler handler, final JSObject styleMap,
      final OnModificationListener listener) {
    this(map, new Vector("ModifyFeature" + DOM.createUniqueId()), handler, styleMap, listener);
  }

  public ModifyFeatureInteractionLayer(final MapLayoutPanel map, final Vector layer, final Handler handler, final JSObject styleMap,
      final OnModificationListener listener) {
    super(map, layer);
    final ModifyFeatureOptions mfo = new ModifyFeatureOptions();
    if (listener != null) {
      mfo.onModification(listener);
    }
    mfo.setStandalone(true);
    modifyFeature = new ModifyFeature(asLayer(), mfo);
    modifyFeature.setMode(ModifyFeature.RESHAPE);
  }

  @Override
  protected void preActivate() {
    getMap().addControl(modifyFeature);
    modifyFeature.activate();
  }

  @Override
  protected void preDeactivate() {
    getMap().removeControl(modifyFeature);
    modifyFeature.deactivate();
  }

  @Override
  public void destroy() {
    deactivate();
    asLayer().destroyFeatures();

  }

  /**
   * Sets the feature on the layer to be modified and activates the layer.
   * @param feature feature to modify
   */
  public void modifyFeature(final VectorFeature feature) {
    activate();
    asLayer().addFeature(feature);
    modifyFeature.selectFeature(feature);
  }

  public void selectFeature(final VectorFeature feature) {
    modifyFeature.selectFeature(feature);
  }
}

