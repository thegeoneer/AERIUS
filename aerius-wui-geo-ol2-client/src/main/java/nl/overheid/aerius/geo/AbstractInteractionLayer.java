/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.util.ArrayList;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.event.MapClickListener;
import org.gwtopenmaps.openlayers.client.layer.Layer;

/**
 * Default implementation of the {@link InteractionLayer}. Interaction layers
 * should use this as base class.
 *
 * @param <L>
 */
public abstract class AbstractInteractionLayer<L extends Layer> implements InteractionLayer<L> {

  private final L layer;
  private final MapLayoutPanel map;
  private final ArrayList<MapClickListener> listeners = new ArrayList<MapClickListener>();
  private boolean activated;

  protected AbstractInteractionLayer(final MapLayoutPanel map, final L layer) {
    this.map = map;
    this.layer = layer;
  }

  @Override
  public L asLayer() {
    return layer;
  }

  @Override
  public final void activate() {
    if (!activated) {
      map.setInteractionLayer(this);
      for (final MapClickListener l : listeners) {
        getMap().addMapClickListener(l);
      }
      preActivate();
      activated = true;
    }
  }

  @Override
  public void addMapClickListener(final MapClickListener listener) {
    listeners.add(listener);
    if (activated) {
      getMap().addMapClickListener(listener);
    }
  }

  @Override
  public final void deactivate(final boolean setDefault) {
    if (activated) {
      for (final MapClickListener l : listeners) {
        getMap().removeListener(l);
      }
      preDeactivate();
      map.removeLayer(asLayer());
      map.resetInteractionLayer(setDefault);
      activated = false;
    }
  }

  @Override
  public final void deactivate() {
    deactivate(true);
  }

  /**
   * This method is called prior to activation of the layer. This subclass
   * should implement any actions for the layer in this method, also because
   * {@link #activate()} is final.
   */
  protected abstract void preActivate();

  /**
   * This method is called prior to deactivation of the layer. This subclass
   * should implement any actions for the layer in this method, also because
   * {@link #deactivate()} is final.
   */
  protected abstract void preDeactivate();

  /**
   * Returns the {@link MapLayoutPanel} associated with this layer.
   * 
   * @return
   */
  protected MapLayoutPanel getMapWidget() {
    return map;
  }

  /**
   * Returns the {@link Map} associated with this layer.
   *
   * @return
   */
  protected Map getMap() {
    return map.getMap();
  }
}
