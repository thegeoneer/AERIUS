/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.client.events.LayerChangeEvent;
import nl.overheid.aerius.geo.client.events.LayerChangeEvent.CHANGE;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;

public class AeriusWMSMapLayer extends WMSMapLayer {
  private static final String YEAR_REPLACEMENT_TAG = "[jaar]";
  private int year;
  private EventBus eventBus;

  interface WMSLayerEventBinder extends EventBinder<AeriusWMSMapLayer> {}

  protected AeriusWMSMapLayer(final String title, final MapLayoutPanel map, final String url, final LayerWMSProps props) {
    this(title, map, url, props, new LayerVisibility());
  }

  protected AeriusWMSMapLayer(final String title, final MapLayoutPanel map, final String url, final LayerWMSProps props,
      final LayerVisibility visibility) {
    super(title, map, url, props, visibility);
  }

  protected static AeriusWMSMapLayer create(final String title, final MapLayoutPanel map, final String url, final LayerWMSProps props) {
    return new AeriusWMSMapLayer(title, map, url, props);
  }

  @EventHandler
  public void onYearChange(final YearChangeEvent event) {
    setYear(event.getValue());
    // Fire a name change event
    eventBus.fireEvent(new LayerChangeEvent(this, CHANGE.NAME));

    // Redraw the layer, applying the changes
    redraw();
  }

  public void setYear(final int year) {
    this.year = year;

    // Update the params
    ((LayerWMSProps) getLayerProps()).setParamFilter("year", year);
  }

  @Override
  public String getTitle() {
    return super.getTitle() == null ? M.messages().noLayerName() : super.getTitle().replace(YEAR_REPLACEMENT_TAG, String.valueOf(year));
  }

  public void setEventBus(final EventBus eventBus) {
    this.eventBus = eventBus;
  }
}
