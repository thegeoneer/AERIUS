/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.geometry.Polygon;

/**
 * Util class with methods to check if a {@link LineString} or {@link Polygon}
 * are self intersecting.
 */
public final class SelfIntersect {

  private SelfIntersect() {
    // unil class.
  }

  /**
   * Checks if the the polygon self intersects.
   *
   * @param polygon polygon to check
   * @return true if the polygon self intersects
   */
  public static boolean checkSelfIntersection(final Polygon polygon) {
    // take component 0, which is the outer polygon.
    return checkSelfIntersection(polygon.getComponents()[0]);
  }

  /**
   * Checks if the line self intersects.
   *
   * @param line line to check
   * @return true if the line self intersects
   */
  public static boolean checkSelfIntersection(final LineString line) {
    final LineString[] segments = new LineString[line.getNumberOfComponents() - 1];
    final Point[] components = line.getComponents();
    for (int i = 0; i < segments.length; i++) {
      final Point[] points = new Point[2];
      points[0] = components[i];
      points[1] = components[i + 1];
      segments[i] = new LineString(points);
    }
    for (int out = 0; out < segments.length; out++) {
      for (int in = 0; in < segments.length; in++) {
        if (out != in && !connectedLines(segments[out], segments[in]) && segments[out].intersects(segments[in])) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Checks if 2 lines are connected, i.e. if their end points overlap.
   * @return true if the lines are connected.
   */
  private static boolean connectedLines(final LineString one, final LineString two) {
    final Point one0 = Point.narrowToPoint(one.getComponent(0));
    final Point one1 = Point.narrowToPoint(one.getComponent(1));
    final Point two0 = Point.narrowToPoint(two.getComponent(0));
    final Point two1 = Point.narrowToPoint(two.getComponent(1));
    return one0.equals(two0) || one0.equals(two1)
        || one1.equals(two0) || one1.equals(two1);
  }
}
