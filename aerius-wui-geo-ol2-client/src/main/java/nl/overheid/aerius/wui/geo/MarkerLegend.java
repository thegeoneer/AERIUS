/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;


/**
 * Marker legend with no implementation whatsoever.
 *
 * Note the double meaning of 'marker'; legend meant for markers, and a marker class
 *
 */
public class MarkerLegend extends ImagesLegend {

  public MarkerLegend() {
    super(M.messages().legendDepositionMarkerText());
  }

  @Override
  protected void addImages(final FlowPanel panel) {
    panel.add(getImageLegend(new Image(R.images().highestCalculatedDepositionMarkerSmall()),
        M.messages().legendDepositionHighestCalculatedMarkerText()));

    panel.add(getImageLegend(new Image(R.images().highestTotalDepositionMarkerSmall()),
        M.messages().legendDepositionHighestTotalMarkerText()));
  }
}
