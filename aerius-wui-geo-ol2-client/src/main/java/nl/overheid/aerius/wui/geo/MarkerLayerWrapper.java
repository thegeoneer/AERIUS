/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.layer.Markers;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import nl.overheid.aerius.geo.BaseMarkerLayerWrapper;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ClusteredReceptorPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.info.InformationReceptorPoint;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.util.ClusterUtil;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.shared.util.FormatUtil;
import nl.overheid.aerius.wui.geo.MarkerItem.TYPE;
import nl.overheid.aerius.wui.main.event.CalculationPointChangeEvent;
import nl.overheid.aerius.wui.main.event.CalculationPointsPurgeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourcesPurgeEvent;
import nl.overheid.aerius.wui.main.event.InformationPointChangeEvent;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Markers in this layer must not be and are not mutably exposed publicly.
 *
 * The {@link MarkerLayerWrapper} manages the markers that are displayed on the map.
 * It listens to various events that will be fired when objects that are displayed
 * are manipulated, changing the marker representation for those objects accordingly.
 */
public class MarkerLayerWrapper extends BaseMarkerLayerWrapper<MarkerItem<?>> {

  public interface MarkerContainer<E extends HasId> {
    /**
     * Clear all data.
     */
    void clear();

    /**
     * @param vectorLayer
     * @param markerLayer
     */
    void init(Vector vectorLayer, Markers markerLayer);

    /**
     * @param src
     */
    void add(EmissionSource src);

    /**
     * @param src
     */
    void remove(EmissionSource src);

    /**
     * @param maritimeRoutes
     */
    void setMaritimeRoutes(List<ShippingRoute> maritimeRoutes);
  }

  /**
   * Inner event that represents a change in the visibility of a specific {@link TYPE} of markers.
   */
  public static class LabelVisibilityChangeEvent extends GenericEvent {
    private final boolean visible;
    private final TYPE type;

    /**
     * Create the {@link LabelVisibilityChangeEvent} with the given visibility boolean for the given
     * marker {@link TYPE}.
     *
     * @param visible True for visible, false for invisible.
     * @param type Type to change.
     */
    public LabelVisibilityChangeEvent(final boolean visible, final MarkerItem.TYPE type) {
      this.visible = visible;
      this.type = type;
    }

    public boolean isVisible() {
      return visible;
    }

    public TYPE getType() {
      return type;
    }
  }

  interface MarkerLayerEventBinder extends EventBinder<MarkerLayerWrapper> { }

  private final MarkerLayerEventBinder eventBinder = GWT.create(MarkerLayerEventBinder.class);

  // TODO The information layer should not in the MarkerLayerWrapper but in a separate class.
  // Information hexagon *
  private final InformationHexagonLayer informationLayer;
  // Source geometries *
  private final SourceVectorLayer vectorLayer;
  private final ArrayList<MarkerItem<?>> markerItems = new ArrayList<MarkerItem<?>>();
  private final HashMap<MarkerItem.TYPE, Boolean> labelModes = new HashMap<MarkerItem.TYPE, Boolean>();

  private InfoMarker infoMarker;

  /**
   * Create the {@link MarkerLayerWrapper} for the given {@link MapLayoutPanel}, with the given name.
   *
   * The ShippingRouteContainer represents a wrapper for a {@link Vector} layer that displays
   * shipping routes. That container will use the {@link MarkerLayerWrapper}'s vector and marker layers
   * to draw its route specific markers on.
   *
   * @param map {@link MapLayoutPanel} markers will be drawn on.
   * @param name Name for this layer.
   */
  public MarkerLayerWrapper(final MapLayoutPanel map, final String name) {
    super(map, name);
    vectorLayer = map instanceof SourceMapPanel ? new SourceVectorLayer(name, (SourceMapPanel) map) : null;
    final ReceptorGridSettings rgs = map.getReceptorGridSettings();
    informationLayer = new InformationHexagonLayer("info", rgs.getZoomLevel1(), new ReceptorUtil(rgs));
    eventBinder.bindEventHandlers(this, map.getEventBus());
  }

  /**
   * Attaches the layers in the class to the map. Call when map is initialized.
   */
  @Override
  public void attach() {
    map.addLayer(informationLayer);
    if (vectorLayer != null) {
      map.addLayer(vectorLayer.asLayer());
      map.addLayer(vectorLayer.asMarkerLayer());
    }
    super.attach();
  }

  @Override
  public void detach() {
    map.removeLayer(informationLayer);
    if (vectorLayer != null) {
      map.removeLayer(vectorLayer.asLayer());
      map.removeLayer(vectorLayer.asMarkerLayer());
    }
    super.detach();
  }

  @EventHandler
  void onEmissionSourceChange(final EmissionSourceChangeEvent event) {
    switch (event.getChange()) {
    case ADD:
      addSourceMarker(event.getListId(), event.getValue(), event.isSelected());
      break;
    case REMOVE:
      removeSourceMarker(event.getListId(), event.getValue(), event.isSelected());
      break;
    case UPDATE:
      setSourceMarkerColor(event.getValue());
      break;
    default:
      // No-op
    }

    // Hard redraw
    redraw(false);
  }

  @EventHandler
  void onCalculationPointChange(final CalculationPointChangeEvent event) {
    switch (event.getChange()) {
    case ADD:
      addCalculationPointMarker(event.getValue());
      break;
    case REMOVE:
      removeMarker(event.getValue(), TYPE.CALCULATION_POINT);
      break;
    case UPDATE:
      break;
    default:
      // No-op
    }
    redraw(false);
  }

  @EventHandler
  void onLocationChangeEvent(final LocationChangeEvent event) {
    // Do a hard redraw when the location changed programmatically, do it deferred because we need to complete the event chain first, and redraw afterward
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        redraw(false);
      }
    });
  }

  @EventHandler
  void purgeAllCalculationPoints(final CalculationPointsPurgeEvent e) {
    purge(TYPE.CALCULATION_POINT, -1); // No valid eslId means, remove markers only
  }

  @EventHandler
  void purgeAllEmissionSources(final EmissionSourcesPurgeEvent e) {
    purge(TYPE.EMISSION_SOURCE, e.getEslId());
    purge(TYPE.EMISSION_SOURCE_SELECTED, e.getEslId());
  }

  private void purge(final TYPE type, final int emissionSourceListId) {
    for (int i = markerItems.size() - 1; i >= 0; i--) {
      if (markerItems.get(i).getType() == type) {
        if ((type == TYPE.EMISSION_SOURCE) && (vectorLayer != null)) {
          vectorLayer.remove(emissionSourceListId, (EmissionSource) markerItems.get(i).getObject());
        }
        markerItems.remove(i);
      }
    }
    redraw(false);
  }

  @EventHandler
  void onInformationPointChange(final InformationPointChangeEvent event) {
    setInfoMarker(event.getValue());
  }

  @EventHandler
  void onLabelVisibilityChange(final LabelVisibilityChangeEvent event) {
    enableMarkerLabels(event.isVisible(), event.getType());
  }

  /**
   * Removes a marker associated with the object.
   * @param obj object associated with the marker
   * @param type type of the object
   */
  private <M extends Point & HasId> boolean removeMarker(final M obj, final TYPE type) {
    return markerItems.remove(findMarkerItem(obj, type));
  }

  /**
   * Sets the markers (sources and calculation points).
   *
   * @param sources sources for a single situation
   * @param calculationPoints the calculationPoints
   */
  public void setMarkers(final EmissionSourceList sources, final CalculationPointList calculationPoints) {
    markerItems.clear();
    if (vectorLayer != null) {
      vectorLayer.clear();
    }

    if (sources != null) {
      if (map instanceof SourceMapPanel) {
        ((SourceMapPanel) map).getMartimeMooringRouteContainer().setMaritimeRoutes(sources.getMaritimeRoutes());
      }
      for (final EmissionSource source : sources) {
        addSourceMarker(sources.getId(), source, false);
      }
    }
    setAdditionalMarkers(calculationPoints);
    redraw(false);
  }

  /**
   * Clear the markers and redraw the layer.
   */
  public void clear() {
    markerItems.clear();
    if (vectorLayer != null) {
      vectorLayer.clear();
    }

    redraw(false);
  }

  /**
   * Sets the markers (situations and calculation points).
   *
   * @param situations sources for all situations. As for right now, we will - by contract - assume
   * there are 2 situations, respectively indexed to 0 and 1 in the given List. If this behavior
   * is to ever change, this implementation will crash if not adapted.
   * @param calculationPoints the calculationPoints
   */
  protected void setMarkers(final ArrayList<EmissionSourceList> situations, final CalculationPointList calculationPoints) {
    markerItems.clear();
    markerItems.addAll(ComparisonMarkerCreator.getComparisonMarkers(labelModes, situations));

    if (vectorLayer != null) {
      vectorLayer.clear();
      for (final EmissionSourceList sources : situations) {
        if (sources != null) {
          for (final EmissionSource source : sources) {
            vectorLayer.add(sources.getId(), source);
          }
        }
      }
    }
    setAdditionalMarkers(calculationPoints);
    redraw(false);
  }

  /**
   * Set the visibility of labels for the given type.
   *
   * @param enable make visible if true, else hide
   * @param type type of marker to show labels for
   */
  private void enableMarkerLabels(final boolean enable, final TYPE type) {
    labelModes.put(type, enable);

    // Update marker items
    for (final MarkerItem<?> item : markerItems) {
      if ((item.getType() == type) && (item instanceof LabelledMarkerItem)) {
        ((LabelledMarkerItem<?>) item).setLabelType(enable);
      }
    }
    redraw(false);
  }

  /**
   * Recalculates the clustering and redraws the markers on the map when soft is true and the map is at a scale that requires redrawing.
   */
  @Override
  protected final Set<ClusteredReceptorPoint<MarkerItem<?>>> createClusters(final double scale) {
    final ArrayList<MarkerItem<?>> filtered = filterMarkerItems(markerItems);

    // Cluster the marker items using an util
    return ClusterUtil.clusterGenericPoints(filtered, scale);
  }

  protected ArrayList<MarkerItem<?>> filterMarkerItems(final ArrayList<MarkerItem<?>> markerItems) {
    // Don't filter by default.
    return markerItems;
  }

  @Override
  protected Marker createMarker(final ClusteredReceptorPoint<MarkerItem<?>> p) {
    final AeriusMarker marker = new AeriusMarker(p);
    marker.setMarkerItems(p.getPoints());

    return marker.getMarker();
  }

  private void addCalculationPointMarker(final AeriusPoint point) {
    final LabelledMarkerItem<AeriusPoint> item =
        addMarker(point, FormatUtil.formatAlphabetical(point.getId()), point.getLabel(), TYPE.CALCULATION_POINT);
    item.setMarkerItemColor(R.css().colorCalculationPoint());
    setOpacityOppositeMarker(point.getId(), TYPE.CALCULATION_POINT, true);
  }

  private void setInfoMarker(final InformationReceptorPoint point) {
    if (infoMarker == null) {
      infoMarker = new InfoMarker(point);
      markerItems.add(infoMarker);
    } else {
      infoMarker.setX(point.getX());
      infoMarker.setY(point.getY());
    }
    informationLayer.drawReceptor(point);
    informationLayer.redraw();

    redraw(false);
  }

  private void addSourceMarker(final int eslId, final EmissionSource source, final boolean selectedSource) {
    final LabelledMarkerItem<EmissionSource> marker =
        addMarker(source, source.getLabel(), selectedSource ? TYPE.EMISSION_SOURCE_SELECTED : TYPE.EMISSION_SOURCE);

    setOpacityOppositeMarker(source.getId(), selectedSource ? TYPE.EMISSION_SOURCE_SELECTED : TYPE.EMISSION_SOURCE, true);
    setSourceMarkerColor(marker, source);
    if (vectorLayer != null) {
      vectorLayer.add(eslId, source);
    }
  }

  private void removeSourceMarker(final int eslId, final EmissionSource source, final boolean selectedSource) {
    removeMarker(source, selectedSource ? TYPE.EMISSION_SOURCE_SELECTED : TYPE.EMISSION_SOURCE);
    setOpacityOppositeMarker(source.getId(), selectedSource ? TYPE.EMISSION_SOURCE_SELECTED : TYPE.EMISSION_SOURCE, false);
    if (vectorLayer != null) {
      vectorLayer.remove(eslId, source);
    }
  }

  private void setSourceMarkerColor(final LabelledMarkerItem<EmissionSource> marker, final EmissionSource source) {
    if (marker != null) {
      final Sector definedSS = source.getSector() == null ? Sector.SECTOR_UNDEFINED : source.getSector();
      marker.setMarkerItemColor(ColorUtil.webColor(definedSS.getProperties().getColor()));
    }
  }

  @SuppressWarnings("unchecked")
  private void setSourceMarkerColor(final EmissionSource source) {
    final LabelledMarkerItem<EmissionSource> marker = (LabelledMarkerItem<EmissionSource>) findMarkerItem(source, TYPE.EMISSION_SOURCE);
    setSourceMarkerColor(marker, source);
  }

  private <M extends HasId> void setOpacityOppositeMarker(final int id, final TYPE type, final boolean opacity) {
    if (id < 0) {
      final MarkerItem<?> opposite = findMarkerItem(-id, type);

      if (opposite instanceof LabelledMarkerItem) {
        ((LabelledMarkerItem<?>) opposite).setMarkerItemOpacity(opacity);
      }
    }
  }

  private <M extends Point & HasId, T extends MarkerItem<M>> T addMarker(final M obj, final String label, final TYPE type) {
    final int id = obj.getId();
    return addMarker(obj, id == 0 ? "" : String.valueOf(Math.abs(id)), label, type);
  }

  /**
   * Adds a marker to the layer, returns the created MarkerItem
   *
   * @param obj object that's both a Point and HasId
   * @param markerLabel the name
   * @param markerLabel the name
   * @param type the TYPE of marker to add
   * @param <M>
   *
   * @return a unique ID identifying the marker
   *
   */
  @SuppressWarnings("unchecked")
  private <M extends Point & HasId, T extends MarkerItem<M>> T addMarker(final M obj, final String markerName,
      final String markerLabel, final TYPE type) {
    MarkerItem<?> item = findMarkerItem(obj, type);

    // If the item does not exist yet, add a new one, else update
    if (item == null) {
      item = new LabelledMarkerItem<M>(obj, obj, markerName, markerLabel, type);
      ((LabelledMarkerItem<M>) item).setLabelType(labelModes.containsKey(type) ? labelModes.get(type) : false);
      markerItems.add(item);
    } else {
      item.setX(obj.getX());
      item.setY(obj.getY());
    }

    return (T) item;
  }

  private <M extends HasId> MarkerItem<?> findMarkerItem(final M obj, final TYPE type) {
    return findMarkerItem(obj.getId(), type);
  }

  private MarkerItem<?> findMarkerItem(final int id, final TYPE type) {
    for (final MarkerItem<? extends HasId> item : markerItems) {
      if ((item.getId() == id) && (item.getType() == type)) {
        return item;
      }
    }
    return null;
  }

  private void setAdditionalMarkers(final CalculationPointList calculationPoints) {
    if (calculationPoints == null) {
      return;
    }

    for (final AeriusPoint point : calculationPoints) {
      addCalculationPointMarker(point);
    }

    if (infoMarker != null) {
      markerItems.add(infoMarker);
    }
  }
}
