/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.user.client.Timer;

import nl.overheid.aerius.geo.LayerPreparationUtil;
import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.TextLegend;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.geo.shared.MultiLayerProps;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.main.event.HabitatTypeEvent.HabitatDisplayType;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;

public abstract class HabitatTypeLayer {
  private static final int REDRAW_DELAY = 100;

  private static final String HABITAT_TYPE_ID = "habitatTypeId";

  private static final int DOESNT_EXIST_HOPEFULLY = 9999998;
  private static final float DEFAULT_OPACITY = 1f;
  private static final int LAYER_MAX_SCALE = 188000;

  private final LayerWMSProps habitatHoverLayerProps;
  private final LayerWMSProps habitatPersistentLayerProps;

  private final MultiLayerProps multiProps;
  private MapLayer layer;

  private MapLayoutPanel map;

  private final HashSet<Integer> currentDisplayTypeIds = new HashSet<Integer>();
  private final HashSet<Integer> currentHoverTypeIds = new HashSet<Integer>();

  private final Timer redrawTimer = new Timer() {
    @Override
    public void run() {
      map.setVisible(layer, true);
      layer.redraw();
    }
  };

  private final FetchGeometryServiceAsync service;

  public HabitatTypeLayer(final FetchGeometryServiceAsync service, final LayerWMSProps layerProps) {
    this.service = service;
    habitatHoverLayerProps = layerProps.shallowCopy();
    habitatPersistentLayerProps = layerProps.shallowCopy();

    multiProps = new MultiLayerProps();
    multiProps.setEnabled(true);
    multiProps.setMinScale(LAYER_MAX_SCALE);
    multiProps.setOpacity(DEFAULT_OPACITY);
    multiProps.setLegend(new TextLegend(M.messages().layerHabitatLegend()));

    multiProps.getLayers().add(habitatHoverLayerProps);
    multiProps.getLayers().add(habitatPersistentLayerProps);

    LayerPreparationUtil.prepareLayer(service, multiProps);
  }

  public void setMap(final MapLayoutPanel map) {
    this.map = map;
  }

  public void setVisible(final boolean visible) {
    if (layer != null) {
      map.setVisible(layer, visible);
    }
  }

  public void hoverHabitats(final int eventId, final Set<Integer> habitatTypeIds, final HabitatDisplayType habitatDisplayType) {
    consolidateHabitatTypeIds(currentHoverTypeIds, habitatTypeIds, habitatDisplayType);

    configureLayer(habitatHoverLayerProps, eventId, currentHoverTypeIds, habitatDisplayType);

    displayLayer();
  }

  public void displayHabitats(final int eventId, final Set<Integer> habitatTypeIds, final HabitatDisplayType habitatDisplayType) {
    consolidateHabitatTypeIds(currentDisplayTypeIds, habitatTypeIds, habitatDisplayType);

    configureLayer(habitatPersistentLayerProps, eventId, currentDisplayTypeIds, habitatDisplayType);

    displayLayer();
  }

  private void displayLayer() {
    if (layer == null) {
      LayerPreparationUtil.prepareLayer(service, multiProps, new AppAsyncCallback<LayerProps>() {
        @Override
        public void onSuccess(final LayerProps result) {
          layer = map.addLayer(multiProps);
        }
      });
    } else {
      redraw();
    }
  }

  private void redraw() {
    if (redrawTimer.isRunning()) {
      redrawTimer.cancel();
    }

    redrawTimer.schedule(REDRAW_DELAY);
  }

  public void clear() {
    redrawTimer.cancel();
    removeLayer();
  }

  private void removeLayer() {
    if (layer != null) {
      map.removeLayer(layer);
      layer = null;
    }
  }

  private void configureLayer(final LayerWMSProps props, final int eventId, final HashSet<Integer> habitatTypeIds, final HabitatDisplayType habitatDisplayType) {
    final HashMap<String, Object> filter = formatHabitatParameters(eventId, habitatTypeIds, habitatDisplayType);

    props.setParamFilters(filter);
    props.setEnabled(true);
    props.setOpacity(DEFAULT_OPACITY);
  }

  private HashMap<String, Object> formatHabitatParameters(final int eventId, final HashSet<Integer> habitatTypeIds, final HabitatDisplayType habitatDisplayType) {
    final String habitatParams = getHabitatTypeIdsViewParam(habitatTypeIds);

    final HashMap<String, Object> filter = new HashMap<String, Object>();
    filter.put(HABITAT_TYPE_ID, habitatParams);

    formatParameters(filter, eventId);

    return filter;
  }

  private void consolidateHabitatTypeIds(final Set<Integer> currentHabitatTypeIds, final Set<Integer> habitatTypeIds, final HabitatDisplayType habitatDisplayType) {
    switch (habitatDisplayType) {
    case ADD:
      currentHabitatTypeIds.addAll(habitatTypeIds);
      break;
    case REPLACE:
      currentHabitatTypeIds.clear();
      currentHabitatTypeIds.addAll(habitatTypeIds);
      break;
    case REMOVE:
      currentHabitatTypeIds.removeAll(habitatTypeIds);
      break;
    default:
      // No-op
    }

    if (currentHabitatTypeIds.isEmpty()) {
      currentHabitatTypeIds.add(DOESNT_EXIST_HOPEFULLY);
    }
  }

  protected abstract void formatParameters(final HashMap<String, Object> filter, final int eventId);

  private String getHabitatTypeIdsViewParam(final HashSet<Integer> habitatTypeIds) {
    final JsArrayString jsa = JavaScriptObject.createArray().cast();
    for (final Integer filter : habitatTypeIds) {
      jsa.push(String.valueOf(filter));
    }

    // Need to escape the comma because otherwise GeoServer will think it's a viewparam for another layer
    // http://osgeo-org.1560.x6.nabble.com/org-geoserver-platform-ServiceException-1-feature-types-requested-but-found-2-view-params-specified-td3790402.html
    return jsa.join("\\,");
  }

  public void removeHoverLayer(final int eventId) {
    hoverHabitats(eventId, new HashSet<Integer>(), HabitatDisplayType.REPLACE);
  }

  public void removePersistentLayer(final int eventId) {
    displayHabitats(eventId, new HashSet<Integer>(), HabitatDisplayType.REPLACE);
  }
}
