/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Image;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Marker displaying the information marker.
 */
@SuppressWarnings("serial")
public class InfoMarker extends MarkerItem<HasId> {
  // No-op implementation of HasId, returning 0 as the id and doing nothing on setId
  private static final HasId NO_RELEVANT_ID = new HasId() {
    @Override
    public void setId(final int id) {
      // Do nothing.
    }

    @Override
    public int getId() {
      return 0;
    }
  };

  /**
   * Create an {@link InfoMarker} at the given point.
   *
   * @param point Point to put the {@link InfoMarker} on.
   */
  public InfoMarker(final Point point) {
    super(point, NO_RELEVANT_ID, TYPE.INFO);
    final Image i = new Image(R.images().informationMarker());
    getElement().appendChild(i.getElement());
    getElement().getStyle().setPosition(Position.ABSOLUTE);
    getElement().getStyle().setTop(-i.getHeight(), Unit.PX);
    getElement().getStyle().setLeft(-i.getWidth() / 2.0, Unit.PX);
  }
}
