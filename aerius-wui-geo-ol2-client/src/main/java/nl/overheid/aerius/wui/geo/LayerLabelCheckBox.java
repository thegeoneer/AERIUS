/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.geo.LabeledMultiMapLayer;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.LayerPanelItemImpl.LayerItemChangeHandler;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

public class LayerLabelCheckBox extends Composite {
  private static LayerLabelCheckBoxUiBinder uiBinder = GWT.create(LayerLabelCheckBoxUiBinder.class);

  interface LayerLabelCheckBoxUiBinder extends UiBinder<Widget, LayerLabelCheckBox> {
  }

  @UiField CheckBox checkBox;

  public LayerLabelCheckBox(final LabeledMultiMapLayer layer, final LayerItemChangeHandler changeHandler, final HelpPopupController hpC) {

    initWidget(uiBinder.createAndBindUi(this));
    checkBox.setValue(Boolean.TRUE);
   
    checkBox.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        changeHandler.changeLabelEnabled(layer, checkBox.getValue());
      }
    });
    
    checkBox.ensureDebugId("label_" + TestID.INPUT_LAYERPANEL_CHECKBOX + "-" + layer.getTitle());
  }

  public void setSelected(final boolean selected) {
    checkBox.setValue(selected);
  }
}
