/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.util.JSObject;

/**
 * Util class to get the style for drawing source objects on the map.
 */
public final class DrawSourceStyleOptions {

  private DrawSourceStyleOptions() {
  }

  /**
   * Layer options are set in JavaScript due to incomplete implementation of
   * the GWT-OpenLayers library and huge amount to add support.
   *
   * @return OpenLayers styleMap JavaScript object
   */
  public static native JSObject getElasticStyleMap() /*-{
    var linemeasureStyles = {
      "Point" : {
        pointRadius : 6,
        graphicName : "circle",
        fillColor : "transparent",
        fillOpacity : 0,
        strokeWidth : 2,
        strokeOpacity : 1,
        strokeColor : "black"
      },
      "Line" : {
        strokeColor : "#114289",
        strokeWidth : 3,
        strokeLinecap : "square",
        strokeDashstyle : "dot"
      }
    };
    var lineStyle = new $wnd.OpenLayers.Style();
    lineStyle.addRules([ new $wnd.OpenLayers.Rule({
      symbolizer : linemeasureStyles
    }) ]);
    var linemeasureStyleMap = new $wnd.OpenLayers.StyleMap({
      "default" : lineStyle,
    });
    return linemeasureStyleMap;
  }-*/;

  /**
   * @return Openlayers style object
   */
  public static Style getElasticStyle() {
    final Style style = new Style();
    style.setStrokeColor("#114289");
    style.setStrokeWidth(3);
    style.setStrokeLinecap("square");
    style.setStrokeDashstyle("dot");
    return style;
  }

  /**
   * @return Openlayers style object
   */
  public static Style getRoadSegmentStyle() {
    final Style style = new Style();
    style.setStrokeColor("#990000");
    style.setStrokeOpacity(0.2);
    style.setStrokeWidth(6);
    style.setStrokeLinecap("square");
    return style;
  }

  /**
   * Layer options are set in JavaScript due to incomplete implementation of
   * the GWT-OpenLayers library and huge amount to add support.
   *
   * @return OpenLayers styleMap JavaScript object
   */
  public static native JSObject getDefaultStyleMap() /*-{
    var linemeasureStyles = {
      "Point" : {
        pointRadius : 6,
        graphicName : "circle",
        fillColor : "transparent",
        fillOpacity : 0,
        strokeWidth : 2,
        strokeOpacity : 1,
        strokeColor : "black"
      },
      "Line" : {
        strokeColor : "red",
        strokeOpacity : 0.3,
        strokeWidth : 3,
        strokeLinecap : "square"
      },
      "Polygon" : {
        strokeWidth : 2,
        strokeOpacity : 1,
        strokeColor : "#666666",
        fillColor : "white",
        fillOpacity : 0.5
      }
    };
    var lineStyle = new $wnd.OpenLayers.Style();
    lineStyle.addRules([ new $wnd.OpenLayers.Rule({
      symbolizer : linemeasureStyles
    }) ]);
    var linemeasureStyleMap = new $wnd.OpenLayers.StyleMap({
      "default" : lineStyle,
    });
    return linemeasureStyleMap;
  }-*/;
}
