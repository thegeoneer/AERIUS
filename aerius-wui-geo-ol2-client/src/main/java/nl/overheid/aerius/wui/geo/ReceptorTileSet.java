/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Optimized collection for massive amounts of receptor objects, distributes receptors into 'tiles', which makes it far more efficient to retrieve a
 * location-based subset of the receptors.
 */
public class ReceptorTileSet implements Collection<AeriusResultPoint> {
  /**
   * Iterator containing iterators of several tiles, iterating through those as if they are one.
   */
  final class TiledIterator<E> implements Iterator<E>, Iterable<E> {
    private int counter;
    private final ArrayList<Iterator<E>> tiles;

    TiledIterator(final ArrayList<Iterator<E>> tiles) {
      this.tiles = tiles;
    }

    @Override
    public boolean hasNext() {
      // If there are no tiles, return false
      // If the current tile hasNext, return true, if not, proceed to the next tile
      // if there are no more, return false, else return the next tile's hasNext
      return tiles.isEmpty() ? false
          : tiles.get(counter).hasNext() ? true
              : ++counter >= tiles.size() ? false
                  : tiles.get(counter).hasNext();
    }

    @Override
    public E next() {
      return tiles.get(counter).next();
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException("This operation is not supported.");
    }

    @Override
    public Iterator<E> iterator() {
      return this;
    }
  }

  /**
   * Auto-filling Map
   */
  private final TreeMap<Integer, ArrayList<AeriusResultPoint>> tiles = new TreeMap<Integer, ArrayList<AeriusResultPoint>>() {
    private static final long serialVersionUID = -3583088750391817304L;

    @Override
    public ArrayList<AeriusResultPoint> get(final Object key) {
      if (key instanceof Integer) {
        return get((Integer) key);
      }

      return null;
    }

    /**
     * Returns the value associated with the given key, if any, or an empty value if no value exists. Auto-filling its empty spot in the process.
     *
     * @param key the key associated with the needed value
     *
     * @return a HashSet associated with the key
     */
    public ArrayList<AeriusResultPoint> get(final Integer key) {
      if (!containsKey(key)) {
        put(key, new ArrayList<AeriusResultPoint>(receptorsInTile));
      }

      return super.get(key);
    }
  };

  private final int tileSize;
  private final int receptorsInTile;
  private final int horizontalTiles;
  private final BBox receptorBounding;
  private final ReceptorUtil receptorUtil;

  /**
   * Creates a new {@link ReceptorTileSet} with the given tileSize.
   *
   * @param horizontalReceptorsPerTile the maximum number of receptor a tile should horizontally hold.
   * @param zoomLevel the zoomLevel of a receptor in this tile.
   */
  public ReceptorTileSet(final ReceptorUtil receptorUtil, final int horizontalReceptorsPerTile, final BBox receptorBounding,
      final HexagonZoomLevel zoomLevel) {
    this.receptorUtil = receptorUtil;
    this.receptorBounding = receptorBounding;
    // The rib distance of a single (square) tile.
    this.tileSize = (int) (horizontalReceptorsPerTile * zoomLevel.getHexagonRadius() * 1.5d);

    // The maximum number of receptors in a single tile (approximate, for now)
    // TODO Make more accurate
    receptorsInTile = horizontalReceptorsPerTile * horizontalReceptorsPerTile;

    // The number of tiles that fit in the map horizontally
    horizontalTiles = (int) Math.ceil(receptorBounding.getMaxX() % tileSize);
  }

  @Override
  public int size() {
    int size = 0;
    for (final ArrayList<AeriusResultPoint> set : tiles.values()) {
      size += set.size();
    }

    return size;
  }

  @Override
  public boolean isEmpty() {
    for (final ArrayList<AeriusResultPoint> set : tiles.values()) {
      if (!set.isEmpty()) {
        return true;
      }
    }

    return false;
  }

  /**
   * <p>
   * Use getTileSet(AeriusResultPoint topLeft, AeriusResultPoint bottomRight) to get a subset.
   * </p>
   *
   * {@inheritDoc}
   */
  @Override
  public Iterator<AeriusResultPoint> iterator() {
    final ArrayList<Iterator<AeriusResultPoint>> iterators = new ArrayList<Iterator<AeriusResultPoint>>();

    for (final ArrayList<AeriusResultPoint> lst : tiles.values()) {
      iterators.add(lst.iterator());
    }

    return new TiledIterator<AeriusResultPoint>(iterators);
  }

  @Override
  public Object[] toArray() {
    throw new UnsupportedOperationException("This operation is not supported.");
  }

  @Override
  public <T> T[] toArray(final T[] a) {
    throw new UnsupportedOperationException("This operation is not supported.");
  }

  /**
   * Returns an iterator for all {@link Point}s contained tiles that overlap the given bounding box.
   *
   * Tiles that are returned will be cached internally, and will not be returned in any subsequent call to this method.
   *
   * @param topLeftReceptor the receptor on the top left
   * @param bottomRightReceptor the receptor on the bottom right
   *
   * @return an iterator for all {@link Point}s in the given bounding box
   */
  public TiledIterator<AeriusResultPoint> getTileSet(final Point topLeftReceptor, final Point bottomRightReceptor) {
    final int topLeft = getTileNumber(topLeftReceptor);
    final int bottomRight = getTileNumber(bottomRightReceptor);

    final ArrayList<Iterator<AeriusResultPoint>> iterators = new ArrayList<Iterator<AeriusResultPoint>>();
    for (final Integer key : tiles.keySet()) {
      // Test for tiles to the north (and east on the first row)
      if (key < topLeft) {
        continue;
      }

      // // Test for tiles to the east
      // if (key % horizontalTiles < topLeft % horizontalTiles) {
      // continue;
      // }
      //
      // // Test for tiles to the west
      // if (key % horizontalTiles > bottomRight % horizontalTiles) {
      // continue;
      // }

      // Test for tiles to the south
      if (key > bottomRight) {
        break;
      }

      if (tiles.containsKey(key)) {
        iterators.add(tiles.get(key).iterator());
      }
    }

    return new TiledIterator<AeriusResultPoint>(iterators);
  }

  @Override
  public boolean contains(final Object o) {
    for (final ArrayList<AeriusResultPoint> set : tiles.values()) {
      if (set.contains(o)) {
        return true;
      }
    }

    return false;
  }

  @Override
  public boolean containsAll(final Collection<?> c) {
    for (final Object o : c) {
      if (!contains(o)) {
        return false;
      }
    }

    return true;
  }

  @Override
  public boolean add(final AeriusResultPoint e) {
    final int tileNumber = getTileNumber(e);

    return tiles.get(tileNumber).add(e);
  }

  @Override
  public boolean addAll(final Collection<? extends AeriusResultPoint> c) {
    boolean ret = false;

    for (final AeriusResultPoint o : c) {
      ret |= add(o);
    }

    return ret;
  }

  @Override
  public boolean remove(final Object o) {
    boolean ret = false;

    for (final ArrayList<AeriusResultPoint> set : tiles.values()) {
      ret |= set.remove(o);
    }

    return ret;
  }

  @Override
  public boolean removeAll(final Collection<?> c) {
    boolean ret = false;

    for (final Object o : c) {
      ret |= remove(o);
    }

    return ret;
  }

  @Override
  public boolean retainAll(final Collection<?> c) {
    throw new UnsupportedOperationException("This operation is not supported.");
  }

  @Override
  public void clear() {
    for (final ArrayList<AeriusResultPoint> set : tiles.values()) {
      set.clear();
    }
  }

  /**
   * Finds the receptor point by identifying the tile it should reside in and returning it.
   *
   * @param recId the receptor id
   *
   * @return a fully fledged AeriusResultPoint, if it's available
   */
  public AeriusResultPoint findAeriusPoint(final int recId) {
    final AeriusResultPoint rec = new AeriusResultPoint(recId);
    receptorUtil.attachReceptorToGrid(rec);

    final int tileNumber = getTileNumber(rec);

    if (tiles.containsKey(tileNumber)) {
      final ArrayList<AeriusResultPoint> receptors = tiles.get(tileNumber);
      final int numReceptors = receptors.size();
      for (int i = 0; i < numReceptors; i++) {
        if (receptors.get(i).getId() == recId) {
          return receptors.get(i);
        }
      }
    }
    return null;
  }

  /**
   * Returns the tile ID for the given {@link Point}.
   *
   * - Each tile must have a unique ID. - ID's must be incremental geographically from top left to bottom right.
   *
   * @note Needs to be severely tested.
   *
   * @param point {@link Point} to get the tile ID of.
   *
   * @return the tile ID.
   */
  private int getTileNumber(final Point point) {
    final int idX = (int) (point.getX() - receptorBounding.getMinX()) / tileSize;
    final int idY = (int) (point.getY() - receptorBounding.getMinY()) / tileSize;

    return idY * horizontalTiles + idX;
  }
}
