/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.geo.MarkerItem.TYPE;

/**
 * Util class to create a set of {@link ComparisonMarkerItem} from a list of sources.
 */
public final class ComparisonMarkerCreator {

  // private util class constructor
  private ComparisonMarkerCreator() {
  }

  /**
   * This type of marker addition is kind of an odd-case for this class.
   *
   * - We'll be receiving an {@link ArrayList} containing {@link EmissionSourceList}s, which represent a situation.
   *
   * - Some objects contained in each situation _may_ be equal. Those objects must be contained in the same {@link MarkerItem}.
   *
   * - Depending on the index in the list, a different style is applied, which we will distinguish by adding a 'type bit' using
   *   exclusive OR for each respective index using Math.pow(2, ind). For now, only type bits for situation 1 and 2 are supported
   *   visually. This technique makes scaling to more situations possible without further changes here, although that's unlikely
   *   to ever happen.
   *
   * @param labelModes
   * @param situations list of situations.
   * @return
   */
  public static Collection<ComparisonMarkerItem> getComparisonMarkers(final HashMap<MarkerItem.TYPE, Boolean> labelModes,
      final ArrayList<EmissionSourceList> situations) {
    @SuppressWarnings("serial")
    final HashMap<EmissionSource, ComparisonMarkerItem> map = new HashMap<EmissionSource, ComparisonMarkerItem>() {
      @Override
      public ComparisonMarkerItem get(final Object src) {
        if (src instanceof EmissionSource) {
          return get((EmissionSource) src);
        }

        return null;
      }

      /**
       * @param src the source value to get
       *
       * @return marker item associated with the source.
       */
      public ComparisonMarkerItem get(final EmissionSource src) {
        if (!containsKey(src)) {
          final ComparisonMarkerItem comparisonMarkerItem = new ComparisonMarkerItem(src);
          comparisonMarkerItem.setLabelType(labelModes.containsKey(TYPE.EMISSION_SOURCE) ? labelModes.get(TYPE.EMISSION_SOURCE) : false);
          put(src, comparisonMarkerItem);
        }

        return super.get(src);
      }
    };

    for (int i = 0; i < situations.size(); i++) {
      final EmissionSourceList esl = situations.get(i);
      if (esl != null) {
        for (final EmissionSource src : esl) {
          map.get(src).addTypeBit((int) Math.pow(2, i));
        }
      }
    }

    for (final ComparisonMarkerItem item : map.values()) {
      item.updateStyle();
      item.setMarkerItemColor(ColorUtil.webColor(item.getObject().getSector().getProperties().getColor()));
    }
    return map.values();
  }
}
