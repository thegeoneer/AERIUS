/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.shared.domain.WMSLayerType;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.main.context.AppContext;
import nl.overheid.aerius.wui.main.event.HabitatClearEvent;
import nl.overheid.aerius.wui.main.event.HabitatTypeDisplayEvent;
import nl.overheid.aerius.wui.main.event.HabitatTypeHoverEvent;

@Singleton
public class HabitatTypeLayerWrapper {
  interface HabitatTypeLayerWrapperEventBinder extends EventBinder<HabitatTypeLayerWrapper> {}

  private static final String RECEPTOR_ID = "receptorId";
  private static final String ASSESSMENT_AREA_ID = "assessmentAreaId";

  private final HabitatTypeLayerWrapperEventBinder eventBinder = GWT.create(HabitatTypeLayerWrapperEventBinder.class);

  private final HabitatTypeLayer habitatForReceptorLayer;
  private final HabitatTypeLayer habitatForAssessmentAreaLayer;

  @Inject
  public HabitatTypeLayerWrapper(final EventBus eventBus, final AppContext<?, ?> appContext, final FetchGeometryServiceAsync service) {
    habitatForReceptorLayer =
        new HabitatTypeLayer(service, (LayerWMSProps) appContext.getContext().getLayer(WMSLayerType.WMS_RELEVANT_HABITAT_INFO_FOR_RECEPTOR_VIEW)) {
      @Override
      protected void formatParameters(final HashMap<String, Object> filter, final int eventId) {
        filter.put(RECEPTOR_ID, eventId);
      }
    };

    habitatForAssessmentAreaLayer = new HabitatTypeLayer(service, (LayerWMSProps) appContext.getContext().getLayer(WMSLayerType.WMS_HABITATS_VIEW)) {
      @Override
      protected void formatParameters(final HashMap<String, Object> filter, final int eventId) {
        filter.put(ASSESSMENT_AREA_ID, eventId);
      }
    };

    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  void onHabitatTypeHover(final HabitatTypeHoverEvent event) {
    if (event.getHoverType() == null) {
      removeHoverLayer(event.getHoverTypeId());
      return;
    }

    switch (event.getHoverType()) {
    case POINT:
      habitatForReceptorLayer.hoverHabitats(event.getHoverTypeId(), event.getHabitatTypeIds(), event.getDisplayType());
      break;
    case NATURE_AREA:
      habitatForAssessmentAreaLayer.hoverHabitats(event.getHoverTypeId(), event.getHabitatTypeIds(), event.getDisplayType());
      break;
    default:
      break;
    }
  }

  @EventHandler
  void onHabitatTypeDisplay(final HabitatTypeDisplayEvent event) {
    if (event.getHoverType() == null) {
      removePersistentLayer(event.getHoverTypeId());
      return;
    }

    switch (event.getHoverType()) {
    case POINT:
      habitatForReceptorLayer.displayHabitats(event.getHoverTypeId(), event.getHabitatTypeIds(), event.getDisplayType());
      break;
    case NATURE_AREA:
      habitatForAssessmentAreaLayer.displayHabitats(event.getHoverTypeId(), event.getHabitatTypeIds(), event.getDisplayType());
      break;
    default:
      break;
    }
  }

  @EventHandler
  void onHabitatClear(final HabitatClearEvent event) {
    clear();
  }

  public void setMap(final MapLayoutPanel map) {
    habitatForReceptorLayer.setMap(map);
    habitatForAssessmentAreaLayer.setMap(map);
  }

  public void clear() {
    habitatForReceptorLayer.clear();
    habitatForAssessmentAreaLayer.clear();
  }

  public void setVisible(final boolean visible) {
    habitatForReceptorLayer.setVisible(visible);
    habitatForAssessmentAreaLayer.setVisible(visible);
  }

  private void removePersistentLayer(final int eventId) {
    habitatForReceptorLayer.removePersistentLayer(eventId);
    habitatForAssessmentAreaLayer.removePersistentLayer(eventId);
  }

  private void removeHoverLayer(final int eventId) {
    habitatForReceptorLayer.removeHoverLayer(eventId);
    habitatForAssessmentAreaLayer.removeHoverLayer(eventId);
  }
}
