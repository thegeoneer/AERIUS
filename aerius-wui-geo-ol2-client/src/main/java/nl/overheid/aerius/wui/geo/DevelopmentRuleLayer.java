/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.HashMap;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.MarkerLayer;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleMarker;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResult;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 * Shows development icons on the map.
 *
 */
public class DevelopmentRuleLayer extends MarkerLayer {

  private final HashMap<Integer, DevelopmentRuleLayerMarker> markers = new HashMap<Integer, DevelopmentRuleLayerMarker>();
  private final ReceptorUtil receptorUtil;

  /**
   * Init the map with with title.
   *
   * @param map the layer for the markers
   */
  public DevelopmentRuleLayer(final MapLayoutPanel map) {
    super(map, M.messages().developmentRuleLayerTitle(), new DevelopmentRuleLegend());
    receptorUtil = new ReceptorUtil(map.getReceptorGridSettings());
  }



  /**
   * Draw the markers on the layer.
   *
   * @param developmentRuleResult development rule info.
   */

  public void drawMarkers(final DevelopmentRuleResult developmentRuleResult) {
    clearMarkers();

    for (final Integer receptorId : developmentRuleResult) {
      final AeriusPoint point = receptorUtil.setAeriusPointFromId(new AeriusPoint(receptorId));
      final  DevelopmentRuleMarker depoMarker = new DevelopmentRuleMarker();
      depoMarker.setReceptorId(receptorId);
      depoMarker.setX(point.getX());
      depoMarker.setY(point.getY());
      final DevelopmentRuleLayerMarker marker = new DevelopmentRuleLayerMarker(depoMarker, MarkerItem.TYPE.DEVELOPMENT_KIND,
          developmentRuleResult.getRule());
      markers.put(receptorId, marker);

    }
    // Draw the markers
    for (final DevelopmentRuleLayerMarker marker : markers.values()) {
      marker.drawMarker();
      final AeriusMarker layerMarker = new AeriusMarker(marker);

      layerMarker.addMarkerItem(marker);
      addMarker(layerMarker.getMarker());
    }
  }

  @Override
  public void clearMarkers() {
    markers.clear();
    super.clearMarkers();
  }

}
