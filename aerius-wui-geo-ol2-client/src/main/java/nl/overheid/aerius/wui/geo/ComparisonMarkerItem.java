/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;

import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.wui.main.resources.R;

@SuppressWarnings("serial")
public class ComparisonMarkerItem extends LabelledMarkerItem<EmissionSource> {
  /**
   * Padding used between double decoration.
   */
  private static final int UNDERLINE_PADDING = 2;

  private static final int SITUATION_1_ID = 1;
  private static final int SITUATION_2_ID = 2;
  private static final int BOTH = 3;

  private final DivElement divWrapper = Document.get().createDivElement();
  private DivElement label;

  private int bit;

  public ComparisonMarkerItem(final EmissionSource obj) {
    super(obj, obj, String.valueOf(obj.getId()), obj.getLabel(), TYPE.EMISSION_SOURCE);
    getElement().addClassName(R.css().sourceMarkerComparable());
    getElement().appendChild(divWrapper);
    divWrapper.getStyle().setPaddingBottom(UNDERLINE_PADDING, Unit.PX);
    divWrapper.appendChild(getContentContainer());
  }

  public void updateStyle() {
    switch (bit) {
    case SITUATION_1_ID:
      divWrapper.addClassName(R.css().underlineSolid());
      break;
    case SITUATION_2_ID:
      getContentContainer().addClassName(R.css().underlineDashed());
      break;
    case BOTH:
      getContentContainer().addClassName(R.css().underlineSolid());
      divWrapper.addClassName(R.css().underlineDashed());
      break;
    default:
      // The marker layer hasn't been configured to handle over 2 situations.
      throw new IllegalStateException("This combination is not possible: " + bit);
    }
  }

  /**
   * Add to this marker item's comparison type.
   *
   * @param id the comparison type to add
   */
  public void addTypeBit(final int id) {
    bit |= id;
  }

  @Override
  protected Element getContentContainer() {
    // Create label here, because the method is called in a super constructor and if it would be created
    // as field parameter the label field wouldn't be initialized and null.
    if (label == null) {
      label = Document.get().createDivElement();
    }
    return label;
  }
}
