/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;

/**
 * Panel to configure active Layers. Layers can be activated/deactivated by moving layers from left to right or otherway around.
 */
@Singleton
public class LayerConfigPanel extends Composite implements HasCloseHandlers<Boolean> {
  interface LayerConfigPanelUiBinder extends UiBinder<Widget, LayerConfigPanel> {}

  private static final LayerConfigPanelUiBinder UI_BINDER = GWT.create(LayerConfigPanelUiBinder.class);

  interface CustomStyle extends LayerCellTable.CustomStyle, CssResource {}

  private final MapLayoutPanel map;

  @UiField CustomStyle style;

  @UiField(provided = true) LayerCellTable activeLayerPanel;
  @UiField(provided = true) LayerCellTable inactiveLayerPanel;

  @Inject
  public LayerConfigPanel(final MapLayoutPanel map) {
    this.map = map;

    activeLayerPanel = new LayerCellTable();
    inactiveLayerPanel = new LayerCellTable();

    initWidget(UI_BINDER.createAndBindUi(this));

    activeLayerPanel.setStyle(style);
    inactiveLayerPanel.setStyle(style);
  }

  public void init() {
    activeLayerPanel.clearLayers();
    inactiveLayerPanel.clearLayers();
    activeLayerPanel.addAllLayers(map.getLayers());
    //    inactiveLayerPanel.addAllLayers(map.getInactiveLayers());
  }

  @UiHandler("moveRight")
  void onMoveRightClick(final ClickEvent e) {
    // Get the selected layers
    final ArrayList<MapLayer> selection = activeLayerPanel.getSelection();

    // Remove them from the map
    for (final MapLayer layer : selection) {
      map.removeLayer(layer);
    }

    // Swap the selection
    inactiveLayerPanel.addAllLayers(selection);
    activeLayerPanel.clearSelection(true);
  }

  @UiHandler("moveLeft")
  void onMoveLeftClick(final ClickEvent e) {
    // Get the selected layers
    final ArrayList<MapLayer> selection = inactiveLayerPanel.getSelection();

    // Add them from to map
    for (final MapLayer layer : selection) {
      map.addLayer(layer.getLayerProps());
    }

    // Swap the selection
    activeLayerPanel.addAllLayers(selection);
    inactiveLayerPanel.clearSelection(true);
  }

  @UiHandler("cancelButton")
  void onCancelClick(final ClickEvent e) {
    CloseEvent.fire(this, true);
  }

  @Override
  public HandlerRegistration addCloseHandler(final CloseHandler<Boolean> handler) {
    return addHandler(handler, CloseEvent.getType());
  }
}
