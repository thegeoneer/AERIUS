/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import nl.overheid.aerius.geo.shared.LayerProps.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.deposition.ComparisonEnum;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * Comparison legend with a default color implementation.
 */
public class ComparisonLegend extends ColorRangesLegend {
  private static final long serialVersionUID = -1468014493348635698L;

  public ComparisonLegend(final boolean hexagon, final EmissionResultType emissionResultType,
      final EmissionResultValueDisplaySettings displaySettings) {
    super(new String[ComparisonEnum.NON_TRIVIAL.length], new String[ComparisonEnum.NON_TRIVIAL.length], hexagon);

    int i = 0;
    for (final ComparisonEnum ce : ComparisonEnum.NON_TRIVIAL) {
      getLegendNames()[i] = FormatUtil.formatEmissionResult(emissionResultType, ce.getValue(), displaySettings);
      getColors()[i] = ColorUtil.webColor(ce.getColor());
      i++;
    }
  }
}
