/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo.search;

import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestionType;
import nl.overheid.aerius.wui.main.ui.search.SearchAction;
import nl.overheid.aerius.wui.main.ui.search.SearchSuggestionTable;
import nl.overheid.aerius.wui.main.ui.search.SearchTable;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

public class MapSearchTable extends SearchTable<MapSearchSuggestionType, MapSearchSuggestion> {
  @Inject
  public MapSearchTable(final HelpPopupController hpC, final MapSearchAction searchAction) {
    super(hpC, searchAction);
  }

  /**
   * TODO Duplication with MapSearchSuggestionTable.
   */
  @Override
  protected SearchSuggestionTable<MapSearchSuggestionType, MapSearchSuggestion> createSuggestionTable(final HelpPopupController hpC, final SearchAction<MapSearchSuggestionType, MapSearchSuggestion> searchAction) {
    return new MapSearchSuggestionTable(hpC, searchAction);
  }
}
