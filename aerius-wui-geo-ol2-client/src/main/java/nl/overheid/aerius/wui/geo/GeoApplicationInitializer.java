/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import nl.overheid.aerius.geo.LayerPreparationUtil;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.service.FetchGeometryService;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.main.ApplicationInitializer;
import nl.overheid.aerius.wui.main.context.AppContext;

public class GeoApplicationInitializer extends ApplicationInitializer {

  private final UserContext userContext;

  public GeoApplicationInitializer(final AppContext<?, ?> context) {
    userContext = context.getUserContext();
  }

  @Override
  public void init() {
    // WORK-AROUND: NO-BULLSHIT-FORCED-PRE-LOAD OF _ALL_ LAYERS, BLOCKING.
    // GET IT OUT OF HERE WHEN LAYER ORDERING IS REFACTORED (which could be a while..)
    // The above caps locked statement may need some context:
    // The applications supports dynamically loading layer capabilities. Unfortunately, for reasons unknown
    // as of yet, this introduces unexplainable/unpredictable behavior where the order at which layers
    // are coming in is not deterministic. It results in somewhat random layer ordering, which we don't want.
    // This workaround forces preloading all layers, making sure we don't have to do any capability calls later
    // on. The loading time for the application will be a little longer, but loading times later on will not exist.
    // Lazy loading is preferred though (in my opinion).

    // These are the solutions: Fix the chaining of retrieving/adding layers, this fix has been attempted before
    // but it doesn't work exactly as expected for some reason. Look into it.

    // Either that, or support adding layers (LayerProps that is) to a Map, then load the layer's capabilities,
    // and insert it at the order _at which it was first added_. You'll need to deal with some concurrency issues
    // here. Many-a-time a bunch of layers will be added to the map at roughly the same time, which potentially
    // requires multiple capability calls which may come back in another order; the _initial_ order needs to be
    // preserved.
    if (userContext.getLayers() == null) {
      return;
    }

    final FetchGeometryServiceAsync service = (FetchGeometryServiceAsync) GWT.create(FetchGeometryService.class);
    final ArrayList<LayerProps> layersToPreload = new ArrayList<LayerProps>(userContext.getLayers());
    layersToPreload.add(userContext.getBaseLayer());

    LayerPreparationUtil.prepareLayers(service, layersToPreload, new AsyncCallback<Integer>() {
      @Override
      public void onFailure(final Throwable caught) {
        // Some layers may have failed to preload (this has been logged), finish loading the application anyway.
      }

      @Override
      public void onSuccess(final Integer result) {}
    });
  }
}
