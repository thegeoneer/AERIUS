/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;
import java.util.Map.Entry;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.geo.LabeledMultiMapLayer;
import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MultiMapLayer;
import nl.overheid.aerius.geo.WMSMapLayer;
import nl.overheid.aerius.geo.shared.LayerMultiWMSProps;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.IsCollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.OptGroupListBox;
import nl.overheid.aerius.wui.main.widget.OptGroupListBox.OptGroupData;

/**
 * A row in the selected layer panel.
 *
 * FIXME Fully incompatible with new layer setup.
 */
class LayerPanelItemListBox extends LayerPanelItemImpl implements LayerPanelItem, IsCollapsiblePanel, ChangeHandler {
  private static final String TYPE = "type";

  private final Label label;
  private final FocusPanel focusLabel;
  private final ListBox listBox;
  private final LayerMultiWMSProps<?> properties;
  private final String macroLabel;
  private LayerLabelCheckBox labelCheckBox;

  @SuppressWarnings("unchecked")
  public LayerPanelItemListBox(final MapLayer layer, final LayerItemChangeHandler changeHandler, final LegendTitleFactory unitFactory,
      final HelpPopupController hpC) {
    super(layer, changeHandler, unitFactory, hpC);

    if (layer instanceof LabeledMultiMapLayer) {
      this.properties = (LayerMultiWMSProps<?>) ((LabeledMultiMapLayer) layer).getLabelLayer().getLayerProps();
    } else {
      this.properties = (LayerMultiWMSProps<?>) layer.getLayerProps();
    }

    final FlowPanel tempFlowPanel = new FlowPanel();
    final FlowPanel tempListBoxPanel = new FlowPanel();

    label = new Label();

    if (!properties.getTypes().isEmpty() && properties.getTypes().values().iterator().next() instanceof Sector) {
      listBox = getSectorOptGroupListBox((LayerMultiWMSProps<Sector>) properties);
    } else {
      listBox = new ListBox();
      listBox.insertItem(M.messages().layerPanelSelect(), "0", 0);
      for (final Entry<? extends Object, ?> entry : properties.getTypes().entrySet()) {
        listBox.addItem(String.valueOf(entry.getValue()), String.valueOf(entry.getKey()));
      }
    }

    // Ensure the first item doesn't show anything.
    properties.setParamFilter(TYPE, "0");
    listBox.addChangeHandler(this);
    label.setText(layer.getTitle());
    macroLabel = layer.getTitle();
    focusLabel = new FocusPanel(label);
    tempFlowPanel.add(focusLabel);
    tempListBoxPanel.add(listBox);

    if (layer instanceof LabeledMultiMapLayer) {
      listBox.setWidth("85%");
      labelCheckBox = new LayerLabelCheckBox((LabeledMultiMapLayer) layer, changeHandler, hpC);
      tempListBoxPanel.add(labelCheckBox);
    }
    tempFlowPanel.add(tempListBoxPanel);
    itemTitle = tempFlowPanel;

    init();

    listBox.setStyleName(R.css().layerPanelListBox());
    listBox.ensureDebugId(TestID.INPUT_LAYERPANEL_LISTBOX + "-" + layer.getTitle());

    hpC.addWidget(listBox, hpC.tt().ttLayerPanelHabitatType());
  }

  @Override
  public final void setLayerName(final String name) {
    label.setText(name);
  }

  /**
   * Returns the widget presenting the draggable area.
   *
   * @return
   */
  @Override
  public Widget getDragHandler() {
    return focusLabel;
  }

  @Override
  public void setEnabled(final boolean enabled) {
    if (enabled && labelCheckBox != null) {
      labelCheckBox.setSelected(true);
    }
    super.setEnabled(enabled);
  }

  @Override
  public void onChange(final ChangeEvent event) {
    // Enable the layer
    changeHandler.changeEnabled(layer, true);
    if (labelCheckBox != null) {
      labelCheckBox.setSelected(((LabeledMultiMapLayer) layer).getLabelLayer().isVisible());
    }

    if (layer instanceof LabeledMultiMapLayer) {
      ((LabeledMultiMapLayer) layer).setParamFilter(TYPE, listBox.getValue(listBox.getSelectedIndex()));
    } else if (layer instanceof WMSMapLayer || layer instanceof MultiMapLayer) {
      properties.setParamFilter(TYPE, listBox.getValue(listBox.getSelectedIndex()));
    }
    layer.redraw();
  }

  private ListBox getSectorOptGroupListBox(final LayerMultiWMSProps<Sector> properties) {
    final OptGroupListBox optGroupListBox = new OptGroupListBox();
    final OptGroupData<Sector> optGroupData = new OptGroupData<Sector>() {
      @Override
      protected String getKey(final Sector value) {
        return String.valueOf(value.getSectorId());
      }

      @Override
      protected String asString(final Sector value) {
        return M.messages().sectorGroup(value.getSectorGroup()) + OptGroupListBox.GROUP_SEPERATOR + value.getName();
      }
    };
    final ArrayList<Sector> sectors = new ArrayList<>();
    for (final Entry<? extends Object, Sector> entry : properties.getTypes().entrySet()) {
      sectors.add(entry.getValue());
    }
    optGroupData.setValue(sectors);
    //ensure a -- Select -- option is first.
    optGroupListBox.addItem(M.messages().layerPanelSelect(), "0");
    optGroupListBox.addData(optGroupData);
    return optGroupListBox;
  }

  @Override
  public String getLayerMacroLabel() {
    return macroLabel;
  }
}
