/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.dom.client.Element;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.wui.main.resources.R;

@SuppressWarnings("serial")
public class LabelledMarkerItem<E extends HasId> extends MarkerItem<E> {
  private static final double FULL_OPACITY = 1.0;
  private static final double HALF_OPACITY = 0.5;
  private String color;
  private boolean opacity;

  private boolean showMarkerAsLabel;
  private final String markerNameId;
  private final String markerNameLabel;

  public LabelledMarkerItem(final Point point, final E obj, final String markerNameId, final String markerNameLabel, final TYPE type) {
    super(point, obj, type);

    this.markerNameId = markerNameId;
    this.markerNameLabel = markerNameLabel;
    updateLabel();

    getElement().addClassName(R.css().textOverflowEllipsis());
    getElement().addClassName(getType() == TYPE.CALCULATION_POINT ? R.css().depositionMarker() : R.css().sourceMarker());
    updateMarkerItemColor();
    updateMarkerItemOpacity();
  }

  public void setLabelType(final boolean b) {
    this.showMarkerAsLabel = b;
    updateLabel();
  }

  public void setMarkerItemColor(final String color) {
    this.color = color;
    updateMarkerItemColor();
  }

  public void setMarkerItemOpacity(final boolean opacity) {
    this.opacity = opacity;
    updateMarkerItemOpacity();
  }

  protected void updateLabel() {
    if (getType() == TYPE.EMISSION_SOURCE_SELECTED) {
      getContentContainer().setInnerText(markerNameLabel);
    } else {
      getContentContainer().setInnerText(showMarkerAsLabel && markerNameLabel != null && !markerNameLabel.isEmpty() ? markerNameLabel : markerNameId);
    }
  }

  protected Element getContentContainer() {
    return getElement();
  }

  protected void updateMarkerItemColor() {
    getElement().getStyle().setBackgroundColor(color);
  }

  protected void updateMarkerItemOpacity() {
    getElement().getStyle().setOpacity(opacity ? HALF_OPACITY : FULL_OPACITY);
  }
}
