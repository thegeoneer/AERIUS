/**
 *
 */
package org.gwtopenmaps.openlayers.client.control;

import org.gwtopenmaps.openlayers.client.event.EventHandler;
import org.gwtopenmaps.openlayers.client.event.EventObject;
import org.gwtopenmaps.openlayers.client.event.EventType;
import org.gwtopenmaps.openlayers.client.event.MeasureEvent;
import org.gwtopenmaps.openlayers.client.event.MeasureListener;
import org.gwtopenmaps.openlayers.client.event.MeasurePartialListener;
import org.gwtopenmaps.openlayers.client.handler.Handler;
import org.gwtopenmaps.openlayers.client.util.JSObject;


/**
 * @author Michel Vitor A Rodrigues - Intec
 *
 */
public class Measure extends Control
{
  public static Measure narrowToMeasure(final JSObject element)
  {
    return element == null ? null : new Measure(element);
  }

  protected Measure(final JSObject element)
  {
    super(element);
  }

  public Measure(final Handler handler)
  {
    this(MeasureImpl.create(handler.getJSObject()));
  }

  public Measure(final Handler handler, final MeasureOptions options)
  {
    this(MeasureImpl.create(handler.getJSObject(), options.getJSObject()));
  }

  public void setPersist(final boolean persist)
  {
    MeasureImpl.setPersist(getJSObject(), persist);
  }

  /**
   * AERIUS EDIT :: JS supports immediate flag but gwt wrapper does not
   */
  public void setImmediate(final boolean immediate)
  {
    MeasureImpl.setImmediate(getJSObject(), immediate);
  }

  public void addMeasureListener(final MeasureListener listener)
  {
    eventListeners.addListener(this, listener, EventType.CONTROL_MEASURE, new EventHandler()
    {
      @Override
      public void onHandle(final EventObject eventObject)
      {
        final MeasureEvent e = new MeasureEvent(eventObject);
        listener.onMeasure(e);
      }
    });
  }

  public void addMeasurePartialListener(final MeasurePartialListener listener)
  {
    eventListeners.addListener(this, listener, EventType.CONTROL_MEASURE_PARTIAL, new EventHandler()
    {
      @Override
      public void onHandle(final EventObject eventObject)
      {
        final MeasureEvent e = new MeasureEvent(eventObject);
        listener.onMeasurePartial(e);
      }
    });
  }
}
