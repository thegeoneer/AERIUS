/**
 *
 */
package org.gwtopenmaps.openlayers.client.geometry;

import org.gwtopenmaps.openlayers.client.util.JObjectArray;
import org.gwtopenmaps.openlayers.client.util.JSObject;


/**
 * @author Edwin Commandeur - Atlis EJS
 *
 */
public class LineString extends Curve
{

  public static LineString narrowToLineString(final JSObject lineString)
  {
    return (lineString == null) ? null : new LineString(lineString);
  }

  protected LineString(final JSObject element)
  {
    super(element);
  }

  public LineString(final Point[] pts)
  {
    this(LineStringImpl.create((new JObjectArray(pts)).getJSObject()));
  }

  /**
   * Test for instersection between two geometries. This is a cheapo
   * implementation of the Bently-Ottmann algorigithm. It doesn’t really keep
   * track of a sweep line data structure.  It is closer to the brute force
   * method, except that segments are sorted and potential intersections are
   * only calculated when bounding boxes intersect.
   * @param line
   * @return
   */
  public boolean intersects(final LineString line) {
    return LineStringImpl.intersects(this.getJSObject(), line.getJSObject());
  }
}
