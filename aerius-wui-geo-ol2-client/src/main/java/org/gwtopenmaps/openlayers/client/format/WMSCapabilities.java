package org.gwtopenmaps.openlayers.client.format;

import org.gwtopenmaps.openlayers.client.util.JSObject;

/**
 * Read WMS Capabilities.
 */
public class WMSCapabilities extends Format {

    /**
     * Create a new parser for WMS capabilities.
     */
    public WMSCapabilities() {
        this(WMSCapabilitiesImpl.create());
    }

    protected WMSCapabilities(JSObject format) {
        super(format);
    }

    /**
     * APIMethod: read
     * Read capabilities data from a string, and return a list of layers.
     *
     * Parameters: 
     * data - {String} or {DOMElement} data to read/parse.
     *
     * Returns:
     * {Array} List of named layers.
     */
    public WMTMSCapabilities read(String data) {
        return WMSCapabilitiesImpl.read(this.getJSObject(), data);
    }
}
