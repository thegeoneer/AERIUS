package org.gwtopenmaps.openlayers.client.control;

import org.gwtopenmaps.openlayers.client.util.JSObject;

/**
 * The Pan control is a single button to pan the map in one direction. 
 */
public class Pan extends Control
{

    public enum Direction
    {
        North, South, East, West
    }
	
    protected Pan(JSObject element) {
        super(element);
		}
	
    public Pan(Direction direction) {
        this(PanImpl.create(direction.name()));
    }

    public Pan(String direction) {
        this(PanImpl.create(direction));
    }

    public void trigger() {
        PanImpl.trigger(this.getJSObject());
    }
}
