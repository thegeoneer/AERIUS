package org.gwtopenmaps.openlayers.client.control;

import org.gwtopenmaps.openlayers.client.util.JSObject;

class PanImpl
{

    public static native JSObject create(String direction) /*-{
        var foo = new Object();
        return new $wnd.OpenLayers.Control.Pan(direction);
    }-*/;

    public static native JSObject create(JSObject options) /*-{
        return new $wnd.OpenLayers.Control.Pan(options);
    }-*/;

    public static native void trigger(JSObject pan) /*-{
        pan.trigger();
    }-*/;
}
