/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationInfoRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.ReceptorInfoRepository;
import nl.overheid.aerius.db.common.UserGeoLayerRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.gml.GMLReaderFactory;
import nl.overheid.aerius.gml.GMLSourceReader;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.FeatureMember;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.importer.ImportedImaerFile;
import nl.overheid.aerius.shared.domain.info.CalculationInfo;
import nl.overheid.aerius.shared.domain.info.DepositionMarker;
import nl.overheid.aerius.shared.domain.info.EmissionResultInfo;
import nl.overheid.aerius.shared.domain.info.ReceptorInfo;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.shared.service.InfoService;
import nl.overheid.aerius.util.GMLUtil;

/**
 * Implementation of {@link InfoServiceImpl}.
 */
public class InfoServiceImpl implements InfoService {
  private static final Logger LOG = LoggerFactory.getLogger(InfoServiceImpl.class);

  private static final int FEATURE_BUFFER = 65; // hexagon long diagonal / 2

  private final PMF pmf;
  private final ScenarioBaseSession session;

  public InfoServiceImpl(final ScenarioBaseSession session, final PMF pmf) {
    this.session = session;
    this.pmf = pmf;
  }

  @Override
  public ReceptorInfo getAreaInfo(final AeriusPoint rec, final int calculationIdOne, final int calculationIdTwo, final int year)
      throws AeriusException {
    final ReceptorInfo info = new ReceptorInfo();
    info.setReceptor(rec);

    // TODO Temporary fix for impossible receptors, mimics database behavior.
    // Should be replaced with some util that actually checks its validity somehow (like whether coordinates are inside the allowed area).
    if (rec.getId() < 0) {
      return info;
    }

    final DBMessagesKey dbMessagesKey = new DBMessagesKey(pmf.getProductType(), session.getLocale());

    try (final Connection con = pmf.getConnection()) {
      info.setNaturaInfo(ReceptorInfoRepository.getNaturaAreaInfo(con, rec, dbMessagesKey));
      info.setHabitatTypeInfo(ReceptorInfoRepository.getReceptorHabitatAreaInfo(con, rec, dbMessagesKey));

      final EmissionResultInfo erInfo = new EmissionResultInfo(year);
      erInfo.setBackgroundEmissionResults(ReceptorInfoRepository.getBackgroundEmissionResult(con, rec, year));
      erInfo.putEmissionResults(calculationIdOne, CalculationInfoRepository.getEmissionResults(con, calculationIdOne, rec.getId()));
      erInfo.putEmissionResults(calculationIdTwo, CalculationInfoRepository.getEmissionResults(con, calculationIdTwo, rec.getId()));

      info.setEmissionResultInfo(erInfo);
    } catch (final SQLException e) {
      LOG.error("Error getting area info for {}, calculation 1: {}, calculation 2: {}, year: {}", rec, calculationIdOne, calculationIdTwo, year, e);
      throw new AeriusException(Reason.SQL_ERROR);
    }

    return info;
  }

  @Override
  public CalculationInfo getCalculationInfo(final int calculationId) throws AeriusException {
    final CalculationInfo info = new CalculationInfo(calculationId);

    try (final Connection con = pmf.getConnection()) {
      final ArrayList<DepositionMarker> markers = new ArrayList<>();
      markers.addAll(CalculationInfoRepository.determineMarkers(con, calculationId));
      info.setDepositionMarkers(markers);
    } catch (final SQLException e) {
      LOG.error("Error getting calculation info for calculation ID {}", calculationId, e);
      throw new AeriusException(Reason.SQL_ERROR);
    }

    return info;
  }

  @Override
  public HashMap<Integer, Double> getCalculationAssessmentHabitatReceptors(final int calculationId, final int assessmentAreaId, final int habitatId)
      throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return CalculationInfoRepository.getCalculationAssessmentHabitatReceptors(con, calculationId, assessmentAreaId, habitatId);
    } catch (final SQLException e) {
      LOG.error("Error getting calculation habitat receptors, calculationId {}, assessmentAreaId {}, habitatId {}",
          calculationId, assessmentAreaId, habitatId, e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public ScenarioGMLs getMeldingGML(final CalculatedScenario scenario) throws AeriusException {
    final ScenarioGMLs info = new ScenarioGMLs();

    try {
      final GMLWriter gmlBuilder = new GMLWriter(ReceptorGridSettingsRepository.getReceptorGridSettings(pmf));
      //ensure a Melding reference is used for these GMLs
      gmlBuilder.setOverrideReference(false);
      scenario.getMetaData().setReference(ReferenceUtil.generateMeldingReference());

      final ArrayList<StringDataSource> gmls = gmlBuilder.writeToStrings(pmf.getDatabaseVersion(), scenario);

      if (gmls.size() > 1) {
        //proposed situation is the last GML in the list, current is the first.
        info.setCurrentGML(gmls.get(0).getData());
        info.setProposedGML(gmls.get(gmls.size() - 1).getData());
      } else if (gmls.size() == 1) {
        info.setProposedGML(gmls.get(0).getData());
      }
    } catch (final SQLException e) {
      LOG.error("Error getting melding GML for scenario {}", scenario, e);
      throw new AeriusException(Reason.SQL_ERROR);
    }

    return info;
  }

  @Override
  public ArrayList<EmissionSource> getImportedImaerFeatures(final int importedImaerFileId, final AeriusPoint point) throws AeriusException {
    try {
      final GMLReaderFactory factory = GMLReaderFactory.getFactory(pmf);

      final ImportedImaerFile importedImaerFile;
      final ArrayList<String> imaerFeatures;
      try (final Connection con = pmf.getConnection()) {
        importedImaerFile = UserGeoLayerRepository.getImportedImaerFile(con, importedImaerFileId);
        imaerFeatures = UserGeoLayerRepository.getImaerFeaturesForPoint(con, importedImaerFileId, point, FEATURE_BUFFER);
      }

      @SuppressWarnings("rawtypes")
      final List<FeatureMember> featureMembers = new ArrayList<>(imaerFeatures.size());
      for (final String imaerFeature : imaerFeatures) {
        featureMembers.add(GMLUtil.toEmissionSource(imaerFeature));
      }

      final SectorCategories sectorCategories = SectorRepository.getSectorCategories(pmf, session.getLocale());
      // errors/warnings are ignored at this time
      final List<AeriusException> errors = new ArrayList<>();
      final List<AeriusException> warnings = new ArrayList<>();

      final AeriusGMLVersion version = AeriusGMLVersion.safeValueOf(importedImaerFile.getVersion());
      final GMLSourceReader reader = factory.createSourceReader(version, featureMembers, sectorCategories, errors, warnings);
      return reader.readEmissionSources();
    } catch (final SQLException e) {
      LOG.error("Error getting imported imaer features for {}", point, e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

}
