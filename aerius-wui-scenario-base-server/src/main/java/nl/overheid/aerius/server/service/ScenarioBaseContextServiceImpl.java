/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ContextRepository;
import nl.overheid.aerius.db.common.LayerRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Service with all methods related to contextual data. This is both global
 * context data which is static during a user session as well as user specific context data.
 */
public abstract class ScenarioBaseContextServiceImpl<S extends ScenarioBaseSession> extends AbstractContextService<S> {
  private static final Logger LOG = LoggerFactory.getLogger(ScenarioBaseContextServiceImpl.class);

  public ScenarioBaseContextServiceImpl(final PMF pmf, final S session) {
    super(pmf, session);
  }

  @Override
  public ScenarioBaseContext getContext() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ContextRepository.getContext(con, pmf.getProductType(), new DBMessagesKey(pmf.getProductType(), session.getLocale()));
    } catch (final SQLException e) {
      LOG.error("Error getting context for product {}, locale {}", pmf.getProductType(), session.getLocale(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  public ScenarioBaseUserContext getUserContext(final ScenarioBaseUserContext u) throws AeriusException {
    u.getEmissionValueKey().setYear(SharedConstants.getCurrentYear());

    try (final Connection con = pmf.getConnection()) {
      u.setBaseLayer(LayerRepository.getBaseLayer(con, getMessagesKey()));
      u.setLayers(LayerRepository.getLayers(con, getMessagesKey()));
      u.setCalculatorLimits(ContextRepository.getCalculatorLimits(con));
      u.setEmissionResultValueDisplaySettings(ContextRepository.getEmissionResultValueDisplaySettings(con));
    } catch (final SQLException e) {
      LOG.error("Error getting context for product {}, locale {}", pmf.getProductType(), session.getLocale(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
    return u;
  }

  @Override
  public void closeSession(final String lastCalculationKey) {
    session.getSession().invalidate();
  }

}
