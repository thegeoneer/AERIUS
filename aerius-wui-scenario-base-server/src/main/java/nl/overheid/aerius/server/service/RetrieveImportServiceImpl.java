/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.util.ArrayList;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.RetrieveImportService;

public class RetrieveImportServiceImpl implements RetrieveImportService {

  private final AeriusSession session;

  public RetrieveImportServiceImpl(final AeriusSession session) {
    this.session = session;
  }

  @Override
  public ImportResult getImportResult(final String key) throws AeriusException {
    final String scenarioKey = SharedConstants.IMPORT_SCENARIO_PREFIX + key;

    final Object object = session.getSession().getAttribute(scenarioKey);
    if (object != null) {
      session.getSession().removeAttribute(scenarioKey);
    }

    if (object instanceof AeriusException) {
      throw (AeriusException) object;
    }

    return (ImportResult) object;
  }

  @SuppressWarnings("unchecked")
  @Override
  public ArrayList<Calculation> getCalculations(final String key) throws AeriusException {
    final String resultsKey = SharedConstants.IMPORT_SCENARIO_CALCULATIONS_PREFIX + key;

    final Object object = session.getSession().getAttribute(resultsKey);
    session.getSession().removeAttribute(resultsKey);

    if (object instanceof AeriusException) {
      throw (AeriusException) object;
    }

    return (ArrayList<Calculation>) object;
  }
}
