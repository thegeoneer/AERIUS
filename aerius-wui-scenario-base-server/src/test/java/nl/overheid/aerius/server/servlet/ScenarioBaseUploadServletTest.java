/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.UserGeoLayer;
import nl.overheid.aerius.shared.exception.AeriusException;

public class ScenarioBaseUploadServletTest extends BaseDBTest {

  private static final String INDUSTRY_SOURCES_GML = "industry_sources.gml";

  private static final String ROAD_SOURCES_GML = "road_sources.gml";
  private static final int ROAD_SOURCES_GML_AMOUNT_FEATURES = 67;
  private static final double ROAD_SOURCES_GML_TOTAL_VEHICLES_SUM = 1282958.0;
  private static final double ROAD_SOURCES_GML_TOTAL_VEHICLES_LIGHTTRAFFIC_SUM = 1171509.0;
  private static final Map<String, Long> ROAD_SOURCES_GML_SPEED_TYPES_GROUP_BY = new HashMap<String, Long>() {
    {
      put("0;false", 1L);
      put("80;false", 1L);
      put("80;true", 2L);
      put("100;false", 40L);
      put("100;true", 2L);
      put("100-130;false", 4L);
      put("120;false", 1L);
      put("120-130;false", 5L);
      put("130;false", 11L);
    }
  };

  private static final Query QUERY_GET_USER_GEO_LAYER_FEATURES = QueryBuilder
      .from("wms_user_geo_layer_features_view")
      .select(QueryAttribute.VALUE)
      .where(QueryAttribute.KEY)
      .getQuery();

  private Importer importer;

  @Before
  public void before() throws SQLException, AeriusException {
    importer = new Importer(getCalcPMF());
  }

  @Test
  public void testRoadGml() throws IOException, AeriusException, SQLException {
    final ImportResult result = processFile(ROAD_SOURCES_GML);

    assertEquals("Amount of user geo layers created not what expected", 4, result.getUserGeoLayers().size());

    final Map<String, ArrayList<String>> featureValues = getUserGeoLayerFeatures(result.getUserGeoLayers());
    assertAmountOfFeatures(featureValues, ROAD_SOURCES_GML_AMOUNT_FEATURES);
    assertSumValue(featureValues, "ROAD_TOTAL_VEHICLES_PER_DAY", ROAD_SOURCES_GML_TOTAL_VEHICLES_SUM);
    assertSumValue(featureValues, "ROAD_TOTAL_VEHICLES_PER_DAY_LIGHT_TRAFFIC", ROAD_SOURCES_GML_TOTAL_VEHICLES_LIGHTTRAFFIC_SUM);
    assertGroupByValues(featureValues, "ROAD_SPEED_TYPES", ROAD_SOURCES_GML_SPEED_TYPES_GROUP_BY);
  }

  @Test
  public void testIndustryGml() throws IOException, AeriusException, SQLException {
    final ImportResult result = processFile(INDUSTRY_SOURCES_GML);
    assertEquals("Expect 0 user geo layers to be created", 0, result.getUserGeoLayers().size());
  }

  private void assertAmountOfFeatures(final Map<String, ArrayList<String>> featureValues, final int expected) {
    for (final Entry<String, ArrayList<String>> entry : featureValues.entrySet()) {
      assertEquals("Amount of user geo layer features not what expected for layerType: " + entry.getKey(), expected, entry.getValue().size());
    }
  }

  private void assertSumValue(final Map<String, ArrayList<String>> featureValues, final String layerType, final double expected) {
    final double actual =
        featureValues.get(layerType).stream()
          .mapToDouble(Double::parseDouble)
          .sum();

    assertEquals("Sum of values isn't what expected for layerType: " + layerType, expected, actual, 0.001);
  }

  private void assertGroupByValues(final Map<String, ArrayList<String>> featureValues, final String layerType, final Map<String, Long> expected) {
    final Map<String, Long> actual =
        featureValues.get(layerType).stream()
          .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    assertEquals("Group by values not what expected for layerType: " + layerType, expected, actual);
  }

  private Map<String, ArrayList<String>> getUserGeoLayerFeatures(final ArrayList<UserGeoLayer> layers) throws SQLException {
    final Map<String, ArrayList<String>> result = new HashMap<>();

    try (final Connection con = getCalcConnection()) {

      for (final UserGeoLayer layer : layers) {
        final PreparedStatement stmt = con.prepareStatement(QUERY_GET_USER_GEO_LAYER_FEATURES.get());
        QUERY_GET_USER_GEO_LAYER_FEATURES.setParameter(stmt, QueryAttribute.KEY, layer.getKey());

        final ArrayList<String> values = new ArrayList<>();
        final ResultSet rst = stmt.executeQuery();
        while (rst.next()) {
          values.add(QueryAttribute.VALUE.getString(rst));
        }

        result.put(layer.getType(), values);
      }
    }

    return result;
  }

  private ImportResult processFile(final String fileName) throws IOException, AeriusException, SQLException {
    final ImportResult result = importer.convertInputStream2ImportResult(fileName, getFileInputStream(fileName));
    final ScenarioBaseUploadServlet servlet = new ScenarioBaseUploadServlet();
    servlet.createGeoLayers(getCalcConnection(), result);

    return result;
  }

}
