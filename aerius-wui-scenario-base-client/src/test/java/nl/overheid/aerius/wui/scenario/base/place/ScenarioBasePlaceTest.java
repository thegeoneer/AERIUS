/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.place;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gwtmockito.GwtMockitoTestRunner;

import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.place.TestScenarioBasePlace.TestCalculatorPlaceTokenizer;

/**
 * Test for {@link CalculatorPlace} class.
 */
@RunWith(GwtMockitoTestRunner.class)
public class ScenarioBasePlaceTest {

  static final String JOBKEY1 = "12345";
  static final String JOBKEY2 = "54321";

  @Test
  public void testNewPlace() {
    final TestScenarioBasePlace place1 = new TestScenarioBasePlace(null);
    assertEquals("New place no values", -1, place1.getSid2());
  }

  @Test
  public void testEquals() {
    final TestScenarioBasePlace place1a = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE);
    final TestScenarioBasePlace place1b = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE);
    assertEquals("Places should be equal", place1a, place1b);
    final TestScenarioBasePlace place2 = new TestScenarioBasePlace(1, 2, Situation.SITUATION_TWO);
    assertNotEquals("Places should not be equal", place1a, place2);
    final ResultTablePlace rtp = new ResultTablePlace(null);
    rtp.setsId1(1);
    rtp.setsId2(2);
    rtp.setSituation(Situation.SITUATION_ONE);
    assertNotEquals("Places should not be equal to other place same parameters", place1a, rtp);
  }

  @Test
  public void testHashcode() {
    final TestScenarioBasePlace place1a = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE);
    final TestScenarioBasePlace place1b = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE);
    assertEquals("Places should have same hascode", place1a.hashCode(), place1b.hashCode());
    final TestScenarioBasePlace place2 = new TestScenarioBasePlace(1, 2, Situation.SITUATION_TWO);
    assertNotEquals("Places should not have same hashcode", place1a.hashCode(), place2.hashCode());
    final ResultTablePlace rtp = new ResultTablePlace(null);
    rtp.setsId1(1);
    rtp.setsId2(2);
    rtp.setSituation(Situation.SITUATION_ONE);
    assertNotEquals("Different places with same parameters should not have same hashcode", place1a.hashCode(), rtp.hashCode());
  }

  @Test
  public void testEqualsJob() {
    final TestScenarioBasePlace place1a = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE, JOBKEY1, JobType.PRIORITY_PROJECT_UTILISATION);
    final TestScenarioBasePlace place1b = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE, JOBKEY1, JobType.PRIORITY_PROJECT_UTILISATION);
    assertEquals("Places should be equal", place1a, place1b);
    final TestScenarioBasePlace place2 = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE, JOBKEY2, JobType.PRIORITY_PROJECT_UTILISATION);
    assertNotEquals("Places should not be equal", place1a, place2);
    final TestScenarioBasePlace place3 = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE, JOBKEY1, JobType.CALCULATION);
    assertNotEquals("Places should not be equal", place1a, place3);

    final ResultTablePlace rtp = new ResultTablePlace(null);
    rtp.setsId1(1);
    rtp.setsId2(2);
    rtp.setSituation(Situation.SITUATION_ONE);
    rtp.setJobKey(JOBKEY1);
    rtp.setJobType(JobType.PRIORITY_PROJECT_UTILISATION);
    assertNotEquals("Places should not be equal to other place same parameters", place1a, rtp);
  }

  @Test
  public void testHashcodeJob() {
    final TestScenarioBasePlace place1a = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE, JOBKEY1, JobType.PRIORITY_PROJECT_UTILISATION);
    final TestScenarioBasePlace place1b = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE, JOBKEY1, JobType.PRIORITY_PROJECT_UTILISATION);
    assertEquals("Places should have same hascode", place1a.hashCode(), place1b.hashCode());
    final TestScenarioBasePlace place2 = new TestScenarioBasePlace(1, 2, Situation.SITUATION_TWO, JOBKEY1, JobType.PRIORITY_PROJECT_UTILISATION);
    assertNotEquals("Places should not have same hashcode", place1a.hashCode(), place2.hashCode());
    final ResultTablePlace rtp = new ResultTablePlace(null);
    rtp.setsId1(1);
    rtp.setsId2(2);
    rtp.setSituation(Situation.SITUATION_ONE);
    rtp.setJobKey(JOBKEY1);
    rtp.setJobType(JobType.PRIORITY_PROJECT_UTILISATION);
    assertNotEquals("Different places with same parameters should not have same hashcode", place1a.hashCode(), rtp.hashCode());
  }

  @Test
  public void testActivePlace() {
    final TestScenarioBasePlace place1 = new TestScenarioBasePlace(1, 2, Situation.SITUATION_ONE);
    assertEquals("ActivePlace situation 1", 1, place1.getActiveSituationId());
    final TestScenarioBasePlace place2 = new TestScenarioBasePlace(1, 2, Situation.SITUATION_TWO);
    assertEquals("ActivePlace situation 2", 2, place2.getActiveSituationId());
    final TestScenarioBasePlace place3 = new TestScenarioBasePlace(1, -1, Situation.SITUATION_TWO);
    assertEquals("ActivePlace situation 2, but no id for 2", 1, place3.getActiveSituationId());
  }

  @Test
  public void testTokens2Place1Situation() {
    final TestCalculatorPlaceTokenizer ct = new TestCalculatorPlaceTokenizer();
    final TestScenarioBasePlace place1 = ct.getPlace("sid1=0");
    assertEquals("Tokens 1 place; no situation; id situation 1", 0, place1.getSid1());
    assertEquals("Tokens 1 place; no situation; id situation 2 is -1", -1, place1.getSid2());
    assertEquals("Tokens 1 place; no situation; situation is nul", Situation.SITUATION_ONE, place1.getSituation());
    final TestScenarioBasePlace place2 = ct.getPlace("sid1=0&situation=1");
    assertEquals("Tokens 1 place; id situation 1", 0, place2.getSid1());
    assertEquals("Tokens 1 place; id situation 2 is -1", -1, place2.getSid2());
    assertEquals("Tokens 1 place; situation is nul", Situation.SITUATION_ONE, place2.getSituation());
  }

  @Test
  public void testTokens2Place2Situations() {
    final TestCalculatorPlaceTokenizer ct = new TestCalculatorPlaceTokenizer();
    final TestScenarioBasePlace place1 = ct.getPlace("sid1=0&sid2=1&situation=0");
    assertEquals("Tokens 2 place; situation=0; id situation 1", 0, place1.getSid1());
    assertEquals("Tokens 2 place; situation=0; id situation 2", 1, place1.getSid2());
    assertEquals("Tokens 2 place; situation=0; situation is 1", Situation.SITUATION_ONE, place1.getSituation());
    final TestScenarioBasePlace place2 = ct.getPlace("sid1=0&sid2=1&situation=1");
    assertEquals("Tokens 2 place; situation=1; id situation 1", 0, place2.getSid1());
    assertEquals("Tokens 2 place; situation=1; id situation 2", 1, place2.getSid2());
    assertEquals("Tokens 2 place; situation=1; situation is 1", Situation.SITUATION_TWO, place2.getSituation());
    final TestScenarioBasePlace place3 = ct.getPlace("sid1=0&sid2=1&situation=2");
    assertEquals("Tokens 2 place; situation=2; id situation 1", 0, place3.getSid1());
    assertEquals("Tokens 2 place; situation=2; id situation 2", 1, place3.getSid2());
    assertEquals("Tokens 2 place; situation=2; situation is 1", Situation.COMPARISON, place3.getSituation());
  }

  @Test
  public void testPlace2Token1Situation() {
    final TestCalculatorPlaceTokenizer ct = new TestCalculatorPlaceTokenizer();
    final TestScenarioBasePlace place1 = new TestScenarioBasePlace(0, -1, Situation.SITUATION_ONE);
    assertEquals("Place 2 tokens; 2 sid's and sitation 1", "sid1=0&theme=n", ct.getToken(place1));
  }

  @Test
  public void testPlace2Token2Situations() {
    final TestCalculatorPlaceTokenizer ct = new TestCalculatorPlaceTokenizer();
    final TestScenarioBasePlace place1 = new TestScenarioBasePlace(0, 1, Situation.SITUATION_ONE);
    assertEquals("Place 2 tokens; 2 sid's and sitation 1", "sid2=1&sid1=0&theme=n&situation=0", ct.getToken(place1));
    final TestScenarioBasePlace place2 = new TestScenarioBasePlace(0, 1, Situation.SITUATION_TWO);
    assertEquals("Place 2 tokens; 2 sid's and sitation 2", "sid2=1&sid1=0&theme=n&situation=1", ct.getToken(place2));
    final TestScenarioBasePlace place3 = new TestScenarioBasePlace(0, 1, Situation.COMPARISON);
    assertEquals("Place 2 tokens; 2 sid's and sitation 2", "sid2=1&sid1=0&theme=n&situation=2", ct.getToken(place3));
  }

  @Test
  public void testTokens2Place2SituationsJobPriorityProjectUtilisation() {
    final TestCalculatorPlaceTokenizer ct = new TestCalculatorPlaceTokenizer();
    final TestScenarioBasePlace place1 = ct.getPlace("sid1=0&sid2=1&situation=0&job=12345&jobtype=priority_project_utilisation");
    assertEquals("Tokens 2 place; situation=0; id situation 1", 0, place1.getSid1());
    assertEquals("Tokens 2 place; situation=0; id situation 2", 1, place1.getSid2());
    assertEquals("Tokens 2 place; situation=0; situation is 1", Situation.SITUATION_ONE, place1.getSituation());
    assertEquals("Tokens 2 place; jobkey=12345", JOBKEY1, place1.getJobKey());
    assertEquals("Tokens 2 place; jobkype=priority_project_utilisation", JobType.PRIORITY_PROJECT_UTILISATION, place1.getJobType());
    final TestScenarioBasePlace place2 = ct.getPlace("sid1=0&sid2=1&situation=1&job=54321&jobtype=priority_project_utilisation");
    assertEquals("Tokens 2 place; situation=1; id situation 1", 0, place2.getSid1());
    assertEquals("Tokens 2 place; situation=1; id situation 2", 1, place2.getSid2());
    assertEquals("Tokens 2 place; situation=1; situation is 1", Situation.SITUATION_TWO, place2.getSituation());
    assertEquals("Tokens 2 place; jobkey=54321", JOBKEY2, place2.getJobKey());
    assertEquals("Tokens 2 place; jobkype=priority_project_utilisation", JobType.PRIORITY_PROJECT_UTILISATION, place2.getJobType());
    final TestScenarioBasePlace place3 = ct.getPlace("sid1=0&sid2=1&situation=2&job=1234554321&jobtype=priority_project_utilisation");
    assertEquals("Tokens 2 place; situation=2; id situation 1", 0, place3.getSid1());
    assertEquals("Tokens 2 place; situation=2; id situation 2", 1, place3.getSid2());
    assertEquals("Tokens 2 place; situation=2; situation is 1", Situation.COMPARISON, place3.getSituation());
    assertEquals("Tokens 2 place; jobkey=1234554321", JOBKEY1 + JOBKEY2, place3.getJobKey());
    assertEquals("Tokens 2 place; jobkype=priority_project_utilisation", JobType.PRIORITY_PROJECT_UTILISATION, place3.getJobType());
  }

  @Test
  public void testPlace2Token2SituationsJobPriorityProjectUtilisation() {
    final TestCalculatorPlaceTokenizer ct = new TestCalculatorPlaceTokenizer();
    final TestScenarioBasePlace place1 = new TestScenarioBasePlace(0, 1, Situation.SITUATION_ONE, JOBKEY1, JobType.PRIORITY_PROJECT_UTILISATION);
    assertEquals("Place 2 tokens; 2 sid's and sitation 1; jobKey and jobType",
        "sid2=1&sid1=0&theme=n&job=12345&jobtype=priority_project_utilisation&situation=0", ct.getToken(place1));
    final TestScenarioBasePlace place2 = new TestScenarioBasePlace(0, 1, Situation.SITUATION_TWO, JOBKEY1, JobType.PRIORITY_PROJECT_UTILISATION);
    assertEquals("Place 2 tokens; 2 sid's and sitation 2; jobKey and jobType",
        "sid2=1&sid1=0&theme=n&job=12345&jobtype=priority_project_utilisation&situation=1", ct.getToken(place2));
    final TestScenarioBasePlace place3 = new TestScenarioBasePlace(0, 1, Situation.COMPARISON, JOBKEY1, JobType.PRIORITY_PROJECT_UTILISATION);
    assertEquals("Place 2 tokens; 2 sid's and sitation 2; jobKey and jobType",
        "sid2=1&sid1=0&theme=n&job=12345&jobtype=priority_project_utilisation&situation=2", ct.getToken(place3));
  }
}
