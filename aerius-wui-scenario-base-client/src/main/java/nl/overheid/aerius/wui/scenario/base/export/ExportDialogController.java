/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.export;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.domain.ScenarioUtil;
import nl.overheid.aerius.wui.scenario.base.export.ExportDialogPanel.ExportDialogDriver;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.processor.ExportProcessor;

/**
 * Controller for starting the Export dialog and communicating with the server.
 */
@Singleton
public class ExportDialogController {

  private final SimpleBeanEditorDriver<ExportProperties, ExportDialogPanel> driver = GWT.create(ExportDialogDriver.class);
  private final ConfirmCancelDialog<ExportProperties, ExportDialogPanel> dialog;
  private final ExportDialogPanel contentPanel;
  private final ExportProcessor exportProcessor;

  private final ConfirmHandler<ExportProperties> confirmHandler = new ConfirmHandler<ExportProperties>() {
    @Override
    public void onConfirm(final ConfirmEvent<ExportProperties> event) {
      dialog.hide();
      final ExportProperties properties = event.getValue();

      final boolean useResults = userContext.getCalculatedScenario() != null && properties.getExportType() == ExportType.OPS;
      final CalculatedScenario cs =
          useResults ? userContext.getCalculatedScenario() : ScenarioUtil.toCalculatedScenario(userContext.getScenario(), place);

      cs.setOptions(appContext.getCalculationOptions().copy());
      exportProcessor.start(properties, cs);
    }
  };
  private final ExportProperties exportProperties;
  private ScenarioBaseUserContext userContext;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private ScenarioBasePlace place;

  @Inject
  public ExportDialogController(final ExportProcessor exportProcessor, final HelpPopupController hpC, final ScenarioBaseAppContext<?, ?> appContext) {
    this.exportProcessor = exportProcessor;
    this.contentPanel = new ExportDialogPanel(hpC);
    this.appContext = appContext;
    dialog = new ConfirmCancelDialog<>(driver, M.messages().exportGo(), M.messages().cancelButton());

    // TODO Internet Explorer, as expected, won't handle the animation correctly.
    dialog.setAnimationEnabled(false);

    dialog.addConfirmHandler(confirmHandler);
    dialog.setWidget(contentPanel);
    contentPanel.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Boolean> event) {
        //When the content changes because another option selected, re-center the dialog
        dialog.center();
      }
    });
    exportProperties = new ExportProperties();
    exportProperties.setLocale(LocaleInfo.getCurrentLocale().getLocaleName());
  }

  /**
   * Show the dialog with the given scenario as input to used to export.
   *
   *  @param userContext Scenario to export
   *  @param cPlace current place
   *  @param year the year to export
   */
  public void show(final ScenarioBaseUserContext userContext, final Place cPlace, final int year) {
    show(userContext, cPlace, year, null);
  }

  /**
   * Show the dialog with the given scenario as input to used to export.
   *
   *  @param userContext Scenario to export
   *  @param cPlace current place
   *  @param year the year to export
   *  @param soleType the default type to select
   */
  public void show(final ScenarioBaseUserContext userContext, final Place cPlace, final int year, final ExportType soleType) {
    this.userContext = userContext;
    this.place = cPlace instanceof ScenarioBasePlace ? (ScenarioBasePlace) cPlace : null;
    exportProperties.setYear(year);
    exportProperties.setScenarioMetaData(userContext.getMetaData());
    driver.initialize(contentPanel);
    driver.edit(exportProperties);
    contentPanel.setDefaultTypeSelection(soleType);
    if (place != null) {
      final boolean has2Situations = place.has2Situations();
      dialog.setHTML(has2Situations
          ? M.messages().exportBothSituations()
          : M.messages().export(userContext.getSources(place.getActiveSituationId()).getName()));
      dialog.center();
      contentPanel.setSituationSelection(has2Situations);
    } // else unknown place, so can't display dialog with correct information.
  }
}
