/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.shared.util.FormatUtil;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.StyledPointColumn;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class CalculationPointDivTable extends Composite implements IsInteractiveDataTable<AeriusPoint> {
  interface CalculationPointDivTableUiBinder extends UiBinder<Widget, CalculationPointDivTable> {}

  private static final CalculationPointDivTableUiBinder UI_BINDER = GWT.create(CalculationPointDivTableUiBinder.class);

  private static final double SHOW_VALUE_MARGIN = 0.005;

  private boolean diff;

  interface CustomStyle extends CssResource {

    String depositionColumn();

  }

  @UiField CustomStyle style;

  private EmissionResultKey key;

  @UiField SimpleInteractiveClickDivTable<AeriusPoint> divTable;

  @UiField(provided = true) StyledPointColumn<AeriusPoint> idColumn;
  @UiField(provided = true) TextColumn<AeriusPoint> labelColumn;

  @Inject
  public CalculationPointDivTable(final EventBus eventBus, final UserContext userContext) {
    this(eventBus, userContext, false);
  }

  protected CalculationPointDivTable(final EventBus eventBus, final UserContext userContext, final boolean hideResults) {
    // Row number column
    idColumn = new StyledPointColumn<AeriusPoint>(ColorUtil.black(), eventBus) {
      @Override
      protected String getText(final AeriusPoint object) {
        return FormatUtil.formatAlphabetical(object.getId());
      }

      @Override
      protected String getColor(final AeriusPoint object) {
        return ColorUtil.webColor(R.css().colorCalculationPoint());
      }
    };

    // Label column
    labelColumn = new TextColumn<AeriusPoint>() {
      @Override
      public String getValue(final AeriusPoint object) {
        return object.getLabel();
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    // Dep column
    if (!hideResults) {
      final TextColumn<AeriusPoint> depositionColumn = new TextColumn<AeriusPoint>() {
        @Override
        public String getValue(final AeriusPoint point) {
          String value = "";

          if ((point instanceof AeriusResultPoint) && (diff || ((AeriusResultPoint) point).hasEmissionResult(key))) {
            final double emissionResult = ((AeriusResultPoint) point).getEmissionResult(key);
            value = nl.overheid.aerius.wui.main.util.FormatUtil.formatEmissionResultWithUnit(key.getEmissionResultType(), emissionResult,
                userContext.getEmissionResultValueDisplaySettings());
            if (emissionResult >= SHOW_VALUE_MARGIN) {
              value = (diff ? "+" : "") + value;
            }
          }

          return value;
        }
      };

      //divTable.addColumn(depositionColumn, style.depositionColumn(), R.css().number());
      depositionColumn.ensureDebugId(TestID.DEPOSITION);
      depositionColumn.setCellStyle(style.depositionColumn());
      divTable.addColumn(depositionColumn);
    }

    idColumn.ensureDebugId(TestID.ID);
    labelColumn.ensureDebugId(TestID.LABEL);

    divTable.ensureDebugId(TestID.TABLE);
    divTable.setSelectionModel(new SingleSelectionModel<AeriusPoint>());
  }

  /**
   * Set if the results should interpreted as differences or as normal values. If shown as differences a + is shown and a color when the value is positive. Else the value is shown as is.
   *
   * @param isDiff
   *          true if results should be interpreted as difference results
   */
  public void setDiff(final boolean isDiff) {
    diff = isDiff;
  }

  public void setEmissionResultKey(final EmissionResultKey key) {
    if (key != null) {
      this.key = key;
    }
  }

  @Override
  public SimpleInteractiveClickDivTable<AeriusPoint> asDataTable() {
    return divTable;
  }
}
