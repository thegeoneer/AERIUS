/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.LodgingStackType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.FarmReductiveLodgingSystem;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SuggestBox.NameTextMachine;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;

class FarmReductiveRowEditor extends FarmRowEditor<FarmReductiveLodgingSystemCategory, FarmReductiveLodgingSystem> implements
    HasValueChangeHandlers<FarmReductiveLodgingSystemCategory> {
  private static final FarmReductiveRowEditorUiBinder UI_BINDER = GWT.create(FarmReductiveRowEditorUiBinder.class);

  interface FarmReductiveRowEditorUiBinder extends UiBinder<Widget, FarmReductiveRowEditor> {
  }

  @UiField(provided = true) SystemDefinitionEditor systemDefinitionEditor;

  private static final TextMachine<FarmReductiveLodgingSystemCategory> REDUCTIVE_NAMER = new NameTextMachine<FarmReductiveLodgingSystemCategory>() {
    @Override
    public String asError(final String input) {
      return M.messages().ivFarmNoValidRavCode(input);
    }
  };

  public FarmReductiveRowEditor(final SectorCategories categories, final HelpPopupController hpC,
      final FarmLodgingCategorySelection categorySelection) {
    super(categories, hpC,  new ValidateLodgingSystemHandler<FarmReductiveLodgingSystemCategory>(categorySelection) {
      @Override
      boolean canStack(final FarmLodgingCategory lodgingCategory, final FarmReductiveLodgingSystemCategory value) {
        return lodgingCategory.canStackReductiveLodgingSystemCategory(value);
      }
    }, REDUCTIVE_NAMER);

    systemDefinitionEditor = new SystemDefinitionEditor();

    getRow().insert(UI_BINDER.createAndBindUi(this), 0);

    systemDefinitionEditor.addValidator(new Validator<FarmLodgingSystemDefinition>() {
      @Override
      public String validate(final FarmLodgingSystemDefinition value) {
        return value == null ? M.messages().ivFarmNoValidBWLCode() : null;
      }
    });

    StyleUtil.I.setPlaceHolder(categoryEditor.getValueBox(), M.messages().farmLodgingStackType(LodgingStackType.REDUCING_MEASURE));
    hpC.addWidget(categoryEditor, hpC.tt().ttFarmLodgingReductiveCategory());
    hpC.addWidget(systemDefinitionEditor, hpC.tt().ttFarmLodgingSystemDefinition());
  }

  @Override
  ArrayList<FarmReductiveLodgingSystemCategory> getCorrectCategories(final SectorCategories categories) {
    return categories.getFarmLodgingCategories().getFarmReductiveLodgingSystemCategories();
  }

  @Override
  public void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    systemDefinitionEditor.ensureDebugId(baseID + "-" + TestID.BWL_SUGGEST_BOX);
  }

  @Override
  void onCategoryValueChanged(final FarmReductiveLodgingSystemCategory newCategory) {
    systemDefinitionEditor.updateSystemDefinitionList(newCategory, null);
  }

  @Override
  public void setValue(final FarmReductiveLodgingSystem value) {
    super.setValue(value);
    systemDefinitionEditor.updateSystemDefinitionList(value.getCategory(), value.getSystemDefinition());
  }

}
