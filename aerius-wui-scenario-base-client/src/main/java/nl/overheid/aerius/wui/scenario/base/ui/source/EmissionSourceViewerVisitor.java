/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;

import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.domain.source.ShippingEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.wui.scenario.base.ui.source.EmissionSourceViewerVisitor.DriverViewerCombo;
import nl.overheid.aerius.wui.scenario.base.ui.source.FarmEmissionViewer.FarmEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.ui.source.GenericEmissionViewer.GenericEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.ui.source.OffRoadMobileEmissionViewer.OffRoadMobileEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.ui.source.SRM2EmissionViewer.SRM2EmissionDriver;
import nl.overheid.aerius.wui.scenario.base.ui.source.ShippingEmissionViewer.ShippingEmissionDriver;

/**
 * Visitor pattern to select the editor to use.
 */
@SuppressWarnings("rawtypes")
class EmissionSourceViewerVisitor implements EmissionSourceVisitor<DriverViewerCombo> {

  static class DriverViewerCombo<S extends EmissionSource, E extends EmissionViewer<S>, D extends SimpleBeanEditorDriver> {
    E editor;
    D driver;

    @SuppressWarnings("unchecked")
    public DriverViewerCombo(final E editor, final D driver) {
      this.editor = editor;
      this.driver = driver;
      driver.initialize(editor);
    }

    public D getDriver() {
      return driver;
    }

    public E getEditor() {
      return editor;
    }
  }

  private final DriverViewerCombo<? extends EmissionSource, GenericEmissionViewer, GenericEmissionDriver> genericViewer;
  private final DriverViewerCombo<FarmEmissionSource, FarmEmissionViewer, FarmEmissionDriver> farmViewer;
  private final DriverViewerCombo<ShippingEmissionSource<?>, ShippingEmissionViewer, ShippingEmissionDriver> shippingViewer;
  private final DriverViewerCombo<SRM2EmissionSource, SRM2EmissionViewer, SRM2EmissionDriver> srm2Viewer;
  private final DriverViewerCombo<OffRoadMobileEmissionSource, OffRoadMobileEmissionViewer, OffRoadMobileEmissionDriver> offroadViewer;

  public EmissionSourceViewerVisitor() {
    genericViewer = new DriverViewerCombo<EmissionSource, GenericEmissionViewer, GenericEmissionDriver>(
        new GenericEmissionViewer(), (GenericEmissionDriver) GWT.create(GenericEmissionDriver.class));

    farmViewer = new DriverViewerCombo<FarmEmissionSource, FarmEmissionViewer, FarmEmissionDriver>(
        new FarmEmissionViewer(), (FarmEmissionDriver) GWT.create(FarmEmissionDriver.class));

    shippingViewer = new DriverViewerCombo<ShippingEmissionSource<?>, ShippingEmissionViewer, ShippingEmissionDriver>(
        new ShippingEmissionViewer(), (ShippingEmissionDriver) GWT.create(ShippingEmissionDriver.class));

    srm2Viewer = new DriverViewerCombo<SRM2EmissionSource, SRM2EmissionViewer, SRM2EmissionDriver>(
        new SRM2EmissionViewer(), (SRM2EmissionDriver) GWT.create(SRM2EmissionDriver.class));

    offroadViewer = new DriverViewerCombo<OffRoadMobileEmissionSource, OffRoadMobileEmissionViewer, OffRoadMobileEmissionDriver>(
        new OffRoadMobileEmissionViewer(), (OffRoadMobileEmissionDriver) GWT.create(OffRoadMobileEmissionDriver.class));
  }

  @Override
  public DriverViewerCombo visit(final GenericEmissionSource emissionSource) throws AeriusException {
    return genericViewer;
  }

  @Override
  public DriverViewerCombo visit(final FarmEmissionSource emissionSource) throws AeriusException {
    return farmViewer;
  }

  @Override
  public DriverViewerCombo visit(final InlandMooringEmissionSource emissionSource) throws AeriusException {
    return shippingViewer;
  }

  @Override
  public DriverViewerCombo visit(final InlandRouteEmissionSource emissionSource) throws AeriusException {
    return shippingViewer;
  }

  @Override
  public DriverViewerCombo visit(final MaritimeMooringEmissionSource emissionSource) throws AeriusException {
    return shippingViewer;
  }

  @Override
  public DriverViewerCombo visit(final MaritimeRouteEmissionSource emissionSource) throws AeriusException {
    return shippingViewer;
  }

  @Override
  public DriverViewerCombo visit(final OffRoadMobileEmissionSource emissionSource) throws AeriusException {
    return offroadViewer;
  }

  @Override
  public DriverViewerCombo visit(final PlanEmissionSource emissionSource) throws AeriusException {
    // PlanEmissionSource viewer needs a UI rework
    return genericViewer;
  }

  @Override
  public DriverViewerCombo visit(final SRM2EmissionSource emissionSource) throws AeriusException {
    return srm2Viewer;
  }

  @Override
  public DriverViewerCombo visit(final SRM2NetworkEmissionSource emissionSource) throws AeriusException {
    // SRM2 Network EmissionSource viewer needs a UI rework
    return genericViewer;
  }

  public void onEnsureDebugId(final String baseID) {
    genericViewer.getEditor().ensureDebugId(baseID);
    farmViewer.getEditor().ensureDebugId(baseID);
    shippingViewer.getEditor().ensureDebugId(baseID);
  }


}