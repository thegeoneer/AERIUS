/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.UseBuildingChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.validation.DoubleRangeValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.InputWithUnitWidget;

/**
 * Editor for {@link OPSSourceCharacteristics}.
 */
public class OPSSourceOutflowNotForcedEditor extends Composite implements Editor<OPSSourceCharacteristics>, HasValueChangeHandlers<Boolean> {

  public interface OPSSourceOutflowNotForcedDriver extends SimpleBeanEditorDriver<OPSSourceCharacteristics, OPSSourceOutflowNotForcedEditor> {}

  interface PSSourceOutflowNotForcedEventBinder extends EventBinder<OPSSourceOutflowNotForcedEditor> {}
  interface OPSSourceOutflowWidgetUiBinder extends UiBinder<Widget, OPSSourceOutflowNotForcedEditor> {}

  private static final PSSourceOutflowNotForcedEventBinder EVENT_BINDER = GWT.create(PSSourceOutflowNotForcedEventBinder.class);
  private static final OPSSourceOutflowWidgetUiBinder UI_BINDER = GWT.create(OPSSourceOutflowWidgetUiBinder.class);

  @UiField(provided = true) @Path("") OPSSourceBuildingEditor buildingEditor;

  @UiField @Ignore InputWithUnitWidget emissionHeightWidget;
  @UiField(provided = true) DoubleValueBox emissionHeightEditor = new DoubleValueBox(M.messages().sourceHeight(),
      OPSLimits.SOURCE_HEIGHT_DIGITS_PRECISION);
  @UiField(provided = true) DoubleValueBox heatContentEditor = new DoubleValueBox(M.messages().sourceHeatContent(),
      OPSLimits.SOURCE_HEAT_CONTENT_DIGITS_PRECISION);


  private final DoubleRangeValidator emissionHeightValidator = new DoubleRangeValidator(OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM,
      OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivStringRangeBetween(M.messages().sourceHeight(),
          M.messages().decimalNumberFixed(OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM, 0),
          M.messages().decimalNumberFixed(OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM, OPSLimits.SOURCE_HEIGHT_DIGITS_PRECISION));
    }
  };
  private final DoubleRangeValidator emissionHeightValidatorWarning = new DoubleRangeValidator(OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM,
      OPSLimits.SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivDoubleRangeBetween(M.messages().sourceHeight(),
          OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM,
          OPSLimits.SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM);
    }
  };
  private final DoubleRangeValidator heatContentValidator = new DoubleRangeValidator(0D, Double.valueOf(OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM)) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivRangeBetween(M.messages().sourceHeatContent(), 0, OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM);
    }
  };

  private boolean buildingInfluence;
  private Double oldHeatContent;

  /**
   * The characteristics of OPS-outflow sources may be set in this editor.
   *
   * @param hpC the popup-controller.
   * @param opsFactsheetUrl
   */
  public OPSSourceOutflowNotForcedEditor(final EventBus eventBus, final HelpPopupController hpC, final String opsFactsheetUrl) {
    buildingEditor = new OPSSourceBuildingEditor(eventBus, hpC, opsFactsheetUrl);
    initWidget(UI_BINDER.createAndBindUi(this));
    heatContentEditor.addValidator(heatContentValidator);

    hpC.addWidget(emissionHeightEditor, hpC.tt().ttOPSHeight());
    hpC.addWidget(heatContentEditor, hpC.tt().ttOPSHeatContent());
    emissionHeightEditor.ensureDebugId(TestID.INPUT_OPS_HEIGHT);
    heatContentEditor.ensureDebugId(TestID.INPUT_OPS_HEATCONTENT);

  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @EventHandler
  void onBuildingChange(final UseBuildingChangeEvent e) {
    setBuildingInfluence(e.getValue());
  }

  @UiHandler({"emissionHeightEditor", "heatContentEditor"})
  void onChangeEvent(final ChangeEvent e) {
    validateWarnings();
  }

  private void validateWarnings() {
    emissionHeightEditor.setValue(emissionHeightEditor.getValue());
    if (heatContentEditor.getValue() > 0) {
      heatContentEditor.setValue(heatContentEditor.getValue());
    }
    if (buildingInfluence) {
      final boolean invalid = emissionHeightValidatorWarning.validate(emissionHeightEditor.getValue()) != null;
      emissionHeightWidget.setStyleName(R.css().suggestionWarningBox(), invalid);
      buildingEditor.setOtherWarning(invalid);
    } else {
      emissionHeightWidget.setStyleName(R.css().suggestionWarningBox(), false);
      buildingEditor.setOtherWarning(false);
    }
  }

  public void setBuildingEditorVisible(final boolean visible) {
    buildingEditor.setVisible(visible);
  }

  public void setBuildingInfluence(final boolean buildingInfluence) {
    this.buildingInfluence = buildingInfluence;
    if (buildingInfluence) {
      emissionHeightEditor.removeValidator(emissionHeightValidator);
      oldHeatContent = heatContentEditor.getValue();
      if (emissionHeightEditor.getValue() > OPSLimits.SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM) {
        emissionHeightEditor.setValue(OPSLimits.SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM);
      }
      heatContentEditor.setValue(0D);
    } else {
      emissionHeightEditor.addValidator(emissionHeightValidator);
      if (oldHeatContent != null) {
        heatContentEditor.setValue(oldHeatContent);
      }
    }
    heatContentEditor.setEnabled(!buildingInfluence);
    validateWarnings();
  }

  public void setEventBus(final EventBus eventBus) {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

}
