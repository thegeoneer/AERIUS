/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.road;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.wui.main.widget.ParameterizedSuggestOracle;
import nl.overheid.aerius.wui.main.widget.ParameterizedSuggestion;

/**
 * MultiWordSuggestOracle to display OnRoadMobileSourceCategory items.
 */
class MobileSourceCategorySuggestOracle extends ParameterizedSuggestOracle<OnRoadMobileSourceCategory> {
  private static final int MIN_DESCRIPTION_LENGTH = 3;

  /**
   * Inner class defining how a suggestion for the given category should be displayed.
   */
  public class MobileSourceCategorySuggestion extends ParameterizedSuggestion<OnRoadMobileSourceCategory> {
    /**
     * @param category The category to display as suggestion.
     */
    public MobileSourceCategorySuggestion(final OnRoadMobileSourceCategory category) {
      super(category);
    }

    @Override
    public String getDisplayString() {
      return object.getName() + " " + object.getDescription();
    }

    @Override
    public String getReplacementString() {
      return object.getName();
    }
  }

  /**
   * @param categories The list of categories to choose from.
   */
  public MobileSourceCategorySuggestOracle(final ArrayList<OnRoadMobileSourceCategory> categories) {
    super(categories);
  }

  @Override
  protected Suggestion getSuggestion(final String query, final OnRoadMobileSourceCategory cat) {
    if (query.isEmpty()) {
      return null;
    }

    final String transformedQuery = query.replace(" ", "");

    final String name = cat.getName().toLowerCase().replace(" ", "");
    final String description = cat.getDescription() == null ? null : cat.getDescription().toLowerCase().replace(" ", "");

    if (name.contains(transformedQuery)
        || transformedQuery.length() >= MIN_DESCRIPTION_LENGTH && description != null && description.contains(transformedQuery)) {
      return getSuggestion(cat);
    }

    return null;
  }

  @Override
  protected Suggestion getSuggestion(final OnRoadMobileSourceCategory cat) {
    return new MobileSourceCategorySuggestion(cat);
  }
}
