/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.source.FarmFodderMeasure;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * Row for {@link FarmFodderMeasure} for emission of a reductive measurements.
 */
class FarmFodderMeasureRowViewer extends FarmEffectRowViewer<FarmFodderMeasure> {

  private static final FarmFodderMeasureRowViewerUiBinder UI_BINDER = GWT.create(FarmFodderMeasureRowViewerUiBinder.class);

  @UiTemplate("FarmEffectRowViewer.ui.xml")
  interface FarmFodderMeasureRowViewerUiBinder extends UiBinder<Widget, FarmFodderMeasureRowViewer> {
  }

  public FarmFodderMeasureRowViewer(final FarmLodgingStandardEmissions source) {
    super(source);

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public void setValue(final FarmFodderMeasure value) {
    //override because we can't determine intermediate reduction factors. Only show numbers for the last one showed.
    if (value != null) {
      setValueTexts(value);

      if (isLast(value)) {
        amountValue.setText(M.messages().farmNoValue());
        factorValue.setText(M.messages().farmNoValue());

        final double reductionPct = source.getReductionFactorCombinedFodderMeasures(EMISSION_VALUE_KEY) * SharedConstants.PERCENTAGE_TO_FRACTION;
        effectValue.setText(M.messages().unitPercentage(FormatUtil.toWhole(reductionPct)));

        final double emission = source.getEmission(EMISSION_VALUE_KEY);
        emissionValue.setText(FormatUtil.formatEmissionWithUnit(emission));
      }
    }
  }

  @Override
  protected void setValueTexts(final FarmFodderMeasure value) {
    headerValue.setText(value.getCategory().getName());
    amountValue.setText("");
    factorValue.setText("");
    effectValue.setText("");
    emissionValue.setText("");
  }

  @Override
  protected boolean isLast(final FarmFodderMeasure value) {
    return source.getFodderMeasures().indexOf(value) == source.getFodderMeasures().size() - 1;
  }

}
