/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import java.util.HashSet;
import java.util.List;

import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.geometry.Polygon;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.SimpleFilter;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.domain.ScenarioUtil;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Emission result layer for custom points.
 */
public class CustomEmissionResultLayer extends EmissionResultLayer {
  private static final double CIRCLE_SURFACE = 100 * 100;
  private static final double CIRCLE_RADIUS = Math.sqrt(CIRCLE_SURFACE / Math.PI);
  private static final double CIRCLE_SCALE = 6771.0002551901260;
  private static final double CIRCLE_GROWTH_FACTOR = 1.5;
  private static final int CIRCLE_PRECISION = 40;
  private static final String CUSTOM_RESULT_LAYER = "custom_result_layer";

  private final ScenarioBaseUserContext userContext;

  public CustomEmissionResultLayer(final MapLayoutPanel map, final ScenarioBasePlace place, final ScenarioBaseUserContext userContext,
      final EmissionResultKey initalKey, final EmissionResultValueDisplaySettings displaySettings) {
    super(CUSTOM_RESULT_LAYER, map, initalKey, place, displaySettings);
    this.userContext = userContext;
  }

  @Override
  public void setValues(final int calculationId, final HashSet<AeriusResultPoint> values) {
    refreshLayer();
  }

  @Override
  public void setFilter(final SimpleFilter<AeriusResultPoint> filter) {
    // No-op, this layer doesn't support filters.
  }

  @Override
  protected void drawFeatures() {
    final ScenarioBasePlace place = getPlace();
    final EmissionResultKey key = getKey();

    clearFeatures();

    final List<AeriusResultPoint> results = ScenarioUtil.getCalculatedCalculationPoints(userContext.getCalculatedScenario(), place);
    if (!results.isEmpty()) {

      // Draw new ones for each calculation point.
      final double radius = ((((CIRCLE_GROWTH_FACTOR - 1) * CIRCLE_RADIUS) / CIRCLE_SCALE) * getMap().getScale())
          + ((2 - CIRCLE_GROWTH_FACTOR) * CIRCLE_RADIUS);
      for (final AeriusResultPoint p : results) {
        final VectorFeature feature = new VectorFeature(getCircle(p, radius));
        final double emissionResult = p.getEmissionResult(key);
        feature.setStyle(place.getSituation() == Situation.COMPARISON
            ? ComparisonStyleUtil.getStyle(emissionResult)
                : ReceptorStyleUtil.getStyle(key.getEmissionResultType(), emissionResult));

        addFeature(feature);
      }
    }
    redraw();
  }

  @Override
  protected void clearFeatures() {
    destroyFeatures();
  }

  private Geometry getCircle(final Point p, final double radius) {
    return Polygon.createRegularPolygon(new org.gwtopenmaps.openlayers.client.geometry.Point(p.getX(), p.getY()), radius, CIRCLE_PRECISION);
  }

  @Override
  public String getTitle() {
    return getName();
  }

  @Override
  public void setTitle(final String name) {
    setName(name);
  }
}
