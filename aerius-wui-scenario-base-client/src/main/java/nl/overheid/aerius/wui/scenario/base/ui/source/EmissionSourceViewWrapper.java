/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

class EmissionSourceViewWrapper extends Composite {
  private static final EmissionSourceViewWrapperUiBinder UI_BINDER = GWT.create(EmissionSourceViewWrapperUiBinder.class);

  interface EmissionSourceViewWrapperUiBinder extends UiBinder<Widget, EmissionSourceViewWrapper> {}

  @UiField FlowPanel container;

  @UiField SimplePanel characteristics;
  @UiField SimplePanel emissionValues;

  public EmissionSourceViewWrapper() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  public void setEmissionValues(final IsWidget w) {
    emissionValues.setWidget(w);
  }

  public void setCharacteristics(final IsWidget w) {
    characteristics.setWidget(w);
  }
}
