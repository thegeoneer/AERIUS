/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.calculators;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleConsumptionSpecification;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleOperatingHoursSpecification;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleSpecification;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup.RadioButtonContentResource;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;
import nl.overheid.aerius.wui.scenario.base.source.calculators.OffRoadEmissionConsumptionEditor.OffRoadEmissionConsumptionDriver;
import nl.overheid.aerius.wui.scenario.base.source.calculators.OffRoadEmissionRunningHoursEditor.OffRoadEmissionRunningHoursDriver;

class OffRoadEmissionEditorWrapper extends Composite {
  public static enum CalculatorEditorType {
    RUNNING_HOURS, CONSUMPTION;
  }

  interface OffRoadEmissionEditorWrapperUiBinder extends UiBinder<Widget, OffRoadEmissionEditorWrapper> {}

  private static final OffRoadEmissionEditorWrapperUiBinder UI_BINDER = GWT.create(OffRoadEmissionEditorWrapperUiBinder.class);

  private static final int EMISSION_RESULT_PRECISION = 2;

  private final OffRoadEmissionConsumptionDriver consumptionDriver = GWT.create(OffRoadEmissionConsumptionDriver.class);
  private final OffRoadEmissionRunningHoursDriver runningHoursDriver = GWT.create(OffRoadEmissionRunningHoursDriver.class);

  @UiField(provided = true) RadioButtonGroup<CalculatorEditorType> toggleButton;

  @UiField SwitchPanel customCalculationEditor;

  @UiField(provided = true) OffRoadEmissionRunningHoursEditor runningHoursEditor;
  @UiField(provided = true) OffRoadEmissionConsumptionEditor consumptionEditor;

  @UiField Label emissionResult;

  private final LeafValueEditor<OffRoadVehicleSpecification> specEditor = new LeafValueEditor<OffRoadVehicleSpecification>() {
    @Override
    public void setValue(final OffRoadVehicleSpecification value) {
      CalculatorEditorType type = CalculatorEditorType.RUNNING_HOURS;
      final OffRoadVehicleConsumptionSpecification cs;
      if (value instanceof OffRoadVehicleConsumptionSpecification) {
        cs = (OffRoadVehicleConsumptionSpecification) value;
        type = CalculatorEditorType.CONSUMPTION;
      } else {
        cs = new OffRoadVehicleConsumptionSpecification();
      }
      consumptionDriver.edit(cs);
      final OffRoadVehicleOperatingHoursSpecification ohs;
      if (value instanceof OffRoadVehicleOperatingHoursSpecification) {
        ohs = (OffRoadVehicleOperatingHoursSpecification) value;
        type = CalculatorEditorType.RUNNING_HOURS;
      } else {
        ohs = new OffRoadVehicleOperatingHoursSpecification();
      }
      runningHoursDriver.edit(ohs);
      toggleButton.setValue(type, true);
    }

    @Override
    public OffRoadVehicleSpecification getValue() {
      if (toggleButton.getValue() == null) {
        return consumptionDriver.flush();
      }

      switch (toggleButton.getValue()) {
      case RUNNING_HOURS:
        return runningHoursDriver.flush();
      case CONSUMPTION:
        return consumptionDriver.flush();
      default:
        return null;
      }
    }
  };

  public OffRoadEmissionEditorWrapper(final ScenarioBaseContext context) {
    runningHoursEditor = new OffRoadEmissionRunningHoursEditor(context);
    consumptionEditor = new OffRoadEmissionConsumptionEditor(context);

    runningHoursDriver.initialize(runningHoursEditor);
    consumptionDriver.initialize(consumptionEditor);

    toggleButton = new RadioButtonGroup<CalculatorEditorType>(new RadioButtonContentResource<CalculatorEditorType>() {
      @Override
      public String getRadioButtonText(final CalculatorEditorType value) {
        return M.messages().offRoadEmissionCalculatorType(value.name());
      }
    });
    toggleButton.addButtons(CalculatorEditorType.values());
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    toggleButton.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_TOGGLE_BUTTON);
    emissionResult.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_RESULT);
  }

  @UiHandler({"consumptionEditor","runningHoursEditor"})
  public void onValueChange(final ValueChangeEvent<Double> e) {
    setEmission(e.getValue());
  }

  private void setEmission(final Double emission) {
    emissionResult.setText(M.messages().unitKgY(FormatUtil.toFixed(emission, EMISSION_RESULT_PRECISION)));
  }

  @UiHandler("toggleButton")
  public void onEditorTypeChangeEvent(final ValueChangeEvent<CalculatorEditorType> e) {
    switch (e.getValue()) {
    case RUNNING_HOURS:
      customCalculationEditor.showWidget(0);
      runningHoursEditor.refresh();
      break;
    case CONSUMPTION:
      customCalculationEditor.showWidget(1);
      consumptionEditor.refresh();
      break;
    default:

    }
  }

  public void setHpC(final HelpPopupController hpC) {
    // No-op
  }

  public void setValue(final OffRoadVehicleSpecification value) {
    specEditor.setValue(value);
    setEmission(value == null ? 0 : value.getEmission());
  }

  public OffRoadVehicleSpecification getValue() {
    // If the result is 0, the spec must be null.
    final OffRoadVehicleSpecification value = specEditor.getValue();
    return value.getEmission() == 0D ? null : value;
  }

  public void setSector(final Sector sector) {
    runningHoursEditor.setSector(sector);
    consumptionEditor.setSector(sector);
  }
}
