/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.progress;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.UIObject;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.result.EmissionResultColorRanges;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * A single vertical bar in the progress panel.
 */
class ProgressPanelBar extends UIObject {
  interface ProgressPanelBarUiBinder extends UiBinder<Element, ProgressPanelBar> {}

  private static final ProgressPanelBarUiBinder UI_BINDER = GWT.create(ProgressPanelBarUiBinder.class);

  interface CustomStyle extends CssResource {
    String threshold();

    String inactive();
  }

  @UiField CustomStyle style;
  @UiField DivElement column;
  @UiField DivElement bar;

  private EmissionResultKey emissionResultKey;
  private final HashMap<EmissionResultKey, Double> emissionResults = new HashMap<>();
  private EmissionResultValueDisplaySettings displaySettings;

  public ProgressPanelBar(final EmissionResultKey emissionResultKey, final EmissionResultValueDisplaySettings displaySettings) {
    this.emissionResultKey = emissionResultKey;
    this.displaySettings = displaySettings;

    setElement(UI_BINDER.createAndBindUi(this));
  }

  public void setEmissionResultValueDisplaySettings(final EmissionResultValueDisplaySettings displaySettings) {
    this.displaySettings = displaySettings;

    draw();
  }

  public void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;

    draw();
  }

  public void setMaxEmissionResult(final EmissionResultKey key, final double emissionResult) {
    if ((emissionResults.get(key) == null) || (emissionResults.get(key) < emissionResult)) {
      emissionResults.put(key, emissionResult);
    }

    draw();
  }

  public double getEmissionResult() {
    return getEmissionResult(emissionResultKey);
  }

  private double getEmissionResult(final EmissionResultKey key) {
    if ((key != null) && (key.getSubstance() == Substance.NOXNH3)) {
      return getEmissionResult(EmissionResultKey.valueOf(Substance.NH3, key.getEmissionResultType()))
          + getEmissionResult(EmissionResultKey.valueOf(Substance.NOX, key.getEmissionResultType()));
    }

    if (emissionResults.get(key) == null) {
      return 0;
    }

    return emissionResults.get(key);
  }

  private void draw() {
    if (emissionResultKey == null) {
      return;
    }
    final double emissionResult = getEmissionResult();
    column.setTitle(FormatUtil.formatEmissionResultWithUnit(emissionResultKey.getEmissionResultType(), emissionResult, displaySettings));
    final EmissionResultColorRanges colorRanges = EmissionResultColorRanges.valueOf(emissionResultKey.getEmissionResultType());
    bar.getStyle().setBackgroundColor(ColorUtil.webColor(colorRanges.getColor(emissionResult)));
  }

  public void setWidth(final double width) {
    getElement().getStyle().setWidth(width, Unit.PX);
  }

  public void setBarHeight(final double height) {
    bar.getStyle().setHeight(height, Unit.PX);
  }

  public void setMaxDistanceThreshold(final boolean enable) {
    if (enable) {
      column.addClassName(style.threshold());
    } else {
      column.removeClassName(style.threshold());
    }
  }

  public void setActive(final boolean active) {
    if (active) {
      column.removeClassName(style.inactive());
    } else {
      column.addClassName(style.inactive());
    }
  }

  public void notifyFirst(final boolean b) {
    column.getStyle().setBorderStyle(b ? BorderStyle.NONE : BorderStyle.SOLID);
  }
}
