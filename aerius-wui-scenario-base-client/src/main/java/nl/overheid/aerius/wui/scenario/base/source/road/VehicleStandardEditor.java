/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.road;

import java.util.TreeSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.sector.category.VehicleType;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.editor.LeafValueAwareEditor;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.DoubleRangeValidator;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Editor for Standard Vehicle item.
 */
class VehicleStandardEditor extends AbstractVehicleEditor<VehicleStandardEmissions> {

  interface VehicleStandardDriver extends SimpleBeanEditorDriver<VehicleStandardEmissions, VehicleStandardEditor> {
  }

  interface VehicleStandardEditorUiBinder extends UiBinder<Widget, VehicleStandardEditor> {
  }

  private static final VehicleStandardEditorUiBinder UI_BINDER = GWT.create(VehicleStandardEditorUiBinder.class);

  private static final int SELECT_SPEED = 0;

  private final RoadEmissionCategories roadEmissionCategories;

  @UiField(provided = true) DoubleValueBox stagnationFractionEditor = new DoubleValueBox(M.messages().roadStagnation()) {
    @Override
    public Double getValueOrThrow() throws java.text.ParseException {
      return super.getValueOrThrow() / SharedConstants.PERCENTAGE_TO_FRACTION;
    }

    @Override
    public void setValue(final Double value, final boolean fireEvents) {
      super.setValue(value * SharedConstants.PERCENTAGE_TO_FRACTION, fireEvents);
    }
  };

  @UiField(provided = true) @Ignore ListBoxEditor<VehicleType> vehicleTypeEditor = new ListBoxEditor<VehicleType>() {
    @Override
    protected String getLabel(final VehicleType value) {
      return value == null ? M.messages().roadCategory() : M.messages().roadVehicleType(value);
    }
  };

  @UiField(provided = true) @Ignore ListBoxEditor<Integer> maximumSpeedEditor = new ListBoxEditor<Integer>() {
    @Override
    protected String getLabel(final Integer value) {
      return value == null || value == 0 ? M.messages().roadSelectMaxSpeed() : FormatUtil.formatSpeedWithUnit(value);
    }

    @Override
    protected String getKey(final Integer value) {
      return value == null ? "" : String.valueOf(value);
    }
  };

  @UiField @Ignore CheckBox strictEnforcement;

  final LeafValueEditor<RoadEmissionCategory> emissionCategoryEditor = new LeafValueAwareEditor<RoadEmissionCategory>() {
    @Override
    public void flush() {
      if (vehicleTypeEditor.getSelectedValue() == null || vehicleTypeEditor.getSelectedValue().isEmpty()) {
        getDelegate().recordError(M.messages().ivRoadVehicleTypeNotSelected(), vehicleTypeEditor.getValue(), vehicleTypeEditor);
      }
      if (speedSelectionPanel.isVisible() && maximumSpeedEditor.getValue() == 0) {
        getDelegate().recordError(M.messages().ivRoadMaxSpeed(), maximumSpeedEditor.getValue(), maximumSpeedEditor);
      }
    }
  };

  @UiField FlowPanel speedSelectionPanel;

  private RoadType roadType;

  public VehicleStandardEditor(final RoadEmissionCategories roadEmissionCategories, final HelpPopupController hpC) {
    this.roadEmissionCategories = roadEmissionCategories;
    initWidget(UI_BINDER.createAndBindUi(this));

    afterBinding(hpC);

    vehicleTypeEditor.addFirstEmptyItem();
    vehicleTypeEditor.addItems(VehicleType.values());

    maximumSpeedEditor.addChangeHandler(new ChangeHandler() {
      @Override
      public void onChange(final ChangeEvent event) {
        updateSpeed();
      }
    });
    strictEnforcement.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Boolean> event) {
        updateSpeed();
      }
    });

    addValidators();

    addHelpWidgets(hpC);
  }

  private void addValidators() {
    stagnationFractionEditor.addValidator(new DoubleRangeValidator(0d, 1d) {
      @Override
      protected String getMessage(final Double value) {
        return M.messages().ivRoadStagnation(FormatUtil.formatDecimalCapped(value * SharedConstants.PERCENTAGE_TO_FRACTION));
      }
    });
  }

  private void addHelpWidgets(final HelpPopupController hpC) {
    hpC.addWidget(vehicleTypeEditor, hpC.tt().ttRoadStandardVehicleType());
    hpC.addWidget(stagnationFractionEditor, hpC.tt().ttRoadEmissionsStagnation());
    hpC.addWidget(maximumSpeedEditor, hpC.tt().ttRoadEmissionsMaxSpeed());
    hpC.addWidget(strictEnforcement, hpC.tt().ttRoadEmissionsEnforcement());
  }

  @UiHandler("vehicleTypeEditor")
  public void vehicleTypeChange(final ChangeEvent event) {
    updateCategory();
  }

  /**
   * @param roadType
   */
  public void setRoadType(final RoadType roadType) {
    final boolean update = this.roadType != roadType;
    this.roadType = roadType;
    if (update) {
      updateSpeedCategories();
    }
  }

  public void setCategory(final RoadEmissionCategory category) {
    if (category == null) {
      vehicleTypeEditor.setValue(null);
      updateCategory();
    } else {
      roadType = category.getRoadType();
      vehicleTypeEditor.setValue(category.getVehicleType());
      if (category.isSpeedRelevant()) {
        maximumSpeedEditor.setValue(category.getMaximumSpeed());
        strictEnforcement.setValue(category.isStrictEnforcement());
      }
      if (!category.equals(emissionCategoryEditor.getValue())) {
        updateCategory();
      }
    }
    updateSpeed();
  }

  public void updateCategory() {
    emissionCategoryEditor.setValue(vehicleTypeEditor.getValue() == null ? null : roadEmissionCategories.getRoadEmissionCategory(
        roadType, vehicleTypeEditor.getValue(), strictEnforcement.getValue(), maximumSpeedEditor.getValue(), null));
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    vehicleTypeEditor.ensureDebugId(TestID.LIST_VEHICLE_TYPE);
    stagnationFractionEditor.ensureDebugId(TestID.INPUT_STAGNATION);
    maximumSpeedEditor.ensureDebugId(TestID.LIST_MAXSPEED);
    strictEnforcement.ensureDebugId(TestID.CHECKBOX_STRICT_ENFORCEMENT);
  }

  private void updateSpeedCategories() {
    maximumSpeedEditor.clear();

    final TreeSet<Integer> speedCategories = roadEmissionCategories.getMaximumSpeedCategories(roadType);

    final boolean hasSpeedCategories = speedCategories != null;
    final boolean hasMoreThanOne = hasSpeedCategories && speedCategories.size() > 1;
    speedSelectionPanel.setVisible(hasSpeedCategories && hasMoreThanOne);
    if (hasSpeedCategories) {
      if (hasMoreThanOne) {
        // if only 1 don't add the select, because this list is not visible then reading the list it will read
        // the first value, which is the 1 and only item in the list.
        maximumSpeedEditor.addItem(SELECT_SPEED);
      }
      for (final Integer ms : speedCategories) {
        maximumSpeedEditor.addItem(ms);
      }
    }
  }

  private void updateSpeed() {
    final boolean enforcementEnabled = roadEmissionCategories.getRoadEmissionCategory(
        new RoadEmissionCategory(roadType, VehicleType.LIGHT_TRAFFIC, true, maximumSpeedEditor.getValue(), null)) != null;

    strictEnforcement.setEnabled(enforcementEnabled);
    strictEnforcement.setValue(strictEnforcement.getValue() && enforcementEnabled);

    updateCategory();
  }
}
