/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.info;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.testing.FakeLeafValueEditor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.info.EcologyQualityType;
import nl.overheid.aerius.shared.domain.info.Natura2000Info;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.LabelledValue;

/**
 * Information panel for Natura 2000 basic information.
 */
public class Natura2000BasicInfoPanel extends Composite implements Editor<Natura2000Info> {

  private static Natura2000BasicInfoPanelUiBinder uiBinder = GWT.create(Natura2000BasicInfoPanelUiBinder.class);

  public interface Natura2000InfoDriver extends SimpleBeanEditorDriver<Natura2000Info, Natura2000BasicInfoPanel> { }

  interface Natura2000BasicInfoPanelUiBinder extends UiBinder<Widget, Natura2000BasicInfoPanel> {
  }

  @UiField LabelledValue name;
  @UiField @Ignore LabelledValue natura2000AreaId;
  @UiField LabelledValue contractor;
  @UiField @Ignore LabelledValue ecologyQualityType;
  @UiField LabelledValue environment;
  @UiField @Ignore LabelledValue surface;
  @UiField LabelledValue protection;
  @UiField LabelledValue status;
  LeafValueEditor<Integer> natura2000AreaIdEditor = new FakeLeafValueEditor<Integer>() {
    @Override
    public void setValue(final Integer value) {
      natura2000AreaId.setValue(value == null ? "" : value.toString());
    }
  };
  LeafValueEditor<EcologyQualityType> ecologyQualityTypeEditor = new FakeLeafValueEditor<EcologyQualityType>() {
    @Override
    public void setValue(final EcologyQualityType value) {
      super.setValue(value);
      ecologyQualityType.setValue(M.messages().infoPanelAreaInfoLabelCategoryValue(value));

      switch (value) {
      case ONE_A:
        hpC.addWidget(ecologyQualityType, hpC.tt().ttEcologicalQuality1a());
        break;
      case ONE_B:
        hpC.addWidget(ecologyQualityType, hpC.tt().ttEcologicalQuality1b());
        break;
      case TWO:
        hpC.addWidget(ecologyQualityType, hpC.tt().ttEcologicalQuality2());
        break;
      default:
        hpC.addWidget(ecologyQualityType, hpC.tt().ttEcologicalQualityNone());
        break;
      }
    }
  };
  LeafValueEditor<Long> surfaceEditor = new FakeLeafValueEditor<Long>() {
    @Override
    public void setValue(final Long value) {
      super.setValue(value);
      surface.setValue(M.messages().infoPanelAreaInfoDataSurface(FormatUtil.toFixed(value, 0)));
    }
  };
  private final HelpPopupController hpC;

  public Natura2000BasicInfoPanel(final HelpPopupController hpC) {
    this.hpC = hpC;
    initWidget(uiBinder.createAndBindUi(this));

    name.ensureDebugId(TestID.AREAINFORMATION_NAME);
    natura2000AreaId.ensureDebugId(TestID.AREAINFORMATION_ID);
    contractor.ensureDebugId(TestID.AREAINFORMATION_CONTRACTOR);
    ecologyQualityType.ensureDebugId(TestID.AREAINFORMATION_QUALITY);
    environment.ensureDebugId(TestID.AREAINFORMATION_ENVIRONMENT);
    surface.ensureDebugId(TestID.AREAINFORMATION_SURFACE);
    protection.ensureDebugId(TestID.AREAINFORMATION_PROTECTION);
    status.ensureDebugId(TestID.AREAINFORMATION_STATUS);
  }

  public void setTitleVisible(final boolean visible) {
    name.setVisible(visible);
  }
}
