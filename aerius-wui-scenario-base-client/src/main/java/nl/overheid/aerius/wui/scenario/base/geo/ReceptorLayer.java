/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.gwtopenmaps.openlayers.client.Map;

import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.shared.domain.SimpleFilter;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.geo.ReceptorFeature;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Emission result layer for receptors as hexagons.
 *
 * <pre>
 *   __    __    __    __    __    _
 *  /  \__/  \__/  \__/  \__/  \__/  \
 *  \__/  \__/  \__/  \__/  \__/  \__/
 *  /XX\__/  \__/  \__/XX\__/  \__/  \
 *  \XX/XX\__/  \__/XX\XX/XX\__/  \__/
 *  /  \XX/XX\__/  \XX/XX\XX/  \__/  \
 *  \__/  \XX/  \__/XX\XX/XX\__/  \__/
 *  /  \__/XX\__/  \XX/XX\XX/  \__/  \
 *  \__/  \XX/  \__/  \XX/  \__/  \__/
 *  /  \__/XX\__/  \__/  \__/  \__/  \
 *  \__/  \XX/  \__/  \__/  \__/  \__/
 * </pre>
 *
 * @deprecated receptors will be drawn via a wms layer instead as vectors, because the vector implementation doesn't scale for large sets.
 */
@Deprecated
public class ReceptorLayer extends EmissionResultLayer {
  private static final String RECEPTOR_LAYER = "receptor_layer";

  // Having 2 scales for different srid's is a hack, but because this class is deprecated it is accepted work around.
  private static final int[] SCALES_29882 = {30000, 60000, 150000, 300000, 800000 };
  private static final int[] SCALES_27700 = { 200000, 400000, 800000, 1600000, 3200000, 6400000, 1280000 };

  private final ReceptorPointsContainer container;
  private final HashSet<Integer> cache = new HashSet<Integer>();
  private final int[] scales;
  private final List<HexagonZoomLevel> zoomLevels;

  private HexagonZoomLevel zoomLevel;
  private SimpleFilter<AeriusResultPoint> filter;

  public ReceptorLayer(final MapLayoutPanel map, final ScenarioBasePlace place, final ReceptorPointsContainer container,
      final EmissionResultKey initialKey, final EmissionResultValueDisplaySettings displaySettings) {
    super(RECEPTOR_LAYER, map, initialKey, place, displaySettings);
    this.container = container;
    container.setPlace(place);
    scales = map.getReceptorGridSettings().getEpsg().getSrid() == RDNew.SRID ? SCALES_29882 : SCALES_27700;
    zoomLevels = map.getReceptorGridSettings().getHexagonZoomLevels();
  }

  @EventHandler
  @Override
  void onPlaceChange(final PlaceChangeEvent event) {
    container.setPlace((ScenarioBasePlace) event.getValue());
    super.onPlaceChange(event);
  }

  @Override
  public void setValues(final int calculationId, final HashSet<AeriusResultPoint> values) {
    container.distributeReceptors(calculationId, values);
  }

  @Override
  public void setFilter(final SimpleFilter<AeriusResultPoint> filter) {
    this.filter = filter;
    refreshLayer(true);
  }

  @Override
  protected void drawFeatures() {
    final Map map = getMap();
    final ScenarioBasePlace place = getPlace();
    final EmissionResultKey key = getKey();

    final HexagonZoomLevel zoomLevel = levelFromScale(map.getScale());
    if (!zoomLevel.equals(this.zoomLevel)) {
      this.zoomLevel = zoomLevel;
      clearFeatures();
    }

    final Iterable<AeriusResultPoint> receptorsToDraw = container.getReceptorTilesForLevel(zoomLevel, map.getExtent());
    if (receptorsToDraw == null) {
      return;
    }

    final ArrayList<ReceptorFeature> features = new ArrayList<>();
    for (final AeriusResultPoint receptor : receptorsToDraw) {
      // Apply the filter, if any.
      if ((filter != null) && !filter.doFilter(receptor)) {
        continue;
      }

      // Apply the draw cache.
      if (cache.contains(receptor.getId())) {
        continue;
      }

      cache.add(receptor.getId());

      // Create the hexagon.
      if (key != null) {
        final ReceptorFeature feature = new ReceptorFeature(receptor, zoomLevel);
        final double emissionResult = receptor.getEmissionResult(key);
        feature.setStyle(place.getSituation() == Situation.COMPARISON ? ComparisonStyleUtil.getStyle(emissionResult)
            : ReceptorStyleUtil.getStyle(key.getEmissionResultType(), emissionResult));
        features.add(feature);
      }
    }

    addFeatures(features.toArray(new ReceptorFeature[features.size()]));
    redraw();
  }

  private HexagonZoomLevel levelFromScale(final double scale) {
    for (int i = 0; i < scales.length; i++) {
      if (scale <= scales[i]) {
        return zoomLevels.get(i);
      }
    }
    return zoomLevels.get(zoomLevels.size() -1);
  }

  @Override
  protected void clearFeatures() {
    if (getNumberOfFeatures() > 0) {
      destroyFeatures();
    }

    cache.clear();
  }

  @Override
  protected boolean isHexagon() {
    return true;
  }

  @Override
  public String getTitle() {
    return getName();
  }

  @Override
  public void setTitle(final String name) {
    setName(name);

  }
}
