/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.scenario.SectorEmissionSummary;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 *
 */
public interface EmissionSourcesView extends IsWidget, HasTitle {
  interface Presenter {

  }

  /**
   * @param key
   * @param enableAdding
   * @param sources
   * @param total
   */
  void setValue(EmissionValueKey key, boolean enableAdding, EmissionSourceList sources, double total, double totalExtra);

  /**
   * @param key
   * @param differences
   * @param explainText
   */
  void setValue(EmissionValueKey key, ArrayList<SectorEmissionSummary> differences, String explainText);

}
