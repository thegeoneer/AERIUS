/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics.HeatContentType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.IsCollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup.RadioButtonContentResource;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.OPSSourceOutflowForcedEditor.OPSSourceOutflowForcedViewDriver;
import nl.overheid.aerius.wui.scenario.base.source.core.OPSSourceOutflowNotForcedEditor.OPSSourceOutflowNotForcedDriver;

/**
 * Editor for {@link OPSSourceCharacteristics}.
 */
public class OPSSourceCharacteristicsEditor extends Composite implements ValueAwareEditor<OPSSourceCharacteristics>,
Editor<OPSSourceCharacteristics>, HasValueChangeHandlers<Boolean>, IsCollapsiblePanel {

  interface OPSSourceCharacteristicsEditorUiBinder extends UiBinder<Widget, OPSSourceCharacteristicsEditor> {}

  private static OPSSourceCharacteristicsEditorUiBinder UI_BINDER = GWT.create(OPSSourceCharacteristicsEditorUiBinder.class);

  private final OPSSourceOutflowForcedEditor vForcedEditor;
  private final OPSSourceOutflowForcedViewDriver vForcedDriver = GWT.create(OPSSourceOutflowForcedViewDriver.class);

  private final OPSSourceOutflowNotForcedEditor vNotForcedEditor;
  private final OPSSourceOutflowNotForcedDriver vNotForcedDriver = GWT.create(OPSSourceOutflowNotForcedDriver.class);

  @SuppressWarnings("rawtypes")
  private SimpleBeanEditorDriver driver;
  @SuppressWarnings("rawtypes")
  private Editor editor;

  class RadioButtonResources implements RadioButtonContentResource<HeatContentType> {
    @Override
    public String getRadioButtonText(final HeatContentType value) {
      return M.messages().sourceHeatType(value);
    }
  }

  @UiField FlowPanel panel;
  @UiField(provided = true) RadioButtonGroup<HeatContentType> heatContentType;
  @UiField SimplePanel buildingInfluencePanel;

  @UiField(provided = true) DoubleValueBox spreadEditor = new DoubleValueBox(M.messages().sourceSpread(), OPSLimits.SOURCE_SPREAD_DIGITS_PRECISION);
  @UiField FlowPanel spreadRow;
  @UiField FlowPanel diurnalVariationRowLabel;
  @UiField @Ignore TextBox diurnalVariationTextBox;
  @UiField FlowPanel diurnalVariationRow;
  @UiField FlowPanel particleSizeDistributionRow;
  @UiField(provided = true) ListBoxEditor<DiurnalVariationSpecification> diurnalVariationSpecificationEditor;
  @UiField IntValueBox particleSizeDistributionEditor;
  @UiField @Ignore CollapsiblePanel collapsiblePanel;

  private EditorDelegate<OPSSourceCharacteristics> delegate;
  private OPSSourceCharacteristics cachedOPSCharacteristics;
  private boolean hasErrors;
  private final EventBus eventBus;

  /**
   * The characteristics of OPS-sources may be set in this editor.
   *
   * @param hpC the popup-controller.
   * @param categories SectorCategories.
   * @param object
   */
  public OPSSourceCharacteristicsEditor(final EventBus eventBus, final HelpPopupController hpC, final SectorCategories categories,
      final String opsFactsheetUrl) {
    this.eventBus = eventBus;

    diurnalVariationSpecificationEditor = new ListBoxEditor<DiurnalVariationSpecification>() {
      @Override
      protected String getLabel(final DiurnalVariationSpecification value) {
        return value.getName();
      }
      @Override
      public void setValue(final DiurnalVariationSpecification value) {
        super.setValue(value);

        if (value != null) {
          diurnalVariationTextBox.setValue(value.getName());
        }
      }
    };

    heatContentType = new RadioButtonGroup<HeatContentType>(new RadioButtonResources());
    heatContentType.ensureDebugId(TestID.INPUT_OPS_HEAT_TOGGLE_BUTTON);
    heatContentType.addButtons(HeatContentType.values());

    initWidget(UI_BINDER.createAndBindUi(this));

    vForcedEditor = new OPSSourceOutflowForcedEditor(eventBus, hpC, opsFactsheetUrl);
    vForcedDriver.initialize(vForcedEditor);

    vNotForcedEditor = new OPSSourceOutflowNotForcedEditor(eventBus, hpC, opsFactsheetUrl);
    vNotForcedDriver.initialize(vNotForcedEditor);

    diurnalVariationSpecificationEditor.addItems(categories.getDiurnalVariations());

    hpC.addWidget(collapsiblePanel.getTitleWidget(), hpC.tt().ttSourceCharacteristics());
    hpC.addWidget(spreadEditor, hpC.tt().ttOPSSpread());
    hpC.addWidget(particleSizeDistributionEditor, hpC.tt().ttOPSParticleSizeDistribution());
    panel.ensureDebugId(TestID.GUI_COLLAPSEPANEL_GEN);
    spreadEditor.ensureDebugId(TestID.INPUT_OPS_SPREAD);
    particleSizeDistributionEditor.ensureDebugId(TestID.INPUT_OPS_PARTICLE_SIZE_DISTRIBUTION);
  }

  /*
   * Add value change handlers.
   */
  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public CollapsiblePanel asCollapsible() {
    return collapsiblePanel;
  }

  @UiHandler("heatContentType")
  void onButtonChange(final ValueChangeEvent<HeatContentType> event) {
    cachedOPSCharacteristics.setHeatContentType(event.getValue());
    setValue(cachedOPSCharacteristics);
  }


  /**
   * Sets the visibility of the specific input fields in the OPS characteristics editor based on some input variables.
   *
   * @param sector sector
   * @param wktGeometry geometry as wkt geometry
   * @param showDiurnalVariation true if diurnal variation must be shown
   */
  public void setVisibility(final Sector sector, final WKTGeometry wktGeometry, final boolean showDiurnalVariation) {
    final boolean pointGeo = (wktGeometry != null) && (wktGeometry.getType() == WKTGeometry.TYPE.POINT);
    final boolean polygonGeo = (wktGeometry != null) && (wktGeometry.getType() == WKTGeometry.TYPE.POLYGON);
    setSpreadVisible(polygonGeo);
    setDiurnalVariationVisible(showDiurnalVariation);

    // building enabled
    vForcedEditor.setBuildingEditorVisible((sector != null) && pointGeo);
    vNotForcedEditor.setBuildingEditorVisible((sector != null) && pointGeo);
    // edit diurnalVariation
    if (showDiurnalVariation) {
      chooseDiurnalVariationEditable((sector != null) && (SectorGroup.OTHER == sector.getSectorGroup()));
    }
  }

  private void setSpreadVisible(final boolean visible) {
    spreadRow.setVisible(visible);
  }

  private void setDiurnalVariationVisible(final boolean visible) {
    diurnalVariationRowLabel.setVisible(visible);
    diurnalVariationRow.setVisible(visible);
  }

  private void chooseDiurnalVariationEditable(final boolean editable) {
    diurnalVariationRowLabel.setVisible(!editable);
    diurnalVariationRow.setVisible(editable);
  }

  /**
   * @param key of the emission value.
   */
  public void setEmissionValueKey(final EmissionValueKey key) {
    particleSizeDistributionEditor.setEnabled(key.getSubstance() == Substance.PM10);
    particleSizeDistributionRow.setVisible(key.getSubstance() == Substance.PM10);
  }

  @Override
  public void flush() {
    driver.flush();
    if (driver.hasErrors()) {
      hasErrors = true;
      for (final Object obj : driver.getErrors()) {
        final EditorError error = (EditorError) obj;
        delegate.recordError(error.getMessage(), error.getValue(), error.getUserData());
        error.setConsumed(true);
      }
    }
    if (this.collapsiblePanel.getValue()) {
      collapsiblePanel.setValue(false);
    }
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @SuppressWarnings("unchecked")
  @Override
  public void setValue(final OPSSourceCharacteristics value) {
    this.cachedOPSCharacteristics = value;
    hasErrors = false;
    // EDIT mode
    if (value == null) {
      setValue(new OPSSourceCharacteristics());
      return;
    } else if (value.getHeatContentType() == HeatContentType.FORCED) {
      vForcedEditor.setEventBus(eventBus);
      driver = vForcedDriver;
      editor = vForcedEditor;
    } else if (value.getHeatContentType() == HeatContentType.NOT_FORCED) {
      vNotForcedEditor.setEventBus(eventBus);
      driver = vNotForcedDriver;
      editor = vNotForcedEditor;
    }

    if (driver != null) {
      driver.edit(value);
      vForcedEditor.setBuildingInfluence(value.isBuildingInfluence());
      vNotForcedEditor.setBuildingInfluence(value.isBuildingInfluence());
      // Reset errors styles on widgets
      ErrorPopupController.clearWidgets();
    }
    buildingInfluencePanel.setWidget((IsWidget) editor);
  }

  @Override
  public void setDelegate(final EditorDelegate<OPSSourceCharacteristics> delegate) {
    this.delegate = delegate;
  }

  public boolean hasErrors() {
    return hasErrors;
  }

}
