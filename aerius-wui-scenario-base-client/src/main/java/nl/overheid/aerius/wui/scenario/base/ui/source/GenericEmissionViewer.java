/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.DivTableKeyValue;
import nl.overheid.aerius.wui.main.widget.table.TextKeyValueDivTable;

public class GenericEmissionViewer extends EmissionViewer<EmissionSource> {
  private static final GenericEmissionViewerUiBinder UI_BINDER = GWT.create(GenericEmissionViewerUiBinder.class);

  @UiTemplate("KeyValueTableViewer.ui.xml")
  interface GenericEmissionViewerUiBinder extends UiBinder<Widget, GenericEmissionViewer> {}

  interface GenericEmissionDriver extends SimpleBeanEditorDriver<EmissionSource, GenericEmissionViewer> {}

  @UiField Label header;
  @UiField TextKeyValueDivTable table;

  private final EmissionValueKey noxKey = new EmissionValueKey(Substance.NOX);
  private final EmissionValueKey nh3Key = new EmissionValueKey(Substance.NH3);
  private final EmissionValueKey pm10Key = new EmissionValueKey(Substance.PM10);

  public GenericEmissionViewer() {
    initWidget(UI_BINDER.createAndBindUi(this));
    table.ensureDebugId(TestID.TABLE_SOURCES_SECTOR_VIEWER);

    header.setText(M.messages().emissionSourceViewerGenericTitle());
  }

  @Override
  public void setValue(final EmissionSource value) {
    table.clear();

    addValue(Substance.NOX.getName(), value.getEmission(noxKey));
    addValue(Substance.NH3.getName(), value.getEmission(nh3Key));
    addValue(Substance.PM10.getName(), 0);// NO PM10 value.getEmission(pm10Key))
  }

  private void addValue(final String key, final double value) {
    if (value > 0) {
      table.addRowData(new DivTableKeyValue<>(key, FormatUtil.formatEmissionWithUnit(value)));
    }
  }
}
