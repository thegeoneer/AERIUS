/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.calculators;

import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.InputCalculator;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;

/**
 * Simple implementation of a {@link InputCalculator} that lets overriding classes
 * create a dialog to put into a popup.
 *
 * @param <E> see {@link InputCalculator}
 */
@SuppressWarnings("rawtypes")
abstract class AbstractInputCalculator<R, T> implements InputCalculator<R>, ConfirmHandler<T>, LeafValueEditor<T> {

  private final ConfirmCancelDialog dialog;
  private HasValue<R> input;
  private T value;

  @SuppressWarnings("unchecked")
  public AbstractInputCalculator() {
    dialog = new ConfirmCancelDialog(getSaveButton(), getCancelButton());
    dialog.setText(getTitle());
    dialog.addConfirmHandler(this);
  }

  @Override
  public void setInput(final HasValue<R> input) {
    this.input = input;
  }

  @Override
  public final void display() {
    dialog.setWidget(createDialog());
    dialog.center();
  }

  /**
   * Adds handler that is called when the popup is closed.
   * @param handler close handler.
   * @return
   */
  public HandlerRegistration addCloseHandler(final CloseHandler<PopupPanel> handler) {
    return dialog.addCloseHandler(handler);
  }

  protected void enableConfirmButton(final boolean enable) {
    dialog.enableConfirmButton(enable);
  }

  @Override
  public void onConfirm(final ConfirmEvent<T> event) {
    value = getConfirmValue();
    input.setValue(getInputValue(value));
    dialog.hide();
  }

  /**
   * Returns the title for the savebutton.
   *
   * @return title for the savebutton screen.
   */
  protected String getSaveButton() {
    return M.messages().calculatorSaveButton();
  }

  /**
   * Returns the title for the cancelbutton.
   *
   * @return title for the cancelbutton screen.
   */
  protected String getCancelButton() {
    return M.messages().cancelButton();
  }

  @Override
  public T getValue() {
    return value;
  }

  @Override
  public void setValue(final T value) {
    this.value = value;
  }

  /**
   * Creates a dialog that will be put into a popup.
   *
   * @return A widget
   */
  protected abstract Widget createDialog();

  /**
   * Returns the calculated value; this method is to be called when confirm event is triggered.
   * @return the calculated value
   */
  protected abstract T getConfirmValue();

  /**
   * Returns the value of the calculated input.
   * @param value
   *
   * @return value to put into the input object.
   */
  protected abstract R getInputValue(T value);

  /**
   * Returns the title for the popup screen.
   *
   * @return title for the popup screen.
   */
  protected abstract String getTitle();
}
