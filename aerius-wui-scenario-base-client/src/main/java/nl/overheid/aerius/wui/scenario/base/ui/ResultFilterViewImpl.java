/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.shared.domain.HabitatHoverType;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResults;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.HabitatTypeEvent.HabitatDisplayType;
import nl.overheid.aerius.wui.main.event.HabitatTypeHoverEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.geo.ReceptorPointsContainer;
import nl.overheid.aerius.wui.scenario.base.geo.VectorCalculationResultLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.ui.overview.AbstractScenarioBaseHabitatFilter;
import nl.overheid.aerius.wui.scenario.base.ui.overview.HabitatComparisonFilter;
import nl.overheid.aerius.wui.scenario.base.ui.overview.HabitatSituationFilter;

public class ResultFilterViewImpl extends Composite implements ResultFilterView {

  interface ResultFilterViewImplUiBinder extends UiBinder<Widget, ResultFilterViewImpl> { }

  private static final ResultFilterViewImplUiBinder UI_BINDER = GWT.create(ResultFilterViewImplUiBinder.class);

  private static final int ALL_HABITAT_TYPES = 0;

  interface HabitatFilterPanelEventBinder extends EventBinder<ResultFilterViewImpl> { }

  private final HabitatFilterPanelEventBinder eventBinder = GWT.create(HabitatFilterPanelEventBinder.class);

  // The type is irrelevant
  private AbstractScenarioBaseHabitatFilter<?> habitatFilter;

  @UiField SwitchPanel switchPanel;
  @UiField(provided = true) ListBoxEditor<AssessmentArea> naturaSelectBox = new ListBoxEditor<AssessmentArea>() {
    @Override
    protected String getLabel(final AssessmentArea value) {
      return value.getName();
    }
  };
  @UiField ListBox habitatSelectBox;
  @UiField SimplePanel habitatFilterContainer;
  @UiField Label noDataLabel;
  private final Label noHabitatsLabel = new Label();

  private final AsyncCallback<HashMap<Integer, Double>> receptorCallback = new AppAsyncCallback<HashMap<Integer, Double>>() {
    @Override
    public void onSuccess(final HashMap<Integer, Double> result) {
      habitatFilter.setData(container.getFilteredReceptors(result));
    }
  };

  // This 'caching' solution is a bit meh, but it'll work fine in JS.
  // Note, this result filter view implementation need not be refreshed/recreated
  // each and every time a place change occurs, this currently does happen,
  // which is why we need this static cache to remember what's been selected
  // (without other components telling this what to remember)
  //
  // Not redrawing this entire thing is way more efficient. Just throwing
  // that out there for future reference.
  private static int lastSelectedAssessmentAreaId;
  private static int lastSelectedHabitatId;

  private int selectedHabitatId;

  private final ReceptorPointsContainer container;
  private final EventBus eventBus;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private final VectorCalculationResultLayerWrapper layer;

  private CalculationSummary summary;

  private Presenter presenter;

  @Inject
  public ResultFilterViewImpl(final ReceptorPointsContainer container, final EventBus eventBus,
      final ScenarioBaseAppContext<?, ?> appContext, final VectorCalculationResultLayerWrapper layer) {
    this.container = container;
    this.eventBus = eventBus;
    this.appContext = appContext;
    this.layer = layer;

    initWidget(UI_BINDER.createAndBindUi(this));
    eventBinder.bindEventHandlers(this, eventBus);
    noHabitatsLabel.addStyleName(R.css().emptyPanelWithText());
    naturaSelectBox.ensureDebugId(TestID.LIST_NATURA);
    habitatSelectBox.ensureDebugId(TestID.LIST_HABITAT);
  }

  @Override
  public void setValue(final Situation situation, final EmissionResultKey resultKey, final CalculationSummary summary,
      final int calcId1, final int calcId2) {
    this.summary = summary;
    switchPanel.showWidget(0);

    naturaSelectBox.clear();

    if (situation == Situation.COMPARISON) {
      habitatFilter = new HabitatComparisonFilter(layer, appContext.getUserContext().getEmissionResultKey());
    } else {
      habitatFilter = new HabitatSituationFilter(layer, appContext.getUserContext().getEmissionResultKey());
    }

    habitatFilterContainer.setWidget(habitatFilter);

    final List<CalculationAreaSummaryResult> areas = new ArrayList<CalculationAreaSummaryResult>(summary.getSummaryResult().values());
    Collections.sort(areas, new Comparator<CalculationAreaSummaryResult>() {

      @Override
      public int compare(final CalculationAreaSummaryResult o1, final CalculationAreaSummaryResult o2) {
        return o1.getArea().getName().compareTo(o2.getArea().getName());
      }
    });
    for (final CalculationAreaSummaryResult entry : areas) {
      final AssessmentArea area = entry.getArea();
      naturaSelectBox.addItem(area);

      if (area.getId() == lastSelectedAssessmentAreaId) {
        naturaSelectBox.setValue(area);
      }
    }
    // Fire native change event so habitat types will be populated on the default/first assessment area
    ChangeEvent.fireNativeEvent(Document.get().createChangeEvent(), naturaSelectBox);
  }

  @UiHandler("naturaSelectBox")
  void onNaturaSelection(final ChangeEvent e) {
    habitatSelectBox.setVisible(true);
    final AssessmentArea selectedArea = naturaSelectBox.getValue();
    lastSelectedAssessmentAreaId = selectedArea == null ? -1 : selectedArea.getId();

    if (fillHabitatSelectBox()) {
      habitatFilterContainer.setWidget(habitatFilter);
      // Fire native change event so filter will be populated with default/first habitat
      ChangeEvent.fireNativeEvent(Document.get().createChangeEvent(), habitatSelectBox);
    } else {
      noHabitatsLabel.setText(M.messages().calculationFilterNoHabitatForArea(
          selectedArea == null ? "" : selectedArea.getName()));
      habitatFilterContainer.setWidget(noHabitatsLabel);
    }
  }

  private boolean fillHabitatSelectBox() {
    habitatSelectBox.clear();
    final CalculationAreaSummaryResult results = summary.getSummaryResult().get(lastSelectedAssessmentAreaId);
    for (final Entry<Integer, HabitatSummaryResults> habitat : results.getHabitats().entrySet()) {
      if (habitatSelectBox.getItemCount() == 0) {
        // Inside the loop so only add when there is at least one habitattype
        habitatSelectBox.addItem(M.messages().habitatFilterAllRelevantHabitats(), String.valueOf(ALL_HABITAT_TYPES));

        if (lastSelectedHabitatId == ALL_HABITAT_TYPES) {
          habitatSelectBox.setSelectedIndex(habitatSelectBox.getItemCount() - 1);
        }
      }

      habitatSelectBox.addItem(habitat.getValue().getCode() + " - " + habitat.getValue().getName(),
          String.valueOf(habitat.getValue().getId()));

      if (habitat.getValue().getId() == lastSelectedHabitatId) {
        habitatSelectBox.setSelectedIndex(habitatSelectBox.getItemCount() - 1);
      }
    }
    return !results.getHabitats().entrySet().isEmpty();
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @UiHandler("habitatSelectBox")
  void onHabitatSelection(final ChangeEvent e) {
    final int habitatId = Integer.parseInt(habitatSelectBox.getValue(habitatSelectBox.getSelectedIndex()));
    selectedHabitatId = habitatId;
    lastSelectedHabitatId = habitatId;

    retrieveFilteredReceptors();
  }

  private HashSet<Integer> findHabitatTypes(final int areaId) {
    final HashSet<Integer> ids = new HashSet<Integer>();
    final CalculationAreaSummaryResult casr = summary.getSummaryResult().get(areaId);
    if (casr != null) {
      for (final Entry<Integer, HabitatSummaryResults> habitat : casr.getHabitats().entrySet()) {
        ids.add(habitat.getKey());
      }
    }
    return ids;
  }

  private void retrieveFilteredReceptors() {
    final AssessmentArea area = naturaSelectBox.getValue();
    if (area != null) {
      // Show the habitat in question.
      if (selectedHabitatId == ALL_HABITAT_TYPES) {
        eventBus.fireEvent(new HabitatTypeHoverEvent(HabitatHoverType.NATURE_AREA, area.getId(), findHabitatTypes(area.getId())));
      } else {
        eventBus.fireEvent(new HabitatTypeHoverEvent(HabitatHoverType.NATURE_AREA, area.getId(), selectedHabitatId));
      }
      presenter.getHabitates(area.getId(), selectedHabitatId, receptorCallback);
    }
  }

  @Override
  public void setEmissionResultKey(final EmissionResultKey value) {
    if (habitatFilter != null) {
      habitatFilter.setEmissionResultKey(value);
    }
  }

  @Override
  public void setNoData(final boolean loading, final String noDataText) {
    eventBus.fireEvent(new HabitatTypeHoverEvent(HabitatDisplayType.REMOVE));
    noDataLabel.setText(noDataText);
    switchPanel.showWidget(loading ? 1 : 2);
  }

  @Override
  public String getTitleText() {
    return M.messages().calculatorMenuResults();
  }
}
