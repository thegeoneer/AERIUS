/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationInitResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.info.CalculationInfo;
import nl.overheid.aerius.shared.service.InfoServiceAsync;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInfoReceivedEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

@Singleton
public class CalculationMarkerLayerWrapper {
  interface MarkerLayerWrapperEventBinder extends EventBinder<CalculationMarkerLayerWrapper> {}

  private final MarkerLayerWrapperEventBinder eventBinder = GWT.create(MarkerLayerWrapperEventBinder.class);

  private CalculationInitResult lastCalculationInfo;
  private CalculationMarkerLayer layer;
  private ScenarioBasePlace place;

  private boolean userInput;
  private boolean hasData;
  private long activeCalculationId;

  private final HashMap<Integer, CalculationInfo> markerInfoMap = new HashMap<Integer, CalculationInfo>();
  private final AsyncCallback<CalculationInfo> handlerCalculationInfo = new AppAsyncCallback<CalculationInfo>() {
    @Override
    public void onSuccess(final CalculationInfo res) {
      markerInfoMap.put(res.getCalculationId(), res);
      updateLayer();
      eventBus.fireEvent(new CalculationInfoReceivedEvent(res));
    }
  };
  private final InfoServiceAsync service;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private final EventBus eventBus;

  private MapLayoutPanel map;

  @Inject
  public CalculationMarkerLayerWrapper(final EventBus eventBus,
      final ScenarioBaseAppContext<?, ?> appContext,
      final PlaceController placeController,
      final InfoServiceAsync service) {
    this.appContext = appContext;
    this.service = service;
    this.eventBus = eventBus;

    // Calculation handlers
    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onFinishCalculation(final CalculationFinishEvent event) {
    retrieveMarkerInfo();
  }

  @EventHandler
  public void onCancelCalculation(final CalculationCancelEvent event) {
    retrieveMarkerInfo();
  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent event) {
    if (event.getValue() instanceof ScenarioBasePlace) {
      place = (ScenarioBasePlace) event.getValue();
      updateLayer();
    }
  }

  @EventHandler
  public void onStartCalculation(final CalculationStartEvent event) {
    removeLayer();
    hasData = false;
  }

  @EventHandler
  public void onInitCalculation(final CalculationInitEvent event) {
    userInput = event.getInitResult().getCalculationSetOptions().getCalculationType() == CalculationType.CUSTOM_POINTS;

    if (userInput) {
      lastCalculationInfo = null;
    } else {
      lastCalculationInfo = event.getInitResult();
    }
  }

  public void setVisible(final boolean visible) {
    if (layer != null) {
      map.setVisible(layer, hasData && visible);
    }
  }

  private void retrieveMarkerInfo() {
    hasData = !userInput;
    if (hasData && lastCalculationInfo != null) {
      for (final Calculation calcSources : lastCalculationInfo.getCalculations()) {
        service.getCalculationInfo(calcSources.getCalculationId(), handlerCalculationInfo);
      }
    }
  }

  /**
   * Updates the marker layer
   */
  private void updateLayer() {
    final CalculatedScenario calculatedScenario = appContext.getUserContext().getCalculatedScenario();
    if (calculatedScenario == null) {
      return;
    }

    final Situation situation = place.getSituation();
    if (situation == Situation.COMPARISON || !calculatedScenario.isInSync()) {
      clearMarkers();
      return;
    }

    addLayer();

    // If we're here, we need to show markers
    final int calculationIdToShow = calculatedScenario.getCalculationId(place.getActiveSituationId());
    if (activeCalculationId != calculationIdToShow) {
      if (markerInfoMap.containsKey(calculationIdToShow)) {
        layer.drawMarkers(markerInfoMap.get(calculationIdToShow));
        activeCalculationId = calculationIdToShow;
      }
    }
  }

  private void clearMarkers() {
    if (layer != null) {
      layer.clearMarkers();
    }

    activeCalculationId = 0;
  }

  /**
   * Add the layer from scratch (no null-check).
   */
  private void addLayer() {
    if (layer == null) {
      layer = new CalculationMarkerLayer(map);
      map.addLayer((MapLayer) layer);
    }
  }

  /**
   * Remove the layer fully, such as when a User Input calculation started.
   */
  private void removeLayer() {
    markerInfoMap.clear();

    if (layer != null) {
      clearMarkers();
      map.removeLayer((MapLayer) layer);
      layer = null;
    }
  }

  public void setMap(final ScenarioBaseMapLayoutPanel map) {
    this.map = map;
  }
}
