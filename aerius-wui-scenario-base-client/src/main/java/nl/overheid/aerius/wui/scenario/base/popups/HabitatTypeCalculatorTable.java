/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.HabitatHoverType;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.HabitatTypeDisplayEvent;
import nl.overheid.aerius.wui.main.event.HabitatTypeEvent.HabitatDisplayType;
import nl.overheid.aerius.wui.main.event.HabitatTypeHoverEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.IsDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveDivTableRow;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class HabitatTypeCalculatorTable extends Composite implements IsDataTable<HabitatInfo> {
  private static final HabitatTypeTableUiBinder UI_BINDER = GWT.create(HabitatTypeTableUiBinder.class);

  interface HabitatTypeTableUiBinder extends UiBinder<Widget, HabitatTypeCalculatorTable> {}

  @UiField(provided = true) SimpleInteractiveClickDivTable<HabitatInfo> divTable;

  @UiField(provided = true) TextColumn<HabitatInfo> codeColumn;
  @UiField(provided = true) TextColumn<HabitatInfo> criticalLoadColumn;
  @UiField(provided = true) TextColumn<HabitatInfo> overlapColumn;

  private int receptorId;

  private EmissionResultValueDisplaySettings displaySettings;
  private EmissionResultKey emissionResultKey;

  public HabitatTypeCalculatorTable(final EventBus eventBus, final EmissionResultKey emissionResultKey,
      final EmissionResultValueDisplaySettings displaySettings) {
    this.emissionResultKey = emissionResultKey;
    this.displaySettings = displaySettings;

    divTable = new SimpleInteractiveClickDivTable<HabitatInfo>() {
      @Override
      public void applyRowOptions(final SimpleInteractiveDivTableRow row, final HabitatInfo item) {
        super.applyRowOptions(row, item);

        row.addDomHandler(new MouseOverHandler() {
          @Override
          public void onMouseOver(final MouseOverEvent event) {
            eventBus.fireEvent(new HabitatTypeHoverEvent(HabitatHoverType.POINT, receptorId, item.getId()));
          }
        }, MouseOverEvent.getType());
        row.addDomHandler(new MouseOutHandler() {
          @Override
          public void onMouseOut(final MouseOutEvent event) {
            eventBus.fireEvent(new HabitatTypeHoverEvent(HabitatDisplayType.REMOVE));
          }
        }, MouseOutEvent.getType());
      }
    };

    codeColumn = new TextColumn<HabitatInfo>() {
      @Override
      public String getValue(final HabitatInfo obj) {
        return M.messages().infoPanelHabitatTypesDataCode(obj.getCode(), obj.getName());
      }
    };

    criticalLoadColumn = new TextColumn<HabitatInfo>() {
      @Override
      public String getValue(final HabitatInfo obj) {
        return FormatUtil.formatEmissionResult(HabitatTypeCalculatorTable.this.emissionResultKey, obj.getCriticalLevels(),
            HabitatTypeCalculatorTable.this.displaySettings);
      }
    };
    criticalLoadColumn.ensureDebugId(TestID.CRITICAL_LOAD);

    overlapColumn = new TextColumn<HabitatInfo>() {
      @Override
      public String getValue(final HabitatInfo obj) {
        return FormatUtil.formatSurfacePreciseWithUnit(obj.getSurface());
      }
    };
    overlapColumn.ensureDebugId(TestID.RELEVANT_COVERAGE);

    initWidget(UI_BINDER.createAndBindUi(this));

    divTable.setSelectionModel(new MultiSelectionModel<HabitatInfo>());
    divTable.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        final Set<HabitatInfo> selectedSet = ((MultiSelectionModel<HabitatInfo>) divTable.getSelectionModel()).getSelectedSet();

        final HashSet<Integer> habitatTypeIds = new HashSet<>();
        for (final HabitatInfo info : selectedSet) {
          habitatTypeIds.add(info.getId());
        }

        eventBus.fireEvent(new HabitatTypeDisplayEvent(HabitatDisplayType.REPLACE, HabitatHoverType.POINT, receptorId, habitatTypeIds));
      }
    });
  }

  @Override
  public SimpleInteractiveClickDivTable<HabitatInfo> asDataTable() {
    return divTable;
  }

  public void setReceptor(final int receptorId) {
    this.receptorId = receptorId;
  }

  public void setDisplaySettings(final EmissionResultValueDisplaySettings displaySettings) {
    this.displaySettings = displaySettings;
  }

  public void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
  }
}
