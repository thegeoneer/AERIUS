/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.LayerPreparationUtil;
import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.WMSMapLayer;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.shared.domain.WMSLayerType;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationResultsRetrievedEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

@Singleton
public class WmsCalculationResultLayerWrapper implements CalculationResultLayerWrapper {

  private static final String SINGLE_SITUATION_PARAM = "calculation_id";
  private static final String COMPARISON_SITUATION_1_PARAM = "calculation_a_id";
  private static final String COMPARISON_SITUATION_2_PARAM = "calculation_b_id";
  private static final String CALCULATION_SUBSTANCE_PARAM = "calculation_substance";

  interface WmsCalculationResultsLayerWrapperEventBinder extends EventBinder<WmsCalculationResultLayerWrapper> {}

  private final WmsCalculationResultsLayerWrapperEventBinder eventBinder = GWT.create(WmsCalculationResultsLayerWrapperEventBinder.class);

  private final FetchGeometryServiceAsync service;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private final LayerWMSProps singleCalculationProps;
  private final LayerWMSProps comparisonCalculationProps;

  private MapLayoutPanel map;
  private LayerWMSProps selectedLayerProps;
  private WMSMapLayer layer;

  @Inject
  public WmsCalculationResultLayerWrapper(final ScenarioBaseAppContext<?, ?> appContext, final FetchGeometryServiceAsync service) {
    this.service = service;
    this.appContext = appContext;
    this.singleCalculationProps = appContext.getContext().getLayer(WMSLayerType.WMS_CALCULATION_SUBSTANCE_DEPOSITION_RESULTS_VIEW);
    this.comparisonCalculationProps = appContext.getContext().getLayer(WMSLayerType.WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW);
    LayerPreparationUtil.prepareLayer(service, singleCalculationProps);
    LayerPreparationUtil.prepareLayer(service, comparisonCalculationProps);
    eventBinder.bindEventHandlers(this, appContext.getEventBus());
  }

  @EventHandler
  void onCalculationInit(final CalculationInitEvent event) {
    updateLayer();
  }

  @EventHandler
  void onCalculationResultsRetrieved(final CalculationResultsRetrievedEvent event) {
    updateLayer();
  }

  @EventHandler
  void onEmissionResultKeyChanged(final EmissionResultKeyChangeEvent event) {
    updateLayer();
  }

  @EventHandler
  void onPlaceChange(final PlaceChangeEvent event) {
    updateLayer();
  }

  /**
   * Updates the layer
   */
  private void updateLayer() {
    final LayerWMSProps layerProps = selectLayer();
    if (layerProps == null && layer != null) {
      detach();
    } else if (layerProps != null && layer != null && layerProps.equals(selectedLayerProps)) {
      layer.redraw();
    } else if (layerProps != null) {
      LayerPreparationUtil.prepareLayer(service, layerProps, new AppAsyncCallback<LayerProps>() {
        @Override
        public void onSuccess(final LayerProps result) {
          detach();
          layerProps.setEnabled(true);
          layer = (WMSMapLayer) map.addLayer(layerProps);
          selectedLayerProps = layerProps;
        }
      });
    }
  }

  private LayerWMSProps selectLayer() {
    final Place where = appContext.getPlaceController().getWhere();
    if (!(where instanceof ScenarioBasePlace)) {
      return null;
    }

    final ScenarioBasePlace place = (ScenarioBasePlace) where;
    final boolean hasScenario = appContext.getUserContext().getCalculatedScenario() != null;
    LayerWMSProps selected = null;
    if (hasScenario && place.getSituation() == Situation.COMPARISON && place.getJobType() != JobType.PRIORITY_PROJECT_UTILISATION) {
      updateComparisonLayerProps(place.getSid1(), place.getSid2());
      selected = comparisonCalculationProps;
    } else if (hasScenario && place.getSituation() != Situation.COMPARISON) {
      updateSingleLayerProps(place.getActiveSituationId());
      selected = singleCalculationProps;
    }
    return selected;
  }

  private void updateSingleLayerProps(final int situationId) {
    updateSubstanceParam(singleCalculationProps);
    singleCalculationProps.setParamFilter(SINGLE_SITUATION_PARAM, appContext.getUserContext().getCalculatedScenario().getCalculationId(situationId));
  }

  private void updateComparisonLayerProps(final int currentSituationId, final int proposedSituationId) {
    updateSubstanceParam(comparisonCalculationProps);
    comparisonCalculationProps.setParamFilter(COMPARISON_SITUATION_1_PARAM,
        appContext.getUserContext().getCalculatedScenario().getCalculationId(currentSituationId));
    comparisonCalculationProps.setParamFilter(COMPARISON_SITUATION_2_PARAM,
        appContext.getUserContext().getCalculatedScenario().getCalculationId(proposedSituationId));
  }

  private void updateSubstanceParam(final LayerWMSProps layerProps) {
    layerProps.setParamFilter(CALCULATION_SUBSTANCE_PARAM, appContext.getUserContext().getEmissionValueKey().getSubstance().toDatabaseString());
  }

  public void detach() {
    if (layer != null) {
      map.removeLayer((MapLayer) layer);
      layer = null;
      selectedLayerProps = null;
    }
  }

  @Override
  public void setMap(final MapLayoutPanel map) {
    this.map = map;
  }

  @Override
  public void setVisible(final boolean visible) {
    if (layer == null) {
      return;
    }

    map.setVisible(layer, visible);
  }
}
