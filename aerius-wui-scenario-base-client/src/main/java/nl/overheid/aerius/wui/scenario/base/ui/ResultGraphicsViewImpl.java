/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.scaling.LogarithmicScalingUtil;
import nl.overheid.aerius.wui.main.widget.HeadingWidget;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.progress.ProgressPanel;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;

/**
 * View implementation of {@link ResultGraphicsView}. This class keeps state of the current calculation running, mainly because the progress panels
 * also keep their own state.
 */
@Singleton
public class ResultGraphicsViewImpl extends Composite implements ResultGraphicsView  {
  /**
   * This is the estimated height of all other elements on the page above and below the progress bars. This value is used to calculate the max
   * height of the table. The max height triggers the scrollbar to be shown.
   */

  private static ResultGraphicsViewImplUiBinder uiBinder = GWT.create(ResultGraphicsViewImplUiBinder.class);

  interface ResultGraphicsViewImplUiBinder extends UiBinder<Widget, ResultGraphicsViewImpl> {}

  interface CustomStyle extends CssResource {
    String graph();
    String ieFix();
  }

  @UiField CustomStyle style;

  @UiField FlowPanel progressPanelContainer;

  private final ArrayList<ProgressPanel> progressPanels = new ArrayList<ProgressPanel>();
  private final HashMap<Integer, HeadingWidget> titles = new HashMap<Integer, HeadingWidget>();
  private final HTML footer = new HTML();

  private final ScenarioBaseAppContext<?, ?> appContext;
  private final EventBus eventBus;

  /**
   * Keep track if a calculation was started. It is set to false when {@link #finishCalculation(boolean)} is called. this variable is
   * used to avoid calling {@link #finishCalculation(boolean)} more than once per calculation.
   */
  private boolean calculationRunning;

  @Inject
  public ResultGraphicsViewImpl(final ScenarioBaseAppContext<?, ?> appContext, final EventBus eventBus, final HelpPopupController hpC) {
    this.appContext = appContext;
    this.eventBus = eventBus;

    initWidget(uiBinder.createAndBindUi(this));
  }

  @Override
  public void refresh(final CalculatedScenario scenario, final boolean calculationRunning, final boolean calculationCancelled) {
    if (scenario == null || !scenario.isInSync()) {
      // If cs is null no progress panel can be shown. delete them if present.
      progressPanels.clear();
      progressPanelContainer.clear();
    } else {
      // update the titles of the progress bars in case a name has changed.
      for (final Entry<Integer, HeadingWidget> entry : titles.entrySet()) {
        final EmissionSourceList esl = scenario.getSources(entry.getKey());
        if (esl != null) {
          entry.getValue().setText(esl.getName());
        }
      }
      if (!calculationRunning) {
        finishCalculation(calculationCancelled);
      }
      updateFooter(appContext.getUserContext().getEmissionResultKey());
    }
  }

  @Override
  public void finishCalculation(final boolean cancelled) {
    if (!progressPanels.isEmpty()) {
      for (final ProgressPanel p : progressPanels) {
        p.updateBarHeight();
      }
    }

    if (calculationRunning) {
      calculationRunning = false;
      for (final ProgressPanel panel : progressPanels) {
        panel.finish();
      }
    }
    // add a footer
    footer.addStyleName(R.css().fontColorAlternative());
    footer.addStyleName(style.ieFix());
    progressPanelContainer.add(footer);
    updateFooter(appContext.getUserContext().getEmissionResultKey());
  }

  private void updateFooter(final EmissionResultKey emissionResultKey) {
    footer.setText(M.messages().emissionResultGraphicsFooter(
        emissionResultKey.getEmissionResultType(),
        emissionResultKey.getSubstance().getName()));
  }

  @Override
  public void startCalculation() {
    calculationRunning = true;

    for (final ProgressPanel pp : progressPanels) {
      pp.clear();
    }
    progressPanels.clear();
    progressPanelContainer.clear();
  }

  @Override
  public ProgressPanel addProgressPanel(final String name, final int calculationId) {
    final ProgressPanel progressPanel = new ProgressPanel(appContext.getUserContext(), eventBus, new LogarithmicScalingUtil(), calculationId);
    progressPanels.add(progressPanel);

    final HeadingWidget label = new HeadingWidget(2);
    titles.put(calculationId, label);
    label.setText(name);

    progressPanelContainer.add(label);
    progressPanelContainer.add(progressPanel);

    return progressPanel;
  }

  @Override
  public String getTitleText() {
    return M.messages().calculatorMenuResults();
  }
}
