/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.BaseEmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.NotEditCancellable;
import nl.overheid.aerius.wui.scenario.base.source.shipping.MaritimeRouteInputEditor.MaritimeRouteInputDriver;

/**
 * Editor for showing a list of ships groups and a edit field for a single ship.
 */
public class MaritimeRouteEmissionEditor extends BaseEmissionSourceEditor<MaritimeRouteEmissionSource> implements NotEditCancellable {

  public interface MaritimeRouteEmissionDriver extends SimpleBeanEditorDriver<MaritimeRouteEmissionSource, MaritimeRouteEmissionEditor> {
  }

  InputListEmissionEditor<RouteMaritimeVesselGroup, MaritimeRouteInputEditor> emissionSubSourcesEditor;

  private final CalculatorContext context;
  private ShippingMovementType movementType;

  private final SourceMapPanel map;

  @SuppressWarnings("unchecked")
  public MaritimeRouteEmissionEditor(final EventBus eventBus, final SourceMapPanel map, final CalculatorContext context,
      final CalculatorServiceAsync service, final HelpPopupController hpC) {
    super(M.messages().shipPanelTitle());
    this.map = map;
    this.context = context;
    emissionSubSourcesEditor = new InputListEmissionEditor<RouteMaritimeVesselGroup, MaritimeRouteInputEditor>(
        eventBus, new MaritimeRouteInputEditor(context.getCategories(), hpC), hpC, new ShipMessages(hpC), RouteMaritimeVesselGroup.class, "Shipping",
        (SimpleBeanEditorDriver<RouteMaritimeVesselGroup, InputEditor<RouteMaritimeVesselGroup>>) GWT.create(MaritimeRouteInputDriver.class),
        service, false) {

      @Override
      protected RouteMaritimeVesselGroup createNewRowEmissionValues() {
        return new RouteMaritimeVesselGroup(movementType);
      }
    };
    addContentPanel(emissionSubSourcesEditor);
    emissionSubSourcesEditor.ensureDebugId(TestID.DIVTABLE_EDITABLE);
  }

  @Override
  public void setEmissionValueKey(final EmissionValueKey key) {
    emissionSubSourcesEditor.setEmissionValueKey(key);
  }

  @Override
  public void postSetValue(final MaritimeRouteEmissionSource source) {
    setMovementType(source.getSector());
    super.postSetValue(source);
    emissionSubSourcesEditor.postSetValue(source);
    // Show the shipping route layer when editing ships
    map.setVisible((String) context.getSetting(SharedConstantsEnum.LAYER_MARITIME_SHIP_NETWORK), true);
  }

  private void setMovementType(final Sector sector) {
    switch (sector.getProperties().getMethod()) {
    case SHIPPING_MARITIME_MARITIME:
      movementType = ShippingMovementType.MARITIME;
      break;
    case SHIPPING_MARITIME_INLAND:
      movementType = ShippingMovementType.INLAND;
      break;
    case SHIPPING_MARITIME_DOCKED:
    default:
      movementType = ShippingMovementType.DOCK;
      break;
    }
  }
}
