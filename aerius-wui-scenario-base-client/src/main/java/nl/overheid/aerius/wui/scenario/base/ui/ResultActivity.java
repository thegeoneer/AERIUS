/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.wui.main.ui.HasChildActivity;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationSummaryChangedEvent;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace.ScenarioBaseResultPlaceState;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlaceFactory;
import nl.overheid.aerius.wui.scenario.base.ui.ResultView.Presenter;

public abstract class ResultActivity extends AbstractActivity implements HasChildActivity, Presenter {
  interface ResultActivityEventBinder extends EventBinder<ResultActivity> { }

  private static final ResultActivityEventBinder EVENT_BINDER = GWT.create(ResultActivityEventBinder.class);

  private final ResultView resultView;

  protected final ScenarioBaseAppContext<?, ?> appContext;

  private final PlaceController placeController;

  private final ResultPlace.ScenarioBaseResultPlaceState resultPlaceState;

  private final ScenarioBaseMapLayoutPanel map;

  private final CalculationController calculatorController;

  public ResultActivity(final ResultView resultView, final ScenarioBaseAppContext<?, ?> appContext,
      final PlaceController placeController, final CalculationController calculatorController,
      final ScenarioBaseResultPlaceState resultPlaceState, final ScenarioBaseMapLayoutPanel map) {
    this.calculatorController = calculatorController;
    this.map = map;
    this.resultView = resultView;
    this.appContext = appContext;
    this.placeController = placeController;
    this.resultPlaceState = resultPlaceState;
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    appContext.setResultPlaceState(resultPlaceState);
    panel.setWidget(resultView);
    startChild(resultView.getContentPanel(), eventBus);

    resultView.setPresenter(this);
    resultView.setTab(resultPlaceState);

    EVENT_BINDER.bindEventHandlers(this, eventBus);

    // This activity defaults to a completed calculation state.
    calculatorController.setButtonState(appContext.isCalculationRunning(), true);

    if (appContext.getCalculationState() == CalculationState.CANCELLED) {
      updateCancelCalculationState();
    } else if (appContext.getCalculationState() == CalculationState.COMPLETED) {
      updateFinishedCalculationState();
      resultView.calculationSummaryAvailable(true);
      map.setVisibleSubstanceLayers(true);
    }
  }

  @EventHandler
  void onStartCalculation(final CalculationStartEvent event) {
    calculatorController.setButtonState(true, true);
    resultView.calculationSummaryAvailable(false);

    resultView.calculationStarted();
  }

  @EventHandler
  void onCancelCalculation(final CalculationCancelEvent event) {
    updateCancelCalculationState();
    resultView.calculationSummaryAvailable(false);
    resultView.calculationCancelled();
  }

  final void updateCancelCalculationState() {
    calculatorController.setButtonState(false, true);
  }

  @EventHandler
  void onFinishCalculation(final CalculationFinishEvent event) {
    updateFinishedCalculationState();
    resultView.calculationFinished();
  }

  final void updateFinishedCalculationState() {
    calculatorController.setButtonState(false, true);
  }

  @EventHandler
  public void onCalculationSummaryChanged(final CalculationSummaryChangedEvent event) {
    resultView.calculationSummaryAvailable(true);
  }

  @Override
  public IsWidget getCalculationButtons() {
    return calculatorController.getCalculationButtons();
  }

  @Override
  public void goTo(final ResultPlace.ScenarioBaseResultPlaceState resultPlace) {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        placeController.goTo(ResultPlaceFactory.resultState2Place(resultPlace, placeController.getWhere()));
      }
    });
  }

  public void setVisibleHabitatLayer(final boolean visible) {
    map.setVisibleHabitatLayer(visible);
  }
}
