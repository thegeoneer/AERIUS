/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.place;

import java.util.Map;

import com.google.gwt.place.shared.Place;

import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.wui.main.place.CompositeTokenizer;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;
import nl.overheid.aerius.wui.main.place.Situation;

/**
 * Abstract class for every place of the calculator. All other places for the calculator extend this place. This class should not be directly used to
 * create places.
 */
public abstract class ScenarioBasePlace extends Place {
  /**
   * No id for situation 2.
   */
  public static final int NO_SID2 = -1;

  private static final String JOB_KEY = "job";
  private static final String JOB_TYPE = "jobtype";

  private static final String SITUATION = "situation";
  private static final String SID_ONE = "sid1";
  private static final String SID_TWO = "sid2";
  private static final String THEME = "theme";

  /**
   * Tokenizer for {@link CalculatorPlace}.
   */
  public abstract static class Tokenizer<P extends ScenarioBasePlace> extends CompositeTokenizer<P> {

    @Override
    protected void updatePlace(final Map<String, String> tokens, final P place) {
      place.setsId1(intValue(tokens, SID_ONE, 0));
      final int sidTwo = intValue(tokens, SID_TWO, NO_SID2);
      place.setsId2(sidTwo);
      place.setTheme(ScenarioBaseTheme.safeValueOf(tokens.get(THEME)));
      place.setSituation(sidTwo == NO_SID2 ? null : Situation.situationFromId(intValue(tokens, SITUATION, NO_SID2)));
      place.setJobKey(tokens.get(JOB_KEY));
      place.setJobType(JobType.safeValueOf(tokens.get(JOB_TYPE)));
    }

    @Override
    protected void setTokenMap(final P place, final Map<String, String> tokens) {
      put(tokens, SID_ONE, place.getSid1());
      put(tokens, THEME, place.getTheme().getType());
      if (place.getSid2() != NO_SID2) {
        put(tokens, SITUATION, place.getSituation().getId());
        put(tokens, SID_TWO, place.getSid2());
      }
      if (place.getJobKey() != null) {
        put(tokens, JOB_KEY, place.getJobKey());
      }
      final JobType currentJobType = place.getJobType();
      if (currentJobType != null) {
        put(tokens, JOB_TYPE, currentJobType.toString());
      }
    }
  }

  private ScenarioBaseTheme theme;
  private Situation situation;
  private int sId1;
  private int sId2;

  private String jobKey;
  private JobType jobType;

  protected ScenarioBasePlace() {
    sId2 = NO_SID2;
  }

  protected ScenarioBasePlace(final Place currentPlace) {
    this(currentPlace instanceof ScenarioBasePlace ? (ScenarioBasePlace) currentPlace : null);
  }

  protected ScenarioBasePlace(final ScenarioBasePlace currentPlace) {
    if (currentPlace == null) {
      sId2 = NO_SID2;
    } else {
      sId1 = currentPlace.getSid1();
      sId2 = currentPlace.getSid2();
      situation = currentPlace.getSituation();
      theme = currentPlace.getTheme();
      jobKey = currentPlace.getJobKey();
      jobType = currentPlace.getJobType();
    }
  }

  /**
   * Copies the place. Subclasses should call constructors to make sure field of super classes are copied.
   *
   * @param <P> type of the subclass implementing this copy.
   * @return copied instance
   */
  public abstract ScenarioBasePlace copy();

  protected <P extends ScenarioBasePlace> P copyTo(final P place) {
    return place;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && getSid1() == ((ScenarioBasePlace) obj).getSid1()
        && getSid2() == ((ScenarioBasePlace) obj).getSid2() && getSituation() == ((ScenarioBasePlace) obj).getSituation()
        && getTheme() == ((ScenarioBasePlace) obj).getTheme() && ((jobKey == null && ((ScenarioBasePlace) obj).jobKey == null)
            || (jobKey != null && jobKey.equals(((ScenarioBasePlace) obj).getJobKey())))
        && ((jobType == null && ((ScenarioBasePlace) obj).jobType == null)
            || (jobType != null && jobType == ((ScenarioBasePlace) obj).getJobType()));
  }

  /**
   * Returns the currently active {@link Situation} id.
   *
   * Will NOT return the ID of the COMPARISON situation. Or situation 1 id.
   *
   * @return the ID of the currently active situation.
   */
  public int getActiveSituationId() {
    return getSituation() == Situation.SITUATION_TWO && sId2 != NO_SID2 ? getSid2() : getSid1();
  }

  public int getSid1() {
    return sId1;
  }

  public int getSid2() {
    return sId2;
  }

  public ScenarioBaseTheme getTheme() {
    // return theme == null ? ScenarioBaseTheme.NATURE_AREAS : theme;
    // Only NATURE_AREAS is supported.
    return ScenarioBaseTheme.NATURE_AREAS;
  }

  /**
   * Returns the current situation.
   *
   * @return the situation.
   */
  public Situation getSituation() {
    return situation == null ? Situation.SITUATION_ONE : situation;
  }

  /**
   * Returns true if 2 situations are set in the place.
   *
   * @return true if 2 situations are set
   */
  public boolean has2Situations() {
    return sId2 != NO_SID2;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + 17 * sId1;
    result = 31 * result + 17 * sId2;
    result = 31 * result + 17 * getSituation().getId();
    result = 31 * result + 17 * getTheme().getType().hashCode();
    result = 31 * result + 17 * (jobKey == null ? 0 : jobKey.hashCode());
    result = 31 * result + 17 * (jobType == null ? 0 : jobType.hashCode());
    result = 31 * result + 17 * getClass().hashCode();
    return result;
  }

  public void setSituation(final Situation situation) {
    this.situation = situation;
  }

  public void setTheme(final ScenarioBaseTheme theme) {
    this.theme = theme;
  }

  public void setsId1(final int sId1) {
    this.sId1 = sId1;
  }

  public void setsId2(final int sId2) {
    this.sId2 = sId2;
  }

  public void setJobKey(final String jobKey) {
    this.jobKey = jobKey;
  }

  public String getJobKey() {
    return jobKey;
  }

  public void setJobType(final JobType jobType) {
    this.jobType = jobType;
  }

  public JobType getJobType() {
    return jobType;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " [situation=" + situation + ", sId1=" + sId1 + ", sId2=" + sId2 + ", theme=" + theme + ", jobKey=" + jobKey
        + ", jobType=" + jobType + "]";
  }
}
