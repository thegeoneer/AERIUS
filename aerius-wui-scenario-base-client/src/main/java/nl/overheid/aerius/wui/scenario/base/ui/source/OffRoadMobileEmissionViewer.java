/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.DivTableKeyValue;
import nl.overheid.aerius.wui.main.widget.table.TextKeyValueDivTable;

/**
 * Road Emission valued editor.
 */
public class OffRoadMobileEmissionViewer extends EmissionViewer<OffRoadMobileEmissionSource> {
  private static final OffroadMobileEmissionViewerUiBinder uiBinder = GWT.create(OffroadMobileEmissionViewerUiBinder.class);

  @UiTemplate("KeyValueTableViewer.ui.xml")
  interface OffroadMobileEmissionViewerUiBinder extends UiBinder<Widget, OffRoadMobileEmissionViewer> {}

  interface OffRoadMobileEmissionDriver extends SimpleBeanEditorDriver<OffRoadMobileEmissionSource, OffRoadMobileEmissionViewer> {}

  @UiField Label header;
  @UiField TextKeyValueDivTable table;

  private final EmissionValueKey noxKey = new EmissionValueKey(Substance.NOX);

  public OffRoadMobileEmissionViewer() {
    initWidget(uiBinder.createAndBindUi(this));
    table.ensureDebugId(TestID.TABLE_SOURCES_SECTOR_VIEWER);

    header.setText(M.messages().emissionSourceViewerRoadTitle());
  }

  @Override
  public void setValue(final OffRoadMobileEmissionSource value) {
    table.clear();

    for (final OffRoadVehicleEmissionSubSource item : value.getEmissionSubSources()) {
      addValue(item.getName(), value.getEmission(item, noxKey));
    }
  }

  private void addValue(final String key, final double value) {
    table.addRowData(new DivTableKeyValue<String, String>(key, FormatUtil.formatEmissionWithUnit(value)));
  }
}
