/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.adapters.ListEditor;
import com.google.gwt.editor.client.adapters.SimpleEditor;
import com.google.gwt.user.client.TakesValue;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.event.DeleteEvent;
import nl.overheid.aerius.wui.scenario.base.source.core.BaseEmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.NotEditCancellable;
import nl.overheid.aerius.wui.scenario.base.source.geo.ShippingRouteContainer;
import nl.overheid.aerius.wui.scenario.base.source.shipping.MaritimeMooringInputEditor.MaritimeMooringInputDriver;
import nl.overheid.aerius.wui.scenario.base.source.shipping.ShipRouteEditorSource.TransferRouteRowEditor;

/**
 * Editor for showing a list of ships groups and a edit field for a single ship.
 */
public class MaritimeMooringEmissionEditor extends BaseEmissionSourceEditor<MaritimeMooringEmissionSource> implements NotEditCancellable {

  public interface MaritimeMooringEmissionDriver extends
      SimpleBeanEditorDriver<MaritimeMooringEmissionSource, MaritimeMooringEmissionEditor> {
  }

  interface ShipRouteDriver extends
      SimpleBeanEditorDriver<List<ShippingRoute>, ListEditor<ShippingRoute, TransferRouteRowEditor>> {
  }

  LeafValueEditor<List<ShippingRoute>> inlandRoutesEditor = SimpleEditor.<List<ShippingRoute>>of();
  InputListEmissionEditor<MooringMaritimeVesselGroup, MaritimeMooringInputEditor> emissionSubSourcesEditor;

  private final CalculatorContext context;
  private final SourceMapPanel map;
  final ShippingRouteContainer shippingRouteContainer;

  private final ShipRouteDriver inlandRoutesDriver;
  private final ListEditor<ShippingRoute, TransferRouteRowEditor> inlandRoutesListEditor;

  @SuppressWarnings("unchecked")
  public MaritimeMooringEmissionEditor(final EventBus eventBus, final SourceMapPanel map,
      final CalculatorContext context, final CalculatorServiceAsync service, final HelpPopupController hpC) {
    super(M.messages().shipPanelTitle());
    emissionSubSourcesEditor = new InputListEmissionEditor<MooringMaritimeVesselGroup, MaritimeMooringInputEditor>(
        eventBus, new MaritimeMooringInputEditor(eventBus, map, context, hpC), hpC, new ShipMessages(hpC),
        MooringMaritimeVesselGroup.class, "MMooring",
        (SimpleBeanEditorDriver<MooringMaritimeVesselGroup, InputEditor<MooringMaritimeVesselGroup>>) GWT
            .create(MaritimeMooringInputDriver.class), service, false) {

      @Override
      protected MooringMaritimeVesselGroup createNewRowEmissionValues() {
        return new MooringMaritimeVesselGroup();
      }

      @Override
      public void onDelete(final DeleteEvent<MooringMaritimeVesselGroup> event) {
        event.getValue().detachRoutes();
        super.onDelete(event);
      }
    };
    this.map = map;
    this.context = context;
    inlandRoutesDriver = GWT.create(ShipRouteDriver.class);
    inlandRoutesListEditor = ListEditor.of(emissionSubSourcesEditor.getInputEditor().getInlandRouteEditorSource());
    shippingRouteContainer = (ShippingRouteContainer) map.getMartimeMooringRouteContainer();
    final ShippingRouteHandler inlandRouteHandler = new ShippingRouteHandler(inlandRoutesEditor, shippingRouteContainer) {
      @Override
      protected void onAdd(final ShippingRoute obj) {
        refreshInlandRoutes();
      }
      @Override
      protected void onRemove(final ShippingRoute obj) {
        refreshInlandRoutes();
      }
    };
    final TakesValue<List<ShippingRoute>> routeListWrapper = new TakesValue<List<ShippingRoute>>() {

      @Override
      public void setValue(final List<ShippingRoute> value) {
        // no-op: only getValue is actually used in ShippingRouteHandler, but interface TakesValue has setValue
      }

      @Override
      public List<ShippingRoute> getValue() {
        return shippingRouteContainer.getMaritimeRoutes();
      }
    };
    final ShippingRouteHandler maritimeRouteHandler = new ShippingRouteHandler(routeListWrapper, shippingRouteContainer) {
      @Override
      protected void onAdd(final ShippingRoute obj) {
        refreshMaritimeRoutes();
      }
      @Override
      protected void onRemove(final ShippingRoute obj) {
        refreshMaritimeRoutes();
      }
    };
    emissionSubSourcesEditor.getInputEditor().setRoutesContainer(inlandRouteHandler, maritimeRouteHandler);
    addContentPanel(emissionSubSourcesEditor);
    emissionSubSourcesEditor.ensureDebugId(TestID.DIVTABLE_EDITABLE);
  }

  @Override
  public void setEmissionValueKey(final EmissionValueKey key) {
    emissionSubSourcesEditor.setEmissionValueKey(key);
  }

  @Override
  public void postSetValue(final MaritimeMooringEmissionSource source) {
    super.postSetValue(source);
    // Show the shipping route layer when editing ships
    map.setVisible((String) context.getSetting(SharedConstantsEnum.LAYER_SHIP_NETWORK), true);
    map.setVisible((String) context.getSetting(SharedConstantsEnum.LAYER_MARITIME_SHIP_NETWORK), true);
    emissionSubSourcesEditor.postSetValue(source);
    refreshInlandRoutes();
    refreshMaritimeRoutes();
  }

  void refreshInlandRoutes() {
    inlandRoutesDriver.initialize(inlandRoutesListEditor);
    inlandRoutesDriver.edit(inlandRoutesEditor.getValue());
  }

  private void refreshMaritimeRoutes() {
    emissionSubSourcesEditor.getInputEditor().reeditMaritimeRoutes(shippingRouteContainer.getMaritimeRoutes());
  }
}
