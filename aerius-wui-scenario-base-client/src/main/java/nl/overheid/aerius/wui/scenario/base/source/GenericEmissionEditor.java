/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.BaseOPSEmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.EmissionRowWidget;

/**
 * Editor for plain emission values. Show an emission edit field for each
 * substance to edit.
 */
class GenericEmissionEditor extends BaseOPSEmissionSourceEditor<GenericEmissionSource> {
  private static final GenericEmissionsEditorUiBinder UI_BINDER = GWT.create(GenericEmissionsEditorUiBinder.class);

  interface GenericEmissionsEditorUiBinder extends UiBinder<Widget, GenericEmissionEditor> { }

  interface GenericEmissionsDriver extends SimpleBeanEditorDriver<GenericEmissionSource, GenericEmissionEditor> { }

  LeafValueEditor<EmissionValues> emissionValuesEditor = new LeafValueEditor<EmissionValues>() {
    private EmissionValues emissionValues;

    @Override
    public EmissionValues getValue() {
      if (emissionValues == null) {
        emissionValues = new EmissionValues(false);
      }
      emissionValues.setEmission(Substance.PM10, emissionPM10.getValue());
      emissionValues.setEmission(Substance.NOX, emissionNOX.getValue());
      emissionValues.setEmission(Substance.NH3, emissionNH3.getValue());
      return emissionValues;
    }

    @Override
    public void setValue(final EmissionValues value) {
      this.emissionValues = value;
      emissionPM10.setValue(emissionValues == null ? 0 : emissionValues.getEmission(keyPM10));
      emissionNOX.setValue(emissionValues == null ? 0 : emissionValues.getEmission(keyNOX));
      emissionNH3.setValue(emissionValues == null ? 0 : emissionValues.getEmission(keyNH3));
    }
  };

  @UiField FlowPanel panel;
  @UiField(provided = true) @Ignore EmissionRowWidget emissionNOX;
  @UiField(provided = true) @Ignore EmissionRowWidget emissionNH3;
  @UiField(provided = true) @Ignore EmissionRowWidget emissionPM10;

  private final EmissionValueKey keyPM10 = new EmissionValueKey(Substance.PM10);
  private final EmissionValueKey keyNOX = new EmissionValueKey(Substance.NOX);
  private final EmissionValueKey keyNH3 = new EmissionValueKey(Substance.NH3);

  public GenericEmissionEditor(final EventBus eventBus, final Context context, final SectorCategories categories,
      final HelpPopupController hpC) {
    super(eventBus, context, categories, hpC, M.messages().sourceEmissions());
    final String unit = M.messages().unitKgYSingular();
    emissionNOX = new EmissionRowWidget(Substance.NOX, unit, hpC, hpC.tt().ttGenericEmissionNOX(), TestID.INPUT_EMISSION_NOX);
    emissionNH3 = new EmissionRowWidget(Substance.NH3, unit, hpC, hpC.tt().ttGenericEmissionNH3(), TestID.INPUT_EMISSION_NH3);
    emissionPM10 = new EmissionRowWidget(Substance.PM10, unit, hpC, hpC.tt().ttGenericEmissionPM10(), TestID.INPUT_EMISSION_PM10);
    addContentPanel(UI_BINDER.createAndBindUi(this));

    panel.ensureDebugId(TestID.GUI_COLLAPSEPANEL_GEN);
    emissionPM10.setVisible(false); // NO PM10
  }
}
