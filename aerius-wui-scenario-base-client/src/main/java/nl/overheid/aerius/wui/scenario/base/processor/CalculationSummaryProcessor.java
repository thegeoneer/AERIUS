/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.processor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.service.CalculateServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationSummaryChangedEvent;
import nl.overheid.aerius.wui.scenario.base.events.SituationSwitchEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Handles CalculationSummary data. Registers itself with the {@link CalculationFinishEvent}
 * and {@link CalculationCancelEvent} and calls the server when finished. It then will
 * fire a {@link CalculationSummaryChangedEvent} with the retrieved data.
 */
@Singleton
public class CalculationSummaryProcessor {

  interface CalculationPostProcessorEventBinder extends EventBinder<CalculationSummaryProcessor> { }

  private static class SummaryAppAsyncCallback extends AppAsyncCallback<CalculationSummary> {

    private final boolean notify;
    private final EventBus eventBus;
    private boolean cancelled;

    public SummaryAppAsyncCallback(final EventBus eventBus, final boolean notify) {
      this.eventBus = eventBus;
      this.notify = notify;
    }

    @Override
    public void onSuccess(final CalculationSummary result) {
      if (!cancelled) {
        if (notify) {
          NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationComplete());
        }
        eventBus.fireEvent(new CalculationSummaryChangedEvent(result));
      }
    }

    @Override
    public void onFailure(final Throwable caught) {
      if (!cancelled) {
        super.onFailure(caught);
      }
    }

    public void cancel() {
      cancelled = true;
    }
  }

  private final CalculationPostProcessorEventBinder eventBinder = GWT.create(CalculationPostProcessorEventBinder.class);
  private final EventBus eventBus;
  private final CalculateServiceAsync calculateService;

  private final ScenarioBaseAppContext<?, ?> appContext;
  private final PlaceController placeController;
  private SummaryAppAsyncCallback summaryAppAsyncCallback;

  @Inject
  public CalculationSummaryProcessor(final EventBus eventBus, final ScenarioBaseAppContext<?, ?> appContext, final PlaceController placeController,
      final CalculateServiceAsync calculateService) {
    this.eventBus = eventBus;
    this.appContext = appContext;
    this.placeController = placeController;
    this.calculateService = calculateService;
    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  void onCalculationFinished(final CalculationFinishEvent event) {
    updateCalculationSummary(!event.isImportedCalculation());
  }

  @EventHandler
  void onSituationSwitchEvent(final SituationSwitchEvent event) {
    updateCalculationSummary(false);
  }

  @EventHandler
  void onCalculationCancel(final CalculationCancelEvent event) {
    eventBus.fireEvent(new CalculationSummaryChangedEvent(null));
  }

  private void updateCalculationSummary(final boolean notify) {
    if (!(placeController.getWhere() instanceof ScenarioBasePlace)) {
      return;
    }

    final CalculatedScenario calcScenario = appContext.getUserContext().getCalculatedScenario();
    final ScenarioBasePlace place = (ScenarioBasePlace) placeController.getWhere();

    //only bother to update if there is a calculatedscenario and it is in sync (no sources changed)
    if (calcScenario != null && calcScenario.isInSync()) {
      final int calcSitOne = calcScenario.getCalculationId(place.getSid1());
      final int calcSitTwo = calcScenario.getCalculationId(place.getSid2());
      // First reset current summary, then call server for new results
      final CalculationSetOptions options = calcScenario.getOptions();
      if (options != null) {
        if (summaryAppAsyncCallback != null) {
          summaryAppAsyncCallback.cancel();
        }
        eventBus.fireEvent(new CalculationSummaryChangedEvent(null));
        summaryAppAsyncCallback = new SummaryAppAsyncCallback(eventBus, notify);
        if (JobType.PRIORITY_PROJECT_UTILISATION == place.getJobType()) {
          calculateService.getSummaryInfo(options.getCalculationType(), calcSitTwo, calcSitOne, options.getSubstances(), summaryAppAsyncCallback);
        } else {
          calculateService.getSummaryInfo(options.getCalculationType(), calcSitOne, calcSitTwo, options.getSubstances(), summaryAppAsyncCallback);
        }
      }
    }
  }
}
