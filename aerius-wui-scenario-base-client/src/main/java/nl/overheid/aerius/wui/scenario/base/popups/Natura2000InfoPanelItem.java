/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.HabitatHoverType;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.info.Natura2000Info;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.ui.habitat.HabitatTypeTable;
import nl.overheid.aerius.wui.main.ui.info.Natura2000BasicInfoPanel;
import nl.overheid.aerius.wui.main.ui.info.Natura2000BasicInfoPanel.Natura2000InfoDriver;
import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.IsCollapsiblePanel;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelViewImpl.Presenter;

public class Natura2000InfoPanelItem extends AreaInfoPanelItem<Natura2000Info> implements IsCollapsiblePanel {

  private static final Natura2000PanelUiBinder UI_BINDER = GWT.create(Natura2000PanelUiBinder.class);

  interface Natura2000PanelUiBinder extends UiBinder<Widget, Natura2000InfoPanelItem> {}

  @UiField CollapsiblePanel cPanel;

  @UiField Label itemLabel;

  @UiField(provided = true) HabitatTypeTable habitatTypeTable;
  @UiField(provided = true) Natura2000BasicInfoPanel areaInfo;
  private final Natura2000InfoDriver driver = GWT.create(Natura2000InfoDriver.class);

  public Natura2000InfoPanelItem(final EventBus eventBus, final ScenarioBaseUserContext userContext, final InfoVector infoVector,
      final Presenter presenter, final HelpPopupController hpC) {
    super(eventBus, presenter);

    this.habitatTypeTable = new HabitatTypeTable(eventBus, hpC, userContext.getEmissionResultKey(), userContext);
    this.depositionMarkerTable = new DepositionMarkerTable(eventBus, userContext, infoVector, presenter);
    this.areaInfo = new Natura2000BasicInfoPanel(hpC);

    initWidget(UI_BINDER.createAndBindUi(this));
    driver.initialize(areaInfo);
    habitatTypeTable.ensureDebugId(TestID.COMPACT_HABITAT_TYPE_TABLE);
  }

  @Override
  public void setAreaInfo(final Natura2000Info result) {
    habitatTypeTable.setAreaInfo(HabitatHoverType.NATURE_AREA, result.getId());

    driver.edit(result);
    cPanel.setTitleText(result.getName());
    itemLabel.setText(result.getName());
    habitatTypeTable.asDataTable().setRowData(result.getHabitats(), true);
  }

  @Override
  public CollapsiblePanel asCollapsible() {
    return cPanel;
  }
}
