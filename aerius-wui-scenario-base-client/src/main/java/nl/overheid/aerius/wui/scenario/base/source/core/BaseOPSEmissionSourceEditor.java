/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.ops.HasOPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * {@link BaseEmissionSourceEditor} with OPS characteristics support.
 */
public abstract class BaseOPSEmissionSourceEditor<E extends EmissionSource> extends BaseEmissionSourceEditor<E> {

  private final OPSSourceCharacteristicsEditor sourceCharacteristicsEditor;

  public BaseOPSEmissionSourceEditor(final EventBus eventBus, final Context context, final SectorCategories categories, final HelpPopupController hpC,
      final String title) {
    super(title);
    sourceCharacteristicsEditor = new OPSSourceCharacteristicsEditor(eventBus, hpC, categories,
        (String) context.getSetting(SharedConstantsEnum.OPS_FACTSHEET_URL));
    getMainPanel().add(sourceCharacteristicsEditor);
    sourceCharacteristicsEditor.ensureDebugId(TestID.DIV_COLLAPSEPANEL_SOURCECHARACTERISTICS);
  }

  @Path("sourceCharacteristics")
  public OPSSourceCharacteristicsEditor getSourceCharacteristicsEditor() {
    return sourceCharacteristicsEditor;
  }

  @Override
  public void setEmissionValueKey(final EmissionValueKey key) {
    sourceCharacteristicsEditor.setEmissionValueKey(key);
  }

  @Override
  public void setValue(final E source) {
    super.setValue(source);
    sourceCharacteristicsEditor.setVisible(source instanceof HasOPSSourceCharacteristics);
    sourceCharacteristicsEditor.setVisibility(source.getSector(), source.getGeometry(), source instanceof HasOPSSourceCharacteristics);
  }

  @Override
  public void focusOnErrors() {
    if (sourceCharacteristicsEditor.hasErrors()) {
      sourceCharacteristicsEditor.asCollapsible().setValue(Boolean.TRUE, false);
    }
  }
}
