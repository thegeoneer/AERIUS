/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable.EditableDivTableMessages;

/**
 * Shipping (help) messages on buttons.
 */
class ShipMessages implements EditableDivTableMessages {
  private final HelpPopupController hpC;

  public ShipMessages(final HelpPopupController hpC) {
    this.hpC = hpC;
  }

  @Override
  public String add() {
    return M.messages().shipAdd();
  }

  @Override
  public HelpInfo editHelp() {
    return hpC.tt().ttShipEdit();
  }

  @Override
  public HelpInfo copyHelp() {
    return hpC.tt().ttShipCopy();
  }

  @Override
  public HelpInfo deleteHelp() {
    return hpC.tt().ttShipDelete();
  }

  @Override
  public HelpInfo addHelp() {
    return hpC.tt().ttShipAdd();
  }

  @Override
  public String customButtonText() {
    return null;
  }

  @Override
  public HelpInfo customButtonHelp() {
    return null;
  }
}
