/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import java.util.HashSet;

import org.gwtopenmaps.openlayers.client.event.MapMoveEndListener;
import org.gwtopenmaps.openlayers.client.layer.Layer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.AbstractVectorLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.client.events.LayerChangeEvent;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.shared.domain.SimpleFilter;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.geo.ComparisonLegend;
import nl.overheid.aerius.wui.geo.ReceptorLegend;
import nl.overheid.aerius.wui.main.event.EmissionResultDisplaySettingChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Abstract layer for showing emission results on a map.
 */
public abstract class EmissionResultLayer extends AbstractVectorLayer {
  interface EmissionResultLayerEventBinder extends EventBinder<EmissionResultLayer> { }

  private final EmissionResultLayerEventBinder eventBinder = GWT.create(EmissionResultLayerEventBinder.class);

  private final MapMoveEndListener mapMoveEndHandler = new MapMoveEndListener() {
    @Override
    public void onMapMoveEnd(final MapMoveEndEvent eventObject) {
      refreshLayer();
    }
  };

  private static final int REFRESH_DELAY = 200;

  private EmissionResultKey key;
  private EmissionResultValueDisplaySettings displaySettings;
  private int year;
  private ScenarioBasePlace place;

  private final Timer refreshTimer = new Timer() {
    @Override
    public void run() {
      drawFeatures();
    }
  };

  private HandlerRegistration hr;
  private Legend legend;

  public EmissionResultLayer(final String name, final MapLayoutPanel map, final EmissionResultKey initalKey, final ScenarioBasePlace place,
      final EmissionResultValueDisplaySettings displaySettings) {
    super(name, map);

    //override the layervisibility as it depends if we have results for the year/key.
    final LayerVisibility lv = new LayerVisibility() {
      @Override
      protected void setNotVisibleReason(final String notVisibleReason) {
        super.setNotVisibleReason(hasResults(year, key) ? null : M.messages().layerNoDataForYear(getName(), String.valueOf(year)));
      }
    };
    lv.setLayer(this);
    setLayerVisibility(lv);

    this.displaySettings = displaySettings;
    this.key = initalKey;
    this.place = place;

    getLayerProps().setEnabled(true);

    updateLegend();

    getMap().addMapMoveEndListener(mapMoveEndHandler);
  }

  @EventHandler
  void onPlaceChange(final PlaceChangeEvent event) {
    this.place = (ScenarioBasePlace) event.getValue();
    refreshLayer(true);
    updateLegend();
  }

  @EventHandler
  void onYearChange(final YearChangeEvent event) {
    year = event.getValue();
    map.setVisible(this, getLayerProps().isEnabled());
  }

  @EventHandler
  void onSubstanceChange(final EmissionResultKeyChangeEvent event) {
    key = event.getValue();
    updateMap();
  }

  void onEmissionResultDisplaySettingsChange(final EmissionResultDisplaySettingChangeEvent event) {
    displaySettings = event.getValue();
    updateMap();
  }

  protected EmissionResultKey getKey() {
    return key;
  }

  protected ScenarioBasePlace getPlace() {
    return place;
  }

  /**
   * Returns true if this layer has results for this year. The default is true.
   * @param year
   * @return
   */
  protected boolean hasResults(final int year, final EmissionResultKey key) {
    return true;
  }

  @Override
  public void attachLayer() {
    super.attachLayer();
    hr = eventBinder.bindEventHandlers(this, map.getEventBus());
  }

  @Override
  public void destroy() {
    super.destroy();
    map.removeLayer((Layer) this);
    hr.removeHandler();
  }

  @Override
  public Legend getLegend() {
    return legend;
  }

  /**
   * Refreshing the layer may cause the browser to freeze for an extended period of time (up to a few seconds) due
   * to it having to redraw thousands of polygons on the map.
   *
   * This method could potentially be called in some rapid succession, so we've implemented a timer that will
   * start running on a method call and cancel a previously started timer, if any, then run the actual redraw call
   * when the scheduled time has passed.
   *
   * In production, it appears that a MapMove or MapZoom event is called twice, causing this method to be called
   * twice, and a refresh to happen twice in all cases. This problem is part of the reason the timer solution has
   * been introduced here. The reason for a double event is unknown and does not seem to occur in development mode,
   * which makes the cause hard to pinpoint. Production compilations also fire a MapMoveEnd event on MapZoom. There
   * is no reason for this but it happens.
   */
  @Override
  public void refreshLayer() {
    refreshLayer(false);
  }

  /**
   * Emission result layers may choose to wait while refreshing the map. Pass true if it's required to instantly refresh.
   * @param force if true layer is directly refreshed, else it's refreshed after a certain time
   */
  public void refreshLayer(final boolean force) {
    refreshTimer.cancel();

    if (!force) {
      refreshTimer.schedule(REFRESH_DELAY);
    } else {
      clearFeatures();
      drawFeatures();
    }
  }

  /**
   *
   * @param calculationId
   * @param values
   */
  public abstract void setValues(int calculationId, HashSet<AeriusResultPoint> values);

  /**
   *
   * @param filter
   */
  public abstract void setFilter(SimpleFilter<AeriusResultPoint> filter);

  protected abstract void drawFeatures();

  protected abstract void clearFeatures();

  public void updateMap() {
    if (key != null) {
      map.setLayerTitle(this, M.messages().emissionResultType(key.getEmissionResultType()) + " " + key.getSubstance().name());
    }
    refreshLayer(true);
    updateLegend();
  }

  private void updateLegend() {
    if (key != null) {
      if (place.getSituation() == Situation.COMPARISON) {
        legend = new ComparisonLegend(isHexagon(), key.getEmissionResultType(), displaySettings);
      } else {
        legend = new ReceptorLegend(isHexagon(), key.getEmissionResultType(), displaySettings);
      }
      map.getEventBus().fireEvent(new LayerChangeEvent(this, LayerChangeEvent.CHANGE.LEGEND_CHANGED));
    }
  }

  protected boolean isHexagon() {
    return false;
  }
}
