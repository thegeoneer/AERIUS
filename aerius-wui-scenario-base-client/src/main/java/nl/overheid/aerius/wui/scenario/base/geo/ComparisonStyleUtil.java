/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import java.util.HashMap;

import org.gwtopenmaps.openlayers.client.Style;

import nl.overheid.aerius.shared.domain.deposition.ComparisonEnum;
import nl.overheid.aerius.shared.util.ColorUtil;

/**
 * Util to get a static style object to be used on a map layer.
 */
final class ComparisonStyleUtil {

  private static final HashMap<ComparisonEnum, Style> HEXAGON_STYLES = new HashMap<ComparisonEnum, Style>();

  static {
    for (final ComparisonEnum ce : ComparisonEnum.values()) {
      final Style style = new Style();

      style.setFillColor(ColorUtil.webColor(ce.getColor()));
      style.setFillOpacity(1d);
      style.setStrokeWidth(1d);
      style.setStrokeColor("#FFF");
      HEXAGON_STYLES.put(ce, style);
    }
  }

  private ComparisonStyleUtil() {
  }

  public static Style getStyle(final Double value) {
    return HEXAGON_STYLES.get(ComparisonEnum.getRange(value));
  }
}
