/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.main.AsyncActivityMapper;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.ParamUtil;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseView;

/**
 */
public abstract class ScenarioBaseActivityMapper<F extends ScenarioBaseActivityFactory, A extends ScenarioBaseAppContext<C, UC>, C extends ScenarioBaseContext, UC extends ScenarioBaseUserContext> extends AsyncActivityMapper<F, A, C, UC> {
  private static ScenarioBaseStartup startup;

  private final PlaceController placeController;

  public ScenarioBaseActivityMapper(final F factory, final A context, final PlaceController placeController) {
    super(factory, context);

    this.placeController = placeController;
  }

  protected void initSourcesList(final Place place, final ScenarioBaseUserContext userContext) {
    if (place instanceof ScenarioBasePlace) {
      context.initEmissionSourceList(((ScenarioBasePlace) place).getSid1());
      context.initEmissionSourceList(((ScenarioBasePlace) place).getSid2());
    }
  }

  protected void redirectPlace(final Place place) {
    Scheduler.get().scheduleFinally(new ScheduledCommand() {
      @Override
      public void execute() {
        placeController.goTo(place);
      }
    });
  }

  /**
   * Returns the emission source from the place and source id. It checks the place and if
   * situation is the second situation it takes the id of the second situation else it takes
   * the id of the first situation. If no id set it takes 0 and creates source lists in case no
   * source lists exist.
   *
   * @param place
   * @param sourceId
   * @return
   */
  protected EmissionSource getEmissionSource(final ScenarioBaseAppContext<?, ?> appContext, final ScenarioBaseUserContext userContext,
      final ScenarioBasePlace place, final int sourceId) {
    final EmissionSourceList sources = userContext.getScenario().getSources(place.getActiveSituationId());
    final EmissionSource es = sourceId == 0 ? null : sources.getById(sourceId);
    return es == null ? appContext.getNewEmissionSource() : es;
  }

  /**
   * Class to be called when the application starts up. In the constructor
   * extra actions initialization actions that should be done when the application is
   * started. This class is created via the ActivityFactory in order to get other
   * classes injected. This class should only be created once, and that should
   * be when the application is started. This
   */
  public static class ScenarioBaseStartup {

    @Inject
    public ScenarioBaseStartup(final ScenarioBaseAppContext<?, ?> appContext, final ScenarioBaseImportController importController) {
      Window.addWindowClosingHandler(new ClosingHandler() {
        @Override
        public void onWindowClosing(final ClosingEvent event) {
          if (!ParamUtil.inDebug() && appContext != null
              && appContext.getUserContext().getScenario().getSources(0) != null
              && !appContext.getUserContext().getScenario().getSources(0).isEmpty()) {
            event.setMessage(M.messages().calculatorLeavePageQuestion());
          }
        }
      });
      Scheduler.get().scheduleFinally(new ScheduledCommand() {
        @Override
        public void execute() {
          importController.processStartUpImport();
        }
      });
    }
  }

  /**
   * Class with method called prior to calling the activity presenter.
   */
  public static class PreActivity {
    /**
     * Constructor performing pre activity operations. It's done via the constructor to be able to use injected methods.
     */
    @Inject
    public PreActivity(final ScenarioBaseAppContext<?, ?> appContext, final ScenarioBaseMapLayoutPanel map, final ScenarioBaseView view,
        @Assisted final ScenarioBasePlace place) {
      final ScenarioBaseUserContext userContext = appContext.getUserContext();
      view.initEmissionValueKey(userContext.getEmissionValueKey());
      map.getMarkersLayer().setMarkers(userContext.getScenario(), place);
    }
  }

  protected void initSingleton() {
    if (startup == null) {
      // only call once.
      startup = factory.startup();
    }
  }
}
