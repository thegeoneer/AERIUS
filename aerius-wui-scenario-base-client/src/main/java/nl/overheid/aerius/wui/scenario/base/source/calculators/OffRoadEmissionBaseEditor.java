/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.calculators;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;

import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.FuelTypeProperties;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.MachineryFuelType;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleSpecification;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;

public abstract class OffRoadEmissionBaseEditor<S extends OffRoadVehicleSpecification> extends Composite implements HasValueChangeHandlers<Double> {

  @UiField(provided = true) ListBoxEditor<OffRoadMachineryType> machineryType = new ListBoxEditor<OffRoadMachineryType>() {
    @Override
    protected String getLabel(final OffRoadMachineryType value) {
      return value.getName();
    }
  };
  @UiField(provided = true) ListBoxEditor<MachineryFuelType> fuelType = new ListBoxEditor<MachineryFuelType>() {
    @Override
    protected String getLabel(final MachineryFuelType value) {
      return value.getName();
    }
  };

  private final ScenarioBaseContext appContext;

  public OffRoadEmissionBaseEditor(final ScenarioBaseContext appContext) {
    this.appContext = appContext;

    machineryType.ensureDebugId(TestID.OFF_ROAD_CALCULATOR_MACHINERY_LIST);
    fuelType.ensureDebugId(TestID.OFF_ROAD_CALCULATOR_FUEL_LIST);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    machineryType.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_MACHINERY_LIST);
    fuelType.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_FUEL_LIST);
  }

  @UiHandler("machineryType")
  void onChangeMachineryListBox(final ChangeEvent e) {
    updateFuelBox();
  }

  @UiHandler("fuelType")
  void onChangeFuelTypeListBox(final ChangeEvent e) {
    setDefaultFuelProperties(machineryType.getValue(), fuelType.getValue());
  }

  private void setDefaultFuelProperties(final OffRoadMachineryType value, final MachineryFuelType value2) {
    for (final FuelTypeProperties ftp : value.getFuelTypes()) {
      if (ftp.getFuelType().equals(value2)) {
        setDefaultFuelProperties(ftp);
        break;
      }
    }
  }

  protected abstract void setDefaultFuelProperties(FuelTypeProperties fuelTypeProperties);

  /**
   * Trigger update emission value shown when widget becomes visible.
   */
  public void refresh() {
    final OffRoadVehicleSpecification spec = getOffRoadVehicleSpecification();

    spec.setFuelType(fuelType.getValue());
    ValueChangeEvent.fire(this, spec.getEmission());
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Double> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  private void updateFuelBox() {
    fuelType.clear();
    for (final FuelTypeProperties ftp : machineryType.getValue().getFuelTypes()) {
      fuelType.addItem(ftp.getFuelType());
    }
    setDefaultFuelProperties(machineryType.getValue(), fuelType.getValue());
  }

  protected abstract S getOffRoadVehicleSpecification();

  public void setSector(final Sector sector) {
    machineryType.clear();

    final ArrayList<OffRoadMachineryType> sectorTypes = new ArrayList<>();

    for(final OffRoadMachineryType type : appContext.getCategories().getOffRoadMachineryTypes()) {
      if (type.getSectorId() == sector.getSectorId()) {
        sectorTypes.add(type);
      }
    }

    machineryType.addItems(sectorTypes);
    updateFuelBox();
  }
}