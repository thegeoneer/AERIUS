/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.List;

import com.google.gwt.user.client.TakesValue;

import nl.overheid.aerius.shared.domain.ConsecutiveNamedList;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.wui.scenario.base.source.geo.ShippingRouteContainer;

/**
 * Route Handler for binding the route lists with an individual input editor.
 */
abstract class ShippingRouteHandler {

  private final TakesValue<List<ShippingRoute>> takesValueRoutes;
  private final ShippingRouteContainer shippingRouteContainer;

  public ShippingRouteHandler(final TakesValue<List<ShippingRoute>> inlandRoutesList, final ShippingRouteContainer shippingRouteContainer) {
    this.takesValueRoutes = inlandRoutesList;
    this.shippingRouteContainer = shippingRouteContainer;
  }

  /**
   * Add route to the list.
   * @param route
   */
  public void add(final ShippingRoute obj) {
    final List<ShippingRoute> list = takesValueRoutes.getValue();
    obj.setId(ConsecutiveNamedList.findFreeId(list));
    list.add(obj);
    shippingRouteContainer.add(obj);
    onAdd(obj);
  }

  /**
   * Get the route from the list on the given index.
   * @param index
   * @return
   */
  public ShippingRoute get(final int index) {
    return takesValueRoutes.getValue().get(index);
  }

  /**
   * Remove route from the list.
   * @param route
   * @return
   */
  public boolean remove(final ShippingRoute obj) {
    final boolean remove = shippingRouteContainer.removeRouteAndRedrawWhenNeeded(obj);
    takesValueRoutes.getValue().remove(obj);
    if (remove) {
      onRemove(obj);
    }
    return remove;
  }

  /**
   * Redraws the route by removing and re-adding the route.
   * @param route to refresh
   */
  public void refresh(final ShippingRoute route) {
    shippingRouteContainer.removeRouteAndRedrawWhenNeeded(route);
    shippingRouteContainer.add(route);
  }

  /**
   * Returns the number of routes.
   * @return
   */
  public int size() {
    return takesValueRoutes.getValue() == null ? 0 : takesValueRoutes.getValue().size();
  }

  /**
   * Route has been added.
   * @param obj added route
   */
  protected abstract void onAdd(final ShippingRoute obj);

  /**
   * Route has been removed.
   * @param obj removed route
   */
  protected abstract void onRemove(final ShippingRoute obj);
}
