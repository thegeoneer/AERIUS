/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleSpecificEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.DivTableKeyValue;
import nl.overheid.aerius.wui.main.widget.table.TextKeyValueDivTable;

/**
 * Road Emission valued editor.
 */
public class SRM2EmissionViewer extends EmissionViewer<SRM2EmissionSource> {
  private static final SRM2EmissionViewerUiBinder UI_BINDER = GWT.create(SRM2EmissionViewerUiBinder.class);

  @UiTemplate("KeyValueTableViewer.ui.xml")
  interface SRM2EmissionViewerUiBinder extends UiBinder<Widget, SRM2EmissionViewer> { }

  interface SRM2EmissionDriver extends SimpleBeanEditorDriver<SRM2EmissionSource, SRM2EmissionViewer> { }

  @UiField Label header;
  @UiField Label headerRight;
  @UiField TextKeyValueDivTable table;

  public SRM2EmissionViewer() {
    initWidget(UI_BINDER.createAndBindUi(this));
    table.ensureDebugId(TestID.TABLE_SOURCES_SECTOR_VIEWER);

    header.setText(M.messages().emissionSourceViewerRoadTitle());
    headerRight.setText(M.messages().emissionSourceViewerShippingUnit());
  }

  @Override
  public void setValue(final SRM2EmissionSource value) {
    table.clear();

    final EmissionValueKey emissionValueKey = new EmissionValueKey(year, Substance.NOX);

    for (final VehicleEmissions item : value.getEmissionSubSources()) {
      final double emission = value.getEmission(item, emissionValueKey);
      if (item instanceof VehicleStandardEmissions) {
        final VehicleStandardEmissions standard = (VehicleStandardEmissions) item;
        addValue(M.messages().roadVehicleType(standard.getEmissionCategory().getVehicleType()), emission);
      } else if (item instanceof VehicleSpecificEmissions) {
        final VehicleSpecificEmissions specific = (VehicleSpecificEmissions) item;
        addValue(specific.getCategory().getName(), emission);
      }
    }
  }

  private void addValue(final String key, final double value) {
    table.addRowData(new DivTableKeyValue<String, String>(key, FormatUtil.formatEmissionWithUnit(value)));
  }
}
