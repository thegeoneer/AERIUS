/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;


import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.HasShippingRoute;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayTypeUtil;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.util.FormatUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Editor for waterway category and the waterway direction.
 */
class InlandWaterwayTypeEditor extends Composite implements ValueAwareEditor<HasShippingRoute> {

  interface WaterwayCategoryEditorUiBinder extends UiBinder<Widget, InlandWaterwayTypeEditor> { }

  private static final WaterwayCategoryEditorUiBinder UI_BINDER = GWT.create(WaterwayCategoryEditorUiBinder.class);

  @UiField @Ignore Label labelId;
  @UiField(provided = true) ListBoxEditor<InlandWaterwayCategory> waterwayCategoryEditor;
  @UiField(provided = true) ListBoxEditor<WaterwayDirection> waterwayDirectionEditor;
  final LeafValueEditor<Integer> idEditor = new LeafValueEditor<Integer>() {
    private Integer value;

    @Override
    public void setValue(final Integer value) {
      this.value = value;
      labelId.setText(FormatUtil.formatAlphabeticalExactUpperCase(value));
    }

    @Override
    public Integer getValue() {
      return value;
    }
  };
  private EditorDelegate<HasShippingRoute> delegate;
  private final EventBus eventBus;
  private final DetermineWaterway determineWaterway;

  public InlandWaterwayTypeEditor(final EventBus eventBus, final HelpPopupController hpC, final CalculatorServiceAsync service,
      final InlandShippingCategories categories, final boolean showLabel) {
    this.eventBus = eventBus;
    waterwayCategoryEditor = new ListBoxEditor<InlandWaterwayCategory>() {
      @Override
      protected String getLabel(final InlandWaterwayCategory value) {
        return value == null ? M.messages().shipWaterwayEmptyOption() : value.getDescription();
      }
    };
    waterwayCategoryEditor.addChangeHandler(new ChangeHandler() {
      @Override
      public void onChange(final ChangeEvent event) {
        updateDirectionAndUpdateRoutes();
      }
    });
    waterwayDirectionEditor = new ListBoxEditor<WaterwayDirection>() {
      @Override
      protected String getLabel(final WaterwayDirection direction) {
        return direction == null ? M.messages().shipWaterwayDirectionEmptyOption() : M.messages().shipWaterwayDirection(direction);
      }
    };
    waterwayDirectionEditor.addChangeHandler(new ChangeHandler() {
      @Override
      public void onChange(final ChangeEvent event) {
        updateInlandRoutEmissionSource();
      }
    });
    determineWaterway = new DetermineWaterway(eventBus, service, categories) {
      @Override
      protected void updateWaterwayType(final InlandWaterwayType waterwayType) {
        waterwayCategoryEditor.setValue(waterwayType.getWaterwayCategory());
        setDirection(waterwayType.getWaterwayDirection());
        updateDirectionAndUpdateRoutes();
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));
    setLabels(showLabel);
    waterwayCategoryEditor.addFirstEmptyItem();
    waterwayCategoryEditor.addItems(categories.getWaterwayCategories());

    hpC.addWidget(waterwayCategoryEditor, hpC.tt().ttWaterwayEditor());
    hpC.addWidget(waterwayDirectionEditor, hpC.tt().ttWaterwayEditor());

    // IDS
    waterwayCategoryEditor.ensureDebugId(TestID.INPUT_WATERWAY);
    waterwayDirectionEditor.ensureDebugId(TestID.INPUT_WATERWAY_DIRECTION);
  }

  private void setLabels(final boolean showLabel) {
    if (!showLabel) {
      labelId.setVisible(false);
    }
    waterwayCategoryEditor.addStyleName(R.css().sourceDetailCol1());
    waterwayDirectionEditor.addStyleName(R.css().sourceDetailCol2());
  }

  private void updateInlandRoutEmissionSource() {
    final InlandWaterwayType iwt = new InlandWaterwayType();
    iwt.setWaterwayCategory(waterwayCategoryEditor.getValue());
    iwt.setWaterwayDirection(waterwayDirectionEditor.getValue());
    eventBus.fireEvent(new InlandWaterwayTypeUpdateEvent(idEditor.getValue(), iwt));
  }

  public void showError(final boolean error) {
    //no-op - update parent to make widget visible to show error
  }

  @Override
  public void setDelegate(final EditorDelegate<HasShippingRoute> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void flush() {
    final boolean categoryIsNull = waterwayCategoryEditor.getValue() == null;
    if (categoryIsNull) {
      showError(true);
      delegate.recordError(M.messages().shipWaterwayRequired(), null, waterwayCategoryEditor);
    }
    if (!InlandWaterwayTypeUtil.isDirectionCorrect(waterwayCategoryEditor.getValue(), waterwayDirectionEditor.getValue())) {
      showError(true);
      delegate.recordError(M.messages().shipWaterwayDirectionRequired(), null, waterwayDirectionEditor);
    }
  }

  @Override
  public void onPropertyChange(final String... paths) {
    //NO-OP
  }

  @Override
  public void setValue(final HasShippingRoute value) {
    if (value != null) {
      waterwayCategoryEditor.setValue(value.getWaterwayCategory());
      determineWaterway.determineWaterwayIf(value);
      setDirection(value.getWaterwayDirection());
    }
  }

  private void updateDirectionAndUpdateRoutes() {
    setDirection(waterwayDirectionEditor.getValue());
    updateInlandRoutEmissionSource();
  }

  private void setDirection(final WaterwayDirection waterwayDirection) {
    waterwayDirectionEditor.clear();
    final InlandWaterwayCategory category = waterwayCategoryEditor.getValue();
    if (category != null) {
      if (category.isDirectionRelevant()) {
        waterwayDirectionEditor.addFirstEmptyItem();
      }
      waterwayDirectionEditor.addItems(category.getDirections());
      waterwayDirectionEditor.setValue(waterwayDirection);
    }
    setDirectionVisible(category != null && category.isDirectionRelevant());
  }

  private void setDirectionVisible(final boolean visible) {
    waterwayDirectionEditor.setVisible(visible);
  }
}
