/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import com.google.inject.Inject;

import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.shared.LayerProps.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.WMSLayerType;
import nl.overheid.aerius.wui.geo.LegendTitleFactory;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;

public class CalculatorLegendTitleFactory implements LegendTitleFactory {

  private final ScenarioBaseAppContext<?, ?> appContext;

  @Inject
  public CalculatorLegendTitleFactory(final ScenarioBaseAppContext<?, ?> appContext) {
    this.appContext = appContext;
  }

  @Override
  public String getLegendTitle(final MapLayer layer) {
    final String title;
    if (!((layer.getLegend() instanceof ColorRangesLegend) && ((ColorRangesLegend) layer.getLegend()).isHexagon())) {
      title = DEFAULT_TITLE;
    } else if (layer.getLayerProps().getName().equals(
        WMSLayerType.WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION_PERCENTAGE.getLayerName(ProductType.CALCULATOR))) {
      title = M.messages().unitPercentageSingular();
    } else {
      switch (appContext.getUserContext().getEmissionResultKey().getEmissionResultType()) {
      case CONCENTRATION:
        title = M.messages().unitUgM3Singular();
        break;
      case NUM_EXCESS_DAY:
        title = M.messages().unitDaysYearSingular();
        break;
      case WET_DEPOSITION:
      case DRY_DEPOSITION:
      case DEPOSITION:
        title = M.messages().unitDepositionSingularHaY(appContext.getUserContext().getEmissionResultValueDisplaySettings().getDisplayType());
        break;
      default:
        throw new IllegalStateException("Invalid emission result type: "
            + appContext.getUserContext().getEmissionResultKey().getEmissionResultType().name());
      }
    }
    return title;
  }
}
