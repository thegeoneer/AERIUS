/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.widget.table.ContextDivTable;
import nl.overheid.aerius.wui.main.widget.table.ImageColumn;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.StyledPointColumn;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;
import nl.overheid.aerius.wui.scenario.base.ui.source.ConciseEmissionValuesWidget;

/**
 * Table displaying sources.
 */
public class SourcesDivTable extends Composite implements IsInteractiveDataTable<EmissionSource> {
  interface SourcesDivTableUiBinder extends UiBinder<Widget, SourcesDivTable> {}

  private static final SourcesDivTableUiBinder UI_BINDER = GWT.create(SourcesDivTableUiBinder.class);

  @UiField SimpleInteractiveClickDivTable<EmissionSource> divTable;

  @UiField(provided = true) StyledPointColumn<EmissionSource> idColumn;
  @UiField(provided = true) ImageColumn<EmissionSource> iconColumn;
  @UiField(provided = true) TextColumn<EmissionSource> labelColumn;

  @Inject
  public SourcesDivTable(final EventBus eventBus, final ConciseEmissionValuesWidget context) {
    // Row number column
    idColumn = new StyledPointColumn<EmissionSource>(ColorUtil.white(), eventBus) {
      @Override
      protected String getText(final EmissionSource object) {
        return String.valueOf(object.getId());
      }

      @Override
      protected String getColor(final EmissionSource object) {
        return ColorUtil.webColor(object.getSector().getProperties().getColor());
      }

      @Override
      public void applyCellOptions(final Widget cell, final EmissionSource object) {
        super.applyCellOptions(cell, object);

        cell.addHandler(new ClickHandler() {
          @Override
          public void onClick(final ClickEvent event) {
            eventBus.fireEvent(new LocationChangeEvent(new BBox(object.getX(), object.getY(), 0)));
          }
        }, ClickEvent.getType());
      }
    };

    // Sector icon column
    iconColumn = new ImageColumn<EmissionSource>() {
      @Override
      public ImageResource getValue(final EmissionSource object) {
        return SectorImageUtil.getEmissionSourceImageResource(object);
      }
    };

    // Label column
    labelColumn = new TextColumn<EmissionSource>() {
      @Override
      public String getValue(final EmissionSource object) {
        return object.getLabel();
      }
    };

    idColumn.ensureDebugId(TestID.ID);
    iconColumn.ensureDebugId(TestID.ICON);
    labelColumn.ensureDebugId(TestID.LABEL);

    initWidget(UI_BINDER.createAndBindUi(this));
    ContextDivTable.wrap(divTable, context);
  }

  @Override
  public SimpleInteractiveClickDivTable<EmissionSource> asDataTable() {
    return divTable;
  }
}
