/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.ScenarioSectorUtil;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;

/**
 * Presenter for emission sources list.
 */
public abstract class EmissionSourcesPresenter extends SituationActivity implements EmissionSourcesView.Presenter {
  protected final SourcesPlace place;
  private final EmissionSourcesView view;

  public EmissionSourcesPresenter(final PlaceController placeController, final CalculationController calculatorController,
      final SituationView parentView, final EmissionSourcesView view, @Assisted final SourcesPlace place,
      final ScenarioBaseAppContext<?, ?> appContext) {
    super(placeController, calculatorController, appContext, place, parentView);
    this.view = view;
    this.place = place;
  }

  @Override
  public void startChild(final AcceptsOneWidget panel, final EventBus eventBus) {
    panel.setWidget(view);
  }

  @Override
  protected void refresh(final Situation situation, final EmissionValueKey key, final EmissionResultKey resultKey) {
    final Scenario scenario = appContext.getUserContext().getScenario();
    if (situation == Situation.COMPARISON) {
      final EmissionSourceList sit1 = scenario.getSources(place.getSid1());
      final EmissionSourceList sit2 = scenario.getSources(place.getSid2());

      view.setValue(key, ScenarioSectorUtil.getDifferences(sit1, sit2, key), M.messages().compareEmissionListExplain(sit1.getName(), sit2.getName()));
    } else {
      final EmissionSourceList sources = scenario.getSources(place.getActiveSituationId());
      if (sources != null) {
        final boolean enableAdding = appContext.getUserContext().getCalculatorLimits().isWithinMaxSourcesLimit(sources.size() + 1);
        if (key.getSubstance() == Substance.NOXNH3) {
          final EmissionValueKey noxKey = new EmissionValueKey(key.getYear(), Substance.NOX);
          final EmissionValueKey nh3Key = new EmissionValueKey(key.getYear(), Substance.NH3);
          view.setValue(key, enableAdding, sources, sources.getTotalEmission(noxKey), sources.getTotalEmission(nh3Key));
        } else {
          view.setValue(key, enableAdding, sources, sources.getTotalEmission(key), 0);
        }
      }
    }
  }
}
