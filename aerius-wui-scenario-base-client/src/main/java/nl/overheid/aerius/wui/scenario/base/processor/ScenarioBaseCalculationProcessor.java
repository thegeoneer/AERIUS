/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.processor;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationInitResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultReady;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.calculation.ResultHighValuesByDistances;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.CalculateServiceAsync;
import nl.overheid.aerius.wui.main.event.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.scenario.base.domain.ScenarioUtil;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationResultsRetrievedEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.events.ReceptorChangeEvent;
import nl.overheid.aerius.wui.scenario.base.events.ResultHighValuesByDistancesEvent;

/**
 * Cancel() may be called when a calculation needs to be interrupted.
 */
public abstract class ScenarioBaseCalculationProcessor {

  private static final int SECONDS_TO_MILLIS = 1000;
  /**
   * The CalculateServiceAsync used to calculate deposition
   */
  protected final CalculateServiceAsync service;

  interface ProcessorEventBinder extends EventBinder<ScenarioBaseCalculationProcessor> {}

  private final ProcessorEventBinder eventBinder = GWT.create(ProcessorEventBinder.class);

  /**
   * When a CalculationAgent has retrieved ResultHighValuesByDistances's, this callback will be used to broadcast them over the event bus.
   */
  private final AsyncCallback<ResultHighValuesByDistances> progressCallback = new AsyncCallback<ResultHighValuesByDistances>() {
    @Override
    public void onSuccess(final ResultHighValuesByDistances results) {
      if (!cancelled) {
        if (isCalculationActual()) {
          if (results != null) {
            eventBus.fireEvent(new ResultHighValuesByDistancesEvent(results));
          }
        } else {
          eventBus.fireEvent(new CalculationCancelEvent());
        }
      }
      finish();
    }

    @Override
    public void onFailure(final Throwable caught) {
      onServiceFailure(caught);
    }
  };

  private final Timer resultsDelayedNotificationTimer = new Timer() {
    @Override
    public void run() {
      notifyDelayedProgress();
    }
  };

  private final AsyncCallback<CalculationResultReady> resultsReadyCallBack = new AsyncCallback<CalculationResultReady>() {
    @Override
    public void onSuccess(final CalculationResultReady result) {
      if (result == null || !result.getKey().equals(lastCalculationKey) || cancelled) {
        // Ignore results for result of old calculations and cancelled results.
        return;
      }

      eventBus.fireEvent(new CalculationResultsRetrievedEvent());
      onResultsReceived();

      if (result.isCalculationFinished()) {
        resultAgent.stop();
        progressAgent.stop();
        // fetch calculation result points in case of a CUSTOM_POINTS calculation
        if (CalculationType.CUSTOM_POINTS == lastScenario.getOptions().getCalculationType()) {
          service.getCalculationResultPoints(lastCalculationKey, calculationResultPointsCallBack);
        } else {
          finished = true;
          finish();
        }
        return;
      }
    }

    @Override
    public void onFailure(final Throwable caught) {
      cancel();
      onServiceFailure(caught);
    }
  };

  private final AsyncCallback<ArrayList<PartialCalculationResult>> calculationResultPointsCallBack =
      new AsyncCallback<ArrayList<PartialCalculationResult>>() {
    @Override
    public void onSuccess(final ArrayList<PartialCalculationResult> results) {
      if (!cancelled) {
        if (!isCalculationActual()) {
          // scenario has changed, cancel calculation
          eventBus.fireEvent(new CalculationCancelEvent());
        } else if (results != null && !results.isEmpty()) {
          for (final PartialCalculationResult result : results) {
            // check if the calculationID in result is available in the calculatedscenario of lastCalculation.
            // Situation where not checking could be dangerous:
            // start calculation, abort after a while and immediately restart.
            // a result could still be on the way back for the old calculation and it would be handled for the new one...
            if (lastScenario.containsCalculationId(result.getCalculationId())) {
              // Store point results in the scenario.
              ScenarioUtil.setCalculatedCalculationPoints(lastScenario, result.getCalculationId(), result.getResults());
              eventBus.fireEvent(new ReceptorChangeEvent(result));
            }
          }
        }
        finished = true;
        finish();
      }
    }

    @Override
    public void onFailure(final Throwable caught) {
      cancel();
      onServiceFailure(caught);
    }
  };

  private final AsyncCallback<CalculationInitResult> calculationInitCallBack = new AsyncCallback<CalculationInitResult>() {
    @Override
    public void onSuccess(final CalculationInitResult result) {
      if (result == null || !result.getCalculationKey().equals(lastCalculationKey)) {
        // Ignore results for result of old calculations and cancelled results.
        return;
      }

      initAgent.stop();
      if (!cancelled) {
        lastCalculation = result;

        lastScenario.getCalculations().clear();
        lastScenario.getCalculations().addAll(result.getCalculations());
        lastScenario.setOptions(result.getCalculationSetOptions());

        eventBus.fireEvent(new CalculationInitEvent(result));
        resultAgent.start(lastCalculationKey, resultsReadyCallBack);
        progressAgent.start(lastCalculationKey, progressCallback);
      }
    }

    @Override
    public void onFailure(final Throwable caught) {
      cancel();
      onServiceFailure(caught);
    }
  };

  /**
   * Marker used to determine if a calculation has been cancelled.
   */
  private boolean cancelled;
  // Marker used to determine if a calculation is finished.
  private boolean finished;

  /**
   * CalculationAgent used to track the CalculationStatus
   */
  private final CalculationInitPollingAgent initAgent;

  /**
   * CalculationAgent used to track the CalculationStatus
   */
  private final CalculationPollingAgent resultAgent;

  /**
   * {@link CalculationHighByDistancePollingAgent} used to track progress.
   */
  private final CalculationHighByDistancePollingAgent progressAgent;

  /**
   * EventBus used to broadcast CalculationResult's over
   */
  private final EventBus eventBus;

  private String lastCalculationKey;

  private CalculationInitResult lastCalculation;

  protected CalculatedScenario lastScenario;

  private final Context context;

  /**
   * Initializes the CalculationProcessor with the given service and callback.
   *
   * @param contextService context service
   * @param service service used to calculate
   * @param resultAgent agent used to get results
   * @param initAgent agent used to get init result
   * @param eventBus eventBus to register for cancel events
   */
  public ScenarioBaseCalculationProcessor(final CalculateServiceAsync service, final CalculationPollingAgent resultAgent,
      final CalculationInitPollingAgent initAgent, final CalculationHighByDistancePollingAgent progressAgent, final Context context,
      final EventBus eventBus) {
    this.service = service;
    this.resultAgent = resultAgent;
    this.initAgent = initAgent;
    this.progressAgent = progressAgent;
    this.context = context;
    this.eventBus = eventBus;

    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onCancelCalculation(final CalculationCancelEvent event) {
    cancel();

    if (event.isUserInitiated()) {
      NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationCancelled());
    }
  }

  @EventHandler
  public void onSourceChangeEvent(final EmissionSourceChangeEvent event) {
    if (lastScenario != null && !lastScenario.isInSync()) {
      NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationInvalidated());
      lastScenario = null;
    }
  }

  /**
   * Initializes the processor by passing the EventBus that CalculationResults will be broadcasted over. Start calculation for the given options and
   * sources.
   *
   * @param scenario Scenario to calculate
   */
  public void start(final CalculatedScenario scenario) {
    if (!cancelled && !finished) {
      cancel();
    }
    cancelled = false;
    finished = false;

    doStartCalculationServiceCall(scenario, lastCalculationKey, new AsyncCallback<String>() {
      @Override
      public void onSuccess(final String result) {
        lastCalculationKey = result;
        lastScenario = scenario;
        eventBus.fireEvent(new CalculationStartEvent());
        initAgent.start(lastCalculationKey, calculationInitCallBack);
        registerProgressTimer();
      }

      @Override
      public void onFailure(final Throwable caught) {
        onServiceFailure(caught);
      }
    });
  }

  protected abstract boolean isCalculationActual();

  /**
   * Initialize the calculation at the service and retrieve a random UID
   *
   * @param key Optional previous calculation.
   * @param callback
   */
  protected abstract void doStartCalculationServiceCall(final CalculatedScenario scenario, final String key, final AsyncCallback<String> callback);

  private void registerProgressTimer() {
    resultsDelayedNotificationTimer.schedule((int) context.getSetting(SharedConstantsEnum.CALCULATION_DELAY_NOTIFICATION_TIME) * SECONDS_TO_MILLIS);
  }

  private void onResultsReceived() {
    resultsDelayedNotificationTimer.cancel();
  }

  private void notifyDelayedProgress() {
    if (cancelled || finished) {
      return;
    }

    NotificationUtil.broadcastMessage(eventBus, M.messages().calculationDelayedNotification());
  }

  /**
   * Cancel the calculation process and clear all results still waiting on the queue.
   */
  public void cancel() {
    if (!finished) {
      cancelled = true;
      initAgent.stop();
      resultAgent.stop();
      progressAgent.stop();
      if (lastCalculation != null) {
        service.cancelCalculation(lastCalculationKey, new AppAsyncCallback<Void>() {
          @Override
          public void onSuccess(final Void result) {
            // Do nothing
          }
        });
      }
      resultsDelayedNotificationTimer.cancel();
    }
  }

  /**
   * Finish the calculation process and clear all results still waiting on the queue.
   */
  public void finish() {
    if (finished) {
      eventBus.fireEvent(new CalculationFinishEvent(false));
    }
  }

  private void onServiceFailure(final Throwable caught) {
    eventBus.fireEvent(new CalculationCancelEvent());
    if (caught instanceof AeriusException) {
      throw new RuntimeException(caught);
    } else {
      NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationError());
    }
  }
}
