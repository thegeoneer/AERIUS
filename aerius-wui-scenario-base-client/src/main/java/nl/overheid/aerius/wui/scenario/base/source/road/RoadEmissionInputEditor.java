/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.road;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.VehicleCustomEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleSpecificEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.ListCancelButtonEvent;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.road.VehicleCustomEditor.VehicleCustomDriver;
import nl.overheid.aerius.wui.scenario.base.source.road.VehicleSpecificEditor.VehicleSpecificDriver;
import nl.overheid.aerius.wui.scenario.base.source.road.VehicleStandardEditor.VehicleStandardDriver;

class RoadEmissionInputEditor extends Composite implements ValueAwareEditor<VehicleEmissions>,
HasClickHandlers, InputEditor<VehicleEmissions> {

  interface RoadEmissionInputDriver extends SimpleBeanEditorDriver<VehicleEmissions, RoadEmissionInputEditor> {
  }

  interface RoadEmissionInputEditorUiBinder extends UiBinder<Widget, RoadEmissionInputEditor> {
  }

  private static final RoadEmissionInputEditorUiBinder UI_BINDER = GWT.create(RoadEmissionInputEditorUiBinder.class);

  private final VehicleStandardEditor vStandardEditor;
  private final VehicleStandardDriver vStandardDriver = GWT.create(VehicleStandardDriver.class);
  private final VehicleSpecificEditor vSpecificEditor;
  private final VehicleSpecificDriver vSpecificDriver = GWT.create(VehicleSpecificDriver.class);
  private final VehicleCustomEditor vCustomEditor;
  private final VehicleCustomDriver vCustomDriver = GWT.create(VehicleCustomDriver.class);

  @SuppressWarnings("rawtypes")
  private SimpleBeanEditorDriver driver;
  @SuppressWarnings("rawtypes")
  private InputEditor editor;
  @Ignore @UiField(provided = true) RadioButtonGroup<Integer> radioButtonGroup;
  @UiField SimplePanel panel;
  @UiField Button cancelButton;
  @UiField Button submitButton;

  private RoadType roadType;
  private EditorDelegate<VehicleEmissions> delegate;

  private VehicleEmissions restoreEmissions;

  private final EventBus eventBus;

  /**
   * @param radioButtonGroup
   *
   */
  public RoadEmissionInputEditor(final EventBus eventBus, final SectorCategories categories, final HelpPopupController hpC,
      final RadioButtonGroup<Integer> radioButtonGroup) {
    this.eventBus = eventBus;
    this.radioButtonGroup = radioButtonGroup;
    initWidget(UI_BINDER.createAndBindUi(this));

    vStandardEditor = new VehicleStandardEditor(categories.getRoadEmissionCategories(), hpC);
    vStandardDriver.initialize(vStandardEditor);
    vSpecificEditor = new VehicleSpecificEditor(categories.getOnRoadMobileSourceCategories(), hpC);
    vSpecificDriver.initialize(vSpecificEditor);
    vCustomEditor = new VehicleCustomEditor(hpC);
    vCustomDriver.initialize(vCustomEditor);
    hpC.addWidget(radioButtonGroup, hpC.tt().ttRoadType());

  }

  @Override
  public void postSetValue(final EmissionSource source) {
    roadType = RoadType.valueFromSectorId(source.getSector().getSectorId());
  }

  @Override
  public void resetPlaceholders() {
    if (editor != null) {
      editor.resetPlaceholders();
    }
  }

  @UiHandler("cancelButton")
  void cancelButtonHandler(final ClickEvent e) {
    if (restoreEmissions != null) {
      setValue(restoreEmissions.copy());
      eventBus.fireEvent(new ListCancelButtonEvent());
    }
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return submitButton.addClickHandler(handler);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void setValue(final VehicleEmissions value) {
    if (value instanceof VehicleStandardEmissions) {
      driver = vStandardDriver;
      editor = vStandardEditor;
      vStandardEditor.setRoadType(roadType);
      vStandardEditor.setCategory(((VehicleStandardEmissions) value).getEmissionCategory());
    } else if (value instanceof VehicleSpecificEmissions) {
      driver = vSpecificDriver;
      editor = vSpecificEditor;
    } else if (value instanceof VehicleCustomEmissions) {
      driver = vCustomDriver;
      editor = vCustomEditor;
    } else if (value == null) {
      setValue(new VehicleStandardEmissions());
      return;
    } // else you're out of luck...
    if (driver != null) {
      driver.edit(value);
      resetPlaceholders();
      // Reset errors styles on widgets
      ErrorPopupController.clearWidgets();

    }
    panel.setWidget(editor);
    restoreEmissions = value.copy();
  }

  @Override
  public void setDelegate(final EditorDelegate<VehicleEmissions> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void flush() {
    if (driver != null) {
      driver.flush();
      if (driver.hasErrors()) {
        for (final Object obj : driver.getErrors()) {
          final EditorError error = (EditorError) obj;
          delegate.recordError(error.getMessage(), error.getValue(), error.getUserData());
          error.setConsumed(true);
        }
      }
    }
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    submitButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_SUBMIT);
    cancelButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_CANCEL);
    vStandardEditor.ensureDebugId(baseID);
    vSpecificEditor.ensureDebugId(baseID);
    vCustomEditor.ensureDebugId(baseID);
  }
}
