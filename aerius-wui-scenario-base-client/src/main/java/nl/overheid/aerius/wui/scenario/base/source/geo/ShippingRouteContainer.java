/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.geo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.layer.Markers;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;

import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.util.FormatUtil;
import nl.overheid.aerius.wui.geo.DrawSourceStyleOptions;
import nl.overheid.aerius.wui.geo.MarkerLayerWrapper.MarkerContainer;
import nl.overheid.aerius.wui.geo.SingleMarker;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Container class to keep track of all shipping route features.
 * The container uses a routes map that uses the objects hash as key.
 */
public class ShippingRouteContainer implements MarkerContainer<EmissionSource> {

  private final Map<Integer, ShippingRouteFeature> routes = new HashMap<>();

  private Vector vectorLayer;
  private Markers markersLayer;

  //This is specific for the global maritime routes. It should probably not be here
  private List<ShippingRoute> maritimeRoutes;

  /**
   * Draws new routes based on the supplied list of sources.
   * @param source The sources to determine routes from.
   */
  @Override
  public void add(final EmissionSource source) {
    if (source instanceof MaritimeMooringEmissionSource) {
      final MaritimeMooringEmissionSource maritimeEmissionValues = (MaritimeMooringEmissionSource) source;
      // Add inland routes
      for (final ShippingRoute route : maritimeEmissionValues.getInlandRoutes()) {
        addRoute(route);
      }
      for (final MooringMaritimeVesselGroup ships : maritimeEmissionValues.getEmissionSubSources()) {
        for (final MaritimeRoute mr : ships.getMaritimeRoutes()) {
          addRoute(mr.getRoute());
        }
      }
    } else if (source instanceof InlandMooringEmissionSource) {
      for (final ShippingRoute route : ((InlandMooringEmissionSource) source).getInlandRoutes()) {
        addRoute(route);
      }
    } // else: nothing to add because source type is not maritime mooring
  }

  /**
   * Adds a route and calls redraw to show the new route on the map.
   * @param route to add
   */
  public void add(final ShippingRoute route) {
    //addRoute should be called before layer check because of side-effects.
    if (addRoute(route)) {
      redrawLayer();
    }
  }

  private boolean addRoute(final ShippingRoute route) {
    if (routes.containsKey(getRouteKey(route))) {
      return false;
    }
    final VectorFeature feature = new VectorFeature(Geometry.fromWKT(route.getGeometry().getWKT()));
    feature.setStyle(DrawSourceStyleOptions.getElasticStyle());
    if (vectorLayer != null) {
      vectorLayer.addFeature(feature);
    }
    final TransferRouteMarker markerItem = new TransferRouteMarker(route);
    if (markersLayer != null) {
      markersLayer.addMarker(markerItem.getMarker());
    }
    final ShippingRouteFeature srf = new ShippingRouteFeature(route, feature, markerItem);
    routes.put(getRouteKey(route), srf);
    return true;
  }

  /**
   * Removes all current routes and markers associated with this container from the map.
   */
  @Override
  public void clear() {
    for (final Entry<Integer, ShippingRouteFeature> entry : new ArrayList<>(routes.entrySet())) {
      removeRoute(entry.getValue());
    }
    routes.clear();
    redrawLayer();
  }

  /**
   * Initializes the class and binds the container to an actual layer and list of markers on the layer.
   * @param layer layer to display routes on
   * @param markersLayer layer to display route markers
   */
  @Override
  public void init(final Vector layer, final Markers markersLayer) {
    this.vectorLayer = layer;
    this.markersLayer = markersLayer;
  }

  /**
   * Removes the emission source related routes from the map if it's a {@link MaritimeMooringEmissionSource} source.
   * @param source source to remove routes
   */
  @Override
  public void remove(final EmissionSource source) {
    if (source instanceof MaritimeMooringEmissionSource) {
      removeUnusedShippingRoutes();
    } else if (source instanceof InlandMooringEmissionSource) {
      removeSourceRoutes((InlandMooringEmissionSource) source);
    }
  }

  /**
   * Determines which ShippingRoutes in the container are unused and removes them.
   */
  private void removeUnusedShippingRoutes() {
    final List<ShippingRoute> shippingRoutesToRemoveList = new ArrayList<ShippingRoute>();
    for (final Entry<Integer, ShippingRouteFeature> route : routes.entrySet()) {
      final boolean notUsed = !route.getValue().getShippingRoute().isUsed();
      if (notUsed) {
        shippingRoutesToRemoveList.add(route.getValue().getShippingRoute());
      }
    }
      removeRoutesAndRedrawWhenNeeded(shippingRoutesToRemoveList);
  }

  /**
   * Tries to remove the ShippingRoutes contained in the source from this container.
   * 
   * @param source
   */
  private void removeSourceRoutes(final InlandMooringEmissionSource source) {
    removeRoutesAndRedrawWhenNeeded(source.getInlandRoutes());
  }

  /**
   * Removes the route when present in this container and redraws the map when it was.
   *
   * @param route route to remove
   * @return returns true if route was present and removed
   */
  public boolean removeRouteAndRedrawWhenNeeded(final ShippingRoute route) {
    final boolean remove = removeRoute(route);
    if (remove) {
      redrawLayer();
    }
    return remove;
  }

  /**
   * Removes the routes when present in this container and redraws the map when any have been removed.
   *
   * @param routeSet route to remove
   * @return returns true if route was present and removed
   */
  private void removeRoutesAndRedrawWhenNeeded(final List<ShippingRoute> shippingRoutesToRemoveList) {
    boolean removed = false;
    for (final ShippingRoute sr : shippingRoutesToRemoveList) {
      removed |= removeRoute(sr);
    }
    if (removed) {
      redrawLayer();
    }
  }

  private boolean removeRoute(final ShippingRouteFeature srf) {
    return removeRoute(srf.getShippingRoute());
  }

  /**
   * Removes the vector and markers for the route from the map.
   * @param route route to remove
   */
  private boolean removeRoute(final ShippingRoute route) {
    if (routes.containsKey(getRouteKey(route))) {
      final ShippingRouteFeature entry = routes.remove(getRouteKey(route));
      removeFromLayer(entry.getVector());
      removeFromMarkerLayer(entry.getMarker());
      return true;
    }
    return false;
  }

  private void removeFromLayer(final VectorFeature feature) {
    if ((vectorLayer != null) && (feature != null)) {
      vectorLayer.removeFeature(feature);
    }
  }

  private void removeFromMarkerLayer(final TransferRouteMarker rMarker) {
    if ((markersLayer != null) && (rMarker != null)) {
      markersLayer.removeMarker(rMarker.getMarker());
    }
  }

  private void redrawLayer() {
    if (vectorLayer != null) {
      vectorLayer.redraw();
    }
  }

  /**
   * Returns the current active maritime sea routes. This list depends on the currently visible sources list, so callers should probably not store
   * this object.
   *
   * @return list of sea routes
   */
  public List<ShippingRoute> getMaritimeRoutes() {
    return maritimeRoutes;
  }

  @Override
  public void setMaritimeRoutes(final List<ShippingRoute> maritimeRoutes) {
    this.maritimeRoutes = maritimeRoutes;
  }

  private int getRouteKey(final ShippingRoute route) {
    return System.identityHashCode(route);
  }

  /**
   * Class extending MarkerItem adding route specific information.
   */
  static class TransferRouteMarker extends SingleMarker<ShippingRoute> {

    private static final long serialVersionUID = 4415391355204971299L;

    private final DivElement label = Document.get().createDivElement();

    TransferRouteMarker(final ShippingRoute route) {
      super(route, route, TYPE.SHIPPING_ROUTE);
      label.setInnerText(FormatUtil.formatAlphabeticalExactUpperCase(route.getId()));
      getElement().addClassName(R.css().textOverflowEllipsis());
      getElement().addClassName(R.css().shipRouteMarker());
      getElement().appendChild(label);
      // Point is top/left, but marker should show in the middle, therefore correcting the offset to the middle of the marker
      getElement().getStyle().setPosition(Position.ABSOLUTE);
      getElement().getStyle().setTop(-R.css().shippingRouteMarkerSize() / 2.0, Unit.PX);
      getElement().getStyle().setLeft(-R.css().shippingRouteMarkerSize() / 2.0, Unit.PX);
    }
  }

  /**
   * Data class containing both route and vector data of a single shipping route.
   */
  private static class ShippingRouteFeature {
    private final ShippingRoute shippingRoute;
    private final VectorFeature vector;
    private final TransferRouteMarker marker;

    public ShippingRouteFeature(final ShippingRoute shippingRoute, final VectorFeature vector, final TransferRouteMarker marker) {
      this.shippingRoute = shippingRoute;
      this.vector = vector;
      this.marker = marker;
    }

    public TransferRouteMarker getMarker() {
      return marker;
    }

    public ShippingRoute getShippingRoute() {
      return shippingRoute;
    }

    public VectorFeature getVector() {
      return vector;
    }
  }
}
