/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SuggestBox;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;

/**
 * Widget for inputting single ship data.
 */
class MaritimeRouteInputEditor extends Composite
implements Editor<RouteMaritimeVesselGroup>, HasClickHandlers, InputEditor<RouteMaritimeVesselGroup> {

  interface MaritimeRouteInputDriver extends SimpleBeanEditorDriver<RouteMaritimeVesselGroup, MaritimeRouteInputEditor> { }

  interface MaritimeRouteInputEditorUiBinder extends UiBinder<Widget, MaritimeRouteInputEditor> { }

  private static final MaritimeRouteInputEditorUiBinder UI_BINDER = GWT.create(MaritimeRouteInputEditorUiBinder.class);

  private static final TextMachine<MaritimeShippingCategory> TEXT_MACHINE = new ShippingCategoryTextMachine<>();

  @UiField @Ignore TextBox name;
  @UiField(provided = true) SuggestBox<MaritimeShippingCategory> category;
  @UiField IntValueBox shipMovementsPerTimeUnit;
  @UiField (provided = true) ListBoxEditor<TimeUnit> timeUnit;
  @UiField @Ignore Button submitButton;
  ValidatedValueBoxEditor<String> nameEditor;

  public MaritimeRouteInputEditor(final SectorCategories categories, final HelpPopupController hpC) {
    final ShipCategorySuggestionOracle<MaritimeShippingCategory> oracle =
        new ShipCategorySuggestionOracle<>(categories.getMaritimeShippingCategories());
    category = new SuggestBox<>(oracle, TEXT_MACHINE);
    timeUnit = new ListBoxEditor<TimeUnit>() {
      @Override
      protected String getLabel(final TimeUnit value) {
        return M.messages().timeUnit(value);
      }
    };
    timeUnit.addItems(TimeUnit.values());

    initWidget(UI_BINDER.createAndBindUi(this));

    nameEditor = new ShippingValidatedValueBoxEditor(name);

    // Help widgets
    hpC.addWidget(name, hpC.tt().ttMaritimeRouteShipName());
    hpC.addWidget(category, hpC.tt().ttMaritimeRouteShipType());
    hpC.addWidget(shipMovementsPerTimeUnit, hpC.tt().ttMaritimeRouteShipTimeUnit());
    hpC.addWidget(timeUnit, hpC.tt().ttShippingTimeUnit());
    hpC.addWidget(submitButton, hpC.tt().ttMaritimeRouteShipSave());

    name.ensureDebugId(TestID.INPUT_SHIPPING_NAME);
    category.getValueBox().ensureDebugId(TestID.INPUT_SHIPPING_CATEGORY);
    shipMovementsPerTimeUnit.ensureDebugId(TestID.INPUT_SHIPPING_AMOUNT);
    timeUnit.ensureDebugId(TestID.LIST_TIMEUNIT);
    submitButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_SUBMIT);
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return submitButton.addClickHandler(handler);
  }

  /**
   * @param source
   */
  @Override
  public void postSetValue(final EmissionSource source) {
    //no-op
  }

  /**
   * Force empty content on the IntValueBox widgets when a new data object is edited.
   * Otherwise the initial value will be 0 and the placeholder text won't be visible.
   */
  @Override
  public void resetPlaceholders() {
    shipMovementsPerTimeUnit.resetPlaceHolder();
  }
}
