/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.sector.category.HasNameDescription;
import nl.overheid.aerius.wui.main.widget.ParameterizedSuggestOracle;
import nl.overheid.aerius.wui.main.widget.ParameterizedSuggestion;

/**
 * MultiWordSuggestOracle to display Category items.
 * @param <E>
 */
class FarmSuggestOracle<E extends HasNameDescription> extends ParameterizedSuggestOracle<E> {
  private static final int MIN_DESCRIPTION_LENGTH = 1;
  private static final String SPACE = " ";
  private static final String EMPTY_SPACE = "";

  /**
   * Inner class defining how a suggestion for the given category should be displayed.
   */
  public static class FarmParameterizedSuggestion<E extends HasNameDescription> extends ParameterizedSuggestion<E> {


    /**
     * @param category The category to display as suggestion.
     */
    public FarmParameterizedSuggestion(final E category) {
      super(category);
    }

    @Override
    public String getDisplayString() {
      return object.getName() + SPACE + object.getDescription();
    }

    @Override
    public String getReplacementString() {
      return object.getName();
    }
  }

  /**
   * @param categories The list of categories to choose from.
   */
  public FarmSuggestOracle(final ArrayList<E> categories) {
    super(categories);
    customSort();
  }

  @Override
  protected Suggestion getSuggestion(final String query, final E cat) {
    if (query.isEmpty()) {
      return null;
    }

    final String transformedQuery = query.replace(SPACE, EMPTY_SPACE);
    final String name = cat.getName().toLowerCase().replace(SPACE, EMPTY_SPACE);
    final String description = cat.getDescription() == null ? null
        : cat.getDescription().toLowerCase().replace(SPACE, EMPTY_SPACE);

    if (name.contains(transformedQuery)
        || (transformedQuery.length() >= MIN_DESCRIPTION_LENGTH && description != null && description.contains(transformedQuery))) {
      return getSuggestion(cat);
    }

    return null;
  }

  public String getStyleName(final E catergory) {
    return null;
  }

  public void customSort() {
  }

  @Override
  protected Suggestion getSuggestion(final E cat) {
    return new FarmParameterizedSuggestion<E>(cat) {
      @Override
      public String getStyleName() {
        return FarmSuggestOracle.this.getStyleName(cat);
      }
      @Override
      public void enableSort() {
        FarmSuggestOracle.this.customSort();
      }
    };
  }
}
