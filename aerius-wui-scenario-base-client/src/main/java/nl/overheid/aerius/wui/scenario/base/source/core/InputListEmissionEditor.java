/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.HasName;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceCollection;
import nl.overheid.aerius.shared.domain.source.EmissionSubSource;
import nl.overheid.aerius.shared.domain.source.EmissionSubSourceList;
import nl.overheid.aerius.shared.domain.source.HasEmissionValues;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.InputListEmissionItemEditEvent;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.event.AddEvent;
import nl.overheid.aerius.wui.main.widget.event.CopyEvent;
import nl.overheid.aerius.wui.main.widget.event.DeleteEvent;
import nl.overheid.aerius.wui.main.widget.event.EditEvent;
import nl.overheid.aerius.wui.main.widget.table.DataProvider;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable.EditableDivTableMessages;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.ui.source.ConciseEmissionSubSourceWidget;
import nl.overheid.aerius.wui.scenario.base.ui.source.EmissionSubSourceViewer;

/**
 * Editor for showing a manageable list of items, and edit fields for a single items.
 * @param <U>
 * @param <I>
 */
public abstract class InputListEmissionEditor<U extends EmissionSubSource, I extends InputEditor<U>>
    extends Composite implements LeafValueEditor<EmissionSubSourceList<U>>, ClickHandler {

  @SuppressWarnings("rawtypes")
  interface InputListEmissionEditorEventBinder extends EventBinder<InputListEmissionEditor> {}

  public interface InputEditor<U> extends Editor<U>, HasClickHandlers, IsWidget {
    /**
     * Called after the EmissionSource edit set is done. Don't use it to initialize data specific for an input editor, but only for data that is
     * independent of the input editor.
     * @param source emission source
     */
    void postSetValue(final EmissionSource source);

    /**
     * Programmatically empty input fields to make the placeholder text visible.
     */
    void resetPlaceholders();
  }

  private final InputListEmissionEditorEventBinder eventBinder = GWT.create(InputListEmissionEditorEventBinder.class);
  private final DataProvider<ArrayList<U>, U> dataProvider = new DataProvider<>();
  private final SimpleBeanEditorDriver<U, InputEditor<U>> driver;
  private final InputListEditorDivTable<U> sourcesDivTable;
  private final EventBus globalEventBus;
  private final com.google.gwt.event.shared.EventBus localEventBus = new SimpleEventBus();
  private final SimplePanel contentPanel = new SimplePanel();
  private final EditableDivTable<U> editableDivTable;
  private final CalculatorServiceAsync service;
  @Ignore private final I inputEditor;

  private EmissionSourceCollection<U> sourceCollection;
  private EmissionValueKey evk;

  public InputListEmissionEditor(final EventBus globalEventBus, final I inputEditor, final HelpPopupController hpC,
      final EditableDivTableMessages messages, final Class<U> emissionValuesClass, final String name,
      final SimpleBeanEditorDriver<U, InputEditor<U>> driver, final CalculatorServiceAsync service, final Boolean enableImage) {
    this.inputEditor = inputEditor;
    this.driver = driver;
    this.globalEventBus = globalEventBus;
    this.service = service;

    inputEditor.asWidget().addStyleName(R.css().flex());
    inputEditor.asWidget().addStyleName(R.css().columns());
    inputEditor.asWidget().addStyleName(R.css().grow());

    final ConciseEmissionSubSourceWidget<U> subSourcePopUp = new ConciseEmissionSubSourceWidget<U>(new EmissionSubSourceViewer()) {
      @Override
      public String getLabel(final U obj) {
        return InputListEmissionEditor.this.getLabel(obj);
      }
    };

    sourcesDivTable = new InputListEditorDivTable<U>(hpC, enableImage, subSourcePopUp) {
      @Override
      protected ImageResource getImage(final U object) {
        return InputListEmissionEditor.this.getImage(object);
      }

      @Override
      protected String getLabel(final U object) {
        return InputListEmissionEditor.this.getLabel(object);
      }

      @Override
      protected double getEmission(final U value, final EmissionValueKey key) {
        return sourceCollection == null ? 0 : sourceCollection.getEmission(value, key);
      }
    };
    dataProvider.addDataDisplay(sourcesDivTable);
    sourcesDivTable.asDataTable().addValueChangeHandler(new ValueChangeHandler<Integer>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Integer> event) {
        // if no entries always show the edit.
        if (event.getValue() != null && event.getValue() != 0) {
          onAdd(null);
        }
      }
    });

    editableDivTable = new EditableDivTable<U>(sourcesDivTable.asDataTable(), hpC, messages) {
      @Override
      public Class<U> getObjectClass() {
        return emissionValuesClass;
      }

      @Override
      public void handleCustomButton() {
        // No-op
      }
    };

    inputEditor.addClickHandler(this);
    eventBinder.bindEventHandlers(this, localEventBus);
    editableDivTable.setEventBus(localEventBus);
    initWidget(contentPanel);
    addStyleName(StyleUtil.joinStyles(R.css().flex(), R.css().grow(), R.css().inputCollapsibleContent()));
    driver.initialize(inputEditor);
  }

  @Override
  public EmissionSubSourceList<U> getValue() {
    final EmissionSubSourceList<U> result = new EmissionSubSourceList<>();
    result.addAll(dataProvider.getValue());
    return result;
  }

  @Override
  public void setValue(final EmissionSubSourceList<U> value) {
    setIds(value);
    dataProvider.setValue(value);
    dataProvider.refresh();
  }

  @SuppressWarnings("unchecked")
  public void postSetValue(final EmissionSource source) {
    if (source instanceof EmissionSourceCollection) {
      this.sourceCollection = (EmissionSourceCollection<U>) source;
    }
    inputEditor.postSetValue(source);
    refresh();
  }

  private void refresh() {
    if (dataProvider.getValue().isEmpty()) {
      onAdd(null);
    } else {
      showEditor(false);
    }
    dataProvider.refresh();
  }

  @Ignore
  public I getInputEditor() {
    return inputEditor;
  }

  protected abstract U createNewRowEmissionValues();

  protected String getLabel(final U object) {
    return object instanceof HasName ? ((HasName) object).getName() : object.toString();
  }

  protected ImageResource getImage(final U object) {
    return (ImageResource) object;
  }

  /**
   * Called when new object should be created.
   * @param event add event with object
   */
  @EventHandler
  public void onAdd(final AddEvent<U> event) {
    addNew(createNewRowEmissionValues());
  }

  protected void addNew(final U value) {
    value.setId(-1);
    edit(value);
  }

  public void onCancel() {
    showEditor(false);
  }

  /**
   * Called when the passed object should be copied and the new copy should be edited.
   * @param event copy event with object of copy
   */
  @EventHandler
  public void onCopy(final CopyEvent<U> event) {
    @SuppressWarnings("unchecked")
    final U copy = (U) event.getValue().copy();
    addNew(copy);
  }

  /**
   * Called when the passed object should be deleted.
   * @param event delete event with the object to delete
   */
  @EventHandler
  public void onDelete(final DeleteEvent<U> event) {
    dataProvider.getValue().remove(event.getValue());
    setIds(dataProvider.getValue());
    refresh();
  }

  /**
   * Called when the passed object should be edited.
   * @param event edit event with the object to edit
   */
  @EventHandler
  public void onEdit(final EditEvent<U> event) {
    // Create and edit a copy so values are not flushed into the original while the editor still has errors.
    @SuppressWarnings("unchecked")
    final U copy = (U) event.getValue().copy();

    edit(copy);
  }

  /**
   * Called when to edit.
   * @param value to pass to the driver edit
   */
  public void edit(final U value) {
    driver.edit(value);
    inputEditor.resetPlaceholders();
    ErrorPopupController.clearWidgets();
    showEditor(true);
  }

  @Override
  public void onClick(final ClickEvent event) {
    final U value = driver.flush();

    if (driver.hasErrors()) {
      ErrorPopupController.addErrors(driver.getErrors());
    } else {
      updateEmission(value, new AppAsyncCallback<U>() {
        @Override
        public void onSuccess(final U result) {
          processValue(value);
        }
      });
    }
  }

  private void updateEmission(final U value, final AsyncCallback<U> callback) {
    if (value instanceof HasEmissionValues) {
      final ArrayList<EmissionValueKey> keys = EmissionValueKey.getEmissionValueKeys(evk.getYear(), Substance.EMISSION_SUBSTANCES);
      service.getEmissions((HasEmissionValues) value, keys, sourceCollection, new AppAsyncCallback<EmissionValues>() {

        @Override
        public void onSuccess(final EmissionValues result) {
          ((HasEmissionValues) value).setEmissionValues(result);
          callback.onSuccess(value);
        }
      });
    } else {
      callback.onSuccess(value);
    }
  }

  private void processValue(final U value) {
    showEditor(false);
    boolean contains = false;
    for (int i = 0; i < dataProvider.getValue().size(); i++) {
      final U oId = dataProvider.getValue().get(i);
      if (oId.getId() == value.getId()) {
        dataProvider.getValue().set(i, value);
        contains = true;
        break;
      }
    }
    if (!contains) {
      value.setId(dataProvider.getValue().size());
      dataProvider.getValue().add(value);
    }
    dataProvider.refresh();
  }

  /**
   * Called to set {@link EmissionValueKey} to sourcesCellTable.
   * @param key to parse
   */
  public void setEmissionValueKey(final EmissionValueKey key) {
    final boolean yearChanged = this.evk != null && key != null && evk.getYear() != key.getYear();
    this.evk = key == null ? null : key.copy();
    updateEmissions(0, yearChanged);
  }

  /**
   * Forces the emissions to be recalculated. This will call the server for each source so only needed when the emissions are really changed.
   */
  public void forceRefresh() {
    updateEmissions(0, true);
  }

  /**
   * Recursively update emissions for each source. But only update if the year was changed and only when actual data present.
   *
   * @param i index in list of sources. Updated in each recursive call
   * @param yearChanged
   */
  private void updateEmissions(final int i, final boolean yearChanged) {
    if (yearChanged && dataProvider.getValue() != null && i < dataProvider.getValue().size()) {
      updateEmission(dataProvider.getValue().get(i), new AppAsyncCallback<U>() {
        @Override
        public void onSuccess(final U result) {
          updateEmissions(i + 1, yearChanged);
        }
      });
    } else {
      sourcesDivTable.setEmissionValueKey(evk);
      dataProvider.refresh();
    }
  }

  public DataProvider<ArrayList<U>, U> getListDataProvider() {
    return dataProvider;
  }

  protected void showEditor(final boolean visible) {
    contentPanel.setWidget(visible ? inputEditor : editableDivTable);
    globalEventBus.fireEvent(new InputListEmissionItemEditEvent(visible));
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    editableDivTable.ensureDebugId(TestID.TABLE_GENERIC_DIVTABLE_OVERVIEW);
    inputEditor.asWidget().ensureDebugId(baseID);
  }

  private void setIds(final ArrayList<U> value) {
    for (int i = 0; i < value.size(); i++) {
      value.get(i).setId(i);
    }
  }

}
