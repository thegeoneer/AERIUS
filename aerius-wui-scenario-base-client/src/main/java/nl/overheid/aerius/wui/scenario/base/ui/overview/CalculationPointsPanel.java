/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.MarkerItem;
import nl.overheid.aerius.wui.geo.MarkerLayerWrapper.LabelVisibilityChangeEvent;
import nl.overheid.aerius.wui.main.event.CalculationPointsPurgeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable.EditableDivTableMessages;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;

/**
 * Panel to display the calculation points list in a table wrapped by editing buttons.
 */
public class CalculationPointsPanel extends Composite {

  interface CalculationPointsPanelUiBinder extends UiBinder<Widget, CalculationPointsPanel> { }

  private static CalculationPointsPanelUiBinder uiBinder = GWT.create(CalculationPointsPanelUiBinder.class);

  @UiField(provided = true) EditableDivTable<AeriusPoint> editableDivTable;
  private final CalculationPointDivTable calculationPointDivTable;

  @UiField CheckBox toggleButton;
  @UiField Button purgeButton;
  private final SpanElement toggleText = Document.get().createSpanElement();
  private final EventBus eventBus;

  @Inject
  public CalculationPointsPanel(final HelpPopupController hpC, final EventBus eventBus, final UserContext userContext,
      final ScenarioBaseImportController importController) {
    this.eventBus = eventBus;
    calculationPointDivTable = new CalculationPointDivTable(eventBus, userContext, true);
    editableDivTable = new EditableDivTable<AeriusPoint>(calculationPointDivTable.asDataTable(), hpC, new EditableDivTableMessages() {
      @Override
      public String add() {
        return M.messages().calculatorAddDeposition();
      }

      @Override
      public HelpInfo editHelp() {
        return hpC.tt().ttOverviewCalculationPointsButtonEdit();
      }

      @Override
      public HelpInfo copyHelp() {
        return hpC.tt().ttOverviewCalculationPointsButtonCopy();
      }

      @Override
      public HelpInfo deleteHelp() {
        return hpC.tt().ttOverviewCalculationPointsButtonDelete();
      }

      @Override
      public HelpInfo addHelp() {
        return hpC.tt().ttOverviewButtonAddCalculationPoint();
      }

      @Override
      public String customButtonText() {
        return M.messages().calculatorImportDeposition();
      }

      @Override
      public HelpInfo customButtonHelp() {
        return hpC.tt().ttOverviewButtonImportCalculationPoint();
      }
    }) {

      @Override
      public void handleCustomButton() {
        importController.showImportDialog();
      }

      @Override
      public Class<AeriusPoint> getObjectClass() {
        return AeriusPoint.class;
      }
    };
    initWidget(uiBinder.createAndBindUi(this));
    editableDivTable.setEventBus(eventBus);

    editableDivTable.ensureDebugId(TestID.DIVTABLE_CALCULATION_POINT);

    toggleButton.getElement().appendChild(toggleText);
    toggleText.setInnerHTML(M.messages().labelOff());

    purgeButton.ensureDebugId(TestID.BUTTON_PURGE_CALCPOINTS);
    toggleButton.ensureDebugId(TestID.TOGGLE_CALCPOINT_LABELS);
  }

  @UiHandler("purgeButton")
  void onPurgeButtonClick(final ClickEvent e) {
    eventBus.fireEvent(new CalculationPointsPurgeEvent());
  }

  @UiHandler("toggleButton")
  void onToggleButtonClick(final ClickEvent e) {
    toggleText.setInnerHTML(toggleButton.getValue() ? M.messages().labelOn() : M.messages().labelOff());
    if (eventBus != null) {
      eventBus.fireEvent(new LabelVisibilityChangeEvent(toggleButton.getValue(), MarkerItem.TYPE.CALCULATION_POINT));
    }
  }

  public void setValue(final List<AeriusPoint> calculationPoints, final EmissionResultKey key) {
    calculationPointDivTable.setEmissionResultKey(key);
    calculationPointDivTable.asDataTable().setRowData(calculationPoints, true);
  }

  public void setMaxHeight(final int maxHeight) {
    editableDivTable.setMaxHeight(maxHeight);
  }

  public void setEditable(final boolean editable) {
    editableDivTable.setTableEnabled(editable);
    purgeButton.setEnabled(editable);
  }
}
