/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.context.AERIUS2Context;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.editor.OptGroupListBoxEditor;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Widget width 2 listboxes, sector group and sectors within the sector group selected.
 * Calls handler when sector is changed via the ui.
 */
public class SectorSelectorWidget extends Composite implements HasValueChangeHandlers<Sector> {

  interface SectorSelectorWidgetUiBinder extends UiBinder<Widget, SectorSelectorWidget> { }

  /**
   * if one sector present the list box contains the text to select and
   * the one sector. In that case that one sector is selected and the select is hidden.
   */
  private static final int ONLY_ONE_SECTOR = 2;
  private static final SectorSelectorWidgetUiBinder UI_BINDER = GWT.create(SectorSelectorWidgetUiBinder.class);

  @UiField(provided = true) ListBoxEditor<SectorGroup> sectorGroupListBox;
  @UiField(provided = true) OptGroupListBoxEditor<Sector> sectorListBox;
  private final AERIUS2Context context;

  public SectorSelectorWidget(final AERIUS2Context context, final HelpPopupController hpC) {
    this.context = context;
    sectorGroupListBox = new ListBoxEditor<SectorGroup>() {
      @Override
      protected String getLabel(final SectorGroup value) {
        return value == null ? M.messages().calculatorSelectSectorGroup() : M.messages().sectorGroup(value);
      }
    };
    sectorGroupListBox.addFirstEmptyItem();
    sectorGroupListBox.addItems(SectorGroup.values());
    sectorListBox = new OptGroupListBoxEditor<Sector>() {
      @Override
      protected String getLabel(final Sector value) {
        return value == null ? M.messages().calculatorSelectSector() : value.getName();
      }
      @Override
      protected String getKey(final Sector value) {
        return value == null ? null : String.valueOf(value.getSectorId());
      }
    };
    initWidget(UI_BINDER.createAndBindUi(this));

    hpC.addWidget(sectorGroupListBox, hpC.tt().ttSourceDetailSectorGroup());
    sectorGroupListBox.ensureDebugId(TestID.LIST_SECTORGROUP);
    sectorListBox.ensureDebugId(TestID.LIST_SECTOR);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Sector> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @UiHandler("sectorGroupListBox")
  final void onSectorGroupChange(final ChangeEvent e) {
    setSectorGroup(sectorGroupListBox.getValue(), true);
  }

  @UiHandler("sectorListBox")
  final void onSectorChange(final ChangeEvent e) {
    ValueChangeEvent.fire(this, sectorListBox.getValue());
  }

  /**
   * Sets the sector/sector group, but doesn't fire events. Use this to initially set the value.
   * @param sector sector
   */
  public void setSector(final Sector sector) {
    final SectorGroup sectorGroup = sector == null ? null : sector.getSectorGroup();
    sectorGroupListBox.setValue(sectorGroup);
    setSectorGroup(sectorGroup, false);
    sectorListBox.setValue(sector);
  }

  /**
   * Set the sector group on the listBox. If fireEvent is true the listener to this class is informed the sector has changed because the group
   * has changed.
   * @param sectorGroup sector group to set
   * @param fireEvent if true fires event when sector is changed
   */
  private void setSectorGroup(final SectorGroup sectorGroup, final boolean fireEvent) {
    sectorListBox.clear();
    Sector sector = null;
    if (sectorGroup == null) {
      sectorListBox.setVisible(false);
    } else {
      fillSectorListBox(sectorListBox, sectorGroup);
      sectorListBox.setVisible(sectorListBox.getItemCount() > ONLY_ONE_SECTOR);
      if (sectorListBox.getItemCount() == ONLY_ONE_SECTOR) {
        sectorListBox.setSelectedIndex(1);
        sector = sectorListBox.getValue();
      }
    }
    if (fireEvent) {
      ValueChangeEvent.fire(this, sector);
    }
  }

  /**
   * Fills the sector Listbox with sectors specific to the sector group.
   * @param sectorListBox Listbox
   * @param sectorGroup
   */
  private void fillSectorListBox(final OptGroupListBoxEditor<Sector> sectorListBox, final SectorGroup sectorGroup) {
    sectorListBox.clear();
    sectorListBox.addFirstEmptyItem();
    sectorListBox.addItems(filterOnGroupSector(context.getCategories().getSectors()));
  }

  /**
   * Returns only sectors for the current sector group.
   * @param sectors all sectors
   * @return filtered sectors.
   */
  private List<Sector> filterOnGroupSector(final ArrayList<Sector> sectors) {
    final ArrayList<Sector> filteredSectors = new ArrayList<Sector>();
    for (final Sector sector : sectors) {
      if (sector.getSectorGroup() == sectorGroupListBox.getValue()) {
        filteredSectors.add(sector);
      }
    }
    return filteredSectors;
  }
}
