/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.MarkerItem;
import nl.overheid.aerius.wui.geo.MarkerLayerWrapper.LabelVisibilityChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;

public class ViewableSourcesPanel extends Composite implements SourcesPanel {
  interface ViewableSourcesPanelUiBinder extends UiBinder<Widget, ViewableSourcesPanel> {}

  private static final ViewableSourcesPanelUiBinder UI_BINDER = GWT.create(ViewableSourcesPanelUiBinder.class);

  @UiField CheckBox toggleButton;
  private final SpanElement toggleText = Document.get().createSpanElement();

  @UiField(provided = true) SourcesDivTable sourcesDivTable;

  @UiField Label totalNOx;
  @UiField Label totalNH3;

  private final EventBus eventBus;

  private final ScenarioBaseMapLayoutPanel map;

  @Inject
  public ViewableSourcesPanel(final HelpPopupController hpC, final SourcesDivTable sourcesDivTable, final ScenarioBaseMapLayoutPanel map,
      final ScenarioBaseImportController importController, final EventBus eventBus) {
    this.sourcesDivTable = sourcesDivTable;
    this.map = map;
    this.eventBus = eventBus;

    initWidget(UI_BINDER.createAndBindUi(this));

    toggleButton.getElement().appendChild(toggleText);
    toggleText.setInnerHTML(M.messages().labelOff());

    toggleButton.ensureDebugId(TestID.TOGGLE_SOURCE_LABELS);

    sourcesDivTable.asDataTable().setMultiSelectionModel();
    sourcesDivTable.asDataTable().addSelectionChangeHandler(new Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        setSelection(((MultiSelectionModel<EmissionSource>) sourcesDivTable.asDataTable().getSelectionModel()).getSelectedSet());
      }
    });
  }

  private void setSelection(final Set<EmissionSource> sources) {
    final HashSet<Integer> selection = new HashSet<Integer>();

    for (final HasId src : sources) {
      selection.add(src.getId());
    }

    map.getMarkersLayer().setSelection(selection);
  }

  @UiHandler("toggleButton")
  void onLabelButtonClick(final ClickEvent e) {
    toggleText.setInnerHTML(toggleButton.getValue() ? M.messages().labelOn() : M.messages().labelOff());
    if (eventBus != null) {
      eventBus.fireEvent(new LabelVisibilityChangeEvent(toggleButton.getValue(), MarkerItem.TYPE.EMISSION_SOURCE));
    }
  }

  /**
   * set the data for the source.
   *
   * @param sources list of sources.
   * @param key current active key.
   * @param enableAdd allow add button.
   */
  @Override
  public void setData(final EmissionSourceList sources, final EmissionValueKey key, final boolean enableAdd) {
    sourcesDivTable.asDataTable().setRowData(sources, true);
  }

  /**
   * set the total for all deposition.
   *
   * @param total value.
   */
  @Override
  public void setTotalEmission(final double total, final Substance substance) {
    // We're showing separate emissions in this view, instead of the total.
  }

  /**
   * set the total and add the substance type.
   *
   * @param noxTotal noxTotal.
   * @param nh3Total nh3Total
   */
  @Override
  public void setTotalEmissionDouble(final double noxTotal, final double nh3Total) {
    totalNOx.setText(FormatUtil.formatEmissionWithUnitSmart(noxTotal));
    totalNH3.setText(FormatUtil.formatEmissionWithUnitSmart(nh3Total));
  }
}
