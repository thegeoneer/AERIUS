/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.CalculationInfo;
import nl.overheid.aerius.shared.domain.info.DepositionMarker;
import nl.overheid.aerius.shared.domain.info.DepositionMarker.DepositionMarkerType;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelViewImpl.Presenter;

/**
 * @param <T> The type that will be used as base for the item data.
 * @param <E> The widget that will be used to display each item.
 */
public abstract class AreaInfoPanel<T extends AssessmentArea, E extends AreaInfoPanelItem<T>> extends FlowPanel {
  private InfoVector infoVector;
  private EventBus eventBus;
  private CalculationInfo calculationInfo;
  private Presenter presenter;
  private ArrayList<T> info;

  public void setAreaInfo(final ArrayList<T> info) {
    this.info = info;

    update();
  }

  private void update() {
    clear();

    if (info != null) {
      for (final T areaInfo : info) {
        final E item = getNewItem(eventBus, infoVector, presenter);
        item.setBounds(areaInfo.getBounds());
        item.setAreaInfo(areaInfo);
        item.setMarkers(determineMarkers(areaInfo.getId()));
        addItem(item);
      }
    }
  }

  private ArrayList<DepositionMarker> determineMarkers(final long assessmentAreaId) {
    final ArrayList<DepositionMarker> markers = new ArrayList<DepositionMarker>();
    if (calculationInfo != null && calculationInfo.getDepositionMarkers() != null) {
      for (final DepositionMarker marker : calculationInfo.getDepositionMarkers()) {
        if (assessmentAreaId == marker.getAssessmentAreaId()) {
          switch (marker.getMarkerType()) {
         case HIGHEST_CALCULATED_AND_TOTAL:
            markers.add(marker.copyTypeOnly(DepositionMarkerType.HIGHEST_CALCULATED_DEPOSITION));
            markers.add(marker.copyTypeOnly(DepositionMarkerType.HIGHEST_TOTAL_DEPOSITION));
            break;
          case HIGHEST_CALCULATED_DEPOSITION:
          case HIGHEST_TOTAL_DEPOSITION:
            markers.add(marker);
            break;
          default:
            break;
          }
        }
      }
    }
    return markers;
  }

  protected void addItem(final E item) {
    add(item);
  }

  protected abstract E getNewItem(EventBus eventBus, InfoVector infoVector, Presenter presenter);

  public void setVectorLayer(final InfoVector infoVector) {
    this.infoVector = infoVector;
  }

  public void setCalculationInfo(final CalculationInfo calculationInfo) {
    this.calculationInfo = calculationInfo;

    update();
  }

  public void setEventBus(final EventBus eventBus) {
    this.eventBus = eventBus;
  }

  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }
}
