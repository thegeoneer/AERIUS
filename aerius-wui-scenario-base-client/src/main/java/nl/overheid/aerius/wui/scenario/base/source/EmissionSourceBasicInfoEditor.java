/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.editor.client.testing.FakeLeafValueEditor;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueBoxBase;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;

/**
 * Editor for some basic info of an {@link EmissionSource} object. The content is only read only and
 * there for should not be flushed to by a driver.
 */
public class EmissionSourceBasicInfoEditor extends Composite implements ValueAwareEditor<EmissionSource> {

  public interface ESBasicInfoDriver extends SimpleBeanEditorDriver<EmissionSource, EmissionSourceBasicInfoEditor> {}

  private static final ESBasicInfoEditorUiBinder UI_BINDER = GWT.create(ESBasicInfoEditorUiBinder.class);

  interface ESBasicInfoEditorUiBinder extends UiBinder<Widget, EmissionSourceBasicInfoEditor> {}

  @UiField @Ignore Label rowNr;
  @UiField @Ignore InlineLabel locationType;
  @UiField @Ignore Label x;
  @UiField @Ignore Label y;
  @UiField @Ignore Label geometryContext;
  //label is not managed by this editor, there for should be ignored.
  @UiField(provided = true) @Ignore TextBox label;
  @UiField Button buttonEditLocation;
  @UiField SwitchPanel locationTypeContext;

  final LeafValueEditor<Integer> idEditor = new FakeLeafValueEditor<Integer>() {
    @Override
    public void setValue(final Integer value) {
      rowNr.setText(String.valueOf(value));
    }
  };

  final LeafValueEditor<WKTGeometry> geometryEditor = new FakeLeafValueEditor<WKTGeometry>() {
    @Override
    public void setValue(final WKTGeometry value) {
      super.setValue(value);
      if (value == null) {
        locationType.setText(M.messages().sourceGeometry());
      } else {
        locationType.setText(M.messages().sourceGeometry(value.getType()));
      }
    }
  };

  public EmissionSourceBasicInfoEditor(final TextBox labelTextBox, final HelpPopupController hpC) {
    label = labelTextBox;
    initWidget(UI_BINDER.createAndBindUi(this));
    hpC.addWidget(buttonEditLocation, hpC.tt().ttSourceDetailButtonEditLocation());
    hpC.addWidget(label, hpC.tt().ttSourceDetailLabel());
    ensureDebugId("");
  }

  /**
   * Add the handler to be called when the edit button is clicked.
   * @param handler click handler
   */
  public void addEditHandler(final ClickHandler handler) {
    buttonEditLocation.addClickHandler(handler);
  }

  @Ignore
  public ValueBoxBase<String> getLabelEditor() {
    return label;
  }

  @Override
  public void setDelegate(final EditorDelegate<EmissionSource> delegate) {
    // no-op
  }

  @Override
  public void flush() {
    // no-op
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @Override
  public void setValue(final EmissionSource value) {
    final boolean noSector = value == null || value.getSector() == null || value.getSector().getProperties() == null;

    final String color = noSector ? null : ColorUtil.webColor(value.getSector().getProperties().getColor());
    rowNr.getElement().getStyle().setBackgroundColor(color);

    if (value == null || value.getGeometry() == null) {
      x.setText("");
      y.setText("");
      locationTypeContext.showWidget(0);
    } else {
      final TYPE type = value.getGeometry().getType();
      if (type == TYPE.POINT) {
        x.setText(FormatUtil.toWhole(value.getX()));
        y.setText(FormatUtil.toWhole(value.getY()));
        locationTypeContext.showWidget(0);
      } else {
        if (type == TYPE.LINE) {
          geometryContext.setText(FormatUtil.formatDistanceWithUnit(value.getGeometry().getMeasure()));
        } else {
          geometryContext.setText(FormatUtil.formatSurfaceWithUnit(value.getGeometry().getMeasure()));
        }
        locationTypeContext.showWidget(1);
      }
    }
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    rowNr.ensureDebugId(TestID.DIV_LABEL);
    locationType.ensureDebugId(TestID.DIV_LOCATIONTYPE);
    ensureDebugId(x.getElement(), TestID.DIV_X_COORDINATE);
    ensureDebugId(y.getElement(), TestID.DIV_Y_COORDINATE);
    buttonEditLocation.ensureDebugId(TestID.BUTTON_EDIT_LOCATION);
    label.ensureDebugId(TestID.INPUT_LABEL);
  }
}
