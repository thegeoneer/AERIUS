/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResult;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.wui.main.event.DevelopmentRulesEvent;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

public class CalculatorDevelopmentRuleLayerWrapper {
  interface DevelopmentRuleLayerWrapperEventBinder extends EventBinder<CalculatorDevelopmentRuleLayerWrapper> {
  }

  private final DevelopmentRuleLayerWrapperEventBinder eventBinder = GWT.create(DevelopmentRuleLayerWrapperEventBinder.class);

  private nl.overheid.aerius.wui.geo.DevelopmentRuleLayer layer;
  private MapLayoutPanel map;
  private DevelopmentRuleResult developmentRuleResult;
  private Boolean visible = false;

  private final ScenarioBaseAppContext<?, ?> appContext;

  @Inject
  public CalculatorDevelopmentRuleLayerWrapper(final EventBus eventBus, final ScenarioBaseAppContext<?, ?> appContext) {
    this.appContext = appContext;
    eventBinder.bindEventHandlers(this, eventBus);

  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent event) {
    if (event.getValue() instanceof ScenarioBasePlace) {
      updateLayer();
    }
  }

  @EventHandler
  public void onCalculationInitEvent(final CalculationInitEvent event) {
    removeLayer();
  }

  @EventHandler
  public void onStartCalculation(final CalculationStartEvent event) {
    removeLayer();
  }

  public void setVisible(final boolean visible) {
    if (layer != null) {
      map.setVisible(layer, visible);
    }
  }

  /**
   * Updates the marker layer
   */
  private void updateLayer() {
    final CalculatedScenario calculatedScenario = appContext.getUserContext().getCalculatedScenario();
    if (calculatedScenario == null) {
      return;
    }

    final Place where = appContext.getPlaceController().getWhere();
    final ScenarioBasePlace place = (ScenarioBasePlace) where;
    if (place.getJobType() == JobType.PRIORITY_PROJECT_UTILISATION) {
      return;
    }

    if (layer == null) {
      addLayer();
    }

    if (layer != null) {
      if (visible) {
        if (developmentRuleResult != null && !developmentRuleResult.isEmpty()) {
          layer.drawMarkers(developmentRuleResult);
        }

      } else {
        layer.clearMarkers();
      }
    }
  }

  private void clearMarkers() {
    if (layer != null) {
      layer.clearMarkers();
    }
  }

  /**
   * Add the layer from scratch (no null-check).
   */
  private void addLayer() {
//    layer = new DevelopmentRuleLayer(map);
//    map.addLayer((MapLayer) layer);
  }

  /**
   * Remove the layer fully, such as when a User Input calculation started.
   */
  private void removeLayer() {
    if (developmentRuleResult != null) {
      developmentRuleResult.clear();
    }

    if (layer != null) {
      clearMarkers();
      map.removeLayer((MapLayer) layer);
      layer = null;
    }
  }

  public void setMap(final ScenarioBaseMapLayoutPanel map) {
    this.map = map;
  }

  public void updateDevelopmentRulesAndDraw(final DevelopmentRulesEvent e) {
    this.developmentRuleResult = e.getResult();
    this.visible = e.isVisible();
    updateLayer();
  }

}
