/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.plan;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory.CategoryUnit;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource.PlanEmission;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.OptGroupListBoxEditor;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.InputWithUnitWidget;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;

/**
 * Input editor for single plan entries.
 */
class PlanInputEditor extends Composite implements Editor<PlanEmission>, HasClickHandlers, InputEditor<PlanEmission> {

  interface PlanInputDriver extends SimpleBeanEditorDriver<PlanEmission, PlanInputEditor> {
  }

  private static final PlanInputEditorUiBinder UI_BINDER = GWT.create(PlanInputEditorUiBinder.class);

  // Arbitrary threshold of the emission factor at which we can assume it is likely there is only 1 amount.
  // Thus it won't happen for number of houses, but it will for number of chemical industry factories.
  private static final double EMISSION_FACTOR_THRESHOLD_DEFAULT_ONE = 1000;

  interface PlanInputEditorUiBinder extends UiBinder<Widget, PlanInputEditor> {
  }

  @UiField @Ignore TextBox name;
  ValidatedValueBoxEditor<String> nameEditor;
  @UiField @Ignore Button submitButton;
  @UiField(provided = true) OptGroupListBoxEditor<PlanCategory> categoryEditor;
  @UiField @Ignore InputWithUnitWidget amountInputWithUnit;
  @UiField IntValueBox amountEditor;

  private double measure;

  public PlanInputEditor(final SectorCategories categories, final HelpPopupController hpC) {
    categoryEditor = new OptGroupListBoxEditor<PlanCategory>() {
      @Override
      protected String getLabel(final PlanCategory value) {
        return value == null ? M.messages().emptyFirstCategory() : value.getName();
      }
      @Override
      public void setValue(final PlanCategory value) {
        super.setValue(value);
        setAmountInputState(value);
      }
    };
    initWidget(UI_BINDER.createAndBindUi(this));
    nameEditor = new ValidatedValueBoxEditor<String>(name, M.messages().planName()) {
      @Override
      protected String noValidInput(final String value) {
        return M.messages().ivPlanNoEmptyName();
      }
    };
    nameEditor.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().ivPlanNoEmptyName();
      }
    });
    categoryEditor.addFirstEmptyItem();
    categoryEditor.addItems(categories.getPlanEmissionCategories());
    categoryEditor.addChangeHandler(new ChangeHandler() {
      @Override
      public void onChange(final ChangeEvent event) {
        changeAmountInput();
      }
    });
    StyleUtil.I.setPlaceHolder(name, M.messages().mobileSourceName());

    amountEditor.ensureDebugId(TestID.INPUT_PLAN_AMOUNT);
    name.ensureDebugId(TestID.INPUT_PLAN_NAME);
    categoryEditor.ensureDebugId(TestID.INPUT_PLAN_CATEGORY);
    submitButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_SUBMIT);

    //  Help widgets
    hpC.addWidget(name, hpC.tt().ttPlanName());
    hpC.addWidget(amountEditor, hpC.tt().ttPlanAmount());
    hpC.addWidget(categoryEditor, hpC.tt().ttPlanCategoryList());
    hpC.addWidget(submitButton, hpC.tt().ttPlanSubmitButton());

  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return submitButton.addClickHandler(handler);
  }

  @Override
  public void postSetValue(final EmissionSource source) {
    measure = source.getGeometry() != null && source.getGeometry().getType() == TYPE.POLYGON
        ? source.getGeometry().getMeasure() : 1;
  }

  @Override
  public void resetPlaceholders() {
    // no-op
  }

  private void changeAmountInput() {
    final PlanCategory category = categoryEditor.getValue();
    setAmountInputState(category);
    final boolean hasUnit = category != null && category.getCategoryUnit() != CategoryUnit.NO_UNIT;
    if (hasUnit) {
      if (category.getCategoryUnit() == CategoryUnit.HECTARE || category.getCategoryUnit() == CategoryUnit.SQUARE_METERS) {
        amountEditor.setValue(MathUtil.round(category.getCategoryUnit() == CategoryUnit.HECTARE ? measure / SharedConstants.M2_TO_HA : measure));
      } else if (category.getEmissionFactor(new EmissionValueKey(Substance.NOX)) > EMISSION_FACTOR_THRESHOLD_DEFAULT_ONE) {
        amountEditor.setValue(1);
      } else {
        amountEditor.setValue(0);
        amountEditor.setText(null);
      }
    } else {
      amountEditor.setValue(1);
    }
  }
  private void setAmountInputState(final PlanCategory category) {
    final boolean hasUnit = category != null && category.getCategoryUnit() != CategoryUnit.NO_UNIT;
    amountInputWithUnit.setVisible(hasUnit);
    amountInputWithUnit.setUnit(hasUnit ? M.messages().planUnit(category.getCategoryUnit()) : "");
    amountEditor.setEnabled(hasUnit);
  }
}
