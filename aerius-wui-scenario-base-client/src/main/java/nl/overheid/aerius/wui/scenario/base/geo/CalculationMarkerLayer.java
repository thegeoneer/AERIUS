/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import java.util.HashMap;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.MarkerLayer;
import nl.overheid.aerius.shared.domain.info.CalculationInfo;
import nl.overheid.aerius.shared.domain.info.DepositionMarker;
import nl.overheid.aerius.wui.geo.AeriusMarker;
import nl.overheid.aerius.wui.geo.MarkerItem;
import nl.overheid.aerius.wui.geo.MarkerLegend;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 * Shows markers on the map with information related to the current calculation.
 * Markers are {@link MARKER_TYPE#MARKER_TWO} and {@link MARKER_TYPE#MARKER_ONE}
 *
 */
public class CalculationMarkerLayer extends MarkerLayer {

  private final HashMap<Integer, CalculationMarker> markers = new HashMap<Integer, CalculationMarker>();

  /**
   * Init the map with title.
   *
   * @param map the layer to hold the markers
   */
  public CalculationMarkerLayer(final MapLayoutPanel map) {
    super(map, M.messages().markerLayerTitle(), new MarkerLegend());
  }

  /**
   * Draw the markers on the layer.
   *
   * @param calculationInfo Marker information
   */
  public void drawMarkers(final CalculationInfo calculationInfo) {
    clearMarkers();
    markers.clear();

    // Add the initial set of markers
    for (final DepositionMarker point : calculationInfo.getDepositionMarkers()) {
      //possible to already have 2 markers on same spot (different assessment areas). Ensure a combined marker in that case.
      CalculationMarker marker = markers.get(point.getReceptorId());
      if (marker == null) {
        marker = new CalculationMarker(point, MarkerItem.TYPE.EMISSION_RESULT);
        markers.put(point.getReceptorId(), marker);
      }
      marker.addMarkerType(point.getMarkerType());
    }

    // Draw the markers
    for (final CalculationMarker marker : markers.values()) {
      marker.drawMarker();
      final AeriusMarker layerMarker = new AeriusMarker(marker);

      layerMarker.addMarkerItem(marker);
      addMarker(layerMarker.getMarker());
    }
  }

}
