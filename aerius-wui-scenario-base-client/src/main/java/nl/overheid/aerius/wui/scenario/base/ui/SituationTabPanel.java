/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.EmissionSourceListNameChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.DoubleDecorationPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SimpleFocusLabel;
import nl.overheid.aerius.wui.main.widget.SimpleTabPanel;
import nl.overheid.aerius.wui.scenario.base.ui.SituationTabItem.SituationTabHandler;

/**
 * Comparison tab handler taking care of all interaction with
 * the tab view.
 */
public class SituationTabPanel extends Composite {
  interface TabPanelEventBinder extends EventBinder<SituationTabPanel> { }

  private final TabPanelEventBinder eventBinder = GWT.create(TabPanelEventBinder.class);

  private static final int MAX_TAB_WIDTH = 90;

  private class EventHandlers implements SelectionHandler<Integer>, SituationTabHandler, ClickHandler {
    @Override
    public void onSelection(final SelectionEvent<Integer> event) {
      final Integer selectedSituationTab = convertSituation(event.getSelectedItem());

      if (handler != null) {
        if (hasNoSituationTwo() && Situation.situationFromId(selectedSituationTab) == Situation.SITUATION_TWO) {
          handler.onAdd(Situation.SITUATION_TWO);
        } else if (selectedSituationTab != -1) {
          handler.onSelect(Situation.situationFromId(selectedSituationTab));
        }
      }
    }

    private Integer convertSituation(final Integer selectedItem) {
      final int csi;
      switch (selectedItem) {
      case 1:
        csi = hasNoSituationTwo() ? 1 : -1;
        break;
      case 3:
      case 2:
        csi = selectedItem - 1;
        break;
      default:
        csi = selectedItem;
        break;
      }
      return csi;
    }

    @Override
    public void delete(final SituationTabItem item) {
      if (item == situation2Item) {
        removeSituationTwoTab();

        if (handler != null) {
          handler.onDelete(Situation.SITUATION_TWO);
        }
      } else if (item == situation1Item) {
        if (!hasNoSituationTwo()) {
          removeSituationTwoTab();
        }
        if (handler != null) {
          handler.onDelete(Situation.SITUATION_ONE);
        }
      }
    }

    @Override
    public void changeName(final SituationTabItem item) {
      eventBus.fireEvent(new EmissionSourceListNameChangeEvent(item.getId(), item.getName()));
    }

    @Override
    public void onClick(final ClickEvent event) {
      handler.switchSituations();
    }
  }

  /**
   * Handler for situation changes.
   */
  public interface SituationChangeHandler {

    /**
     * @param situation Situation that was deleted
     */
    void onDelete(Situation situation);

    /**
     * @param situation Situation that was added.
     */
    void onAdd(Situation situation);

    /**
     * @param situation Situation that was selected.
     */
    void onSelect(Situation situation);

    /**
     * Switch situations so that situation 1 is swapped with situation 2.
     */
    void switchSituations();
  }

  private final EventHandlers handlers = new EventHandlers();

  private final SimpleTabPanel tabPanel = new SimpleTabPanel();

  // Tabs
  private final SituationTabItem situation1Item;
  private final Label createNewSituationLabel;
  private final SituationTabItem situation2Item;
  private final SimpleFocusLabel comparisonItem;

  private final EventBus eventBus;

  private SituationChangeHandler handler;

  private final FocusPanel switcheroo;

  /**
   * Constructor.
   *
   * @param eventBus The {@link EventBus} used to fire name change events
   * @param hpC Helptooltip controller
   */
  @Inject
  public SituationTabPanel(final EventBus eventBus, final HelpPopupController hpC) {
    this.eventBus = eventBus;

    // Add handlers
    tabPanel.addSelectionHandler(handlers);

    eventBinder.bindEventHandlers(this, eventBus);

    // Situation 1 tab
    situation1Item = new SituationTabItem(M.messages().situationOne());
    situation1Item.setCanDelete(false);
    situation1Item.setHandler(handlers);
    situation1Item.getElement().getStyle().setPropertyPx("maxWidth", MAX_TAB_WIDTH);
    hpC.addWidget(situation1Item, hpC.tt().ttSituationOne());

    // Create switcheroo tab
    switcheroo = new FocusPanel();
    switcheroo.addStyleName(R.css().buttonSwitch());
    switcheroo.addClickHandler(handlers);

    // Create a new situation tab
    createNewSituationLabel = new Label(M.messages().situationCreate());
    hpC.addWidget(createNewSituationLabel, hpC.tt().ttSituationNew());

    // Situation 2 tab
    situation2Item = new SituationTabItem(M.messages().situationTwo());
    situation2Item.setHandler(handlers);
    situation2Item.setCanDelete(true);
    situation2Item.getElement().getStyle().setPropertyPx("maxWidth", MAX_TAB_WIDTH);
    hpC.addWidget(situation2Item, hpC.tt().ttSituationTwo());

    // Comparison tab
    comparisonItem = new SimpleFocusLabel(M.messages().situationsCompare());
    // FIXME This widget should not reference DoubleDecorationPanel, but the UNDERLINE_PADDING value needs to be visible to both (sprint 15)
    comparisonItem.getElement().getStyle().setPaddingBottom(DoubleDecorationPanel.UNDERLINE_PADDING, Unit.PX);
    hpC.addWidget(comparisonItem, hpC.tt().ttSituationComparison());

    // Add initial tabs
    tabPanel.add(situation1Item);
    tabPanel.add(createNewSituationLabel);
    initWidget(tabPanel);

    situation1Item.ensureDebugId(TestID.TAB_SIT_1);
    createNewSituationLabel.ensureDebugId(TestID.TAB_NEW);
    situation2Item.ensureDebugId(TestID.TAB_SIT_2);
    comparisonItem.ensureDebugId(TestID.TAB_COMPARE);
    switcheroo.ensureDebugId(TestID.TAB_SWITCH);
  }

  @EventHandler
  void onNameChange(final EmissionSourceListNameChangeEvent event) {
    if (situation1Item.getId() == event.getId()) {
      situation1Item.setName(event.getName());
    } else if (situation2Item.getId() == event.getId()) {
      situation2Item.setName(event.getName());
    } // not active visible emission source list change, so no visible name to be changed.
  }

  /**
   * Set the current situation.
   *
   * @param situation the current situation to set.
   * @param id situation id
   * @param name situation name
   */
  public void setSituationName(final Situation situation, final int id, final String name) {
    switch (situation) {
    case SITUATION_ONE:
      situation1Item.setName(name);
      situation1Item.setId(id);
      break;
    case SITUATION_TWO:
      if (name == null) {
        removeSituationTwoTab();
      } else {
        if (hasNoSituationTwo()) {
          addSituationTwoTab();
        }
        situation2Item.setName(name);
        situation2Item.setId(id);
      }
      break;
    case COMPARISON:
      break;
    default:
      throw new IllegalStateException("There must never be more than 3 tabs here.");
    }
  }

  public void setHandler(final SituationChangeHandler handler) {
    this.handler = handler;
  }

  /**
   * Set the focus for a particular situation.
   *
   * @param situation The situation to focus on.
   */
  public void setFocus(final Situation situation) {
    Widget focusOn = null;
    switch (situation) {
    case SITUATION_ONE:
      situation1Item.removeTopStyle(R.css().underlineSolidDark());
      situation2Item.removeTopStyle(R.css().underlineDashedDark());
      focusOn = situation1Item;
      break;
    case SITUATION_TWO:
      situation1Item.removeTopStyle(R.css().underlineSolidDark());
      situation2Item.removeTopStyle(R.css().underlineDashedDark());
      focusOn = situation2Item;
      break;
    case COMPARISON:
      situation1Item.addTopStyle(R.css().underlineSolidDark());
      situation2Item.addTopStyle(R.css().underlineDashedDark());
      focusOn = comparisonItem;
      break;
    default:
      throw new IllegalStateException("There must never be more than 3 tabs here.");
    }

    tabPanel.setFocus(focusOn, false);
  }

  /**
   * Remove the second situation from the tab panel.
   */
  private void removeSituationTwoTab() {
    tabPanel.remove(switcheroo);
    tabPanel.remove(situation2Item);
    tabPanel.remove(comparisonItem);
    tabPanel.add(createNewSituationLabel);
    situation1Item.setCanDelete(false);
  }

  /**
   * Add the second situation to the tab panel.
   */
  private void addSituationTwoTab() {
    tabPanel.remove(createNewSituationLabel);
    tabPanel.add(switcheroo);
    tabPanel.add(situation2Item, true);
    tabPanel.add(comparisonItem);
    situation1Item.setCanDelete(true);
  }

  private boolean hasNoSituationTwo() {
    return tabPanel.getWidgetIndex(createNewSituationLabel) != -1;
  }

  /**
   * Sets the state of the tabs. If they are in edit mode or not. In edit mode user can add/remove situations.
   * @param enable if true in edit mode
   */
  public void setEditable(final boolean enable) {
    createNewSituationLabel.setVisible(enable);
  }
}
