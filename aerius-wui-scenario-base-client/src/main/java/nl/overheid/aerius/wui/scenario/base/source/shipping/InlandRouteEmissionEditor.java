/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayTypeUtil;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.BaseEmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.NotEditCancellable;
import nl.overheid.aerius.wui.scenario.base.source.shipping.InlandRouteInputEditor.InlandRouteInputDriver;

/**
 * List editor of inland ships.
 */
public class InlandRouteEmissionEditor extends BaseEmissionSourceEditor<InlandRouteEmissionSource> implements NotEditCancellable {

  public interface InlandRouteEmissionDriver extends SimpleBeanEditorDriver<InlandRouteEmissionSource, InlandRouteEmissionEditor> {}

  interface InlandRouteEmissionEditorEventBinder extends EventBinder<InlandRouteEmissionEditor> { }

  InputListEmissionEditor<RouteInlandVesselGroup, InlandRouteInputEditor> emissionSubSourcesEditor;

  @Path("")
  final InlandRouteWaterwayTypeEditor inlandWaterwayTypeEditor;

  private final InlandRouteEmissionEditorEventBinder eventBinder = GWT.create(InlandRouteEmissionEditorEventBinder.class);
  private final EventBus eventBus;
  private final SourceMapPanel map;
  private final CalculatorContext context;
  private final InlandShippingCategories inlandShippingCategories;
  private final InlandRouteInputEditor inlandRouteInputEditor;
  private EditorDelegate<InlandRouteEmissionSource> delegate;
  private InlandRouteEmissionSource source;

  @SuppressWarnings("unchecked")
  public InlandRouteEmissionEditor(final EventBus eventBus, final SourceMapPanel map, final CalculatorContext context,
      final CalculatorServiceAsync service, final HelpPopupController hpC) {
    super(M.messages().inlandShipPanelTitle());
    this.eventBus = eventBus;
    this.map = map;
    this.context = context;
    inlandShippingCategories = context.getCategories().getInlandShippingCategories();
    inlandWaterwayTypeEditor = new InlandRouteWaterwayTypeEditor(eventBus, hpC, service, inlandShippingCategories);
    getMainPanel().add(inlandWaterwayTypeEditor);

    inlandRouteInputEditor = new InlandRouteInputEditor(eventBus, map, inlandShippingCategories, hpC);
    inlandRouteInputEditor.setInlandRouteDirectionWidget(inlandWaterwayTypeEditor.inlandWaterwayTypeEditor.waterwayDirectionEditor);
    emissionSubSourcesEditor = new InputListEmissionEditor<RouteInlandVesselGroup, InlandRouteInputEditor>(
        eventBus, inlandRouteInputEditor,
        hpC, new ShipMessages(hpC),
        RouteInlandVesselGroup.class, "InlandShipping",
        (SimpleBeanEditorDriver<RouteInlandVesselGroup, InputEditor<RouteInlandVesselGroup>>) GWT.create(InlandRouteInputDriver.class),
        service, false) {
      @Override
      protected RouteInlandVesselGroup createNewRowEmissionValues() {
        return new RouteInlandVesselGroup();
      }
    };

    addContentPanel(emissionSubSourcesEditor);
    emissionSubSourcesEditor.ensureDebugId(TestID.DIVTABLE_EDITABLE);
  }

  @EventHandler
  void onInlandWaterwayTypeUpdateEvent(final InlandWaterwayTypeUpdateEvent event) {
    updateInlandWaterType(event.getInlandWaterwayType());
  }

  /**
   * Updates the inlandWaterwayType for all sub sources so the new emissions can be calculated.
   * @param inlandWaterwayType
   */
  private void updateInlandWaterType(final InlandWaterwayType inlandWaterwayType) {
    final InlandWaterwayCategory category = inlandWaterwayType.getWaterwayCategory();
    final WaterwayDirection direction = inlandWaterwayType.getWaterwayDirection();
    source.setWaterwayCategory(category);
    source.setWaterwayDirection(direction);
    if (InlandWaterwayTypeUtil.isDirectionCorrect(category, direction)) {
      emissionSubSourcesEditor.forceRefresh();
    }
    inlandRouteInputEditor.setInlandWaterwayType(category, direction);
    inlandWaterwayTypeEditor.setDirectionLabelVisible(category != null && category.isDirectionRelevant());
  }

  @Override
  public void setEmissionValueKey(final EmissionValueKey key) {
    emissionSubSourcesEditor.setEmissionValueKey(key);
  }

  @Override
  public void postSetValue(final InlandRouteEmissionSource source) {
    eventBinder.bindEventHandlers(this, eventBus);
    this.source = source;
    super.postSetValue(source);
    emissionSubSourcesEditor.postSetValue(source);
    // Show the shipping route layer when editing ships
    map.setVisible((String) context.getSetting(SharedConstantsEnum.LAYER_SHIP_NETWORK), true);
  }

  @Override
  public void setDelegate(final EditorDelegate<InlandRouteEmissionSource> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void flush() {
    final InlandWaterwayCategory waterwayCategory = source.getWaterwayCategory();
    if (waterwayCategory != null) {
      for (final RouteInlandVesselGroup rivg : emissionSubSourcesEditor.getValue()) {
        if (!inlandShippingCategories.isValidCombination(rivg.getCategory(), waterwayCategory)) {
          delegate.recordError(M.messages().shipWaterwayNotAllowedCombination(rivg.getCategory().getCode(), waterwayCategory.getCode()),
              rivg.getCategory(), inlandWaterwayTypeEditor.inlandWaterwayTypeEditor);
        }
      }
    }
  }
}
