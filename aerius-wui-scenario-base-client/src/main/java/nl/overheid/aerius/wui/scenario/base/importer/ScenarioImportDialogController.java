/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.importer;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;

import nl.overheid.aerius.shared.domain.ConsecutiveNamedList;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.importer.ImportConflictAction;
import nl.overheid.aerius.wui.main.ui.importer.ImportDialogController;
import nl.overheid.aerius.wui.main.ui.importer.ImportProperties;
import nl.overheid.aerius.wui.main.widget.event.CancelEvent;
import nl.overheid.aerius.wui.scenario.base.domain.ScenarioUtil;
import nl.overheid.aerius.wui.scenario.base.processor.ImportOutputPollingAgent;

/**
 * Controller for import dialog.
 */
public abstract class ScenarioImportDialogController extends ImportDialogController<ImportProperties, ScenarioImportDialogPanel, ImportResult> {
  private final ChangeHandler fileUploadChangedHandler = new ChangeHandler() {
    @Override
    public void onChange(final ChangeEvent event) {
      lastResult = null;
      validateForm();
    }
  };

  private final ValueChangeHandler<Boolean> validateOnChangeHandler = new ValueChangeHandler<Boolean>() {
    @Override
    public void onValueChange(final ValueChangeEvent<Boolean> event) {
      validateForm();
    }
  };

  private final ImportOutputPollingAgent pollingAgent;

  private final ConsecutiveNamedList<AeriusPoint> originalCustomCalculationPoints;
  private final EmissionSourceList originalList;

  /**
   * The value lastResult is used to store the value in case conflicts are detected. In that case the user is asked to resolve the import conflicts
   * and then the user can click submit. The submit doesn't do the server call, but continues importing the already retrieved result.
   */
  private ImportResult lastResult;

  private String uuid;

  /**
   * Creates controller for importing sources.
   *
   * @param originalList list of current list of sources, can be null
   * @param originalCustomCalculationPoints
   * @param service
   * @param content
   */
  public ScenarioImportDialogController(final EmissionSourceList originalList,
      final ConsecutiveNamedList<AeriusPoint> originalCustomCalculationPoints, final ImportOutputPollingAgent pollingAgent,
      final ScenarioImportDialogPanel content) {
    super(content);

    this.pollingAgent = pollingAgent;

    this.originalList = originalList;
    this.originalCustomCalculationPoints = originalCustomCalculationPoints;

    this.content = content;
    content.addFileUploadChangedHandler(fileUploadChangedHandler);
    content.addSubstanceValueChangeHandler(validateOnChangeHandler);
    content.addConflictValueChangeHandler(validateOnChangeHandler);
    content.addErrorOnContinueValueChangeHandler(validateOnChangeHandler);
  }

  @Override
  public void onSubmit(final SubmitEvent event) {
    super.onSubmit(event);
    if (isInProgress()) {
      return;
    }

    setInProgress(true);
  }

  @Override
  public void onCancel(final CancelEvent event) {
    pollingAgent.stop();
    setInProgress(false);
  }

  /**
   * Handle the import result.
   *
   * @param result The result to handle.
   * @param conflictAction Which action to take according to user.
   * @param importType The imported file type.
   */
  protected abstract void handleImportResult(ImportResult result, ImportConflictAction conflictAction, String uuid);

  @Override
  public void show() {
    lastResult = null;
    content.showError(null);
    content.showLineErrorDiv(false);
    content.hideCounterData();
    content.showConflictDiv(false);
    content.showFileField(true);
    content.enableSubstanceRadioButtons(true);
    content.showSubstanceDiv(false);
    setConfirmMode(true);
    super.show();
  }

  @Override
  public void onFailure(final Throwable error) {
    setInProgress(false);
    content.showError(error == null ? null : M.getErrorMessage(error));
  }

  private void showOnImport(final ImportResult result) {
    content.showError(null);
    content.showSubstanceDiv(false);

    final CalculationPointList calcPoints = result.getCalculationPoints();

    final int amountOfImportedSources = result.getSources(0) == null ? 0 : result.getSources(0).size();
    final int amountOfImportedCalcPoints = calcPoints.size();
    int conflictsExisting = 0;
    int conflictsImported = 0;
    if (amountOfImportedSources > 0) {
      conflictsExisting = ScenarioUtil.getAmountOfDuplicates(originalList, result.getSources(0));
      conflictsImported = ScenarioUtil.getAmountOfDuplicates(result.getSources(0), originalList);
    }
    if (amountOfImportedCalcPoints > 0) {
      conflictsExisting += ScenarioUtil.getAmountOfDuplicates(originalCustomCalculationPoints, calcPoints);
      conflictsImported += ScenarioUtil.getAmountOfDuplicates(calcPoints, originalCustomCalculationPoints);
    }
    content.setCounterData(amountOfImportedSources, amountOfImportedCalcPoints, conflictsImported, conflictsExisting);
    content.showConflictDiv(conflictsImported > 0);

    final boolean hasErrors = !result.getExceptions().isEmpty();
    content.showLineErrorDiv(hasErrors);
    if (hasErrors) {
      content.clearErrorList();

      for (final Throwable error : result.getExceptions()) {
        content.addErrorListEntry(M.getErrorMessage(error));
      }
    }

    content.enableSubstanceRadioButtons(false);
    // ensure the button doesn't get used for another file upload.
    content.showFileField(false);
    setConfirmMode(false);
    setSubmitEnabled(false);
    super.show();
  }

  @Override
  public void onSuccess(final ImportResult result) {
    pollingAgent.stop();
    // if no exceptions, handle the import if the import is for PAA or if there are no duplicates (both sources and calculation points)
    if (shouldImportRightAway(result)) {
      hide();
      handleImportResult(result, content.getConflictAction(), uuid);
    } else {
      lastResult = result;
      showOnImport(lastResult);
      setInProgress(false);
    }
  }

  public boolean isUtilisation() {
    return content.isUtilisation();
  }

  protected boolean shouldImportRightAway(final ImportResult result) {
    boolean shouldWe = false;

    if (result != null) {
      shouldWe = result.getExceptions().isEmpty()
        && (ImportType.PAA == result.getType()
            || (ScenarioUtil.getAmountOfDuplicates(originalList, result.getSources(0)) == 0
                && ScenarioUtil.getAmountOfDuplicates(originalCustomCalculationPoints, result.getCalculationPoints()) == 0));
    }

    return shouldWe;
  }

  @Override
  protected void submitClick() {
    if (lastResult != null) {
      hide();
      handleImportResult(lastResult, content.getConflictAction(), uuid);
    }
  }

  @Override
  public void getImport(final String uuid) {
    this.uuid = uuid;
    pollingAgent.start(uuid, this);
  }

  private void validateForm() {
    content.showError(null);
    content.showSubstanceDiv(lastResult == null && ImportType.BRN == ImportType.determineByFilename(content.getFileUploadFilename()));
    final boolean submit;

    if (lastResult == null && !content.isFileUploadPresent()) {
      submit = false;
    } else if (content.isSubstanceDivVisible() && !content.isSubstanceValueSet()) {
      submit = false;
    } else if (content.isConflictDivVisibile() && !content.isConflictValueSet()) {
      submit = false;
    } else if (lastResult != null && !lastResult.getExceptions().isEmpty() && !content.isErrorContinueButtonChecked()) {
      submit = false;
    } else {
      submit = true;
    }
    setSubmitEnabled(submit);
  }
}
