/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.source.EmissionSubSource;
import nl.overheid.aerius.wui.main.widget.table.SimpleContextualContent;

public abstract class ConciseEmissionSubSourceWidget<T extends EmissionSubSource> extends SimpleContextualContent<T> {
  private final EmissionSubSourceViewer viewer;

  public ConciseEmissionSubSourceWidget(final EmissionSubSourceViewer viewer) {
    super();

    this.viewer = viewer;
  }

  @Override
  public boolean view(final T obj) {
    viewer.setValue(obj);

    setTitleText(getLabel(obj));

    final Widget widg = viewer.asWidget();
    initContent(widg);
    return widg != null;
  }

  public abstract String getLabel(final T obj);
}
