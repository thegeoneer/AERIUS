/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.DepositionMarker;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelViewImpl.Presenter;

public abstract class AreaInfoPanelItem<T extends AssessmentArea> extends Composite {
  protected final EventBus eventBus;
  protected final Presenter presenter;

  @UiField(provided = true) DepositionMarkerTable depositionMarkerTable;

  private BBox bounds;

  public AreaInfoPanelItem(final EventBus eventBus, final Presenter presenter) {
    this.eventBus = eventBus;
    this.presenter = presenter;
  }

  @UiHandler("zoomButton")
  void onZoomClick(final ClickEvent ev) {
    eventBus.fireEvent(new LocationChangeEvent(bounds));
  }

  public abstract void setAreaInfo(final T result);

  public void setMarkers(final ArrayList<DepositionMarker> markers) {
    depositionMarkerTable.setRowData(markers);
    depositionMarkerTable.setVisible(!markers.isEmpty());
  }

  public void setBounds(final BBox bounds) {
    this.bounds = bounds;
  }
}
