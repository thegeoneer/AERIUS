/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.List;

import nl.overheid.aerius.shared.domain.sector.category.ShippingCategory;
import nl.overheid.aerius.wui.main.widget.ParameterizedSuggestOracle;
import nl.overheid.aerius.wui.main.widget.ParameterizedSuggestion;

/**
 * MultiWordSuggestOracle to display {@link ShippingCategory} items.
 */
class ShipCategorySuggestionOracle<S extends ShippingCategory> extends ParameterizedSuggestOracle<S> {

  /**
   * Inner class defining how a suggestion for the given category should be displayed.
   */
  static class ShipCategorySuggestion<C extends ShippingCategory> extends ParameterizedSuggestion<C> {
    /**
     * @param category The category to display as suggestion.
     */
    public ShipCategorySuggestion(final C category) {
      super(category);
    }

    @Override
    public String getDisplayString() {
      return object.getName() + " " + object.getDescription();
    }

    @Override
    public String getReplacementString() {
      return object.getName();
    }
  }

  /**
   * @param categories The list of categories to choose from.
   */
  public ShipCategorySuggestionOracle(final List<S> categories) {
    super(categories);
  }

  @Override
  protected Suggestion getSuggestion(final String query, final S cat) {
    if (cat.getName().toLowerCase().contains(query)
        || (query.length() > 1 && cat.getDescription() != null && cat.getDescription().toLowerCase().contains(query))) {
      return getSuggestion(cat);
    }

    return null;
  }

  @Override
  protected Suggestion getSuggestion(final S cat) {
    return new ShipCategorySuggestion<S>(cat);
  }
}
