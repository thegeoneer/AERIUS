/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.FarmLodgingCustomEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.wui.main.domain.FarmUtil;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * Row for {@link FarmEmissionViewer} for emission of a single farm lodging type.
 */
class FarmEmissionViewerRow extends Composite implements LeafValueEditor<FarmLodgingEmissions> {

  private static final EmissionValueKey EMISSION_VALUE_KEY = new EmissionValueKey(Substance.NH3);

  private static final FarmEmissionViewerRowUiBinder UI_BINDER = GWT.create(FarmEmissionViewerRowUiBinder.class);

  interface FarmEmissionViewerRowUiBinder extends UiBinder<Widget, FarmEmissionViewerRow> { }

  @UiField Image image;
  @UiField Label emission;
  @UiField Label animalAmount;
  @UiField Label animalType;

  public FarmEmissionViewerRow() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public void setValue(final FarmLodgingEmissions value) {
    if (value != null) {
      image.setResource(FarmUtil.getFarmImageResource(value));
      emission.setText(FormatUtil.formatEmissionWithUnit(value.getEmission(EMISSION_VALUE_KEY)));
      animalAmount.setText(String.valueOf(value.getAmount()));
      if (value instanceof FarmLodgingStandardEmissions) {
        animalType.setText(((FarmLodgingStandardEmissions) value).getCategory().getName());
        animalType.setTitle(((FarmLodgingStandardEmissions) value).getCategory().getDescription());
      } else if (value instanceof FarmLodgingCustomEmissions) {
        animalType.setText(((FarmLodgingCustomEmissions) value).getDescription());
        animalType.setTitle(((FarmLodgingCustomEmissions) value).getDescription());
      }
    }
  }

  @Override
  public FarmLodgingEmissions getValue() {
    // No-op
    return null;
  }

}
