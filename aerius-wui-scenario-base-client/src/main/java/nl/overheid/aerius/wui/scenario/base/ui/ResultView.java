/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;

public interface ResultView extends IsWidget, HasTitle {

  interface Presenter {
    IsWidget getCalculationButtons();

    void goTo(ResultPlace.ScenarioBaseResultPlaceState overview);
  }

  void setPresenter(Presenter presenter);

  AcceptsOneWidget getContentPanel();

  void calculationStarted();

  void calculationCancelled();

  void calculationFinished();

  void calculationSummaryAvailable(boolean status);

  void setTab(ResultPlace.ScenarioBaseResultPlaceState tabState);
}
