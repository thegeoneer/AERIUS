/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.core.client.impl.SchedulerImpl;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.LayerPanel;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.AttachedPopupButton;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.ListButton;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelPresenter;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelViewImpl;

@Singleton
public abstract class ScenarioBaseTaskBarImpl extends Composite implements ScenarioBaseTaskBar {

  interface ScenarioBaseViewImplEventBinder extends EventBinder<ScenarioBaseTaskBarImpl> {}

  private static final ScenarioBaseViewImplEventBinder eventBinder = GWT.create(ScenarioBaseViewImplEventBinder.class);

  public interface CustomStyle extends CssResource {
    String substanceSelectionPulldown();
  }

  public @UiField CustomStyle style;

  public @UiField ListButton yearSelection;
  public @UiField ListButton substanceSelection;
  public @UiField(provided = true) ListButton resultTypeSelection = new ListButton() {
    @Override
    protected void setButtonText(final Widget selectedWidget) {
      // Override setting text on button. The text is set on change events.
    }
  };

  public @UiField AttachedPopupButton infoPanelButton;
  public @UiField AttachedPopupButton layerPanelButton;

  public @UiField(provided = true) LayerPanel layerPanel;
  public @UiField(provided = true) InfoPanelViewImpl infoPanel;

  private final Label rtDepositionLabel = new Label(M.messages().emissionResultType(EmissionResultType.DEPOSITION));
  private final Label rtConcentrationLabel = new Label(M.messages().emissionResultType(EmissionResultType.CONCENTRATION));
  private final Label rtNumberOfDaysLabel = new Label(M.messages().emissionResultType(EmissionResultType.NUM_EXCESS_DAY));
  private final List<HandlerRegistration> handlers = new ArrayList<>();

  protected final EventBus eventBus;
  protected final HelpPopupController hpC;
  protected final ScenarioBaseAppContext<?, ?> appContext;
  private ScenarioBaseTheme theme;

  private final ClickHandler yearClickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      final String year = ((HasText) event.getSource()).getText();

      yearSelection.setText(year);
      yearSelection.addStyleName(R.css().number());
      appContext.getUserContext().getEmissionValueKey().setYear(Integer.parseInt(year));
      if (eventBus != null) {
        eventBus.fireEvent(new YearChangeEvent(Integer.parseInt(year)));
      }
    }
  };

  private final ClickHandler substanceClickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      final Substance substance = Substance.safeValueOf(((HasText) event.getSource()).getText());
      updateContext(substance);
    }
  };

  private final ClickHandler rtClickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      EmissionResultType ert;
      if (event.getSource() == rtConcentrationLabel) {
        ert = EmissionResultType.CONCENTRATION;
      } else if (event.getSource() == rtNumberOfDaysLabel) {
        ert = EmissionResultType.NUM_EXCESS_DAY;
      } else {
        ert = EmissionResultType.DEPOSITION;
      }

      initSubstanceListBox(ert);

      final EmissionResultKey erk = appContext.getUserContext().getEmissionResultKey();
      final ScenarioBaseUserContext uc = appContext.getUserContext();
      uc.setEmissionResultKey(EmissionResultKey.safeValueOf(erk.getSubstance(), ert));
      eventBus.fireEvent(new EmissionResultKeyChangeEvent(uc.getEmissionResultKey()));
    }
  };

  private Substance selectedSubstance;

  public ScenarioBaseTaskBarImpl(final ScenarioBaseAppContext<? extends ScenarioBaseContext, ? extends ScenarioBaseUserContext> appContext,
      final EventBus eventBus, final HelpPopupController hpC, final InfoPanelPresenter infoPanelPresenter, final LayerPanel layerPanel) {
    this.appContext = appContext;
    this.eventBus = eventBus;
    this.hpC = hpC;
    this.infoPanel = infoPanelPresenter.getView();
    this.layerPanel = layerPanel;

    eventBinder.bindEventHandlers(this, eventBus);
  }

  private void updateContext(final Substance substance) {
    selectedSubstance = substance;
    appContext.getUserContext().getEmissionValueKey().setSubstance(substance);
    final EmissionResultKey erk = appContext.getUserContext().getEmissionResultKey();
    appContext.getUserContext().setEmissionResultKey(EmissionResultKey.safeValueOf(substance, erk.getEmissionResultType()));
    eventBus.fireEvent(new EmissionResultKeyChangeEvent(appContext.getUserContext().getEmissionResultKey()));
  }

  @Override
  public void initWidget(final Widget widget) {
    super.initWidget(widget);

    substanceSelection.getPopupPanel().addStyleName(style.substanceSelectionPulldown());

    initYearListBoxes(appContext.getContext());

    hpC.addWidget(infoPanelButton, hpC.tt().ttInfoPanel());
    hpC.addWidget(layerPanelButton, hpC.tt().ttLayerPanel());
    hpC.addWidget(yearSelection, hpC.tt().ttYearSelection());
    hpC.addWidget(substanceSelection, hpC.tt().ttSubstanceSelection());
    hpC.addWidget(resultTypeSelection, hpC.tt().ttResultTypeSelection());

    infoPanelButton.ensureDebugId(TestID.BUTTON_INFOPANEL);
    layerPanelButton.ensureDebugId(TestID.BUTTON_LAYERPANEL);
    yearSelection.ensureDebugId(TestID.BUTTON_YEARSELECTION);
    substanceSelection.ensureDebugId(TestID.BUTTON_SUBSTANCESELECTION);
    resultTypeSelection.ensureDebugId(TestID.BUTTON_RESULTTYPESELECTION);
    rtDepositionLabel.ensureDebugId(TestID.BUTTON_RESULTTYPE_DEPOSITION);
    rtConcentrationLabel.ensureDebugId(TestID.BUTTON_RESULTTYPE_CONCENTRATION);
    rtNumberOfDaysLabel.ensureDebugId(TestID.BUTTON_RESULTTYPE_NUM_OF_DAYS);
  }

  private void initYearListBoxes(final ScenarioBaseContext context) {
    final int firstYear = context.getSetting(SharedConstantsEnum.MIN_YEAR);
    final int lastYear = context.getSetting(SharedConstantsEnum.MAX_YEAR);
    for (int i = firstYear; i <= lastYear; i++) {
      final Label item = new Label(String.valueOf(i));

      item.ensureDebugId(TestID.LABEL_YEARSELECTION + "-" + i);
      item.addStyleName(R.css().number());
      item.addClickHandler(yearClickHandler);
      yearSelection.getListPanel().add(item);
    }

    yearSelection.getPopupPanel().addStyleName(R.css().buttonYearSelection());

    rtDepositionLabel.addClickHandler(rtClickHandler);
    rtConcentrationLabel.addClickHandler(rtClickHandler);
    rtNumberOfDaysLabel.addClickHandler(rtClickHandler);
  }

  private void initSubstanceListBox(final EmissionResultType selectedType) {
    substanceSelection.getListPanel().clear();
    for (final HandlerRegistration handler : handlers) {
      handler.removeHandler();
    }

    final Set<Substance> substances = new LinkedHashSet<>();
    for (final EmissionResultKey key : theme.getSubstancesEmissionType()) {
      if (key.getEmissionResultType() == selectedType) {
        substances.add(key.getSubstance());
      }
    }

    if (!substances.contains(selectedSubstance)) {
      selectedSubstance = null;
    }

    for (final Substance s : substances) {
      if (selectedSubstance == null) {
        selectedSubstance = s;
      }
      final Label item = new Label(s.getName());

      UIObject.ensureDebugId(item.getElement(), TestID.LABEL_SUBSTANCESELECTION, s.getName());
      item.addStyleName(R.css().listButtonItem());
      handlers.add(item.addClickHandler(substanceClickHandler));
      substanceSelection.getListPanel().add(item);
    }

    appContext.getUserContext().getEmissionValueKey().setSubstance(selectedSubstance);
    updateContext(selectedSubstance);
  }

  @EventHandler
  void onYearChange(final YearChangeEvent e) {
    yearSelection.setText(String.valueOf(e.getValue()));
  }

  @EventHandler
  void onEmissionResultKeyChange(final EmissionResultKeyChangeEvent e) {
    updateSubstanceAndResultType(e.getValue().getSubstance(), e.getValue().getEmissionResultType());
  }

  private void updateSubstanceAndResultType(final Substance substance, final EmissionResultType ert) {
    substanceSelection.setText(substance == null ? "" : substance.getName());
    final EmissionResultType actualEmissionResultType = getEmissionResultTypeAndSetKey(substance, ert);
    if (actualEmissionResultType == null) {
      return; // just ignore if no emission result type could be found.
    }
    if (actualEmissionResultType == ert) {
      resultTypeSelection.setText(actualEmissionResultType.toString());
      fillResultTypeBoxData(substance);
      resultTypeSelection.setVisible(theme.hasMultipleResultTypes());
      updateResultTypeSelectionText(actualEmissionResultType);
    } else {
      appContext.getUserContext().setEmissionResultKey(
          EmissionResultKey.safeValueOf(appContext.getUserContext().getEmissionResultKey().getSubstance(), actualEmissionResultType));

      // do deferred because this method is called from within another event, cause it to be handled in the wrong order.
      Scheduler.get().scheduleDeferred(new ScheduledCommand() {
        @Override
        public void execute() {
          eventBus.fireEvent(new EmissionResultKeyChangeEvent(appContext.getUserContext().getEmissionResultKey()));
        }
      });
    }
  }

  /**
   * Gets a valid {@link EmissionResultType} given a substance and theme. When a new substance and emissionResultType is given the combination should
   * also be supported by the theme.
   *
   * @param substance new substance
   * @param ert new emissionResultType
   * @return the actual emissionResultType set
   */
  private EmissionResultType getEmissionResultTypeAndSetKey(final Substance substance, final EmissionResultType ert) {
    final EmissionResultKey erk = EmissionResultKey.valueOf(substance, ert);
    EmissionResultType result = null;
    for (final EmissionResultKey resultKey : theme.getSubstancesEmissionType()) {
      if (erk == resultKey) {
        return erk.getEmissionResultType();
      }
    }
    for (final EmissionResultKey resultKey : theme.getSubstancesEmissionType()) {
      if (resultKey.getSubstance().contains(substance)) {
        result = resultKey.getEmissionResultType();
        break;
      }
    }
    return result;
  }

  private void fillResultTypeBoxData(final Substance substance) {
    resultTypeSelection.getListPanel().clear();
    boolean concentrateLabelExist = false;
    boolean depositionLabelExist = false;
    boolean numberOfDaysLabelExist = false;
    for (final EmissionResultKey resultKey : theme.getSubstancesEmissionType()) {
      if (resultKey.getSubstance().containsCurrentOrGiven(substance)) {
        switch (resultKey.getEmissionResultType()) {
        case CONCENTRATION:
          if (!concentrateLabelExist) {
            resultTypeSelection.getListPanel().add(rtConcentrationLabel);
            concentrateLabelExist = true;
          }
          break;
        case DEPOSITION:
          if (!depositionLabelExist) {
            resultTypeSelection.getListPanel().add(rtDepositionLabel);
            depositionLabelExist = true;
          }
          break;
        case NUM_EXCESS_DAY:
          if (!numberOfDaysLabelExist) {
            resultTypeSelection.getListPanel().add(rtNumberOfDaysLabel);
            numberOfDaysLabelExist = true;
          }
          break;
        default:
          break;
        }
      }
    }
  }

  private void updateResultTypeSelectionText(final EmissionResultType ert) {
    resultTypeSelection.setText(M.messages().emissionResultTypeChar(ert));
  }

  @Override
  public void setYearCaption(final int year) {
    yearSelection.setText(String.valueOf(year));
  }

  @Override
  public void setTheme(final ScenarioBaseTheme theme, final EmissionResultType ert) {
    this.theme = theme;
    SchedulerImpl.get().scheduleDeferred(new Command() {
      @Override
      public void execute() {
        initSubstanceListBox(ert);
      }
    });
  }

}
