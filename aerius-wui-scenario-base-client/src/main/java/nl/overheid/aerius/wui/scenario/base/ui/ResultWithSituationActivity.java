/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.event.EmissionSourceListNameChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationSummaryChangedEvent;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * An activity that shows the 'result' taskbar, and the situation activity's
 * situation tabs all together.
 */
abstract class ResultWithSituationActivity extends ResultActivity implements ResultView.Presenter {
  interface ResultWithSituationActivityEventBinder extends EventBinder<ResultWithSituationActivity> { }

  private static final ResultWithSituationActivityEventBinder EVENT_BINDER = GWT.create(ResultWithSituationActivityEventBinder.class);

  private final SituationActivity situationActivity;


  public ResultWithSituationActivity(final PlaceController placeController, final CalculationController calculatorController,
      final ScenarioBaseAppContext<?, ?> appContext, final ScenarioBasePlace place, final SituationView situationView,
      final ResultView resultView, final ResultPlace.ScenarioBaseResultPlaceState resultPlaceState, final ScenarioBaseMapLayoutPanel map) {
    super(resultView, appContext, placeController, calculatorController, resultPlaceState, map);
    situationActivity = new SituationActivity(placeController, calculatorController, appContext, place, situationView) {
      @Override
      public void startChild(final AcceptsOneWidget panel, final EventBus eventBus) {
        startSituationChild(panel, eventBus);
        setCalculationButtonVisible(false);
        situationView.setEditable(false);

        initEventBinders(eventBus);
      }

      @Override
      protected void refresh(final Situation sit, final EmissionValueKey key, final EmissionResultKey resultKey) {
        ResultWithSituationActivity.this.refresh(sit, key, resultKey);
      }
    };
  }

  @Override
  public final void startChild(final AcceptsOneWidget panel, final EventBus eventBus) {
    situationActivity.start(panel, eventBus);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  protected abstract void startSituationChild(final AcceptsOneWidget panel, final EventBus eventBus);

  @Override
  @EventHandler
  public void onCalculationSummaryChanged(final CalculationSummaryChangedEvent event) {
    super.onCalculationSummaryChanged(event);
    situationActivity.refresh();
  }

  @EventHandler
  void onESLNameChange(final EmissionSourceListNameChangeEvent event) {
    situationActivity.refresh();
  }

  protected abstract void refresh(final Situation sit, final EmissionValueKey key, final EmissionResultKey resultKey);

  @Override
  public void setVisibleHabitatLayer(final boolean visible) {
    super.setVisibleHabitatLayer(visible);
  }

  protected String getNoResultText(final ScenarioBasePlace place, final Situation sit, final EmissionValueKey key, final EmissionResultKey resultKey,
      final CalculatedScenario calcScenario) {
    final String noResultText;
    if (calcScenario == null) {
      noResultText = M.messages().calculationTableNoData();
    } else {
      final int activeCalculationId = calcScenario.getCalculationId(place.getActiveSituationId());
      final CalculationSummary summary = appContext.getCalculationSummary();
      if (!calcScenario.isInSync() || activeCalculationId == 0 || summary == null || calcScenario.getCalculation(activeCalculationId) == null) {
        noResultText = M.messages().calculationTableNoData();
      } else if (key.getYear() != calcScenario.getCalculation(activeCalculationId).getYear()) {
        noResultText = M.messages().calculationTableNoDataYear(key.getYear());
      } else if (!Substance.containsOrPartOf(calcScenario.getCalculation(activeCalculationId).getOptions().getSubstances(),
          resultKey.getSubstance())) {
        noResultText = M.messages().calculationTableNoDataSubstance(resultKey.getSubstance().getName());
      } else if (calcScenario.getOptions().getCalculationType() == CalculationType.CUSTOM_POINTS) {
        noResultText = null; // if custom points don't check for other types of input here.
      } else if (summary.getSummaryResult().isEmpty()) {
        noResultText = M.messages().calculationTableNoResultsInRange();
      } else {
        noResultText = null;
      }
    }
    return noResultText;
  }
}
