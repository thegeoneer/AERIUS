/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasName;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.ShippingCategory;
import nl.overheid.aerius.shared.domain.source.EmissionSubSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.ShippingEmissionSource;
import nl.overheid.aerius.shared.domain.source.VesselGroupEmissionSubSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.DivTableKeyValue;
import nl.overheid.aerius.wui.main.widget.table.TextKeyValueDivTable;

public class ShippingEmissionViewer extends EmissionViewer<ShippingEmissionSource<? extends VesselGroupEmissionSubSource<? extends ShippingCategory>>> {
  public static final ShippingEmissionViewerUiBinder uiBinder = GWT.create(ShippingEmissionViewerUiBinder.class);

  @UiTemplate("KeyValueTableViewer.ui.xml")
  interface ShippingEmissionViewerUiBinder extends UiBinder<Widget, ShippingEmissionViewer> {}

  interface ShippingEmissionDriver extends SimpleBeanEditorDriver<ShippingEmissionSource<? extends VesselGroupEmissionSubSource<? extends ShippingCategory>>, ShippingEmissionViewer> {}

  @UiField Label header;
  @UiField Label headerRight;
  @UiField TextKeyValueDivTable table;

  public ShippingEmissionViewer() {
    initWidget(uiBinder.createAndBindUi(this));

    table.ensureDebugId(TestID.TABLE_SOURCES_SECTOR_VIEWER);

    header.setText(M.messages().emissionSourceViewerShippingTitle());
    headerRight.setText(M.messages().emissionSourceViewerShippingUnit());
  }

  @Override
  public void setValue(final ShippingEmissionSource<?> value) {
    if (value instanceof InlandRouteEmissionSource && ((InlandRouteEmissionSource) value).getInlandWaterwayType() != null
        && ((InlandRouteEmissionSource) value).getInlandWaterwayType().getWaterwayCategory() != null) {
      header.setText(M.messages().emissionSourceViewerShippingWaterWayTitle(
          ((InlandRouteEmissionSource) value).getInlandWaterwayType().getWaterwayCategory().getDescription()));
    }
    table.clear();

    final EmissionValueKey emissionValueKey = new EmissionValueKey(year, Substance.NOX);

    for (final EmissionSubSource item : value.getEmissionSubSources()) {
      addValue(((HasName) item).getName(), value.getEmission(item, emissionValueKey));
    }
  }

  private void addValue(final String key, final double value) {
    table.addRowData(new DivTableKeyValue<String, String>(key, FormatUtil.formatEmissionWithUnit(value)));
  }
}
