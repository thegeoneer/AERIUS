/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.adapters.ListEditor;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.sector.ShippingNode;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayTypeUtil;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.domain.source.ShippingRoute.ShippingRouteReference;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DynamicRowEditor;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.NotZeroValidator;
import nl.overheid.aerius.wui.main.ui.validation.PercentageRangeValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.ListButton;
import nl.overheid.aerius.wui.scenario.base.source.shipping.InlandMooringEmissionEditor.ShipRouteDriver;
import nl.overheid.aerius.wui.scenario.base.source.shipping.ShipRouteEditorHandlerImpl.EnableButtons;

/**
 * Row editor for {@link InlandMooringRoute} data.
 */
class InlandMooringRowEditor extends DynamicRowEditor<InlandMooringRoute> implements HasValueChangeHandlers<Integer> {

  private static final InlandMooringRowEditorUiBinder UI_BINDER = GWT.create(InlandMooringRowEditorUiBinder.class);

  interface InlandMooringRowEditorUiBinder extends UiBinder<Widget, InlandMooringRowEditor> { }

  @UiField(provided = true) ListButton inlandRouteBox;
  RouteLeafValueEditor routeEditor;
  @UiField IntValueBox shipMovementsPerTimeUnit;
  @UiField IntValueBox percentageLadenEditor;
  @UiField(provided = true) ListBoxEditor<TimeUnit>  timeUnit;

  private final ShipRouteDriver routesDriver = GWT.create(ShipRouteDriver.class);
  private Point dockingPoint;

  public InlandMooringRowEditor(final EventBus eventBus, final MapLayoutPanel map, final InlandShippingCategories inlandShippingCategories,
      final HelpPopupController hpC, final ShippingRouteHandler inlandRouteHandler, final EnableButtons enableButtons) {
    super(hpC, hpC.tt().ttDeleteButtonHelp());
    inlandRouteBox = new ListButton(M.messages().shipRouteShort());
    timeUnit = new ListBoxEditor<TimeUnit>() {
      @Override
      protected String getLabel(final TimeUnit value) {
        return M.messages().timeUnit(value);
      }

    };

    timeUnit.addItems(TimeUnit.values());
    getRow().insert(UI_BINDER.createAndBindUi(this), 0);

    routeEditor = new RouteLeafValueEditor(inlandRouteBox, M.messages().shipInlandRouteSelect(),
        ShipRouteEditorSource.INLAND_WATERS_SHIP_ROUTE_NAME);
    final ShipRouteEditorSource inlandRouteEditorSource = new ShipRouteEditorSource(eventBus, map, inlandRouteBox,
        new ArrayList<ShippingNode>(), ShipRouteEditorSource.INLAND_WATERS_SHIP_ROUTE_NAME);
    routesDriver.initialize(ListEditor.of(inlandRouteEditorSource));
    inlandRouteEditorSource.setRouteChangedHandler(
        new ShipRouteEditorHandlerImpl(inlandRouteBox, routeEditor, inlandRouteHandler, enableButtons) {
          @Override
          public void onAdd(final ShippingRoute route) {
            super.onAdd(route);
            resetPlaceHolders();
          }
          @Override
          public void onSelect(final ShippingRoute route) {
            getShadowValue().setRoute(route);
            super.onSelect(route);
          }

          @Override
          public void onRemove(final ShippingRoute route) {
            super.onRemove(route);
            // call setRoute after onRemove because setRoute will remove relation between route and ship
            // end then it's not possible to check if this route was only used by this ship.
            getShadowValue().setRoute(null);
          }

          @Override
          public Point onStartDrawNewRoute() {
            super.onStartDrawNewRoute();
            return dockingPoint;
          }

          @Override
          public ShippingRouteReference getRouteRef() {
            return getShadowValue();
          }
        });
    shipMovementsPerTimeUnit.addKeyUpHandler(this);
    shipMovementsPerTimeUnit.addValidator(new NotZeroValidator<Integer>() {
      @Override
      public String validate(final Integer value) {
        return routeEditor.getValue() == null ? null : super.validate(value);
      }
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivInlandShipMovementsNotEmpty();
      }
    });
    percentageLadenEditor.addKeyUpHandler(this);
    percentageLadenEditor.addValidator(new PercentageRangeValidator() {
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivInlandShippingPercentageLaden(value);
      }
    });
  }

  @Override
  protected boolean isEmpty() {
    return routeEditor.getValue() == null && shipMovementsPerTimeUnit.getValue() == 0;
  }

  @Override
  protected void resetPlaceHolders() {
    super.resetPlaceHolders();
    shipMovementsPerTimeUnit.resetPlaceHolder();
    percentageLadenEditor.resetPlaceHolder();
  }

  @Override
  public void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    ensureDebugId(getElement(), baseID, TestID.PANEL_INLAND_MOORING_ROUTES);
    ensureDebugId(inlandRouteBox.getElement(), baseID, TestID.BUTTON_SHIPPING_INLAND_ROUTES);
    ensureDebugId(shipMovementsPerTimeUnit.getElement(), baseID, TestID.INPUT_SHIPPING_MOVEMENTS);
    ensureDebugId(percentageLadenEditor.getElement(), baseID, TestID.INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN);
    ensureDebugId(timeUnit.getElement(), baseID, TestID.LIST_TIMEUNIT);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Integer> handler) {
    return shipMovementsPerTimeUnit.addValueChangeHandler(handler);
  }

  @Override
  public void flush() {
    super.flush();
    final EditorDelegate<InlandMooringRoute> delegate = getDelegate();
    if (delegate != null && routeEditor.getValue() != null) {
      if (shipMovementsPerTimeUnit.getValue() == 0) {
        delegate.recordError(M.messages().ivInlandShipMovementsNotEmpty(), null, shipMovementsPerTimeUnit);
      }
      if (!InlandWaterwayTypeUtil.isDirectionCorrect(routeEditor.getValue().getInlandWaterwayType())) {
        delegate.recordError(M.messages().ivInlandShipDirectionNotEmpty(), null, inlandRouteBox);
      }
    }
  }

  public void setDockingPoint(final Point dockingPoint) {
    this.dockingPoint = dockingPoint;
  }

  public void enableButtons(final boolean enabled) {
    inlandRouteBox.setEnabled(enabled);
  }

  public void setRoutes(final List<ShippingRoute> routes) {
    routesDriver.edit(routes);
  }
}
