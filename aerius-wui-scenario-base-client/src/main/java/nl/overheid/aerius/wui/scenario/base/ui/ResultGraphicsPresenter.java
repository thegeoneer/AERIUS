/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.progress.ProgressPanel;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ResultGraphicsPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.ui.ResultGraphicsView.Presenter;

/**
 * Presenter for Progress View.
 */
public class ResultGraphicsPresenter extends ResultActivity implements Presenter {
  interface ResultGraphicsEventBinder extends EventBinder<ResultGraphicsPresenter> {}

  private final ResultGraphicsEventBinder eventBinder = GWT.create(ResultGraphicsEventBinder.class);

  private final ResultGraphicsView view;

  private final ArrayList<ProgressPanel> progressPanels = new ArrayList<>();

  private final ScenarioBaseAppContext<?, ?> appContext;

  private final HelpPopupController hpC;

  @Inject
  public ResultGraphicsPresenter(final PlaceController placeController, final CalculationController calculatorController,
      final ScenarioBaseAppContext<?, ?> appContext, @Assisted final ResultGraphicsPlace place,
      final ResultGraphicsView view, final ResultView resultView, final ScenarioBaseMapLayoutPanel map, final HelpPopupController hpC) {
    super(resultView, appContext, placeController, calculatorController, ResultPlace.ScenarioBaseResultPlaceState.GRAPHICS, map);
    this.appContext = appContext;
    this.view = view;
    this.hpC = hpC;
  }

  @Override
  public void startChild(final AcceptsOneWidget panel, final EventBus eventBus) {
    panel.setWidget(view);
    eventBinder.bindEventHandlers(this, eventBus);
    view.refresh(appContext.getUserContext().getCalculatedScenario(), appContext.isCalculationRunning(), appContext.isCalculationCancelled());
  }

  @EventHandler
  void onYearChange(final YearChangeEvent substance) {
    view.refresh(appContext.getUserContext().getCalculatedScenario(), appContext.isCalculationRunning(), appContext.isCalculationCancelled());
  }

  @EventHandler
  void onEmissionResultKeyChange(final EmissionResultKeyChangeEvent event) {
    view.refresh(appContext.getUserContext().getCalculatedScenario(), appContext.isCalculationRunning(), appContext.isCalculationCancelled());
  }

  @Override
  void onStartCalculation(final CalculationStartEvent event) {
    super.onStartCalculation(event);
    view.startCalculation();
  }

  @EventHandler
  void onInitCalculation(final CalculationInitEvent event) {
    final int panelCount = event.getInitResult().getCalculations().size();

    for (final Calculation cesl : event.getInitResult().getCalculations()) {
      final EmissionSourceList sources = cesl.getSources() == null ? null : appContext.getUserContext().getSources(cesl.getSources().getId());

      final String name = sources == null ? FormatUtil.formatDate(cesl.getCreationDate()) : sources.getName();

      // Have the view add a panel, use the global event bus not the presenter bus as that one is reset after this presenter is closed and we need
      // to continue receiving events on the progress bars themselves after this presenter is closed.
      final ProgressPanel panel = view.addProgressPanel(name, cesl.getCalculationId());
      progressPanels.add(panel);
      // Calculation maximum range is in meters, panel uses kilometers.
      setMaxDistance(event, panel);

      if (panelCount > 1) {
        hpC.addWidget(panel, hpC.tt().ttSituationComparisonDeposition());
      }
    }
  }

  private void setMaxDistance(final CalculationInitEvent event, final ProgressPanel panel) {
    panel.setMaxDistance(event.getInitResult().getCalculationSetOptions().isMaximumRangeRelevant()
        ? (int) Math.ceil(event.getInitResult().getCalculationSetOptions().getCalculateMaximumRange()
            / SharedConstants.M_TO_KM) : 0);
  }

  @Override
  void onCancelCalculation(final CalculationCancelEvent event) {
    super.onCancelCalculation(event);
    view.finishCalculation(true);
  }

  @Override
  void onFinishCalculation(final CalculationFinishEvent event) {
    super.onFinishCalculation(event);
    view.finishCalculation(false);
  }
}
