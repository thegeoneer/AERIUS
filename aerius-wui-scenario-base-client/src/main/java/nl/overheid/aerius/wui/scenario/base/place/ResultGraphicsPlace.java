/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

/**
 * Place for Calculator results displayed as table.
 */
public class ResultGraphicsPlace extends ResultPlace {
  private static final String PREFIX = "rg";

  /**
   * Tokenizer for {@link ResultGraphicsPlace}.
   */
  @Prefix(PREFIX)
  public static class Tokenizer extends ScenarioBasePlace.Tokenizer<ResultGraphicsPlace> implements PlaceTokenizer<ResultGraphicsPlace> {
    @Override
    protected ResultGraphicsPlace createPlace() {
      return new ResultGraphicsPlace();
    }
  }

  public ResultGraphicsPlace(final Place currentPlace) {
    super(currentPlace);
  }

  ResultGraphicsPlace() {}

  @Override
  public ResultGraphicsPlace copy() {
    return copyTo(new ResultGraphicsPlace());
  }
}
