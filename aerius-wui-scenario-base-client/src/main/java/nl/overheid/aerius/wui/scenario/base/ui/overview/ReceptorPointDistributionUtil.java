/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import nl.overheid.aerius.shared.domain.Range;
import nl.overheid.aerius.shared.domain.deposition.HabitatFilterRangeContent;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 *
 */
final class ReceptorPointDistributionUtil {

  /**
   * Interface to implement when using this util.
   * @param <T> The type of ranges to distribute.
   */
  public interface RangeDistributor<T extends Range> {

    /**
     * @return A list of all possible ranges.
     */
    ArrayList<T> getPossibleRanges();

    /**
     * @param rp The AeriusPoint to determine the range for.
     * @return The range that the AeriusPoint belongs to.
     */
    T getRange(AeriusResultPoint rp);
  }

  private static final Comparator<Range> RANGE_COMPARATOR = new Comparator<Range>() {
    @Override
    public int compare(final Range o1, final Range o2) {
      return -1 * Double.compare(o1.getLowerValue(),  o2.getLowerValue());
    }
  };


  private ReceptorPointDistributionUtil() {}

  /**
   * @param filtered The list of receptors (already filtered by habitat if needed).
   * @param key The key to use when filtering.
   * @param distributor The distributor to use when determining ranges.
   * @param <T> The type of range to get ranges for.
   * @return The map of range, content values.
   */
  public static <T extends Range> Map<T, HabitatFilterRangeContent> getRanges(
      final HashMap<AeriusResultPoint, Double> filtered, final EmissionResultKey key, final RangeDistributor<T> distributor) {
    // Create and fill the map with all DepositionEnum values
    final TreeMap<T, HabitatFilterRangeContent> hm = new TreeMap<T, HabitatFilterRangeContent>(RANGE_COMPARATOR);

    // Fill with keys
    for (final T range : distributor.getPossibleRanges()) {
      hm.put(range, new HabitatFilterRangeContent());
    }

    // Distribute receptors in ranges
    for (final Entry<AeriusResultPoint, Double> entry : filtered.entrySet()) {
      final HabitatFilterRangeContent content = hm.get(distributor.getRange(entry.getKey()));
      content.setNumberOfReceptors(content.getNumberOfReceptors() + 1);
      content.setSurface(content.getSurface() + entry.getValue());
    }
    return hm;
  }
}
