/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.events;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.wui.main.event.SimpleGenericEvent;

/**
 * Represents an Event telling listeners the imported IMAER features have changed.
 */
public class ImportedImaerFeaturesChangeEvent extends SimpleGenericEvent<ArrayList<EmissionSource>> {
  /**
   * Creates a {@link ImportedImaerFeaturesChangeEvent}.
   *
   * @param result the result representing the change.
   */
  public ImportedImaerFeaturesChangeEvent(final ArrayList<EmissionSource> result) {
    super(result);
  }
}
