/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.HasDirection.Direction;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.HasDirectionalHtml;
import com.google.gwt.user.client.ui.HasDirectionalSafeHtml;
import com.google.gwt.user.client.ui.HasDirectionalText;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PopupPanel.AnimationType;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.widget.DoubleDecorationPanel;
import nl.overheid.aerius.wui.main.widget.SimpleTabPanel.CanHaveFocus;
import nl.overheid.aerius.wui.scenario.base.ui.SituationTabPopup.SituationPopupHandler;

/**
 * A single tab item that represents a situation.
 *
 * FIXME Mimicking all of InlineHTML's text interfaces may not be an excellent idea. (Not broken, could be neater architecturally)
 */
public class SituationTabItem extends DoubleDecorationPanel implements CanHaveFocus, HasDirectionalHtml,
HasDirectionalSafeHtml, HasText, HasDirectionalText, HasClickHandlers {
  /**
   * Handler for a tab item representing a situation.
   */
  public interface SituationTabHandler {
    /**
     * Delete the given situation tab item.
     *
     * @param item item to delete
     */
    void delete(SituationTabItem item);

    /**
     * Change the name of this situation tab.
     *
     * Use .getName to get the new name.
     *
     * @param item Tab item that's been changed
     */
    void changeName(SituationTabItem item);
  }

  private final InlineHTML label = new InlineHTML();

  private static final String DROP_DOWN = " " + SharedConstants.ARROW_DOWN_SMALL;

  private final class EventHandlers implements ClickHandler, SituationPopupHandler {
    @Override
    public void onClick(final ClickEvent event) {
      togglePopup();
    }

    @Override
    public void changeName(final String name) {
      SituationTabItem.this.name = name;

      if (tabHandler != null) {
        tabHandler.changeName(SituationTabItem.this);
      }
      togglePopup();
    }

    @Override
    public void delete() {
      if (canDelete && tabHandler != null) {
        pp.hide();
        tabHandler.delete(SituationTabItem.this);
      }
    }
  }

  private final PopupPanel pp = new PopupPanel(true);

  private final EventHandlers handlers = new EventHandlers();

  private final SituationTabPopup situationTabPopup;

  private HandlerRegistration clickHandlerRegistration;

  private SituationTabHandler tabHandler;

  private int id;

  private String name;

  private boolean canDelete;

  private boolean focus;


  /**
   * Creates a SituationTabItem with the specified text.
   *
   * @param text the new label's text
   */
  public SituationTabItem(final String text) {
    situationTabPopup = new SituationTabPopup(handlers);
    pp.setWidget(situationTabPopup);
    pp.addAutoHidePartner(getElement());
    pp.setAnimationType(AnimationType.ROLL_DOWN);

    setWidget(label);
    setName(text);

    this.label.ensureDebugId(TestID.TAB_SIT_LABEL + "-" + text);
  }

  private void togglePopup() {
    if (pp.isShowing()) {
      pp.hide();
    } else {
      pp.showRelativeTo(this);
    }
  }

  /**
   * Sets focus to this item, enabling editing and deleting (if allowed).
   *
   * @param focus true to set focus, false to remove it.
   */
  @Override
  public void setFocus(final boolean focus) {
    if (focus && clickHandlerRegistration == null) {
      clickHandlerRegistration = addClickHandler(handlers);
    } else if (!focus) {
      pp.hide();
      if (clickHandlerRegistration != null) {
        clickHandlerRegistration.removeHandler();
        clickHandlerRegistration = null;
      }
    }

    // Update focus and name
    this.focus = focus;
    setHTML(name);
  }

  public void setHandler(final SituationTabHandler handler) {
    this.tabHandler = handler;
  }

  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  @Override
  public void setHTML(final String text) {
    label.setHTML(SafeHtmlUtils.fromString(text).asString() + (focus ? DROP_DOWN : ""));
  }

  /**
   * Sets whether this tab item can be deleted.
   *
   * Saves the value for a check later on because not showing the delete button is not safe enough.
   *
   * @param canDelete true for yay, false for nay
   */
  public void setCanDelete(final boolean canDelete) {
    this.canDelete = canDelete;

    situationTabPopup.setCanDelete(canDelete);
  }

  /**
   * Sets this situation tab's name, handles propagation to the popup.
   *
   * @param name the new name
   */
  public void setName(final String name) {
    this.name = name;

    setHTML(name);
    situationTabPopup.setName(name);
  }

  @Override
  public String getHTML() {
    return label.getHTML();
  }

  @Override
  public void setHTML(final SafeHtml html) {
    label.setHTML(html);
  }

  @Override
  public Direction getTextDirection() {
    return label.getTextDirection();
  }

  @Override
  public void setText(final String text, final Direction dir) {
    label.setText(text, dir);
  }

  @Override
  public String getText() {
    return label.getText();
  }

  @Override
  public void setText(final String text) {
    label.setText(text);
  }

  @Override
  public void setHTML(final SafeHtml html, final Direction dir) {
    label.setHTML(html, dir);
  }

  @Override
  public void setHTML(final String html, final Direction dir) {
    label.setHTML(html, dir);
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return label.addClickHandler(handler);
  }
}
