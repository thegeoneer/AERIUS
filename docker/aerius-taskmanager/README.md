### Taskmanager image

The taskmanager jar should be placed in this directory as `app.jar` while building the image.

##### ENV variables that are required or set by default for convenience
- `DBNAME`: Defaults to `aerius`.
- `DBHOSTNAME`: Defaults to `localhost`.
- `DBUSERNAME`: Defaults to `aerius`.
- `DBPASSWORD`: Defaults to `aerius`.

##### Example build
```shell
docker build -t aerius-taskmanager:latest .
```

##### Example run
```shell
docker run --rm --network host \
  -e DBPASSWORD=password \
  aerius-taskmanager:latest
```
