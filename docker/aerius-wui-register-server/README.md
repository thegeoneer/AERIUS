### Register Web application image

The webapp war should be placed in this directory as `register.war` while building the image.

##### Example build
```shell
docker build -t aerius-wui-register-server:latest .
```

##### Example run
```shell
docker run --rm --network host \
  -e DBPASSWORD="password" \
  -p 8080:8080 \
  aerius-wui-register-server:latest
```

##### Example run with custom DBNAME
```shell
docker run --rm --network host \
  -e DBPASSWORD="password" \
  -e DBNAME="unittest_register" \
  -p 8080:8080 \
  aerius-wui-register-server:latest
```
