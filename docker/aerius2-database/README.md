### Base for DB images

Contains configurations that are the same for all DB images.
Only for use in other images.

##### Example build
```shell
docker build -t aerius2-database:latest .
```
