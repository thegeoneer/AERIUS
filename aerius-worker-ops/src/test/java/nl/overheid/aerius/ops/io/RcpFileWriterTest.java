/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.geo.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Test class for {@link RcpFileWriter}
 *
 */
public class RcpFileWriterTest {

  private static final String RECEPTORS1 = "receptor/receptor_1.rcp";
  private static final String RECEPTORS2 = "receptor/receptor_2.rcp";
  @Rule
  public final TemporaryFolder folder = new TemporaryFolder();

  @Test
  public void writeFileTest() throws IOException {
    final File path = folder.newFolder();

    RcpFileWriter.writeFile(path, createReceptorData());
    final String output =  OPSTestUtil.readFile(new File(path, RcpFileWriter.getFileName()));
    final String reference = OPSTestUtil.readFile(RcpFileWriter.class, RECEPTORS1);

    assertEquals("Read receptors file should be same as reference file.", reference, output);
  }

  @Test
  public void writeFileWithTerrainTest() throws IOException {
    final File path = folder.newFolder();
    final ArrayList<OPSReceptor> receptors = createReceptorData();

    double z0 = 1.23456789;
    int landuse = 0;
    for (final OPSReceptor receptor : receptors) {
      receptor.setAverageRoughness(z0);
      receptor.setLandUse(LandUse.values()[landuse++ % LandUse.values().length]);
      z0 += z0;
    }
    receptors.get(0).setLandUses(new int[] {60, 0, 0, 0, 0, 0, 0, 0, 40});

    RcpFileWriter.writeFile(path, receptors);
    final String output =  OPSTestUtil.readFile(new File(path, RcpFileWriter.getFileName()));
    final String reference = OPSTestUtil.readFile(RcpFileWriter.class, RECEPTORS2);

    assertEquals("Read receptors file with terrain data should be same as reference file.", reference, output);
  }

  public static ArrayList<OPSReceptor> createReceptorData() {
    final ArrayList<OPSReceptor> data = new ArrayList<>();

    create(data, 3310760, 106206, 419024);
    create(data, 3324315, 106485, 419507);
    create(data, 3303231, 106485, 418756);
    create(data, 3325821, 106392, 419561);
    create(data, 3316786, 106578, 419239);
    create(data, 3304735, 106020, 418809);
    create(data, 3304739, 106764, 418809);
    return data;
  }

  private static void create(final ArrayList<OPSReceptor> data, final int id, final int x, final int y) {
    data.add(new OPSReceptor(id, x, y));
  }

}
