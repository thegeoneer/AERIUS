/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.road;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.ops.BaseRunOPSTest;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.runner.OPSSingleSubstanceRunner;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link OPSRoadRunner}.
 */
public class OPSRoadRunnerTest extends BaseRunOPSTest {
  private static final double EMISSION_FACTOR = 100.0;

  private static final Logger LOG = LoggerFactory.getLogger(OPSRoadRunnerTest.class);

  @Mock
  private OPSSingleSubstanceRunner opsRunner;
  private OPSRoadRunner roadRunner;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    MockitoAnnotations.initMocks(this);
    roadRunner = new OPSRoadRunner(opsRunner);
  }

  @Test
  public void testRun() throws IOException, AeriusException {
    when(opsRunner.runSingleSubstance(any(), any(), any(), eq(YEAR), any())).then(new Answer<List<AeriusResultPoint>>() {
      @Override
      @SuppressWarnings("unchecked")
      public List<AeriusResultPoint> answer(final InvocationOnMock invocation) throws Throwable {
        final List<OPSSource> sources = invocation.getArgumentAt(0, List.class);
        final List<OPSReceptor> receptors = invocation.getArgumentAt(1, List.class);
        final Set<EmissionResultKey> erks = invocation.getArgumentAt(4, Set.class);
        final List<AeriusResultPoint> results =
            receptors.stream().map(r -> new AeriusResultPoint(r.getId(), r.getPointType(), r.getX(), r.getY()))
            .collect(Collectors.toList());

        results.forEach(r -> erks.forEach(key -> r.setEmissionResult(key, sources.size() * EMISSION_FACTOR)));
        return results;
      }
    });

    final Map<Substance, EnumSet<EmissionResultKey>> erkMap = new HashMap<>();
    erkMap.put(Substance.NOX, EnumSet.of(EmissionResultKey.NOX_DEPOSITION));
    erkMap.put(Substance.NH3, EnumSet.of(EmissionResultKey.NH3_DEPOSITION));

    final List<AeriusResultPoint> results = createResultPoints();
    final List<OPSSource> sources = createSources();

    results.forEach(r -> LOG.debug("Min distance for receptors:(x:{},y:{}) = {}, #{}", r.getRoundedX(), r.getRoundedY(),
        (int) sources.stream().min((s1, s2) -> Double.compare(s1.getPoint().distance(r), s2.getPoint().distance(r))).get().getPoint().distance(r),
        sources.stream().filter(s -> s.getPoint().distance(r) > 5000).count()));
    roadRunner.run(sources, result2Receptors(results), results, Substance.NOXNH3.hatch(), YEAR, erkMap);
    results.forEach(r -> r.getEmissionResults().getHashMap()
        .forEach((k,v) -> LOG.info("Result (x:{},y:{}) {} = {}", r.getRoundedX(), r.getRoundedY(), k, v)));
    results.forEach(r -> {
        final int range = sources.stream().anyMatch(s-> s.getPoint().distance(r) <= 5000) ? 5000 : 5000;
        assertEquals("Number of receptors > 5km should match emission passed by runner - x:"
            + r.getRoundedX() + ", y:" + r.getRoundedY(),
            sources.stream().filter(s -> s.getPoint().distance(r) > range).count() + 1,
            (int) (r.getEmissionResult(EmissionResultKey.NOX_DEPOSITION) / EMISSION_FACTOR));
        });
  }

  private List<OPSSource> createSources() {
    final List<OPSSource> sources = new ArrayList<>();
    // =< 3km
    addSource(sources, 10000, 10000);
    addSource(sources, 10040, 10050);
    addSource(sources, 10080, 10250);
    // > 3km && =< 5km
    addSource(sources, 11500, 13500);
    // > 5km
    addSource(sources, 20000, 20000);
    return sources;
  }

  private List<AeriusResultPoint> createResultPoints() {
    final List<AeriusResultPoint> points = new ArrayList<>();

    addResultPoint(points, 20010, 20010);
    addResultPoint(points, 20030, 20040);
    addResultPoint(points, 11510, 13510);
    addResultPoint(points, 11530, 13530);
    addResultPoint(points, 10030, 10080);
    addResultPoint(points, 10050, 10070);
    addResultPoint(points, 8500, 6500);
    addResultPoint(points, 8530, 6300);
    addResultPoint(points, 50, 50);
    addResultPoint(points, 80, 150);
    return points;
  }

  private void addSource(final List<OPSSource> sources, final double x, final double y) {
    sources.add(new OPSSource(sources.size(), RDNew.SRID, x, y));
  }

  private void addResultPoint(final List<AeriusResultPoint> points, final double x, final double y) {
    final AeriusResultPoint point = new AeriusResultPoint(points.size(), AeriusPointType.POINT, x, y);
    point.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, EMISSION_FACTOR);
    point.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, EMISSION_FACTOR);
    points.add(point);
  }

  private Collection<OPSReceptor> result2Receptors(final List<AeriusResultPoint> results) {
    return results.stream().map(OPSReceptor::new).collect(Collectors.toList());
  }
}
