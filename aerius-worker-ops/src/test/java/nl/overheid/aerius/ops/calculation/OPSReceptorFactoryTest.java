/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.shared.domain.geo.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link OPSReceptorFactory}.
 */
public class OPSReceptorFactoryTest {

  private OPSReceptor point;

  @Before
  public void before() {
    point = new OPSReceptor(1, 1000, 2000);
  }

  @Test
  public void testCreateEmpty() throws SQLException, AeriusException {
    point = OPSReceptorFactory.createReceptor(point);
    assertEquals("Receptor X value.", 1000, point.getRoundedX());
    assertEquals("Receptor Y value.", 2000, point.getRoundedY());
    assertEquals("Receptor ID.", 1, point.getId());
    assertNull("Recepter has no average roughness.", point.getAverageRoughness());
    assertNull("Receptor has no dominant land use.", point.getLandUse());
    assertNull("Receptor has no land use fractions.", point.getLandUses());
  }

  @Test
  public void testCreateWithOnlyAvRoughness() throws SQLException, AeriusException {
    //converted avg roughness and landuse should be null when supplied avg roughness and/or landuse supplied are null
    point.setAverageRoughness(1.1);
    point = OPSReceptorFactory.createReceptor(point);
    assertNull("Recepter has no average roughness.", point.getAverageRoughness());
    assertNull("Receptor has no dominant land use.", point.getLandUse());
    assertNull("Receptor has no land use fractions.", point.getLandUses());
  }

  @Test
  public void testCreateWithOnlyLandUse() throws SQLException, AeriusException {
    point.setAverageRoughness(null);
    point.setLandUse(LandUse.DECIDUOUS_FOREST);
    point = OPSReceptorFactory.createReceptor(point);
    assertNull("Recepter has no average roughness.", point.getAverageRoughness());
    assertNull("Receptor has no dominant land use.", point.getLandUse());
    assertNull("Receptor has no land use fractions.", point.getLandUses());
  }

  @Test
  public void testCreateWithLandUseAndAv() throws SQLException, AeriusException {
    point.setAverageRoughness(1.1);
    point.setLandUse(LandUse.DECIDUOUS_FOREST);
    point = OPSReceptorFactory.createReceptor(point);
    assertEquals("Receptor average roughness.", 1.1, point.getAverageRoughness().doubleValue(), 0.0001);
    assertEquals("Receptor dominant land use.", LandUse.DECIDUOUS_FOREST.getOption(), point.getLandUse().getOption());
    assertNull("Receptor has no land use fractions.", point.getLandUses());
  }

  @Test
  public void testCreateFull() throws SQLException, AeriusException {
    point.setAverageRoughness(1.1);
    point.setLandUse(LandUse.DECIDUOUS_FOREST);
    point.setLandUses(new int[] {20, 20, 30, 30, 0, 0, 0, 0, 0});
    point = OPSReceptorFactory.createReceptor(point);
    assertEquals("Receptor average roughness.", 1.1, point.getAverageRoughness().doubleValue(), 0.0001);
    assertEquals("Receptor dominant land use.", LandUse.DECIDUOUS_FOREST.getOption(), point.getLandUse().getOption());
    assertEquals("Receptor first land use fraction.", 20, point.getLandUses()[0]);
    assertEquals("Receptor last land use fraction.", 0, point.getLandUses()[8]);
  }
}
