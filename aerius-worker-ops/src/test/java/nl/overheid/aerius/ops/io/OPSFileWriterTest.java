/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.junit.Test;

import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Test class for {@link OPSFileWriter}.
 */
public class OPSFileWriterTest {

  @Test
  public void testWriteFiles() throws IOException {
    final File tempDir = Files.createTempDirectory("OPSFileWriterTest").toFile();
    final OPSConfiguration cfo = new OPSConfiguration();
    cfo.setOpsRoot(new File("[OPS_root]"));
    cfo.setRunFilesDirectory(tempDir);
    final OPSFileWriter fileWriter = new OPSFileWriter(cfo);
    final String runId = "test";
    final OPSInputData data = OPSTestUtil.getInputTestData(EmissionResultKey.NH3_CONCENTRATION, 2020);

    fileWriter.writeFiles(runId, data.getEmissionSources(), data.getReceptors(), Substance.NH3, 2020);
    assertTrue("Run ID directory should exist.", fileWriter.getRunDirectory(runId).exists());
    assertEquals("Should be 3 files created", 3, fileWriter.getRunDirectory(runId).listFiles().length);
  }
}
