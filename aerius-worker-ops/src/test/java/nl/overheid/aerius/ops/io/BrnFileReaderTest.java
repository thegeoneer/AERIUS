/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WGS84;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.domain.OPSSource.OPSSourceOutflowDirection;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for BrnFileReaders.
 */
public class BrnFileReaderTest {

  private static final double EPSILON = 1E-5;

  private static final String RESOURCES_DIRECTORY = "source/";
  private static final String USED_EXTENSION = ".brn";
  private static final String BRN_V0_STYLE_FILE = "emission_v0";
  private static final String BRN_V1_STYLE_FILE = "emission_v1";
  private static final String BRN_V4_STYLE_FILE = "emission_v4";
  private static final String EMISSION_FAIL_FILE = "emission_fail";

  @Test
  public void testParseOPSv0File() throws IOException {
    assertParseRegularOPSFile(new BrnFileReader(Substance.NH3), BRN_V0_STYLE_FILE);
  }

  @Test
  public void testParseOPSv1File() throws IOException {
    assertParseRegularOPSFile(new BrnFileReader(Substance.NH3), BRN_V1_STYLE_FILE);
  }

  @Test
  public void testParseOPSv4File() throws IOException {
    assertParseRegularOPSFile(new BrnFileReader(Substance.NH3), BRN_V4_STYLE_FILE);
    assertPauseV4SpecificFile(new BrnFileReader(Substance.NH3), BRN_V4_STYLE_FILE);
  }

  @Test
  public void testParseRegularOPSFileFail1() throws IOException {
    // first fail file is a file where a line has been cut in half.
    final LineReaderResult<OPSSource> result = readBrnFile(EMISSION_FAIL_FILE + 1);

    assertEquals("Number of exceptions", 2, result.getExceptions().size());
    // Fail line: "   0  139123  434456 2.500E-01  0.000   2.5      0   0.2   4   "
    final AeriusException e1 = result.getExceptions().get(0);
    assertEquals("Error not enough fields", AeriusException.Reason.IO_EXCEPTION_NOT_ENOUGH_FIELDS, e1.getReason());
    assertEquals("Error line number not enough fields", "2", e1.getArgs()[0]);
    // Fail line: "   1   1   0  17"
    final AeriusException e2 = result.getExceptions().get(1);
    assertEquals("Error reason", AeriusException.Reason.IO_EXCEPTION_NUMBER_FORMAT, e2.getReason());
    assertEquals("Error line number", "3", e2.getArgs()[0]);
    assertEquals("Error column name", "x(m)", e2.getArgs()[1]);
    assertEquals("Error column content", "1   0", e2.getArgs()[2]);
  }


  @Test
  public void testParseRegularOPSFileFail2() throws IOException {
    // second fail file is a file where a line contains a word.
    final LineReaderResult<OPSSource> result = readBrnFile(EMISSION_FAIL_FILE + 2);

    assertEquals("Number of exceptions", 1, result.getExceptions().size());
    // Fail line: "   0  139123  434456 2.500E-01  0.000   two      0   0.2   4   1   1   0  17"
    final AeriusException e = result.getExceptions().get(0);
    assertEquals("Error reason", AeriusException.Reason.IO_EXCEPTION_NUMBER_FORMAT, e.getReason());
    assertEquals("Error line number", "2", e.getArgs()[0]);
    assertEquals("Error column name", "h(m)", e.getArgs()[1]);
    assertEquals("Error column content", "two", e.getArgs()[2]);
  }


  @Test
  public void testParseRegularOPSFileFail3() throws IOException {
    // third is an empty file, which won't cause an exception, parsing just returns empty collection.

    final LineReaderResult<OPSSource> result = readBrnFile(EMISSION_FAIL_FILE + 3);
    final List<OPSSource> sources = result.getObjects();

    assertNotNull("Check sources for fail3 test should not be null", sources);
    assertEquals("Sources parsed", 0, sources.size());
  }

  @Test
  public void testParseRegularOPSFileFail4() throws IOException {
    // 4rd fail file is a file which has a line where proper format isn't used (space separated instead of fixed width).
    final LineReaderResult<OPSSource> result = readBrnFile(EMISSION_FAIL_FILE + 4);

    assertEquals("Number of exceptions", 1, result.getExceptions().size());
    // Fail line: " 0 139123 434456 2.500E-01 0.000 2.5 0 0.2 4 1 1 0 17",
    final AeriusException e = result.getExceptions().get(0);
    assertEquals("Error reason", AeriusException.Reason.IO_EXCEPTION_NUMBER_FORMAT, e.getReason());
    assertEquals("Error line number", "2", e.getArgs()[0]);
    assertEquals("Error column name", "snr", e.getArgs()[1]);
    assertEquals("Error column content", "0 1", e.getArgs()[2]);
  }

  private void assertParseRegularOPSFile(final BrnFileReader reader, final String filename) throws IOException {
    final LineReaderResult<OPSSource> result = readBrnFile(reader, filename);
    final List<OPSSource> sources = result.getObjects();

    assertNotNull("Sources for regual file should not be null", sources);
    assertEquals("Sources parsed", 7, sources.size());
    // ID's are descending from 7
    int id = 7;
    for (final OPSSource source : sources) {
      assertEquals("Source ID", id--, source.getId());
    }
    // header    : snr    x(m)    y(m)    q(g/s) hc(MW)  h(m)   d(m)  s(m)  dv cat area sd  comp
    // first line:   7  139123  434456 2.500E-01  2.100   2.5      3   0.2   4   2   2   2  17
    assertEquals("X coord", 139123, sources.get(0).getPoint().getX(), EPSILON);
    assertEquals("Y coord", 434456, sources.get(0).getPoint().getY(), EPSILON);
    assertEquals("Emission", 2.500E-01 * Substance.EMISSION_IN_G_PER_S_FACTOR, sources.get(0).getEmission(Substance.NH3), EPSILON);
    assertEquals("Heat content", 2.100, sources.get(0).getHeatContent(), 0.000001);
    assertEquals("Height", 2.5, sources.get(0).getEmissionHeight(), EPSILON);
    assertEquals("Diameter", 3, sources.get(0).getDiameter());
    assertEquals("Spread", 0.2, sources.get(0).getSpread(), EPSILON);
    assertEquals("Diurnal variation", 4, sources.get(0).getDiurnalVariation());
    assertEquals("Particle size distribution", 2, sources.get(0).getParticleSizeDistribution());
    assertEquals("Category number", 2, sources.get(0).getCat());
    assertEquals("Area", 2, sources.get(0).getArea());
    assertEquals("Comp", "17", sources.get(0).getComp());

    // Test for EPSG 4326 coordinate.
    assertPoint(sources.get(1).getPoint(), 13.123, 43.456, WGS84.SRID);
  }

  private void assertPauseV4SpecificFile(final BrnFileReader reader, final String filename) throws IOException {
    final LineReaderResult<OPSSource> result = readBrnFile(reader, filename);
    final List<OPSSource> sources = result.getObjects();

    assertEquals("Exit surface", 1.10, sources.get(0).getOutflowDiameter(), EPSILON);
    assertEquals("Exit velocity", 25.94, sources.get(0).getOutflowVelocity(), EPSILON);
    assertEquals("Exit direction", OPSSourceOutflowDirection.VERTICAL, sources.get(0).getOutflowDirection());
    assertEquals("Emission temperature", 50.00, sources.get(0).getEmissionTemperature(), EPSILON);

    // second source should have a negative velocity which means horizontal exit direction, but the method should return the absolute value
    assertEquals("Exit velocity", 25.94, sources.get(1).getOutflowVelocity(), EPSILON);
    assertEquals("Exit direction", OPSSourceOutflowDirection.HORIZONTAL, sources.get(1).getOutflowDirection());
  }

  private void assertPoint(final Point point, final double x, final double y, final int srid) {
    assertEquals("X coord", x, point.getX(), EPSILON);
    assertEquals("Y coord", y, point.getY(), EPSILON);
    assertEquals("SRID", srid, point.getSrid());
  }

  private LineReaderResult<OPSSource> readBrnFile(final String fileName) throws IOException {
    return OPSTestUtil.readBrnFile(Substance.NH3, getClass(), RESOURCES_DIRECTORY + fileName + USED_EXTENSION);
  }

  private LineReaderResult<OPSSource> readBrnFile(final BrnFileReader reader, final String fileName) throws IOException {
    return OPSTestUtil.readBrnFile(reader, getClass(), RESOURCES_DIRECTORY + fileName + USED_EXTENSION);
  }
}
