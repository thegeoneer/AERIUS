/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.conversion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.ops.domain.OPSCopyFactory;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorProperties;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link EmissionSourceOPSConverter}.
 */
public class EmissionSourceOPSConverterTest {

  private static final int SOURCE_ID = 12;
  private static final int SOURCE_X = 101;
  private static final int SOURCE_Y = 201;
  private static final String SOURCE_LABEL = "SomeLabel";
  private static final int SECTOR_ID = 323;
  private static final double EMISSION = 231.234;
  private static final OPSSourceCharacteristics SOURCE_CHARACTERISTICS =
      new OPSSourceCharacteristics(2, 1, 3, 2, new DiurnalVariationSpecification(1, "1"), 2);

  /**
   * Test method for {@link nl.overheid.aerius.ops.conversion.EmissionSourceOPSConverter#convert(nl.overheid.aerius.shared.domain.source.EmissionSource, nl.overheid.aerius.shared.domain.EmissionValueKey)}.
   */
  @Test
  public void testConvertEmissionSourceEmissionValueKey() {
    final EmissionValueKey key = new EmissionValueKey(Substance.NOX);
    final GenericEmissionSource source = new GenericEmissionSource(SOURCE_ID, new Point(SOURCE_X, SOURCE_Y));
    source.setEmission(Substance.NOX, EMISSION);
    source.setX(SOURCE_X);
    source.setY(SOURCE_Y);
    source.setLabel(SOURCE_LABEL);
    source.setSourceCharacteristics(SOURCE_CHARACTERISTICS.copy());
    source.setSector(new Sector(323, SectorGroup.INDUSTRY, StringUtils.EMPTY, new SectorProperties()));

    final EmissionSourceOPSConverter converter = new EmissionSourceOPSConverter();
    final OPSSource opsSource = converter.convert(source, source, source, key.hatch()).get(0);
    assertEquals("ID", SOURCE_ID, opsSource.getId());
    assertEquals("X coord", SOURCE_X, opsSource.getPoint().getX(), 0.00001);
    assertEquals("Y coord", SOURCE_Y, opsSource.getPoint().getY(), 0.00001);
    assertEquals("Emission", EMISSION, opsSource.getEmission(key.getSubstance()), 0.00001);
    assertEquals("Source Characteristics", SOURCE_CHARACTERISTICS, OPSCopyFactory.toSourceCharacteristics(getDVS(), opsSource));
    assertEquals("Area", 1, opsSource.getArea());
    assertEquals("Comp", StringUtils.EMPTY, opsSource.getComp());
  }

  private ArrayList<DiurnalVariationSpecification> getDVS() {
    final ArrayList<DiurnalVariationSpecification> dvs = new ArrayList<>();
    dvs.add(new DiurnalVariationSpecification(1, "1"));
    return dvs;
  }

  @Test
  public void testConvertEmissionSourceDiurnalCorrection() {
    final EmissionSourceOPSConverter converter = new EmissionSourceOPSConverter();
    final EmissionValueKey keyNOx = new EmissionValueKey(Substance.NOX);
    final EmissionValueKey keyNH3 = new EmissionValueKey(Substance.NH3);
    final GenericEmissionSource source = new GenericEmissionSource(SOURCE_ID, new Point(SOURCE_X, SOURCE_Y));
    source.setEmission(Substance.NOX, EMISSION);
    source.setEmission(Substance.NH3, EMISSION);
    source.setX(SOURCE_X);
    source.setY(SOURCE_Y);
    source.setLabel(SOURCE_LABEL);
    source.setSourceCharacteristics(SOURCE_CHARACTERISTICS.copy());
    source.setSector(new Sector(323, SectorGroup.INDUSTRY, StringUtils.EMPTY, new SectorProperties()));

    source.getSourceCharacteristics().setDiurnalVariationSpecification(new DiurnalVariationSpecification(3, "3"));
    assertEquals("Diurnal variation to be the same", 3, converter.convert(source, source, source, keyNOx.hatch()).get(0).getDiurnalVariation());

    source.getSourceCharacteristics().setDiurnalVariationSpecification(new DiurnalVariationSpecification(4, "4"));
    assertEquals("Same diurnal variation for 4 and NOx", 4,
        converter.convert(source, source, source, keyNOx.hatch()).get(0).getDiurnalVariation());
    source.getSourceCharacteristics().setDiurnalVariationSpecification(new DiurnalVariationSpecification(4, "4"));
    assertEquals("Same diurnal variation for 4 and NH3", 4,
        converter.convert(source, source, source, keyNH3.hatch()).get(0).getDiurnalVariation());

    source.getSourceCharacteristics().setDiurnalVariationSpecification(new DiurnalVariationSpecification(5, "5"));
    assertEquals("Same diurnal variation for 5 and NOx", 5,
        converter.convert(source, source, source, keyNOx.hatch()).get(0).getDiurnalVariation());
    source.getSourceCharacteristics().setDiurnalVariationSpecification(new DiurnalVariationSpecification(5, "5"));
    assertEquals("Same diurnal variation for 5 and NH3", 5,
        converter.convert(source, source, source, new EmissionValueKey(Substance.NH3).hatch()).get(0).getDiurnalVariation());
  }

  /**
   * Test method for {@link nl.overheid.aerius.ops.conversion.EmissionSourceOPSConverter#convert(nl.overheid.aerius.ops.domain.OPSSource, nl.overheid.aerius.shared.domain.Substance, java.util.ArrayList)}.
   */
  @Test
  public void testConvertOPSSourceSubstanceNullSectors() {
    final SectorCategories categories = createCategories();
    final OPSSource opsSource = getStandardOPSSource(Substance.NOX);
    final EmissionValueKey key = new EmissionValueKey(Substance.NOX);
    final EmissionSource source = EmissionSourceOPSConverter.convert(opsSource, Substance.NOX, categories, true);
    final Sector sectorUndefined = Sector.SECTOR_DEFAULT;

    assertEquals("ID", 0, source.getId());
    assertEquals("X coord", SOURCE_X, source.getX(), 0.00001);
    assertEquals("Y coord", SOURCE_Y, source.getY(), 0.00001);
    assertEquals("Emission", EMISSION, source.getEmission(key), 0.00001);
    assertEquals("Source Characteristics", SOURCE_CHARACTERISTICS, source.getSourceCharacteristics());
    assertEquals("Category number", sectorUndefined.getSectorId(), source.getSector().getSectorId());
    assertEquals("Comp", opsSource.getComp() + EmissionSourceOPSConverter.OPS_ID_PREFIX + opsSource.getId()
        + EmissionSourceOPSConverter.OPS_ID_POSTFIX, source.getLabel());

    final EmissionSource sourceWithFakeSector = EmissionSourceOPSConverter.convert(opsSource, Substance.NOX, categories, false);

    assertEquals("ID", 0, sourceWithFakeSector.getId());
    assertEquals("X coord", SOURCE_X, sourceWithFakeSector.getX(), 0.00001);
    assertEquals("Y coord", SOURCE_Y, sourceWithFakeSector.getY(), 0.00001);
    assertEquals("Emission", EMISSION, sourceWithFakeSector.getEmission(key), 0.00001);
    assertEquals("Source Characteristics", SOURCE_CHARACTERISTICS, sourceWithFakeSector.getSourceCharacteristics());
    assertEquals("Category number", SECTOR_ID, sourceWithFakeSector.getSector().getSectorId());
    assertEquals("Comp", opsSource.getComp() + EmissionSourceOPSConverter.OPS_ID_PREFIX + opsSource.getId()
        + EmissionSourceOPSConverter.OPS_ID_POSTFIX, sourceWithFakeSector.getLabel());
  }

  private SectorCategories createCategories() {
    final SectorCategories categories = new SectorCategories();
    categories.setDiurnalVariations(getDVS());
    return categories;
  }

  /**
   * Test method for {@link nl.overheid.aerius.ops.conversion.EmissionSourceOPSConverter#convert(nl.overheid.aerius.ops.domain.OPSSource, nl.overheid.aerius.shared.domain.Substance, java.util.ArrayList)}.
   */
  @Test
  public void testConvertOPSSourceSubstanceWithSectors() {
    final SectorCategories categories = createCategories();
    final OPSSource opsSource = getStandardOPSSource(Substance.NOX);
    final ArrayList<Sector> sectors = new ArrayList<>();
    sectors.add(new Sector(901, SectorGroup.INDUSTRY, StringUtils.EMPTY, null));
    sectors.add(new Sector(SECTOR_ID, SectorGroup.INDUSTRY, StringUtils.EMPTY, null));
    sectors.add(new Sector(161, SectorGroup.INDUSTRY, StringUtils.EMPTY, null));
    categories.setSectors(sectors);

    final EmissionValueKey key = new EmissionValueKey(Substance.NOX);
    final EmissionSource source = EmissionSourceOPSConverter.convert(opsSource, Substance.NOX, categories, true);
    assertEquals("ID", 0, source.getId());
    assertEquals("X coord", SOURCE_X, source.getX(), 0.00001);
    assertEquals("Y coord", SOURCE_Y, source.getY(), 0.00001);
    assertEquals("Emission", EMISSION, source.getEmission(key), 0.00001);
    assertEquals("Source Characteristics", SOURCE_CHARACTERISTICS, source.getSourceCharacteristics());
    assertEquals("Category number", SECTOR_ID, source.getSector().getSectorId());
    assertEquals("Comp", opsSource.getComp() + EmissionSourceOPSConverter.OPS_ID_PREFIX + opsSource.getId()
        + EmissionSourceOPSConverter.OPS_ID_POSTFIX, source.getLabel());
  }

  @Test
  public void testToOPSSources() throws AeriusException {
    final EmissionSourceOPSConverter esOPSConverter = new EmissionSourceOPSConverter();
    List<OPSSource> opsSources = null;

    final EmissionValueKey pm10 = new EmissionValueKey(Substance.PM10);
    final EmissionValueKey nox = new EmissionValueKey(Substance.NOX);
    final EmissionValueKey noxnh3 = new EmissionValueKey(Substance.NOXNH3);
    final EmissionValueKey nh3 = new EmissionValueKey(Substance.NH3);

    final GenericEmissionSource source = new GenericEmissionSource(1, new Point(3, 4));
    source.setGeometry(new WKTGeometry(source.toWKT(), 1));
    source.setEmission(Substance.NOX, 9.8);

    final ArrayList<EmissionSource> sources = new ArrayList<>();
    sources.add(source);

    opsSources = esOPSConverter.convert(source, source, source, pm10.hatch());
    assertTrue("Sources list pm10 should be empty.", opsSources.isEmpty()); // Because source has NOx emission, and conversion done for PM10 only.

    opsSources = esOPSConverter.convert(source, source, source, nox.hatch());
    assertEquals("List should have 1 source.", 1, opsSources.size());
    OPSSource opsSource = opsSources.get(0);
    assertEquals("Source x", source.getX(), opsSource.getPoint().getX(), 0.001);
    assertEquals("Source y", source.getY(), opsSource.getPoint().getY(), 0.001);
    assertEquals("Source emission", source.getEmission(nox), opsSource.getEmission(Substance.NOX), 0.001);

    opsSources = esOPSConverter.convert(source, source, source, noxnh3.hatch());
    assertEquals("List should have 1 source.", 1, opsSources.size());
    opsSource = opsSources.get(0);
    assertEquals("Source x", source.getX(), opsSource.getPoint().getX(), 0.001);
    assertEquals("Source y", source.getY(), opsSource.getPoint().getY(), 0.001);
    assertEquals("Source emission", source.getEmission(nox), opsSource.getEmission(Substance.NOX), 0.001);

    source.setEmission(Substance.NH3, 8.28);

    opsSources = esOPSConverter.convert(source, source, source, noxnh3.hatch());
    opsSource = opsSources.get(0);
    assertEquals("List should have 1 source.", 1, countEmissionResults(opsSources, Substance.NOX));
    assertEquals("Source emission", source.getEmission(nox), opsSource.getEmission(Substance.NOX), 0.001);
    assertEquals("List should have 1 source.", 1, countEmissionResults(opsSources, Substance.NH3));
    assertEquals("Source emission", source.getEmission(nh3), opsSource.getEmission(Substance.NH3), 0.001);
  }

  private int countEmissionResults(final List<OPSSource> sources, final Substance substance) {
    int cnt = 0;
    for (final OPSSource source: sources) {
      cnt += source.getEmission(substance) > 0 ? 1 : 0;
    }
    return cnt;
  }

  private OPSSource getStandardOPSSource(final Substance substance) {
    final OPSSource opsSource = new OPSSource(SOURCE_ID, RDNew.SRID, SOURCE_X, SOURCE_Y);
    opsSource.setEmission(substance, EMISSION);
    OPSCopyFactory.toOpsSource(SOURCE_CHARACTERISTICS, opsSource);
    opsSource.setCat(SECTOR_ID);
    opsSource.setComp(SOURCE_LABEL);
    return opsSource;
  }

}
