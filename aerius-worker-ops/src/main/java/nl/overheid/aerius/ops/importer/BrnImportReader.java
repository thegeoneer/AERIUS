/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.importer;

import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.conversion.EmissionSourceOPSConverter;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.BrnFileReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Import reader for OPS brn source files. It supports reading the fixed width version 0 and the space delimited v1 and v2 of OPS source files.
 */
public class BrnImportReader implements ImportReader {

  private static final String BRN_FILE_EXTENSION = ".brn";
  private static final Set<Substance> SUPPORTED_SUBSTANCES = EnumSet.of(Substance.NH3, Substance.NOX, Substance.PM10);

  private final boolean useValidSectors;

  public BrnImportReader(final boolean useValidSectors) {
    this.useValidSectors = useValidSectors;
  }

  @Override
  public boolean detect(final String filename) {
    return filename.toLowerCase(Locale.ENGLISH).endsWith(BRN_FILE_EXTENSION);
  }

  @Override
  public void read(final String filename, final InputStream inputStream, final SectorCategories categories, final Substance substance,
      final ImportResult importResult) throws IOException, AeriusException {
    if (substance == null) {
      throw new AeriusException(Reason.BRN_WITHOUT_SUBSTANCE);
    }
    if (!SUPPORTED_SUBSTANCES.contains(substance)) {
      throw new AeriusException(Reason.BRN_SUBSTANCE_NOT_SUPPORTED, substance.getName(), supportedSubstancesAsString());
    }
    final LineReaderResult<OPSSource> results = new BrnFileReader(substance).readObjects(inputStream);
    final List<OPSSource> sources = results.getObjects();

    // Convert the OPS sources to an EmissionSource list
    final EmissionSourceList emissionSources = new EmissionSourceList();

    for (final OPSSource src : sources) {
      emissionSources.add(EmissionSourceOPSConverter.convert(src, substance, categories, useValidSectors));
    }
    importResult.addSources(emissionSources);
    importResult.getExceptions().addAll(results.getExceptions());
  }

  private String supportedSubstancesAsString() {
    return SUPPORTED_SUBSTANCES.stream().map(Substance::getName).collect(Collectors.joining(", "));
  }
}
