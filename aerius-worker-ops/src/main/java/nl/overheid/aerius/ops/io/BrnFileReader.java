/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.overheid.aerius.io.AbstractLineReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Use to read in brn files. Will detect version and use the reader it needs.
 */
public class BrnFileReader extends AbstractLineReader<OPSSource> {

  // How many bytes we need to read at the most to determine which version we need.
  private static final int HEADER_MAX_LENGTH = 15;
  private static final Pattern HEADER = Pattern.compile("![\\s\t]BRN-VERSION[\\s\t](\\d)");
  private static final String HEADER_V1 = "1";
  private static final String HEADER_V4 = "4";

  private final Substance substance;

  public BrnFileReader(final Substance substance) {
    this.substance = substance;
  }

  @Override
  public LineReaderResult<OPSSource> readObjects(final InputStream inputStream) throws IOException {
    // Wrap inputstream because if might not support mark/reset
    try (BufferedInputStream buf = new BufferedInputStream(inputStream)) {
      return determineBrnReader(substance, buf).readObjects(buf);
    }
  }

  @Override
  protected OPSSource parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    // Will not parse anything but defer it to the actual readers, so this will not be called.
    return null;
  }

  private BrnFileReaderV0 determineBrnReader(final Substance substance, final InputStream inputStream) throws IOException {
    try {
      inputStream.mark(HEADER_MAX_LENGTH);
      final byte[] headerStartBytes = new byte[HEADER_MAX_LENGTH];
      inputStream.read(headerStartBytes, 0, HEADER_MAX_LENGTH);
      final String headerStartString = new String(headerStartBytes, StandardCharsets.UTF_8.name());

      final BrnFileReaderV0 reader;
      final Matcher matcher = HEADER.matcher(headerStartString.trim());

      if (matcher.find()) {
        switch (matcher.group(1)) {
        case HEADER_V4:
          reader = new BrnFileReaderV4(substance);
          break;
        case HEADER_V1:
          reader = new BrnFileReaderV1(substance);
          break;
        default:
          throw new IOException("Unsupported ops input version: " + matcher.group(1));
        }
      } else {
        reader = new BrnFileReaderV0(substance);
      }

      return reader;
    } finally {
      inputStream.reset();
    }
  }

}
