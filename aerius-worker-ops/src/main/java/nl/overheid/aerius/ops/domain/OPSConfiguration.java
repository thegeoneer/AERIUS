/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import java.io.File;

/**
 * Configuration file for ops.
 */
public class OPSConfiguration {

  private File opsRoot;
  private File runFilesDirectory;
  private boolean keepGeneratedFiles;
  private String settingsMeteoFile;
  private String settingsLandUse;
  private GridSize gridSize;

  public File getOPSRoot() {
    return opsRoot;
  }

  public void setOpsRoot(final File opsRoot) {
    this.opsRoot = opsRoot;
  }

  public File getRunFilesDirectory() {
    return runFilesDirectory;
  }

  public void setRunFilesDirectory(final File runFilesDirectory) {
    this.runFilesDirectory = runFilesDirectory;
  }

  public boolean isKeepGeneratedFiles() {
    return keepGeneratedFiles;
  }

  public void setKeepGeneratedFiles(final boolean keepGeneratedFiles) {
    this.keepGeneratedFiles = keepGeneratedFiles;
  }

  public GridSize getSettingsGridSize(final GridSize defaultGridSize) {
    return gridSize == null ? defaultGridSize : gridSize;
  }

  public String getSettingsMeteoFile() {
    return settingsMeteoFile;
  }

  public void setSettingsMeteoFile(final String settingsMeteoFile) {
    this.settingsMeteoFile = settingsMeteoFile;
  }

  public String getSettingsLandUse() {
    return settingsLandUse;
  }

  public void setSettingsLandUse(String settingsLandUse) {
    this.settingsLandUse = settingsLandUse;
  }
}
