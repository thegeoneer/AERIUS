/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Locale;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WGS84;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.domain.OPSSource.OPSSourceOutflowDirection;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.util.OSUtils;

/**
 * Writes an OPS sources file.
 *
 * <dl>
 * <dt>snr</dt>
 * <dd>- source identification number (not essential)</dd>
 * <dt>x</dt>
 * <dd>- x-coordinate (m) (or decimal degrees longitude)</dd>
 * <dt>y</dt>
 * <dd>- y-coordinate (m) (or decimal degrees latitude)</dd>
 * <dt>q</dt>
 * <dd>- source strength (g/s)</dd>
 * <dt>hc</dt>
 * <dd>- heat content (MW)</dd>
 * <dt>h</dt>
 * <dd>- source height (m)</dd>
 * <dt>d</dt>
 * <dd>- source diameter (m)</dd>
 * <dt>s</dt>
 * <dd>- vertical spread of source height(s) (m)</dd>
 * <dt>D_stack</dt>
 * <dd>- inner diameter stack (m)</dd>
 * <dt>V_stack</dt>
 * <dd>- exit velocity (m/s)</dd>
 * <dt>Ts_stack</dt>
 * <dd>- temperature effluent gas (C)</dd>
 * <dt>dv</dt>
 * <dd>- code for diurnal variation of emission (-999 to 3)</dd>
 * <dt>cat</dt>
 * <dd>- source category number (0-9999) (for administrative purposes only)</dd>
 * <dt>area</dt>
 * <dd>- country or area number (0-9999) (for administrative purposes only)</dd>
 * <dt>ps</dt>
 * <dd>- particle-size distribution code (-999 to 3)</dd>
 * <dt>length(m)</dt>
 * <dd>- building length in meter (-9999 for not used otherwise range 10.0 to 105.0)</dd>
 * <dt>width(m)</dt>
 * <dd>- building width in meter (-9999 for not used otherwise range 1.5 to 87.15)</dd>
 * <dt>height(m)</dt>
 * <dd>- building height in meter (-9999 for not used otherwise range 3.0 to 8.0)</dd>
 * <dt>orientation(degrees</dt>
 * <dd>- building degrees (-9999 for not used otherwise range 0 to 179)</dd>
 * <dt>comp</dt>
 * <dd>- name of the substance (not essential)</dd>
 * </dl>
 */
public final class BrnFileWriter {

  /**
   * if category would be lower 0, set it to 1, otherwise ops will log error.
   */
  private static final int MIN_CAT = 1;

  /**
   * @see OPSRunner#createSourceInputFile original called source.brn
   */
  private static final String FILENAME = "emission.brn";

  private static final String HEADER_VERSION_4 =
      "! BRN-VERSION 4" + OSUtils.NL
      + "! snr    x(m)     y(m)     q(g/s)   hc(MW)   h(m)    r(m)   s(m)   D_stack(m) V_stack(m/s) Ts_stack(C) dv  cat area   ps length(m) width(m) height(m) orientation(degrees) component"
      + OSUtils.NL;

  //Format representation of the following fortran notation:
  // i4,<based on EPSG see below>,e10.3,f7.3,f6.1,i7.0,f6.1,4i4,2x,a12
  private static final String FORMAT_SOURCE =
      "%4d " // i4 snr
      + "%s " // 3.5f  x y lat/lot 1 meter precision
      + "%10.3E " // e10.3 q(g/s)
      + "%7.3f " // f7.3 hc(mw)
      + "%6.1f " // f6.1 h(m)
      + "%7d " // i7.0 d(m)
      + "%6.1f " // f6.1 s(m)
      + "%4.2f " // D_stack(m)
      + "%4.1f " // V_stack(m/s)
      + "%4.2f " // Ts_stack(C)
      + "%4d %4d %4d %4d " // 4i4 dv cat area sd
      + "%6.1f " // length(m)
      + "%6.1f " // width(m)
      + "%6.1f " // height(m)
      + "%6.1f " // orientation(degrees)
      + "%-12s" // a12 comp
      + OSUtils.NL;

  // Location format representation of the following fortran notation for RD New: 2i8.0
  private static final String FORMAT_LOCATION_RDNEW = "%8.0f %8.0f";
  // Location format representation of the following fortran notation for WGS84/EPSG:4326: 2f3.5. lat/lon 1 meter precision.
  private static final String FORMAT_LOCATION_EPSG_4326 = "%3.5f %3.5f";

  private BrnFileWriter() {
  }

  public static String getFileName() {
    return FILENAME;
  }

  /**
   * Writes OPS data for the given substance to a file with a default name: {@link #FILENAME}.
   * @param path path
   * @param data ops data
   * @param substance substance.
   * @throws IOException io exception.
   */
  public static void writeFile(final File path, final Collection<OPSSource> data, final Substance substance) throws IOException {
    writeFile(path, FILENAME, data, substance);
  }

  /**
   * Writes OPS data for the given substance to a file.
   * @param path path
   * @param fileName name of the file
   * @param data ops data
   * @param substance substance
   * @throws IOException io exception
   */
  private static void writeFile(final File path, final String fileName, final Collection<OPSSource> data, final Substance substance)
      throws IOException {
    try (final Writer writer = Files.newBufferedWriter(new File(path, fileName).toPath(), StandardCharsets.UTF_8)) {
      boolean writeHeader = true;

      for (final OPSSource d : data) {
        if (writeHeader) {
          writer.write(HEADER_VERSION_4);
          writeHeader = false;
        }

        // if emission is 0 don't add to file because ops doesn't allow sources with 0 emissions.
        if (d.getEmission(substance) > 0) {
          writeSingleSource(substance, writer, d);
        }
      }
    }
  }

  private static void writeSingleSource(final Substance substance, final Writer writer, final OPSSource d) throws IOException {
    //use 0 as default for SNR (or ID) instead of the source ID. It's not used in the result and has quite the limit.
    // Cast spread to a precision factor of 1, otherwise it's rounded down.
    final boolean epsg4326 = d.getPoint().getSrid() == WGS84.SRID;
    // The spread cannot be higher than the height of the building
    // (prior to OPS 4.6.2.1 this used to be a warning, but will now cause a crash if not handled properly).
    //      /
    // ____/
    // |  |\
    // |  | \
    // |  |
    // |  |
    // |  |
    // This chimney has a height (in characters) of 5 and a spread of 2. A higher spread than 5 would force it to hit the ground.
    // If we use a building the maximum allowed EmissionHeight is 13, we have to check if we have to correct the EmissionHeight with
    // getEmissionHeight(d)
    final double spread = Math.min(getEmissionHeight(d),
        new BigDecimal(String.valueOf(d.getSpread())).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue());

    writer.write(String.format(Locale.US, FORMAT_SOURCE, 0, getLocation(d.getPoint(), epsg4326),
        d.getEmission(substance) / Substance.EMISSION_IN_G_PER_S_FACTOR, getHeatContent(d), getEmissionHeight(d),
        d.getDiameter(), spread, getOutflowDiameter(d), getOutflowVelocity(d), getEmissionTemperature(d),
        d.getDiurnalVariation(), Math.min(MIN_CAT, d.getCat()), d.getArea(), d.getParticleSizeDistribution(),
        useBuildingValueMinMax(d.getBuildingLength(), OPSLimits.SCOPE_BUILDING_LENGTH_MINIMUM, OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM,
            BrnConstants.BUILDING_LENGTH_NOT_APPLICABLE),
        useBuildingValueMinMax(d.getBuildingWidth(), OPSLimits.SCOPE_BUILDING_WIDTH_MINIMUM, OPSLimits.SCOPE_BUILDING_WIDTH_MAXIMUM,
            BrnConstants.BUILDING_WIDTH_NOT_APPLICABLE),
        useBuildingValueMinMax(d.getBuildingHeight(), OPSLimits.SCOPE_BUILDING_HEIGHT_MINIMUM, OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM,
            BrnConstants.BUILDING_HEIGHT_NOT_APPLICABLE),
        Double.valueOf(d.getBuildingOrientation()), ""));
  }

  private static String getLocation(final Point p, final boolean epsg4326) {
    final String formatLocation = epsg4326 ? FORMAT_LOCATION_EPSG_4326 : FORMAT_LOCATION_RDNEW;

    return String.format(Locale.US, formatLocation, getX(p, epsg4326), getY(p, epsg4326));
  }

  private static Object getX(final Point p, final boolean epsg4326) {
    return epsg4326 ? p.getX() : p.getRoundedX();
  }

  private static Object getY(final Point p, final boolean epsg4326) {
    return epsg4326 ? p.getY() : p.getRoundedY();
  }

  private static double getHeatContent(final OPSSource s) {
    if (useOutflow(s)) {
      return useBuilding(s) ? 0d : BrnConstants.HEAT_CONTENT_NOT_APPLICABLE;
    } else {
      return  s.getHeatContent() < 0 ? BrnConstants.HEAT_CONTENT_NOT_APPLICABLE : s.getHeatContent();
    }
  }

  private static double getEmissionHeight(final OPSSource s) {
    if (useBuilding(s)) {
      return Math.min(OPSLimits.SCOPE_OUTFLOW_HEIGHT_MAXIMUM, s.getEmissionHeight());
    } else {
      return s.getEmissionHeight();
    }
  }

  private static double getOutflowDiameter(final OPSSource s) {
    if (useOutflow(s)) {
     return useBuilding(s) ? adjustValueToMinMax(s.getOutflowDiameter(), OPSLimits.SCOPE_OUTFLOW_DIAMETER_MINIMUM,
         OPSLimits.SCOPE_OUTFLOW_DIAMETER_MAXIMUM) : s.getOutflowDiameter();
    } else {
      return BrnConstants.OUTFLOW_DIAMETER_NOT_APPLICABLE;
    }
  }

  private static double getOutflowVelocity(final OPSSource s) {
    if (useOutflow(s) && s.getOutflowDirection() != null && s.getOutflowVelocity() > BrnConstants.OUTFLOW_VELOCITY_NOT_APPLICABLE) {
      final double outflowValue;
      if (useBuilding(s)) {
        outflowValue = adjustValueToMinMax(s.getOutflowVelocity(), OPSLimits.SCOPE_OUTFLOW_VELOCITY_MINIMUM,
            OPSLimits.SCOPE_OUTFLOW_VELOCITY_MAXIMUM);
      } else {
        outflowValue = s.getOutflowVelocity();
      }
      return (s.getOutflowDirection() == OPSSourceOutflowDirection.HORIZONTAL ? -1 : 1) * outflowValue;
    }
    return BrnConstants.OUTFLOW_VELOCITY_NOT_APPLICABLE;
  }

  private static double getEmissionTemperature(final OPSSource s) {
    if (useOutflow(s)) {
      return useBuilding(s) ? BrnConstants.EMISSION_TEMPERATURE_NOT_APPLICABLE : s.getEmissionTemperature();
    } else {
      return BrnConstants.EMISSION_TEMPERATURE_NOT_APPLICABLE;
    }
  }

  /**
   * If heat content equals HEAT_CONTENT_NOT_APPLICABLE we use outflow.
   *
   * @param s
   * @return
   */
  private static boolean useOutflow(final OPSSource s) {
    return Double.compare(BrnConstants.HEAT_CONTENT_NOT_APPLICABLE, s.getHeatContent()) == 0;
  }

  private static double useBuildingValueMinMax(final double value, final double minValue, final double maxValue, final double notapplicableValue) {
    if (value > 0) {
      return adjustValueToMinMax(value, minValue, maxValue);
    } else {
      return notapplicableValue;
    }
  }

  private static double adjustValueToMinMax(final double value, final double minValue, final double maxValue) {
    return Math.max(minValue, Math.min(maxValue, value));
  }

  private static boolean useBuilding(final OPSSource s) {
    return s.getBuildingWidth() > 0 || s.getBuildingLength() > 0 || s.getBuildingHeight() > 0;
  }

}
