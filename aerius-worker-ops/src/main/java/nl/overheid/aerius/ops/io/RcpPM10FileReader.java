/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import nl.overheid.aerius.shared.domain.Substance;

/**
 * Reads the OPS results file for PM10 results.
 *
 * PM10 results file consist of 2 partitions: first lines are for PM2.5 concentrations
 * and last lines are for PM10 concentrations. Each of the 2 partitions has 3 header
 * lines. Hence this is where the reader should start reading for PM10: lines / 2 + 3.
 *
 */
public class RcpPM10FileReader extends ResultDataFileReader {

  private static final int PARTITION_FACTOR = 2;

  private Substance resultSubstance = Substance.PM10;

  @Override
  protected int getNumHeaderLines() throws IOException {
     // For PM10 (to skip the PM2.5 results) the reader should start reading at: lines / 2 + 3.
    return resultSubstance == Substance.PM10 ? (numberOfLinesInFile() / PARTITION_FACTOR + DEFAULT_NUM_HEADER_LINES) : super.getNumHeaderLines();
  }

  @Override
  protected int getMaxLineNumber() throws IOException {
    // For PM2.5 (to ensure the PM10 header/results won't cause errors) the reader should stop reading at: lines / 2.
    return resultSubstance == Substance.PM25 ? (numberOfLinesInFile() / PARTITION_FACTOR) : super.getMaxLineNumber();
  }

  private int numberOfLinesInFile() throws IOException {
    int counter = 0;
    try (Scanner inputScanner = new Scanner(inputFile, StandardCharsets.UTF_8.name())) {
      while (inputScanner.hasNextLine()) {
        inputScanner.nextLine();
        counter++;
      }
    }
    return counter;
  }

  public void setResultSubstance(final Substance resultSubstance) {
    this.resultSubstance = resultSubstance;
  }

}
