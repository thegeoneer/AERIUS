/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Data class to combine all data needed for a single OPS run.
 */
public class OPSInputData extends EngineInputData<OPSSource, OPSReceptor> {

  private static final long serialVersionUID = 2669320689905895241L;

  private final String opsVersion;
  private final int hexagonSurfaceLevel1;

  private Collection<OPSSource> nearbySources;

  public OPSInputData(final String opsVersion, final int hexagonSurfaceLevel1) {
    super(Profile.PAS); // At this moment only WETNB is supported with OPS
    this.opsVersion = opsVersion;
    this.hexagonSurfaceLevel1 = hexagonSurfaceLevel1;
  }

  @Override
  public int getSourcesSize() {
    return getEmissionSources().size() + (nearbySources == null ? 0 : nearbySources.size());
  }

  public String getOpsVersion() {
    return opsVersion;
  }

  @Override
  @NotNull
  @Size(min = 1)
  @Valid
  public Collection<OPSReceptor> getReceptors() {
    return super.getReceptors();
  }

  public int getHexagonSurfaceLevel1() {
    return hexagonSurfaceLevel1;
  }

  public Collection<OPSSource> getNearbySources() {
    return nearbySources;
  }

  public void setNearbySources(final Collection<OPSSource> nearbySources) {
    this.nearbySources = nearbySources;
  }
}
