/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import java.util.List;

import nl.overheid.aerius.ops.domain.OPSSource.OPSSourceOutflowDirection;
import nl.overheid.aerius.ops.io.BrnConstants;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;

/**
 * Copy factor class to copy from {@link OPSSourceCharacteristics} to {@link OPSSource} and back.
 */
public final class OPSCopyFactory {

  private OPSCopyFactory() {
    // util
  }

  /**
   * Copies the content of {@link OPSSourceCharacteristics} to the {@link OPSSource} object.
   * @param characteristics input {@link OPSSourceCharacteristics}
   * @param opsSource output {@link OPSSource}
   */
  public static void toOpsSource(final OPSSourceCharacteristics characteristics, final OPSSource opsSource) {
    opsSource.setOutflowDiameter(characteristics.getOutflowDiameter());
    opsSource.setOutflowVelocity(characteristics.getOutflowVelocity());
    opsSource.setEmissionTemperature(characteristics.getEmissionTemperature());
    opsSource.setOutflowDirection(
        characteristics.getOutflowDirection() == OutflowDirectionType.HORIZONTAL
        ? OPSSourceOutflowDirection.HORIZONTAL : OPSSourceOutflowDirection.VERTICAL);
    opsSource.setHeatContent(characteristics.isUserdefinedHeatContent() ? characteristics.getHeatContent() : BrnConstants.HEAT_CONTENT_NOT_APPLICABLE);
    opsSource.setDiameter(characteristics.getDiameter());
    opsSource.setDiurnalVariation(characteristics.getDiurnalVariationSpecification().getId());
    opsSource.setEmissionHeight(characteristics.getEffectiveHeight());
    opsSource.setSpread(characteristics.getSpread());
    opsSource.setParticleSizeDistribution(characteristics.getParticleSizeDistribution());
    if (characteristics.isBuildingInfluence()) {
      opsSource.setBuildingWidth(characteristics.getBuildingWidth());
      opsSource.setBuildingLenght(characteristics.getBuildingLength());
      opsSource.setBuildingHeight(characteristics.getBuildingHeight());
      opsSource.setBuildingOrientation(characteristics.getBuildingOrientation());
    } else {
      opsSource.setBuildingWidth(BrnConstants.BUILDING_WIDTH_NOT_APPLICABLE);
      opsSource.setBuildingLenght(BrnConstants.BUILDING_LENGTH_NOT_APPLICABLE);
      opsSource.setBuildingHeight(BrnConstants.BUILDING_HEIGHT_NOT_APPLICABLE);
      opsSource.setBuildingOrientation(BrnConstants.BUILDING_ORIENTATION_NOT_APPLICABLE);
    }
  }

  /**
   * Returns a new {@link OPSSourceCharacteristics} objects from the {@link OPSSource} object.
   * @param dvs list of {@link DiurnalVariationSpecification} objects.
   * @param opsSource input {@link OPSSource}.
   * @return new {@link OPSSourceCharacteristics} object.
   */
  public static OPSSourceCharacteristics toSourceCharacteristics(final List<DiurnalVariationSpecification> dvs, final OPSSource opsSource) {
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics();
    characteristics.setDiameter(opsSource.getDiameter());
    characteristics.setDiurnalVariationSpecification(getDiurnalVariation(dvs, opsSource.getDiurnalVariation()));
    characteristics.setHeatContent(opsSource.getHeatContent());
    characteristics.setEmissionHeight(opsSource.getEmissionHeight());

    characteristics.setOutflowDiameter(opsSource.getOutflowDiameter());
    characteristics.setOutflowVelocity(opsSource.getOutflowVelocity());
    characteristics.setEmissionTemperature(opsSource.getEmissionTemperature());
    characteristics.setOutflowDirection(
        opsSource.getOutflowDirection() == OPSSourceOutflowDirection.HORIZONTAL
        ? OutflowDirectionType.HORIZONTAL : OutflowDirectionType.VERTICAL);
    characteristics.setBuildingWidth(opsSource.getBuildingWidth());
    characteristics.setBuildingLength(opsSource.getBuildingLength());
    characteristics.setBuildingHeight(opsSource.getBuildingHeight());
    characteristics.setBuildingOrientation(opsSource.getBuildingOrientation());
    characteristics.setSpread(opsSource.getSpread());
    characteristics.setParticleSizeDistribution(opsSource.getParticleSizeDistribution());

    return characteristics;
  }

  private static DiurnalVariationSpecification getDiurnalVariation(final List<DiurnalVariationSpecification> dvs, final int diurnalVariation) {
    for (final DiurnalVariationSpecification diurnalVariationSpec : dvs) {
      if (diurnalVariationSpec.getId() == diurnalVariation) {
        return diurnalVariationSpec;
      }
    }
    return null;
  }
}
