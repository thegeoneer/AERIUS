/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.HashMap;

import org.junit.Test;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Envelope;

import nl.overheid.aerius.taskmanager.client.util.QueueHelper;

/**
 * Test class for WorkerConsumer. Tests should work without MQ.
 */
public class WorkerConsumerTest {

  @Test
  public void testHandleDeliveryTestTaskInput() throws IOException {
    final Object returned = handleDeliveryForObject(new TestTaskInput());
    assertTrue("Returned object should be one of TestTaskOutput", returned instanceof TestTaskOutput);
  }

  @Test
  public void testHandleDeliveryTestIntegerDoubleTaskInput() throws IOException {
    final TestIntegerDoubleTaskInput input = new TestIntegerDoubleTaskInput();
    input.setTobeDoubled(2);
    final Object returned = handleDeliveryForObject(input);
    assertTrue("Returned object should be one of TestIntegerDoubleTaskOutput",
        returned instanceof TestIntegerDoubleTaskOutput);
    final TestIntegerDoubleTaskOutput output = (TestIntegerDoubleTaskOutput) returned;
    assertEquals("Output of doubled", 4, output.getDoubled());
  }

  @Test
  public void testHandleDeliveryWrongObject() throws IOException {
    final Object returned = handleDeliveryForObject(Integer.valueOf(12));
    assertTrue("Should have thrown exception", returned instanceof UnsupportedOperationException);
  }

  private Object handleDeliveryForObject(final Serializable object) throws IOException {
    final MockWorkerHandler workerHandler = new MockWorkerHandler();
    final WorkerConsumer workerConsumer = new WorkerConsumer(new MockChannel(), workerHandler);
    final BasicProperties props = new BasicProperties.Builder()
    .correlationId("SOME")
    .headers(new HashMap<String, Object>())
    .build();
    final Envelope envelope = new Envelope(0, false, null, null);
    workerConsumer.handleDelivery(null, envelope, props, QueueHelper.objectToBytes(object));
    return workerHandler.getLastReturnObject();
  }

  @Test(expected = IllegalArgumentException.class)
  public void testWorkerConsumerWithoutHandler() throws IOException {
    final WorkerConsumer workerConsumer = new WorkerConsumer(new MockChannel(), null);
    final BasicProperties props = new BasicProperties.Builder()
    .correlationId("SOME")
    .headers(new HashMap<String, Object>())
    .build();
    workerConsumer.handleDelivery(null, null, props, QueueHelper.objectToBytes("SerializeThis"));
  }

  @Test(expected = NotSerializableException.class)
  public void testConvertingNonSerializableObject() throws IOException {
    QueueHelper.objectToBytes(new Serializable() {

      private static final long serialVersionUID = -3407379864907106639L;

    });
  }

  @Test(expected = IOException.class)
  public void testWorkerConsumerWithStringAsByteInput() throws IOException, ClassNotFoundException {
    final MockWorkerHandler workerHandler = new MockWorkerHandler();
    final MockChannel mockChannel = new MockChannel();
    final WorkerConsumer workerConsumer = new WorkerConsumer(mockChannel, workerHandler);
    final BasicProperties props = new BasicProperties.Builder()
    .correlationId("SOME").headers(new HashMap<String, Object>()).build();
    final Envelope envelope = new Envelope(0, false, null, null);
    //object should be serialized by QueueHelper.
    workerConsumer.handleDelivery(null, envelope, props, "Invalid".getBytes());
    final Object received = QueueHelper.bytesToObject(mockChannel.getReceived());
    fail("Should have thrown an exception, instead returned "
        + received.getClass().toString());
  }

}
