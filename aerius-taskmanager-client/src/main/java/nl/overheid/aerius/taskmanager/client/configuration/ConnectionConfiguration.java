/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client.configuration;

/**
 * Configuration object for different (queue) properties.
 */
public interface ConnectionConfiguration {

  /**
   * RabbitMQ default port.
   */
  int DEFAULT_BROKER_PORT = 5672;

  /**
   * RabbitMQ default virtualHost (root).
   */
  String DEFAULT_BROKER_VIRTUAL_HOST = "/";

  /**
   * RabbitMQ default management port.
   */
  int DEFAULT_BROKER_MANAGEMENT_PORT = 15672;

  /**
   * Default refresh time in seconds.
   */
  int DEFAULT_MANAGEMENT_REFRESH_RATE = 60; //seconds

  /**
   * @return The host used to communicate with the broker
   */
  String getBrokerHost();

  /**
   * @return The port used to communicate with the broker (rabbitMQ default: 5672)
   */
  int getBrokerPort();

  /**
   * @return The username to be used while communicating with the broker
   */
  String getBrokerUsername();

  /**
   * @return The password to be used while communicating with the broker
   */
  String getBrokerPassword();

  /**
   * @return The virtual host to be used on the broker (rabbitMQ default: /)
   */
  String getBrokerVirtualHost();

  /**
   * @return The port used to communicate with the management interface of the broker (rabbitMQ default: 15672)
   */
  int getBrokerManagementPort();

  /**
   * @return The refresh rate in seconds the RabbitMQ management api is queried for status changes.
   */
  int getManagementRefreshRate();
}
