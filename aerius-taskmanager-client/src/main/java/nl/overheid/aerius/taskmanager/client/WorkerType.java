/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import java.util.Locale;

/**
 * Contains the names of the group of queues.
 */
public enum WorkerType {

  /**
   * Worker type for processing calculator database actions.
   */
  CALCULATOR,
  /**
   * Worker type for processing connect database actions.
   */
  CONNECT,
  /**
   * Worker type for processing 'Meldingen'.
   */
  MELDING,
  /**
   * Worker type for processing register database actions.
   */
  REGISTER,
  /**
   * AERIUS SRM2 calculation engine.
   */
  ASRM2,
  /**
   * OPS calculation engine.
   */
  OPS,
  /**
   * Worker type for processing messages (to be send to users for instance).
   */
  MESSAGE,

  /**
   * An import for register database.
   */
  IMPORT_REGISTER,

  /**
   * Test worker type. doesn't have it's own queue, but can be used in mockup testing.
   */
  TEST;

  /**
   * Main prefix for queue names.
   */
  private static final String NAMING_PREFIX = "aerius.";
  private static final char DOT = '.';

  /**
   * @param taskName The name of the task to get the consumer tag for.
   * @return The right consumertag to use for this task when consuming messages.
   */
  public String getConsumerTag(final String taskName) {
    return getWorkerType() + DOT + taskName;
  }

  /**
   * @param taskName The name of the task to get the queueName for.
   * @return The queuename that should be used for declaring the queue.
   */
  public String getTaskQueueName(final String taskName) {
    return getWorkerType() + DOT + taskName;
  }

  public String getWorkerQueueName() {
    return NAMING_PREFIX + "worker." + propertyName();
  }

  public String propertyName() {
    return name().toLowerCase(Locale.ENGLISH);
  }

  private String getWorkerType() {
    return NAMING_PREFIX + propertyName();
  }
}
