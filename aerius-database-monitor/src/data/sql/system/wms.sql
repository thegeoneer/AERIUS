{import_common 'system/sld.sql'}

--Product specific zoom_levels
INSERT INTO system.wms_zoom_levels (wms_zoom_level_id,name) VALUES (201,'Gebiedssamenvattingen - standaard');

INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (201,0,30000,1);
INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (201,30000,50000,2);
INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (201,50000,75000,3);
INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (201,75000,150000,4);
INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (201,150000,3000000,5);

--Product specific SLD's,
--Monitor, ranged between 2001-2999
INSERT INTO system.sld (sld_id, description) VALUES (2001,'Totale depositie per sector');
INSERT INTO system.sld (sld_id, description) VALUES (2003,'Gebiedssamenvatting - Natuurgebieden');
INSERT INTO system.sld (sld_id, description) VALUES (2004,'Gebiedssamenvatting - Stikstofoverbelasting per natuurgebied/leefgebied');
INSERT INTO system.sld (sld_id, description) VALUES (2005,'Totale depositieruimte');
INSERT INTO system.sld (sld_id, description) VALUES (2006,'Herstelmaatregelen per natuurgebied Monitor');
INSERT INTO system.sld (sld_id, description) VALUES (2007,'Herstelmaatregelen in clusters');
INSERT INTO system.sld (sld_id, description) VALUES (2008,'Verschil ontwikkeling met/zonder PAS Monitor');
INSERT INTO system.sld (sld_id, description) VALUES (2009,'Kenschets - Depositiedaling');
INSERT INTO system.sld (sld_id, description) VALUES (2010,'Kenschets - Depositietoename');
INSERT INTO system.sld (sld_id, description) VALUES (2011,'Depositie in jaar X met PAS inclusief provinciaal beleid, minus depositie in basisjaar.');
INSERT INTO system.sld (sld_id, description) VALUES (2012,'Relevante hexagonen.');
INSERT INTO system.sld (sld_id, description) VALUES (2013,'Afstand tot de KDW voor zichtjaar');
INSERT INTO system.sld (sld_id, description) VALUES (2014,'Ontwikkelingssaldo');
--REMEMBER: update when new SLDs are added: next sld_id = 2015

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2001,'deposition >= 0 && deposition <= 50','F6EFF7','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2001,'deposition > 50 && deposition <= 100','D0D1E6','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2001,'deposition > 100 && deposition <= 200','A6BDDB','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2001,'deposition > 200 && deposition <= 300','67A9CF','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2001,'deposition > 300 && deposition <= 400','3690C0','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2001,'deposition > 400 && deposition <= 500','02818A','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2001,'deposition > 500','016450','FFFFFF',null,null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2003,null,null,null,
'<PolygonSymbolizer>
  <Stroke>
    <CssParameter name="stroke">#4E4E4E</CssParameter>
    <CssParameter name="stroke-width">4</CssParameter>
    <CssParameter name="stroke-dasharray">14 5</CssParameter>
  </Stroke>
</PolygonSymbolizer>',null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2004,'nitrogen_load = no_nitrogen_problem','41AB5D','4E4E4E',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2004,'nitrogen_load = equilibrium','F7FFF7','4E4E4E',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2004,'nitrogen_load = light_overload','C2A5CF','4E4E4E',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2004,'nitrogen_load = heavy_overload','7B3294','4E4E4E',null,null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2005,'total_space >= 0 && total_space <= 20','F2F0F7','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2005,'total_space > 20 && total_space <= 40','DADAEB','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2005,'total_space > 40 && total_space <= 60','BCBDDC','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2005,'total_space > 60 && total_space <= 80','9E9AC8','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2005,'total_space > 80 && total_space <= 100','807DBA','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2005,'total_space > 100 && total_space <= 150','6A51A3','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2005,'total_space > 150','4A1486','FFFFFF',null,null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2006,null,null,null,
'<PolygonSymbolizer>
  <Fill>
    <CssParameter name="fill">#000080</CssParameter>
  </Fill>
  <Stroke>
     <CssParameter name="stroke">#FFFFFF</CssParameter>
    <CssParameter name="stroke-width">1</CssParameter>
  </Stroke>
</PolygonSymbolizer>',
'<ogc:Or>
<ogc:PropertyIsEqualTo>
    <ogc:Function name="geometryType">
       <ogc:PropertyName>geometry</ogc:PropertyName>
    </ogc:Function>
    <ogc:Literal>Polygon</ogc:Literal>
</ogc:PropertyIsEqualTo>
<ogc:PropertyIsEqualTo>
    <ogc:Function name="geometryType">
       <ogc:PropertyName>geometry</ogc:PropertyName>
    </ogc:Function>
    <ogc:Literal>MultiPolygon</ogc:Literal>
</ogc:PropertyIsEqualTo>
</ogc:Or>
',null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2006,null,null,null,'
<LineSymbolizer>
  <Stroke>
    <CssParameter name="stroke">#800000</CssParameter>
    <CssParameter name="stroke-width">1</CssParameter>
  </Stroke>
</LineSymbolizer>',
'<ogc:PropertyIsEqualTo>
    <ogc:Function name="geometryType">
       <ogc:PropertyName>geometry</ogc:PropertyName>
    </ogc:Function>
    <ogc:Literal>LineString</ogc:Literal>
 </ogc:PropertyIsEqualTo>
',null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2007,'geometry_accuracy = strict && index_within_group = 1',null,null,
'<sld:PolygonSymbolizer>
  <sld:Fill>
    <sld:CssParameter name="fill">#CC4C02</sld:CssParameter>
    <sld:CssParameter name="fill-opacity">0.7</sld:CssParameter>
  </sld:Fill>
</sld:PolygonSymbolizer>',null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2007,'geometry_accuracy = strict && index_within_group = 2',null,null,
'<sld:PolygonSymbolizer>
  <sld:Fill>
    <sld:CssParameter name="fill">#AE017E</sld:CssParameter>
    <sld:CssParameter name="fill-opacity">0.7</sld:CssParameter>
  </sld:Fill>
</sld:PolygonSymbolizer>',null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2007,'geometry_accuracy = strict && index_within_group = 3',null,null,
'<sld:PolygonSymbolizer>
  <sld:Fill>
    <sld:CssParameter name="fill">#4A14D7</sld:CssParameter>
    <sld:CssParameter name="fill-opacity">0.7</sld:CssParameter>
  </sld:Fill>
</sld:PolygonSymbolizer>',null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2007,'geometry_accuracy = strict && index_within_group = 4',null,null,
'<sld:PolygonSymbolizer>
  <sld:Fill>
    <sld:CssParameter name="fill">#005A32</sld:CssParameter>
    <sld:CssParameter name="fill-opacity">0.7</sld:CssParameter>
  </sld:Fill>
</sld:PolygonSymbolizer>',null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2007,'geometry_accuracy = sketch && index_within_group = 1',null,null,
'<PolygonSymbolizer>
  <Fill>
    <GraphicFill>
      <Graphic>
        <Mark>
          <WellKnownName>shape://slash</WellKnownName>
          <Stroke>
            <CssParameter name="stroke">#CC4C02</CssParameter>
            <CssParameter name="fill-opacity">1</CssParameter>
            <CssParameter name="stroke-width">2.0</CssParameter>
          </Stroke>
        </Mark>
        <Size>5</Size>
      </Graphic>
    </GraphicFill>
  </Fill>
</PolygonSymbolizer>',null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2007,'geometry_accuracy = sketch && index_within_group = 2',null,null,
'<PolygonSymbolizer>
  <Fill>
    <GraphicFill>
      <Graphic>
        <Mark>
          <WellKnownName>shape://slash</WellKnownName>
          <Stroke>
            <CssParameter name="stroke">#AE017E</CssParameter>
            <CssParameter name="fill-opacity">1</CssParameter>
            <CssParameter name="stroke-width">2.0</CssParameter>
          </Stroke>
        </Mark>
        <Size>5</Size>
      </Graphic>
    </GraphicFill>
  </Fill>
</PolygonSymbolizer>',null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2007,'geometry_accuracy = sketch && index_within_group = 3',null,null,
'<PolygonSymbolizer>
  <Fill>
    <GraphicFill>
      <Graphic>
        <Mark>
          <WellKnownName>shape://slash</WellKnownName>
          <Stroke>
            <CssParameter name="stroke">#4A14D7</CssParameter>
            <CssParameter name="fill-opacity">1</CssParameter>
            <CssParameter name="stroke-width">2.0</CssParameter>
          </Stroke>
        </Mark>
        <Size>5</Size>
      </Graphic>
    </GraphicFill>
  </Fill>
</PolygonSymbolizer>',null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2007,'geometry_accuracy = sketch && index_within_group = 4',null,null,
'<PolygonSymbolizer>
  <Fill>
    <GraphicFill>
      <Graphic>
        <Mark>
          <WellKnownName>shape://slash</WellKnownName>
          <Stroke>
            <CssParameter name="stroke">#005A32</CssParameter>
            <CssParameter name="fill-opacity">1</CssParameter>
            <CssParameter name="stroke-width">2.0</CssParameter>
          </Stroke>
        </Mark>
        <Size>5</Size>
      </Graphic>
    </GraphicFill>
  </Fill>
</PolygonSymbolizer>',null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2008,'jurisdiction_and_no_policies_difference < -140','168D36','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2008,'jurisdiction_and_no_policies_difference >= -140 && jurisdiction_and_no_policies_difference < -70','3FAC5D','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2008,'jurisdiction_and_no_policies_difference >= -70 && jurisdiction_and_no_policies_difference < -35','7AC48F','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2008,'jurisdiction_and_no_policies_difference >= -35 && jurisdiction_and_no_policies_difference < -10','A0DDB2','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2008,'jurisdiction_and_no_policies_difference >= -10 && jurisdiction_and_no_policies_difference <= 10','ECF2C9','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2008,'jurisdiction_and_no_policies_difference > 10','7C3494','FFFFFF',null,null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2009,'delta_deposition_jurisdiction_policies <= 50','F0F9E8','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2009,'delta_deposition_jurisdiction_policies > 50 && delta_deposition_jurisdiction_policies <= 100','BAE4BC','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2009,'delta_deposition_jurisdiction_policies > 100 && delta_deposition_jurisdiction_policies <= 175','7BCCC4','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2009,'delta_deposition_jurisdiction_policies > 175 && delta_deposition_jurisdiction_policies <= 250','43A2CA','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2009,'delta_deposition_jurisdiction_policies > 250','0868AC','FFFFFF',null,null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2010,'delta_deposition_jurisdiction_policies > 0.05','000080','000080',null,null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies < -250','6C8B41','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies >= -250 && delta_deposition_jurisdiction_policies < -175','89A267','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies >= -175 && delta_deposition_jurisdiction_policies < -100','A7B98D','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies >= -100 && delta_deposition_jurisdiction_policies < -50','C4D1B3','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies >= -50 && delta_deposition_jurisdiction_policies < -1','E2E8D9','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies >= -1 && delta_deposition_jurisdiction_policies <= 1','F2F2F2','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies > 1 && delta_deposition_jurisdiction_policies <= 50','DCD9E9','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies > 50 && delta_deposition_jurisdiction_policies <= 100','B9B3D4','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies > 100 && delta_deposition_jurisdiction_policies <= 175','978CBE','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies > 175 && delta_deposition_jurisdiction_policies <= 250','7466A9','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2011,'delta_deposition_jurisdiction_policies > 250','514093','FFFFFF',null,null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2012,null,null,null,
'<PolygonSymbolizer>
  <Fill>
    <GraphicFill>
      <Graphic>
        <Mark>
          <WellKnownName>shape://dot</WellKnownName>
          <Stroke>
            <CssParameter name="stroke">#153C6E</CssParameter>
            <CssParameter name="stroke-width">2.5</CssParameter>
            <CssParameter name="stroke-opacity">0.8</CssParameter>
            <CssParameter name="stroke-linejoin">round</CssParameter>
          </Stroke>
        </Mark>
        <Size>8</Size>
      </Graphic>
    </GraphicFill>
  </Fill>
  <Stroke>
    <CssParameter name="stroke">#153C6E</CssParameter>
    <CssParameter name="stroke-width">0</CssParameter>
    <CssParameter name="stroke-opacity">1</CssParameter>
  </Stroke>
</PolygonSymbolizer>',null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2013,'deviation_from_critical_deposition < 0','6C8B41','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2013,'deviation_from_critical_deposition >= 0 && deviation_from_critical_deposition <= 30','F2F0F7','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2013,'deviation_from_critical_deposition > 30 && deviation_from_critical_deposition <= 60','DADAEB','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2013,'deviation_from_critical_deposition > 60 && deviation_from_critical_deposition <= 90','BCBDDC','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2013,'deviation_from_critical_deposition > 90 && deviation_from_critical_deposition <= 120','9E9AC8','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2013,'deviation_from_critical_deposition > 120 && deviation_from_critical_deposition <= 150','807DBA','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2013,'deviation_from_critical_deposition > 150 && deviation_from_critical_deposition <= 200','6A51A3','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2013,'deviation_from_critical_deposition > 200','4A1486','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2014,'delta_space_desire_range = shortage_70+','514093','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2014,'delta_space_desire_range = shortage_35-70','7466A9','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2014,'delta_space_desire_range = shortage_1-35','978CBE','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2014,'delta_space_desire_range = equal','F2F2F2','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2014,'delta_space_desire_range = surplus_1-35','C4D1B3','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2014,'delta_space_desire_range = surplus_35-70','89A267','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (2014,'delta_space_desire_range = surplus_70+','6C8B41','FFFFFF',null,null,null);

--Product specific WMS layers. Can refer to sld_id 1-999 (common) and 2001-2999 (monitor).
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (1,1,'monitor:wms_depositions_jurisdiction_policies_view','Totale depositie');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (1,201,'monitor_report:wms_assessment_area_receptor_depositions_view','Totale depositie');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2,2,'monitor:wms_habitat_areas_sensitivity_view','Stikstofgevoelige habitattypen');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (3,2,'monitor:wms_nature_areas_view','Natuurgebieden');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (7,4,'monitor:wms_receptors_to_habitats_with_relevant_geometry_view','Habitattypes per receptor');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (7,4,'monitor:wms_habitats_view','Habitattype voor natuurgebied');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (8,1,'monitor:wms_critical_deposition_area_receptor_depositions_view','Deposities per receptor, habitat en assessment area');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (8,1,'monitor:wms_critical_deposition_area_receptor_delta_depositions_view','Delta deposities per receptor, habitat en assessment area');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (8,1,'monitor:wms_critical_deposition_area_receptor_deviations_view','Afstand-tot-KDW per receptor, habitat en assessment area');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (9,5,'monitor:wms_habitat_types','Habitattypen');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (14,5,'monitor:wms_province_areas_view', 'Provincegrenzen');

INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2001,1,'monitor:wms_sector_depositions_jurisdiction_policies_view','Totale depositie per sector');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2001,1,'monitor:wms_other_depositions_jurisdiction_policies_view','Depositie buitenland');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title, target_layer_name)
	VALUES (2003,2,'monitor_report:wms_nature_areas_view','Gebiedssamenvatting - Natuurgebieden','monitor:wms_nature_areas_view');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2004,201,'monitor_report:wms_assessment_area_receptor_nitrogen_loads_view','Gebiedssamenvatting - Stikstofoverbelasting per natuurgebied/leefgebied');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2005,1,'monitor:wms_assessment_area_receptor_deposition_spaces_view','Depositieruimte');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title, target_layer_name)
	VALUES (2005,201,'monitor_report:wms_assessment_area_receptor_deposition_spaces_view','Gebiedssamenvatting - Totale depositieruimte','monitor:wms_assessment_area_receptor_deposition_spaces_view');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2006,4,'monitor:wms_rehabilitation_strategies_view','Herstelmaatregelen');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2007,5,'monitor_report:wms_rehabilitation_strategy_clusters_view','Herstelmaatregelen in clusters');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2008,1,'monitor:wms_assessment_area_receptor_delta_policy_depositions_view','Effect programma [jaar] t.o.v. autonoom');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title, target_layer_name)
	VALUES (2008,201,'monitor_report:wms_assessment_area_receptor_delta_policy_depositions_view','Gebiedssamenvatting - Verschil ontwikkeling met/zonder PAS Monitor','monitor:wms_assessment_area_receptor_delta_policy_depositions_view');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2009,201,'monitor_report:wms_delta_deposition_view_decrease','Gebiedssamenvatting - Depositiedaling');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2010,2,'monitor_report:wms_delta_deposition_view_increase','Gebiedssamenvatting - Depositietoename');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2011,1,'monitor:wms_assessment_area_receptor_delta_depositions_view','Depositieontwikkeling [jaar] t.o.v. 2014');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2012,1,'monitor:wms_included_receptors_view','Relevante hexagonen');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2013,1,'monitor:wms_deviations_from_critical_deposition_view','Afstand tot de KDW voor zichtjaar');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2014,1,'monitor:wms_delta_space_desire_view','Ontwikkelingssaldo');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (9,5,'monitor_report:wms_habitats_view','GsBijlage - Habitattype');

