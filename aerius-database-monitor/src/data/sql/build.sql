-- Toepassen van correcties op receptoren met een te korte rekenafstand tot de bronnen
BEGIN;
	SELECT ae_raise_notice('Build: ae_apply_overkill_corrections() @ ' || timeofday());

	SELECT setup.ae_apply_overkill_corrections();
COMMIT;


-- Emissie reductie weidereducties
BEGIN;
	SELECT ae_raise_notice('Build: farm_emission_correction_factors_grazing @ ' || timeofday());

	INSERT INTO farm_emission_correction_factors_grazing (jurisdiction_id, farm_animal_category_id, substance_id, correction_factor, description)
		SELECT jurisdiction_id, farm_animal_category_id, substance_id, correction_factor, description FROM setup.build_farm_emission_correction_factors_grazing_view;
COMMIT;


-- Sources
BEGIN;
	SELECT ae_raise_notice('Build: farm_sites @ ' || timeofday());
	
	UPDATE farm_sites SET recreational = build_farm_sites_view.recreational
		FROM setup.build_farm_sites_view 
		WHERE farm_sites.site_id = build_farm_sites_view.site_id;
COMMIT;

BEGIN;
	SELECT ae_raise_notice('Build: farm_site_suspenders @ ' || timeofday());

	INSERT INTO farm_site_suspenders (site_id, farm_emission_ceiling_category_id, suspender)
		SELECT site_id, farm_emission_ceiling_category_id, suspender FROM setup.build_farm_site_suspenders_view;
COMMIT;


-- Emissieplafonds
BEGIN;
	SELECT ae_raise_notice('Build: farm_emission_ceilings_no_policies @ ' || timeofday());

	INSERT INTO farm_emission_ceilings_no_policies (farm_emission_ceiling_category_id, year, substance_id, emission_ceiling, description)
		SELECT farm_emission_ceiling_category_id, year, substance_id, emission_ceiling, description FROM setup.build_farm_emission_ceilings_no_policies_view;
COMMIT;

BEGIN;
	SELECT ae_raise_notice('Build: farm_emission_ceilings_global_policies @ ' || timeofday());

	INSERT INTO farm_emission_ceilings_global_policies (farm_emission_ceiling_category_id, year, substance_id, emission_ceiling, description)
		SELECT farm_emission_ceiling_category_id, year, substance_id, emission_ceiling, description FROM setup.build_farm_emission_ceilings_global_policies_view;
COMMIT;

BEGIN;
	SELECT ae_raise_notice('Build: farm_emission_ceilings_jurisdiction_policies @ ' || timeofday());

	INSERT INTO farm_emission_ceilings_jurisdiction_policies (jurisdiction_id, farm_emission_ceiling_category_id, year, substance_id, emission_ceiling, description)
		SELECT jurisdiction_id, farm_emission_ceiling_category_id, year, substance_id, emission_ceiling, description FROM setup.build_farm_emission_ceilings_jurisdiction_policies_view;
COMMIT;


-- NEMA emissie correctie (wordt tijdens de build bepaald op basis van de totale landbouw-emissie so-far; dus zonder voermanagement)
BEGIN;
	SELECT ae_raise_notice('Build: farm_emission_correction_factors_nema @ ' || timeofday());

	INSERT INTO farm_emission_correction_factors_nema (farm_nema_cluster_id, substance_id, correction_factor)
		SELECT farm_nema_cluster_id, substance_id, correction_factor FROM setup.build_farm_emission_correction_factors_nema_view;
COMMIT;


-- Emissie reductie voermanagement (wordt tijdens de build bepaald op basis van de totale landbouw-emissie so-far; dus inclusief NEMA-correctie)
BEGIN;
	SELECT ae_raise_notice('Build: farm_emission_correction_factors_fodder @ ' || timeofday());

	INSERT INTO farm_emission_correction_factors_fodder (farm_animal_category_id, year, substance_id, correction_factor, description)
		SELECT farm_animal_category_id, year, substance_id, correction_factor, description FROM setup.build_farm_emission_correction_factors_fodder_view;
COMMIT;



-- Totale reductie
BEGIN;
	SELECT ae_raise_notice('Build: reductions_jurisdiction_policies @ ' || timeofday());

	INSERT INTO reductions_jurisdiction_policies(receptor_id, year, reduction)
		SELECT receptor_id, year, reduction FROM setup.build_reductions_jurisdiction_policies_view;
COMMIT;



-- Groei

SELECT ae_raise_notice('Build: setup.sector_economic_growths @ ' || timeofday());

{multithread on: SELECT unnest(enum_range(NULL::setup.sectorgroup)) AS sector_group ORDER BY sector_group}
	SELECT setup.ae_build_sector_economic_growths('{sector_group}');
{/multithread}


-- Ontwikkelingsbehoefte

SELECT ae_raise_notice('Build: sector_economic_desires @ ' || timeofday());

{multithread on: SELECT unnest(enum_range(NULL::setup.sectorgroup)) AS sector_group ORDER BY sector_group}
	SELECT setup.ae_build_sector_economic_desires('{sector_group}');
{/multithread}

BEGIN;
	SELECT ae_raise_notice('Build: economic_desires @ ' || timeofday());

	INSERT INTO economic_desires(year, receptor_id, total_desire)
		SELECT year, receptor_id, total_desire FROM setup.build_economic_desires_view;
COMMIT;


-- Depositie (per sector)

SELECT ae_raise_notice('Build: sector_depositions_no_policies @ ' || timeofday());

{multithread on: SELECT unnest(enum_range(NULL::setup.sectorgroup)) AS sector_group ORDER BY sector_group}
	SELECT setup.ae_build_sector_depositions_no_policies('{sector_group}');
{/multithread}

BEGIN;
	SELECT ae_raise_notice('Build: sector_depositions_global_policies @ ' || timeofday());

	INSERT INTO sector_depositions_global_policies (year, sector_id, receptor_id, deposition)
		SELECT year, sector_id, receptor_id, deposition FROM setup.build_sector_depositions_global_policies_view;
COMMIT;

BEGIN;
	SELECT ae_raise_notice('Build: sector_depositions_jurisdiction_policies @ ' || timeofday());

	INSERT INTO sector_depositions_jurisdiction_policies(year, sector_id, receptor_id, deposition)
		SELECT year, sector_id, receptor_id, deposition FROM setup.build_sector_depositions_jurisdiction_policies_view;
COMMIT;


-- Other depositions
BEGIN;
	SELECT ae_raise_notice('Build: other_depositions @ ' || timeofday());

	SELECT setup.ae_build_other_depositions();
COMMIT;


-- Depositieruimte
BEGIN;
	SELECT ae_raise_notice('Build: setup.deposition_spaces @ ' || timeofday());

	INSERT INTO setup.deposition_spaces (year, receptor_id, total_space, total_space_addition)
		SELECT year, receptor_id, total_space, total_space_addition FROM setup.build_deposition_spaces_view;
COMMIT;

BEGIN;
	SELECT ae_raise_notice('Build: deposition_spaces_divided @ ' || timeofday());

	INSERT INTO deposition_spaces_divided (year, receptor_id, no_permit_required, permit_threshold, priority_projects, projects)
		SELECT year, receptor_id, no_permit_required, permit_threshold, priority_projects, projects FROM setup.build_deposition_spaces_divided_view;
COMMIT;


-- Other depositions (part 2)
BEGIN;
	SELECT ae_raise_notice('Build: other_depositions @ ' || timeofday());

	SELECT setup.ae_build_other_depositions_part_2();
COMMIT;


-- Totale depositie (met other depositions)
BEGIN;
	SELECT ae_raise_notice('Build: depositions_no_policies @ ' || timeofday());

	INSERT INTO depositions_no_policies(receptor_id, year, deposition)
		SELECT receptor_id, year, deposition FROM setup.build_depositions_no_policies_view;
COMMIT;

BEGIN;
	SELECT ae_raise_notice('Build: depositions_global_policies @ ' || timeofday());

	INSERT INTO depositions_global_policies(receptor_id, year, deposition)
		SELECT receptor_id, year, deposition FROM setup.build_depositions_global_policies_view;
COMMIT;

BEGIN;
	SELECT ae_raise_notice('Build: depositions_jurisdiction_policies @ ' || timeofday());

	INSERT INTO depositions_jurisdiction_policies(receptor_id, year, deposition)
		SELECT receptor_id, year, deposition FROM setup.build_depositions_jurisdiction_policies_view;
COMMIT;


-- Delta depositie
BEGIN;
	SELECT ae_raise_notice('Build: delta_depositions_no_policies @ ' || timeofday());

	INSERT INTO delta_depositions_no_policies(receptor_id, year, delta_deposition)
		SELECT receptor_id, year, delta_deposition FROM setup.build_delta_depositions_no_policies_view;
COMMIT;

BEGIN;
	SELECT ae_raise_notice('Build: delta_depositions_global_policies @ ' || timeofday());

	INSERT INTO delta_depositions_global_policies(receptor_id, year, delta_deposition)
		SELECT receptor_id, year, delta_deposition FROM setup.build_delta_depositions_global_policies_view;
COMMIT;

BEGIN;
	SELECT ae_raise_notice('Build: delta_depositions_jurisdiction_policies @ ' || timeofday());

	INSERT INTO delta_depositions_jurisdiction_policies(receptor_id, year, delta_deposition)
		SELECT receptor_id, year, delta_deposition FROM setup.build_delta_depositions_jurisdiction_policies_view;
COMMIT;


-- Receptoren met KDW overschrijding
BEGIN;
	SELECT ae_raise_notice('Build: exceeding_receptors @ ' || timeofday());

	INSERT INTO exceeding_receptors(receptor_id, year)
		SELECT receptor_id, year FROM setup.build_exceeding_receptors_view;
COMMIT;


-- OR relevante receptoren die enkel in een NIET PAS gebied vallen
BEGIN;
	SELECT ae_raise_notice('Build: override_relevant_development_space_receptors @ ' || timeofday());

	INSERT INTO override_relevant_development_space_receptors(receptor_id, year, relevant, reason)
		SELECT receptor_id, year, relevant, reason FROM setup.build_override_relevant_development_space_receptors_view;
COMMIT;