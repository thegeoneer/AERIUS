BEGIN; SELECT setup.ae_load_table('setup.sectors_sectorgroup', '{data_folder}/setup/setup.sectors_sectorgroup_20160617.txt'); COMMIT;


BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_scale_factors', '{data_folder}/setup/setup.gcn_sector_economic_scale_factors_20160922.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_scale_factors_no_economy', '{data_folder}/setup/setup.gcn_sector_economic_scale_factors_no_economy_20160922.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_growth_factors', '{data_folder}/setup/setup.gcn_sector_economic_growth_factors_20160922.txt'); COMMIT;


BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_growth_factor_corrections', '{data_folder}/setup/setup.gcn_sector_economic_growth_factor_corrections_20161130.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.sector_priority_project_economic_growth_limiter_factors', '{data_folder}/setup/setup.sector_priority_project_economic_growth_limiter_factors_20161130.txt'); COMMIT;


BEGIN; SELECT setup.ae_load_table('setup.sector_deposition_space_segmentations', '{data_folder}/setup/setup.sector_deposition_space_segmentations_20160712.txt'); COMMIT;