/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.api.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.HttpClientManager;

@RunWith(Parameterized.class)
public class ConnectIntegrationTest {

  enum HttpMethod {
    POST, GET, DELETE
  };

  enum ApiRequests {
    CALCULATE("calculate", HttpMethod.POST, true),
    REPORT("report", HttpMethod.POST, true),
    STATUS_JOBS("status/jobs", HttpMethod.GET, false),
    GENERATEAPIKEY("generateAPIKey", HttpMethod.POST, false),
    CONVERT("convert", HttpMethod.POST, false),
    VALUEPERHEXAGON_HIGHEST("valuePerHexagon/highest", HttpMethod.POST, false),
    VALUEPERHEXAGON_TOTAL("valuePerHexagon/total", HttpMethod.POST, false),
    VALUEPERHEXAGON_DELTA("valuePerHexagon/delta", HttpMethod.POST, false),
    RECEPTORSET_DELETE("receptorSet/receptorsetName", HttpMethod.DELETE, false),
    RECEPTORSET("receptorSet", HttpMethod.POST, false),
    RECEPTORSETS("receptorSets", HttpMethod.GET, false),
    VALIDATE("validate", HttpMethod.POST, false);

    private String apiRequest;
    private HttpMethod httpMethod;
    private boolean jobStatus;

    ApiRequests(final String apiRequest, final HttpMethod httpMethod, final boolean jobStatus) {
      this.apiRequest = apiRequest;
      this.httpMethod = httpMethod;
      this.jobStatus = jobStatus;
    }

    public String getApiRequest() {
      return apiRequest;
    }

    public HttpMethod getHttpMethod() {
      return httpMethod;
    }

    public boolean isJobStatus() {
      return jobStatus;
    }
  };

  /**
   * Example: http://localhost:8080/api/5/
   */
  private static final String PROP_CONNECT_API_URL = "connect.api.url";
  private static final String PROP_CONNECT_API_KEY = "connect.api.key";

  private static final String APIURL = System.getProperty(PROP_CONNECT_API_URL);
  private static final String APIKEY = System.getProperty(PROP_CONNECT_API_KEY);
  private static final int API_TIMEOUT = 30;
  private static final String STATUS_JOBS = "status/jobs?apiKey=[apiKey]";
  private static final String INVALID_RESPONSE_FILE_NAME_PART = "invalid";
  private static final String REPLACE_API_KEY = "[apiKey]";
  private static final String IGNORE_KEY = "(,\"key\":\")[-0-9a-fA-F]{36}";
  private static final String IGNORE_IMAER_REFERENCE_BLOCK = "<imaer:reference>.*?<\\/imaer:reference>";
  private static final String IGNORE_IMAER_DATABASE_VERSION_BLOCK = "<imaer:databaseVersion>.*?</imaer:databaseVersion>";
  private static final String JOB_ENTRIES = "entries";

  private final String fileRequest;
  private final String fileResponse;
  private final ApiRequests method;

  /**
   * Initialize test with method and file(s) to test.
   */
  public ConnectIntegrationTest(final ApiRequests apiRequest, final String fileRequest, final String fileResponse) {
    this.method = apiRequest;
    this.fileRequest = fileRequest;
    this.fileResponse = fileResponse;
  }

  @Before
  public void validateProperties() {
    if (StringUtils.isEmpty(APIKEY)) {
      fail("This unittest is missing the mandatory property " + PROP_CONNECT_API_KEY + ".");
    }
    if (StringUtils.isEmpty(APIURL)) {
      fail("This unittest is missing the mandatory property " + PROP_CONNECT_API_URL + ".");
    }
  }

  @Parameters(name = "{0} - {1} - {2}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> jobs = new ArrayList<>();

    for (final ApiRequests apiRequest : ApiRequests.values()) {
      final String fileExtentionType = apiRequest.getHttpMethod() == HttpMethod.POST ? "json" : "txt";
      final String fileLocation = ConnectIntegrationTest.class.getResource(apiRequest.getApiRequest() + "/").getFile();
      final String[] requestFiles = new File(fileLocation).list(new FilenameFilter() {

        @Override
        public boolean accept(final File dir, final String name) {
          return name.endsWith("-request." + fileExtentionType);
        }
      });
      for (final String requestFile : requestFiles) {
        final Object[] data = new Object[3];
        data[0] = apiRequest;
        data[1] = apiRequest.getApiRequest() + "/" + requestFile;
        data[2] = apiRequest.getApiRequest() + "/" + requestFile.replace("-request." + fileExtentionType, "-response." + "json");
        jobs.add(data);
      }
    }
    return jobs;
  }

  @Test(timeout = 10000)
  public void testConnectAPIRequests() throws ParseException, URISyntaxException, IOException, AeriusException, HttpException {
    final String fileRequestContent = replaceRequestParameters(getFileContent(fileRequest));
    final String fileResponseContent = getFileContent(fileResponse);

    String preResult = null;
    if (method.isJobStatus()) {
      preResult = connectAPIGetMethod(replaceRequestParameters(STATUS_JOBS));
    }
    String result = "";
    switch (method.getHttpMethod()) {
      case GET:
        result = connectAPIGetMethod(method.getApiRequest() + "/" + fileRequestContent);
        break;
      case POST:
        result = connectAPIPostMethod(method.getApiRequest(), fileRequestContent);
        break;
      case DELETE:
        result = connectAPIDeleteMethod(method.getApiRequest() + "/" + fileRequestContent);
        break;
    }

    // ignore the entries content for status/jobs
    result = replaceResponseParameters(fileResponseContent, result);

    assertEquals("Result the same ", replaceIgnoreBlocks(fileResponseContent), replaceIgnoreBlocks(result));

    if (method.isJobStatus()) {
      final String postResult = connectAPIGetMethod(replaceRequestParameters(STATUS_JOBS));
      // test if we expect that the apiRequest is put on the status/job result list
      if (fileRequest.toLowerCase().contains(INVALID_RESPONSE_FILE_NAME_PART)) {
        assertEquals("Status jobs entries result count should not have been changed", jobEntriesCount(preResult), jobEntriesCount(postResult));
      } else {
        assertNotEquals("Status jobs result should have been changed", preResult, postResult);
      }
    }
  }

  private int jobEntriesCount(final String preResult) {
    final JsonObject result = (JsonObject) new JsonParser().parse(preResult);
    final JsonArray rows = result.getAsJsonArray(JOB_ENTRIES);
    return rows.size();
  }

  /*
   * We should ignore some xml blocks, and key value, they will change.
   */
  private String replaceIgnoreBlocks(final String inputText) {
    final String result = inputText.replaceAll(IGNORE_IMAER_REFERENCE_BLOCK, "");
    return result.replaceAll(IGNORE_IMAER_DATABASE_VERSION_BLOCK, "").replaceAll(IGNORE_KEY, "$1");
  }

  private String replaceRequestParameters(final String fileRequestContent) {
    return fileRequestContent.replace(REPLACE_API_KEY, APIKEY);
  }

  private String replaceResponseParameters(final String fileResponseContent, final String result) {
    if (fileResponseContent.contains("*")) {
      String newResult = result.substring(0, fileResponseContent.indexOf("*"));
      newResult += fileResponseContent.substring(fileResponseContent.indexOf("*"));
      return newResult;
    }
    return result;
  }

  /**
   * Read file from resource location
   *
   * @param filepath resource location.
   * @return content of the file.
   * @throws IOException.
   */
  public String getFileContent(final String filepath) throws IOException {
    try (InputStream inputStream = getClass().getResourceAsStream("./" + filepath)) {
      return IOUtils.toString(inputStream);
    }
  }

  /**
   * Post method for connect API request
   *
   * @param cmd url api request path.
   * @param requestData payload.
   * @return result from service call.
   * @throws ParseException
   * @throws URISyntaxException
   * @throws IOException
   * @throws AeriusException
   * @throws HttpException
   */
  private String connectAPIPostMethod(final String cmd, final String requestData) throws ParseException, URISyntaxException, IOException,
      AeriusException, HttpException {
    try (final CloseableHttpClient httpClient = getHttpClient()) {
      return HttpClientProxy.postJsonRemoteContent(httpClient, APIURL + cmd, requestData);
    } catch (final HttpException e) {
      return e.getContent();
    }
  }

  /**
   * Get method for Connect API request.
   *
   * @param cmd url request path.
   * @return result from service call.
   * @throws IOException
   * @throws HttpException
   * @throws URISyntaxException
   * @throws ParseException
   */
  public String connectAPIGetMethod(final String cmd)
      throws IOException, ParseException, URISyntaxException, HttpException {
    try (final CloseableHttpClient httpClient = getHttpClient()) {
      return new String(HttpClientProxy.getRemoteContent(httpClient, APIURL + cmd),
          StandardCharsets.UTF_8.name());
    } catch (final HttpException e) {
      return e.getContent();
    }
  }
  
  /**
   * Delete method for Connect API request.
   *
   * @param cmd url request path.
   * @return result from service call.
   * @throws IOException
   * @throws HttpException
   * @throws URISyntaxException
   * @throws ParseException
   */
  public String connectAPIDeleteMethod(final String cmd)
      throws IOException, ParseException, URISyntaxException, HttpException {
    try (final CloseableHttpClient httpClient = getHttpClient()) {
        return HttpClientProxy.deleteRemoteContent(httpClient, APIURL + cmd);
      } catch (final HttpException e) {
        return e.getContent();
      }
  }

  protected CloseableHttpClient getHttpClient() {
    return HttpClientManager.getHttpClient(API_TIMEOUT);
  }

}
