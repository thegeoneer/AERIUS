/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.eherkenning;

import org.apache.shiro.authc.AuthenticationToken;
import org.federatenow.er4b.sp.client.entities.AcsResponse;

/**
 * {@link AuthenticationToken} containing the eherkenning returned object.
 */
public class EHerkenningToken implements AuthenticationToken {

  private static final long serialVersionUID = 8755314716979321615L;

  private final AcsResponse acsResponse;

  /**
   * Constructor initialized with response object.
   * @param acsResponse
   */
  public EHerkenningToken(final AcsResponse acsResponse) {
    this.acsResponse = acsResponse;
  }

  public AcsResponse getAcsResponse() {
    return acsResponse;
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public Object getPrincipal() {
    return acsResponse;
  }
}
