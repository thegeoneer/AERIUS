/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.eherkenning;

import java.util.List;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.CollectionUtils;

/**
 * Realm implementation for eHerkenning library.
 *
 * Configuration {@code shiro.ini} for the realm:
 * <pre>
 * [main]
 * ...
 * eHerkenningRealm = nl.overheid.aerius.server.eherkenning.EHerkenningRealm
 * </pre>
 */
public class EHerkenningRealm extends AuthorizingRealm {

  public EHerkenningRealm() {
    setAuthenticationTokenClass(EHerkenningToken.class);
  }

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(final PrincipalCollection principals) {
    return new SimpleAuthorizationInfo();
  }

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token) throws AuthenticationException {
    AuthenticationInfo aInfo;
    if (token == null) {
      aInfo = null;
    } else {
      final EHerkenningToken eToken = (EHerkenningToken) token;
      final List<? extends Object> principals = CollectionUtils.asList(eToken.getPrincipal(), eToken);
      final PrincipalCollection principalCollection = new SimplePrincipalCollection(principals, getName());
      aInfo = new SimpleAuthenticationInfo(principalCollection, eToken.getCredentials());
    }
    return aInfo;
  }

  @Override
  protected void assertCredentialsMatch(final AuthenticationToken token, final AuthenticationInfo info)
      throws AuthenticationException {
    // Don't assert credentials, they are managed by eHerkenning and not available here.
  }
}
