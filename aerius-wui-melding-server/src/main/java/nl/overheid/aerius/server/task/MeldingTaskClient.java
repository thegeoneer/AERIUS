/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.task;

import java.io.IOException;

import nl.overheid.aerius.register.MeldingPayloadData;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.TaskResultCallback;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Class to send a Register-specific task to the workers.
 */
public final class MeldingTaskClient {

  private MeldingTaskClient() {
    // utility class
  }

  /**
   * Invoked when a melding has to be handled by Register app (or worker).
   * @param client the message bus client
   * @param payload zip content for complete melding
   * @param callback The callback that will be called when the melding task is finished.
   * @throws IOException In case of errors sending the task to the worker.
   */
  public static void startMeldingTask(final TaskManagerClient client, final byte[] payload, final TaskResultCallback callback) throws IOException {
    final MeldingPayloadData inputData = new MeldingPayloadData();
    inputData.setPayload(payload);
    client.sendTask(inputData, callback, WorkerType.MELDING, QueueEnum.MELDING);
  }
}
