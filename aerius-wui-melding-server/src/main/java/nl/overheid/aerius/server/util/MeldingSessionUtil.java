/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.SharedConstants;

/**
 * Simplest post-payload-store-to-session thing ever.
 *
 * Unsure about how we'll be using this later on (uuid's, persisting in db instead, something else?), so no point doing something super-complex.
 */
public final class MeldingSessionUtil {
  private static final Logger LOG = LoggerFactory.getLogger(MeldingSessionUtil.class);

  private MeldingSessionUtil() {}

  public static Path getUploadPath(final HttpSession session, final String uuid) throws IOException {
    final Path sessionTempDir = (Path) getAttribute(session, uuid, SharedConstants.MELDING_UPLOAD_DIRECTORY);
    final Path tempDir;
    if (sessionTempDir == null) {
      tempDir = Files.createTempDirectory(SharedConstants.MELDING_UPLOAD_DIRECTORY);
      setAttribute(session, uuid, SharedConstants.MELDING_UPLOAD_DIRECTORY, tempDir);
    } else {
      tempDir = sessionTempDir;
    }
    return tempDir;
  }

  public static File addUploadFile(final HttpSession session, final String uuid, final String filename) throws IOException {
    final Path tempDir = MeldingSessionUtil.getUploadPath(session, uuid);
    final Map<String, File> uploadFiles = getUploadFiles(session, uuid);
    final File file = new File(tempDir.toFile(), filename);
    uploadFiles.put(filename, file);
    return file;
  }

  @SuppressWarnings("unchecked")
  public static Map<String, File> getUploadFiles(final HttpSession session, final String uuid) {
    final Map<String, File> sessionFileList = (Map<String, File>) getAttribute(session, uuid, SharedConstants.MELDING_UPLOAD_FILES);
    final Map<String, File> fileList;
    if (sessionFileList == null) {
      fileList = new HashMap<String, File>();
      setAttribute(session, uuid, SharedConstants.MELDING_UPLOAD_FILES, fileList);
    } else {
      fileList = sessionFileList;
    }
    return fileList;
  }

  public static String getGMLFromSession(final HttpSession session, final String uuid, final String key) {
    return (String) session.getAttribute(getUUIDKey(uuid, key));
  }

  public static String setGMLInSession(final HttpServletRequest request) {
    final String gmlProprosed = request.getParameter(SharedConstants.MELDING_GML_PROPOSED);
    final HttpSession session = request.getSession(true);
    final String uuid = gmlProprosed == null || gmlProprosed.isEmpty() ? null : request.getParameter(SharedConstants.MELDING_UUID_PARAM);

    if (uuid != null) {
      LOG.info("Put a proposed GML in session with ID {} with UUID {}", session.getId(), uuid);
      setAttribute(session, uuid, SharedConstants.MELDING_GML_PROPOSED, gmlProprosed);
      if (!StringUtils.isEmpty(request.getParameter(SharedConstants.MELDING_GML_CURRENT))) {
        LOG.info("Put a current GML in session with ID {} with UUID {}", session.getId(), uuid);
        setAttribute(session, uuid, SharedConstants.MELDING_GML_CURRENT, request.getParameter(SharedConstants.MELDING_GML_CURRENT));
      }
    }
    return uuid;
  }

  private static Object getAttribute(final HttpSession session, final String uuid, final String key) {
    return session.getAttribute(getUUIDKey(uuid, key));
  }

  private static void setAttribute(final HttpSession session, final String uuid, final String key, final Object value) {
    session.setAttribute(getUUIDKey(uuid, key), value);
  }

  private static String getUUIDKey(final String uuid, final String property) {
    return (uuid == null ? "" : uuid) + '-' + property;
  }
}
