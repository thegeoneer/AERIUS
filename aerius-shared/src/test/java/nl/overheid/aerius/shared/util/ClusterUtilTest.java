/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.ClusteredReceptorPoint;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;

public class ClusterUtilTest {
  public static final double PROXIMITY_THRESHOLD = 3 * 1000;
  private static final HexagonZoomLevel ZOOM_LEVEL_1 = new HexagonZoomLevel(1, 10000);

  @Test
  public void testFarSeperation() {
    final Point p1 = new Point(0, 0);
    final Point p2 = new Point(PROXIMITY_THRESHOLD - 1, 0);
    final Point p3 = new Point(0, PROXIMITY_THRESHOLD - 1);

    final Point p4 = new Point(PROXIMITY_THRESHOLD * 10, PROXIMITY_THRESHOLD * 10);

    final ArrayList<Point> al = new ArrayList<Point>();
    al.add(p1);
    al.add(p2);
    al.add(p3);
    al.add(p4);

    final Set<ClusteredReceptorPoint<Point>> clusterSources = ClusterUtil.clusterGenericPoints(al, PROXIMITY_THRESHOLD);

    assertEquals("Size not 2", 2, clusterSources.size());
  }

  @Test
  public void testNegative() {
    final Point p1 = new Point(0, 0);
    final Point p2 = new Point(-PROXIMITY_THRESHOLD + 1, 0);
    final Point p3 = new Point(0, -PROXIMITY_THRESHOLD + 1);

    final ArrayList<Point> al = new ArrayList<Point>();
    al.add(p1);
    al.add(p2);
    al.add(p3);

    final Set<ClusteredReceptorPoint<Point>> clusterSources = ClusterUtil.clusterGenericPoints(al, PROXIMITY_THRESHOLD);

    assertEquals("Size not 1", 1, clusterSources.size());
  }

  @Test
  public void testMiddle() {
    final Point p1 = new Point(3604 + ZOOM_LEVEL_1.getHexagonRadius(), 296800 + ZOOM_LEVEL_1.getHexagonHeight());
    final Point p2 = new Point(3604 + ZOOM_LEVEL_1.getHexagonRadius() + (PROXIMITY_THRESHOLD / 2d) - 0.5, 296800 + ZOOM_LEVEL_1.getHexagonHeight());
    final Point p3 = new Point(3604 + ZOOM_LEVEL_1.getHexagonRadius() + PROXIMITY_THRESHOLD - 1, 296800 + ZOOM_LEVEL_1.getHexagonHeight());

    final ArrayList<Point> al = new ArrayList<Point>();
    al.add(p1);
    al.add(p2);
    al.add(p3);

    final Set<ClusteredReceptorPoint<Point>> clusterSources = ClusterUtil.clusterGenericPoints(al, PROXIMITY_THRESHOLD);

    assertEquals("Size not 1", 1, clusterSources.size());
    assertEquals(p2.getX(), clusterSources.iterator().next().getX(), ZOOM_LEVEL_1.getHexagonRadius());
    assertEquals(p2.getY(), clusterSources.iterator().next().getY(), ZOOM_LEVEL_1.getHexagonHeight());
    assertEquals(p2.getX() - p1.getX(), clusterSources.iterator().next().getMaxDistance(), ZOOM_LEVEL_1.getHexagonRadius());
  }

  @Test
  public void testMiddleForUselessCluster() {
    final Random random = new Random();

    for (int i = 0; i < 1000; i++) {
      final double xCoord = 3604 + random.nextDouble() * (227135 - 3604 + 1);
      final double yCoord = 296800 + random.nextDouble() * (619493 - 296800 + 1);

      final ArrayList<Point> al = new ArrayList<Point>();
      al.add(new Point(xCoord, yCoord));

      final Set<ClusteredReceptorPoint<Point>> clusterSources = ClusterUtil.clusterGenericPoints(al, PROXIMITY_THRESHOLD);

      assertEquals("Size not 1", 1, clusterSources.size());
      assertEquals("max distance not 0", 0, clusterSources.iterator().next().getMaxDistance(), ZOOM_LEVEL_1.getHexagonRadius());
    }
  }

}
