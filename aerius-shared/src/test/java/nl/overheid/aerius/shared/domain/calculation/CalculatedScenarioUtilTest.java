/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;

/**
 * Test class for {@link CalculatedScenarioUtil}.
 */
public class CalculatedScenarioUtilTest {

  @Test
  public void testToCalculatedScenario() {
    final Scenario scenario = new Scenario();
    final EmissionSourceList esl1 = new EmissionSourceList();
    esl1.setId(100);
    esl1.add(new GenericEmissionSource());
    scenario.addSources(esl1);
    CalculatedScenario cs = CalculatedScenarioUtil.toCalculatedScenario(scenario);
    assertSame("Instanceof check  should be CalculatedSingle", CalculatedSingle.class, cs.getClass());
    assertEquals("Check if nr of sources is still 1.", 1, ((CalculatedSingle) cs).getSources().size());
    final EmissionSourceList esl2 = new EmissionSourceList();
    esl2.setId(50);
    esl2.add(new GenericEmissionSource());
    scenario.addSources(esl2);
    cs = CalculatedScenarioUtil.toCalculatedScenario(scenario);
    assertSame("Instanceof check should be CalculatedComparison", CalculatedComparison.class, cs.getClass());
    assertEquals("Check if nr of sources is of one is 1.", 1, ((CalculatedComparison) cs).getSourcesOne().size());
    assertEquals("Check if nr of sources is of two is 1.", 1, ((CalculatedComparison) cs).getSourcesTwo().size());
  }

  @Test
  public void testToCalculatedScenarioSingle() {
    final Scenario scenario = new Scenario();
    final EmissionSourceList esl1 = new EmissionSourceList();
    esl1.add(new GenericEmissionSource());
    scenario.addSources(esl1);
    // place doesn't match emission source list id, should not throw an error.
    CalculatedScenarioUtil.toCalculatedScenario(scenario, 10, -1);
    // correct place
    final CalculatedScenario cs = CalculatedScenarioUtil.toCalculatedScenario(scenario, 0, -1);
    assertSame("Instanceof check  should be CalculatedSingle", CalculatedSingle.class, cs.getClass());
    assertEquals("Check if nr of sources is still 1.", 1, ((CalculatedSingle) cs).getSources().size());
  }


  @Test
  public void testToCalculatedScenarioComparisonEmpty() {
    final Scenario scenario = new Scenario();
    final EmissionSourceList esl1 = new EmissionSourceList();
    esl1.add(new GenericEmissionSource());
    scenario.addSources(esl1);
    scenario.addSources(new EmissionSourceList());
    // place doesn't match emission source list id, should not throw an error.
    CalculatedScenarioUtil.toCalculatedScenario(scenario, 10, -1);
    // correct place
    final CalculatedScenario cs = CalculatedScenarioUtil.toCalculatedScenario(scenario, 0, 1);
    assertSame("Instanceof check  should be CalculatedSingle", CalculatedComparison.class, cs.getClass());
    assertEquals("Check if nr of sources of one is still 1.", 1, ((CalculatedComparison) cs).getSourcesOne().size());
    assertEquals("Check if nr of sources of two is still 0.", 0, ((CalculatedComparison) cs).getSourcesTwo().size());
  }

  @Test
  public void testToCalculatedScenarioComparison() {
    final Scenario scenario = new Scenario();
    final EmissionSourceList esl1 = new EmissionSourceList();
    esl1.add(new GenericEmissionSource());
    scenario.addSources(esl1);
    final EmissionSourceList esl2 = new EmissionSourceList();
    esl2.add(new GenericEmissionSource());
    scenario.addSources(esl2);
    final CalculatedScenario cs = CalculatedScenarioUtil.toCalculatedScenario(scenario, 0, 1);
    assertSame("Instanceof check should be CalculatedComparison", CalculatedComparison.class, cs.getClass());
    assertEquals("Check if nr of sources is of one is 1.", 1, ((CalculatedComparison) cs).getSourcesOne().size());
    assertEquals("Check if nr of sources is of two is 1.", 1, ((CalculatedComparison) cs).getSourcesTwo().size());
  }
}
