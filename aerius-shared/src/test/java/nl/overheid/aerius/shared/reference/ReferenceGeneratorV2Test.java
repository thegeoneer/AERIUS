/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ReferenceGeneratorV2Test {

  protected ReferenceGeneratorV2 createReferenceGeneratorV2() {
    return (ReferenceGeneratorV2) ReferenceGeneratorFactory.createReferenceGenerator(ReferenceGeneratorV2.VERSION_ID);
  }

  protected String generatePermitReference() {
    return createReferenceGeneratorV2().generateReference(ReferenceType.PERMIT);
  }

  protected String generateMeldingReference() {
    return createReferenceGeneratorV2().generateReference(ReferenceType.MELDING);
  }

  protected boolean validatePermitReference(final String reference) {
    return createReferenceGeneratorV2().validateReference(ReferenceType.PERMIT, reference);
  }

  protected boolean validateMeldingReference(final String reference) {
    return createReferenceGeneratorV2().validateReference(ReferenceType.MELDING, reference);
  }

  protected ReferenceType getReferenceType(final String reference) throws Exception {
    return createReferenceGeneratorV2().getReferenceType(reference);
  }

  protected byte getVersion(final String reference) throws Exception {
    return createReferenceGeneratorV2().getVersion(reference);
  }

  @Test
  public void testReferenceTypesDifferent() {
    for (int i = 0; i < 10; i++) {
      assertNotEquals("Melding reference and permit reference generated at same time should still never be equal",
          generateMeldingReference(), generatePermitReference());
    }
  }

  @Test
  public void testReferenceTimesDifferent() throws InterruptedException {
    String a, b;

    a = generatePermitReference();
    Thread.sleep(150);
    b = generatePermitReference();
    assertNotEquals("References generated with an interval > 100 ms should never be equal", a, b);

    a = generateMeldingReference();
    Thread.sleep(150);
    b = generateMeldingReference();
    assertNotEquals("References generated with an interval > 100 ms should never be equal", a, b);
  }

  @Test
  public void testReferenceRandomness() {
    int equalCount = 0;
    for (int i = 0; i < 100000; i++) {
      if (generatePermitReference().equals(generatePermitReference())) {
        equalCount++;
      }
    }
    assertFalse("Too much references with equal values while random bytes should prevent this", equalCount > 10);
  }

  @Test
  public void testCorrectPermitReferences() throws Exception {
    final List<String> references = new ArrayList<>();

    // Add some manual and live references
    references.add("RjqZTvBGHvWq");
    references.add("S5k1WaEG3KYs");
    references.add(generatePermitReference());
    Thread.sleep(150);
    references.add(generatePermitReference());
    Thread.sleep(250);
    references.add(generatePermitReference());

    for (final String reference : references) {
      final ReferenceType type = getReferenceType(reference);
      final byte version = getVersion(reference);

      assertEquals("Reference type", ReferenceType.PERMIT, type);
      assertEquals("Reference version", ReferenceGeneratorV2.VERSION_ID, version);
      assertTrue(reference + " not correct", validatePermitReference(reference));
      assertFalse(reference + " should not validate as melding reference", validateMeldingReference(reference));
    }
  }

  @Test
  public void testCorrectMeldingReferences() throws Exception {
    final List<String> references = new ArrayList<>();

    // Add some manual and live references
    references.add("2BC7RQUjnqGU");
    references.add("znD3nkTUYmS");
    references.add(generateMeldingReference());
    Thread.sleep(150);
    references.add(generateMeldingReference());
    Thread.sleep(250);
    references.add(generateMeldingReference());

    for (final String reference : references) {
      final ReferenceType type = getReferenceType(reference);
      final byte version = getVersion(reference);

      assertEquals("Reference type", ReferenceType.MELDING, type);
      assertEquals("Reference version", ReferenceGeneratorV2.VERSION_ID, version);
      assertTrue(reference + " not correct", validateMeldingReference(reference));
      assertFalse(reference + " should not validate as permit reference", validatePermitReference(reference));
    }
  }

  @Test
  public void testIncorrectReferences() throws Exception {
    final List<String> references = new ArrayList<>();

    // Add some manual and live incorrect references
    references.add("1uE8hNWmk");
    references.add("3Rz2TRNUHX");
    references.add("X");
    references.add("");
    references.add("S5k1WaEG3KYs=");
    references.add(generateMeldingReference() + " ");

    for (final String reference : references) {
      assertFalse(reference + " should not validate as reference", validateMeldingReference(reference));
      assertFalse(reference + " should not validate as reference", validatePermitReference(reference));
    }
  }
}
