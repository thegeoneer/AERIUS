/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.deposition;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class for {@link PasValueUtil}.
 */
public class PasValueUtilTest {

  private static final String EMPTY_STRING = "";
  private static final String HIGHER = ">";
  private static final String LOWER_EQUAL = "<=";

  
  @Test
  public void testPasProofValue() {
    assertEquals("0.006 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.006));
    assertEquals("0.005 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.005));
    assertEquals("0.004 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.004));
    assertEquals("0.0 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.0));
  }
  
  @Test
  public void testPasProofValueMelding() {
    assertEquals("0.04 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.04));
    assertEquals("0.05 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.05));
    assertEquals("0.053 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.053));
    assertEquals("0.055 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.055));
    assertEquals("0.057 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.057));
  }

  @Test
  public void testPasProofValuePermit() {
    assertEquals("1.004 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(1.004));
    assertEquals("1.0049 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(1.0049));
    assertEquals("1.005 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(1.005));
    assertEquals("1.0053 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(1.0053));
    assertEquals("1.04 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(1.04));
  }

  @Test
  public void testPasProofValueReplaceLowMelding() {
    assertEquals("0.04 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.04, true));
    assertEquals("0.05 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.05, true));
    assertEquals("0.050 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.050, true));
    assertEquals("0.052 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.052, true));
    assertEquals("0.053 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.053, true));
    assertEquals("0.055 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.055, true));
    assertEquals("0.057 = ''", EMPTY_STRING, PasValueUtil.pasProofValue(0.057, true));
  }

  @Test
  public void testPasProofValueBelowMelding() {
    assertEquals("0.00 = 0.05", 0.05, PasValueUtil.pasProofValueOverwrite(0.0), 0.01);
    assertEquals("0.04 = 0.05", 0.05, PasValueUtil.pasProofValueOverwrite(0.04), 0.001);
    assertEquals("0.05 = 0.05", 0.05, PasValueUtil.pasProofValueOverwrite(0.05), 0.001);
    assertEquals("0.052 = 0.052", 0.052, PasValueUtil.pasProofValueOverwrite(0.052), 0.001);
    assertEquals("0.053 = 0.53", 0.053, PasValueUtil.pasProofValueOverwrite(0.053), 0.001);
    assertEquals("0.055 = 0.55", 0.055, PasValueUtil.pasProofValueOverwrite(0.055), 0.001);
    assertEquals("0.057 = 0.57", 0.057, PasValueUtil.pasProofValueOverwrite(0.057), 0.001);
  }
    
  @Test 
  public void pasProoftoFixed() {
    assertEquals("-0.01 = 0.0", "0.0", String.valueOf(PasValueUtil.pasProoftoFixed(-0.01, 1)));
    assertEquals("-0.05 = -0.05", "-0.05", String.valueOf(PasValueUtil.pasProoftoFixed(-0.05, 1)));
    assertEquals("0.05 = 0.05", "0.05", String.valueOf(PasValueUtil.pasProoftoFixed(0.05, 1)));
    assertEquals("0.01 = 0.0", "0.0", String.valueOf(PasValueUtil.pasProoftoFixed(0.01, 1)));
    assertEquals("-0.001 = 0.00", "0.0", String.valueOf(PasValueUtil.pasProoftoFixed(-0.001, 2)));
    assertEquals("-0.005 = -0.005", "-0.005", String.valueOf(PasValueUtil.pasProoftoFixed(-0.005, 2)));
    assertEquals("-0.0049 = 0.0", "0.0", String.valueOf(PasValueUtil.pasProoftoFixed(-0.0049, 2)));
    assertEquals("0.001 = 0.00", "0.0", String.valueOf(PasValueUtil.pasProoftoFixed(0.001, 2)));
    assertEquals("0.005 = 0.005", "0.005", String.valueOf(PasValueUtil.pasProoftoFixed(0.005, 2)));
    assertEquals("0.0049 = 0.0", "0.0", String.valueOf(PasValueUtil.pasProoftoFixed(0.0049, 2)));
  }
}
