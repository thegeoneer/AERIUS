/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadElevation;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier.RoadSideBarrierType;

/**
 * Test for {@link SRM2EmissionSource} class.
 */
public class SRM2EmissionSourceTest extends SRM2TestBase {

  @Test
  public void testGetEmissionAB() {
    final SRM2EmissionSource rev = new SRM2EmissionSource();
    final VehicleStandardEmissions ab = new VehicleStandardEmissions();
    rev.getEmissionSubSources().add(ab);
    ab.setEmissionCategory(AUTO_BUS);
    ab.setVehicles(100, TimeUnit.DAY);
    Assert.assertEquals("SRM2EmissionSource only AUTO_BUS emission", 0.073, rev.getEmission(KEY_2013_NH3), 0.00001);
    ab.setStagnationFraction(0.5);
    Assert.assertEquals("SRM2EmissionSource only AUTO_BUS emission + stagnation", 0.09125, rev.getEmission(KEY_2013_NH3), 0.00001);
  }

  @Test
  public void testGetEmissionABTwice() {
    final SRM2EmissionSource rev = new SRM2EmissionSource();
    final VehicleStandardEmissions ab = new VehicleStandardEmissions();
    rev.getEmissionSubSources().add(ab);
    ab.setEmissionCategory(AUTO_BUS);
    ab.setVehicles(100, TimeUnit.DAY);
    Assert.assertEquals("SRM2EmissionSource with 1 AUTO_BUS emission", 0.073, rev.getEmission(KEY_2013_NH3), 0.00001);

    final VehicleStandardEmissions ab2 = new VehicleStandardEmissions();
    rev.getEmissionSubSources().add(ab2);
    ab2.setEmissionCategory(AUTO_BUS);
    ab2.setVehicles(400, TimeUnit.DAY);
    Assert.assertEquals("SRM2EmissionSource with 2nd AUTO_BUS emission", 0.073 * 5, rev.getEmission(KEY_2013_NH3), 0.00001);
  }

  @Test
  public void testGetEmissionABHF() {
    final SRM2EmissionSource rev = new SRM2EmissionSource();
    final VehicleStandardEmissions ab = new VehicleStandardEmissions();
    final VehicleStandardEmissions hf = new VehicleStandardEmissions();

    rev.getEmissionSubSources().add(ab);
    rev.getEmissionSubSources().add(hf);
    ab.setEmissionCategory(AUTO_BUS);
    ab.setVehicles(100, TimeUnit.DAY);
    hf.setEmissionCategory(HEAVY_FREIGHT);
    hf.setVehicles(100, TimeUnit.DAY);
    Assert.assertEquals("SRM2EmissionSource AUTO_BUS + HEAVY_FREIGHT emission", 0.219, rev.getEmission(KEY_2013_NH3), 0.00001);
    hf.setStagnationFraction(0.5);
    Assert.assertEquals("SRM2EmissionSource AUTO_BUS + HEAVY_FREIGHT emission + stagnation HF only", 0.23725, rev.getEmission(KEY_2013_NH3), 0.00001);
    ab.setStagnationFraction(0.5);
    Assert.assertEquals("SRM2EmissionSource AUTO_BUS + HEAVY_FREIGHT emission + stagnation", 0.2555, rev.getEmission(KEY_2013_NH3), 0.00001);
  }

  @Test
  public void testGetEmissionABHFLT() {
    final SRM2EmissionSource rev = new SRM2EmissionSource();
    final VehicleStandardEmissions ab = new VehicleStandardEmissions();
    final VehicleStandardEmissions hf = new VehicleStandardEmissions();
    final VehicleStandardEmissions lt = new VehicleStandardEmissions();

    rev.getEmissionSubSources().add(ab);
    rev.getEmissionSubSources().add(hf);
    rev.getEmissionSubSources().add(lt);
    ab.setEmissionCategory(AUTO_BUS);
    ab.setVehicles(100, TimeUnit.DAY);
    hf.setEmissionCategory(HEAVY_FREIGHT);
    hf.setVehicles(100, TimeUnit.DAY);
    lt.setEmissionCategory(LIGHT_TRAFFIC);
    lt.setVehicles(100, TimeUnit.DAY);
    Assert.assertEquals("SRM2EmissionSource AUTO_BUS + HEAVY_FREIGHT + LIGHT_TRAFFIC + emission", 0.365, rev.getEmission(KEY_2013_NH3), 0.00001);
    hf.setStagnationFraction(0.5);
    lt.setStagnationFraction(0.5);
    Assert.assertEquals("SRM2EmissionSource AUTO_BUS + HEAVY_FREIGHT + LIGHT_TRAFFIC + emission + stagnation HF + LT", 0.4015,
        rev.getEmission(KEY_2013_NH3), 0.00001);
    ab.setStagnationFraction(0.5);
    Assert.assertEquals("SRM2EmissionSource AUTO_BUS + HEAVY_FREIGHT + LIGHT_TRAFFIC + emission + stagnation", 0.41975,
        rev.getEmission(KEY_2013_NH3), 0.00001);
  }

  @Test
  public void testCopy() {
    final SRM2EmissionSource rev = new SRM2EmissionSource();
    final VehicleStandardEmissions ab = new VehicleStandardEmissions();
    rev.getEmissionSubSources().add(ab);

    ab.setEmissionCategory(AUTO_BUS);
    ab.setVehicles(100, TimeUnit.DAY);
    final SRM2EmissionSource copy = rev.copy();

    Assert.assertNotSame("Differenent object", rev, copy);
    final VehicleStandardEmissions re = (VehicleStandardEmissions) copy.getEmissionSubSources().get(0);
    Assert.assertSame("Same category", ab.getEmissionCategory(), re.getEmissionCategory());
    Assert.assertEquals("Same vehicles per year", ab.getVehiclesPerYear(), re.getVehiclesPerYear(), 0.001);
  }

  @Test
  public void testUniqueHash() {
    final SRM2EmissionSource rev = new SRM2EmissionSource();
    final VehicleStandardEmissions ab = new VehicleStandardEmissions();
    final VehicleStandardEmissions hf = new VehicleStandardEmissions();
    rev.getEmissionSubSources().add(ab);
    rev.getEmissionSubSources().add(hf);

    ab.setEmissionCategory(AUTO_BUS);
    ab.setVehicles(100, TimeUnit.DAY);
    final int uh1 = rev.getStateHash();
    ab.setVehicles(200, TimeUnit.DAY);
    final int uh2 = rev.getStateHash();
    Assert.assertFalse("StateHash diff vehicles per day (" + uh1 + " == " + uh2 + ")", uh1 == uh2);
    ab.setVehicles(100, TimeUnit.DAY);
    Assert.assertTrue("StateHash vehicles per day same again (" + uh1 + " == " + rev.getStateHash() + ")", uh1 == rev.getStateHash());
    hf.setEmissionCategory(HEAVY_FREIGHT);
    hf.setVehicles(100, TimeUnit.DAY);
    final int uh3 = rev.getStateHash();
    Assert.assertFalse("StateHash diff additional vehicle type (" + uh1 + " == " + uh3 + ")", uh1 == uh3);
  }

  @Test
  public void testSRM2TunnelFactor() {
    final SRM2EmissionSource rev = new SRM2EmissionSource();
    final VehicleStandardEmissions hf = new VehicleStandardEmissions();

    rev.getEmissionSubSources().add(hf);
    hf.setEmissionCategory(HEAVY_FREIGHT);
    hf.setVehicles(100, TimeUnit.DAY);
    Assert.assertEquals("Emission for tunnel factor default",
        HEAVY_FREIGHT.getEmissionFactor(KEY_2013_NH3) * 100 * 365 / 1000000,
        rev.getEmission(KEY_2013_NH3), 0.001);
    rev.setTunnelFactor(0.0);
    Assert.assertEquals("Emission for tunnel factor 0",
        0,
        rev.getEmission(KEY_2013_NH3), 0.001);
    rev.setTunnelFactor(1.0);
    Assert.assertEquals("Emission for tunnel factor 1",
        HEAVY_FREIGHT.getEmissionFactor(KEY_2013_NH3) * 100 * 365 / 1000000,
        rev.getEmission(KEY_2013_NH3), 0.001);
    rev.setTunnelFactor(2.0);
    Assert.assertEquals("Emission for tunnel factor 2",
        HEAVY_FREIGHT.getEmissionFactor(KEY_2013_NH3) * 100 * 2.0 * 365 / 1000000,
        rev.getEmission(KEY_2013_NH3), 0.001);
  }

  @Test
  public void testUniqueHashSRM2() {
    final SRM2EmissionSource rev = new SRM2EmissionSource();
    final VehicleStandardEmissions ab = new VehicleStandardEmissions();
    final VehicleStandardEmissions hf = new VehicleStandardEmissions();

    rev.getEmissionSubSources().add(ab);
    rev.getEmissionSubSources().add(hf);
    ab.setEmissionCategory(AUTO_BUS);
    ab.setVehicles(100, TimeUnit.DAY);
    hf.setEmissionCategory(HEAVY_FREIGHT);
    hf.setVehicles(100, TimeUnit.DAY);
    int originalHash = rev.getStateHash();
    rev.setElevation(RoadElevation.STEEP_DYKE);
    Assert.assertNotEquals("StateHash after different elevation", originalHash, rev.getStateHash());
    originalHash = rev.getStateHash();
    rev.setElevationHeight(4);
    Assert.assertNotEquals("StateHash after different elevation height", originalHash, rev.getStateHash());
    originalHash = rev.getStateHash();
    rev.setFreeway(true);
    Assert.assertNotEquals("StateHash after changing to freeway", originalHash, rev.getStateHash());
    originalHash = rev.getStateHash();
    rev.setBarrierLeft(new RoadSideBarrier());
    rev.getBarrierLeft().setBarrierType(RoadSideBarrierType.SCREEN);
    Assert.assertNotEquals("StateHash after setting a barrier", originalHash, rev.getStateHash());
    originalHash = rev.getStateHash();
    rev.getBarrierLeft().setDistance(25);
    Assert.assertNotEquals("StateHash after setting a barrier distance", originalHash, rev.getStateHash());
    rev.getBarrierLeft().setHeight(3);
    Assert.assertNotEquals("StateHash after setting a barrier height", originalHash, rev.getStateHash());
  }

}
