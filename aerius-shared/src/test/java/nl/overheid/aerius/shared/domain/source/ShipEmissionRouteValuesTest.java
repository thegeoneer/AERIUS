/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;

/**
 * Test class for {@link MaritimeRouteEmissionSource}.
 */
public class ShipEmissionRouteValuesTest {

  @Test
  public void testStateHash() {
    final MaritimeRouteEmissionSource ship = createNew(ShippingMovementType.INLAND);
    final MaritimeRouteEmissionSource copy = ship.copy();
    Assert.assertEquals("StateHash should be equal", ship.getStateHash(), copy.getStateHash());
  }


  private MaritimeRouteEmissionSource createNew(final ShippingMovementType type) {
    final MaritimeRouteEmissionSource sev = new MaritimeRouteEmissionSource();
    sev.getEmissionSubSources().add(createNewGroup(1));
    sev.getEmissionSubSources().add(createNewGroup(2));
    sev.getEmissionSubSources().add(createNewGroup(3));
    return sev;
  }

  private MaritimeShippingCategory createCategory(final int id) {
    final MaritimeShippingCategory c = new MaritimeShippingCategory();
    c.setName("ship name" + id);
    c.setDescription("ship description" + id);
    return c;
  }

  private RouteMaritimeVesselGroup createNewGroup(final int id) {
    final RouteMaritimeVesselGroup v = new RouteMaritimeVesselGroup();

    v.setId(id);
    v.setCategory(createCategory(id));
    v.setName("name" + id);
    v.setShipMovements(id * 13 + 5, TimeUnit.YEAR);
    return v;
  }

}
