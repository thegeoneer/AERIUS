/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

/**
 * Test class for {@link FarmLodgingCategory}.
 */
public class FarmLodgingCategoriesTest {

  private static final double EPSILON = 1E-5;

  private static int lastId;

  @Test
  public void testDetermineByCode() {
    final FarmLodgingCategories cats = createFixture();
    assertEquals("B2.1.100.1", 2.1, cats.determineFarmLodgingCategoryByCode("B2.1.100.1").getEmissionFactor(), EPSILON);
    assertEquals("E1.1", 1.1, cats.determineAdditionalLodgingSystemByCode("E1.1").getEmissionFactor(), EPSILON);
    assertEquals("E1.1.0", 0.1, cats.determineReductiveLodgingSystemByCode("E1.1.0").getReductionFactor(), EPSILON);
    assertEquals("PAS 2015.03-01", 0.4, cats.determineLodgingFodderMeasureByCode("PAS 2015.03-01").getReductionFactorCellar(), EPSILON);
    assertNull("Category by code should not be null", cats.determineFarmLodgingCategoryByCode("abc"));
  }

  @Test
  public void testConstrainedReductionFactor() {
    final FarmLodgingCategory traditional = createFarmLodgingCategory(null, 5.0, null, null);
    final FarmLodgingCategory c1 = createFarmLodgingCategory(null, 3.0, null, traditional);
    final FarmLodgingCategory c2 = createFarmLodgingCategory(null, 1.5, null, traditional);
    final FarmLodgingCategory c3 = createFarmLodgingCategory(null, 1.0, null, traditional);

    assertNull("TraditionalFarmLodgingCategory should be null", traditional.getTraditionalFarmLodgingCategory());
    assertNotNull("C1 TraditionalFarmLodgingCategory should not be null", c1.getTraditionalFarmLodgingCategory());

    assertEquals("Reductionfactor traditional", 0.0, traditional.getReductionFactor(), EPSILON);
    assertEquals("Reductionfactor c1", 0.4, c1.getReductionFactor(), EPSILON);
    assertEquals("Reductionfactor c2", 0.7, c2.getReductionFactor(), EPSILON);
    assertEquals("Reductionfactor c3", 0.8, c3.getReductionFactor(), EPSILON);

    assertFalse("Traditional should not constrain reduction factor", traditional.shouldConstrainReductionFactor());
    assertFalse("C1 should not constrain reduction factor", c1.shouldConstrainReductionFactor());
    assertFalse("C2 should not constrain reduction factor", c2.shouldConstrainReductionFactor());
    assertTrue("C3 should constrain reduction factor", c3.shouldConstrainReductionFactor());

    assertEquals("ConstrainedEmissionFactor traditional", 5.0, traditional.getConstrainedEmissionFactor(), EPSILON);
    assertEquals("ConstrainedEmissionFactor c1", 3.0, c1.getConstrainedEmissionFactor(), EPSILON);
    assertEquals("ConstrainedEmissionFactor c2", 1.5, c2.getConstrainedEmissionFactor(), EPSILON);
    assertEquals("ConstrainedEmissionFactor c3", 1.5, c3.getConstrainedEmissionFactor(), EPSILON);
  }

  @Test
  public void testCanStackCategory() {
    final FarmAdditionalLodgingSystemCategory a1 = createFarmAdditionalLodgingSystemCategory(null, 5.0);
    final FarmAdditionalLodgingSystemCategory a2 = createFarmAdditionalLodgingSystemCategory(null, 6.0);
    final FarmReductiveLodgingSystemCategory r1 = createFarmReductiveLodgingSystemCategory(null, 35.0);
    final FarmReductiveLodgingSystemCategory r2 = createFarmReductiveLodgingSystemCategory(null, 25.0);
    final FarmLodgingCategory c1 = createFarmLodgingCategory(null, 5.0, null, null);
    c1.getFarmAdditionalLodgingSystemCategories().add(a1);
    c1.getFarmReductiveLodgingSystemCategories().add(r1);

    assertTrue("Valid additional sytem", c1.canStackAdditionalLodgingSystemCategory(a1));
    assertFalse("Not a valid additional sytem", c1.canStackAdditionalLodgingSystemCategory(a2));
    assertFalse("Null not a valid additional sytem", c1.canStackAdditionalLodgingSystemCategory(null));
    assertTrue("Valid reductive sytem", c1.canStackReductiveLodgingSystemCategory(r1));
    assertFalse("Not a valid reductive sytem", c1.canStackReductiveLodgingSystemCategory(r2));
    assertFalse("Null not a valid reductive sytem", c1.canStackAdditionalLodgingSystemCategory(null));
  }

  @Test
  public void testFodderCategories() {
    final FarmLodgingCategories cats = createFixture();
    final FarmLodgingCategory cat = cats.determineFarmLodgingCategoryByCode("A1.2");
    final FarmLodgingFodderMeasureCategory fod = cats.determineLodgingFodderMeasureByCode("PAS 2015.03-01");
    assertFalse("Measure can't be applied to A1", fod.canApplyToFarmLodgingCategory(cat));
    fod.addAmmoniaProportion(cat.getAnimalCategory(), 0.3, 0.7);
    assertTrue("Measure can now be applied to A1", fod.canApplyToFarmLodgingCategory(cat));
  }

  private static FarmLodgingCategories createFixture() {
    final FarmLodgingCategories cats = new FarmLodgingCategories();
    cats.setFarmLodgingSystemCategories(createFarmLodgingCategories());
    cats.setFarmAdditionalLodgingSystemCategories(createFarmAdditionalLodgingSystemCategories());
    cats.setFarmReductiveLodgingSystemCategories(createFarmReductiveLodgingSystemCategories());
    cats.setFarmLodgingFodderMeasureCategories(createFarmLodgingFodderMeasureCategories());
    return cats;
  }

  private static ArrayList<FarmLodgingCategory> createFarmLodgingCategories() {
    final ArrayList<FarmLodgingCategory> list = new ArrayList<>();
    list.add(createFarmLodgingCategory("A1.2", 3.0, "A1", null));
    list.add(createFarmLodgingCategory("B2.1.100.1", 2.1, "B2", null));
    list.add(createFarmLodgingCategory("D2.3.4", 0.7, "D2", null));
    return list;
  }

  private static ArrayList<FarmAdditionalLodgingSystemCategory> createFarmAdditionalLodgingSystemCategories() {
    final ArrayList<FarmAdditionalLodgingSystemCategory> list = new ArrayList<>();
    list.add(createFarmAdditionalLodgingSystemCategory("E1.1", 1.1));
    list.add(createFarmAdditionalLodgingSystemCategory("E2.1.0", 2.2));
    return list;
  }

  private static ArrayList<FarmReductiveLodgingSystemCategory> createFarmReductiveLodgingSystemCategories() {
    final ArrayList<FarmReductiveLodgingSystemCategory> list = new ArrayList<>();
    list.add(createFarmReductiveLodgingSystemCategory("B2.1", 0.25));
    list.add(createFarmReductiveLodgingSystemCategory("E1.1.0", 0.10));
    return list;
  }

  private static ArrayList<FarmLodgingFodderMeasureCategory> createFarmLodgingFodderMeasureCategories() {
    final ArrayList<FarmLodgingFodderMeasureCategory> list = new ArrayList<>();
    list.add(createFarmLodgingFodderMeasureCategory("PAS 2015.03-01", 0.16, 0.40, 0.35));
    list.add(createFarmLodgingFodderMeasureCategory("PAS 2015.04-01", 0.10, 0.10, 0.1));
    return list;
  }

  private static FarmLodgingCategory createFarmLodgingCategory(final String code, final double ef, final String animalCategory,
      final FarmLodgingCategory traditional) {
    return new FarmLodgingCategory(lastId++, code, code, "", ef, new FarmAnimalCategory(lastId++, animalCategory, animalCategory, ""),
        traditional, false);
  }

  private static FarmAdditionalLodgingSystemCategory createFarmAdditionalLodgingSystemCategory(final String code, final double ef) {
    return new FarmAdditionalLodgingSystemCategory(lastId++, code, code, "", ef, false);
  }

  private static FarmReductiveLodgingSystemCategory createFarmReductiveLodgingSystemCategory(final String code, final double ef) {
    return new FarmReductiveLodgingSystemCategory(lastId++, code, code, "", ef, false);
  }

  private static FarmLodgingFodderMeasureCategory createFarmLodgingFodderMeasureCategory(final String code, final double efFloor, final double efCellar, final double efTotal) {
    final FarmLodgingFodderMeasureCategory cat = new FarmLodgingFodderMeasureCategory(lastId++, code, code, "", efFloor, efCellar, efTotal);
    cat.addAmmoniaProportion(new FarmAnimalCategory(lastId++, "D1.1", "D1.1", ""), 0.1, 0.9);
    cat.addAmmoniaProportion(new FarmAnimalCategory(lastId++, "D3", "D3", ""), 0.3, 0.7);
    return cat;
  }
}
