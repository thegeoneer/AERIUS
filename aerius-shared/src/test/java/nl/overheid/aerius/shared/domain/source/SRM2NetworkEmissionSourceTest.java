/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 *
 */
public class SRM2NetworkEmissionSourceTest extends SRM2TestBase {

  @Test
  public void testCopy() {
    final SRM2NetworkEmissionSource original = getExampleNetwork();
    final EmissionSource copy = original.copy();
    assertTrue("Copy should be right object", copy instanceof SRM2NetworkEmissionSource);
    assertEquals("Emission should be same", original.getEmission(KEY_2013_NH3), copy.getEmission(KEY_2013_NH3), 1E-3);
    ((SRM2EmissionSource) original.getEmissionSources().get(0)).getEmissionSubSources().get(0).setVehicles(3333, TimeUnit.DAY);
    assertNotEquals("Emission shouldn't be same (different object)", original.getEmission(KEY_2013_NH3), copy.getEmission(KEY_2013_NH3), 1E-3);
  }

  @Test
  public void testIsEmissionPerUnit() {
    final SRM2NetworkEmissionSource example = getExampleNetwork();
    assertTrue("The network should have emission per unit", example.isEmissionPerUnit());
  }

  @Test
  public void testIsYearDependent() {
    final SRM2NetworkEmissionSource example = getExampleNetwork();
    assertTrue("The network should be year dependent", example.isYearDependent());
  }

  private SRM2NetworkEmissionSource getExampleNetwork() {
    final SRM2NetworkEmissionSource networkEmissionValues = new SRM2NetworkEmissionSource();
    networkEmissionValues.getEmissionSources().add(getExampleRoad(1000, 100, 100));
    networkEmissionValues.getEmissionSources().add(getExampleRoad(2000, 0, 0));
    return networkEmissionValues;
  }

  private SRM2EmissionSource getExampleRoad(final double nrLightTraffic, final double nrBusses, final double nrFreight) {
    final SRM2EmissionSource source = new SRM2EmissionSource();
    final VehicleStandardEmissions lt = new VehicleStandardEmissions();
    final VehicleStandardEmissions ab = new VehicleStandardEmissions();
    final VehicleStandardEmissions hf = new VehicleStandardEmissions();

    source.getEmissionSubSources().add(lt);
    source.getEmissionSubSources().add(ab);
    source.getEmissionSubSources().add(hf);
    lt.setEmissionCategory(LIGHT_TRAFFIC);
    lt.setVehicles(nrLightTraffic, TimeUnit.DAY);
    ab.setEmissionCategory(AUTO_BUS);
    ab.setVehicles(nrBusses, TimeUnit.DAY);
    hf.setEmissionCategory(HEAVY_FREIGHT);
    hf.setVehicles(nrFreight, TimeUnit.DAY);
    return source;
  }

}
