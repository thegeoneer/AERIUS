/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.developmentspace;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test class for {@link DevelopmentRule}.
 */
public class DevelopmentRuleTest {

  /**
   * Test if order still correct.
   */
  @Test
  public void testOrder() {
    assertTrue("EXCEEDING_SPACE_CHECK > NOT_EXCEEDING_SPACE_CHECK", DevelopmentRule.EXCEEDING_SPACE_CHECK.compareTo(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK) > 0);
    assertTrue("NOT_EXCEEDING_SPACE_CHECK < EXCEEDING_SPACE_CHECK", DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK.compareTo(DevelopmentRule.EXCEEDING_SPACE_CHECK) < 0);
  }
}
