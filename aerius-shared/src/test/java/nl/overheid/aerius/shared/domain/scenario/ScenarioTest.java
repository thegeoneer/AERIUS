/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;

/**
 * Test class for {@link Scenario}.
 */
public class ScenarioTest {

  @Test
  public void testHasSources() {
    final Scenario s = new Scenario();
    assertFalse("Should report no sources when on list", s.hasSources());
    s.addSources(new EmissionSourceList());
    assertFalse("Should report no sources with 1 list that is empty.", s.hasSources());
    s.getSourceLists().get(0).add(new GenericEmissionSource());
    assertTrue("Should report to have sources with 1 list that does have a source", s.hasSources());
    s.addSources(new EmissionSourceList());
    assertFalse("Should report no sources with 2 list, with 1 list that is empty.", s.hasSources());
    s.getSourceLists().get(1).add(new GenericEmissionSource());
    assertTrue("Should report to have sources with 2 list that both have a source", s.hasSources());
  }

  @Test
  public void testHasAnyResults() {
    final Scenario s = new Scenario();
    assertFalse("Should report no results when on list", s.hasAnyResultPoints());
    s.addResultPoints(new ArrayList<AeriusResultPoint>());
    assertFalse("Should report no results with 1 list that is empty.", s.hasAnyResultPoints());
    s.getResultPoints(0).add(new AeriusResultPoint());
    assertTrue("Should report to have results with 1 list that does have a source", s.hasAnyResultPoints());
    s.addResultPoints(new ArrayList<AeriusResultPoint>());
    assertTrue("Should report results with 2 list, with 1 list that is empty.", s.hasAnyResultPoints());
    s.getResultPoints(1).add(new AeriusResultPoint());
    assertTrue("Should report to have results with 2 list that both have a source", s.hasAnyResultPoints());
  }

}
