/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FormatUtilTest {

  @Test
  public void testAlphabeticalFormatting() {
    assertTrue("Format 0 should be empty String", FormatUtil.formatAlphabetical(0).isEmpty());

    assertEquals("15 = o", "o", FormatUtil.formatAlphabetical(15));
    assertEquals("8 = h", "h", FormatUtil.formatAlphabetical(8));
    assertEquals("1 = a", "a", FormatUtil.formatAlphabetical(1));
    assertEquals("9 = i", "i", FormatUtil.formatAlphabetical(9));
    assertEquals("26 = z", "z", FormatUtil.formatAlphabetical(26));
    assertEquals("27 = ba", "ba", FormatUtil.formatAlphabetical(27));
    assertEquals("-27 = ba", "ba", FormatUtil.formatAlphabetical(-27));

    final int ohai = Integer.parseInt("e708", 26) + 1;
    assertEquals("e708 = ohai", "ohai", FormatUtil.formatAlphabetical(ohai));
  }
}
