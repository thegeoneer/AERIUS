/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;

/**
 * Test class for {@link GenericEmissionSource} object.
 */
public class GenericEmissionSourceTest {

  private static final int TEST_YEAR = 2013;

  @Test
  public void testGenericEmissionSourceNewGenericEmissionSource() {
    final GenericEmissionSource es = new  GenericEmissionSource();
    Assert.assertEquals("Default undefined sector", Sector.SECTOR_UNDEFINED, es.getSector());
  }

  @Test
  public void testGenericEmissionSourceCopy() {

    final GenericEmissionSource generic = new  GenericEmissionSource(1, new Point(2, 3));
    final FarmEmissionSource farm = new FarmEmissionSource();

    Assert.assertEquals(generic , generic.copy());
    Assert.assertEquals(farm, farm.copy());
  }

  @Test
  public void testCopyGeneric() {
    final GenericEmissionSource generic = new  GenericEmissionSource(1, new Point(2, 3));
    final GenericEmissionSource genericCopy = generic.copy();

    Assert.assertEquals(generic , genericCopy);
  }


  @Test
  public void testGenericEmissionSourceConvertAll() {
    final ArrayList<EmissionSource> list = new ArrayList<EmissionSource>();
    final ArrayList<EmissionSource> copyList = new ArrayList<EmissionSource>();

    list.add(new GenericEmissionSource(1, new Point(2, 3)));
    list.add(new FarmEmissionSource());
    list.add(new SRM2EmissionSource());
    list.add(new SRM2NetworkEmissionSource());
    list.add(new OffRoadMobileEmissionSource());
    list.add(new InlandMooringEmissionSource());
    list.add(new InlandRouteEmissionSource());
    list.add(new MaritimeMooringEmissionSource());
    list.add(new MaritimeRouteEmissionSource());

    for (final EmissionSource row : list) {
      for (final EmissionSource row2 : list) {
        copyList.add(row.copyTo(row2));
      }
    }

    for (int x = 0; x < 8; x += 9) {
      Assert.assertTrue(copyList.get(x + 0) instanceof GenericEmissionSource);
      Assert.assertTrue(copyList.get(x + 1) instanceof FarmEmissionSource);
      Assert.assertTrue(copyList.get(x + 2) instanceof SRM2EmissionSource);
      Assert.assertTrue(copyList.get(x + 3) instanceof SRM2NetworkEmissionSource);
      Assert.assertTrue(copyList.get(x + 4) instanceof OffRoadMobileEmissionSource);
      Assert.assertTrue(copyList.get(x + 5) instanceof InlandMooringEmissionSource);
      Assert.assertTrue(copyList.get(x + 6) instanceof InlandRouteEmissionSource);
      Assert.assertTrue(copyList.get(x + 7) instanceof MaritimeMooringEmissionSource);
      Assert.assertTrue(copyList.get(x + 8) instanceof MaritimeRouteEmissionSource);
    }

  }

  @Test
  public void testGenericEmissionSourceConvert() {

    final GenericEmissionSource generic = new  GenericEmissionSource(1, new Point(2, 3));
    final FarmEmissionSource farm = generic.copyTo(new FarmEmissionSource());
    final GenericEmissionSource backToGeneric = farm.copyTo(new GenericEmissionSource());

    final SRM2EmissionSource srm2 = generic.copyTo(new SRM2EmissionSource());
    final SRM2NetworkEmissionSource srm2Network = generic.copyTo(new SRM2NetworkEmissionSource());
    final PlanEmissionSource plan = generic.copyTo(new PlanEmissionSource());
    final OffRoadMobileEmissionSource offRoadMobile = generic.copyTo(new OffRoadMobileEmissionSource());
    final InlandMooringEmissionSource inlandMooring = generic.copyTo(new InlandMooringEmissionSource());
    final InlandRouteEmissionSource inlandRoute = generic.copyTo(new InlandRouteEmissionSource());
    final MaritimeMooringEmissionSource maritimeMooring = generic.copyTo(new MaritimeMooringEmissionSource());
    final MaritimeRouteEmissionSource maritimeRoute = generic.copyTo(new MaritimeRouteEmissionSource());

    Assert.assertNotSame("Do not match", generic, farm);
    Assert.assertNotSame("Do not match", generic, backToGeneric);

    Assert.assertTrue(generic instanceof GenericEmissionSource);
    Assert.assertTrue(backToGeneric instanceof GenericEmissionSource);
    Assert.assertTrue(farm instanceof FarmEmissionSource);
    Assert.assertTrue(srm2 instanceof SRM2EmissionSource);
    Assert.assertTrue(srm2Network instanceof SRM2NetworkEmissionSource);
    Assert.assertTrue(plan instanceof PlanEmissionSource);
    Assert.assertTrue(offRoadMobile instanceof OffRoadMobileEmissionSource);
    Assert.assertTrue(inlandMooring instanceof InlandMooringEmissionSource);
    Assert.assertTrue(inlandRoute instanceof InlandRouteEmissionSource);
    Assert.assertTrue(maritimeMooring instanceof MaritimeMooringEmissionSource);
    Assert.assertTrue(maritimeRoute instanceof MaritimeRouteEmissionSource);

    Assert.assertEquals("same id", generic.getId(), farm.getId());
    Assert.assertEquals("same Point x", generic.getX(), farm.getX(), 0.0001);
    Assert.assertEquals("same Point y", generic.getY(), farm.getY(), 0.0001);

    Assert.assertEquals("same id", generic.getId(), backToGeneric.getId());
    Assert.assertEquals("same Point x", generic.getX(), backToGeneric.getX(), 0.0001);
    Assert.assertEquals("same Point y", generic.getY(), backToGeneric.getY(), 0.0001);
  }

  @Test
  public void testSetAndGetEmissionYearIndependent() {
    final GenericEmissionSource gevNotPerYear = new GenericEmissionSource();

    final double firstValue = 10.0;
    final EmissionValueKey evk = new EmissionValueKey(TEST_YEAR, Substance.NH3);
    gevNotPerYear.setEmission(evk, firstValue);

    assertEquals("getEmission(not per year) with year 0", firstValue,
        gevNotPerYear.getEmission(new EmissionValueKey(Substance.NH3)), 0.0001);
    assertEquals("getEmission(not per year) with year " + TEST_YEAR, firstValue,
        gevNotPerYear.getEmission(new EmissionValueKey(TEST_YEAR, Substance.NH3)), 0.0001);
    assertEquals("getEmission(not per year) with year 2010", firstValue,
        gevNotPerYear.getEmission(new EmissionValueKey(2010, Substance.NH3)), 0.0001);
    assertEquals("getEmission(not per year) other substance", 0,
        gevNotPerYear.getEmission(new EmissionValueKey(Substance.NOX)), 0.0001);
  }
  @Test
  public void testSetAndGetEmissionYearDependent() {
    final GenericEmissionSource gevPerYear = new GenericEmissionSource();
    gevPerYear.setYearDependent(true);

    final double firstValue = 10.0;
    final EmissionValueKey evk = new EmissionValueKey(TEST_YEAR, Substance.NH3);
    gevPerYear.setEmission(evk, firstValue);

    assertEquals("getEmission(per year) with year 0", 0, gevPerYear.getEmission(new EmissionValueKey(Substance.NH3)), 0.0001);
    assertEquals("getEmission(per year) with year " + TEST_YEAR, firstValue,
        gevPerYear.getEmission(new EmissionValueKey(TEST_YEAR, Substance.NH3)), 0.0001);
    assertEquals("getEmission(per year) with year 2010", 0,
        gevPerYear.getEmission(new EmissionValueKey(2010, Substance.NH3)), 0.0001);
    assertEquals("getEmission(per year) other substance", 0,
        gevPerYear.getEmission(new EmissionValueKey(Substance.NOX)), 0.0001);
  }

  @Test
  public void testCopy() {
    final EmissionValueKey evk2012NH3 = new EmissionValueKey(2012, Substance.NH3);
    final EmissionValueKey evk2012NOx = new EmissionValueKey(2012, Substance.NOX);
    final EmissionValueKey evk2013NH3 = new EmissionValueKey(2013, Substance.NH3);
    final EmissionValueKey evk2013NOx = new EmissionValueKey(2013, Substance.NOX);
    final GenericEmissionSource gev = new GenericEmissionSource();
    gev.setYearDependent(true);
    gev.setEmission(evk2012NH3, 4.4);
    gev.setEmission(evk2012NOx, 3.2);
    gev.setEmission(evk2013NH3, 5.12);
    gev.setEmission(evk2013NOx, 33.2);
    gev.setYearDependent(true);
    gev.setEmissionPerUnit(true);
    final GenericEmissionSource copy = gev.copy();
    assertTrue("Copy year dependent", copy.isYearDependent());
    assertTrue("Copy year emissions per unit", copy.isEmissionPerUnit());
    assertEquals("Copy savem value for key 'evk2012NH3'", gev.getEmission(evk2012NH3), copy.getEmission(evk2012NH3), 0.0001);
  }

  @Test
  public void testStateHash() {
    final EmissionValueKey evk2012NH3 = new EmissionValueKey(2012, Substance.NH3);
    final EmissionValueKey evk2012NOx = new EmissionValueKey(2012, Substance.NOX);
    final EmissionValueKey evk2013NH3 = new EmissionValueKey(2013, Substance.NH3);
    final EmissionValueKey evk2013NOx = new EmissionValueKey(2013, Substance.NOX);
    final GenericEmissionSource gev1 = new GenericEmissionSource();
    gev1.setYearDependent(true);
    gev1.setEmission(evk2012NH3, 4.4);
    gev1.setEmission(evk2012NOx, 3.2);
    gev1.setEmission(evk2013NH3, 5.12);
    gev1.setEmission(evk2013NOx, 33.2);
    final GenericEmissionSource gev2 = new GenericEmissionSource();
    gev2.setYearDependent(true);
    gev2.setEmission(evk2013NOx, 33.2);
    gev2.setEmission(evk2013NH3, 5.12);
    gev2.setEmission(evk2012NOx, 3.2);
    gev2.setEmission(evk2012NH3, 4.4);
    assertEquals("State hash different order keys:", gev1.getStateHash(), gev2.getStateHash());
  }
}
