/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.request;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class for {@link PotentialRequestDetails}.
 */
public class PotentialRequestDetailsTest {

  private static final double TEST_MELDING_THRESHOLD_VALUE = 0.05;
  private static final double TEST_DEFAULT_PERMIT_THRESHOLD_VALUE = 1;

  @Test
  public void testGetPotentialRequestType() {
    final PotentialRequestDetails prd = new PotentialRequestDetails(TEST_MELDING_THRESHOLD_VALUE, TEST_DEFAULT_PERMIT_THRESHOLD_VALUE);
    assertEquals("Empty should be none", PotentialRequestType.NONE, prd.getPotentialRequestType());
    prd.addAssessmentAreaDetails(1, 0, TEST_DEFAULT_PERMIT_THRESHOLD_VALUE);
    assertEquals("Should be none", PotentialRequestType.NONE, prd.getPotentialRequestType());
    prd.addAssessmentAreaDetails(1, 0.3, TEST_DEFAULT_PERMIT_THRESHOLD_VALUE);
    assertEquals("Should be Melding", PotentialRequestType.MELDING, prd.getPotentialRequestType());
    prd.addAssessmentAreaDetails(1, 1.3, TEST_DEFAULT_PERMIT_THRESHOLD_VALUE);
    assertEquals("Should be Permit", PotentialRequestType.PERMIT, prd.getPotentialRequestType());
    prd.addAssessmentAreaDetails(1, 0.3, TEST_DEFAULT_PERMIT_THRESHOLD_VALUE);
    assertEquals("Should be still be permit", PotentialRequestType.PERMIT, prd.getPotentialRequestType());
  }

}
