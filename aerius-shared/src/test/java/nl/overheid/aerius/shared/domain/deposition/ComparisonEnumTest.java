/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.deposition;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class for {@link ComparisonEnum}.
 */
public class ComparisonEnumTest {

  @Test
  public void testGetRange() {
    assertEquals("Less 5, -100", ComparisonEnum.LESS_5, ComparisonEnum.getRange(-100));
    assertEquals("Less 4, -16", ComparisonEnum.LESS_4, ComparisonEnum.getRange(-16));
    assertEquals("Less 3, -9", ComparisonEnum.LESS_3, ComparisonEnum.getRange(-9));
    assertEquals("Less 2, -4", ComparisonEnum.LESS_2, ComparisonEnum.getRange(-4));
    assertEquals("Less 1, -2", ComparisonEnum.LESS_1, ComparisonEnum.getRange(-2));
    assertEquals("POSITIVE_NEGLIGIBLE, -0.5", ComparisonEnum.NEGATIVE_NEGLIGIBLE, ComparisonEnum.getRange(-0.5));
    assertEquals("Same, -0.005", ComparisonEnum.SAME, ComparisonEnum.getRange(-0.005));
    assertEquals("Same, 0", ComparisonEnum.SAME, ComparisonEnum.getRange(0));
    assertEquals("Same, 0.005", ComparisonEnum.SAME, ComparisonEnum.getRange(0.005));
    assertEquals("POSITIVE_NEGLIGIBLE, 0.5", ComparisonEnum.POSITIVE_NEGLIGIBLE, ComparisonEnum.getRange(0.5));
    assertEquals("More 1, 2", ComparisonEnum.MORE_1, ComparisonEnum.getRange(2));
    assertEquals("More 2, 4", ComparisonEnum.MORE_2, ComparisonEnum.getRange(4));
    assertEquals("More 3, 9", ComparisonEnum.MORE_3, ComparisonEnum.getRange(9));
    assertEquals("More 4, 16", ComparisonEnum.MORE_4, ComparisonEnum.getRange(16));
    assertEquals("More 5, 100", ComparisonEnum.MORE_5, ComparisonEnum.getRange(100));
  }

  @Test
  public void testRange() {
    assertEquals("Same, -0.055", ComparisonEnum.NEGATIVE_NEGLIGIBLE, ComparisonEnum.getRange(-0.055));
    assertEquals("Same, -0.05", ComparisonEnum.SAME, ComparisonEnum.getRange(-0.05));
    assertEquals("Same, -0.049", ComparisonEnum.SAME, ComparisonEnum.getRange(-0.049));
    assertEquals("Same, 0.049", ComparisonEnum.SAME, ComparisonEnum.getRange(0.049));
    assertEquals("Same, 0.05", ComparisonEnum.SAME, ComparisonEnum.getRange(0.05));
    assertEquals("Same, 0.055", ComparisonEnum.POSITIVE_NEGLIGIBLE, ComparisonEnum.getRange(0.055));

  }
}
