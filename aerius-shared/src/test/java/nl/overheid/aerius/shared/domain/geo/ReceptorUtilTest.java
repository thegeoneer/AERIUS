/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSGProxy;

/**
 * Test class for {@link ReceptorUtil}.
 */
public class ReceptorUtilTest {
  private static final double[][] RPS = {
      { 1, 3604, 296800 },
      { 800, 152314.656485, 296800 },
      { 1529, 287996.844942, 296800 },
      { 1530, 3697.06048591021, 296853.72849659115 },
      { 3058, 288089.905428, 296853.72849659115 },
      { 3059, 3604, 296907.45699318236 },
      { 3060, 3790.12097182, 296907.45699318236 },
      { 3061, 3976.24194364, 296907.45699318236 },
      { 291550, 196983.689721416, 307008.41435234 },
      { 293046, 190934.758137253, 307062.142848915 },
      { 4706906, 123279.78488053002, 462176.3125076431 },
      { 9184376, 227135.287156324, 619493.35052661 } };

  private static final HexagonZoomLevel ZOOM_LEVEL_1 = new HexagonZoomLevel(1, 10000);
  private static final BBox RECEPTOR_BBOX = new BBox(3604, 296800, 287959, 629300);

  private static final ArrayList<HexagonZoomLevel> hexagonZoomLevels = createZoomLevels();
  private static final ReceptorGridSettings RGS = new ReceptorGridSettings(RECEPTOR_BBOX, EPSGProxy.defaultEpsg(), 1529, hexagonZoomLevels);
  private static final ReceptorUtil RECEPTOR_UTIL = new ReceptorUtil(RGS);

  @Test
  public void testRPFromAndToRandom() {
    final Random random = new Random();
    random.setSeed(1337);

    for (int i = 0; i < 1000; i++) {
      final AeriusPoint r = RECEPTOR_UTIL.setAeriusPointFromId(new AeriusPoint(random.nextInt(9184376)));
      final AeriusPoint r2 = RECEPTOR_UTIL.setReceptorIdFromPoint(new AeriusPoint(r.getX(), r.getY()));
      final AeriusPoint r3 = RECEPTOR_UTIL.setReceptorIdFromPoint(new AeriusPoint(r.getX(), r.getY()));
      Assert.assertEquals("Random check for values (exact)", r.getId(), r2.getId());
      Assert.assertEquals("Random check for values", r.getId(), r3.getId());
    }

    for (int i = 0; i < 1000; i++) {
      final double xCoord = 3604 + random.nextDouble() * (227135 - 3604 + 1);
      final double yCoord = 296800 + random.nextDouble() * (619493 - 296800 + 1);
      final AeriusPoint r1 = RECEPTOR_UTIL.setReceptorIdFromPoint(new AeriusPoint(xCoord, yCoord));
      final AeriusPoint r2 = RECEPTOR_UTIL.setReceptorIdFromPoint(new AeriusPoint(xCoord, yCoord));

      final AeriusPoint r3 = RECEPTOR_UTIL.setAeriusPointFromId(new AeriusPoint(r1.getId()));
      final AeriusPoint r4 = RECEPTOR_UTIL.setAeriusPointFromId(new AeriusPoint(r2.getId()));

      Assert.assertEquals("Random check for values X coord", r3.getX(), r3.getX(), ZOOM_LEVEL_1.getHexagonRadius());
      Assert.assertEquals("Random check for values Y coord", r3.getY(), r3.getY(), ZOOM_LEVEL_1.getHexagonHeight() / 2);

      Assert.assertEquals("Random check for values X coord (exact)", r4.getX(), r4.getX(), ZOOM_LEVEL_1.getHexagonRadius());
      Assert.assertEquals("Random check for values Y coord (exact)", r4.getY(), r4.getY(), ZOOM_LEVEL_1.getHexagonHeight() / 2);
    }
  }

  private static ArrayList<HexagonZoomLevel> createZoomLevels() {
    final ArrayList<HexagonZoomLevel> zoomLevels = new ArrayList<>();
    zoomLevels.add(ZOOM_LEVEL_1);
    zoomLevels.add(new HexagonZoomLevel(2, 10000));
    return zoomLevels;
  }

  @Test
  public void setAeriusPointFromIdTest() {
    for (final double[] element : RPS) {
      final AeriusPoint rp = RECEPTOR_UTIL.setAeriusPointFromId(new AeriusPoint((int) element[0]));

      Assert.assertEquals("Receptor(" + element[0] + ") X:", element[1], rp.getX(), 0.000001);
      Assert.assertEquals("Receptor(" + element[0] + ") Y:", element[2], rp.getY(), 0.000001);
    }
  }

  @Test
  public void setReceptorIdFromPointTest() {
    for (int i = 0; i < RPS.length; i++) {
      final AeriusPoint rp = RECEPTOR_UTIL.setReceptorIdFromPoint(new AeriusPoint(RPS[i][1], RPS[i][2]));

      Assert.assertEquals("Receptor(" + i + ":" + RPS[i][1] + ", " + RPS[i][2] + ") ID:", (int) RPS[i][0], rp.getId());
    }
  }

  @Test
  public void setReceptorIdFromPointRandomTest() {
    final Random random = new Random();
    random.setSeed(1337);

    for (int i = 0; i < RPS.length; i++) {
      final int sign = random.nextInt() % 2 == 0 ? 1 : -1;
      final AeriusPoint rp = RECEPTOR_UTIL.setReceptorIdFromPoint(new AeriusPoint(
          RPS[i][1] + sign * random.nextDouble() * ZOOM_LEVEL_1.getHexagonRadius() / 2,
          RPS[i][2] + sign * random.nextDouble() * ZOOM_LEVEL_1.getHexagonHeight() / 2));

      Assert.assertEquals("Receptor(" + i + ":" + rp.getX() + ", " + rp.getY() + ") ID:", (int) RPS[i][0], rp.getId());
    }
  }

  @Test
  public void setReceptorIdFromPointMaximums() {
    final Random random = new Random();
    random.setSeed(1337);
    final double divergeFromMaximum = 0.000001;

    // left-right middle corners
    for (int i = 0; i < RPS.length; i++) {
      final int sign = random.nextInt() % 2 == 0 ? 1 : -1;
      final AeriusPoint rp = RECEPTOR_UTIL.setReceptorIdFromPoint(new AeriusPoint(
          RPS[i][1] + sign * (ZOOM_LEVEL_1.getHexagonRadius() - divergeFromMaximum),
          RPS[i][2]));

      Assert.assertEquals("Receptor(" + i + ":" + rp.getX() + ", " + rp.getY() + ") ID:", (int) RPS[i][0], rp.getId());
    }
    // left-right top corners
    for (int i = 0; i < RPS.length; i++) {
      final int sign = random.nextInt() % 2 == 0 ? 1 : -1;
      final AeriusPoint rp = RECEPTOR_UTIL.setReceptorIdFromPoint(new AeriusPoint(
          RPS[i][1] + sign * (ZOOM_LEVEL_1.getHexagonRadius() / 2 - divergeFromMaximum),
          RPS[i][2] + (ZOOM_LEVEL_1.getHexagonHeight() / 2 - divergeFromMaximum)));

      Assert.assertEquals("Receptor(" + i + ":" + rp.getX() + ", " + rp.getY() + ") ID:", (int) RPS[i][0], rp.getId());
    }
    // left-right bottom corners
    for (int i = 0; i < RPS.length; i++) {
      final int sign = random.nextInt() % 2 == 0 ? 1 : -1;
      final AeriusPoint rp = RECEPTOR_UTIL.setReceptorIdFromPoint(new AeriusPoint(
          RPS[i][1] + sign * (ZOOM_LEVEL_1.getHexagonRadius() / 2 - divergeFromMaximum),
          RPS[i][2] - (ZOOM_LEVEL_1.getHexagonHeight() / 2 - divergeFromMaximum)));

      Assert.assertEquals("Receptor(" + i + ":" + rp.getX() + ", " + rp.getY() + ") ID:", (int) RPS[i][0], rp.getId());
    }
  }

  @Test
  public void testIsReceptorAtZoomLevel() {
    final AeriusPoint inPoint = new AeriusPoint(135378, 462284);
    final AeriusPoint outPoint = new AeriusPoint(134354,462552);
    for (int i = 2; i < 5; i++) {
      final HexagonZoomLevel zoomLevel = new HexagonZoomLevel(i, ZOOM_LEVEL_1.getSurfaceLevel1());
      assertTrue("Point should be return as a receptor at zoomLevel " + i, RECEPTOR_UTIL.isReceptorAtZoomLevel(inPoint, zoomLevel));
      assertFalse("Point should be return not as a receptor at zoomLevel " + i, RECEPTOR_UTIL.isReceptorAtZoomLevel(outPoint, zoomLevel));
    }
  }

  @Test
  public void testAttachReceptor() {
    final Random random = new Random();
    random.setSeed(1337);

    for (int i = 0; i < 1000; i++) {
      final double xCoord = 3604 + random.nextDouble() * (227135 - 3604 + 1);
      final double yCoord = 296800 + random.nextDouble() * (619493 - 296800 + 1);
      final AeriusPoint p1 = new AeriusPoint(xCoord, yCoord);
      final AeriusPoint p2 = new AeriusPoint(xCoord, yCoord);
      RECEPTOR_UTIL.attachReceptorToGrid(p2);

      Assert.assertEquals(0, p1.distance(p2), ZOOM_LEVEL_1.getHexagonRadius());
    }
  }

  @Test
  public void testGetZoomLevelForReceptor() {
    final AeriusPoint zoomLevel1Point = RECEPTOR_UTIL.setAeriusPointFromId(new AeriusPoint(3842801));
    assertEquals("Receptor on zoom level 1", 1, RECEPTOR_UTIL.getZoomLevelForReceptor(zoomLevel1Point));
    final AeriusPoint zoomLevel5Point = RECEPTOR_UTIL.setAeriusPointFromId(new AeriusPoint(1334301));
    assertEquals("Receptor on zoom level 2", 2, RECEPTOR_UTIL.getZoomLevelForReceptor(zoomLevel5Point));
  }
}
