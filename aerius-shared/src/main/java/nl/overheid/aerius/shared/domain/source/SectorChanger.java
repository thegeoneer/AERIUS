/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import nl.overheid.aerius.shared.domain.ops.HasOPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.EmissionCalculationMethod;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;

/**
 * Util class to change the sector of an emission source.
 */
public final class SectorChanger {

  private SectorChanger() {
    // util
  }

  /**
   * Changed the EmissionSource to the new sector. Only generic data is preserved. Sector specific data is omitted.
   * @param current current emission source to copy data from.
   * @param newSector the new sector
   * @return new object for the new sector
   */
  public static EmissionSource changeSector(final EmissionSource current, final Sector newSector) {
    final EmissionSource es = switchSource(current, newSector == null ? EmissionCalculationMethod.GENERIC : newSector.getProperties().getMethod());
    es.setSector(newSector);
    if (es instanceof HasOPSSourceCharacteristics && newSector != null) {
      es.setSourceCharacteristics(newSector.getDefaultCharacteristics().copy());
    }
    return es;
  }

  private static EmissionSource switchSource(final EmissionSource current, final EmissionCalculationMethod method) {
    final EmissionSource newEmissionSource;
    switch (method) {
    case FARM_LODGE:
      newEmissionSource = new FarmEmissionSource();
      break;
    case OFFROAD_MOBILE:
      newEmissionSource = new OffRoadMobileEmissionSource();
      break;
    case PLAN:
      newEmissionSource = new PlanEmissionSource();
      break;
    case ROAD:
      newEmissionSource = new SRM2EmissionSource();
      break;
    case SHIPPING_MARITIME_DOCKED:
      newEmissionSource = new MaritimeMooringEmissionSource();
      break;
    case SHIPPING_MARITIME_INLAND:
      newEmissionSource = new MaritimeRouteEmissionSource(ShippingMovementType.INLAND);
      break;
    case SHIPPING_MARITIME_MARITIME:
      newEmissionSource = new MaritimeRouteEmissionSource(ShippingMovementType.MARITIME);
      break;
    case SHIPPING_INLAND_DOCKED:
      newEmissionSource = new InlandMooringEmissionSource();
      break;
    case SHIPPING_INLAND:
      newEmissionSource = new InlandRouteEmissionSource();
      break;
    case GENERIC:
    default:
      newEmissionSource = new GenericEmissionSource();
      break;
    }
    return current.copyTo(newEmissionSource);
  }
}
