/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario;

import java.io.Serializable;
import java.util.ArrayList;

import javax.validation.Valid;

import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Class to store Scenario specific data.
 */
public class Scenario implements Serializable {
  private static final long serialVersionUID = 1174742963914361878L;

  @Valid private ScenarioMetaData metaData = new ScenarioMetaData();
  @Valid private ArrayList<EmissionSourceList> sources = new ArrayList<EmissionSourceList>();
  @Valid private CalculationPointList calculationPoints = new CalculationPointList();
  private ArrayList<ArrayList<AeriusResultPoint>> resultPoints = new ArrayList<ArrayList<AeriusResultPoint>>();

  /**
   * Add the {@link EmissionSourceList} and if the id of the list is < 0 set the id of the list to the value the id of the last list + 1 or If no
   * entries yet the value of the id is 0.
   *
   * @param esl {@link EmissionSourceList} to add
   */
  public void addSources(final EmissionSourceList esl) {
    if (esl.getId() < 0) {
      esl.setId(sources.isEmpty() ? 0 : sources.get(sources.size() - 1).getId() + 1);
    }
    sources.add(esl);
  }

  public ScenarioMetaData getMetaData() {
    return metaData;
  }

  /**
   * Get the {@link EmissionSourceList} by id of the list.
   *
   * @param elsId id of the list
   * @return {@link EmissionSourceList} or null if not found
   */
  public EmissionSourceList getSources(final int elsId) {
    for (final EmissionSourceList esl : sources) {
      if (esl != null && elsId == esl.getId()) {
        return esl;
      }
    }
    return null;
  }

  public ArrayList<EmissionSourceList> getSourceLists() {
    return sources;
  }

  /**
   * Check to see if ALL source list have at least one source.
   *
   * @return True if in each of the source list there is at least one source.
   */
  public boolean hasSources() {
    boolean found = !getSourceLists().isEmpty();
    for (final EmissionSourceList sourceList : getSourceLists()) {
      found &= !sourceList.isEmpty();
    }
    return found;
  }

  /**
   * Remove the {@link EmissionSourceList} for the given id. If the id is not present nothing changes.
   *
   * @param elsId id of the {@link EmissionSourceList}to remove
   */
  public void removeSources(final int elsId) {
    for (final EmissionSourceList esl : sources) {
      if (elsId == esl.getId()) {
        sources.remove(esl);
        break;
      }
    }
  }

  public CalculationPointList getCalculationPoints() {
    return calculationPoints;
  }

  public void setCalculationPoints(final CalculationPointList calculationPoints) {
    this.calculationPoints = calculationPoints;
  }

  public void clearResultPoints() {
    resultPoints.clear();
  }

  public void addResultPoints(final ArrayList<AeriusResultPoint> results) {
    resultPoints.add(results);
  }

  /**
   * Returns the results related for the given index. To match sources with results make sure they are added on the same index.
   *
   * @param i index to get results
   * @return list of results.
   */
  public ArrayList<AeriusResultPoint> getResultPoints(final int i) {
    return i >= resultPoints.size() || i < 0 ? new ArrayList<AeriusResultPoint>() : resultPoints.get(i);
  }

  /**
   * Returns true if any of the results points has any results.
   *
   * @return true if any list has any results
   */
  public boolean hasAnyResultPoints() {
    boolean found = false;
    for (final ArrayList<AeriusResultPoint> rp : resultPoints) {
      found |= !rp.isEmpty();
    }
    return found;
  }

  public void setMetaData(final ScenarioMetaData metaData) {
    this.metaData = metaData;
  }

  @Override
  public String toString() {
    return "Scenario [sources=" + sources + ", customCalculationPoints=" + calculationPoints + "]";
  }
}
