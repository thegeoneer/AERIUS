/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer;

import java.io.Serializable;
import java.util.ArrayList;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Wrapper class to act as a result object for a file import.
 */
public class ImportResult extends Scenario implements Serializable {

  private static final long serialVersionUID = 7559226999884712416L;

  private int importedYear;
  private Integer importedTemporaryPeriod;
  private PermitCalculationRadiusType permitCalculationRadiusType;
  private String version;
  private String databaseVersion;
  private ImportType type;
  private int srid;
  private ArrayList<AeriusException> exceptions = new ArrayList<AeriusException>();
  private ArrayList<AeriusException> warnings = new ArrayList<AeriusException>();
  private Integer importedImaerFileId;
  private ArrayList<UserGeoLayer> userGeoLayers = new ArrayList<>();


  public ArrayList<AeriusException> getExceptions() {
    return exceptions;
  }

  public ArrayList<AeriusException> getWarnings() {
    return warnings;
  }

  @Min(value = 1900, message = "year must be greater than 1900")
  @Max(value = 2100, message = "year must be less than 2100")
  public int getImportedYear() {
    return importedYear;
  }

  public void setImportedYear(final int importedYear) {
    this.importedYear = importedYear;
  }

  public Integer getImportedTemporaryPeriod() {
    return importedTemporaryPeriod;
  }

  public void setImportedTemporaryPeriod(final Integer importedTemporaryPeriod) {
    this.importedTemporaryPeriod = importedTemporaryPeriod;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public String getDatabaseVersion() {
    return databaseVersion;
  }

  public void setDatabaseVersion(final String databaseVersion) {
    this.databaseVersion = databaseVersion;
  }

  public ImportType getType() {
    return type;
  }

  public void setType(final ImportType type) {
    this.type = type;
  }

  public int getSrid() {
    return srid;
  }

  public void setSrid(final int srid) {
    this.srid = srid;
  }

  public PermitCalculationRadiusType getPermitCalculationRadiusType() {
    return permitCalculationRadiusType;
  }

  public void setPermitCalculationRadiusType(final PermitCalculationRadiusType permitCalculationRadiusType) {
    this.permitCalculationRadiusType = permitCalculationRadiusType;
  }

  public ArrayList<UserGeoLayer> getUserGeoLayers() {
    return userGeoLayers;
  }

  public Integer getImportedImaerFileId() {
    return importedImaerFileId;
  }

  public void setImportedImaerFileId(final Integer importedImaerFileId) {
    this.importedImaerFileId = importedImaerFileId;
  }
}
