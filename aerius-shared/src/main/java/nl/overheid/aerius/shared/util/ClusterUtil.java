/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.ClusteredReceptorPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;

/**
 * Static cluster util providing the possibility to cluster a list of points.
 *
 * Uses {@link ClusteredReceptorPoint} to retain the distance of the item in the cluster that is farthest away.
 *
 * All points in any given cluster are within the radius of the returned {@link ClusteredReceptorPoint}'s maxDistance field.
 */
public final class ClusterUtil {
  /**
   * Private constructor
   */
  private ClusterUtil() {
  }

  /**
   * Returns a list of clustered points, but the clustered point is not a receptor point.
   * @param points points to cluster
   * @param threshold distance to cluster
   * @return
   */
  public static <E extends Point> HashSet<ClusteredReceptorPoint<E>> clusterGenericPoints(final Collection<E> points, final double threshold) {
    return clusterPoints(points, threshold, null);
  }

  /**
   * Returns a list of clustered points, if receptorUtil not is null the points are clustered around a receptor point.
   * @param points points to cluster
   * @param threshold distance to cluster
   * @param receptorUtil util if not null used to set the cluster point as a receptor point.
   * @return
   */
  public static <E extends Point> HashSet<ClusteredReceptorPoint<E>> clusterPoints(final Collection<E> points, final double threshold,
      final ReceptorUtil receptorUtil) {
    final HashSet<ClusteredReceptorPoint<E>> clusters = new HashSet<ClusteredReceptorPoint<E>>();

    // Create a pool containing the location of all sources (do not clone)
    final ArrayList<E> pool = new ArrayList<E>();
    for (final E src : points) {
      pool.add(src);
    }

    // Loop through the pool until all locations have been clustered
    while (!pool.isEmpty()) {
      // Create a cluster and remove the result from the pool
      final ArrayList<E> blob = findSourcesInProximity(pool.get(0), pool, threshold);
      pool.removeAll(blob);

      // Get the middle
      final ClusteredReceptorPoint<E> cluster = new ClusteredReceptorPoint<E>(blob, receptorUtil);

      // Add it to the clustered items
      clusters.add(cluster);
    }

    return clusters;
  }

  private static <E extends Point> ArrayList<E> findSourcesInProximity(final E seed, final List<E> pool, final double distance) {
    final ArrayList<E> proximityItems = new ArrayList<E>();

    for (final E poolItem : pool) {
      if (isInProximityOf(poolItem, seed, distance)) {
        proximityItems.add(poolItem);
      }
    }

    return proximityItems;
  }

  private static boolean isInProximityOf(final Point poolItem, final Point item, final double distance) {
    final double difX = Math.abs(poolItem.getX() - item.getX());
    final double difY = Math.abs(poolItem.getY() - item.getY());

    return difX * difX + difY * difY <= distance * distance;
  }
}
