/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.test;

/**
 * TestID class for product Melding.
 */
public final class TestIDMelding extends TestID {
  public static final String ORGANISATION_INFO = "organisation-information";
  public static final String ORG_NAME = "organisation-name";
  public static final String ORG_ADDR = "organisation-addr";
  public static final String ORG_CITY = "organisation-city";
  public static final String ORG_POST = "organisation-post";
  public static final String ORG_EMAIL = "organisation-email";
  public static final String ORG_CONTACT = "organisation-contact";

  public static final String NEXT = "next";
  public static final String PREVIOUS = "previous";

  public static final String BENEFICIARY = "beneficiary";
  public static final String BENEFICIARY_CORRESPONDENCE = "correspondence";
  public static final String BENEFACTOR = "benefactor";
  public static final String EXECUTOR = "executor";

  public static final String SAME = "same";
  public static final String CUSTOM = "different";

  public static final String SEND_CONFIRMATION = "send-confirmation";

  public static final String NB_PERMIT_NO = "permit-no";
  public static final String NB_PERMIT_YES = "permit-yes";
  public static final String NB_SOURCE_PERMIT_NO = "permit-source-no";
  public static final String NB_SOURCE_PERMIT_YES = "permit-source-yes";
  public static final String PREV_MELDING_NO = "prev-melding-no";
  public static final String PREV_MELDING_YES = "prev-melding-yes";
  public static final String PROVINCE_REF = "province-reference";
  public static final String PREV_MELDING_REF = "prev-melding-reference";
  public static final String REFERENCE_DESCRIPTION = "reference-description";
  public static final String TRUTH = "truth-check";

  public static final String BUTTON_PREVIOUS_PAGE = "melding-previous-page";
  public static final String BUTTON_NEXT_PAGE = "melding-next-page";
}
