/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.request;

/**
 * An NB-wet calculation might require a request (into Register) in case it's a melding or
 * permit, or it could be that no request is needed. This enum indicates which is the case.
 *
 * Keep the order intact, it is used to determine the final type over all types in a list of areas.
 */
public enum PotentialRequestType {
  /**
   * Highest < 0.05.
   */
  NONE,

  /**
   * Highest < 1 && Highest >= 0.05.
   */
  MELDING,

  /**
   * Highest < 1 && Highest >= 0.05
   * But there's no more room so it's a permit anyway.
   */
  PERMIT_INSIDE_MELDING_SPACE,

  /**
   * Highest >= 1.
   */
  PERMIT;
}
