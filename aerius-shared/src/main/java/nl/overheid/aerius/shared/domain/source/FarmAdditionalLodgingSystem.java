/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;

/**
 * Data class for a single Additional Lodging System.
 */
public class FarmAdditionalLodgingSystem extends CategorizedEmissionSubSource<FarmAdditionalLodgingSystemCategory> implements Serializable {

  private static final long serialVersionUID = -8757595799685493267L;

  private FarmLodgingSystemDefinition systemDefinition;

  public FarmLodgingSystemDefinition getSystemDefinition() {
    return systemDefinition;
  }

  public void setSystemDefinition(final FarmLodgingSystemDefinition systemDefinition) {
    this.systemDefinition = systemDefinition;
  }

  @Override
  public FarmAdditionalLodgingSystem copy() {
    return copyTo(new FarmAdditionalLodgingSystem());
  }

  protected <U extends FarmAdditionalLodgingSystem> U copyTo(final U copy) {
    super.copyTo(copy);
    copy.setSystemDefinition(systemDefinition);

    return copy;
  }

  @Override
  public double getEmission(final EmissionValueKey key) {
    return getCategory() != null && key != null && Substance.NH3 == key.getSubstance()
        ? getAmount() * getCategory().getEmissionFactor() : 0;
  }

  @Override
  public String toString() {
    return "FarmAdditionalLodgingSystem [FarmAdditionalLodgingSystemCategory=" + getCategory() + ", amount=" + getAmount() + "]";
  }
}
