/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasState;

/**
 * Emission values with a category and amount value.
 * @param <E> category type
 */
public abstract class CategorizedEmissionSubSource<E extends HasId & HasState> extends EmissionSubSource implements Serializable {

  private static final long serialVersionUID = 5740559995609521937L;

  private int amount;
  private E category;

  protected <U extends CategorizedEmissionSubSource<E>> U copyTo(final U copy) {
    super.copyTo(copy);
    copy.setAmount(amount);
    copy.setCategory(category);

    return copy;
  }

  @Min(0)
  public int getAmount() {
    return amount;
  }

  public void setAmount(final int amount) {
    this.amount = amount;
  }

  @NotNull
  public E getCategory() {
    return category;
  }

  public void setCategory(final E category) {
    this.category = category;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((category == null) ? 0 : category.getStateHash());
    result = prime * result + amount;
    return result;
  }
}
