/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

/**
 * Contains any limits for input for OPS (Source and Emission files).
 */
public final class OPSLimits {

  /**
   * receptor ID: fortran notation: I8 -> Over max value of int.
   */
  public static final int RECEPTOR_ID_MINIMUM = Integer.MIN_VALUE;

  /**
   * receptor ID: fortran notation: I8 -> Over max value of int.
   */
  public static final int RECEPTOR_ID_MAXIMUM = Integer.MAX_VALUE;

  /**
   * source ID (ssn): fortran notation: I4 -> -999 min.
   * NOTE: not used, source ID for OPS is defined by the worker.
   */
  public static final int SOURCE_ID_MINIMUM = -999;

  /**
   * source ID (ssn): fortran notation: I4 -> 9999 max.
   * NOTE: not used, source ID for OPS is defined by the worker.
   */
  public static final int SOURCE_ID_MAXIMUM = 9999;

  /**
   * source emission (q): fortran notation: E10.3 -> no min/max (will be converted to something like 0.9876E543).
   * negative emission makes no sense.
   */
  public static final int SOURCE_EMISSION_MINIMUM = 0;

  /**
   *  source heat content (hc): fortran notation: F7.3 -> -99.999 min.
   *  RIVM: negative heat content would make no sense
   */
  public static final int SOURCE_HEAT_CONTENT_MINIMUM = -999;

  /**
   *  source heat content (hc): fortran notation: F7.3 -> 999.999 max.
   */
  public static final int SOURCE_HEAT_CONTENT_MAXIMUM = 999;

  /**
   * The number of digits after the decimal point for heat content.
   */
  public static final int SOURCE_HEAT_CONTENT_DIGITS_PRECISION = 3;

  /**
   * source height(h): fortran notation: F6.1 -> -999.9 min.
   * RIVM: negative height would make no sense
   */
  public static final double SOURCE_EMISSION_HEIGHT_MINIMUM = 0;

  /**
   * source height(h): fortran notation: F6.1 -> 9999.9 max.
   */
  public static final double SOURCE_EMISSION_HEIGHT_MAXIMUM = 4999.0;

  /**
   * source height(h): fortran notation: F6.1 -> 9999.9 max.
   */
  public static final double SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM = 20;

  /**
   * The number of digits after the decimal point for height.
   */
  public static final int SOURCE_HEIGHT_DIGITS_PRECISION = 1;

  /**
   * The number of digits after the decimal point for temperature.
   */
  public static final int SOURCE_TEMPERATURE_DIGITS_PRECISION = 2;

  /**
   * source diameter(d): fortran notation: I7 -> -999999 min.
   * negative diameter is used for circular sources.
   * 0 is used for point-sources.
   * positive diameter is used for square sources.
   */
  public static final int SOURCE_DIAMETER_MINIMUM = -999999;

  /**
   * source diameter(d): fortran notation: I7 -> 999999 max.
   * negative diameter is used for circular sources.
   * 0 is used for point-sources.
   * positive diameter is used for square sources.
   */
  public static final int SOURCE_DIAMETER_MAXIMUM = 999999;

  /**
   * source spread or source height distribution(s): fortran notation: F6.1 -> -999.9 min.
   */
  public static final int SOURCE_SPREAD_MINIMUM = 0;

  /**
   * source spread or source height distribution(s): fortran notation: F6.1 -> 9999.9 max.
   * TODO: maximum is actually the height of the source. How to validate that?
   */
  public static final int SOURCE_SPREAD_MAXIMUM = 9999;

  /**
   * The number of digits after the decimal point for spread.
   */
  public static final int SOURCE_SPREAD_DIGITS_PRECISION = 1;

  /**
   * The number of digits after the decimal point for heat content.
   */
  public static final int SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION = 1;

  /**
   * inner diameter stack (m): fortran notation: XXX -> -999.9 min.
   * negative is used to signal it isn't specified, heat content should be set in such a case.
   * if positive heat content should not be set.
   */
  public static final double SOURCE_OUTFLOW_DIAMETER_MINIMUM = -999;

  /**
   * inner diameter stack (m) used in the user interface
   */
  public static final double SOURCE_UI_OUTFLOW_DIAMETER_MINIMUM = 0.1;

  /**
   * inner diameter stack (m): fortran notation: XXX -> 999.9 max.
   * negative is used to signal it isn't specified, heat content should be set in such a case.
   * if positive heat content should not be set.
   * is used for a valiation
   */
  public static final double SOURCE_OUTFLOW_DIAMETER_MAXIMUM = 30;

  /**
   * inner diameter stack (m) maximum when building influence is applicable
   * is used for a warning.
   */
  public static final double SOURCE_IS_BUILDING_OUTFLOW_DIAMETER_MAXIMUM = 5;

  /**
   * The number of digits after the decimal point for heat content.
   */
  public static final int SOURCE_OUTFLOW_VELOCITY_DIGITS_PRECISION = 1;

  /**
   * exit velocity (m/s): fortran notation: XXX -> -999.9 min.
   * negative is used to signal it isn't specified, heat content should be set in such a case.
   * negative also means the outflow direction is horizontal.
   * if positive heat content should not be set.
   * positive also means the outflow direction is vertical (the default).
   */
  public static final double SOURCE_OUTFLOW_VELOCITY_MINIMUM = -999;

  /**
   * exit velocity (m/s) value used for user input.
   */
  public static final double SOURCE_UI_OUTFLOW_VELOCITY_MINIMUM = 0;

  /**
   * exit velocity (m/s): fortran notation: XXX -> 999.9 max.
   * negative is used to signal it isn't specified, heat content should be set in such a case.
   * negative also means the outflow direction is horizontal.
   * if positive heat content should not be set.
   * positive also means the outflow direction is vertical (the default).
   */
  public static final double SOURCE_OUTFLOW_VELOCITY_MAXIMUM = 50;

  /**
   * When buiding influence is applied the maximum is 8.4
   */
  public static final double SOURCE_IS_BUILDING_OUTFLOW_VELOCITY_MAXIMUM = 8.4;

  /**
   * temperature effluent gas (C): fortran notation: XXX -> -999.9 min.
   * negative is used to signal it isn't specified, heat content should be set in such a case.
   * if positive heat content should not be set.
   */
  public static final double SOURCE_EMISSION_TEMPERATURE_MINIMUM = -999;

  /**
   * temperature effluent gas (C): fortran notation: XXX -> -999.9 max.
   * negative is used to signal it isn't specified, heat content should be set in such a case.
   * if positive heat content should not be set.
   */
  public static final double SOURCE_EMISSION_TEMPERATURE_MAXIMUM = 2000;

  /**
   * source diurnal variation(tb): fortran notation: I4 -> -999 min.
   * negative values are for custom files (which we don't use)
   * OPS will return an error if negative is used and no file is present.
   */
  public static final int SOURCE_DIURNAL_VARIATION_MINIMUM = 0;

  /**
   * source diurnal variation(tb): fortran notation: I4 -> 9999 max.
   * from documentation: 3 is max.
   * from RIVM: 33 is max, 31, 32 and 33 are uses for traffic.
   */
  public static final int SOURCE_DIURNAL_VARIATION_MAXIMUM = 33;

  /**
   * source category number(cat): fortran notation: I4 -> -999 min.
   * from documentation: 0 is min.
   * RIVM: 1 is min.
   * cat is only used for administrive purposes, so allow 0.
   */
  public static final int SOURCE_CAT_MINIMUM = 0;

  /**
   * source category number(cat): fortran notation: I4 -> 9999 max.
   */
  public static final int SOURCE_CAT_MAXIMUM = 9999;

  /**
   * source area (area): fortran notation: I4 -> -999 min.
   * from documentation: 1 is min.
   * RIVM: 1 is min.
   */
  public static final int SOURCE_AREA_MINIMUM = 1;

  /**
   * source area (area): fortran notation: I4 -> 9999 max.
   */
  public static final int SOURCE_AREA_MAXIMUM = 9999;

  /**
   * source particle size distribution(psd): fortran notation: I4 -> -999 min.
   * Negative values are used for custom files (which we don't use).
   * OPS won't return an error if negative is used and no file is present.
   * Limit depends on input. For PM10 this value must correspond with the
   * sector ID. However, for other substances this value is irrelevant and
   * 0 is a legal value. Therefore the technical minimum is 0.
   */
  public static final int SOURCE_PARTICLE_SIZE_DISTRIBUTION_MINIMUM = 0;

  /**
   * source particle size distribution(psd): fortran notation: I4 -> 9999 max.
   * For PM10 this value must correspond with the sector ID, which is a
   * 4-digit integer. Therefore the maximum is defined as 9999.
   */
  public static final int SOURCE_PARTICLE_SIZE_DISTRIBUTION_MAXIMUM = 9999;

  /**
   * source component name (comp) fortran notation: A12 -> max size is 12.
   * field is actually ignored by OPS and we don't use it.
   */
  public static final int SOURCE_COMP_MAXIMUM_SIZE = 12;

  /**
   * lowest technical value for year.
   */
  public static final int YEAR_MINIMUM = 0;

  /**
   * highest technical value for year.
   */
  public static final int YEAR_MAXIMUM = 9999;

  /**
   * The number of digits after the decimal point for height / width / length
   */
  public static final int SOURCE_BUILDING_DIGITS_PRECISION = 1;

  /**
   * Minimum value for scope of ops for height of building.
   */
  public static final double SCOPE_BUILDING_HEIGHT_MINIMUM = 0;

  /**
   * Maximum value for scope of ops for height of building.
   */
  public static final double SCOPE_BUILDING_HEIGHT_MAXIMUM = 20.0;

  /**
   * Minimum value for scope of ops for width of building.
   */
  public static final double SCOPE_BUILDING_WIDTH_MINIMUM = 1.5;

  /**
   * Maximum value for scope of ops for width of building.
   */
  public static final double SCOPE_BUILDING_WIDTH_MAXIMUM = 87.15;

  /**
   * Minimum value for scope validation of width length ration.
   */
  public static final double SCOPE_BUILDING_WIDTH_LENGTH_RATIO_MINIMUM = 0.15;

  /**
   * Maximum value for scope validation of width length ration.
   */
  public static final double SCOPE_BUILDING_WIDTH_LENGTH_RATIO_MAXIMUM = 1.0;

  /**
   * Minimum value for scope of ops for length of building.
   */
  public static final double SCOPE_BUILDING_LENGTH_MINIMUM = 10.0;

  /**
   * Maximum value for scope of ops for length of building.
   */
  public static final double SCOPE_BUILDING_LENGTH_MAXIMUM = 105.0;

  /**
   * Minimum value for scope of ops for orientation of building.
   */
  public static final double SCOPE_BUILDING_ORIENTATION_MINIMUM = 0;

  /**
   * Maximum value for scope of ops for orientation of building.
   */
  public static final double SCOPE_BUILDING_ORIENTATION_MAXIMUM = 179;

  /**
   * Minimum value for scope of ops for outflow diameter.
   */
  public static final double SCOPE_OUTFLOW_DIAMETER_MINIMUM = 0.01;

  /**
   * Maximum value for scope of ops for outflow diameter.
   */
  public static final double SCOPE_OUTFLOW_DIAMETER_MAXIMUM = 5.0;

  /**
   * Minimum value for scope of ops for outflow height.
   */
  public static final double SCOPE_OUTFLOW_HEIGHT_MINIMUM = 0;

  /**
   * Maximum value for scope of ops for outflow height.
   */
  public static final double SCOPE_OUTFLOW_HEIGHT_MAXIMUM = 20;

  /**
   * Minimum value for scope of ops for outflow velocity.
   */
  public static final double SCOPE_OUTFLOW_VELOCITY_MINIMUM = 0;

  /**
   * Maximum value for scope of ops for outflow velocity.
   */
  public static final double SCOPE_OUTFLOW_VELOCITY_MAXIMUM = 8.4;

  private OPSLimits() {
  }
}
