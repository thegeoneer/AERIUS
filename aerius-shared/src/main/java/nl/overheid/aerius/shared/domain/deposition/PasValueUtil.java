/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.deposition;

import nl.overheid.aerius.shared.MathUtil;

/**
 * Util class specific for PAS related rounding.
 */
public final class PasValueUtil {

  private static final double THRESHOLD = 0.005;
  private static final double MELDING_THRESHOLD = 0.05;
  private static final double MELDING_THRESHOLD_ROUND = 0.055;
  private static final double PERMIT_THRESHOLD = 1.0;
  private static final double PERMIT_THRESHOLD_ROUND = 1.005;
  private static final String LESS_THAN_EQUAL_TO = "<=";

  private PasValueUtil() {
    // util class.
  }

  /**
   * In the PAS if a value is > 0.05 or > 1.00 but it is rounded to 0.05, 1.00 it would give the wrong information, because while the values 0.05
   * and 1.0 could mean no 'melding' or 'vergunning'.
   * @param value value to check
   * @return '>' if meets condition, else empty string.
   */
  public static String pasProofValue(final double value) {
    return pasProofValue(value, false);
  }

  /**
   * In the PAS if a value is > 0.05 or > 1.00 but it is rounded to 0.05, 1.00 it would give the wrong information, because while the values 0.05
   * and 1.0 could mean no 'melding' or 'vergunning'.
   * show <= value when values is smaller than MELDING_THRESHOLD_ROUND
   * @param value value to check
   * @param boolean test on MELDING_THRESHOLD_ROUND
   * @return '>' if meets condition, and ≤ if we meetisSuppressLowValue && MELDING_THRESHOLD_ROUND, else empty string.
   */
  public static String pasProofValue(final double value, final boolean isSuppressLowValue) {
    // no formatting for low values
    /*
    final String sign;
    if (value > 0 && value < THRESHOLD) {
      sign = ">"; 
    } else if (isSuppressLowValue && value <= MELDING_THRESHOLD) {
      sign = ""; // remove LESS_THAN_EQUAL_TO for rounding condition
    } else {
      sign = "";
    }
    */
    return "";

  }

  /**
   * Test if value is below the MELDING_THRESHOLD if so return MELDING_THRESHOLD otherwise return value
   * @param value value to check
   * @return double value
   */
  public static double pasProofValueOverwrite(final double value) {
    return value <= MELDING_THRESHOLD ? MELDING_THRESHOLD : value;

  }

  /***
   * Returns 0.0 if the rounded value with supplied decimal length would result in a zero value 
   * (e.g. 0.0/0.00 etc) (negative or not). In other words, when formatted to a fixed value if 
   * the value is zero, use the zero value, otherwise keep the value.
   * <pre>Examples (with a length of 2 decimal):
   * <strong>0.05</strong> stays the same.
   * <strong>0.005</strong> becomes <strong>0.00</strong>
   * <strong>-0.05</strong> stays the same.
   * <strong>-0.005</strong> becomes <strong>0.00</strong> (instead of <strong>-0.00</strong>)
   * </pre>
   * @param value value to check
   * @param length length of decimals to round 
   * @return proofed fixed value.
   */
  public static double pasProoftoFixed(final double value, final int length) {
    return Math.abs(MathUtil.round(value * Math.pow(10, length))) > 0 ? value : 0.0;
  }
}
