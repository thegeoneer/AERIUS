/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.search;

import nl.overheid.aerius.shared.domain.geo.AreaType;


/**
 * All possible search suggestions.
 */
public enum MapSearchSuggestionType implements SearchSuggestionType {
  /**
   * Nature area.
   */
  NATURE_AREA,
  /**
   * Province aera.
   */
  PROVINCE_AREA,
  /**
   * Municipality area.
   */
  MUNICIPALITY_AREA,
  /**
   * Town area.
   */
  TOWN_AREA,
  /**
   * Zip code area.
   */
  ZIP_CODE_AREA,
  /**
   * Address.
   */
  ADDRESS,
  /**
   * Rijksdriehoekstelsel XY coordinate.
   */
  XY_COORDINATE,
  /**
   * Receptor ID
   */
  RECEPTOR_ID;

  public boolean isPoint() {
    return (this == ADDRESS) || (this == XY_COORDINATE) || (this == RECEPTOR_ID);
  }

  public AreaType toAreaType() {
    final AreaType at;
    switch (this) {
    case MUNICIPALITY_AREA:
      at = AreaType.MUNICIPALITY_AREA;
      break;
    case PROVINCE_AREA:
      at = AreaType.PROVINCE_AREA;
      break;
    case TOWN_AREA:
      at = AreaType.TOWN_AREA;
      break;
    case ZIP_CODE_AREA:
      at = AreaType.ZIP_CODE_AREA;
      break;
    case NATURE_AREA:
      at = AreaType.NATURA2000_AREA;
      break;
    default:
      at = null;
      break;
    }
    return at;
  }

  public static MapSearchSuggestionType fromAreaType(final AreaType areaType) {
    final MapSearchSuggestionType sst;
    switch (areaType) {
    case MUNICIPALITY_AREA:
      sst = MUNICIPALITY_AREA;
      break;
    case PROVINCE_AREA:
      sst = PROVINCE_AREA;
      break;
    case TOWN_AREA:
      sst = TOWN_AREA;
      break;
    case ZIP_CODE_AREA:
      sst = ZIP_CODE_AREA;
      break;
    case NATURA2000_AREA:
      sst = NATURE_AREA;
      break;
    default:
      sst = null;
      break;
    }
    return sst;
  }

}
