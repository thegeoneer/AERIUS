/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import java.io.Serializable;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.WKTGeometry;

/**
 * {@link WKTGeometry} with bounding box.
 */
public class WKTGeometryWithBox extends WKTGeometry implements Serializable {
  private static final long serialVersionUID = -6172132587996634761L;

  private BBox boundingBox;

  public WKTGeometryWithBox() {
    // Needed for GWT
  }

  public WKTGeometryWithBox(final WKTGeometry geometry, final BBox boundingBox) {
    super(geometry.getWKT(), geometry.getMeasure());
    this.boundingBox = boundingBox;
  }

  public BBox getBoundingBox() {
    return boundingBox;
  }

  public void setBoundingBox(final BBox boundingBox) {
    this.boundingBox = boundingBox;
  }
}
