/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasState;

/**
 * List of {@link EmissionSubSource} objects.
 */
public class EmissionSubSourceList<EV extends EmissionSubSource> extends ArrayList<EV> implements HasState {

  private static final long serialVersionUID = -9050439144262898677L;

  public EmissionSubSourceList<EV> copyTo(final EmissionSubSourceList<EV> copy) {
    for (final EV item: this) {
      copy.add((EV) item.copy());
    }
    return copy;
  }

  @Override
  public int getStateHash() {
    int state = 0;
    for (final EV item: this) {
      state += item.getStateHash();
    }
    return state;
  }

  double getTotalEmission(final EmissionValueKey key, final double weight) {
    double emission = 0;
    for (final EV item : this) {
      emission += getEmission(item, key, weight);
    }
    return emission;
  }

  double getEmission(final EmissionSubSource emissionSubSource, final EmissionValueKey key, final double weight) {
    return emissionSubSource.getEmission(key) * weight;
  }
}
