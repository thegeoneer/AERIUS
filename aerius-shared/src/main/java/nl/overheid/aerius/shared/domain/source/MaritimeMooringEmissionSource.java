/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;
import java.util.ArrayList;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute.ShippingRouteReference;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Emission values for maritime mooring ships.
 */
public class MaritimeMooringEmissionSource extends MooringEmissionSource<MooringMaritimeVesselGroup> implements Serializable {

  private static final long serialVersionUID = 1762658719570272626L;

  /**
   * A maritime route is the route at sea of docked ship.
   */
  public static class MaritimeRoute extends MooringRoute {

    private static final long serialVersionUID = 8401390552245148013L;

    @Override
    public MaritimeRoute copy() {
      return copyTo(new MaritimeRoute());
    }

    @Override
    public String toString() {
      return "MaritimeRoute [" + super.toString() + "]";
    }
  }

  /**
   * Emissions for a group of vessels for maritime shipping. The emission of a group of vessels is
   * calculated as follows:
   * <p>The vessels are connected to an inland route (each vessel traversing that route twice) and 1 or more maritime route(s).
   * The emission should be the emission at the dock + the emission of the vesselgroup on the inland route
   * + the emission of the vesselgroup on the maritime route(s).
   */
  public static class MooringMaritimeVesselGroup extends VesselGroupEmissionSubSource<MaritimeShippingCategory> implements ShippingRouteReference {

    private static final long serialVersionUID = 8875261071033358054L;

    /**
     * Number of ships per time unit.
     */
    private int numberOfShipsPerTimeUnit;
    private TimeUnit timeUnit = TimeUnit.YEAR;

    /**
     * The number of hours per year ships reside at the dock.
     */
    private int residenceTime;

    /**
     * Route a ship travels.
     */
    private ShippingRoute inlandRoute;

    /**
     * Route a ship travels on sea.
     */
    private ArrayList<MaritimeRoute> maritimeRoutes = new ArrayList<>();

    @Override
    public MooringMaritimeVesselGroup copy() {
      return copyTo(new MooringMaritimeVesselGroup());
    }

    protected <V extends MooringMaritimeVesselGroup> V copyTo(final V copy) {
      super.copyTo(copy);
      copy.setNumberOfShipsPerTimeUnit(numberOfShipsPerTimeUnit);
      copy.setTimeUnit(timeUnit);
      copy.setResidenceTime(residenceTime);
      copy.setInlandRoute(inlandRoute == null ? null : inlandRoute.copy());
      final ArrayList<MaritimeRoute> copyMaritimeRoutes = new ArrayList<>();
      for (final MaritimeRoute maritimeRoute : maritimeRoutes) {
        copyMaritimeRoutes.add(maritimeRoute.copy());
      }
      copy.setMaritimeRoutes(copyMaritimeRoutes);
      return copy;
    }

    /**
     * Removes the route relation of this object. This method should be called when an object is deleted,
     * otherwise the relations are persisted.
     */
    public void detachRoutes() {
      setInlandRoute(null);
      if (getMaritimeRoutes() != null) {
        for (final MaritimeRoute mr : getMaritimeRoutes()) {
          mr.getRoute().detachRoute(mr);
        }
      }
    }

    @Min(0)
    public int getNumberOfShipsPerTimeUnit() {
      return numberOfShipsPerTimeUnit;
    }

    @NotNull
    public TimeUnit getTimeUnit() {
      return timeUnit;
    }

    public int getNumberOfShipsPerYear() {
      return timeUnit.getPerYear(numberOfShipsPerTimeUnit);
    }

    @Min(0)
    public int getResidenceTime() {
      return residenceTime;
    }

    public ShippingRoute getInlandRoute() {
      return inlandRoute;
    }

    public ArrayList<MaritimeRoute> getMaritimeRoutes() {
      return maritimeRoutes;
    }

    @Override
    public int getStateHash() {
      final int prime = 31;
      int result = 1;
      result = prime * result + super.getStateHash();
      result = prime * result + ((inlandRoute == null) ? 0 : inlandRoute.getStateHash());
      result = prime * result + numberOfShipsPerTimeUnit;
      result = prime * result + timeUnit.ordinal();
      result = prime * result + residenceTime;
      for (final MaritimeRoute mtr : maritimeRoutes) {
        result = prime * result + mtr.getStateHash();
      }
      return result;
    }

    public void setNumberOfShips(final int numberOfShipsPerTimeUnit, final TimeUnit timeUnit) {
      setNumberOfShipsPerTimeUnit(numberOfShipsPerTimeUnit);
      setTimeUnit(timeUnit);
    }

    public void setNumberOfShipsPerTimeUnit(final int numberOfShipsPerTimeUnit) {
      this.numberOfShipsPerTimeUnit = numberOfShipsPerTimeUnit;
    }

    public void setTimeUnit(final TimeUnit timeUnit) {
      this.timeUnit = timeUnit;
    }

    public void setResidenceTime(final int residenceTime) {
      this.residenceTime = residenceTime;
    }

    /**
     * @param inlandRoute The route to use as inland route. Automatically gets attached/detached.
     */
    public void setInlandRoute(final ShippingRoute inlandRoute) {
      if (this.inlandRoute != null) {
        this.inlandRoute.detachRoute(this);
      }
      this.inlandRoute = inlandRoute;
      if (inlandRoute != null) {
        inlandRoute.attachRoute(this);
      }
    }

    public void setMaritimeRoutes(final ArrayList<MaritimeRoute> maritimeRoutes) {
      this.maritimeRoutes = maritimeRoutes;
    }

    @Override
    public String toString() {
      return "VesselGroupEmissionValues [" + super.toString() + ", numberOfShipsPerTimeUnit=" + numberOfShipsPerTimeUnit
          + ", timeUnit=" + timeUnit + ", residenceTime=" + residenceTime + ", inland route=" + inlandRoute
          + ", maritime routes=" + maritimeRoutes + "]";
    }
  }

  @Override
  public <T> T accept(final EmissionSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public MaritimeMooringEmissionSource copy() {
    return copyTo(new MaritimeMooringEmissionSource());
  }

  public <E extends MaritimeMooringEmissionSource> E copyTo(final E copy) {
    super.copyTo(copy);
    for (int i = 0; i < getEmissionSubSources().size(); i++) {
      final MooringMaritimeVesselGroup vesselGroup = getEmissionSubSources().get(i);
      final MooringMaritimeVesselGroup vgCopy = copy.getEmissionSubSources().get(i);
      for (int j = 0; j < getInlandRoutes().size(); j++) {
        if (vesselGroup.getInlandRoute() != null && vesselGroup.getInlandRoute().getId() == getInlandRoutes().get(j).getId()) {
          vgCopy.setInlandRoute(copy.getInlandRoutes().get(j));
          break;
        }
      }
    }
    return copy;
  }

  /**
   * Removes the route relations. Call the method when this object is deleted to detach the routes from the object.
   */
  public void detachRoutes() {
    for (final MooringMaritimeVesselGroup vesselGroup : getEmissionSubSources()) {
      vesselGroup.detachRoutes();
    }
  }

  @Override
  public String toString() {
    return "MaritimeMooringEmissionValues [" + super.toString() + "]";
  }
}
