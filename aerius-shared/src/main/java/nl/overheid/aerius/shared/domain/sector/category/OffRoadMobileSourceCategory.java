/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.ArrayList;
import java.util.HashMap;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Off-Road Mobile Source Category class. For each mobile source per substance the
 * emission factors, that is factor per liter fuel used, is available in the
 * object.
 */
public class OffRoadMobileSourceCategory extends AbstractEmissionCategory {

  private static final long serialVersionUID = 3437955341962732809L;

  /**
   * Emission factors per substance. Where emission factor is factor per liter fuel used.
   */
  private HashMap<Substance, Double> emissionFactors = new HashMap<Substance, Double>();

  public double getEmissionFactor(final EmissionValueKey key) {
    return emissionFactors.containsKey(key.getSubstance()) ? emissionFactors.get(key.getSubstance()) : 0;
  }

  public ArrayList<Substance> getKeys() {
    return new ArrayList<Substance>(emissionFactors.keySet());
  }

  public void setEmissionFactor(final Substance substance, final double emissionFactor) {
    emissionFactors.put(substance, Double.valueOf(emissionFactor));
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && getId() == ((OffRoadMobileSourceCategory) obj).getId();
  }

  @Override
  public int hashCode() {
    return getId();
  }

  @Override
  public String toString() {
    return super.toString() + ", emissionFactors=" + emissionFactors;
  }
}
