/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasState;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;

/**
 * Data class for a single Reductive Lodging System.
 */
public class FarmReductiveLodgingSystem implements Serializable, HasState {

  private static final long serialVersionUID = -7927401975458085754L;

  private FarmReductiveLodgingSystemCategory category;

  private FarmLodgingSystemDefinition systemDefinition;

  public FarmLodgingSystemDefinition getSystemDefinition() {
    return systemDefinition;
  }

  public void setSystemDefinition(final FarmLodgingSystemDefinition systemDefinition) {
    this.systemDefinition = systemDefinition;
  }

  /**
   * @return A copy of this lodgingsystem.
   */
  public FarmReductiveLodgingSystem copy() {
    final FarmReductiveLodgingSystem copy = new FarmReductiveLodgingSystem();
    copy.setCategory(getCategory());
    return copy;
  }

  public FarmReductiveLodgingSystemCategory getCategory() {
    return category;
  }

  public void setCategory(final FarmReductiveLodgingSystemCategory category) {
    this.category = category;
  }

  /**
   * Returns the emission factor.
   * Note that the reduction factor which was fetched from the database is converted to an emission factor
   * (ef = 1 - rf).
   * So for an air scrubber 70%, the reduction factor is 0.7 but the emission factor is 0.3, since
   * we're interested in the remaining emission.
   * @param key The key to determine the factor to be used on the emission.
   * @return The emission factor to be used on the emission (fraction).
   */
  public double getEmissionFactor(final EmissionValueKey key) {
    return getCategory() != null && key != null && Substance.NH3 == key.getSubstance()
        ? 1 - getCategory().getReductionFactor() : 0;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = prime * result + category.getStateHash();
    return result;
  }

}
