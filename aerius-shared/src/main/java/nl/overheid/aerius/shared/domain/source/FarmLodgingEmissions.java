/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import javax.validation.constraints.Min;

import nl.overheid.aerius.shared.domain.EmissionValueKey;

/**
 * Abstract class for Farm Lodging emissions.
 */
public abstract class FarmLodgingEmissions extends EmissionSubSource implements VisitableEmissionSubSource {

  private static final long serialVersionUID = -7526383050750298628L;

  private int amount;

  @Min(0)
  public int getAmount() {
    return amount;
  }

  public void setAmount(final int amount) {
    this.amount = amount;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = prime * result + amount;
    return result;
  }

  @Override
  public abstract FarmLodgingEmissions copy();

  protected <U extends FarmLodgingEmissions> U copyTo(final U copy) {
    super.copyTo(copy);
    copy.setAmount(amount);

    return copy;
  }

  @Override
  public abstract double getEmission(EmissionValueKey key);

  @Override
  public String toString() {
    return "amount=" + amount;
  }

}
