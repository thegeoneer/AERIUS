/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info.pdf;

import java.util.HashSet;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.info.HabitatType;

/**
 *
 */
public class PDFExportHabitatInfo extends HabitatType implements HasExportDepositionInfo {

  private static final long serialVersionUID = -8823644947367218152L;

  private PDFExportDepositionInfo depositionInfo;

  private HashSet<DevelopmentRule> showRules = new HashSet<>();

  @Override
  public PDFExportDepositionInfo getDepositionInfo() {
    return depositionInfo;
  }

  public void setDepositionInfo(final PDFExportDepositionInfo depositionInfo) {
    this.depositionInfo = depositionInfo;
  }

  public HashSet<DevelopmentRule> getShowRules() {
    return showRules;
  }

  public void setShowRules(final HashSet<DevelopmentRule> showRules) {
    this.showRules = showRules;
  }

}
