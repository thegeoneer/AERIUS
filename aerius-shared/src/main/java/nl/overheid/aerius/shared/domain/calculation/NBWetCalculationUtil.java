/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Util to get Proposed or Current situation details.
 */
public final class NBWetCalculationUtil {

  private NBWetCalculationUtil() {
    // util class.
  }

  public static Calculation getCurrentCalculation(final CalculatedScenario calculatedScenario) {
    return calculatedScenario instanceof CalculatedComparison
        ? ((CalculatedComparison) calculatedScenario).getCalculationOne() : null;
  }

  public static int getCurrentCalculationId(final CalculatedScenario calculatedScenario) {
    return calculatedScenario instanceof CalculatedComparison
        ? ((CalculatedComparison) calculatedScenario).getCalculationIdOne() : 0;
  }

  public static EmissionSourceList getCurrentSources(final CalculatedScenario calculatedScenario) {
    return calculatedScenario instanceof CalculatedComparison
        ? ((CalculatedComparison) calculatedScenario).getSourcesOne() : null;
  }

  public static Calculation getProposedCalculation(final CalculatedScenario calculatedScenario) {
    Calculation calculation;
    if (calculatedScenario instanceof CalculatedSingle) {
      calculation = ((CalculatedSingle) calculatedScenario).getCalculation();
    } else if (calculatedScenario instanceof CalculatedComparison) {
      calculation = ((CalculatedComparison) calculatedScenario).getCalculationTwo();
    } else {
      calculation = null;
    }
    return calculation;
  }

  public static int getProposedCalculationId(final CalculatedScenario calculatedScenario) {
    final Calculation calculation = getProposedCalculation(calculatedScenario);
    return calculation == null ? 0 : calculation.getCalculationId();
  }

  public static EmissionSourceList getProposedSources(final CalculatedScenario calculatedScenario) {
    EmissionSourceList esl;
    if (calculatedScenario instanceof CalculatedSingle) {
      esl = ((CalculatedSingle) calculatedScenario).getSources();
    } else if (calculatedScenario instanceof CalculatedComparison) {
      esl = ((CalculatedComparison) calculatedScenario).getSourcesTwo();
    } else {
      esl = null;
    }
    return esl;
  }
}
