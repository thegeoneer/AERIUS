/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info.pdf;

import java.util.ArrayList;
import java.util.HashSet;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.Country;
import nl.overheid.aerius.shared.domain.info.NatureAreaDirective;

/**
 *
 */
public class PDFExportAssessmentAreaInfo extends AssessmentArea implements HasExportDepositionInfo {

  private static final long serialVersionUID = 6628151485905108341L;

  private AeriusPoint maxDeltaPoint;

  private PDFExportDepositionInfo depositionInfo;

  private HashSet<DevelopmentRule> showRules = new HashSet<>();

  private ArrayList<PDFExportHabitatInfo> habitatInfos = new ArrayList<>();

  private boolean pasArea;

  private boolean foreignAuthority;

  private NatureAreaDirective directive;

  private Country country;

  public AeriusPoint getMaxDeltaPoint() {
    return maxDeltaPoint;
  }

  public void setMaxDeltaPoint(final AeriusPoint maxDeltaPoint) {
    this.maxDeltaPoint = maxDeltaPoint;
  }

  @Override
  public PDFExportDepositionInfo getDepositionInfo() {
    return depositionInfo;
  }

  public void setDepositionInfo(final PDFExportDepositionInfo depositionInfo) {
    this.depositionInfo = depositionInfo;
  }

  public HashSet<DevelopmentRule> getShowRules() {
    return showRules;
  }

  public void setShowRules(final HashSet<DevelopmentRule> showRules) {
    this.showRules = showRules;
  }

  public ArrayList<PDFExportHabitatInfo> getHabitatInfos() {
    return habitatInfos;
  }

  public void setHabitatInfos(final ArrayList<PDFExportHabitatInfo> habitatInfos) {
    this.habitatInfos = habitatInfos;
  }

  public boolean isPasArea() {
    return pasArea;
  }

  public void setPasArea(final boolean pasArea) {
    this.pasArea = pasArea;
  }

  public boolean isForeignAuthority() {
    return foreignAuthority;
  }

  public void setForeignAuthority(final boolean foreignAuthority) {
    this.foreignAuthority = foreignAuthority;
  }

  public NatureAreaDirective getDirective() {
    return directive;
  }

  public void setDirective(final NatureAreaDirective directive) {
    this.directive = directive;
  }

  public Country getCountry() {
    return country;
  }

  public void setCountry(final Country country) {
    this.country = country;
  }

}
