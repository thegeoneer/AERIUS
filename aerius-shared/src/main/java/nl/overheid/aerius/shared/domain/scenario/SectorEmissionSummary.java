/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasEmission;
import nl.overheid.aerius.shared.domain.sector.Sector;

/**
 * Data class with emission source details of a sector.
 */
public class SectorEmissionSummary implements Serializable {

  private static final long serialVersionUID = 1502455441841042811L;

  public static final Comparator<SectorEmissionSummary> COMPARATOR = new Comparator<SectorEmissionSummary>() {
    @Override
    public int compare(final SectorEmissionSummary first, final SectorEmissionSummary compareWith) {
      final int compared = (int) (compareWith.getSummedEmission() - first.getSummedEmission());

      if (compared == 0) {
        return compareWith.getNumberOfSources() - first.getNumberOfSources();
      }
      return compared;
    }
  };

  private Sector sector;
  private int numberOfSources;
  private HashMap<EmissionValueKey, Double> emissions = new HashMap<>();

  public SectorEmissionSummary(final Sector sector) {
    this.sector = sector;
  }

  public void addEmission(final EmissionValueKey key, final double emission) {
    emissions.put(key, getEmission(key) + emission);
  }

  public void addEmissions(final HasEmission source, final List<EmissionValueKey> keys) {
    numberOfSources++;
    for (final EmissionValueKey key : keys) {
      addEmission(key, source.getEmission(key));
    }
  }

  public Sector getSector() {
    return sector;
  }

  public void setSector(final Sector sector) {
    this.sector = sector;
  }

  public int getNumberOfSources() {
    return numberOfSources;
  }

  public void setNumberOfSources(final int numberOfSources) {
    this.numberOfSources = numberOfSources;
  }

  public Set<Entry<EmissionValueKey, Double>> getEmissions() {
    return emissions.entrySet();
  }

  public double getSummedEmission() {
    double total = 0;
    for (final Entry<EmissionValueKey, Double> entry : emissions.entrySet()) {
      total += entry.getValue();
    }
    return total;
  }

  public double getEmission(final EmissionValueKey key) {
    return emissions.containsKey(key) ? emissions.get(key) : 0.0;
  }

  @Override
  public String toString() {
    return "SectorEmissionSummary [sector=" + sector + ", numberOfSources=" + numberOfSources + ", emissions=" + emissions + "]";
  }
}
