/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;

/**
 * Emissions for Specific classified vehicle types.
 */
public class VehicleSpecificEmissions extends VehicleEmissions implements Serializable {

  private static final long serialVersionUID = -8328044280667216980L;

  private RoadType roadType = RoadType.URBAN_ROAD;
  private OnRoadMobileSourceCategory category;

  @Override
  public VehicleSpecificEmissions copy() {
    return copyTo(new VehicleSpecificEmissions());
  }

  protected <U extends VehicleSpecificEmissions> U copyTo(final U copy) {
    super.copyTo(copy);
    copy.setRoadType(roadType);
    copy.setCategory(category);
    return copy;
  }

  public OnRoadMobileSourceCategory getCategory() {
    return category;
  }

  @Override
  double getEmission(final EmissionValueKey key) {
    return category == null ? 0 : category.getEmissionFactor(key.getSubstance(), roadType) * getVehiclesPerYear()
        / SharedConstants.GRAM_PER_KM_TO_KG_PER_METER;
  }

  public RoadType getRoadType() {
    return roadType;
  }

  @Override
  public boolean isYearDependent() {
    return false;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = super.getStateHash();
    result = prime * result + (category == null ? 0 : category.getStateHash());
    result = prime * result + (roadType == null ? 0 : roadType.ordinal());
    return result;
  }

  public void setCategory(final OnRoadMobileSourceCategory category) {
    this.category = category;
  }

  public void setRoadType(final RoadType roadType) {
    this.roadType = roadType;
  }
}
