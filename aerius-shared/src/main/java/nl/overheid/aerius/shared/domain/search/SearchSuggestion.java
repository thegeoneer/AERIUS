/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.search;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasName;

/**
 * POJO containing a Search suggestion to be used in the search widget.
 */
public abstract class SearchSuggestion<T extends SearchSuggestionType, E extends SearchSuggestion<T, E>> implements Serializable, HasName, HasId {

  private static final long serialVersionUID = 6485305781632653L;

  private ArrayList<E> subItems;

  private int id;
  private String name;
  private T type;

  private boolean lazySubItems;

  public SearchSuggestion(final int id, final String name, final T type) {
    this.id = id;
    this.name = name;
    this.type = type;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  public T getType() {
    return type;
  }

  public ArrayList<E> getSubItems() {
    if (subItems == null) {
      subItems = new ArrayList<>();
    }

    return subItems;
  }

  public boolean hasLazySubItems() {
    return lazySubItems;
  }

  /**
   * When true, the subitems list may be filled later. So even though it could have 0 entries right
   * now, it could be filled when the search item is expanded.
   * @param lazySubItems Whether the subitems list is lazy-loaded.
   */
  public void setLazySubItems(final boolean lazySubItems) {
    this.lazySubItems = lazySubItems;
  }

  public void setSubItems(final ArrayList<E> subItems) {
    this.subItems = subItems;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  public void setType(final T type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "SearchSuggestion [id=" + id + ", name=" + name + ", type=" + type + "]";
  }
}
