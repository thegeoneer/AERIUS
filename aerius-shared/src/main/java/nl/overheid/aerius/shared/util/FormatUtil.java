/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

/**
 * Utility class with helpers for formatting messages.
 */
public final class FormatUtil {

  private static final int ASCII_A = 97;
  private static final int LETTERS_IN_ALPHABET = 26;

  private FormatUtil() {}

  /**
   * Format the given integer to its letter representation in the
   * latin alphabet. Semantically correct version.
   *
   * -1 is 'a' (absolute value of integer)
   * 0 is an empty string
   * 1 is 'a'
   * 26 is 'z'
   * 27 is 'ba'
   *
   * @param i integer to format
   *
   * @return letter representation of the given integer.
   */
  public static String formatAlphabetical(final int i) {
    return i == 0 ? "" : formatAlphabeticalExact(Math.abs(i) - 1);
  }

  /**
   * Returns the value of {@link #formatAlphabeticalExact(int)} in upper case.
   * @param letter integer to format
   *
   * @return letter representation in upper case of the given integer.
   */
  public static String formatAlphabeticalExactUpperCase(final int letter) {
    return formatAlphabeticalExact(letter).toUpperCase();
  }

  /**
   * Format the given integer to its exact letter representation.
   *
   * -1 results in a {@link NegativeArraySizeException}
   * 0 is 'a'
   * 1 is 'b'
   * 25 is 'z'
   * 26 is 'ba'
   *
   * @param letter integer to format
   *
   * @return letter representation of the given integer.
   */
  public static String formatAlphabeticalExact(final int letter) {
    int i = letter;
    final int radix = LETTERS_IN_ALPHABET;
    final char[] buf = new char[1 + (int) (Math.log(i + 1) / Math.log(radix))];
    int charPos = buf.length;

    do {
      // 97 is the ASCII code for the lower-case 'a'
      buf[--charPos] = (char) (i % radix + ASCII_A);
    } while ((i = i / radix) > 0);

    return new String(buf, charPos, buf.length - charPos);
  }
}
