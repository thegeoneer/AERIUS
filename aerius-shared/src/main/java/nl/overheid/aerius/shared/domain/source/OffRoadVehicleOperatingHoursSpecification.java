/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import nl.overheid.aerius.shared.SharedConstants;

/**
 *
 */
public class OffRoadVehicleOperatingHoursSpecification extends OffRoadVehicleSpecification {

  private static final long serialVersionUID = -3996934629982966350L;

  private static final int TOTAL_DIVISOR = SharedConstants.PERCENTAGE_TO_FRACTION * SharedConstants.GRAM_TO_KILOGRAM;

  private double emissionFactor;
  private int load;
  private int power;
  private int usage;

  public double getEmissionFactor() {
    return emissionFactor;
  }

  public void setEmissionFactor(final double emissionFactor) {
    this.emissionFactor = emissionFactor;
  }

  public int getLoad() {
    return load;
  }

  public void setLoad(final int load) {
    this.load = load;
  }

  public int getPower() {
    return power;
  }

  public void setPower(final int power) {
    this.power = power;
  }

  public int getUsage() {
    return usage;
  }

  public void setUsage(final int usage) {
    this.usage = usage;
  }

  /**
   * Formula:
   *    Emission NOx = (power * load * usage * emissionFactor)
   *
   *    The calculation starts with the emission factor as that is a double and forces the calculation to be done with
   *    doubles. If we use another order, it will calculate in ints as long as it can and run over the limits resulting
   *    in negatives...
   *
   * @return the emission in kilograms NOx per year
   */
  @Override
  public double getEmission() {
    return emissionFactor * power * load * usage / TOTAL_DIVISOR;
  }

  @Override
  public OffRoadVehicleOperatingHoursSpecification copy() {
    return copyTo(new OffRoadVehicleOperatingHoursSpecification());
  }

  public <U extends OffRoadVehicleOperatingHoursSpecification> U copyTo(final U copy) {
    super.copyTo(copy);

    copy.setEmissionFactor(emissionFactor);
    copy.setLoad(load);
    copy.setPower(power);
    copy.setUsage(usage);

    return copy;
  }
}
