/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportStatus;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service providing export functionality.
 */
@RemoteServiceRelativePath(ServiceURLConstants.EXPORT_SERVICE_PATH)
public interface ExportService extends RemoteService {
  /**
   * Export the calculation to the user specified in the e-mail address.
   *
   * @param exportProperties the calculation key
   * @param scenario the email address
   * @return A key to identify the export with. Can be used to poll whether the export is finished.
   * @throws AeriusException on error
   */
  String exportCalculation(ExportProperties exportProperties, CalculatedScenario scenario) throws AeriusException;

  /**
   * Get the status of the export result.
   * @param key Unique key to identify export with.
   * @return status object.
   * @throws AeriusException on error
   * @throws
   */
  ExportStatus getExportResult(String key) throws AeriusException;
}
