/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import java.io.Serializable;

/**
 *
 */
public class ColorRange implements Range, Serializable {
  private static final long serialVersionUID = 3518530062444655264L;

  private String color;
  private double lowerValue;

  /**
   * Default constructor.
   */
  public ColorRange() {
    // GWT required constructor.
  }

  /**
   * @param lowerValue The lower value for this range.
   * @param color The color to use for this range.
   */
  public ColorRange(final double lowerValue, final String color) {
    this.lowerValue = lowerValue;
    this.color = color;
  }

  @Override
  public boolean equals(final Object o) {
    boolean equal = false;
    if (o instanceof ColorRange) {
      final ColorRange other = (ColorRange) o;
      equal = lowerValue == other.lowerValue
          && color != null && color.equals(other.color);
    }
    return equal;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (int) lowerValue;
    result = prime * result + ((color == null) ? 0 : color.hashCode());
    return result;
  }

  @Override
  public String getColor() {
    return color;
  }

  @Override
  public String toString() {
    return "ColorRange[lowerValue=" + lowerValue + ", color=" + color + "]";
  }

  @Override
  public double getLowerValue() {
    return lowerValue;
  }
}
