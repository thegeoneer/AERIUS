/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasState;
import nl.overheid.aerius.shared.domain.sector.category.ShippingCategory;
import nl.overheid.aerius.shared.domain.source.ShippingRoute.ShippingRouteReference;

/**
 * Emission values for multiple Ships that are mooring and have a route.
 * The emission returned from {@link #getEmission(nl.overheid.aerius.shared.domain.EmissionValueKey)}
 * should be the total emission of the mooring and route(s) part.
 *
 * @param <V> The type of VesselGroupEmissionValues to use.
 */
public abstract class MooringEmissionSource<V extends VesselGroupEmissionSubSource<? extends ShippingCategory>> extends ShippingEmissionSource<V> {

  private static final long serialVersionUID = 2723913128783473229L;

  /**
   * A mooring route is the route of a docked ship when arriving/leaving.
   */
  public abstract static class MooringRoute implements ShippingRouteReference, HasState, HasId, Serializable {

    private static final long serialVersionUID = -7376745369370080775L;

    private int id;
    private ShippingRoute route;
    private int shipMovementsPerTimeUnit;
    private TimeUnit timeUnit = TimeUnit.YEAR;

    /**
     * Makes a copy of the route. It shouldn't create a copy of the geographical route as it is an external object.
     * It should just set the route as the route for this object.
     *
     * Use copy(MooringRoute copy) convenience method when implementing.
     *
     * @return copy of mooring route
     */
    public abstract MooringRoute copy();

    /**
     * Convenience method to use when implementing copy().
     * @param <M> The type of mooring route.
     * @return the same object.
     */
    protected <M extends MooringRoute> M copyTo(final M copy) {
      copy.setId(id);
      copy.setRoute(route);
      copy.setShipMovementsPerTimeUnit(shipMovementsPerTimeUnit);
      copy.setTimeUnit(timeUnit);
      return copy;
    }

    @Override
    public int getId() {
      return id;
    }

    @Override
    public void setId(final int id) {
      this.id = id;
    }

    @Override
    public int getStateHash() {
      final int prime = 31;
      int result = 1;
      result = prime * result + shipMovementsPerTimeUnit;
      result = prime * result + timeUnit.ordinal();
      result = prime * result + ((route == null) ? 0 : route.getStateHash());
      return result;
    }

    public ShippingRoute getRoute() {
      return route;
    }

    /**
     * @param route The route to set. Automatically detaches/attaches routes.
     */
    public void setRoute(final ShippingRoute route) {
      if (this.route != null) {
        this.route.detachRoute(this);
      }
      this.route = route;
      if (this.route != null) {
        route.attachRoute(this);
      }
    }

    public int getShipMovementsPerTimeUnit() {
      return shipMovementsPerTimeUnit;
    }

    public void setShipMovements(final int shipMovementsPerTimeUnit, final TimeUnit timeUnit) {
      setShipMovementsPerTimeUnit(shipMovementsPerTimeUnit);
      setTimeUnit(timeUnit);
    }

    public void setShipMovementsPerTimeUnit(final int shipMovementsPerTimeUnit) {
      this.shipMovementsPerTimeUnit = shipMovementsPerTimeUnit;
    }

    public TimeUnit getTimeUnit() {
      return timeUnit;
    }

    public void setTimeUnit(final TimeUnit timeUnit) {
      this.timeUnit = timeUnit;
    }

    public int getShipMovementsPerYear() {
      return timeUnit.getPerYear(shipMovementsPerTimeUnit);
    }

    @Override
    public String toString() {
      return "id=" + id + ", route=" + route + ", shipMovementsPerTimeUnit=" + shipMovementsPerTimeUnit + ", timeUnit=" + timeUnit;
    }
  }

  private ArrayList<ShippingRoute> inlandRoutes = new ArrayList<>();

  public <E extends MooringEmissionSource<V>> E copyTo(final E copy) {
    for (final ShippingRoute route : inlandRoutes) {
      copy.getInlandRoutes().add(route.copy());
    }
    super.copyTo(copy);
    return copy;
  }

  public ArrayList<ShippingRoute> getInlandRoutes() {
    return inlandRoutes;
  }

  @Override
  public String toString() {
    return super.toString() + ", inlandRoutes=" + inlandRoutes;
  }
}
