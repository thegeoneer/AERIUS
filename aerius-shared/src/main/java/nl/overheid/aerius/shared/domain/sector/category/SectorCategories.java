/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.ShippingNode;

/**
 * Data object with all sector categories.
 */
public class SectorCategories implements Serializable {

  private static final long serialVersionUID = -5196028505716652488L;

  // Source
  private ArrayList<DiurnalVariationSpecification> diurnalVariations;

  // Sectors
  private ArrayList<Sector> sectors;

  // Shipping
  private ArrayList<ShippingNode> shippingSnappableNodes;

  // Emission categories
  private FarmLodgingCategories farmLodgingCategories;
  private RoadEmissionCategories roadEmissionCategories;
  private ArrayList<OffRoadMobileSourceCategory> offRoadMobileSourceCategories;
  private ArrayList<OnRoadMobileSourceCategory> onRoadMobileSourceCategories;
  private ArrayList<PlanCategory> planEmissionCategories;
  private ArrayList<MaritimeShippingCategory> maritimeShippingCategories;
  private InlandShippingCategories inlandShippingCategories;

  // machinery types
  private ArrayList<OffRoadMachineryType> offRoadMachineryTypes;

  /**
   * Returns the {@link Sector} matching the sectorId or {@link Sector#SECTOR_UNDEFINED}
   * if the sectorId wasn't found.
   *
   * @param sectorId The ID to search for
   * @return Sector or sector undefined if no matching id
   */
  public Sector getSectorById(final int sectorId) {
    for (final Sector s : sectors) {
      if (s.getSectorId() == sectorId) {
        return s;
      }
    }
    return Sector.SECTOR_UNDEFINED;
  }

  /**
   * @param subSectorId The subsector ID to determine the Sector object for.
   * @return The right Sector object, or Sector.SECTOR_DEFAULT if no match could be made based on the ID.
   */
  public Sector determineSectorById(final int subSectorId) {
    // Default the sector in case the default sector cannot be found in the provided list
    Sector rightSector = Sector.SECTOR_DEFAULT;

    // Find the correct sector
    if (sectors != null) {
      for (final Sector sector : sectors) {
        if (sector.getSectorId() == subSectorId) {
          rightSector = sector;
          break;
        }
      }
    }
    return rightSector;
  }

  /**
   * @param code The farm lodging category code to determine the FarmLodgingCategory object for.
   * @return The right FarmLodgingCategory object, or null if no match could be made based on the code.
   */
  public FarmLodgingCategory determineFarmLodgingCategoryByCode(final String code) {
    return farmLodgingCategories.determineFarmLodgingCategoryByCode(code);
  }

  /**
   * @param code The mobile source category code to determine the OnRoadMobileSourceCategory object for.
   * @return The right OnRoadMobileSourceCategory object, or null if no match could be made based on the code.
   */
  public OnRoadMobileSourceCategory determineOnRoadMobileSourceCategoryByCode(final String code) {
    return determineCategoryByCode(onRoadMobileSourceCategories, code);
  }

  /**
   * @param code The mobile source category code to determine the OffRoadMobileSourceCategory object for.
   * @return The right OffRoadMobileSourceCategory object, or null if no match could be made based on the code.
   */
  public OffRoadMobileSourceCategory determineOffRoadMobileSourceCategoryByCode(final String code) {
    return determineCategoryByCode(offRoadMobileSourceCategories, code);
  }

  /**
   * @param code The maritime shipping category code to determine the MaritimeShippingCategory object for.
   * @return The right MaritimeShippingCategory object, or null if no match could be made based on the code.
   */
  public MaritimeShippingCategory determineMaritimeShippingCategoryByCode(final String code) {
    return determineCategoryByCode(maritimeShippingCategories, code);
  }

  /**
   * @param code The plan category code to determine the PlanCategory object for.
   * @return The right PlanCategory object, or null if no match could be made based on the code.
   */
  public PlanCategory determinePlanEmissionCategoryByCode(final String code) {
    return determineCategoryByCode(planEmissionCategories, code);
  }

  /**
   * @param code The offroad machinery type category code to determine the OffRoadMachineryType object for.
   * @return The right OffRoadMachineryType object, or null if no match could be made based on the code.
   */
  public OffRoadMachineryType determineMachineryTypeCategoryByCode(final String code) {
    return determineCategoryByCode(offRoadMachineryTypes, code);
  }

  private <T extends AbstractCategory> T determineCategoryByCode(final ArrayList<T> categories, final String code) {
    return AbstractCategory.determineByCode(categories, code);
  }

  public ArrayList<Sector> getSectors() {
    return sectors;
  }

  public void setSectors(final ArrayList<Sector> sectors) {
    this.sectors = sectors;
  }

  public FarmLodgingCategories getFarmLodgingCategories() {
    return farmLodgingCategories;
  }

  public void setFarmLodgingCategories(final FarmLodgingCategories farmLodgingCategories) {
    this.farmLodgingCategories = farmLodgingCategories;
  }

  public RoadEmissionCategories getRoadEmissionCategories() {
    return roadEmissionCategories;
  }

  public void setRoadEmissionCategories(final RoadEmissionCategories roadEmissionCategories) {
    this.roadEmissionCategories = roadEmissionCategories;
  }

  public ArrayList<OffRoadMobileSourceCategory> getOffRoadMobileSourceCategories() {
    return offRoadMobileSourceCategories;
  }

  public void setOffRoadMobileSourceCategories(final ArrayList<OffRoadMobileSourceCategory> offRoadMobileSourceCategories) {
    this.offRoadMobileSourceCategories = offRoadMobileSourceCategories;
  }

  public ArrayList<OnRoadMobileSourceCategory> getOnRoadMobileSourceCategories() {
    return onRoadMobileSourceCategories;
  }

  public void setOnRoadMobileSourceCategories(final ArrayList<OnRoadMobileSourceCategory> onRoadMobileSourceCategories) {
    this.onRoadMobileSourceCategories = onRoadMobileSourceCategories;
  }

  public ArrayList<PlanCategory> getPlanEmissionCategories() {
    return planEmissionCategories;
  }

  public void setPlanEmissionCategories(final ArrayList<PlanCategory> planEmissionCategories) {
    this.planEmissionCategories = planEmissionCategories;
  }

  public ArrayList<MaritimeShippingCategory> getMaritimeShippingCategories() {
    return maritimeShippingCategories;
  }

  public void setMaritimeShippingCategories(final ArrayList<MaritimeShippingCategory> maritimeShippingCategories) {
    this.maritimeShippingCategories = maritimeShippingCategories;
  }

  public InlandShippingCategories getInlandShippingCategories() {
    return inlandShippingCategories;
  }

  public void setInlandShippingCategories(final InlandShippingCategories inlandShippingCategories) {
    this.inlandShippingCategories = inlandShippingCategories;
  }

  public ArrayList<ShippingNode> getShippingSnappableNodes() {
    return shippingSnappableNodes;
  }

  public void setShippingSnappableNodes(final ArrayList<ShippingNode> shippingSnappableNodes) {
    this.shippingSnappableNodes = shippingSnappableNodes;
  }

  public ArrayList<OffRoadMachineryType> getOffRoadMachineryTypes() {
    return offRoadMachineryTypes;
  }

  public void setOffRoadMachineryTypes(final ArrayList<OffRoadMachineryType> offRoadMachineryTypes) {
    this.offRoadMachineryTypes = offRoadMachineryTypes;
  }

  public void setDiurnalVariations(final ArrayList<DiurnalVariationSpecification> diurnalVariations) {
    this.diurnalVariations = diurnalVariations;
  }

  public ArrayList<DiurnalVariationSpecification> getDiurnalVariations() {
    return diurnalVariations;
  }
}
