/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.test;


/**
 * Class with all ID strings used by Selenium test.
 *
 * Seperate classes for product-specific ID's because it's easier
 * to maintain from the tester's point of view.
 */
public class TestID {

  public static final String MAP = "map";

  //#####  LocationViewImpl.java
  public static final String BUTTON_IMPORT = "buttonImport";
  public static final String BUTTON_NEXTSTEP = "buttonNextStep";
  public static final String BUTTON_CANCEL = "buttonCancel";
  public static final String BUTTON_POINTSOURCE = "buttonPoint";
  public static final String BUTTON_LINESOURCE = "buttonLine";
  public static final String BUTTON_POLYGONSOURCE = "buttonPolygon";
  public static final String BUTTON_EDITSHAPESOURCE = "buttonEditShape";

  public static final String INPUT_X_COORDINATE = "xCoordinate";
  public static final String INPUT_Y_COORDINATE = "yCoordinate";
  public static final String DIV_X_COORDINATE = "actualXCoordinate";
  public static final String DIV_Y_COORDINATE = "actualYCoordinate";
  public static final String INPUT_COORDINATES = "multiCoordinates";

  //######  OffRoadMobileInputEditor.java
  public static final String INPUT_MOBILESOURCE_TYPE_TOGGLE = "mobileSourceTypeToggle";
  public static final String INPUT_MOBILESOURCE_CATEGORY = "mobileSourceCategoryListBox";
  public static final String INPUT_MOBILESOURCE_NAME = "mobileSourceName";
  public static final String INPUT_MOBILESOURCE_FUELAMOUNT = "mobileSourceFuelAmount";
  public static final String INPUT_MOBILESOURCE_HEIGHT = "mobileSourceOpsHeight";
  public static final String INPUT_MOBILESOURCE_SPREAD = "mobileSourceOpsSpread";
  public static final String INPUT_MOBILESOURCE_HEAT = "mobileSourceOpsHeat";
  public static final String INPUT_MOBILESOURCE_EMISSION = "mobileSourceOpsEmission";
  public static final String RADIO_MOBILESOURCE_STAGECLASS = "mobileSourceStageClass";
  public static final String RADIO_MOBILESOURCE_CUSTOM = "mobileSourceCustom";

  //######  SourceOverview.java
  public static final String TOGGLE_SOURCE_LABELS = "toggleSourceLabel";
  public static final String BUTTON_PURGE_SOURCES = "purgeSources";

  //######  CalculationPointOverview.java
  public static final String TOGGLE_CALCPOINT_LABELS = "toggleCalcPointLabel";
  public static final String BUTTON_PURGE_CALCPOINTS = "purgeCalcPoint";

  //######  SourceDetailViewImpl.java
  public static final String DIV_LABEL = "rowNr";
  public static final String DIV_LOCATIONTYPE = "locationType";
  public static final String LIST_SECTORGROUP = "sectorGroupListBox";
  public static final String LIST_TIMEUNIT = "timeUnitListBox";
  public static final String LIST_SECTOR = "sectorListBox";
  public static final String DIV_HTMLPANEL = "details";
  public static final String BUTTON_EDIT_LOCATION = "buttonEditLocation";
  public static final String INPUT_LABEL = "label";
  public static final String DIV_COLLAPSEPANEL_EMISSION_SOURCE_DETAILS = "emissionSourceDetailEditor";
  public static final String DIV_COLLAPSEPANEL_SOURCECHARACTERISTICS = "sourceCharacteristicsEditor";
  public static final String DIV_COLLAPSEPANEL_WATERWAYCATEGORY = "sourceWaterwayCategory";

  //GEN
  public static final String GUI_COLLAPSEPANEL_GEN = "panel";
  public static final String INPUT_EMISSION_NH3 = "emissionNH3";
  public static final String INPUT_EMISSION_NOX = "emissionNOX";
  public static final String INPUT_EMISSION_PM10 = "emissionPM10";
  public static final String INPUT_OPS_HEIGHT = "heightEditor";
  public static final String INPUT_OPS_BUILDING_WARNING_LABEL = "inputBuildingWaringLabel";
  public static final String INPUT_OPS_BUILDING_INFLUENCE_CHECKBOX = "inputBuildingInfluenceCheckbox";
  public static final String INPUT_OPS_BUILDING_HEIGHT = "buildingHeightEditor";
  public static final String INPUT_OPS_BUILDING_LENGTH = "inputBuildingLengthEditor";
  public static final String INPUT_OPS_BUILDING_WIDTH = "inputBuildingWidthEditor";
  public static final String INPUT_OPS_BUILDING_ORIENTATION = "inputBuildingOrientationEditor";
  public static final String INPUT_OPS_SPREAD = "spreadEditor";
  public static final String INPUT_OPS_OUTFLOW_DIRECTION = "inputOpsExitDirection";
  public static final String INPUT_EMISSION_TEMPERATURE = "inputEmissionTemperature";
  public static final String INPUT_OUTFLOW_DIAMETER = "inputOutflowSurface";
  public static final String INPUT_OUTFLOW_VELOCITY = "inputOutflowVelocity";
  public static final String INPUT_OPS_HEATCONTENT = "heatContentEditor";
  public static final String INPUT_OPS_PARTICLE_SIZE_DISTRIBUTION = "particleSizeDistributionEditor";
  public static final String INPUT_OPS_HEAT_TOGGLE_BUTTON = "inputOPSHeatToggle";
  public static final String INPUT_OPS_HEAT_TOGGLE_BUTTON_FORCED = INPUT_OPS_HEAT_TOGGLE_BUTTON + "-forced";
  public static final String INPUT_OPS_HEAT_TOGGLE_BUTTON_NOT_FORCED = INPUT_OPS_HEAT_TOGGLE_BUTTON + "-not_forced";

  //FARM
  public static final String GUI_COLLAPSEPANEL_FARM = "panel";
  public static final String FARM_TOGGLE_BUTTON = "farmLodgingToggleButton";
  public static final String BUTTON_ADD_CUSTOMLODGE = "farmLodgingAddCustomLodgeButton";

  //ROAD
  public static final String ROAD_TOGGLE_BUTTON = "roadToggleButton";


  public static final String LIST_MAXSPEED = "maxSpeedListBox";
  public static final String CHECKBOX_STRICT_ENFORCEMENT = "strictEnforcement";
  public static final String INPUT_STAGNATION = "stagnationEditor";
  public static final String INPUT_VEHICLES_PER_TIMEUNIT = "vehiclesPerTimeUnit";
  public static final String LIST_VEHICLE_TYPE = "vehicleTypeListBox";
  public static final String LIST_ROAD_SELECT_TYPE = "roadSelectType";
  public static final String ROAD_TOGGLE_BUTTON_STANDARD = LIST_ROAD_SELECT_TYPE + "-0";
  public static final String ROAD_TOGGLE_BUTTON_EURO = LIST_ROAD_SELECT_TYPE + "-1";
  public static final String INPUT_VEHICLE_CUSTOM_DESCRIPTION ="vehicleCustomDescription";
  public static final String INPUT_EMISSION_VEHICLE_NOX = "emissionVehicleNOX";
  public static final String INPUT_EMISSION_VEHICLE_NO2 = "emissionVehicleNO2";
  public static final String INPUT_EMISSION_VEHICLE_NH3 = "emissionVehicleNH3";
  public static final String INPUT_EMISSION_VEHICLE_PM10 = "emissionVehiclePM10";

  // ##### EditableDivTable.java
  public static final String DIVTABLE_SOURCE = "source";
  public static final String DIVTABLE_CALCULATION_POINT = "calculationPoint";
  public static final String DIVTABLE_EDITABLE = "editableTable";
  public static final String TOOLBOX = "toolbox";
  public static final String TABLE = "divtable";
  public static final String DIVTABLE_TOOLBOX = DIVTABLE_EDITABLE + "-" + TOOLBOX;

  // FarmLodgingEditor.java
  public static final String LODGING = "lodging";
  public static final String CUSTOMLODGING = "custom";
  public static final String ADDITONAL_MEASURE = "additional";
  public static final String REDUCTIVE_MEASURE = "reductive";
  public static final String FODDER_MEASURE = "fodder";
  public static final String RAV_SUGGEST_BOX = "RAV";
  public static final String BWL_SUGGEST_BOX = "BWL";
  public static final String DESCRIPTION_BOX = "description";
  public static final String FACTOR_BOX = "factor";
  public static final String AMOUNT_BOX = "amount";
  public static final String LODGING_STACK_TYPE = "loddgingStackType";

  // PlanInputEditor.java
  public static final String INPUT_PLAN_NAME = "planName";
  public static final String INPUT_PLAN_AMOUNT = "planAmount";
  public static final String INPUT_PLAN_CATEGORY = "planCategory";

  // HeatContentCalculator.java
  public static final String INPUT_OUTFLOW_SURFACE = "inputOutflowDiameter";
  public static final String INPUT_SURROUNDING_TEMPERATURE = "surroundingTemperature";
  public static final String INPUT_HEAT_CONTENT = "heatContent";
  
  public static final String BUTTON_OPEN_CALCULATOR = "openCalculator";
  public static final String BUTTON_OPEN_CALCULATOR_MOBILE = "openCalculatorMobile";

  // OffRoadEmissionCalculator.java
  public static final String OFF_ROAD_CALCULATOR = "offroadcalculator";
  public static final String OFF_ROAD_CALCULATOR_RUNNING_HOURS = "offroadcalculatorRunningHours";
  public static final String OFF_ROAD_CALCULATOR_CONSUMPTION = "offroadcalculatorConsumption";
  public static final String OFF_ROAD_CALCULATOR_TOGGLE_BUTTON = "Togglebutton";
  public static final String OFF_ROAD_CALCULATOR_MACHINERY_LIST = "Machinerylist";
  public static final String OFF_ROAD_CALCULATOR_FUEL_LIST = "Fuellist";
  public static final String OFF_ROAD_CALCULATOR_POWER = "Power";
  public static final String OFF_ROAD_CALCULATOR_LOAD = "Load";
  public static final String OFF_ROAD_CALCULATOR_USAGE = "Usage";
  public static final String OFF_ROAD_CALCULATOR_EF_MACHINERY = "EFMachinery";
  public static final String OFF_ROAD_CALCULATOR_FUEL_USAGE = "Fuelusage";
  public static final String OFF_ROAD_CALCULATOR_EFFICIENCY = "Efficiency";
  public static final String OFF_ROAD_CALCULATOR_EF_FUEL = "EFFuel";
  public static final String OFF_ROAD_CALCULATOR_RESULT = "Result";

  // Waterway widget
  public static final String INPUT_WATERWAY = "shipWaterway";
  public static final String INPUT_WATERWAY_DIRECTION = "shipWaterwayDirection";

  // MaritimeMooringInputEditor.java
  public static final String INPUT_SHIPPING_NAME = "shipName";
  public static final String INPUT_SHIPPING_CATEGORY = "shipCategoryListbox";
  public static final String INPUT_SHIPPING_AMOUNT = "shipAmount";
  public static final String INPUT_SHIPPING_RESIDENCETIME = "shipResidenceTime";
  public static final String BUTTON_SHIPPING_ROUTES = "shipRoutesButton";
  public static final String BUTTON_SHIPPING_MARITIME_ROUTES = "shipMaritimeRoutesButton";
  public static final String INPUT_SHIPPING_MOVEMENTS = "shipMovements";
  public static final String BUTTON_SHIPPING_EDITROUTE = "shipEditRoute";
  public static final String BUTTON_SHIPPING_DELETEROUTE = "shipDeleteRoute";
  public static final String PANEL_MARITIME_ROUTES = "maritimeRoutes";

  // MaritimeRouteEditorSource.java
  public static final String BUTTON_SHIPPING_NEWROUTE = "shipRoutesNew";
  public static final String BUTTON_SHIPPING_EXISTINGROUTE = "shipExistinRoute";
  public static final String PANEL_SHIPPING_ROUTESLIST = "shipRoutesList";

  // InlandMooringInputEditor
  public static final String INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN = "inlandShipPercentageLaden";
  public static final String BUTTON_SHIPPING_INLAND_ROUTES = "inlandRoutes";
  public static final String BUTTON_SHIPPING_INLAND_NAVIGATION_DIRECTION = "inlandShipNavigationDirection";
  public static final String PANEL_SHIPPING_INLAND_DIRECTION_LIST = "inlandShipNavigationDirectionList";
  public static final String BUTTON_SHIPPING_INLAND_NAVIGATION_ARRIVE = "inlandShipNavigationArrive";
  public static final String BUTTON_SHIPPING_INLAND_NAVIGATION_DEPART = "inlandShipNavigationDepart";
  public static final String TAB_ROUTE_ARRIVE = "inlandShipRouteTabRouteArrive";
  public static final String TAB_ROUTE_DEPART = "inlandShipRouteTabRouteDepart";
  public static final String BUTTON_WATERWAY_EDITOR = "inlandShipWaterwayEditor";
  public static final String PANEL_INLAND_MOORING_ROUTES = "inlandMooringRoutes";

  // InlandRouteInputEditor
  public static final String INPUT_INLAND_SHIPPING_AMOUNT_ATOB = "inlandShipAmountAtoB";
  public static final String INPUT_INLAND_SHIPPING_AMOUNT_BTOA = "inlandShipAmountBtoA";
  public static final String INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN_ATOB = "inlandShipPercentageLadenAtoB";
  public static final String INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN_BTOA = "inlandShipPercentageLadenBtoA";
  public static final String INPUT_INLAND_TIMEUNIT_ATOB = "inlandTimeUnitAtoB";
  public static final String INPUT_INLAND_TIMEUNIT_BTOA = "inlandTimeUnitBtoA";

  //######  CalculationPointViewImpl.java
  public static final String BUTTON_DETERMINE_CALCPOINTS = "buttonDetermineCalcPoints";

  //######  CalculationOverviewViewImpl.java
  public static final String TABLE_SOURCES_OVERVIEW = DIVTABLE_SOURCE + "-" + TABLE;
  public static final String TABLE_CALCULATIONPOINT_OVERVIEW = DIVTABLE_CALCULATION_POINT + "-" + TABLE;
  public static final String TABLE_GENERIC_DIVTABLE_OVERVIEW = DIVTABLE_EDITABLE;
  public static final String BUTTON_CALCULATE = "calculate";
  public static final String TOOLBOX_SOURCE_PREFIX = "source";
  public static final String TOOLBOX_CALCULATIONPOINT_PREFIX = "calculationPoint";
  public static final String TOOLBOX_EDITABLE_DIVTABLE_PREFIX = "editableTable";
  public static final String BUTTON_EXPORT = "buttonExport";

  //######  CalculationOverviewPanel.java
  public static final String DIV_TABLE_CALC_RES_DEPOSITION_COMPARISON = "depositionComparisonTable";
  public static final String DIV_TABLE_CALC_RES_CALCPOINT_COMPARISON = "calcPointComparisonTable";
  public static final String INPUT_DEPOSITION_COMPARISON_TYPE = "depositionComparisonType";
  public static final String DEPOSITION_SPACE_TABLE_LAYERCHECKBOX = "depositionSpaceTableLayerCheckBox";

  //CollapsiblePanel.java
  public static final String BUTTON_COLLAPSEPANEL_PLUSMINUS = "buttonPlusMinus";

  // CalculateOptionsPanel.java
  public static final String RADIO_CALC_OPT_N2000 = "natureareaRadioButton";
  public static final String RADIO_CALC_OPT_RADIUS = "radiusRadioButton";
  public static final String RADIO_CALC_OPT_CALCPOINTS = "userDefinedRadioButton";
  public static final String DIV_SUBSTANCE_EMISSION = "substanceEmission";
  public static final String DIV_TOTAL_EMISSION = "totalEmission";
  public static final String DIV_TOTAL_NOX_EMISSION = "totalNOXEmission";
  public static final String DIV_TOTAL_NH3_EMISSION = "totalNH3Emission";
  public static final String INPUT_CALC_OPT_MAXRANGE = "maxRangeInput";
  public static final String INPUT_CALC_OPT_THRESHOLD = "thresholdInput";
  public static final String CHECKBOX_RESULT_TYPE = "checkBoxResultType";
  public static final String CHECKBOX_TEMPORARY_PROJECT = "checkBoxTemporaryProject";
  public static final String YEARS_LISTBOX = "yearsListBox";
  public static final String CHECKBOX_PERMIT_CALCULATION_RADIUS = "checkBoxPermitCalculationRadius";
  public static final String PERMIT_CALCULATION_RADIUS_LISTBOX = "permitCalculationRadiusListBox";

  // EditableDivTable.java
  // This generic toolbox is used several times across the application and we will therefore define
  // a post-fix for each button instead of the full id.
  // Whichever prefix is assigned to the specific toolbox will be prepended to the following ID's.
  public static final String BUTTON_COPY = "copy";
  public static final String BUTTON_DELETE = "delete";
  public static final String BUTTON_EDIT = "edit";
  public static final String BUTTON_ADD = "add";
  public static final String BUTTON_TOOLBOX_CUSTOM = "toolbox_custom";

  public static final String BUTTON_CP_COPY = TestID.TOOLBOX_CALCULATIONPOINT_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_COPY;
  public static final String BUTTON_CP_DELETE = TestID.TOOLBOX_CALCULATIONPOINT_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_DELETE;
  public static final String BUTTON_CP_EDIT = TestID.TOOLBOX_CALCULATIONPOINT_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_EDIT;
  public static final String BUTTON_CP_ADD = TestID.TOOLBOX_CALCULATIONPOINT_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_ADD;

  public static final String BUTTON_SOURCE_COPY = TestID.TOOLBOX_SOURCE_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_COPY;
  public static final String BUTTON_SOURCE_DELETE = TestID.TOOLBOX_SOURCE_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_DELETE;
  public static final String BUTTON_SOURCE_EDIT = TestID.TOOLBOX_SOURCE_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_EDIT;
  public static final String BUTTON_SOURCE_ADD = TestID.TOOLBOX_SOURCE_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_ADD;

  public static final String BUTTON_EDITABLETABLE_COPY = TestID.TOOLBOX_EDITABLE_DIVTABLE_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_COPY;
  public static final String BUTTON_EDITABLETABLE_DELETE = TestID.TOOLBOX_EDITABLE_DIVTABLE_PREFIX + "-" + TestID.TOOLBOX + "-"
      + TestID.BUTTON_DELETE;
  public static final String BUTTON_EDITABLETABLE_EDIT = TestID.TOOLBOX_EDITABLE_DIVTABLE_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_EDIT;
  public static final String BUTTON_EDITABLETABLE_ADD = TestID.TOOLBOX_EDITABLE_DIVTABLE_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_ADD;
  public static final String BUTTON_EDITABLETABLE_SUBMIT = "editableTableSubmit";
  public static final String BUTTON_EDITABLETABLE_CANCEL = "editableTableCancel";

  //###### CalculatorViewImpl.java
  public static final String BUTTON_LEGENDPANEL = "legendPanelButton";
  public static final String BUTTON_INFOPANEL = "infoPanelButton";
  public static final String BUTTON_OPTIONSPANEL = "optionsPanelButton";
  public static final String BUTTON_LAYERPANEL = "layerPanelButton";
  public static final String BUTTON_YEARSELECTION = "yearSelection";
  public static final String BUTTON_SUBSTANCESELECTION = "substanceSelection";
  public static final String BUTTON_RESULTTYPESELECTION = "resultTypeSelection";
  public static final String LABEL_YEARSELECTION = "year";
  public static final String LABEL_SUBSTANCESELECTION = "substance";
  public static final String BUTTON_RESULTTYPE_DEPOSITION = "deposition";
  public static final String BUTTON_RESULTTYPE_CONCENTRATION = "concentration";
  public static final String BUTTON_RESULTTYPE_NUM_OF_DAYS = "numOfDays";

  //###### SearchPanel.java
  public static final String INPUT_SEARCHBOX = "searchBox";
  public static final String DIV_SEARCHSUGGESTION = "searchSuggestion";
  public static final String DIV_SEARCHSUGGESTIONCATEGORY = "searchSuggestionType";
  public static final String BUTTON_SEARCHIMAGE = "searchImage";

  //###### LayerPanelItem.java
  public static final String DIV_LAYERPANELITEM = "layername";
  public static final String DIV_LAYERPANELDRAGGER = "dragger";
  public static final String DIV_LAYERPANELCHECKBOX = "checkbox";
  public static final String DIV_LAYERPANELZOOM = "zoom";

  //###### LayerPanelItemListbox.java
  public static final String INPUT_LAYERPANEL_LISTBOX = "layerPanelList";
  public static final String INPUT_LAYERPANEL_CHECKBOX = "layerPanelCheckBox";

  //###### ProgressViewImpl.java
  public static final String DIV_CALC_ABORTWARNING = "calculationAbortedWarningText";
  public static final String DIV_CALC_EXPLANATION = "explanationText";
  public static final String DIV_CALC_STOPTEXT = "modifyStopText";

  //###### ExportDialogPanel.java
  public static final String INPUT_EXPORT_COORPORATION = "corporation";
  public static final String INPUT_EXPORT_PROJECTNAME = "projectName";
  public static final String INPUT_EXPORT_STREET_ADDRESS = "streetAddress";
  public static final String INPUT_EXPORT_POSTCODE = "postcode";
  public static final String INPUT_EXPORT_CITY = "city";
  public static final String INPUT_EXPORT_DESCRIPTION = "description";
  public static final String INPUT_EXPORT_EMAILADRESS = "emailAddress";
  public static final String RADIO_EXPORT_GISONLY = "buttonGISOnlyInput";
  public static final String RADIO_EXPORT_GISALL = "buttonGISAll";
  public static final String RADIO_EXPORT_PAA_DEVELOPMENT_SPACES = "buttonPAADevelopmentSpaces";
  public static final String RADIO_EXPORT_PAA_DEMAND = "buttonPAADemand";
  public static final String RADIO_EXPORT_PAA_OWNUSE = "buttonPAAOwnUse";

  //###### LayerPopupBase.java
  public static final String BUTTON_PANEL_OK = "okButton";

  //###### ConfirmCancelDialog.java
  public static final String BUTTON_DIALOG_CONFIRM = "okButton";
  public static final String BUTTON_DIALOG_CANCEL = "cancelButton";

  //###### NotificationPanel.java
  public static final String DIV_NOTIFICATIONLIST = "notificationList";
  public static final String BUTTON_NOTIFICATION = "notificationButton";
  public static final String DIV_NOTIFICATIONPOPUP = "attachedPopupPanel";

  //###### NotificationPopup.java
  public static final String BUTTON_DELETE_ALL_NOTIFICATIONS = "deleteAllNotifications";

  //###### NotificationItem.java
  public static final String DIV_NOTIFICATIONITEM = "item";
  public static final String BUTTON_NOTIFICATION_CLOSE = "closeButton";

  //###### NavigationWidget.java
  public static final String BUTTON_ZOOM_IN = "zoomIn";
  public static final String BUTTON_ZOOM_OUT = "zoomOut";
  public static final String BUTTON_PAN = "panButton";

  //###### ImportDialogPanel.java
  public static final String INPUT_IMPORTFILE = "import-filefield";
  public static final String RADIO_IMPORT_SUBSTANCE = "importSubstance";
  public static final String RADIO_IMPORT_PM10 = "importSubstance-pm10";
  public static final String RADIO_IMPORT_NOX = "importSubstance-nox";
  public static final String RADIO_IMPORT_NH3 = "importSubstance-nh3";
  public static final String RADIO_IMPORT_OVERWRITE = "conflictOverwriteRadioButton";
  public static final String RADIO_IMPORT_ADD = "conflictAddRadioButton";
  public static final String RADIO_IMPORT_REMOVE_EXISTING = "conflictRemoveExistingRadioButton";
  public static final String CHECKBOX_IMPORT_ERRORCONTINUE = "errorContinueButton";
  public static final String DIV_IMPORT_ERRORS = "errorField";
  public static final String DIV_IMPORT_DUPLICATE_ERRORS = "errorDuplicateField";
  public static final String DIV_IMPORT_COUNT = "counterLabel";
  public static final String DIV_IMPORT_CONFLICTSCOUNT = "counterConflictLabel";
  public static final String CHECKBOX_UTILISATION = "checkBoxUtilisation";
  public static final String LABEL_CHECKBOX_UTILISATION = "labelCheckBoxUtilisation";
 
  //###### DetermineCalculationPointsDialogPanel.java
  public static final String DETERMINE_CALCPOINTS_BOUNDARY = "determineCalcpointsBoundary";
  public static final String DETERMINE_CALCPOINTS_NOTSOUSELESSCALCBUTTON = "determineCalcpointsNotSoUselessCalcButton";
  public static final String DETERMINE_CALCPOINTS_RESULTLABEL = "determineCalcpointsResultLabel";

  //###### StartUpViewImpl.java
  public static final String BUTTON_INPUT_SOURCES = "inputSource";

  //###### ErrorPopupPanel.java
  public static final String DIV_VALIDATION_POPUP = "contentValidationPopup";

  //###### SituationTabPanel.java
  public static final String TAB_SIT_1 = "tabSituation1";
  public static final String TAB_SIT_2 = "tabSituation2";
  public static final String TAB_NEW = "tabNewSituation";
  public static final String TAB_COMPARE = "tabCompare";
  public static final String TAB_SWITCH = "tabSwitch";

  //###### SituationTabItem.java
  public static final String TAB_SIT_LABEL = "tabSitLabel";

  //###### SituationTabItem.java
  public static final String TAB_SIT_NAMEFIELD = "tabSitNameField";
  public static final String TAB_SIT_DELETE = "tabSitDelete";
  public static final String TAB_SIT_OK = "tabSitOk";

  //##### ComparisonOverview.java
  public static final String TABLE_COMPARISON_OVERVIEW = "tableComparisonOverview";

  // Calculator Menu Items
  public static final String MENU_ITEMS = "menuItem";
  public static final String MENU_ITEM_START = "start";
  public static final String MENU_ITEM_EMISSIONSOURCES_OVERVIEW = "emissionSources";
  public static final String MENU_ITEM_CALCULATION_POINTS = "calculationPoints";
  public static final String MENU_ITEM_RESULTS = "calculationResults";
  public static final String MENU_ITEM_MELDING = "meldingMenu";

  // Scenario Menu Items
  public static final String MENU_ITEM_SCENARIOS = "calculationScenarios";
  public static final String MENU_ITEM_UTILS = "calculationUtils";

  public static final String LIST_NATURA = "naturaSelectBox";
  public static final String LIST_HABITAT = "habitatSelectBox";

  public static final String MENU_ITEM_CALCULATOR_THEME = "calculatorThemeSwitch";

  public static final String RESULT_GROUPBUTTONS = "resultGroupButtons";

  // Export
  public static final String EXPORT_SELECT_NATURA = "exportSelectNatura";
  public static final String EXPORT_SELECT_PROVINCE = "exportSelectProvince";
  public static final String EXPORT_SELECT_NATIONAL = "exportSelectNational";
  public static final String EXPORT_NATURALISTBOX = "exportNaturaListbox";
  public static final String EXPORT_PROVINCELISTBOX = "exportProvinceListbox";
  public static final String EXPORT_OBJECT_TYPES = "exportObjectTypes";
  public static final String EXPORT_FILE_TYPES = "exportFileTypes";
  public static final String EXPORT_YEARS = "exportYears";
  public static final String EXPORT_RESULT_TYPES = "exportResultTypes";
  public static final String EXPORT_CANCEL_BUTTON = "exportCancelButton";
  public static final String EXPORT_DOWNLOAD_BUTTON = "exportDownloadButton";
  public static final String EXPORT_COORPORATION_EMAIL = "exportCoorporationEmail";

  // Application change status
  public static final String STATUS_CHANGE_ITEM_PREFIX = "statusChange-";
  public static final String STATUS_CHANGE_CANCEL_BUTTON = "statusChangeCancelButton";
  public static final String STATUS_CHANGE_OK_BUTTON = "statusChangeOkButton";

  public static final String COMPACT_HABITAT_TYPE_TABLE = "compactHabitatTypeTable";

  public static final String DEPOSITION_YEAR_INFO_BAR = "depositionYearInfoBar";

  public static final String CELLTABLE_NETWORK = "celltableNetwork";

  public static final String TABLE_SOURCES_CHARACTERISTICS_VIEWER = "tableSourcesDetailsViewer";

  public static final String TABLE_SOURCES_SECTOR_VIEWER = "tableSourcesSectorViewer";

  public static final String HELP_BUTTON_TOGGLE = "helpButtonToggle";

  //#### Generic ID's
  public static final String ID = "id";
  public static final String ICON = "icon";
  public static final String LABEL = "label";
  public static final String DEPOSITION = "deposition";
  public static final String EMISSION = "emission";
  public static final String VALUE = "value";
  public static final String INNER_TABLE = "innerTable";

  public static final String CODE = "code";

  public static final String RELEVANT_COVERAGE = "relevantCoverage";
  public static final String CRITICAL_LOAD = "criticalLoad";

  public static final String TURBOTURBO = "turboturbo";

  public static final String BUTTON_MELDING_EXPORT = "meldingVergunningExportButton";
  public static final String BUTTON_MELDING = "meldingButton";

  public static final String BUTTON_MELDING_SUBMIT = "buttonMeldingSubmit";

  public static final String BUTTON_FILTER_APPLY = "filterApply";
  public static final String BUTTON_FILTER_RESET = "filterReset";
  public static final String DIV_FILTER_PANEL = "filterPanel";

  public static final String SYSTEM_MESSAGE_TXT = "message";
  public static final String SYSTEM_MESSAGE_LOCALE = "locale";
  public static final String SYSTEM_MESSAGE_UUID = "uuid";
  public static final String SYSTEM_MESSAGE_DELETE = "delete-all";
  public static final String SYSTEM_MESSAGE_SUBMIT = "submit";
  public static final String SYSTEM_MESSAGE_BANNER = "banner";

  public static final String FILTER_LOWER_KNOB = "habitatFilterLowerKnob";
  public static final String FILTER_UPPER_KNOB = "habitatFilterUpperKnob";

  public static final String AREAINFORMATION_TYPETABLE_ECOLOGICAL_QUALITY = "ecoQuality";
  public static final String AREAINFORMATION_TYPETABLE_SURFACE = "surface";
  public static final String AREAINFORMATION_TYPETABLE_EXTENT = "extent";
  public static final String AREAINFORMATION_TYPETABLE_QUALITY_GOAL = "qualityGoal";
  public static final String AREAINFORMATION_TYPETABLE_CRITICAL_DEPOSITION = "criticalDeposition";

  public static final String AREAINFORMATION_TYPETABLE_SURFACE_TOTAL_MAPPED = "totalMapped";
  public static final String AREAINFORMATION_TYPETABLE_SURFACE_TOTAL_CARTOGRAPHIC = "totalCartographic";
  public static final String AREAINFORMATION_TYPETABLE_SURFACE_RELEVANT_MAPPED = "relevantMapped";
  public static final String AREAINFORMATION_TYPETABLE_SURFACE_RELEVANT_CARTOGRAPHIC = "relevantMapped";
  public static final String AREAINFORMATION_TYPETABLE_RELEVANT_COVERAGE = "relevantCoverage";

  public static final String AREAINFORMATION_NAME = "areaInfoName";
  public static final String AREAINFORMATION_ID = "areaInfoId";
  public static final String AREAINFORMATION_CONTRACTOR = "areaInfoContractor";
  public static final String AREAINFORMATION_QUALITY = "areaInfoQuality";
  public static final String AREAINFORMATION_ENVIRONMENT = "areaInfoEnvironment";
  public static final String AREAINFORMATION_SURFACE = "areaInfoSurface";
  public static final String AREAINFORMATION_PROTECTION = "areaInfoProtection";
  public static final String AREAINFORMATION_STATUS = "areaInfoStatus";

  public static String concat(final String ... testIdParts) {
    final StringBuilder builder = new StringBuilder();
    for (int i = 0; i < testIdParts.length; i++) {
      builder.append(testIdParts[i]);

      if (i != testIdParts.length - 1) {
        builder.append("-");
      }
    }

    return builder.toString();
  }

  protected TestID() {
    // Util class constructor
  }
}
