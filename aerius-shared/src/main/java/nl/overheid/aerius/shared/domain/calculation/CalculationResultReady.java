/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;

/**
 * Event indicating a calculation result is available on the server.
 */
public class CalculationResultReady implements Serializable {

  private static final long serialVersionUID = -7171279993188029773L;

  private String key;
  private String uid;
  private boolean calculationFinished;

  // Needed for GWT.
  public CalculationResultReady() {
    // Needed for GWT.
  }

  /**
   * Initializes this event with the given calculation key, unique chunk id, flag to let the client know whether the calculation is finished.
   *
   * @param calculationKey calculation key
   * @param calculationFinished Whether the calculation finished
   */
  public CalculationResultReady(final String calculationKey, final boolean calculationFinished, final String uniqueObjectId) {
    this.key = calculationKey;
    this.calculationFinished = calculationFinished;
    //id of the result, used to make this result unique (via {@link #equals(Object)} or {@link #hashCode()}
    //This is required for the client side.
    this.uid = uniqueObjectId;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass()
        ? (uid != null ? uid.equals(((CalculationResultReady) obj).uid) : false)
            && key.equals(((CalculationResultReady) obj).key) : false;
  }

  public String getKey() {
    return key;
  }

  @Override
  public int hashCode() {
    return (31 * (17 + (uid == null ? 1 : uid.hashCode()))) + (key == null ? 1 : key.hashCode());
  }

  // TODO Calculation finished should be moved out of this class so it doesn't depend on calculation results.
  public boolean isCalculationFinished() {
    return calculationFinished;
  }
}
