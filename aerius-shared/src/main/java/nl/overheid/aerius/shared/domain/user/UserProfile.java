/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.user;

import java.io.Serializable;
import java.util.HashSet;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.auth.Permission;

/**
 * Data object of user profile.
 * TODO When email is the name: the fields email and name are basically the same.
 */
public class UserProfile implements HasId, Serializable {

  private static final long serialVersionUID = 158544847778890161L;

  private int id;
  private String name;
  private String emailAddress;
  private String initials;
  private String firstName;
  private String lastName;
  private Authority authority;
  private HashSet<String> permissions = new HashSet<>();
  private HashSet<UserRole> roles = new HashSet<>();

  /**
   * Check whether the current user has given permission.
   * @param permission The permission to check.
   * @return true if the permission is present, false otherwise.
   * @throws IllegalArgumentException When null is supplied.
   */
  public boolean hasPermission(final Permission permission) {
    checkArgument(permission);
    return permissions.contains(permission.getName());
  }

  /**
   * Add given permission for the current user.
   * @param permission The permission to add for this user.
   * @throws IllegalArgumentException When null is supplied.
   */
  public void addPermission(final Permission permission) {
    checkArgument(permission);
    permissions.add(permission.getName());
  }

  private void checkArgument(final Permission permission) {
    if (permission == null) {
      throw new IllegalArgumentException("Invalid argument (permission was null).");
    }
  }
  /**
   * Whether user has given role.
   *
   * @param role
   *          The role to check for.
   * @return true if the user has role, false otherwise.
   */
  public boolean hasRole(final UserRole role) {
    return roles.contains(role);
  }

  public HashSet<UserRole> getRoles() {
    return roles;
  }

  public void setRoles(final HashSet<UserRole> roles) {
    this.roles = roles;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(final String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public String getInitials() {
    return initials;
  }

  public void setInitials(final String initials) {
    this.initials = initials;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public Authority getAuthority() {
    return authority;
  }

  public void setAuthority(final Authority authority) {
    this.authority = authority;
  }

  public HashSet<String> getPermissions() {
    return permissions;
  }

  public void setPermissions(final HashSet<String> permissions) {
    this.permissions = permissions;
  }

  public UserProfile copy() {
    return copyTo(new UserProfile());
  }

  public <U extends UserProfile> U copyTo(final U copy) {
    copy.setAuthority(authority);
    copy.setEmailAddress(emailAddress);
    copy.setFirstName(firstName);
    copy.setId(id);
    copy.setInitials(initials);
    copy.setLastName(lastName);
    copy.setName(name);
    copy.setPermissions(new HashSet<String>(permissions));

    final HashSet<UserRole> rolesCopy = new HashSet<>();
    for (final UserRole role : roles) {
      rolesCopy.add(role.copy());
    }
    copy.setRoles(rolesCopy);

    return copy;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && id == ((UserProfile) obj).id;
  }

  @Override
  public String toString() {
    return "UserProfile [id=" + id + ", name=" + name + ", emailAddress=" + emailAddress + ", initials=" + initials
        + ", firstName=" + firstName + ", lastName=" + lastName + ", authority=" + authority + ", permissions=" + permissions
        + "]";
  }
}
