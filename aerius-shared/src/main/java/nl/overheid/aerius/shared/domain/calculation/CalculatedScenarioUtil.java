/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.util.List;

import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Util class to convert a Scenario to a CalculatedScenario object.
 */
public final class CalculatedScenarioUtil {

  private CalculatedScenarioUtil() {
    // util class.
  }

  /**
   * Creates a {@link CalculatedScenario} from a {@link Scenario}. Only works for scenario's with 1 or 2 source lists.
   *
   * @param scenario Scenario to get calculated scenario information like sources from
   * @return new Calculated Scenario object.
   * If (at least) 2 source lists are available in the scenario, it'll be a CalculatedComparison (based on first 2 source lists).
   * If 1 source lists is available in the scenario, it'll be a CalculatedSingle.
   */
  public static CalculatedScenario toCalculatedScenario(final Scenario scenario) {
    final List<EmissionSourceList> sl = scenario.getSourceLists();
    return CalculatedScenarioUtil.toCalculatedScenario(scenario, sl.get(0).getId(), sl.size() > 1 ? sl.get(1).getId() : -1);
  }

  /**
   * Creates a {@link CalculatedScenario} from a {@link Scenario} using the 2 situation id's. If the id of situation 2 < 0 then a single
   * calculation is initialized else it is a comparison calculation.
   *
   * @param scenario Scenario to get sources from
   * @param situationId1 id of situation 1 in scenario sources list
   * @param situationId2 id of situation 2 in scenario sources list. If < 0 then assumes a single situation.
   * @return new Calculated Scenario object
   */
  public static CalculatedScenario toCalculatedScenario(final Scenario scenario, final int situationId1, final int situationId2) {
    final CalculatedScenario returnScenario;

    if (situationId2 < 0) {
      final CalculatedSingle cs = new CalculatedSingle();
      cs.setSources(scenario.getSources(situationId1));
      returnScenario = cs;
    } else {
      final EmissionSourceList sources1 = scenario.getSources(situationId1);
      final EmissionSourceList sources2 = scenario.getSources(situationId2);

      // If either source list doesn't exist, start a single calculation
      if (sources1 == null || sources2 == null) {
        final CalculatedSingle cs = new CalculatedSingle();
        cs.setSources(sources1 == null || sources1.isEmpty() ? sources2 : sources1);
        returnScenario = cs;
      } else {
        final CalculatedComparison cc = new CalculatedComparison();
        cc.setSourcesOne(sources1);
        cc.setSourcesTwo(sources2);
        returnScenario = cc;
      }
    }
    returnScenario.getScenario().setMetaData(scenario.getMetaData());
    returnScenario.getCalculationPoints().addAll(scenario.getCalculationPoints());
    for (int j = 0; j < scenario.getSourceLists().size(); j++) {
      returnScenario.getScenario().addResultPoints(scenario.getResultPoints(j));
    }
    // Certify the scenario.
    returnScenario.certify();
    return returnScenario;
  }
}
