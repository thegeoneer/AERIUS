/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasName;

/**
 * Contains the core habitat type information.
 */
public class HabitatType implements Serializable, Comparable<HabitatType>, HasName, HasId {

  private static final long serialVersionUID = -1546036783511200909L;

  private int id;
  private String code;
  private String name;

  // Required by GWT.
  public HabitatType() {}

  /**
   * Convenience setter constructor.
   * @param code The code
   * @param name The name
   */
  public HabitatType(final String code, final String name) {
    this.code = code;
    this.name = name;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public int compareTo(final HabitatType o) {
    return name.compareTo(o.getName());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((code == null) ? 0 : code.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    boolean equals = false;
    if (obj != null && this.getClass() == obj.getClass()) {
      final HabitatType other = (HabitatType) obj;
      equals = ((this.code == null && other.code == null) || (this.code != null && this.code.equals(other.code)))
          && ((this.name == null && other.name == null) || (this.name != null && this.name.equals(other.name)));
    }
    return equals;
  }

  @Override
  public String toString() {
    return "HabitatType [id=" + id + ", code=" + code + ", name=" + name + "]";
  }

}
