/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

import java.util.ArrayList;
import java.util.Date;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

public class ScenarioBaseUserContext implements UserContext {
  private static final long serialVersionUID = 4542643522171364825L;

  private EmissionValueKey emissionValueKey;
  private EmissionResultKey emissionResultKey;

  private EmissionResultValueDisplaySettings emissionResultValueDisplaySettings;

  private LayerProps baseLayer;
  private ArrayList<LayerProps> layers;
  private CalculatorLimits calculatorLimits;
  private CalculatedScenario calculatedScenario;

  protected Scenario scenario;
  private EmissionSourceList sourcesCopy;

  public ScenarioBaseUserContext() {
    this.scenario = new Scenario();
  }

  @Override
  public LayerProps getBaseLayer() {
    return baseLayer;
  }

  public void setBaseLayer(final LayerProps baseLayer) {
    this.baseLayer = baseLayer;
  }

  @Override
  public ArrayList<LayerProps> getLayers() {
    return layers;
  }

  public void setLayers(final ArrayList<LayerProps> layers) {
    this.layers = layers;
  }

  public CalculatedScenario getCalculatedScenario() {
    return calculatedScenario;
  }

  public void setCalculatedScenario(final CalculatedScenario calculatedScenario) {
    this.calculatedScenario = calculatedScenario;
  }

  @Override
  public EmissionValueKey getEmissionValueKey() {
    if (emissionValueKey == null) {
      // To get the year, we're using the deprecated getYear method because Calendar is not supported in GWT.
      emissionValueKey = new EmissionValueKey(new Date().getYear() + 1900, Substance.NOXNH3);
    }
    return emissionValueKey;
  }

  public void setEmissionValueKey(final EmissionValueKey emissionValueKey) {
    this.emissionValueKey = emissionValueKey;
  }

  public EmissionResultKey getEmissionResultKey() {
    if (emissionResultKey == null) {
      emissionResultKey = EmissionResultKey.valueOf(getEmissionValueKey().getSubstance());
    }
    return emissionResultKey;
  }

  public void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
  }

  public CalculatorLimits getCalculatorLimits() {
    return calculatorLimits;
  }

  public void setCalculatorLimits(final CalculatorLimits calculatorLimits) {
    this.calculatorLimits = calculatorLimits;
  }

  public Scenario getScenario() {
    return scenario;
  }

  public void setSourceListCopy(final EmissionSourceList emissionSourceList) {
    sourcesCopy = emissionSourceList;
  }

  public EmissionSourceList getSourceListCopy() {
    return sourcesCopy;
  }

  /**
   * Get the {@link EmissionSourceList} by id of the list.
   *
   * @param elsId
   *          id of the list
   * @return {@link EmissionSourceList} or null if not found
   */
  public EmissionSourceList getSources(final int sid1) {
    return scenario.getSources(sid1);
  }

  public void addSources(final EmissionSourceList esl) {
    scenario.addSources(esl);
  }

  public ScenarioMetaData getMetaData() {
    return scenario.getMetaData();
  }

  public ArrayList<EmissionSourceList> getSourceLists() {
    return scenario.getSourceLists();
  }

  /**
   * Check to see if all sourcelist have sources.
   *
   * @return True if there is a source in all of the emission source lists.
   */
  public boolean hasSources() {
    return scenario.hasSources();
  }

  /**
   * Remove the {@link EmissionSourceList} for the given id. If the id is not present nothing changes.
   *
   * @param elsId
   *          id of the {@link EmissionSourceList}to remove
   */
  public void removeSources(final int elsId) {
    scenario.removeSources(elsId);
  }

  public CalculationPointList getCalculationPoints() {
    return scenario.getCalculationPoints();
  }

  public void setCalculationPoints(final CalculationPointList calculationPoints) {
    scenario.setCalculationPoints(calculationPoints);
  }

  public void setMetaData(final ScenarioMetaData metaData) {
    scenario.setMetaData(metaData);
  }

  @Override
  public EmissionResultValueDisplaySettings getEmissionResultValueDisplaySettings() {
    return emissionResultValueDisplaySettings;
  }

  public void setEmissionResultValueDisplaySettings(final EmissionResultValueDisplaySettings emissionResultValueDisplaySettings) {
    this.emissionResultValueDisplaySettings = emissionResultValueDisplaySettings;
  }
}
