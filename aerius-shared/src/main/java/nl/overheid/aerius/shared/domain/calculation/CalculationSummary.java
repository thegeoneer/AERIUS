/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResults;
import nl.overheid.aerius.shared.domain.request.PotentialRequestInfo;

/**
 * Data class for calculation summary results.
 */
public class CalculationSummary implements Serializable {

  private static final long serialVersionUID = -7141275965364070985L;

  private PotentialRequestInfo potentialRequestInfo;

  /**
   * Summary data per assessment area.
   */
  private HashMap<Integer, CalculationAreaSummaryResult> summaryResult = new HashMap<>();

  /**
   * Date and time at which the development space was last synced (should happen nightly). This is communicated to the
   * user and applies to {@link CalculationAreaSummaryResult#getDevelopmentRuleResults()}.
   */
  private Date developmentSpaceSnapshotTime;

  /**
   * Get the summary results in a HashMap, with assessment area id as key of the map and data per assessment area as value.
   * @return hashmap
   */
  public HashMap<Integer, CalculationAreaSummaryResult> getSummaryResult() {
    return summaryResult;
  }

  public void setSummaryResult(final HashMap<Integer, CalculationAreaSummaryResult> summaryResult) {
    this.summaryResult = summaryResult;
  }

  public PotentialRequestInfo getPotentialRequestInfo() {
    return potentialRequestInfo;
  }

  public void setPotentialRequestInfo(final PotentialRequestInfo potentialRequestType) {
    this.potentialRequestInfo = potentialRequestType;
  }

  public Date getDevelopmentSpaceSnapshotTime() {
    return developmentSpaceSnapshotTime;
  }

  public void setDevelopmentSpaceSnapshotTime(final Date developmentSpaceSnapshotTime) {
    this.developmentSpaceSnapshotTime = developmentSpaceSnapshotTime;
  }

  public boolean isDevelopmentResultAvailable() {
    final List<CalculationAreaSummaryResult> areas = new ArrayList<CalculationAreaSummaryResult>(getSummaryResult().values());
    // test if any Development result is available
    for (final CalculationAreaSummaryResult area : areas) {
      if (!area.getDevelopmentRuleResults().isEmpty()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Remove results for the given calculationId.
   * @param calculationId calculation to remove
   */
  public void removeResults(final int calculationId) {
    if (summaryResult != null) {
      for (final Entry<Integer, CalculationAreaSummaryResult> entry : summaryResult.entrySet()) {
        entry.getValue().getDevelopmentRuleResults().clear();
        for (final Entry<Integer, HabitatSummaryResults> habitatEntry : entry.getValue().getHabitats().entrySet()) {
          habitatEntry.getValue().remove(calculationId);
        }
      }
    }
  }
}
