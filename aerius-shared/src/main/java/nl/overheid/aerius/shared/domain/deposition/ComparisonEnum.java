/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.deposition;

import nl.overheid.aerius.shared.domain.Range;

/**
 * Enum for categorizing calculations differences by amount of different deposition
 * and defines a color for the difference.
 */
public enum ComparisonEnum implements Range {

  LESS_5(-20, "507122"),
  LESS_4(-13, "65853F"),
  LESS_3(-5, "84A267"),
  LESS_2(-3, "A7C296"),
  LESS_1(-1, "C7DEBE"),
  NEGATIVE_NEGLIGIBLE(-0.05, "E2EFE1"),
  SAME(0, "EDEDED"),
  POSITIVE_NEGLIGIBLE(0.05, "DED9EF"),
  MORE_1(1, "C6BFDD"),
  MORE_2(3, "A299C4"),
  MORE_3(5, "7A6CA7"),
  MORE_4(13, "55438E"),
  MORE_5(20, "3D277C");

  public static final ComparisonEnum[] BELOW_ZERO =  {NEGATIVE_NEGLIGIBLE, LESS_1, LESS_2, LESS_3, LESS_4, LESS_5};
  public static final ComparisonEnum[] ABOVE_ZERO = {POSITIVE_NEGLIGIBLE, MORE_1, MORE_2, MORE_3, MORE_4, MORE_5};
  public static final ComparisonEnum[] NON_TRIVIAL =
    {LESS_5, LESS_4, LESS_3, LESS_2, LESS_1, NEGATIVE_NEGLIGIBLE, SAME, POSITIVE_NEGLIGIBLE, MORE_1, MORE_2, MORE_3, MORE_4, MORE_5 };

  private final double value;
  private final String color;

  private ComparisonEnum(final double value, final String color) {
    this.value = value;
    this.color = color;
  }

  @Override
  public String getColor() {
    return color;
  }

  public double getValue() {
    return value;
  }

  /**
   * Get the value the difference has considering the ranges.
   * @param value The difference value.
   * @return Representing enum value.
   */
  public static ComparisonEnum getRange(final double value) {
    if (value < NEGATIVE_NEGLIGIBLE.value) {
      for (int i = BELOW_ZERO.length - 1; i >= 0; i--) {
        if (value <= BELOW_ZERO[i].value) {
          return BELOW_ZERO[i];
        }
      }
      // Left-overs will be included with LESS_5
      return LESS_5;
    } else if (value > POSITIVE_NEGLIGIBLE.value) {
      for (int i = ABOVE_ZERO.length - 1; i >= 0; i--) {
        if (value >= ABOVE_ZERO[i].value) {
          return ABOVE_ZERO[i];
        }
      }
      // Left-overs will be included with MORE_5
      return MORE_5;
    } else {
      return SAME;
    }
  }

  @Override
  public double getLowerValue() {
    return value;
  }
}
