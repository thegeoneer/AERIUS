/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer;

import java.io.Serializable;

public class UserGeoLayer implements Serializable {

  private static final long serialVersionUID = 3837297216252605881L;

  private String key;
  private String type;

  public String getKey() {
    return key;
  }

  public UserGeoLayer setKey(final String key) {
    this.key = key;
    return this;
  }

  public String getType() {
    return type;
  }

  public UserGeoLayer setType(final String type) {
    this.type = type;
    return this;
  }

}
