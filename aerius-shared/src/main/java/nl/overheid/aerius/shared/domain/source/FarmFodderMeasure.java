/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasState;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;

/**
 * Data class for a single Farm Lodging Fodder Measure.
 */
public class FarmFodderMeasure implements Serializable, HasState {

  private static final long serialVersionUID = -4140992373335917141L;

  private FarmLodgingFodderMeasureCategory category;

  /**
   * @return A copy of this fodder measure.
   */
  public FarmFodderMeasure copy() {
    final FarmFodderMeasure copy = new FarmFodderMeasure();
    copy.setCategory(getCategory());
    return copy;
  }

  public FarmLodgingFodderMeasureCategory getCategory() {
    return category;
  }

  public void setCategory(final FarmLodgingFodderMeasureCategory category) {
    this.category = category;
  }

  /**
   * Returns the emission factor for the emissions originating from the floor.
   * Note that the reduction factor which was fetched from the database is converted to an emission factor
   * (ef = 1 - rf).
   * @param key The key to determine the factor to be used on the emission.
   * @return The emission factor to be used on the emission (fraction).
   */
  public double getEmissionFactorFloor(final EmissionValueKey key) {
    return getCategory() != null && key != null && Substance.NH3 == key.getSubstance()
        ? 1 - getCategory().getReductionFactorFloor() : 0;
  }

  /**
   * See {@link #getEmissionFactorFloor} but for the emissions originating from the manure pit.
   */
  public double getEmissionFactorCellar(final EmissionValueKey key) {
    return getCategory() != null && key != null && Substance.NH3 == key.getSubstance()
        ? 1 - getCategory().getReductionFactorCellar() : 0;
  }

  /**
   * See {@link #getEmissionFactorFloor} but for the emissions originating from both the floor and
   * the manure pit. Only use this in case there is only one measure.
   */
  public double getEmissionFactorTotal(final EmissionValueKey key) {
    return getCategory() != null && key != null && Substance.NH3 == key.getSubstance()
        ? 1 - getCategory().getReductionFactorTotal() : 0;
  }
  /**
   * Return the proportion of ammonia originating from the floor for the given lodging type.
   * Will fail when there is no proportion for the animal category. You can check this with
   * {@link FarmLodgingFodderMeasureCategory#canApplyToFarmLodgingCategory}.
   */
  public double getProportionFloor(final FarmLodgingCategory farmLodgingCategory) {
      return category.getAmmoniaProportions().get(farmLodgingCategory.getAnimalCategory()).getProportionFloor();
  }

  /**
   * See {@link #getProportionFloor} but for the emissions originating from the manure pit.
   */
  public double getProportionCellar(final FarmLodgingCategory farmLodgingCategory) {
      return category.getAmmoniaProportions().get(farmLodgingCategory.getAnimalCategory()).getProportionCellar();
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = prime * result + category.getStateHash();
    return result;
  }

}
