/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario.connect;

import java.io.Serializable;

public class ConnectCalculationFile implements Serializable {

  private static final long serialVersionUID = -8708925446072232722L;

  public enum Situation {
    CURRENT,
    PROPOSED
  }

  private String uuid;
  private Situation situation = Situation.PROPOSED;
  private boolean researchArea;

  private transient String filename;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  public Situation getSituation() {
    return situation;
  }

  public void setSituation(final Situation situation) {
    this.situation = situation;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(final String filename) {
    this.filename = filename;
  }

  @Override
  public String toString() {
    return "ConnectCalculationFile [uuid=" + uuid + ", situation=" + situation + ", researchArea=" + researchArea + "]";
  }
}
