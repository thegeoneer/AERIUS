/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.HasName;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Emission data for a mobile sources. The class can contain multiple vehicles.
 * which are all one one single location.
 */
public class OffRoadMobileEmissionSource extends EmissionSourceCollection<OffRoadVehicleEmissionSubSource> {

  private static final long serialVersionUID = 2191758298137867828L;

  private static final int KG = 1000;

  /**
   * Emission data for mobile source. Emission is calculated using the amount of
   * fuel used on a yearly basis. Specific for a category a emission factor is
   * specified and the emission is the amount fuel times the emission factor.
   */
  public static class OffRoadVehicleEmissionSubSource extends EmissionSubSource implements HasName, Serializable {
    public static enum SourceCategoryType {
      STAGE_CLASS, DEVIATING_CLASS;
    }

    private static final long serialVersionUID = -2809329933077990813L;

    private static final EmissionValueKey KEY_NOX = new EmissionValueKey(Substance.NOX);

    private String name;
    private SourceCategoryType categoryType;

    private OffRoadMobileSourceCategory stageCategory;
    private int fuelage;

    private OPSSourceCharacteristics sourceCharacteristics = new OPSSourceCharacteristics();
    private EmissionValues emissionValues = new EmissionValues();
    private OffRoadVehicleSpecification vehicleSpecification;

    @Override
    public OffRoadVehicleEmissionSubSource copy() {
      return copyTo(new OffRoadVehicleEmissionSubSource());
    }

    protected <U extends OffRoadVehicleEmissionSubSource> U copyTo(final U copy) {
      super.copyTo(copy);
      copy.setName(name);
      copy.setCategoryType(categoryType);
      copy.setStageCategory(stageCategory);
      copy.setFuelage(fuelage);
      copy.setCharacteristics(sourceCharacteristics.copy());
      copy.setEmissionNOX(getEmissionNOX());
      copy.setVehicleSpecification(vehicleSpecification == null ? null : vehicleSpecification.copy());
      return copy;
    }

    @Override
    public int getStateHash() {
      final int prime = 31;
      int result = 1;
      result = prime * result + getId();
      result = prime * result + categoryType.name().hashCode();
      result = prime * result + (emissionValues == null ? 0 : emissionValues.getStateHash());
      result = prime * result + fuelage;
      result = prime * result + (name == null ? 0 : name.hashCode());
      result = prime * result + (sourceCharacteristics == null ? 0 : sourceCharacteristics.getStateHash());
      result = prime * result + (stageCategory == null ? 0 : stageCategory.getStateHash());
      return result;
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public void setName(final String name) {
      this.name = name;
    }

    public OffRoadMobileSourceCategory getStageCategory() {
      return stageCategory;
    }

    public void setStageCategory(final OffRoadMobileSourceCategory stageCategory) {
      this.stageCategory = stageCategory;
    }

    public int getFuelage() {
      return fuelage;
    }

    public void setFuelage(final int fuelage) {
      this.fuelage = fuelage;
    }

    public double getEmissionHeight() {
      return sourceCharacteristics.getEmissionHeight();
    }

    public void setEmissionHeight(final double emissionHeight) {
      sourceCharacteristics.setEmissionHeight(emissionHeight);
    }

    public double getSpread() {
      return sourceCharacteristics.getSpread();
    }

    public void setSpread(final double spread) {
      sourceCharacteristics.setSpread(spread);
    }

    public double getHeatContent() {
      return sourceCharacteristics.getHeatContent();
    }

    public void setHeatContent(final double heatContent) {
      sourceCharacteristics.setHeatContent(heatContent);
    }

    public OPSSourceCharacteristics getCharacteristics() {
      return sourceCharacteristics.copy();
    }

    public void setCharacteristics(final OPSSourceCharacteristics characteristics) {
      if (characteristics != null) {
        sourceCharacteristics.setEmissionHeight(characteristics.getEmissionHeight());
        sourceCharacteristics.setSpread(characteristics.getSpread());
        sourceCharacteristics.setHeatContent(characteristics.getHeatContent());
        sourceCharacteristics.setHeatContentSpecification(characteristics.getHeatContentSpecification());
      }
    }

    public double getEmissionNOX() {
      return emissionValues.getEmission(KEY_NOX);
    }

    public void setEmissionNOX(final double emissionNOX) {
      emissionValues.setEmission(KEY_NOX, emissionNOX);
    }

    public EmissionValues getEmissionValues() {
      return emissionValues;
    }

    public OffRoadVehicleSpecification getVehicleSpecification() {
      return vehicleSpecification;
    }

    public void setVehicleSpecification(final OffRoadVehicleSpecification vehicleSpecification) {
      this.vehicleSpecification = vehicleSpecification;
    }

    @Override
    double getEmission(final EmissionValueKey key) {
      double emission;
      if (categoryType == SourceCategoryType.DEVIATING_CLASS) {
        emission = emissionValues.getEmission(key);
      } else {
        emission = (stageCategory == null ? 0 : stageCategory.getEmissionFactor(key) * fuelage) / KG;
      }
      return emission;
    }

    @Override
    public String toString() {
      return "VehicleEmissionValues [id=" + getId() + ", name=" + name + ", categoryType=" + categoryType.name()
          + ", category=" + stageCategory + ", literPerYear=" + fuelage + ", emissionNOX=" + getEmissionNOX()
          + ", sourceCharacteristics=" + sourceCharacteristics.toString()
          + ", emissionValues=" + emissionValues.toString() + "]";
    }

    public SourceCategoryType getCategoryType() {
      return categoryType;
    }

    public void setCategoryType(final SourceCategoryType categoryType) {
      this.categoryType = categoryType;
    }

    public boolean isCustomCategory() {
      return categoryType == SourceCategoryType.DEVIATING_CLASS;
    }
  }

  @Override
  public <T> T accept(final EmissionSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public OffRoadMobileEmissionSource copy() {
    return copyTo(new OffRoadMobileEmissionSource());
  }

  @Override
  public int getStateHash() {
    int state = super.getStateHash();
    state += getEmissionSubSources().getStateHash();
    return state;
  }
}
