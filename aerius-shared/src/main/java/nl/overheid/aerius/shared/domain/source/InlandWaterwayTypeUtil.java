/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.util.List;

import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Util class for {@link InlandWaterwayType} related operations.
 */
public final class InlandWaterwayTypeUtil {

  private InlandWaterwayTypeUtil() {
    // util class.
  }

  public static boolean isDirectionCorrect(final InlandWaterwayType iwt) {
    return isDirectionCorrect(iwt.getWaterwayCategory(), iwt.getWaterwayDirection());
  }

  /**
   * Returns true if the direction is correct for the given water category. If waterway category not set or irrelevant direction nothing can be said
   * about direction so it returns true. If direction of set water category is relevant the direction must be set and not IRRELEVANT.
   *
   * @return true if the direction is correct for the given water category
   */
  public static boolean isDirectionCorrect(final InlandWaterwayCategory category, final WaterwayDirection direction) {
    final boolean categoryIrrelevant = category == null || !category.isDirectionRelevant();
    return categoryIrrelevant || (direction != null && direction != WaterwayDirection.IRRELEVANT);
  }

  /**
   *
   * @param categories
   * @param waterwayType
   * @param shipCategory
   * @param sourceId
   * @throws AeriusException
   */
  public static void validateWaterwayForShip(final SectorCategories categories, final InlandWaterwayType waterwayType,
      final InlandShippingCategory shipCategory, final String sourceId) throws AeriusException {
    if (waterwayType != null && !categories.getInlandShippingCategories().isValidCombination(shipCategory, waterwayType.getWaterwayCategory())) {
      throw new AeriusException(Reason.GML_INVALID_SHIP_FOR_WATERWAY, sourceId, shipCategory.getCode(), waterwayType.getWaterwayCategory().getCode());
    }
  }

  /**
   * If the list contains more then 1 result it means multiple water way types were found. In that case an {@link AeriusException} with
   * {@link Reason#INLAND_SHIPPING_WATERWAY_INCONCLUSIVE} is returned.
   * @param sourceId the id of the emission source
   * @param result list of water way types.
   * @return {@link AeriusException} in case multiple water ways were in the list otherwise returns null.
   */
  public static AeriusException detectInconclusiveRoute(final String sourceId, final List<InlandWaterwayType> result) {
    final int size = result.size();
    if (size > 1) {
      final StringBuilder names = new StringBuilder();
      for (int i = 1; i < size; i++) {
        if (i > 1) {
          names.append(", ");
        }
        names.append(result.get(i).getWaterwayCategory().getCode());
      }
      return new AeriusException(
          Reason.INLAND_SHIPPING_WATERWAY_INCONCLUSIVE, sourceId, result.get(0).getWaterwayCategory().getCode(), names.toString());
    }
    return null;
  }
}
