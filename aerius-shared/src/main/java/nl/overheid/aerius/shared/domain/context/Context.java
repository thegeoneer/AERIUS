/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

import java.io.Serializable;
import java.util.HashMap;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.WMSLayerType;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;

/**
 *
 */
public class Context implements Serializable {
  private static final long serialVersionUID = -1113424588583464218L;

  // Generic settings
  private HashMap<SharedConstantsEnum, Object> settings = new HashMap<SharedConstantsEnum, Object>();
  // Specific layers
  private HashMap<WMSLayerType, LayerProps> layers = new HashMap<WMSLayerType, LayerProps>();

  private ReceptorGridSettings receptorGridSettings;

  public <T extends Object> T getSetting(final SharedConstantsEnum key) {
    return getSetting(key, false);
  }

  @SuppressWarnings("unchecked")
  public <T extends Object> T getSetting(final SharedConstantsEnum key, final boolean soft) {
    checkMap(settings, key, "setting", soft);

    return (T) settings.get(key);
  }

  public void setSetting(final SharedConstantsEnum key, final Object obj) {
    settings.put(key, obj);
  }

  @SuppressWarnings("unchecked")
  public <T extends LayerProps> T getLayer(final WMSLayerType layerType) {
    checkMap(layers, layerType, "layer");

    return (T) layers.get(layerType);
  }

  public void setLayer(final WMSLayerType layerType, final LayerProps layerProps) {
    layers.put(layerType, layerProps);
  }

  public ReceptorGridSettings getReceptorGridSettings() {
    return receptorGridSettings;
  }

  public void setReceptorGridSettings(final ReceptorGridSettings receptorGridSettings) {
    this.receptorGridSettings = receptorGridSettings;
  }

  private <K extends Object, V extends Object> void checkMap(final HashMap<K, V> map, final K key, final String description) {
    checkMap(map, key, description, false);
  }

  private <K extends Object, V extends Object> void checkMap(final HashMap<K, V> map, final K key, final String description, final boolean soft) {
    if (!map.containsKey(key) && !soft) {
      throw new IllegalStateException("Required " + description + " '" + key + "' not present.");
    }
  }

  @Override
  public String toString() {
    return "Context [settings=" + settings + ", layers=" + layers + "]";
  }
}
