/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.info.CalculationInfo;
import nl.overheid.aerius.shared.domain.info.ReceptorInfo;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

@RemoteServiceRelativePath(ServiceURLConstants.INFO_SERVICE_PATH)
public interface InfoService extends RemoteService {

  /**
   * @param point The point to get the information for.
   * @param calculationIdOne The ID of the first calculation to get the deposition values for (can be 0)
   * @param calculationIdTwo The ID of the second calculation to get the deposition values for (can be 0)
   * @param year The year to get the information for.
   * @return The information for the receptor.
   * Any lists should be initialized.
   * @throws AeriusException When an exception occurs gathering the information.
   */
  ReceptorInfo getAreaInfo(AeriusPoint point, int calculationIdOne, int calculationIdTwo, int year) throws AeriusException;

  /**
   * @param importedImaerFileId The importd IMAER file id.
   * @param point The point to get the features for.
   * @return The features for the given point.
   * @throws AeriusException When an exception occurs gathering the information.
   */
  ArrayList<EmissionSource> getImportedImaerFeatures(int importedImaerFileId, AeriusPoint point) throws AeriusException;

  /**
   * Get the calculation info for the given calculation and emission value key.
   *
   * @param calculationId
   * @param substances
   * @return
   * @throws AeriusException
   */
  CalculationInfo getCalculationInfo(int calculationId) throws AeriusException;

  /**
   * Get the melding information including GML.
   *
   * @return MeldingInfo
   * @throws AeriusException
   */
  ScenarioGMLs getMeldingGML(CalculatedScenario scenario) throws AeriusException;

  /**
   * Get the receptor ID's + surface of habitat of corresponding hexagon for all receptors in the given habitat,
   * in the given assessment area that have been calculated.
   *
   * @param calculationId
   * @param assessmentAreaId
   * @param habitatId
   * @return
   * @throws AeriusException
   */
  HashMap<Integer, Double> getCalculationAssessmentHabitatReceptors(int calculationId, int assessmentAreaId, int habitatId) throws AeriusException;

}
