/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.HashMap;

import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Keeps track of the maximum result values by distance. Results can be tracked for multiple calculations.
 */
public class ResultHighValuesByDistances implements Serializable {

  private static final long serialVersionUID = -5667529730684558362L;

  // This is a Map<CalculationId, highestDistance>
  private HashMap<Integer, Integer> highestDistance = new HashMap<>();

  // This is a Map<CalculationId>, Map<Distance, EmissionResults>>
  private HashMap<Integer, HashMap<Integer, EmissionResults>> results = new HashMap<>();

  private int numberOfResults;

  public void addNumberOfResults(final int numberOfResults) {
    this.numberOfResults += numberOfResults;
  }

  public int getNumberOfResults() {
    return numberOfResults;
  }

  public HashMap<Integer, EmissionResults> getCalculationIdMap(final int calculationId) {
    if (!results.containsKey(calculationId)) {
      results.put(calculationId, new HashMap<Integer, EmissionResults>());
    }
    return results.get(calculationId);
  }

  public HashMap<Integer, HashMap<Integer, EmissionResults>> getResults() {
    return results;
  }

  public int getHighestDistance(final int calculationId) {
    return highestDistance.get(calculationId);
  }

  public void updateHighestValue(final int calculationId, final Integer distance) {
    final Integer current = highestDistance.get(calculationId);
    highestDistance.put(calculationId, Math.max(current == null ? 0 : current, distance));
  }
}
