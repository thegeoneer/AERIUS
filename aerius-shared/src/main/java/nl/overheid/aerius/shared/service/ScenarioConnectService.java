/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service containing Scenario calculation related calls.
 */
@RemoteServiceRelativePath(ServiceURLConstants.SCENARIO_CONNECT_SERVICE_PATH)
public interface ScenarioConnectService extends RemoteService {

  /**
   * Submit the Scenario Calculation to Connect.
   *
   * @param connectCalculationInformation The information to submit along with the files.
   * @throws AeriusException In case of errors.
   */
  String submitCalculation(ConnectCalculationInformation connectCalculationInformation) throws AeriusException;

  /**
   * Delete the file.
   *
   * @param uuid UUID of file to delete.
   * @throws AeriusException In case of errors.
   */
  void deleteUploadedFile(String uuid) throws AeriusException;

  /**
   * Fetch the calculations for the given jobkey
   *
   * @param jobKey Jobkey to fetch calculations for.
   *
   * @return A list of calculations associated with the jobkey.
   */
  ArrayList<Calculation> fetchJobCalculations(String jobKey) throws AeriusException;

  /**
   * Submit this util request to Connect API.
   *
   * @param connectUtilInformation The information to submit along with the files.
   * @throws AeriusException In case of errors.
   */
  String submitUtil(ConnectUtilInformation connectUtilInformation) throws AeriusException;

}
