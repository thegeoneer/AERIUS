/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.HasOPSSourceCharacteristics;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Generic EmissionsFactory supporting multiple substance types.
 * <p>The emission values can be stored as the total emission of as the emission
 * per unit. For example the unit can be per meter. It is up to the user of this
 * class to make sure correct total emission is calculated.
 * <p>The emission values can be
 */
public class GenericEmissionSource extends EmissionSource implements VisitableEmissionSource, HasOPSSourceCharacteristics {

  private static final long serialVersionUID = -2034603750459540497L;

  private EmissionValues emissionValues = new EmissionValues(false);

  public GenericEmissionSource() {
  }

  public GenericEmissionSource(final int i, final Point point) {
    super(i, point);
  }

  @Override
  public <T> T accept(final EmissionSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public GenericEmissionSource copy() {
    return copyTo(new GenericEmissionSource());
  }

  public <E extends GenericEmissionSource> E copyTo(final E copy) {
    super.copyTo(copy);
    // only copy emissions per unit and year dependent if same type, else these values should be as set in the copy itself.
    copy.setEmissionPerUnit(isEmissionPerUnit());
    copy.setYearDependent(isYearDependent());
    copy.setEmissionValues(emissionValues.copy());
    return copy;
  }

  @Override
  protected double getEmission(final EmissionValueKey key, final double weight) {
    return emissionValues.getEmission(key) * weight;
  }

  public EmissionValues getEmissionValues() {
    return emissionValues;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    return super.getStateHash() + prime * emissionValues.getStateHash();
  }

  @Override
  public boolean hasAnyEmissions() {
    return emissionValues != null && !emissionValues.entrySet().isEmpty();
  }

  /**
   * Sets the emission for the substance.
   *
   * @param substance Substance
   * @param emission emission
   */
  public void setEmission(final Substance substance, final double emission) {
    setEmission(new EmissionValueKey(substance), emission);
  }

  public void setEmission(final EmissionValueKey key, final double emission) {
    emissionValues.setEmission(key, emission);
  }

  public void setEmissionValues(final EmissionValues emissions) {
    this.emissionValues = emissions;
    setYearDependent(isYearDependent()); // set year dependent on emissionValues.
  }

  @Override
  public void setYearDependent(final boolean emissionPerYear) {
    super.setYearDependent(emissionPerYear);
    if (emissionValues != null) {
      emissionValues.setYearDependent(emissionPerYear);
    }
  }
}
