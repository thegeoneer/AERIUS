/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

import nl.overheid.aerius.shared.domain.auth.Permission;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AuthorizationException;

public class UserProfileUtil {

  /**
   * Returns whether the given user has any of the permissions.
   * @param userProfile User to check
   * @param permissions One or more permissions to check for
   */
  public static boolean hasPermission(final UserProfile userProfile, final Permission... permissions) {
    boolean hasPermission = permissions.length == 0;
    for (final Permission permission : permissions) {
      hasPermission = hasPermission || userProfile.hasPermission(permission);
    }
    return hasPermission;
  }

  /**
   * Returns whether the given user has any of the permissions for an authority.
   * @param userProfile User to check.
   * @param authority The authority to check for.
   * @param permissions The permissions to check for.
   * @return True if the user has permission, false if not.
   * In case of multiple permissions, the result will be true if the user has any of the permissions.
   */
  public static boolean hasPermissionForAuthority(final UserProfile userProfile, final Authority authority, final Permission... permissions) {
    return userProfile.getAuthority().equals(authority) && hasPermission(userProfile, permissions);
  }

  /**
   * Throws an AuthorizationException when the given user has none of the permissions.
   * @param userProfile User to check
   * @param permissions One or more permissions to check for
   * @throws AuthorizationException in case the user does not have any of the permissions supplied
   */
  public static void checkPermission(final UserProfile userProfile, final Permission... permissions) throws AuthorizationException {
    if (!hasPermission(userProfile, permissions)) {
      throw new AuthorizationException();
    }
  }

  /**
   * @param userProfile User to check.
   * @param authority The authority to check for.
   * @param permissions The permissions to check for.
   * @throws AuthorizationException If the user doesn't have any of the permissions supplied.
   */
  public static void checkPermissionAndAuthority(final UserProfile userProfile, final Authority authority, final Permission... permissions)
      throws AuthorizationException {
    if (!hasPermissionForAuthority(userProfile, authority, permissions)) {
      throw new AuthorizationException();
    }
  }
}
