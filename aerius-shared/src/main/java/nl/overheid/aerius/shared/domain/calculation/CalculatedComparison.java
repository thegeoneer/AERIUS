/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Calculation details for a comparison scenario. A comparison scenario always
 * consists of 2 {@link EmissionSourceList}'s with 2 calculation id's.
 */
public class CalculatedComparison extends CalculatedScenario implements Serializable {

  private static final long serialVersionUID = 8223521369451695989L;

  public CalculatedComparison() {
    getCalculations().add(new Calculation());
    getCalculations().add(new Calculation());
    getScenario().addSources(new EmissionSourceList());
    getScenario().addSources(new EmissionSourceList());
  }

  /**
   * Swaps the situations from position one to two and two to one.
   */
  public void swap() {
    getCalculations().add(getCalculations().remove(0));
    getScenario().addSources(getScenario().getSourceLists().remove(0));
  }

  @Override
  public int getCalculationId(final int eslId) {
    int calculationId = 0;
    if (getSourcesOne() != null && getSourcesOne().getId() == eslId) {
      calculationId = getCalculationIdOne();
    } else if (getSourcesTwo() != null && getSourcesTwo().getId() == eslId) {
      calculationId = getCalculationIdTwo();
    }
    return calculationId;
  }

  @Override
  public boolean containsCalculationId(final int calculationId) {
    return calculationId != 0 && (getCalculationIdOne() == calculationId || getCalculationIdTwo() == calculationId);
  }

  public int getCalculationIdOne() {
    return getCalculationOne().getCalculationId();
  }

  public int getCalculationIdTwo() {
    return getCalculationTwo().getCalculationId();
  }

  public Calculation getCalculationOne() {
    return getCalculations().get(0);
  }

  public Calculation getCalculationTwo() {
    return getCalculations().get(1);
  }

  @Override
  public Calculation getCalculation(final int calculationId) {
    return getCalculationIdOne() == calculationId ? getCalculationOne()
        : (getCalculationIdTwo() == calculationId ? getCalculationTwo() : null);
  }

  @Override
  public EmissionSourceList getSources(final int calculationId) {
    return getCalculationIdOne() == calculationId ? getSourcesOne()
        : (getCalculationIdTwo() == calculationId ? getSourcesTwo() : null);
  }

  public EmissionSourceList getSourcesOne() {
    return getScenario().getSourceLists().get(0);
  }

  public EmissionSourceList getSourcesTwo() {
    return getScenario().getSourceLists().get(1);
  }

  /**
   * Set the calculationId for situation One.
   * @param calculationId id to set.
   */
  public void setCalculationIdOne(final int calculationId) {
    getCalculationOne().setCalculationId(calculationId);
  }

  /**
   * Set the calculationId for situation Two.
   * @param calculationId id to set.
   */
  public void setCalculationIdTwo(final int calculationId) {
    getCalculationTwo().setCalculationId(calculationId);
  }

  /**
   * Set the {@link EmissionSourceList} for situation One.
   * @param listOne {@link EmissionSourceList} to set.
   */
  public void setSourcesOne(final EmissionSourceList listOne) {
    final EmissionSourceList actual = listOne == null ? new EmissionSourceList() : listOne;
    getScenario().getSourceLists().set(0, actual);
    getCalculationOne().setSources(actual);
  }

  /**
   * Set the {@link EmissionSourceList} for situation Two.
   * @param listTwo {@link EmissionSourceList} to set.
   */
  public void setSourcesTwo(final EmissionSourceList listTwo) {
    final EmissionSourceList actual = listTwo == null ? new EmissionSourceList() : listTwo;
    getScenario().getSourceLists().set(1, actual);
    getCalculationTwo().setSources(actual);
  }

  @Override
  public ArrayList<AeriusResultPoint> getCalculationPointResultsList(final int calculatorId) {
    return getCalculationIdOne() == calculatorId ? getCalculationPointsOne()
        : (getCalculationIdTwo() == calculatorId ? getCalculationPointsTwo() : null);
  }

  public ArrayList<AeriusResultPoint> getCalculationPointsOne() {
    return getCalculationOne().getCalculationPoints();
  }

  public ArrayList<AeriusResultPoint> getCalculationPointsTwo() {
    return getCalculationTwo().getCalculationPoints();
  }

  @Override
  public boolean hasCalculations() {
    return getCalculationIdOne() != 0 && getCalculationIdTwo() != 0;
  }
}
