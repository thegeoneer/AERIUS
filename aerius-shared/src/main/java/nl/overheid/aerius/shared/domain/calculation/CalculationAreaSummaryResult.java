/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.deposition.ResultInfoType;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResult;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Calculation summary result of a single assessment areas. Contains area information, and if calculated the
 * {@link DevelopmentRuleResult} results. Subclasses should add the habitat specific data, which is calculation specific.
 */
public class CalculationAreaSummaryResult implements Serializable, HasId {
  private static final long serialVersionUID = -7041620381150149956L;

  /**
   * Results for a single habitat in a single assessment area. Results is data per {@link nl.overheid.aerius.shared.domain.EmissionValueKey} objects.
   */
  public static class HabitatSummaryResult implements Serializable  {
    private static final long serialVersionUID = 4177641413288420439L;

    private EmissionResults total = new EmissionResults();
    private EmissionResults max = new EmissionResults();
    private EmissionResults average = new EmissionResults();
    private EmissionResults percentageKDW = new EmissionResults();

    public EmissionResults getTotal() {
      return total;
    }

    public EmissionResults getMax() {
      return max;
    }

    public EmissionResults getAverage() {
      return average;
    }

    public EmissionResults getPercentageKDW() {
      return percentageKDW;
    }

    public EmissionResults getResultForType(final ResultInfoType infoType) {
      EmissionResults results;
      switch (infoType) {
      case TOTAL_DEPOSITION:
        results = getTotal();
        break;
      case MAXIMUM_DEPOSITION:
      case MAXIMUM_INCREASE:
        results = getMax();
        break;
      case AVERAGE_DEPOSITION:
        results = getAverage();
        break;
      case PERCENTAGE_KDW:
        results = getPercentageKDW();
        break;
      default:
        results = null;
        break;
      }
      return results;
    }
  }

  /**
   * Habitat results per calculation.
   */
  public static class HabitatSummaryResults extends HabitatType implements Serializable, HasId {
    private static final long serialVersionUID = 4971607683488711621L;

    private HashMap<Integer, HabitatSummaryResult> situations = new HashMap<>();
    private HashMap<String, HabitatSummaryResult> comparisons = new HashMap<>();

    public HabitatSummaryResult getHabitatSummaryResults(final Integer calculationId) {
      if (!situations.containsKey(calculationId)) {
        situations.put(calculationId, new HabitatSummaryResult());
      }
      return situations.get(calculationId);
    }

    /**
     * Returns the comparison summary for Situation 2 - 1.
     * @param calculationOneId situation 1
     * @param calculationTwoId situation 2
     * @return comparison of situation 2 - 1
     */
    public HabitatSummaryResult getComparisonSummary(final int calculationOneId, final int calculationTwoId) {
      final String comparisonKey = comparisonKey(calculationOneId, calculationTwoId);
      if (!comparisons.containsKey(comparisonKey)) {
        comparisons.put(comparisonKey, new HabitatSummaryResult());
      }
      return comparisons.get(comparisonKey);
    }

    private String comparisonKey(final int calculationOneId, final int calculationTwoId) {
      return calculationOneId + "C" + calculationTwoId;
    }

    /**
     * Remove the results for the given calculationId. Doesn't remove comparisons results.
     * @param calculationId calculation to remove
     */
    public void remove(final int calculationId) {
      situations.remove(calculationId);
    }
  }

  /**
   * Habitat results per calculation habitat.
   */
  private HashMap<Integer, HabitatSummaryResults> habitats = new HashMap<>();
  //TODO shouldn't development results not depend which sits are compared: sit 2-1 != sit 1-2
  private ArrayList<DevelopmentRuleResult> developmentRuleResults = new ArrayList<>();
  private AssessmentArea area;

  @Override
  public int getId() {
    return area == null ? 0 : area.getId();
  }

  @Override
  public void setId(final int id) {
    // no-op
  }

  public AssessmentArea getArea() {
    return area;
  }

  public void setArea(final AssessmentArea area) {
    this.area = area;
  }

  public ArrayList<DevelopmentRuleResult> getDevelopmentRuleResults() {
    return developmentRuleResults;
  }

  public HashMap<Integer, HabitatSummaryResults> getHabitats() {
    return habitats;
  }

  public Comparator<CalculationAreaSummaryResult> getComparitor(final ResultInfoType resultType, final EmissionResultKey erk) {
    return new Comparator<CalculationAreaSummaryResult>() {

      @Override
      public int compare(final CalculationAreaSummaryResult casr1, final CalculationAreaSummaryResult casr2) {
        return Double.compare(getMax(casr2), getMax(casr1));
      }

      private double getMax(final CalculationAreaSummaryResult casr) {
        double max = Double.NEGATIVE_INFINITY;
        for (final Entry<Integer, HabitatSummaryResults> entry : casr.getHabitats().entrySet()) {
          max = Math.max(max, entry.getValue().getHabitatSummaryResults(casr.getId()).getResultForType(resultType).get(erk));
        }
        return max;
      }
    };
  }
}
