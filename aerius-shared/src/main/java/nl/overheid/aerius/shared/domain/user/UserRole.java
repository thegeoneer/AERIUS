/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.user;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasName;

public class UserRole implements Serializable, HasId, HasName {
  private static final long serialVersionUID = -7804707989793233027L;

  private int id;
  private String name;
  private String color;

  public UserRole() {}

  public UserRole(final int id, final String name, final String color) {
    this.id = id;
    this.name = name;
    this.color = color;
  }

  /**
   * @return the id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @return the name
   */
  @Override
  public String getName() {
    return name;
  }

  /**
   * @return the color
   */
  public String getColor() {
    return color;
  }

  /**
   * @param id the id to set
   */
  @Override
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @param name the name to set
   */
  @Override
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @param color the color to set
   */
  public void setColor(final String color) {
    this.color = color;
  }

  public UserRole copy() {
    return copyTo(new UserRole());
  }

  public <U extends UserRole> U copyTo(final U copy) {
    copy.setColor(color);
    copy.setId(id);
    copy.setName(name);

    return copy;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    boolean equal = false;
    if (obj instanceof UserRole) {
      final UserRole other = (UserRole) obj;
      equal = other.canEqual(this) && id == other.id;
    }
    return equal;
  }

  public boolean canEqual(final Object obj) {
    return obj instanceof UserRole;
  }

  @Override
  public String toString() {
    return "UserRole [id=" + id + ", name=" + name + ", color=" + color + "]";
  }

}
