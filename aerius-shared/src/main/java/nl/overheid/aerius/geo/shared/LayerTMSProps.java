/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo.shared;

import java.io.Serializable;

/**
 * Layer properties for TMS layers.
 *
 * TODO use parsing of service instead of values here...
 */
public class LayerTMSProps extends LayerProps implements Serializable {

  private static final long serialVersionUID = 2323348018205666646L;

  private String baseUrl;

  /**
   * The format extension corresponding to the requested tile image type.
   */
  private String type = "png8";

  /**
   * Service version for tile requests.
   */
  private String serviceVersion = "1.0.0";

  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(final String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public String getType() {
    return type;
  }

  public void setType(final String type) {
    this.type = type;
  }

  public String getServiceVersion() {
    return serviceVersion;
  }

  public void setServiceVersion(final String serviceVersion) {
    this.serviceVersion = serviceVersion;
  }

}
