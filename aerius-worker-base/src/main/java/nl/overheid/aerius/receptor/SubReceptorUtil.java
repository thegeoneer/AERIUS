/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.receptor;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;

/**
 *
 */
public abstract class SubReceptorUtil<T extends AeriusPoint> {

  private final HexagonZoomLevel zoomLevel1;

  protected SubReceptorUtil(final HexagonZoomLevel zoomLevel1) {
    this.zoomLevel1 = zoomLevel1;
  }

  /**
   * @param original The point to base the list on.
   * @return The subpoints, based on this wonderful piece of ASCII-art.
   *
   * <pre>
   *     .   .   .
   *
   *   .   .   .   .
   *
   * .   .   o   .   .
   *
   *   .   .   .   .
   *
   *     .   .   .
   *  </pre>
   */
  public ArrayList<T> determineSubPoints(final T original, final int rings) {
    final ArrayList<T> subPoints = new ArrayList<T>();
    int id = original.getId();
    subPoints.add(original);
    final double stepX = getStepX(rings);
    final double stepY = getStepY(rings);
    for (int i = -rings; i <= rings; i++) {
      final double yCoord = original.getY() + i * stepY;
      for (int j = -rings + Math.abs(i / 2); j <= rings - Math.abs(i / 2); j++) {
        double xCoord = original.getX() + j * stepX;
        if (i % 2 != 0 && j == 0 || i == 0 && j == 0) {
          //original point is added at the start (to preserve ID), so don't add again.
          //At odd levels we don't want to add the 'middle' point.
          continue;
        } else if (i % 2 != 0) {
          //Odd levels start halfway the step.
          xCoord += -stepX / 2 * Integer.signum(j);
        }
        final T displaced = newPoint(original);
        //change the ID, because ID is used in the equal method.
        displaced.setId(++id);
        displaced.setX(xCoord);
        displaced.setY(yCoord);
        subPoints.add(displaced);
      }
    }
    return subPoints;
  }

  private double getStepX(final int rings) {
    return zoomLevel1.getHexagonRadius() / rings;
  }

  private double getStepY(final int rings) {
    return zoomLevel1.getHexagonHeight() / (rings * 2);
  }

  /**
   * @param original The point to base the new point on.
   * @return The new point to use, should be a copy, not the original.
   */
  public abstract T newPoint(T original);

}
