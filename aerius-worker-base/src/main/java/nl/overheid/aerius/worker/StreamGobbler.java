/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Utility class to correctly use output stream of a invoked process. Can be
 * needed for process to end gracefully
 *
 * @see http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html
 */
public class StreamGobbler extends Thread {
  private static final Logger LOG = Logger.getLogger(StreamGobbler.class);

  /**
   * Some calculation methods give some useless error messages, because it tries to execute a clear command that isn't present,
   *  tries to use a UI object while running via the console (wine) etc. It results in clobbering the error stream.
   * Here are the complete strings of those error messages so they can be filtered out.
   */
  private static final List<String> IGNORE_ERROR_LINES = Arrays.asList(new String[] {
      "clear wordt niet herkend als een interne", // Dutch windows version of the error message by OPS - part I
      "of externe opdracht, programma of batchbestand.", // Dutch windows version of the error message by OPS - part II
      "'clear' is not recognized as an internal or external command,", // Windows english version version of the error message by OPS - part I
      "operable program or batch file.", // Windows english version of the error message by OPS - part II
      "'unknown': unknown terminal type.", // Linux version of the error message by OPS.
      "'unknown': I need something more specific.", // Linux version of the error message by OPS.
      "Make sure that your X server is running and that $DISPLAY is set correctly.", // Wine default error message on UI object creation
      "Application tried to create a window, but no driver could be loaded.", // Wine error message on UI window creation
      "err:systray:initialize_systray Could not create tray window", // Wine error message on systray usage
      "TERM environment variable not set.", // Linux version TERM environment warnings
  });
  private final String type;
  private final Level level;
  private final String parentId;
  private InputStream is;

  /**
   * Constructor of the stream gobbler.
   *
   * @param type type of stream, ERROR, or OUTPUT
   * @param level error level to report on
   * @param parentId id of the process using the StreamGobbler, used for reference
   */
  public StreamGobbler(final String type, final Level level, final String parentId) {
    this.type = type;
    this.level = level;
    this.parentId = parentId;
  }

  protected InputStream getInputStream() {
    return is;
  }

  /**
   * Set the input steam.
   * @param is inputSteam of data
   */
  public void setInputStream(final InputStream is) {
    this.is = is;
  }

  /**
   * Start the thread dealing with the stream output.
   */
  @Override
  public void run() {
    try (final BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
      String line = "";
      while ((line = br.readLine()) != null) { // eof
        if (!line.isEmpty() && containsError(line)) {
          logLine(line);
        }
      }
    } catch (final IOException ioe) {
      LOG.error("Error gobbling stream", ioe);
    }
  }

  private void logLine(final String line) {
    LOG.log(level, parentId + ": " + type + ">" + line);
  }

  /**
   * Returns true if the line contains an error message
   * @param line line to check
   * @return true if line contains error.
   */
  protected boolean containsError(final String line) {
    return !IGNORE_ERROR_LINES.contains(line);
  }
}
