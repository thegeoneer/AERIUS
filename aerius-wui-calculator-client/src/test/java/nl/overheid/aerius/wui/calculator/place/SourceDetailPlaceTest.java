/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.place;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gwtmockito.GwtMockitoTestRunner;

/**
 * Test class for {@link SourceDetailPlace}.
 */
@RunWith(GwtMockitoTestRunner.class)
public class SourceDetailPlaceTest {
  @Test
  public void testTokenizer() {
    final SourceDetailPlace.Tokenizer t = new SourceDetailPlace.Tokenizer();
    //This test is not totally save because the individual items are put in a hash an therefore the order is theoretically random.
    final String token = "sourceId=1&sid2=1&sid1=0&theme=n&situation=1";
    assertEquals("Check if SourceDetailPlace token conversion remains the same", t.getPlace(token), t.getPlace(t.getToken(t.getPlace(token))));
  }
}
