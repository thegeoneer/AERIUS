/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.scenario.base.ui.overview.CalculationPointsPanel;

/**
 * Implementation for {@link CalculationPointsView}.
 */
@Singleton
public class CalculationPointsViewImpl extends Composite implements CalculationPointsView {
  private static CalculationPointsViewImplUiBinder uiBinder = GWT.create(CalculationPointsViewImplUiBinder.class);

  interface CalculationPointsViewImplUiBinder extends UiBinder<Widget, CalculationPointsViewImpl> {
  }

  @UiField(provided = true) CalculationPointsPanel calculationPointOverview;
  @UiField SimplePanel buttonPanel;
  private Presenter presenter;

  @Inject
  public CalculationPointsViewImpl(final CalculationPointsPanel calculationPointOverview) {
    this.calculationPointOverview = calculationPointOverview;
    initWidget(uiBinder.createAndBindUi(this));
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setCalculationButtons(final IsWidget calculationButtons) {
    buttonPanel.setWidget(calculationButtons);
  }

  @Override
  public void setValue(final List<AeriusPoint> calculationPoints) {
    calculationPointOverview.setValue(calculationPoints, null);
  }

  @Override
  public String getTitleText() {
    return M.messages().calculationListCalculationPointNameHeader();
  }
}
