/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.place;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

public class MeldingPlace extends ScenarioBasePlace {
  private static final String PREFIX = "mel";

  /**
   * Tokenizer for {@link MeldingPlace}.
   */
  @Prefix(PREFIX)
  public static class Tokenizer extends ScenarioBasePlace.Tokenizer<MeldingPlace> implements PlaceTokenizer<MeldingPlace> {

    @Override
    protected MeldingPlace createPlace() {
      return new MeldingPlace();
    }
  }

  public MeldingPlace(final ScenarioBasePlace place) {
    super(place);
  }

  MeldingPlace() {
  }

  @Override
  public MeldingPlace copy() {
    return new MeldingPlace(this);
  }
}
