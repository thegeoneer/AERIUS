/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.context;

import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;

/**
 * Application Context object for the Calculator.
 */
@Singleton
public class CalculatorAppContext extends ScenarioBaseAppContext<CalculatorContext, CalculatorUserContext> {

  @Inject
  public CalculatorAppContext(final EventBus eventBus, final PlaceController placeController, final CalculatorContext context,
      final CalculatorUserContext userContext) {
    super(eventBus, placeController, context, userContext);
  }

  @Override
  public boolean enableHabitatFilter() {
    return true;
  }

}
