/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.place.CalculationPointPlace;
import nl.overheid.aerius.wui.calculator.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.main.event.CalculationPointChangeEvent;
import nl.overheid.aerius.wui.main.event.CalculationPointsPurgeEvent;
import nl.overheid.aerius.wui.main.event.SimpleGenericEvent.CHANGE;
import nl.overheid.aerius.wui.main.widget.event.AddEvent;
import nl.overheid.aerius.wui.main.widget.event.CopyEvent;
import nl.overheid.aerius.wui.main.widget.event.DeleteEvent;
import nl.overheid.aerius.wui.main.widget.event.EditEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.ui.CalculationControllerImpl;
import nl.overheid.aerius.wui.scenario.base.ui.SituationView;

/**
 * Presenter for calculation points overview.
 */
public class CalculationPointsPresenter extends AbstractActivity implements CalculationPointsView.Presenter {
  interface CalculationPointsPresenterEventBinder extends EventBinder<CalculationPointsPresenter> { }

  private final CalculationPointsPresenterEventBinder eventBinder = GWT.create(CalculationPointsPresenterEventBinder.class);

  private final PlaceController placeController;
  private final CalculatorUserContext userContext;
  private final CalculationPointsView view;
  private final CalculationPointsPlace place;
  private final CalculationControllerImpl calculatorController;
  private final CalculatorAppContext appContext;

  private EventBus eventBus;


  @Inject
  public CalculationPointsPresenter(final PlaceController placeController, final CalculationControllerImpl calculatorController,
      final CalculatorUserContext userContext, final CalculatorAppContext appContext, final SituationView parentView,
      final CalculationPointsView view, @Assisted final CalculationPointsPlace place) {
    this.placeController = placeController;
    this.calculatorController = calculatorController;
    this.userContext = userContext;
    this.appContext = appContext;
    this.view = view;
    this.place = place;
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.eventBus = eventBus;
    view.setPresenter(this);
    view.setCalculationButtons(calculatorController.getCalculationButtons());
    calculatorController.setButtonState(appContext.isCalculationRunning(), true);
    panel.setWidget(view);
    refresh();
    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onAdd(final AddEvent<?> event) {
    if (event.getValue() == AeriusPoint.class) {
      placeController.goTo(new CalculationPointPlace(null, place));
    } // else ignore
  }

  @EventHandler
  public void onCopy(final CopyEvent<?> event) {
    if (event.getValue() instanceof AeriusPoint) {
      copy((AeriusPoint) event.getValue());
    } // else ignore
  }

  /**
   * Although the copy function for CalculationPoints is utterly pointless, as the deposition will always
   * be the same for a point on the same location and only result in a calculation that takes twice as
   * long, it would seem this feature does not make sense. Think again, however, as in (very) limited
   * cases, the time wasted (re)writing a calculation point's label is spared!
   *
   * @param calculationPoint {@link AeriusPoint} to copy
   */
  private void copy(final AeriusPoint calculationPoint) {
    final AeriusPoint copy = calculationPoint.copy();
    userContext.getCalculationPoints().add(copy);
    eventBus.fireEvent(new CalculationPointChangeEvent(place.getActiveSituationId(), copy, CHANGE.ADD));
    placeController.goTo(new CalculationPointPlace(copy, place));
  }

  @EventHandler
  public void purgeAllCalculationPoints(final CalculationPointsPurgeEvent e) {
    userContext.getCalculationPoints().clear();
    refresh();
  }

  @EventHandler
  public void onDelete(final DeleteEvent<?> event) {
    if (event.getValue() instanceof AeriusPoint) {
      delete((AeriusPoint) event.getValue());
    }
    refresh();
  }

  private void delete(final AeriusPoint calculationPoint) {
    userContext.getCalculationPoints().remove(calculationPoint);

    eventBus.fireEvent(new CalculationPointChangeEvent(place.getActiveSituationId(), calculationPoint, CHANGE.REMOVE));
    refresh();
  }

  @EventHandler
  public void onEdit(final EditEvent<?> event) {
    if (event.getValue() instanceof AeriusPoint) {
      placeController.goTo(new CalculationPointPlace((AeriusPoint) event.getValue(), place));
    } // else ignore
  }

  @EventHandler
  void onCancelCalculation(final CalculationCancelEvent event) {
    calculatorController.setButtonState(false, true);
  }

  /**
   *
   */
  private void refresh() {
    view.setValue(userContext.getCalculationPoints());
  }
}
