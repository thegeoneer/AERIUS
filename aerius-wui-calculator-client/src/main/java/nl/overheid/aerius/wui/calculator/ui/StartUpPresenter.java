/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.place.SourceLocationPlace;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;
import nl.overheid.aerius.wui.scenario.base.place.StartUpPlace;

public class StartUpPresenter extends AbstractActivity implements StartUpView.Presenter {
  private final StartUpView view;
  private final PlaceController placeController;
  private final ScenarioBaseImportController importController;
  private final CalculatorAppContext context;
  private final StartUpPlace place;

  @Inject
  public StartUpPresenter(final StartUpView view, final ScenarioBaseImportController importController, final PlaceController placeController,
      final CalculatorAppContext context, @Assisted final StartUpPlace place) {
    this.view = view;
    this.placeController = placeController;
    this.importController = importController;
    this.context = context;
    this.place = place;
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    view.setPresenter(this);
    panel.setWidget(view);
  }

  @Override
  public void inputSource() {
    context.initEmissionSourceList(place.getActiveSituationId());
    placeController.goTo(new SourceLocationPlace());
  }

  @Override
  public void showImportDialog() {
    importController.showImportDialog();
  }
}
