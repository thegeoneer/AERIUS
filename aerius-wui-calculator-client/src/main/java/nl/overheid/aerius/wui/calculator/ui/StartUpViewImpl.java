/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

public class StartUpViewImpl extends Composite implements StartUpView {
  private static StartUpViewImplUiBinder uiBinder = GWT.create(StartUpViewImplUiBinder.class);

  interface StartUpViewImplUiBinder extends UiBinder<Widget, StartUpViewImpl> {}

  @UiField Button inputSource;
  @UiField Button importSources;

  @UiField HTMLPanel introSection;
  @UiField IFrameElement introMovie;

  private Presenter presenter;

  @Inject
  public StartUpViewImpl(final HelpPopupController hpC, final CalculatorContext c) {
    initWidget(uiBinder.createAndBindUi(this));

    hpC.addWidget(inputSource, hpC.tt().ttLocationButtonManual());
    hpC.addWidget(importSources, hpC.tt().ttLocationButtonImport());

    introSection.setVisible(false);

    importSources.ensureDebugId(TestID.BUTTON_IMPORT);
    inputSource.ensureDebugId(TestID.BUTTON_INPUT_SOURCES);
  }

  @UiHandler("inputSource")
  void inputSourceClickHandler(final ClickEvent e) {
    presenter.inputSource();
  }

  @UiHandler("importSources")
  void importSourcesClickHandler(final ClickEvent e) {
    presenter.showImportDialog();
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public String getTitleText() {
    return M.messages().startUpTitle();
  }
}
