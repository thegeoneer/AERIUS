/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui.melding;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.SubmitButton;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.test.TestID;

public class MeldingForwardPanel extends Composite implements HasCloseHandlers<Boolean> {
  private static final MeldingForwardPanelUiBinder UI_BINDER = GWT.create(MeldingForwardPanelUiBinder.class);

  interface MeldingForwardPanelUiBinder extends UiBinder<Widget, MeldingForwardPanel> {}

  private static final String TARGET = "_melding";

  @UiField FormPanel form;

  @UiField Hidden proposedGMLPayloadField;
  @UiField Hidden currentGMLPayloadField;

  @UiField SubmitButton submitButton;

  public MeldingForwardPanel(final String uri, final ScenarioGMLs result) {
    initWidget(UI_BINDER.createAndBindUi(this));

    form.setAction(uri);
    form.getElement().setAttribute("target", TARGET);

    proposedGMLPayloadField.setName(SharedConstants.MELDING_GML_PROPOSED);
    proposedGMLPayloadField.setValue(result.getProposedGML());
    currentGMLPayloadField.setName(SharedConstants.MELDING_GML_CURRENT);
    currentGMLPayloadField.setValue(result.getCurrentGML());

    submitButton.ensureDebugId(TestID.BUTTON_MELDING_SUBMIT);
  }

  @UiHandler("cancelButton")
  public void onCancelButtonClick(final ClickEvent click) {
    CloseEvent.fire(this, true);
  }

  public HandlerRegistration addSubmitHandler(final SubmitHandler submitHandler) {
    return form.addSubmitHandler(submitHandler);
  }

  @Override
  public HandlerRegistration addCloseHandler(final CloseHandler<Boolean> handler) {
    return addHandler(handler, CloseEvent.getType());
  }
}
