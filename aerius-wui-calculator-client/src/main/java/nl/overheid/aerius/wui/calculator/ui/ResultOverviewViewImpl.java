/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.request.PotentialRequestInfo;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;

/**
 * View impl for results overview page.
 */
public class ResultOverviewViewImpl extends Composite implements ResultOverviewView {

  private static ResultOverviewViewImplUiBinder uiBinder = GWT.create(ResultOverviewViewImplUiBinder.class);

  interface ResultOverviewViewImplUiBinder extends UiBinder<Widget, ResultOverviewViewImpl> {
  }

  @UiField SwitchPanel switchPanel;

  @UiField HTML overViewTextPermitRequired;
  @UiField HTML overViewTextPermitRequiredNoSpace;

  @UiField Button meldingButton;

  private Presenter presenter;

  public ResultOverviewViewImpl() {
    initWidget(uiBinder.createAndBindUi(this));

    switchPanel.showWidget(0);
  }

  @Override
  public void setCalculationSummary(final CalculationSummary calculationSummary) {
    if (calculationSummary == null) {
      return;
    }

    final PotentialRequestInfo potentialRequestInfo = calculationSummary.getPotentialRequestInfo();
    final int showId;

    switch (potentialRequestInfo.getPotentialRequestType()) {
    case MELDING:
      showId = 1;
      break;
    case PERMIT:
      showId = 2;
      final int num = potentialRequestInfo.getExceedingAssessmentAreaNum();
      overViewTextPermitRequired.setHTML(M.messages().calculationOverviewTextPermitRequired(num));
      break;
    case PERMIT_INSIDE_MELDING_SPACE:
      showId = 3;
      overViewTextPermitRequiredNoSpace.setHTML(M.messages().calculationOverviewTextPermitRequiredNoSpace());
      break;
    case NONE:
    default:
      showId = 0;
    }
    switchPanel.showWidget(showId);
  }

  @UiHandler("meldingButton")
  public void onMeldingButtonClick(final ClickEvent e) {
    presenter.postMelding();
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public String getTitleText() {
    return M.messages().calculatorMenuResults();
  }
}
