/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.place.SourceDetailPlace;
import nl.overheid.aerius.wui.calculator.place.SourceLocationPlace;
import nl.overheid.aerius.wui.main.event.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourcesPurgeEvent;
import nl.overheid.aerius.wui.main.event.SimpleGenericEvent.CHANGE;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.widget.event.AddEvent;
import nl.overheid.aerius.wui.main.widget.event.CopyEvent;
import nl.overheid.aerius.wui.main.widget.event.DeleteEvent;
import nl.overheid.aerius.wui.main.widget.event.EditEvent;
import nl.overheid.aerius.wui.scenario.base.place.NetworkDetailPlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;
import nl.overheid.aerius.wui.scenario.base.ui.CalculationController;
import nl.overheid.aerius.wui.scenario.base.ui.EmissionSourcesPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.EmissionSourcesView;
import nl.overheid.aerius.wui.scenario.base.ui.SituationView;

/**
 * Presenter for emission sources list.
 */
public class CalculatorEmissionSourcesPresenter extends EmissionSourcesPresenter {
  interface EmissionSourcesPresenterEventBinder extends EventBinder<CalculatorEmissionSourcesPresenter> {}

  private static final EmissionSourcesPresenterEventBinder EVENT_BINDER = GWT.create(EmissionSourcesPresenterEventBinder.class);

  private EventBus eventBus;

  @Inject
  public CalculatorEmissionSourcesPresenter(final PlaceController placeController, final CalculationController calculatorController,
      final SituationView parentView, final EmissionSourcesView view, @Assisted final SourcesPlace place, final CalculatorAppContext appContext) {
    super(placeController, calculatorController, parentView, view, place, appContext);
  }

  @Override
  public void startChild(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.eventBus = eventBus;
    EVENT_BINDER.bindEventHandlers(this, eventBus);

    super.startChild(panel, eventBus);
  }

  @EventHandler
  public void onAdd(final AddEvent<?> event) {
    if (event.getValue() == EmissionSource.class) {
      if (checkMaxSources()) {
        return;
      }
      appContext.clearNewEmissionSource();
      placeController.goTo(new SourceLocationPlace(0, place));
    } // else ignore
  }

  @EventHandler
  public void onCopy(final CopyEvent<?> event) {
    if (event.getValue() instanceof EmissionSource) {
      copy((EmissionSource) event.getValue());
    } // else ignore
  }

  private void copy(final EmissionSource emissionSource) {
    if (checkMaxSources()) {
      return;
    }
    createCopy();
    final EmissionSource copy = emissionSource.copy();
    appContext.getUserContext().getScenario().getSources(place.getActiveSituationId()).add(copy);
    eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), copy, CHANGE.ADD));
    placeController.goTo(new SourceDetailPlace(copy.getId(), place));
  }

  private void createCopy() {
    appContext.getUserContext().setSourceListCopy(appContext.getUserContext().getScenario().getSources(place.getActiveSituationId()).copy());

  }

  private boolean checkMaxSources() {
    final ScenarioBaseUserContext userContext = appContext.getUserContext();
    final EmissionSourceList lst = userContext.getScenario().getSources(place.getActiveSituationId());
    final int max = userContext.getCalculatorLimits().getMaxSources();

    if (lst.size() >= max) {
      NotificationUtil.broadcastMessage(eventBus, M.messages().notificationMaxSourcesReached(max));
      return true;
    }

    return false;
  }

  @EventHandler
  public void purgeAllEmissionSources(final EmissionSourcesPurgeEvent e) {
    appContext.setCalculationState(CalculationState.INITIALIZED);
    refresh();
  }

  @EventHandler
  public void onDelete(final DeleteEvent<?> event) {
    if (event.getValue() instanceof EmissionSource) {
      delete((EmissionSource) event.getValue());
    }
    refresh();
  }

  private void delete(final EmissionSource src) {
    appContext.getUserContext().getScenario().getSources(place.getActiveSituationId()).removeDestroy(src);
    eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), src, CHANGE.REMOVE));
  }

  @EventHandler
  public void onEdit(final EditEvent<?> event) {
    createCopy();
    if (event.getValue() instanceof SRM2NetworkEmissionSource) {
      placeController.goTo(new NetworkDetailPlace(((HasId) event.getValue()).getId(), place));
    } else if (event.getValue() != null) {
      placeController.goTo(new SourceDetailPlace(((HasId) event.getValue()).getId(), place));
    }
  }

}
