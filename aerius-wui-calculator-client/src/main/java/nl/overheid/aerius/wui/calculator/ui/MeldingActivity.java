/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.ui.melding.MeldingExportController;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationSummaryChangedEvent;
import nl.overheid.aerius.wui.scenario.base.export.ExportDialogController;

/**
 * Activity class for Melding page.
 */
public class MeldingActivity extends AbstractActivity implements MeldingView.Presenter {

  interface MeldingActivityEventBinder extends EventBinder<MeldingActivity> { }

  private static final MeldingActivityEventBinder EVENT_BINDER = GWT.create(MeldingActivityEventBinder.class);

  private final MeldingView view;

  private final CalculatorAppContext appContext;

  private final MeldingExportController meldingController;
  private final ExportDialogController edController;

  private final CalculatorUserContext userContext;

  private final PlaceController placeController;

  @Inject
  public MeldingActivity(final CalculatorAppContext appContext, final MeldingView view, final MeldingExportController meldingController,
      final ExportDialogController edController, final PlaceController placeController, final CalculatorUserContext userContext) {
    this.appContext = appContext;
    this.view = view;
    this.meldingController = meldingController;
    this.edController = edController;
    this.placeController = placeController;
    this.userContext = userContext;
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    panel.setWidget(view);

    view.setPresenter(this);
    EVENT_BINDER.bindEventHandlers(this, eventBus);
    setCalculationState();
  }

  @Override
  public void postMelding() {
    meldingController.showPostMeldingDialog();
  }

  @Override
  public void doExport() {
//    edController.show(userContext, placeController.getWhere(), userContext.getEmissionValueKey().getYear(), ExportType.PAA_DEVELOPMENT_SPACES);
  }

  @EventHandler
  void onCalculationStartEvent(final CalculationStartEvent event) {
    setCalculationState();
  }

  @EventHandler
  void onCalculationInitEvent(final CalculationInitEvent event) {
    setCalculationState();
  }

  @EventHandler
  void onCancelCalculation(final CalculationCancelEvent event) {
    setCalculationState();
  }

  @EventHandler
  void onCalculationSummaryChanged(final CalculationSummaryChangedEvent event) {
    setCalculationState();
  }

  private void setCalculationState() {
    view.setCalculationComplete(appContext.getCalculationState() == CalculationState.COMPLETED);
  }
}
