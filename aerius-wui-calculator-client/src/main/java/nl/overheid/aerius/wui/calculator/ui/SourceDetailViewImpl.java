/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.source.EmissionSourceBasicInfoEditor;
import nl.overheid.aerius.wui.scenario.base.source.EmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.SectorSelectorWidget;
import nl.overheid.aerius.wui.scenario.base.source.core.NotEditCancellable;

@Singleton
public class SourceDetailViewImpl extends Composite implements SourceDetailView {
  private static SourceDetailViewImplUiBinder uiBinder = GWT.create(SourceDetailViewImplUiBinder.class);

  interface SourceDetailViewImplUiBinder extends UiBinder<Widget, SourceDetailViewImpl> { }

  @Path("") @UiField(provided = true) EmissionSourceBasicInfoEditor basicInfoEditor;
  @UiField(provided = true) SectorSelectorWidget sectorSelectorWidget;
  @UiField(provided = true) final EmissionSourceEditor esEdit;
  @UiField Button buttonCancel;
  @UiField Button buttonNextStep;

  private Presenter presenter;
  private HandlerRegistration sectorHandleRegisration;

  @Inject
  public SourceDetailViewImpl(final ScenarioBaseMapLayoutPanel map, final CalculatorContext context, final CalculatorServiceAsync service,
      final UserContext userContext, final HelpPopupController hpC) {
    esEdit = new EmissionSourceEditor(map.getEventBus(), map, context, service, hpC);
    basicInfoEditor = new EmissionSourceBasicInfoEditor(esEdit.getLabelEditor(), hpC);
    basicInfoEditor.addEditHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        presenter.editLocation();
      }
    });
    sectorSelectorWidget = new SectorSelectorWidget(context, hpC);
    initWidget(uiBinder.createAndBindUi(this));

    hpC.addWidget(buttonNextStep, hpC.tt().ttSourceDetailButtonNextStep());
    hpC.addWidget(buttonCancel, hpC.tt().ttSourceDetailButtonNextStep());

    buttonNextStep.ensureDebugId(TestID.BUTTON_NEXTSTEP);
    buttonCancel.ensureDebugId(TestID.BUTTON_CANCEL);
  }

  @Override
  public Editor<EmissionSource> getBasicInfoEditor() {
    return basicInfoEditor;
  }

  @Override
  public EmissionSourceEditor getEmissionSourceEditor() {
    return esEdit;
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
    if (sectorHandleRegisration != null) {
      sectorHandleRegisration.removeHandler();
    }
    sectorHandleRegisration = sectorSelectorWidget.addValueChangeHandler(presenter);
  }

  @UiHandler("buttonCancel")
  void onClickButtonCancel(final ClickEvent e) {
    presenter.cancelEdit();
  }

  @UiHandler("buttonNextStep")
  void onClickButtonNextStep(final ClickEvent e) {
    presenter.nextStep();
  }

  /**
   * Adapts the UI based on the currently selected sector.
   * @param sector sector
   */
  @Override
  public void setSourceDetailVisibility(final Sector sector) {
    sectorSelectorWidget.setSector(sector);
    final boolean v = sector != null && sector != Sector.SECTOR_UNDEFINED;
    updateDetailsVisiblity(sector);
    buttonNextStep.setHTML(v ? M.messages().save() : M.messages().nextStepButton());

  }

  @Override
  public void setNextButtonVisible(final boolean visible) {
    buttonNextStep.setVisible(visible);
    buttonCancel.setStyleName(R.css().primaryButton(), !visible);
    buttonCancel.setStyleName(R.css().flexButton(), visible);
    setCancelButtonVisible(visible);
  }

  private void setCancelButtonVisible(final boolean visible) {
    buttonCancel.setVisible(visible);
  }

  private void updateCancelButton(final Sector sector) {
    setCancelButtonVisible(sector == null ? true : !(esEdit instanceof NotEditCancellable));
  }

  @Override
  public void updateDetailsVisiblity(final Sector sector) {
    final boolean v = sector != null && sector != Sector.SECTOR_UNDEFINED;

    esEdit.setVisible(v);
    setNextButtonVisible(v);
    buttonNextStep.setEnabled(v);
    buttonNextStep.setHTML(M.messages().nextStepButton());
    updateCancelButton(sector);
  }

  @Override
  public String getTitleText() {
    return M.messages().configureEmissionSource();
  }


}
