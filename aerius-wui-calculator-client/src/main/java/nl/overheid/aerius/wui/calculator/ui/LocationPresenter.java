/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.control.ModifyFeature.OnModificationListener;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.geometry.MultiLineString;
import org.gwtopenmaps.openlayers.client.geometry.Polygon;
import org.gwtopenmaps.openlayers.client.handler.PathHandler;
import org.gwtopenmaps.openlayers.client.handler.PointHandler;
import org.gwtopenmaps.openlayers.client.handler.PolygonHandler;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.geo.InteractionLayer;
import nl.overheid.aerius.geo.LineUtil;
import nl.overheid.aerius.geo.MeasureInteractionLayer;
import nl.overheid.aerius.geo.MeasureInteractionLayer.MeasureInteractionEvent;
import nl.overheid.aerius.geo.MeasureInteractionLayer.MeasureInteractionLayerListener;
import nl.overheid.aerius.geo.ModifyFeatureInteractionLayer;
import nl.overheid.aerius.geo.SelfIntersect;
import nl.overheid.aerius.geo.WktUtil;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.wui.calculator.place.SourceDetailPlace;
import nl.overheid.aerius.wui.calculator.place.SourceLocationPlace;
import nl.overheid.aerius.wui.calculator.ui.SourceLocationView.Presenter;
import nl.overheid.aerius.wui.calculator.ui.SourceLocationView.Tool;
import nl.overheid.aerius.wui.geo.DrawSourceStyleOptions;
import nl.overheid.aerius.wui.main.event.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.main.event.SimpleGenericEvent.CHANGE;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;

/**
 * Presenter for view to set the location of a source on the map. This can be an point, line or polygon.
 */
public class LocationPresenter extends AbstractActivity implements Presenter, MeasureInteractionLayerListener, OnModificationListener {

  private static final int MIN_POINTS_IN_POLYGON = 4;
  private final PlaceController placeController;
  private final SourceLocationPlace place;
  private final SourceLocationView view;
  private final ScenarioBaseMapLayoutPanel map;
  private final CalculatorUserContext userContext;
  private InteractionLayer<? extends Vector> diaLayer;
  private InteractionLayer<? extends Vector> liaLayer;
  private InteractionLayer<? extends Vector> piaLayer;
  private InteractionLayer<? extends Vector> eiaLayer;
  private InteractionLayer<? extends Vector> aiLayer;

  private final EmissionSource emissionSource;
  private final EmissionSource tmpEmissionSource;
  private VectorFeature editFeature;
  private EventBus eventBus;

  @Inject
  public LocationPresenter(final PlaceController placeController, final SourceLocationView view, final ScenarioBaseMapLayoutPanel map,
      final CalculatorUserContext userContext, @Assisted final SourceLocationPlace place, @Assisted final EmissionSource emissionSource) {
    this.placeController = placeController;
    this.place = place;
    this.view = view;
    this.map = map;
    this.userContext = userContext;
    this.emissionSource = emissionSource;
    //Negative id is interpreted differently by SourceMarkerlayer..
    tmpEmissionSource = emissionSource.copy();
    tmpEmissionSource.setId(-this.emissionSource.getId());
  }

  @Override
  public void cancelEdit() {
    // This will (in normal flow) either go back to LocationPresenter or EmissionSourcesPresenter
    History.back();
  }

  @Override
  public void nextStep() {
    if ((editFeature != null) && !updateEmissionSourceFromFeature(tmpEmissionSource, editFeature)) {
      // if an feature was edited and it's not within the limits no next step allowed, so just return. Error notification is already given in limit
      // check method.
      return;
    }
    if (checkMultiCoordinates(tmpEmissionSource)) {
      // validate line and polygon on amount of points
      return;
    }

    // Another limit check
    if (tmpEmissionSource.getGeometry() != null) {
      if (!limitCheck(tmpEmissionSource.getGeometry())) {
        return;
      }
    }

    emissionSource.setX(tmpEmissionSource.getX());
    emissionSource.setY(tmpEmissionSource.getY());
    emissionSource.setGeometry(tmpEmissionSource.getGeometry());

    eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), tmpEmissionSource, CHANGE.REMOVE));
    // if id == 0 it's a new source. if id is not null but not in the list it's a new source for which the details were added,
    // and then the user hit back.
    if ((emissionSource.getId() == 0) || (userContext.getScenario().getSources(place.getActiveSituationId()).getById(emissionSource.getId()) == null)) {
      userContext.getScenario().getSources(place.getActiveSituationId()).add(emissionSource);
      // If no label set, set a default label, but only for new sources
      if (emissionSource.getLabel() == null) {
        emissionSource.setLabel(M.messages().source() + " " + emissionSource.getId());
      }
    } else {
      eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), emissionSource, CHANGE.UPDATE));
    }

    placeController.goTo(new SourceDetailPlace(emissionSource.getId(), place));
  }

  /**
   * Returns true if there is a problem with multi-coordinates input. It will also update the view with the error state.
   * @param emissionSource the source to check.
   * @return true if there is a problem.
   */
  private boolean checkMultiCoordinates(final EmissionSource emissionSource) {
    boolean problem = false;
    if (emissionSource.getGeometry().getType() == WKTGeometry.TYPE.POLYGON) {
      // if POLYGON has less than 3 coordinates throw warning that POLYGON has to view points
      final String points = emissionSource.getGeometry().getPoints();
      if (points.split(",").length < MIN_POINTS_IN_POLYGON) {
        view.invalidMultiCoordinatesWarning(true, Tool.POLYGON, M.messages().ivEmissionUseMorePointForPolygon());
        problem = true;
      }
    } else if (emissionSource.getGeometry().getType() == WKTGeometry.TYPE.LINE) {
      // if LINE has 1 coordinates throw warning that LINE has to view points
      final String points = emissionSource.getGeometry().getPoints();
      if (points.split(",").length < 2) {
        view.invalidMultiCoordinatesWarning(true, Tool.LINE, M.messages().ivEmissionUseMorePointForLine());
        problem = true;
      } else {
        // if Line begin and end are the same throw warning that LINE has to view points
        final int indexOf = points.indexOf(',');
        final String leftSide = points.substring(0, indexOf);
        final String rightSide = points.substring(indexOf + 1);
        if (leftSide.contentEquals(rightSide)) {
          view.invalidMultiCoordinatesWarning(true, Tool.LINE, M.messages().ivEmissionUseDifferentPointForLine());
          problem = true;
        }
      }
    }
    return problem;
  }

  @Override
  public void onCancel() {
    super.onCancel();
    cleanUp();
  }

  @Override
  public void onStop() {
    super.onStop();
    cleanUp();
  }

  private void cleanUp() {
    if (diaLayer != null) {
      diaLayer.destroy();
    }
    if (liaLayer != null) {
      liaLayer.destroy();
    }
    if (piaLayer != null) {
      piaLayer.destroy();
    }
    if (eiaLayer != null) {
      eiaLayer.destroy();
    }
  }

  @Override
  public void onSwitchTool(final Tool tool, final boolean track) {
    view.selectTool(tool);
    view.invalidInputMultiCoordinates(false);
    if (aiLayer != null) {
      aiLayer.asLayer().destroyFeatures();
      aiLayer.deactivate();
    }
    // Destroy before call of onSwitchTool because otherwise it will destroy the newly created edit feature.
    if (editFeature != null) {
      // Only destroy editFeature if new data set, don't destroy just on switch tool, because the feature could have be changed and therefore
      // if the user changed a line, switched tool to point, and then saves, the modified line should be stored.
      editFeature.destroy();
      editFeature = null;
    }
    switch (tool) {
    case POINT:
      aiLayer = getPointIALayer();
      break;
    case LINE:
      aiLayer = getLineIALayer();
      break;
    case POLYGON:
      aiLayer = getPolygonIALayer();
      break;
    case EDIT:
      aiLayer = getEditIALayer();
      if (editFeature == null) {
        editFeature = new VectorFeature(Geometry.fromWKT(tmpEmissionSource.getGeometry().getWKT()));
        ((ModifyFeatureInteractionLayer) aiLayer).modifyFeature(editFeature);
      }
      break;
    default:
      aiLayer = getPointIALayer();
      break;
    }

    // If tracking is true and the interaction layer supports it, activate it
    if (track && (aiLayer instanceof MeasureInteractionLayer)) {
      ((MeasureInteractionLayer) aiLayer).activatePartialListener();
    }

    aiLayer.activate();
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.eventBus = eventBus;
    view.setPresenter(this);
    map.setVisibleSubstanceLayers(false);
    panel.setWidget(view);
    SourceLocationView.Tool tool = SourceLocationView.Tool.POINT;
    if (emissionSource.getId() == 0) {
      setData(null, 1, 0, 0, true, true);
    } else if (emissionSource.getGeometry() != null) {
      tool = SourceLocationView.Tool.wkt2Tool(emissionSource.getGeometry().getType());
      // if the tool is a line or polygon (meaning not a point) set the tool to edit mode.
      if (tool != SourceLocationView.Tool.POINT) {
        tool = SourceLocationView.Tool.EDIT;
      }
      setData(Geometry.fromWKT(emissionSource.getGeometry().getWKT()), emissionSource.getGeometry().getMeasure(), emissionSource.getX(),
          emissionSource.getY(), true, true);
    } else {
      setData(null, 1, emissionSource.getX(), emissionSource.getY(), true, true);
    }
    view.setEditMode(emissionSource.getId() != 0);
    onSwitchTool(tool, false);
  }

  @Override
  public void updatePosition(final Integer xCoordinate, final Integer yCoordinate) {
    //if updated through the view (inputs on left side of screen), set the right data (expect the source to be a point source).
    //could use geometry = null, but that'd cause an incorrect emissionsource (without a WKTGeometry).
    //could use geometry = tmpEmissionSource.getGeometry(), but that'd mean difference between x/y and the actual geometry.
    final boolean xNull = xCoordinate == null;
    final boolean yNull = yCoordinate == null;
    setData(xNull || yNull ? null : Geometry.fromWKT(new Point(xCoordinate, yCoordinate).toWKT()),
        1, xNull ? 0 : xCoordinate, xNull ? 0 : yCoordinate, false, false);
  }

  @Override
  public void updatePosition(final String value) {
    view.invalidInputMultiCoordinates(false);
    if (aiLayer == liaLayer) {
      try {
        final Geometry geoLine = WktUtil.string2LineString(value);
        if (geoLine != null) {
          final MultiLineString line = MultiLineString.narrowToMultiLineString(geoLine.getJSObject());
          final org.gwtopenmaps.openlayers.client.geometry.Point middle = LineUtil.middleOfMultiLineString(line);
          setData(line, line.getLength(), middle.getX(), middle.getY(), false, false);
        }
      } catch (final JavaScriptException ex) {
        view.invalidInputMultiCoordinates(true);
      }
    } else if (aiLayer == piaLayer) {
      try {
        final Geometry geoPoly = WktUtil.string2Polygon(value);
        if (geoPoly != null) {
          final Polygon polygon = Polygon.narrowToPolygon(geoPoly.getJSObject());
          setData(polygon, polygon.getArea(), polygon.getCentroid().getX(), polygon.getCentroid().getY(), false, false);
        }
      } catch (final JavaScriptException ex) {
        view.invalidInputMultiCoordinates(true);
      }
    }
  }

  private InteractionLayer<? extends Vector> getPointIALayer() {
    if (diaLayer == null) {
      diaLayer = new MeasureInteractionLayer(map, new PointHandler(), DrawSourceStyleOptions.getDefaultStyleMap(), this);
    }
    return diaLayer;
  }

  private InteractionLayer<? extends Vector> getLineIALayer() {
    if (liaLayer == null) {
      liaLayer = new MeasureInteractionLayer(map, new PathHandler(), DrawSourceStyleOptions.getDefaultStyleMap(), this);
    }
    return liaLayer;
  }

  private InteractionLayer<? extends Vector> getPolygonIALayer() {
    if (piaLayer == null) {
      piaLayer = new MeasureInteractionLayer(map, new PolygonHandler(), DrawSourceStyleOptions.getDefaultStyleMap(), this);
    }
    return piaLayer;
  }

  private InteractionLayer<? extends Vector> getEditIALayer() {
    if (eiaLayer == null) {
      eiaLayer = new ModifyFeatureInteractionLayer(map, null, DrawSourceStyleOptions.getDefaultStyleMap(), this);
    }
    return eiaLayer;
  }

  @Override
  public void onMeasure(final MeasureInteractionEvent e) {
    setData(e.getGeometry(), e.getMeasure(), e.getX(), e.getY(), true, true);
  }

  @Override
  public void onMeasurePartial(final MeasureInteractionEvent e) {
    view.setPartialMeasurement(e.getMeasure());
  }

  @Override
  public void onModification(final VectorFeature vectorFeature) {
    final Geometry geometry = vectorFeature.getGeometry();

    if (geometry.getClassName().equals(Geometry.LINESTRING_CLASS_NAME)) {
      final Geometry line = LineString.narrowToLineString(geometry.getJSObject());
      view.setPartialMeasurement(line.getLength());
    } else if (geometry.getClassName().equals(Geometry.POLYGON_CLASS_NAME)) {
      final Polygon poly = Polygon.narrowToPolygon(geometry.getJSObject());
      view.setPartialMeasurement(poly.getGeodesicArea(new Projection(map.getMap().getProjection())));
    }
  }

  /**
   * Sets the x and y coordinates on the view and updates the marker on the map. If there is no marker yet, because it's a new source a new marker is
   * placed. If it is an existing source the current marker is marked differently.
   * <p>If the geometry is not null and not a point the view is set to edit mode.
   * <p>If an feature was edited, it will be destroyed as there is new data.
   * @param updateView if true also update the view content
   */
  private void setData(final Geometry geometry, final double measure, final double x, final double y, final boolean updateView, final boolean toEdit) {
    view.setPartialMeasurement(measure);

    final WKTGeometry wktgeo = geometry == null ? null : new WKTGeometry(geometry.toString(), measure);
    limitCheck(wktgeo); // call limitCheck to trigger notifications.
    // if it's a point there is no edit mode, so no edit is false
    final boolean canEdit = (wktgeo != null) && (wktgeo.getType() != WKTGeometry.TYPE.POINT);
    view.setEnabledEdit(canEdit);
    if (updateView) {
      if ((wktgeo == null) || (wktgeo.getType() == TYPE.POINT)) {
        view.setXY(x, y);
      } else {
        view.setMultiCoordinates(wktgeo.getPoints());
      }
    }
    if (geometry != null) {
      tmpEmissionSource.setX(x);
      tmpEmissionSource.setY(y);
      tmpEmissionSource.setGeometry(wktgeo);
      if ((x > 0) && (y > 0)) {
        eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), tmpEmissionSource, CHANGE.ADD));
      }
    }
    // tmpEmissionSource must be updated before calling onSwitchTool because the method depends on tmpEmissionSource.
    // only switch tool to edit if can be set toEdit and can edit in the first place
    if (toEdit && canEdit) {
      onSwitchTool(SourceLocationView.Tool.EDIT, false);
    }
    view.setEnabledNextStep((x > 0) && (y > 0));
  }

  /**
   * Checks if the geometry violates any limit. If fires an event if a limit is violated and returns false. If everything is ok true is returned.
   * @param geometry geometry to check
   * @return true if everything is ok, false if a limit is violated
   */
  private boolean limitCheck(final WKTGeometry geometry) {
    boolean noLimit = true;
    if (geometry != null) {
      view.setPartialMeasurement(geometry.getMeasure());
      final CalculatorLimits limits = userContext.getCalculatorLimits();

      switch (geometry.getType()) {
      case LINE:
        final LineString line = LineString.narrowToLineString(LineString.fromWKT(geometry.getWKT()).getJSObject());
        if (SelfIntersect.checkSelfIntersection(line)) {
          NotificationUtil.broadcastError(eventBus, M.messages().notificationLineIntersects());
          noLimit = false;
        } else if (!limits.isWithinLineLengthLimit(geometry.getMeasure())) {
          NotificationUtil.broadcastError(eventBus,
              M.messages().notificationLineTooLong(limits.getMaxLineLength(), MathUtil.round(geometry.getMeasure())));
          noLimit = false;
        } // else everything is fine.
        break;
      case POLYGON:
        final Polygon polygon = Polygon.narrowToPolygon(Polygon.fromWKT(geometry.getWKT()).getJSObject());
        final int surface = (int) (geometry.getMeasure() / SharedConstants.M2_TO_HA);
        if (SelfIntersect.checkSelfIntersection(polygon)) {
          NotificationUtil.broadcastError(eventBus, M.messages().notificationPolygonIntersects());
          noLimit = false;
        } else if (!limits.isWithinPolygonSurfaceLimit(surface)) {
          NotificationUtil.broadcastError(eventBus, M.messages().notificationSurfaceToLarge(limits.getMaxPolygonSurface(), surface));
          noLimit = false;
        } // else everything is fine.
        break;
      default:
        // everything is fine.
      }
    }
    return noLimit;
  }

  /**
   * Updates the source from the feature when the source is a line or polygon and the feature is within the limits set.
   * If a line or polygon and it's new geometry is within the limits true is returned. if any limits were violated it returns false.
   * @param source source to update
   * @param feature feature to get new geometry from
   * @return true if no geometry limit violated
   */
  private boolean updateEmissionSourceFromFeature(final EmissionSource source, final VectorFeature feature) {
    boolean noLimit = true;
    if ((feature == null) || (source.getGeometry() == null)) {
      return noLimit;
    }
    double measure = 0;
    double x = 0;
    double y = 0;
    if (source.getGeometry().getType() == TYPE.LINE) {
      final MultiLineString line = MultiLineString.narrowToMultiLineString(feature.getGeometry().getJSObject());
      final LonLat ll = LineUtil.middleOfMultiLineString(line).getBounds().getCenterLonLat();
      measure = line.getLength();
      x = ll.lon();
      y = ll.lat();
    } else if (source.getGeometry().getType() == TYPE.POLYGON) {
      final Polygon polygon = Polygon.narrowToPolygon(feature.getGeometry().getJSObject());
      measure = polygon.getArea();
      // test if we have area
      if (measure > 0) {
        x = polygon.getCentroid().getX();
        y = polygon.getCentroid().getY();
      } else {
        return noLimit;
      }
    } else {
      // else nothing to update, just return.
      return noLimit;
    }
    // this code is and should only be reached when line or polygon.
    final WKTGeometry wktGeometry = new WKTGeometry(feature.getGeometry().toString(), measure);
    noLimit = limitCheck(wktGeometry);
    if (noLimit) {
      source.setX(x);
      source.setY(y);
      source.setGeometry(wktGeometry);
    }
    return noLimit;
  }
}
