/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.place.shared.Place;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.importer.CalculatorImportController;
import nl.overheid.aerius.wui.calculator.place.CalculationPointPlace;
import nl.overheid.aerius.wui.calculator.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.calculator.place.MeldingPlace;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.menu.MenuItem;
import nl.overheid.aerius.wui.main.ui.menu.SimpleMenuItem;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.menu.ScenarioBaseListMenuItem;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlaceFactory;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseTaskBar;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseViewImpl;

/**
 * Main View for the Calculator application. Contains left panel + top buttons and map.
 */
@Singleton
public class CalculatorViewImpl extends ScenarioBaseViewImpl {

  private final HelpPopupController hpC;

  @Inject
  public CalculatorViewImpl(final CalculatorAppContext appContext, final EventBus eventBus, final HelpPopupController hpC,
      final ScenarioBaseTaskBar taskBar, final ScenarioBaseMapLayoutPanel map, final CalculatorImportController importController) {
    super(appContext, eventBus, taskBar, map, importController, new SimplePanel());
    this.hpC = hpC;
  }

  @Override
  public String getTitleText() {
    return M.messages().calculator();
  }

  @Override
  public String getTitleColor() {
    return R.css().aeriusCalculatorTitle();
  }

  @Override
  public List<MenuItem> getMenuItems() {
    final List<MenuItem> menus = new ArrayList<>();
    final ScenarioBaseListMenuItem calculatorTheme = new ScenarioBaseListMenuItem(TestID.MENU_ITEM_CALCULATOR_THEME);
    final SimpleMenuItem emissionMenuItem = new SimpleMenuItem(M.messages().calculatorMenuEmissionSources(),
        TestID.MENU_ITEM_EMISSIONSOURCES_OVERVIEW) {

      @Override
      public Place getPlace(final Place place) {
        return new SourcesPlace(place);
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof SourcesPlace || place instanceof SourcePlace;
      }

      @Override
      public void setHelpInfo(final HelpInfo helpInfo) {
        super.setHelpInfo(hpC.tt().ttCalculationMainMenu());
      }
    };
    final SimpleMenuItem calculationPointMenuItem = new SimpleMenuItem(M.messages().calculatorMenuCalculationPoints(),
        TestID.MENU_ITEM_CALCULATION_POINTS) {
      @Override
      public Place getPlace(final Place place) {
        return new CalculationPointsPlace((ScenarioBasePlace) place);
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof CalculationPointsPlace || place instanceof CalculationPointPlace;
      }
    };
    final SimpleMenuItem resultMenuItem = new SimpleMenuItem(M.messages().calculatorMenuResults(), TestID.MENU_ITEM_RESULTS) {
      @Override
      public Place getPlace(final Place place) {
        return ResultPlaceFactory.resultState2Place(appContext.getResultPlaceState(), place);
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof ResultPlace;
      }
    };
    final SimpleMenuItem meldingMenuItem = new SimpleMenuItem(M.messages().calculatorMenuMelding(), TestID.MENU_ITEM_MELDING) {
      @Override
      public Place getPlace(final Place place) {
        return new MeldingPlace((ScenarioBasePlace) place);
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof MeldingPlace;
      }
    };

    calculatorTheme.setHelpInfo(hpC.tt().ttCalculationMainMenu());
    resultMenuItem.setHelpInfo(hpC.tt().ttCalculationMainMenu());
    calculationPointMenuItem.setHelpInfo(hpC.tt().ttCalculationMainMenu());
    emissionMenuItem.setHelpInfo(hpC.tt().ttCalculationMainMenu());

    menus.add(calculatorTheme);
    menus.add(emissionMenuItem);
    menus.add(calculationPointMenuItem);
    menus.add(resultMenuItem);

    // temporarily disable - might be removed in next release
//    if ((Boolean) appContext.getContext().getSetting(SharedConstantsEnum.MELDING_ENABLED)) {
//      menus.add(meldingMenuItem);
//    }

    return menus;
  }

  @Override
  public DataResource getLogo() {
    return R.images().aeriusCalculatorLogo();
  }
}