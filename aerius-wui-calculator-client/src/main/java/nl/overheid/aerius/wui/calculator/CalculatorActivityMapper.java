/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator;

import java.util.ArrayList;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.wui.calculator.CalculatorActivityMapper.CalculatorActivityFactory;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.place.CalculationPointPlace;
import nl.overheid.aerius.wui.calculator.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.calculator.place.MeldingPlace;
import nl.overheid.aerius.wui.calculator.place.SourceDetailPlace;
import nl.overheid.aerius.wui.calculator.place.SourceLocationPlace;
import nl.overheid.aerius.wui.calculator.ui.CalculationPointPresenter;
import nl.overheid.aerius.wui.calculator.ui.CalculationPointsPresenter;
import nl.overheid.aerius.wui.calculator.ui.CalculatorEmissionSourcesPresenter;
import nl.overheid.aerius.wui.calculator.ui.LocationPresenter;
import nl.overheid.aerius.wui.calculator.ui.MeldingActivity;
import nl.overheid.aerius.wui.calculator.ui.ResultOverviewPresenter;
import nl.overheid.aerius.wui.calculator.ui.SourceDetailPresenter;
import nl.overheid.aerius.wui.calculator.ui.StartUpPresenter;
import nl.overheid.aerius.wui.scenario.base.ScenarioBaseActivityFactory;
import nl.overheid.aerius.wui.scenario.base.ScenarioBaseActivityMapper;
import nl.overheid.aerius.wui.scenario.base.place.NetworkDetailPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultFilterPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultGraphicsPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultOverviewPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultTablePlace;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;
import nl.overheid.aerius.wui.scenario.base.place.StartUpPlace;
import nl.overheid.aerius.wui.scenario.base.ui.EmissionSourcesPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.NetworkDetailPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.ResultFilterPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.ResultGraphicsPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.ResultTablePresenter;

/**
 * Activity Mapper for the Calculator.
 */
class CalculatorActivityMapper extends ScenarioBaseActivityMapper<CalculatorActivityFactory, CalculatorAppContext, CalculatorContext, CalculatorUserContext> {

  /**
   * CaclulatorActivityMapper associates each Place with its corresponding
   * {@link Activity}.
   *
   * @param factory Generated factory to add params to constructors
   * @param contextProvider user context object
   */
  @Inject
  public CalculatorActivityMapper(final CalculatorActivityFactory factory, final PlaceController placeController,
      final CalculatorAppContext appContext) {
    super(factory, appContext, placeController);
  }

  @Override
  public Activity getActivity(final Place place) {
    initSingleton();

    Activity presenter = null;
    Place redirectPlace = new StartUpPlace(null);
    final CalculatorUserContext userContext = context.getUserContext();
    // First check if we have any valid data already, if not always go to the startup place.
    if (userContext.getScenario().getSourceLists().isEmpty()) {
      if (place instanceof StartUpPlace) {
        // empty source list, but place is default, therefore just show the default.
        presenter = factory.createStartUpPresenter((StartUpPlace) place);
      }
    } else {
      initSourcesList(place, userContext);
      if (place instanceof SourcePlace) {
        final EmissionSource es = getEmissionSource(context, userContext, (SourcePlace) place, ((SourcePlace) place).getSourceId());

        // if EmissionSource has no geometry yet don't show source detail
        if (place instanceof SourceDetailPlace && es.getGeometry() != null) {
          presenter = factory.createSourceDetailPresenter((SourceDetailPlace) place, es);
        } else if (place instanceof NetworkDetailPlace) {
          presenter = factory.createNetworkDetailPresenter((NetworkDetailPlace) place, ((SRM2NetworkEmissionSource) es).getEmissionSources());
        } else if (place instanceof SourceLocationPlace) {
          presenter = factory.createLocationPresenter((SourceLocationPlace) place, es);
        } // else unknown place, so fall though with presenter null.
      } else if (place instanceof SourcesPlace) {
        presenter = factory.createEmissionSourcesPresenter((SourcesPlace) place);
      } else if (place instanceof CalculationPointsPlace) {
        if (userContext.getScenario().getCalculationPoints().isEmpty()) {
          redirectPlace = new CalculationPointPlace(null, (ScenarioBasePlace) place);
        } else {
          presenter = factory.createCalculationPointsPresenter((CalculationPointsPlace) place);
        }
      } else if (place instanceof ResultOverviewPlace) {
        presenter = factory.createResultOverviewPresenter((ResultOverviewPlace) place);
      } else if (place instanceof ResultGraphicsPlace) {
        presenter = factory.createResultGraphicsPresenter((ResultGraphicsPlace) place);
      } else if (place instanceof ResultTablePlace) {
        presenter = factory.createResultTablePresenter((ResultTablePlace) place);
      } else if (place instanceof ResultFilterPlace) {
        presenter = factory.createResultFilterPresenter((ResultFilterPlace) place);
      } else if (place instanceof CalculationPointPlace) {
        final AeriusPoint cp = userContext.getScenario().getCalculationPoints().getById(((CalculationPointPlace) place).getCPId());
        presenter = factory.createCalculationPointPresenter((CalculationPointPlace) place, cp);
      } else if (place instanceof MeldingPlace) {
        presenter = factory.createMeldingPresenter((MeldingPlace) place);
      } else if (place instanceof StartUpPlace) {
        presenter = factory.createStartUpPresenter((StartUpPlace) place);
      } // else: unknown place or empty source list. Go to the default location
    }
    if (presenter != null) {
      factory.createPreActivity((ScenarioBasePlace) place);
      // normal place just go to this place
      return presenter;
    } else {
      // empty source list, no default place or known place and not the default place
      //, so defer a goto that will be run after all is finished.
      redirectPlace(redirectPlace);
      return null;
    }
  }

  /**
   * Methods capable of creating presenters given the place that is passed in.
   */
  public interface CalculatorActivityFactory extends ScenarioBaseActivityFactory {
    /**
     * Create a new MeldingActivity.
     * @param place
     * @return
     */
    MeldingActivity createMeldingPresenter(MeldingPlace place);

    /**
     * Create a new {@link StartUpPresenter}.
     * @param place
     *
     * @return new {@link StartUpPresenter}
     */
    StartUpPresenter createStartUpPresenter(StartUpPlace place);

    /**
     * Create a new {@link LocationPresenter} and passes the emission source
     * referred to in the place.
     *
     * @param place current place
     * @param emissionSource emissionSource referred to in the place
     * @return new {@link LocationPresenter}
     */
    LocationPresenter createLocationPresenter(SourceLocationPlace place, EmissionSource emissionSource);

    /**
     * Create a new {@link SourceDetailPresenter} and passes the emission source
     * referred to in the place.
     *
     * @param place current place
     * @param emissionSource emissionSource referred to in the place
     * @return new {@link SourceDetailPresenter}
     */
    SourceDetailPresenter createSourceDetailPresenter(SourceDetailPlace place, EmissionSource emissionSource);

    /**
     * Create a new {@link NetworkDetailPresenter} and passes the emission source
     * referred to in the place.
     *
     * @param place current place
     * @param arrayList emissionSource referred to in the place
     * @return new {@link NetworkDetailPresenter}
     */
    NetworkDetailPresenter createNetworkDetailPresenter(NetworkDetailPlace place, ArrayList<EmissionSource> arrayList);

    /**
     * Create a new {@link EmissionSourcesPresenter}.
     *
     * @param place current place
     * @return new {@link EmissionSourcesPresenter}
     */
    CalculatorEmissionSourcesPresenter createEmissionSourcesPresenter(SourcesPlace place);

    /**
     * Create a new {@link CalculationPointsPresenter}.
     *
     * @param place current place
     * @return new {@link CalculationPointsPresenter}
     */
    CalculationPointsPresenter createCalculationPointsPresenter(CalculationPointsPlace place);

    /**
     * Create a new {@link CalculationPointPresenter}.
     *
     * @param place current place
     * @param point the CalculationPoint
     * @return new {@link CalculationPointPresenter}
     */
    CalculationPointPresenter createCalculationPointPresenter(CalculationPointPlace place, AeriusPoint point);

    /**
     * Create a new {@link ResultOverviewPresenter}
     *
     * @param place a current place
     * @return new {@link ResultOverviewPresenter}
     */
    ResultOverviewPresenter createResultOverviewPresenter(ResultOverviewPlace place);

    /**
     * Create a new {@link ResultGraphicsPresenter}.
     *
     * @param place current place
     * @return new {@link ResultGraphicsPresenter}
     */
    ResultGraphicsPresenter createResultGraphicsPresenter(ResultGraphicsPlace place);

    /**
     * Create a new {@link ResultTablePresenter}.
     *
     * @param place current place
     * @return new {@link ResultTablePresenter}
     */
    ResultTablePresenter createResultTablePresenter(ResultTablePlace place);

    /**
     * Create a new {@link ResultFilterPresenter}.
     *
     * @param place current place
     * @return new {@link ResultFilterPresenter}
     */
    ResultFilterPresenter createResultFilterPresenter(ResultFilterPlace place);
  }
}
