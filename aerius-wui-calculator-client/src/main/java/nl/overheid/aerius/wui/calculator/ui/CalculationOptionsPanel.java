/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.ui.editor.RadioButtonGroupEditor;
import nl.overheid.aerius.wui.main.widget.AttachedPopupBase;
import nl.overheid.aerius.wui.main.widget.AttachedPopupPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Panel handling all configuration of calculation options.
 */
public class CalculationOptionsPanel extends Composite implements CalculationOptionsView, CloseHandler<PopupPanel>, RequiresResize {

  private static CalculationOptionsPanelUiBinder uiBinder = GWT.create(CalculationOptionsPanelUiBinder.class);

  interface CalculationOptionsPanelUiBinder extends UiBinder<Widget, CalculationOptionsPanel> { }

  public interface CalculationOptionsPanelDriver extends SimpleBeanEditorDriver<CalculationSetOptions, CalculationOptionsPanel> { }

  @UiField AttachedPopupBase wrapper;
  @UiField FlowPanel container;

  @UiField @Ignore RadioButton permitRadioButton;
  @UiField @Ignore RadioButton natureareaRadioButton;
  @UiField @Ignore RadioButton userDefinedRadioButton;

  RadioButtonGroupEditor<CalculationType> calculationTypeEditor = new RadioButtonGroupEditor<CalculationType>("calcType") {

    @Override
    protected CalculationType string2Value(final String value) {
      final CalculationType safeValue = CalculationType.safeValueOf(value);
      return safeValue == null ? CalculationType.PAS : safeValue;
    }

    @Override
    protected String value2String(final CalculationType value) {
      return (value == null ? CalculationType.PAS : value).name();
    }

    @Override
    public void setValue(final CalculationType value) {
      super.setValue(value);
    }
  };

  private PopupPanel popupPanel;
  private Presenter presenter;

  private final ScheduledCommand layoutCmd = new ScheduledCommand() {
    @Override
    public void execute() {
      layoutScheduled = false;
      forceLayout();
    }
  };

  private boolean layoutScheduled;

  @Inject
  public CalculationOptionsPanel(final HelpPopupController hpC, final CalculatorContext context) {
    initWidget(uiBinder.createAndBindUi(this));
    calculationTypeEditor.addRadioButton(permitRadioButton, CalculationType.PAS);
    calculationTypeEditor.addRadioButton(userDefinedRadioButton, CalculationType.CUSTOM_POINTS);

    hpC.addWidget(wrapper, hpC.tt().ttOverviewCalculation());
    hpC.addWidget(natureareaRadioButton, hpC.tt().ttOverviewCalculationMethod());
    hpC.addWidget(userDefinedRadioButton, hpC.tt().ttOverviewCalculationMethodUserDefined());

    natureareaRadioButton.ensureDebugId(TestID.RADIO_CALC_OPT_N2000);
    userDefinedRadioButton.ensureDebugId(TestID.RADIO_CALC_OPT_CALCPOINTS);
  }

  /**
   * Check if endyear is selected and greater than start year and show user warning.
   *
   * @return boolean.
   */
  public boolean isEndYearValid() {
    return true;
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setPopup(final AttachedPopupPanel popupPanel) {
    this.popupPanel = popupPanel;
    popupPanel.addCloseHandler(this);

    wrapper.setPopup(popupPanel);
  }

  @Override
  public void onClose(final CloseEvent<PopupPanel> event) {
    if (!presenter.saveOptions(true)) {
      Scheduler.get().scheduleDeferred(new ScheduledCommand() {

        @Override
        public void execute() {
          popupPanel.show();
        }
      });
    }
  }

  @Override
  public void onResize() {
    scheduledLayout();
  }

  private void scheduledLayout() {
    if (layoutScheduled) {
      return;
    }

    layoutScheduled = true;
    Scheduler.get().scheduleDeferred(layoutCmd);
  }

  private void forceLayout() {
    container.getElement().getStyle().setProperty("maxHeight", wrapper.calculateMaxHeightBasedOnScreen(), Unit.PX);
  }
}
