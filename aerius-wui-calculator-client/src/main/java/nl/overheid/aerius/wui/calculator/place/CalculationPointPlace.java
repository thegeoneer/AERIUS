/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.place;

import java.util.Map;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Place for CalculationPoint location configuration..
 */
public class CalculationPointPlace extends ScenarioBasePlace {

  private static final String PREFIX = "cp";
  private static final String CP_ID = "cpid";

  /**
   * Tokenizer for {@link CalculationPointPlace}.
   */
  @Prefix(PREFIX)
  public static class Tokenizer extends ScenarioBasePlace.Tokenizer<CalculationPointPlace> implements PlaceTokenizer<CalculationPointPlace> {

    private static final int NO_CPID = -1;

    @Override
    protected CalculationPointPlace createPlace() {
      return new CalculationPointPlace();
    }

    @Override
    protected void updatePlace(final Map<String, String> tokens, final CalculationPointPlace place) {
      super.updatePlace(tokens, place);
      place.setCPId(intValue(tokens, CP_ID));
    }

    @Override
    protected void setTokenMap(final CalculationPointPlace place, final Map<String, String> tokens) {
      super.setTokenMap(place, tokens);
      if (place.getCPId() != NO_CPID) {
        put(tokens, CP_ID, place.getCPId());
      }
    }
  }

  private int cpId;

  public CalculationPointPlace(final HasId cp, final ScenarioBasePlace currentPlace) {
    super(currentPlace);
    this.cpId = cp == null ? 0 : cp.getId();
  }

  public CalculationPointPlace(final CalculationPointPlace currentPlace) {
    super(currentPlace);
    this.cpId = currentPlace.cpId;
  }

  CalculationPointPlace() {
  }

  @Override
  public CalculationPointPlace copy() {
    return copyTo(new CalculationPointPlace(this));
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && cpId == ((CalculationPointPlace) obj).cpId
        && super.equals(obj);
  }

  public int getCPId() {
    return cpId;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + 17 * cpId;
    result = 31 * result + super.hashCode();
    return result;
  }

  public void setCPId(final int cpId) {
    this.cpId = cpId;
  }

  public void setCPId(final HasId cp) {
    this.cpId = cp == null ? 0 : cp.getId();
  }

  @Override
  public String toString() {
    return "CalculationPointPlace [cpId=" + cpId + "], " + super.toString();
  }
}
