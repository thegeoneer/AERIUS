/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.util.development;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.ButtonElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.sector.EmissionCalculationMethod;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.calculator.place.SourceDetailPlace;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.util.development.EmbeddedDevPanel;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;

/**
 * For development puposes only. Will not be present in production code.
 */
public class CalculatorDevPanel extends Composite implements EmbeddedDevPanel {

  interface CalculatorDevPanelUiBinder extends UiBinder<Widget, CalculatorDevPanel> { }

  private static CalculatorDevPanelUiBinder uiBinder = GWT.create(CalculatorDevPanelUiBinder.class);
  private static final int NEAR_BOTSHOL_X = 121326;
  private static final int NEAR_BOTSHOL_Y = 475447;
  private static final int RANDOM_DISTANCE = 1000;
  private static final int RANDOM_LARGE_DISTANCE = 10000;
  private static final double ZOOM_BOUNDARY = 10000;
  private static final int HUNDRED = 100;
  private static final int[] INLAND_COOR = {208272, 473281, 208160, 473301, };
  private static final int[] MARITIME_COOR = {62561, 439180, 64523, 439879, };

  private final EventBus eventBus;
  private final PlaceController placeController;
  private final CalculatorAppContext appContext;

  @Inject
  public CalculatorDevPanel(final EventBus eventBus, final PlaceController placeController, final CalculatorAppContext appContext) {
    this.eventBus = eventBus;
    this.placeController = placeController;
    this.appContext = appContext;
    initWidget(uiBinder.createAndBindUi(this));
  }

  @UiHandler("createSrc")
  void onCreateSrcClick(final ClickEvent e) {
    addSource(createGenericSource());
  }

  @UiHandler("createHundredSrc")
  void onCreateHundredSrcClick(final ClickEvent e) {
    final int activeSId = ((ScenarioBasePlace) placeController.getWhere()).getActiveSituationId();

    for (int i = 0; i < HUNDRED; i++) {
      final EmissionSource es = createGenericSource();
      addSource2Context(activeSId, es);
    }
    goToLocation(appContext.getUserContext().getSources(activeSId).get(0));
    placeController.goTo(new SourcesPlace((ScenarioBasePlace) placeController.getWhere()));
  }

  private void addSource2Context(final int activeSId, final EmissionSource es) {
    appContext.getUserContext().getSources(activeSId).add(es);
    es.setLabel("Source " + es.getId());
  }

  private EmissionSource createGenericSource() {
    final GenericEmissionSource src = createSrc(new GenericEmissionSource(), EmissionCalculationMethod.GENERIC);
    setRandomEmission(src, Substance.NH3);
    setRandomEmission(src, Substance.NOX);
    return src;
  }

  private void setRandomEmission(final GenericEmissionSource source, final Substance substance) {
    final int year = appContext.getUserContext().getEmissionValueKey().getYear();
    source.setEmission(new EmissionValueKey(year, substance), RANDOM_DISTANCE + getRandomDistance());
  }

  @UiHandler("createRAV")
  void onCreateRAVClicked(final ClickEvent e) {
    addSource(createSrc(new FarmEmissionSource(), EmissionCalculationMethod.FARM_LODGE));
  }

  @UiHandler("createInlandShip")
  void onCreateInlandShipClick(final ClickEvent e) {
    addSource(createLineSrc(new InlandMooringEmissionSource(), EmissionCalculationMethod.SHIPPING_INLAND_DOCKED,
        INLAND_COOR[0], INLAND_COOR[1], INLAND_COOR[2], INLAND_COOR[3]));
  }

  @UiHandler("createShip")
  void onCreateShipClicked(final ClickEvent e) {
    addSource(createLineSrc(new MaritimeMooringEmissionSource(), EmissionCalculationMethod.SHIPPING_MARITIME_DOCKED,
        MARITIME_COOR[0], MARITIME_COOR[1], MARITIME_COOR[2], MARITIME_COOR[3]));
  }

  @UiHandler("createRoad")
  void onCreateVehicleClicked(final ClickEvent e) {
    addSource(createLineSrc(new SRM2EmissionSource(), EmissionCalculationMethod.ROAD));
  }

  private void addSource(final EmissionSource es) {
    final int activeSId = ((ScenarioBasePlace) placeController.getWhere()).getActiveSituationId();
    appContext.initEmissionSourceList(activeSId);
    addSource2Context(activeSId, es);
    goToLocation(es);
    placeController.goTo(new SourceDetailPlace(es.getId(), (ScenarioBasePlace) placeController.getWhere()));
  }

  @UiHandler("createCP")
  void onCreateCpClick(final ClickEvent e) {
    final AeriusPoint cp = new AeriusPoint(AeriusPointType.POINT);
    cp.setX(NEAR_BOTSHOL_X + getRandomLargeDistance());
    cp.setY(NEAR_BOTSHOL_Y + getRandomLargeDistance());
    cp.setLabel("Dev panel cp");

    goToLocation(cp);
    appContext.getUserContext().getCalculationPoints().add(cp);
    placeController.goTo(new CalculationPointsPlace((ScenarioBasePlace) placeController.getWhere()));
  }

  private void goToLocation(final Point p) {
    eventBus.fireEvent(new LocationChangeEvent(new BBox(p.getX(), p.getY(), ZOOM_BOUNDARY)));
  }

  @UiHandler("purge")
  void onPurgeClick(final ClickEvent e) {
    appContext.getUserContext().getCalculationPoints().clear();
    for (final EmissionSourceList l : appContext.getUserContext().getSourceLists()) {
      l.clear();
    }
  }

  @UiHandler("calculate")
  void onCalculateClick(final ClickEvent e) {
    final int activeSId = ((ScenarioBasePlace) placeController.getWhere()).getActiveSituationId();
    appContext.initEmissionSourceList(activeSId);

    for (int i = 0; i < 3; i++) {
      final EmissionSource es = createGenericSource();
      addSource2Context(activeSId, es);
    }

    goToLocation(appContext.getUserContext().getSources(activeSId).get(0));
    placeController.goTo(new SourcesPlace((ScenarioBasePlace) placeController.getWhere()));

    appContext.getCalculationOptions().setCalculateMaximumRange(4000);
    appContext.getCalculationOptions().setCalculationType(CalculationType.NATURE_AREA);

    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        final ButtonElement button = DOM.getElementById("gwt-debug-" + TestID.BUTTON_CALCULATE).cast();
        button.click();
      }
    });
  }

  private <E extends EmissionSource> E createSrc(final E src, final EmissionCalculationMethod type) {
    src.setX(NEAR_BOTSHOL_X + getRandomDistance());
    src.setY(NEAR_BOTSHOL_Y + getRandomDistance());
    src.setGeometry(new WKTGeometry(src.toWKT()));
    src.setSector(getSector(type));
    return src;
  }

  private <E extends EmissionSource> E createLineSrc(final E src, final EmissionCalculationMethod type) {
    final int minx = (int) (NEAR_BOTSHOL_X + getRandomDistance());
    final int miny = (int) (NEAR_BOTSHOL_Y + getRandomDistance());
    final int maxx = (int) (NEAR_BOTSHOL_X + getRandomDistance());
    final int maxy = (int) (NEAR_BOTSHOL_Y + getRandomDistance());
    return createLineSrc(src, type, minx, miny, maxx, maxy);
  }

  private <E extends EmissionSource> E createLineSrc(final E src, final EmissionCalculationMethod type, final int minx, final int miny,
      final int maxx, final int maxy) {
    src.setGeometry(new WKTGeometry("LINESTRING(" + minx + " " + miny + "," + maxx + " " + maxy + ")"));
    src.setX(Math.min(minx, maxx) + Math.abs(minx - maxx) / 2);
    src.setY(Math.min(miny, maxy) + Math.abs(miny - maxy) / 2);
    final int deltaX = Math.abs(minx - maxx);
    final int deltaY = Math.abs(miny - maxy);
    src.getGeometry().setMeasure((int) Math.sqrt(deltaX * deltaX + deltaY * deltaY));
    src.setSector(getSector(type));
    return src;
  }

  private Sector getSector(final EmissionCalculationMethod method) {
    for (final Sector s : appContext.getContext().getCategories().getSectors()) {
      if (s.getProperties().getMethod() == method) {
        return s;
      }
    }
    return null;
  }

  private int getRandomLargeDistance() {
    return (int) (Math.random() * RANDOM_LARGE_DISTANCE);
  }

  private double getRandomDistance() {
    return Math.random() * RANDOM_DISTANCE;
  }
}
