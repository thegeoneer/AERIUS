/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.importer;

import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.service.RetrieveImportServiceAsync;
import nl.overheid.aerius.wui.calculator.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;

/**
 * Import Controller for Calculator.
 */
public class CalculatorImportController extends ScenarioBaseImportController {

  private final PlaceController placeController;

  @Inject
  public CalculatorImportController(final PlaceController placeController, final EventBus eventBus, final ScenarioBaseMapLayoutPanel map,
      final ScenarioBaseAppContext<?, ?> context, final RetrieveImportServiceAsync service, final HelpPopupController hpC) {
    super(map, context, service, hpC, false, false, false, false);
    this.placeController = placeController;
  }

  @Override
  protected ScenarioBasePlace getImportCompletePlace(final int sid1, final int sid2, final ImportResult result, final Situation viewSituation,
      final String uuid) {
    final ScenarioBasePlace current = (ScenarioBasePlace) placeController.getWhere();
    final ScenarioBasePlace place = result.getSourceLists().isEmpty() && !result.getCalculationPoints().isEmpty()
        ? new CalculationPointsPlace(current) : new SourcesPlace(current);
    place.setsId1(sid1);
    place.setsId2(sid2);
    place.setSituation(viewSituation);
    return place;
  }
}
