/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import org.gwtopenmaps.openlayers.client.handler.PointHandler;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.geo.InteractionLayer;
import nl.overheid.aerius.geo.MeasureInteractionLayer;
import nl.overheid.aerius.geo.MeasureInteractionLayer.MeasureInteractionEvent;
import nl.overheid.aerius.geo.MeasureInteractionLayer.MeasureInteractionLayerListener;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.util.FormatUtil;
import nl.overheid.aerius.wui.calculator.calculationpoint.DetermineCalculationPointsDialogController;
import nl.overheid.aerius.wui.calculator.place.CalculationPointPlace;
import nl.overheid.aerius.wui.calculator.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.calculator.ui.CalculationPointView.Presenter;
import nl.overheid.aerius.wui.geo.DrawSourceStyleOptions;
import nl.overheid.aerius.wui.main.event.CalculationPointChangeEvent;
import nl.overheid.aerius.wui.main.event.SimpleGenericEvent.CHANGE;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;

/**
 */
public class CalculationPointPresenter extends AbstractActivity implements Presenter, MeasureInteractionLayerListener {
  private final CalculationPointView view;
  private final CalculatorUserContext userContext;
  private final ScenarioBaseMapLayoutPanel map;
  private final PlaceController placeController;
  private final DetermineCalculationPointsDialogController determinePointsController;

  private InteractionLayer<? extends Vector> interactionLayer;

  private final AeriusPoint point;
  private final CalculationPointPlace place;
  private EventBus eventBus;

  @Inject
  public CalculationPointPresenter(final PlaceController placeController, final CalculationPointView view, final ScenarioBaseMapLayoutPanel map, final CalculatorUserContext userContext,
      final DetermineCalculationPointsDialogController determinePointsController, @Assisted final CalculationPointPlace place, @Assisted final AeriusPoint point) {
    this.map = map;
    this.view = view;
    this.place = place;
    this.userContext = userContext;
    this.placeController = placeController;
    this.determinePointsController = determinePointsController;

    if (point == null) {
      this.point = new AeriusPoint(AeriusPointType.POINT);
      view.setEnabledNextStep(false);
    } else {
      this.point = point;
    }
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.eventBus = eventBus;
    map.setVisibleSubstanceLayers(false);
    panel.setWidget(view);
    view.setPresenter(this);
    view.setName(point.getLabel());

    if (interactionLayer == null) {
      interactionLayer = new MeasureInteractionLayer(map, new PointHandler(), DrawSourceStyleOptions.getDefaultStyleMap(), this);
    }
    if (point.getId() == 0) {
      setData(0, 0, true);
    } else {
      setData(point.getX(), point.getY(), true);
    }
    interactionLayer.activate();
  }

  @Override
  public void onCancel() {
    super.onCancel();
    cleanUp();
  }

  @Override
  public void onStop() {
    super.onStop();
    cleanUp();
  }

  private void cleanUp() {
    interactionLayer.destroy();
  }

  @Override
  public void updatePosition(final double xCoordinate, final double yCoordinate) {
    setData(xCoordinate, yCoordinate, false);
  }

  @Override
  public void finish() {
    point.setLabel(view.getName());

    if (point.getId() == 0) {
      userContext.getScenario().getCalculationPoints().add(point);
    }

    if (point.getLabel().isEmpty()) {
      point.setLabel(M.messages().calculationPoint() + " " + FormatUtil.formatAlphabetical(point.getId()));
    }

    placeController.goTo(new CalculationPointsPlace(place));
  }

  @Override
  public void setLabel(final String label) {
    point.setLabel(label);
  }

  @Override
  public void onMeasure(final MeasureInteractionEvent event) {
    setData(event.getX(), event.getY(), true);
  }

  @Override
  public void onMeasurePartial(final MeasureInteractionEvent measureInteractionEvent) {
    // No-op
  }

  @Override
  public void showDetermineCalculationPointsDialog() {
    determinePointsController.show(place);
  }

  /**
   * Sets the x and y coordinates on the view and updates the marker on the
   * map. If there is no marker yet, because it's a new source a new marker is
   * placed. If it is an existing source the current marker is marked
   * differently.
   *
   * @param newX
   * @param newY
   * @param updateView
   *            if true also update the view content
   */
  private void setData(final double newX, final double newY, final boolean updateView) {
    final int x = MathUtil.round(newX);
    final int y = MathUtil.round(newY);
    point.setX(x);
    point.setY(y);
    point.setId(place.getCPId());
    final boolean hasXY = x > 0 && y > 0;
    view.showDetermineFunctionality(!hasXY);
    if (updateView) {
      view.setXY(x, y);
    }
    if (hasXY) {
      eventBus.fireEvent(new CalculationPointChangeEvent(place.getActiveSituationId(), point, CHANGE.ADD));
    }
    view.setEnabledNextStep(hasXY);
  }
}
