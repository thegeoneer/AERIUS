/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.search;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.shared.service.MapSearchServiceAsync;
import nl.overheid.aerius.wui.geo.search.MapSearchAction;

/**
 * Search actions. Call the server for finding results and perform the action when a user selects a suggestion.
 */
@Singleton
public class CalculatorMapSearchAction extends MapSearchAction {
  @Inject
  public CalculatorMapSearchAction(final EventBus eventBus, final Context context, final MapSearchServiceAsync searchService,
      final FetchGeometryServiceAsync fetchGeometryService) {
    super(eventBus, context.getReceptorGridSettings(), searchService, fetchGeometryService);
  }

  @Override
  protected ProductType getProductType() {
    return ProductType.CALCULATOR;
  }
}
