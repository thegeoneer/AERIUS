/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.calculationpoint;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.calculator.calculationpoint.DetermineCalculationPointsDialogPanel.DetermineReceptorPointsProperties;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.validation.IntegerRangeValidator;

/**
 * Export Dialog content panel with export options.
 */
class DetermineCalculationPointsDialogPanel extends Composite implements Editor<DetermineReceptorPointsProperties> {
  private static final int MIN_VALUE = 1;

  /**
   * Properties for the determine receptor points functionality.
   */
  static class DetermineReceptorPointsProperties {

    private int distanceFromSource;

    public int getDistanceFromSource() {
      return distanceFromSource;
    }

    public void setDistanceFromSource(final int distanceFromSource) {
      this.distanceFromSource = distanceFromSource;
    }
  }

  interface DetermineCalculationPointsDialogDriver extends
  SimpleBeanEditorDriver<DetermineReceptorPointsProperties, DetermineCalculationPointsDialogPanel> { }

  private static DetermineCalculationPointsDialogPanelUiBinder uiBinder = GWT.create(DetermineCalculationPointsDialogPanelUiBinder.class);

  interface DetermineCalculationPointsDialogPanelUiBinder extends UiBinder<Widget, DetermineCalculationPointsDialogPanel> {
  }

  @UiField @Ignore InlineHTML generationResult;
  /**
   * The not so useless calculate button. How it got that name you ask? Well. Before it was just a button, used for show. Always disabled, no
   * functionality attached to it. It was the button this application deserved, but didn't need at that time. But all that has changed.
   */
  @UiField @Ignore Button notSoUselessCalculateButton;
  @UiField(provided = true) IntValueBox distanceFromSource = new IntValueBox(M.messages().determineCalcPointsDistanceFromSources()) {
    @Override
    public void setValue(final Integer value) {
      super.setValue(value == 0 ? null : value);
    }
  };

  public DetermineCalculationPointsDialogPanel(final int maxRange) {
    initWidget(uiBinder.createAndBindUi(this));

    distanceFromSource.ensureDebugId(TestID.DETERMINE_CALCPOINTS_BOUNDARY);
    notSoUselessCalculateButton.ensureDebugId(TestID.DETERMINE_CALCPOINTS_NOTSOUSELESSCALCBUTTON);
    generationResult.ensureDebugId(TestID.DETERMINE_CALCPOINTS_RESULTLABEL);

    distanceFromSource.addValidator(new IntegerRangeValidator(MIN_VALUE, maxRange) {

      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivRangeBetween(M.messages().determineCalcPointsDistanceFromSources(), MIN_VALUE, maxRange);
      }
    });
  }

  public void addClickHandlerOnNotSoUselessCalculateButton(final ClickHandler handler) {
    notSoUselessCalculateButton.addClickHandler(handler);
  }

  public Integer getDistanceFromSource() {
    return distanceFromSource.getValue();
  }

  public void showAdditionalInfo(final boolean show, final int amountOfPoints, final int amountOfAreas) {
    generationResult.setHTML(show ? M.messages().determineCalcPointsResult(amountOfPoints, amountOfAreas) : "");
  }

  public void showWarningNoSource() {
    generationResult.setHTML(M.messages().determineCalcPointsWarningNoSource());
  }

}
