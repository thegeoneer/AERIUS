/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;

@Singleton
public class SourceLocationViewImpl extends Composite implements SourceLocationView {
  private static final int WAIT_FOR_FIRE_UPDATE_COORDINATES = 200; //ms

  private static final LocationViewImplUiBinder UI_BINDER = GWT.create(LocationViewImplUiBinder.class);

  interface LocationViewImplUiBinder extends UiBinder<Widget, SourceLocationViewImpl> {}

  @UiField Button buttonNextStep;
  @UiField Button buttonCancel;
  @UiField RadioButton buttonPoint;
  @UiField RadioButton buttonLine;
  @UiField RadioButton buttonPolygon;
  @UiField RadioButton buttonEdit;
  @UiField FlowPanel xyInputFields;
  @UiField Label explainLabel;
  @UiField IntValueBox xCoordinate;
  @UiField IntValueBox yCoordinate;
  @UiField TextBox multiCoordinates;
  @UiField Label partialUpdates;

  private final Timer keyPressDelayTimer = new Timer() {

    @Override
    public void run() {
      actualOnKeyUpMulti();
    }
  };
  private Presenter presenter;

  private final FocusHandler coordinateFocusHandler = new FocusHandler() {
    @Override
    public void onFocus(final FocusEvent event) {
      if (!buttonPoint.getValue()) {
        buttonPoint.setValue(true, true);
      }
    }
  };

  private Tool tool;

  @Inject
  public SourceLocationViewImpl(final HelpPopupController hpC) {
    initWidget(UI_BINDER.createAndBindUi(this));

    StyleUtil.I.setPlaceHolder(xCoordinate, M.messages().xCoordinate());
    StyleUtil.I.setPlaceHolder(yCoordinate, M.messages().yCoordinate());
    hpC.addWidget(buttonPoint, hpC.tt().ttLocationButtonPoint());
    hpC.addWidget(buttonLine, hpC.tt().ttLocationButtonLine());
    hpC.addWidget(buttonPolygon, hpC.tt().ttLocationButtonPolygon());
    hpC.addWidget(buttonEdit, hpC.tt().ttLocationButtonEditShape());
    hpC.addWidget(xCoordinate, hpC.tt().ttLocationXCoordinate());
    hpC.addWidget(yCoordinate, hpC.tt().ttLocationYCoordinate());
    hpC.addWidget(buttonNextStep, hpC.tt().ttLocationButtonNextStep());
    hpC.addWidget(buttonCancel, hpC.tt().ttLocationButtonCancel());

    xCoordinate.addFocusHandler(coordinateFocusHandler);
    yCoordinate.addFocusHandler(coordinateFocusHandler);
    onEnsureDebugId("");
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
    setEnabledNextStep(false);
  }

  @Override
  public void selectTool(final SourceLocationView.Tool tool) {
    if (tool != Tool.EDIT) {
      this.tool = tool;
    }

    setInputFieldVisibility(tool);
    buttonPoint.setValue(tool == Tool.POINT);
    buttonLine.setValue(tool == Tool.LINE);
    buttonPolygon.setValue(tool == Tool.POLYGON);
    buttonEdit.setValue(tool == Tool.EDIT);
  }

  @UiHandler({ "xCoordinate", "yCoordinate" })
  void onKeyUpXY(final KeyUpEvent event) {
    if (xCoordinate.getValue() == null || yCoordinate.getValue() == null) {
      setEnabledNextStep(false);
    } else {
      presenter.updatePosition(xCoordinate.getValue(), yCoordinate.getValue());
    }
  }

  @UiHandler({ "multiCoordinates" })
  void onKeyUpMulti(final KeyUpEvent event) {
    keyPressDelayTimer.schedule(WAIT_FOR_FIRE_UPDATE_COORDINATES);
  }

  private void actualOnKeyUpMulti() {
    if (multiCoordinates.getValue() == null) {
      setEnabledNextStep(false);
    } else {
      presenter.updatePosition(multiCoordinates.getValue());
    }
  }

  @UiHandler({ "buttonPoint", "buttonLine", "buttonPolygon", "buttonEdit" })
  void onClickTool(final ValueChangeEvent<Boolean> e) {
    invalidInputMultiCoordinates(false);
    if (e.getSource() == buttonPoint) {
      switchTool(SourceLocationView.Tool.POINT);
    } else if (e.getSource() == buttonLine) {
      switchTool(SourceLocationView.Tool.LINE);
    } else if (e.getSource() == buttonPolygon) {
      switchTool(SourceLocationView.Tool.POLYGON);
    } else if (e.getSource() == buttonEdit) {
      switchTool(SourceLocationView.Tool.EDIT);
    }
  }

  private void switchTool(final Tool tool) {
    presenter.onSwitchTool(tool, canTrack(tool));
  }

  private boolean canTrack(final Tool tool) {
    return tool == Tool.LINE || tool == Tool.POLYGON || tool == Tool.EDIT;
  }

  @UiHandler("buttonNextStep")
  void onClickButtonNextStep(final ClickEvent e) {
    invalidInputMultiCoordinates(false);
    presenter.nextStep();
  }

  @UiHandler("buttonCancel")
  void onClickButtonCancel(final ClickEvent e) {
    presenter.cancelEdit();
  }

  @Override
  public void invalidInputMultiCoordinates(final boolean fail) {
    multiCoordinates.setStyleName(R.css().errorInput(), fail);
  }

  @Override
  public void invalidMultiCoordinatesWarning(final boolean fail, final Tool tool, final String errorMessage) {
    // switch to polygon input field, activate state and show error
    if (fail) {
      selectTool(tool);
      setInputFieldVisibility(tool);
      ErrorPopupController.addError(multiCoordinates, errorMessage);
    }
    invalidInputMultiCoordinates(fail);
  }

  @Override
  public void setXY(final double x, final double y) {
    // on zero set null to trigger placeholder texts.
    xCoordinate.setValue(x == 0 ? null : MathUtil.round(x));
    yCoordinate.setValue(y == 0 ? null : MathUtil.round(y));
  }

  @Override
  public void setMultiCoordinates(final String coordinates) {
    multiCoordinates.setText(coordinates);
  }

  @Override
  public void setEnabledNextStep(final boolean nextStep) {
    buttonNextStep.setEnabled(nextStep);
  }

  @Override
  public void setEditMode(final boolean editMode) {
    if (editMode) {
      buttonNextStep.setText(M.messages().save());
    } else {
      buttonNextStep.setText(M.messages().nextStepButton());
    }
  }

  @Override
  public void setEnabledEdit(final boolean enabled) {
    buttonEdit.setEnabled(enabled);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    buttonNextStep.ensureDebugId(TestID.BUTTON_NEXTSTEP);
    buttonCancel.ensureDebugId(TestID.BUTTON_CANCEL);
    buttonPoint.ensureDebugId(TestID.BUTTON_POINTSOURCE);
    buttonLine.ensureDebugId(TestID.BUTTON_LINESOURCE);
    buttonPolygon.ensureDebugId(TestID.BUTTON_POLYGONSOURCE);
    buttonEdit.ensureDebugId(TestID.BUTTON_EDITSHAPESOURCE);
    xCoordinate.ensureDebugId(TestID.INPUT_X_COORDINATE);
    yCoordinate.ensureDebugId(TestID.INPUT_Y_COORDINATE);
    multiCoordinates.ensureDebugId(TestID.INPUT_COORDINATES);
  }

  private void setInputFieldVisibility(final Tool tool) {
    explainLabel.setVisible(true);
    xyInputFields.setVisible(false);
    partialUpdates.setVisible(canTrack(tool));
    multiCoordinates.setVisible(false);
    if (tool == Tool.POINT) {
      xyInputFields.setVisible(true);
    } else if (tool == Tool.EDIT) {
      explainLabel.setVisible(false);
    } else {
      multiCoordinates.setVisible(true);
    }
  }

  @Override
  public void setPartialMeasurement(final double measure) {
    if (tool == Tool.LINE || tool == Tool.POLYGON) {
      partialUpdates.setText(tool == Tool.LINE
          ? M.messages().calculatorToolDistance(FormatUtil.formatDistanceWithUnit(measure))
              : M.messages().calculatorToolSurface(FormatUtil.formatSurfaceWithUnit(measure)));
    }
  }

  @Override
  public String getTitleText() {
    return M.messages().createEmissionSource();
  }
}
