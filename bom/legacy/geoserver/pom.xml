<!--

    Copyright the State of the Netherlands

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.overheid.aerius.bom</groupId>
    <artifactId>bom.aerius.legacy</artifactId>
    <version>2019A</version>
  </parent>

  <artifactId>bom.legacy.geoserver</artifactId>
  <packaging>pom</packaging>

  <name>AERIUS :: BOM :: Legacy :: Geoserver</name>
  <description>The dependencies that are used by the AERIUS-II projects</description>

  <properties>
    <geoserver.version>2.8.5</geoserver.version>
    <geotools.version>14.5</geotools.version>
    <forced.springframework.version>3.1.4.RELEASE</forced.springframework.version>
  </properties>

  <repositories>
    <repository>
      <id>opengeo</id>
      <name>OpenGeo Maven Repository</name>
      <url>http://repo.boundlessgeo.com/main</url>
    </repository>
  </repositories>

  <dependencies>
    <dependency>
      <groupId>nl.overheid.aerius.bom</groupId>
      <artifactId>bom.legacy.logging</artifactId>
      <version>${project.version}</version>
      <type>pom</type>
    </dependency>
    <dependency>
      <groupId>nl.overheid.aerius.bom</groupId>
      <artifactId>bom.legacy.postgresql</artifactId>
      <version>${project.version}</version>
      <type>pom</type>
    </dependency>
    <!-- Change dependencies to specific version, somehow, somewhere a pom breaks everything by mixing Spring versions. Use the proper versions. -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-expression</artifactId>
      <version>${forced.springframework.version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-tx</artifactId>
      <version>${forced.springframework.version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-web</artifactId>
      <version>${forced.springframework.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.web</groupId>
      <artifactId>gs-web-core</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.web</groupId>
      <artifactId>gs-web-wms</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.web</groupId>
      <artifactId>gs-web-gwc</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.web</groupId>
      <artifactId>gs-web-wfs</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.web</groupId>
      <artifactId>gs-web-wcs</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.web</groupId>
      <artifactId>gs-web-demo</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.web</groupId>
      <artifactId>gs-web-sec-jdbc</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.web</groupId>
      <artifactId>gs-web-sec-ldap</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-platform</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-main</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-wcs</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-wcs1_0</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-wcs1_1</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-wcs2_0</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-wfs</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-wms</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-kml</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-gwc</artifactId>
      <version>${geoserver.version}</version>
      <exclusions>
        <exclusion>
          <artifactId>postgresql</artifactId>
          <groupId>postgresql</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-rest</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver</groupId>
      <artifactId>gs-restconfig</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.security</groupId>
      <artifactId>gs-sec-jdbc</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geoserver.security</groupId>
      <artifactId>gs-sec-ldap</artifactId>
      <version>${geoserver.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geotools.jdbc</groupId>
      <artifactId>gt-jdbc-postgis</artifactId>
      <version>${geotools.version}</version>
      <exclusions>
        <exclusion>
          <artifactId>postgresql</artifactId>
          <groupId>postgresql</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.geotools</groupId>
      <artifactId>gt-wfs</artifactId>
      <version>${geotools.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geotools</groupId>
      <artifactId>gt-geotiff</artifactId>
      <version>${geotools.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geotools</groupId>
      <artifactId>gt-arcgrid</artifactId>
      <version>${geotools.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geotools</groupId>
      <artifactId>gt-imagemosaic</artifactId>
      <version>${geotools.version}</version>
    </dependency>
    <dependency>
      <groupId>org.geotools</groupId>
      <artifactId>gt-image</artifactId>
      <version>${geotools.version}</version>
    </dependency>
  </dependencies>

</project>
