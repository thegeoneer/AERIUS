add_build_constants

cluster_tables

run_sql "general.sql"
run_sql "background_cells.sql"

run_sql_folder "system"
run_sql_folder "i18n"

run_sql "test_data.sql"

synchronize_serials

$do_run_unit_tests = true if has_build_flag :test
$do_validate_contents = true if has_build_flag :validate
$do_create_summary = true if has_build_flag :summary
