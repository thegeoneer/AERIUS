/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.checksum;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;

public class ChecksumRepositoryTest extends BaseDBTest {

  private static final long CHECKSUM_RECEPTORS = 1166717344798L;

  @Test
  public void testGetApplicableTables() throws SQLException {
    final ArrayList<String> excludeTables = new ArrayList<>(Arrays.asList("public.receptors"));
    final List<String> tables = ChecksumRepository.getApplicableTables(getCalcConnection(), excludeTables);
    assertNotNull("Applicable tables list shouldn't be null", tables);
    assertFalse("Applicable tables list shouldn't be empty", tables.isEmpty());
    assertNotEquals("Missing expected table", -1, tables.indexOf("public.hexagons"));
    assertEquals("Exclude filter does not work", -1, tables.indexOf("public.receptors"));
  }

  @Test
  public void testGetTablesContentChecksum() throws SQLException {
    // Build exclude table list that excludes everything except 3 tables -- otherwise unit test is too slow
    final List<String> excludeTables = ChecksumRepository.getApplicableTables(getCalcConnection(), new ArrayList<String>());
    excludeTables.remove("public.receptors");
    excludeTables.remove("system.options");

    final Map<String, Long> checksums = ChecksumRepository.getTablesContentChecksum(getCalcConnection(), excludeTables);
    assertNotNull("Checksums map shouldn't be null", checksums);
    assertEquals("Exclude list should have excluded all but 2 tables", 2, checksums.size());

    // Test receptors because it is unlikely to change contents - and if it does it's under our own control.
    assertEquals("Unexpected checksum of receptors", CHECKSUM_RECEPTORS, checksums.get("public.receptors").longValue());
    assertNotEquals("Checksum of receptors table should not be 0", 0L, checksums.get("public.receptors").longValue());
    assertEquals("Checksum of empty table should be 0", 0L, checksums.get("system.options").longValue());
  }

  @Test
  public void testGetTableContentChecksum() throws SQLException {
    // Test receptors because it is unlikely to change contents
    final long checksum = ChecksumRepository.getTableContentChecksum(getCalcConnection(), "public.receptors");
    assertEquals("Unexpected checksum of receptors", CHECKSUM_RECEPTORS, checksum);
  }

  @Test
  public void testGetDatabaseStructureChecksum() throws SQLException {
    final long checksum = ChecksumRepository.getDatabaseStructureChecksum(getCalcConnection());
    assertNotEquals("Checksum of structure should not be 0", 0L, checksum);
  }

}
