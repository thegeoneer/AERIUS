/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.register.AuditTrailItem;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public class PermitModifyRepositoryTest extends RequestTestBase {

  private static final String UPDATE_ALL_PERMIT_STATUS =
      "UPDATE requests SET status = 'assigned' "
          + " FROM permits WHERE permits.request_id = requests.request_id "
          + " AND segment = 'projects' "
          + " AND (now()::date - received_date::date) >= ae_constant('PERMIT_RECEIVED_DATE_TERM')::integer";

  @Test
  public void testInsertNewPermit() throws SQLException, AeriusException {
    final Permit permit = getExamplePermit();
    assertEquals("Permit ID before inserting", 0, permit.getId());
    assertNull("Permit state before inserting", permit.getRequestState());
    assertEquals("Permit audit trail", 0, permit.getChangeHistory().size());
    PermitModifyRepository.insertNewPermit(getRegConnection(), permit, getExampleUserProfile(), getFakeInsertRequestFile());
    assertNotEquals("Permit ID after inserting", 0, permit.getId());

    final Permit permitInDB = PermitRepository.getPermitByKey(getRegConnection(), permit.getPermitKey());
    assertPermitAfterInsert(permitInDB);

    try {
      PermitModifyRepository.insertNewPermit(getRegConnection(), permit, getExampleUserProfile(), getFakeInsertRequestFile());
      fail("Should be an exception when trying to insert the same permit twice.");
    } catch (final AeriusException e) {
      assertEquals("Reason", Reason.REQUEST_ALREADY_EXISTS, e.getReason());
    }
  }

  @Test
  public void testPermitExists() throws SQLException, AeriusException {
    assertFalse("Null permit key", PermitRepository.permitExists(getRegConnection(), null));
    assertFalse("New permit key", PermitRepository.permitExists(getRegConnection(), new PermitKey(null, null)));
    assertFalse("Unknown permit key", PermitRepository.permitExists(getRegConnection(), new PermitKey("Bladi", "Bla")));
    final Permit permit = getInsertedExamplePermit();
    assertTrue("Unknown permit key", PermitRepository.permitExists(getRegConnection(), permit.getPermitKey()));
  }

  @Test
  public void testInsertPermitCalculation() throws SQLException, AeriusException {
    final Permit permit = getInsertedExamplePermit();

    final Calculation calculation = getExampleCalculation();
    final int calculationId = calculation.getCalculationId();
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, calculationId, permit);
    try {
      RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, calculationId, permit);
      fail("SQL exception expected.");
    } catch (final SQLException e) {
      //expected this one, can't duplicate the calculation in DB.
    }
  }

  @Test
  public void testUpdatePermit() throws SQLException, AeriusException {
    Permit permit = getInsertedExamplePermit();
    //retrieve permit again for the modified date
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    final PermitKey originalKey = permit.getPermitKey();
    final String originalReference = permit.getReference();
    final String newReference = UUID.randomUUID().toString();
    final RequestState originalState = permit.getRequestState();
    permit.getDossierMetaData().setDossierId(newReference);
    permit.setRequestState(RequestState.PENDING_WITH_SPACE);
    permit.getScenarioMetaData().setReference(newReference);
    permit.getScenarioMetaData().setCorporation("MegaCorp");
    permit.getScenarioMetaData().setProjectName("Cop de Robo");
    permit.getDossierMetaData().setReceivedDate(new GregorianCalendar(2001, 1, 5).getTime());
    permit.getDossierMetaData().setHandler(getExampleUserProfile());
    permit.getDossierMetaData().setRemarks("He won't be back...");
    PermitModifyRepository.updatePermit(getRegConnection(), originalKey, permit.getDossierMetaData(), getExampleUserProfile(),
        permit.getLastModified());
    final Permit permitFromDB = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertPermitComparison(permit, permitFromDB);
    //some things are not updated by the updatePermit calls (PDF stuff and state)
    assertNotEquals("Current state", permit.getRequestState(), permitFromDB.getRequestState());
    assertNotEquals("PDF reference", newReference, permitFromDB.getReference());
    assertNotEquals("Corporation", permit.getScenarioMetaData().getCorporation(), permitFromDB.getScenarioMetaData().getCorporation());
    assertNotEquals("Project name", permit.getScenarioMetaData().getProjectName(), permitFromDB.getScenarioMetaData().getProjectName());
    assertEquals("Current state", originalState, permitFromDB.getRequestState());
    assertEquals("DossierId", newReference, permitFromDB.getDossierMetaData().getDossierId());
    assertEquals("Received date", permit.getDossierMetaData().getReceivedDate(), permitFromDB.getDossierMetaData().getReceivedDate());
    assertEquals("PDF reference", originalReference, permitFromDB.getReference());
    assertEquals("Handler ID", permit.getDossierMetaData().getHandler().getId(), permitFromDB.getDossierMetaData().getHandler().getId());
    assertEquals("Number of audits", 2, permitFromDB.getChangeHistory().size());
  }

  @Test
  public void testPermitSwitchHandler() throws SQLException, AeriusException {
    //tests if an permit changes user within same authority without changing anything else works.
    Permit permit = getInsertedExamplePermit();
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    permit.getDossierMetaData().setHandler(getExampleUserProfile());
    PermitModifyRepository.updatePermit(getRegConnection(), permit.getPermitKey(), permit.getDossierMetaData(), getExampleUserProfile(),
        permit.getLastModified());
    final Permit permitFromDB = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertEquals("Handler", permit.getDossierMetaData().getHandler().getName(), permitFromDB.getDossierMetaData().getHandler().getName());
  }

  @Test
  public void testUpdatePermitWithExistingDossierID() throws SQLException, AeriusException {
    Permit permit = getInsertedExamplePermit();
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    final Permit permit2 = getInsertedExamplePermit();

    final PermitKey permitKey = permit.getPermitKey();
    permit.getDossierMetaData().setDossierId(permit2.getDossierMetaData().getDossierId());

    try {
      PermitModifyRepository.updatePermit(getRegConnection(), permitKey, permit.getDossierMetaData(), getExampleUserProfile(),
          permit.getLastModified());
      fail("Expected an error");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception", Reason.REQUEST_ALREADY_EXISTS, e.getReason());
    }
  }

  @Test
  public void testUpdatePermitAfterOtherUserUpdated() throws SQLException, AeriusException {
    Permit permit = getInsertedExamplePermit();
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    final Permit permit2 = PermitRepository.getPermit(getRegConnection(), permit.getId());

    //this change doesn't actually change anything in the database... Even if nothing is updated, the call to updatePermit will cause lastmodified to be updated.
    permit.getScenarioMetaData().setCorporation("MegaCorp");
    permit2.getScenarioMetaData().setCorporation("MegaCorp2");

    PermitModifyRepository.updatePermit(getRegConnection(), permit.getPermitKey(), permit.getDossierMetaData(), getExampleUserProfile(),
        permit.getLastModified());

    try {
      PermitModifyRepository.updatePermit(getRegConnection(), permit2.getPermitKey(), permit2.getDossierMetaData(), getExampleUserProfile(),
          permit2.getLastModified());
      fail("Expected an error");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception", Reason.PERMIT_ALREADY_UPDATED, e.getReason());
    }
  }

  @Test(expected = AeriusException.class)
  public void testUpdatePermitBeforeInsert() throws SQLException, AeriusException {
    final Permit permit = getExamplePermit();
    PermitModifyRepository.updatePermit(getRegConnection(), permit.getPermitKey(), permit.getDossierMetaData(), getExampleUserProfile(),
        new Date().getTime());
  }

  @Test
  public void testDeletePermit() throws SQLException, AeriusException {
    final Permit permit = getInsertedExamplePermit();
    PermitModifyRepository.deletePermit(getRegConnection(), permit, getExampleUserProfile());
    assertEquals("ID after deletion", 0, permit.getId());
    final PermitKey permitKeyFromDB = PermitRepository.getPermitKey(getRegConnection(), permit.getReference());
    assertNull("Deleted permit from DB by PDF reference", permitKeyFromDB);
    final boolean exists = PermitRepository.permitExists(getRegConnection(), permit.getPermitKey());
    assertFalse("Deleted permit from DB by key", exists);
  }

  @Test
  public void testDoublePermits() throws SQLException, AeriusException {
    final Permit permit = getInsertedExamplePermit();
    final int originalId = permit.getId();
    try {
      PermitModifyRepository.insertNewPermit(getRegConnection(), permit, getExampleUserProfile(), getFakeInsertRequestFile());
      fail("Expected an error");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception", Reason.REQUEST_ALREADY_EXISTS, e.getReason());
    }
    PermitModifyRepository.deletePermit(getRegConnection(), permit, getExampleUserProfile());
    assertEquals("ID of permit after deleting", 0, permit.getId());
    PermitModifyRepository.insertNewPermit(getRegConnection(), permit, getExampleUserProfile(), getFakeInsertRequestFile());
    final Permit permitFromDB = PermitRepository.getSkinnedPermitByKey(getRegConnection(), permit.getPermitKey());
    assertNotEquals("ID of permit after deleting", originalId, permitFromDB.getId());
  }

  @Test
  public void testDoubleDossierPermits() throws SQLException, AeriusException {
    final Permit originalPermit = getInsertedExamplePermit();
    final int originalId = originalPermit.getId();
    final Permit duplicateDossierPermit = getExamplePermit();
    duplicateDossierPermit.getDossierMetaData().setDossierId(originalPermit.getDossierMetaData().getDossierId());
    try {
      PermitModifyRepository.insertNewPermit(getRegConnection(), duplicateDossierPermit, getExampleUserProfile(), getFakeInsertRequestFile());
      fail("Expected an error");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception", Reason.REQUEST_ALREADY_EXISTS, e.getReason());
    }
    PermitModifyRepository.deletePermit(getRegConnection(), originalPermit, getExampleUserProfile());
    assertEquals("ID of permit after deleting", 0, originalPermit.getId());
    PermitModifyRepository.insertNewPermit(getRegConnection(), duplicateDossierPermit, getExampleUserProfile(), getFakeInsertRequestFile());
    final Permit permitFromDB = PermitRepository.getSkinnedPermitByKey(getRegConnection(), duplicateDossierPermit.getPermitKey());
    assertNotEquals("ID of permit after deleting", originalId, permitFromDB.getId());
  }

  @Test(expected = AeriusException.class)
  public void testDeletePermitUnknown() throws SQLException, AeriusException {
    final Permit permit = getExamplePermit();
    PermitModifyRepository.deletePermit(getRegConnection(), permit, getExampleUserProfile());
  }

  @Test
  public void testUpdateState() throws SQLException, AeriusException {
    Permit permit = getPermitWithCalculation();
    assertEquals("Permit state in DB", RequestState.INITIAL, permit.getRequestState());

    updateState(permit, RequestState.QUEUED);
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertEquals("Permit state in DB", RequestState.QUEUED, permit.getRequestState());

    updateState(permit, RequestState.PENDING_WITH_SPACE);
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertEquals("Permit state in DB", RequestState.PENDING_WITH_SPACE, permit.getRequestState());
  }

  @Test
  public void testUpdateStateWithEdits() throws SQLException, AeriusException {
    final Permit permit = getPermitWithCalculation();

    assertEquals("Permit state in DB", RequestState.INITIAL, permit.getRequestState());
    permit.getDossierMetaData().setRemarks("SomeOtherRemark");
    PermitModifyRepository.updatePermit(getRegConnection(), permit.getPermitKey(), permit.getDossierMetaData(),
        getExampleUserProfile(), permit.getLastModified());
    try {
      updateState(permit, RequestState.ASSIGNED);
      fail("Expected a modification exception");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception", Reason.PERMIT_ALREADY_UPDATED, e.getReason());
    }
  }

  @Test(expected = AeriusException.class)
  public void testUpdateStateFailure() throws SQLException, AeriusException {
    //DB currently does not allow updating the state to assigned right away.
    //the exception returned probably isn't a good one however...
    Permit permit = getInsertedExamplePermit();
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());

    assertEquals("Permit state in DB", RequestState.INITIAL, permit.getRequestState());

    PermitModifyRepository.updateState(getRegConnection(), permit.getPermitKey(), RequestState.ASSIGNED, getLowLevelExampleUserProfile(),
        permit.getLastModified());
  }

  @Test(expected = AeriusException.class)
  public void testUpdateStateUnknownPermit() throws SQLException, AeriusException {
    PermitModifyRepository.updateState(getRegConnection(), new PermitKey("NotADossierId", "NotACode"), RequestState.ASSIGNED,
        getExampleUserProfile(), new Date().getTime());
  }

  @Test
  public void testDequeuePermits() throws SQLException, AeriusException {
    //update all permits that would be dequeued to something that won't trigger them to be dequeued... (saves some time)
    try (final Connection con = getRegConnection();
        final PreparedStatement ps = con.prepareStatement(UPDATE_ALL_PERMIT_STATUS)) {
      ps.executeUpdate();
    }
    clearPermitsAndNoticesDevelopmentSpaces();

    //insert permit that should stay on queue.
    Permit queuedPermit = getDequeueingPermit(1);

    //insert permit that should be dequeued
    Permit dequeuablePermit = getDequeueingPermit(60);

    //insert permit that shouldn't change state.
    Permit pendingPermit = getDequeueingPermit(62);

    //refresh so we can update to pending_with_space
    pendingPermit = PermitRepository.getPermit(getRegConnection(), pendingPermit.getId());
    updateState(pendingPermit, RequestState.PENDING_WITH_SPACE);

    //finally, try to dequeue
    PermitModifyRepository.dequeuePermits(getRegConnection(), getExampleUserProfile());

    //now check what this has done to our permits
    queuedPermit = PermitRepository.getPermit(getRegConnection(), queuedPermit.getId());
    assertEquals("Request state shouldn't change",
        RequestState.QUEUED, queuedPermit.getRequestState());

    dequeuablePermit = PermitRepository.getPermit(getRegConnection(), dequeuablePermit.getId());
    assertTrue("Request state wasn't pending but: " + dequeuablePermit.getRequestState(),
        dequeuablePermit.getRequestState() == RequestState.PENDING_WITH_SPACE
            || dequeuablePermit.getRequestState() == RequestState.PENDING_WITHOUT_SPACE);
    checkStateChangeAudit(dequeuablePermit.getChangeHistory().get(0), RequestState.QUEUED, dequeuablePermit.getRequestState());

    pendingPermit = PermitRepository.getPermit(getRegConnection(), pendingPermit.getId());
    assertEquals("Request state should stay the same", RequestState.PENDING_WITH_SPACE, pendingPermit.getRequestState());
    //check for duplicate audits (should be initial -> queued -> pending, not initial -> queued -> pending -> pending...
    checkStateChangeAudit(dequeuablePermit.getChangeHistory().get(0), RequestState.QUEUED, RequestState.PENDING_WITH_SPACE);
    checkStateChangeAudit(dequeuablePermit.getChangeHistory().get(1), RequestState.INITIAL, RequestState.QUEUED);
  }

  @Test
  public void testUpdateStateDecreeRemovalAssignedToAssignedFinal() throws SQLException, AeriusException {
    updateStateDecreeRemoval(RequestState.ASSIGNED, RequestState.ASSIGNED_FINAL, true);
  }


  @Test
  public void testUpdateStateDecreeRemovalRejectedWithSpaceToQueued() throws SQLException, AeriusException {
    updateStateDecreeRemoval(RequestState.REJECTED_WITHOUT_SPACE, RequestState.QUEUED, false);
  }

  private Permit getDequeueingPermit(final int daysAgo) throws SQLException, AeriusException {
    Permit permit = getPermitWithCalculation();

    final Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, -daysAgo);
    permit.getDossierMetaData().setReceivedDate(calendar.getTime());

    PermitModifyRepository.updatePermit(getRegConnection(), permit.getPermitKey(), permit.getDossierMetaData(),
        getExampleUserProfile(), permit.getLastModified());
    //refresh last modified so we can switch state
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    updateState(permit, RequestState.QUEUED);
    return permit;
  }

  private void checkStateChangeAudit(final AuditTrailItem item, final RequestState from, final RequestState to) {
    assertEquals("1 item in change", 1, item.getItems().size());
    assertEquals("State should be changed", AuditTrailType.STATE, item.getItems().get(0).getType());
    assertEquals("State should be changed from", from.getDbValue(), item.getItems().get(0).getOldValue());
    assertEquals("State should be changed to", to.getDbValue(), item.getItems().get(0).getNewValue());
  }

  private void updateState(final Permit permit, final RequestState state) throws AeriusException, SQLException {
    PermitModifyRepository.updateState(getRegConnection(), permit.getPermitKey(), state, getExampleUserProfile(), permit.getLastModified());
  }

  private void updateStateDecreeRemoval(final RequestState fromState, final RequestState toState, final boolean decreesPresentAtEnd)
      throws SQLException, AeriusException {
    Permit permit = getPermitWithCalculation();
    updateState(permit, RequestState.QUEUED);

    switch (fromState) {
    case ASSIGNED:
    case ASSIGNED_FINAL:
      updateState(permit, RequestState.PENDING_WITH_SPACE);
      break;
    case REJECTED_WITHOUT_SPACE:
      updateState(permit, RequestState.PENDING_WITHOUT_SPACE);
      break;
    default:
    }

    if (fromState == RequestState.ASSIGNED_FINAL) {
      updateState(permit, RequestState.ASSIGNED);
    }

    updateState(permit, fromState);

    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());

    // insert decree files
    final InsertRequestFile insertRequestFile = getFakeInsertRequestFile();
    insertRequestFile.setRequestFileType(RequestFileType.DECREE);
    RequestModifyRepository.insertFileContent(getRegConnection(), permit.getId(), insertRequestFile);
    insertRequestFile.setRequestFileType(RequestFileType.DETAIL_DECREE);
    RequestModifyRepository.insertFileContent(getRegConnection(), permit.getId(), insertRequestFile);

    updateState(permit, toState);

    final boolean hasAnyDecreeFiles =
        RequestRepository.getSpecificRequestFile(getRegConnection(), permit.getReference(), RequestFileType.DECREE) != null
        || RequestRepository.getSpecificRequestFile(getRegConnection(), permit.getReference(), RequestFileType.DETAIL_DECREE) != null;

    if (decreesPresentAtEnd) {
      assertEquals("Expected decrees but were not present when changing state from " + fromState + " to " + toState,
          decreesPresentAtEnd, hasAnyDecreeFiles);
    } else {
      assertEquals("Didn't expect decrees but were present when changing state from " + fromState + " to " + toState,
          decreesPresentAtEnd, hasAnyDecreeFiles);
    }
  }

}
