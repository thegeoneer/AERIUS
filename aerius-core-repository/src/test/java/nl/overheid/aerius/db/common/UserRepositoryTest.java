/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.admin.UserManagementFilter;
import nl.overheid.aerius.shared.domain.auth.AdminPermission;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.domain.user.UserRole;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link UserRepository}.
 */
public class UserRepositoryTest extends BaseDBTest {

  private static final String TEST_USER_IN_USER = "test";
  private static final String TEST_USERNAME = "UnitTester1"; // This user has no roles
  private static final String TEST_PASSWORD = "Tested";
  private static final String TEST_EMAIL = "No_email@test.com";
  private static final String TEST2_USERNAME = "UnitTester2";
  private static final String TEST2_PASSWORD = "Tested2";
  private static final String TEST2_EMAIL = "No_email2@test.com";
  private static final String QUERY_ROLE_ADD = "INSERT INTO users_userroles(user_id, userrole_id) VALUES (?, ae_get_userrole_id(?))";
  private static final int DEFAULT_USERFILTER_SIZE = 14;

  @Before
  public void initSharedData() throws Exception {
    // Don't put connection in try to auto close, otherwise insert will be rolled back and user won't exist before test starts.
    // Closing will be done in cleanup of super class.
    final Connection connection = getRegConnection();
    try {
      UserRepository.createUser(connection, getExampleAdminProfile(TEST_USERNAME, TEST_EMAIL), TEST_PASSWORD);

      final int userId = UserRepository.createUser(connection, getExampleAdminProfile(TEST2_USERNAME, TEST2_EMAIL), TEST2_PASSWORD);
      // If user exists, next queries won't be fired (which would cause a SQL exception otherwise)
      final PreparedStatement stmtRole = connection.prepareStatement(QUERY_ROLE_ADD);
      stmtRole.setInt(1, userId);
      stmtRole.setString(2, TestDomain.USERROLE_REGISTER_VIEWER);

      stmtRole.execute();
    } catch (final AeriusException e) {
      // should be thrown when already exists (unit test has been used before).
      if (e.getReason() != Reason.USER_ALREADY_EXISTS) {
        throw e;
      }
    }
  }

  @Test
  public void testCreateUser() throws SQLException, AeriusException {
    final String username = UUID.randomUUID().toString();
    final String email = UUID.randomUUID().toString();
    final int createdUserId = UserRepository.createUser(getRegConnection(), getExampleAdminProfile(username, email), TEST_PASSWORD);
    assertNotEquals("created user ID", 0, createdUserId);
    try {
      UserRepository.createUser(getRegConnection(), getExampleAdminProfile(username, "Other_email@test.com"), TEST_PASSWORD);
      fail("Should have thrown exception when trying to save another user with same username");
    } catch (final AeriusException e) {
      assertEquals("Reason", Reason.USER_ALREADY_EXISTS, e.getReason());
    }
    try {
      UserRepository.createUser(getRegConnection(), getExampleAdminProfile("Other", email), TEST_PASSWORD);
      fail("Should have thrown exception when trying to save another user with same email");
    } catch (final AeriusException e) {
      assertEquals("Reason", Reason.USER_ALREADY_EXISTS, e.getReason());
    }
  }

  @Test
  public void testGetUserProfileByName() throws SQLException, AeriusException {
    final UserProfile userProfile = UserRepository.getUserProfileByName(getRegConnection(), TEST_USERNAME);
    assertNotEquals("User Id", 0, userProfile.getId());
    testUserProfile("By name", userProfile);

    try {
      UserRepository.getUserProfileByName(getRegConnection(), "Unknown");
      fail("Userprofile with unknown username");
    } catch (final AeriusException e) {
      assertSame("Userprofile with unknown username", Reason.USER_DOES_NOT_EXIST, e.getReason());
    }
    try {
      UserRepository.getUserProfileByName(getRegConnection(), null);
      fail("Userprofile with null username");
    } catch (final AeriusException e) {
      assertSame("Userprofile with null username", Reason.USER_DOES_NOT_EXIST, e.getReason());
    }
  }

  @Test
  public void testGetUserProfile() throws SQLException, AeriusException {
    final UserProfile userProfile = UserRepository.getUserProfileByName(getRegConnection(), TEST_USERNAME);
    final UserProfile userProfileById = UserRepository.getUserProfile(getRegConnection(), userProfile.getId());
    assertEquals("User Id", userProfile.getId(), userProfileById.getId());
    testUserProfile("By ID", userProfileById);

    final UserProfile userProfileUnknown = UserRepository.getUserProfile(getRegConnection(), Integer.MAX_VALUE);
    assertNull("Userprofile with bogus ID", userProfileUnknown);
  }

  private static void testUserProfile(final String testDescription, final UserProfile userProfile) {
    if (userProfile instanceof AdminUserProfile) {
      assertTrue(testDescription + ": Enabled", ((AdminUserProfile) userProfile).isEnabled());
    }

    assertEquals(testDescription + ": Name", TEST_USERNAME, userProfile.getName());
    assertEquals(testDescription + ": Email", TEST_EMAIL.toLowerCase(), userProfile.getEmailAddress());
    assertTrue(testDescription + ": Permissions should be empty by default", userProfile.getPermissions().isEmpty());
    assertTrue(testDescription + ": Permissions by ID should be empty by default", userProfile.getPermissions().isEmpty());
    assertNotNull(testDescription + ": Userprofile shouldn't have a null authority", userProfile.getAuthority());
  }

  @Test
  public void testUpdateUserProfile() throws SQLException, AeriusException {
    final AdminUserProfile userProfile = UserRepository.getAdminUserProfileByName(getRegConnection(), TEST_USERNAME);
    testUserProfile("Ensuring userprofile", userProfile);

    userProfile.setEnabled(false);
    final String testEmail = "updated@example.com";
    userProfile.setEmailAddress(testEmail);
    final String testInitials = "updatedInitials";
    userProfile.setInitials(testInitials);
    final String testFirstName = "updatedFirstName";
    userProfile.setFirstName(testFirstName);
    final String testLastName = "updatedLastName";
    userProfile.setLastName(testLastName);
    final String testName = "updatedUsername";
    userProfile.setName(testName);
    final Authority changedAuthority = new Authority();
    changedAuthority.setId(2);
    userProfile.setAuthority(changedAuthority);
    userProfile.getRoles().add(new UserRole(2, null, null));
    UserRepository.updateUser(getRegConnection(), userProfile);

    final AdminUserProfile userProfileFromDB = UserRepository.getAdminUserProfile(getRegConnection(), userProfile.getId());
    assertNotNull("Updated userprofile", userProfileFromDB);
    assertFalse("User should be disabled", userProfileFromDB.isEnabled());
    assertEquals("Email address", testEmail, userProfileFromDB.getEmailAddress());
    assertEquals("Initials", testInitials, userProfileFromDB.getInitials());
    assertEquals("First name", testFirstName, userProfileFromDB.getFirstName());
    assertEquals("Last name", testLastName, userProfileFromDB.getLastName());
    assertEquals("username (not updateable)", TEST_USERNAME, userProfileFromDB.getName());
    assertEquals("Authority ID", 2, userProfileFromDB.getAuthority().getId());
    assertEquals("Roles", 1, userProfileFromDB.getRoles().size());
    for (final UserRole role : userProfileFromDB.getRoles()) {
      assertEquals("Role ID", 2, role.getId());
    }
  }

  @Test
  public void testUpdateUserProfileFail() throws SQLException, AeriusException {
    final AdminUserProfile userProfile = UserRepository.getAdminUserProfileByName(getRegConnection(), TEST_USERNAME);
    testUserProfile("Ensuring userprofile", userProfile);

    userProfile.getRoles().add(new UserRole(1, null, null));
    UserRepository.updateUser(getRegConnection(), userProfile);

    final String testEmail = "updated@example.com";
    userProfile.setEmailAddress(testEmail);
    userProfile.getRoles().add(new UserRole(Integer.MAX_VALUE, null, null));
    try {
      UserRepository.updateUser(getRegConnection(), userProfile);
      fail("Expected an exception at this point");
    } catch (final SQLException e) {
      assertEquals("Reason for exception", "23503", e.getSQLState());
    }

    final AdminUserProfile userProfileFromDB = UserRepository.getAdminUserProfile(getRegConnection(), userProfile.getId());
    assertNotNull("Updated userprofile", userProfileFromDB);
    assertEquals("Email address", TEST_EMAIL.toLowerCase(), userProfileFromDB.getEmailAddress());
    assertEquals("Authority ID", 1, userProfileFromDB.getAuthority().getId());
    // existing roles shouldn't be removed.
    assertEquals("Roles", 1, userProfileFromDB.getRoles().size());
    for (final UserRole role : userProfileFromDB.getRoles()) {
      assertEquals("Role ID", 1, role.getId());
    }
  }

  @Test
  public void testDeleteUserProfile() throws SQLException, AeriusException {
    final String username = UUID.randomUUID().toString();
    final String email = UUID.randomUUID().toString();
    final AdminUserProfile userProfile = getExampleAdminProfile(username, email);
    final int createdUserId = UserRepository.createUser(getRegConnection(), userProfile, TEST_PASSWORD);
    userProfile.setId(createdUserId);

    UserProfile fromDB = UserRepository.getUserProfile(getRegConnection(), createdUserId);
    assertNotNull("User profile should exist", fromDB);
    UserRepository.deleteUser(getRegConnection(), userProfile);
    fromDB = UserRepository.getAdminUserProfile(getRegConnection(), createdUserId);
    assertNull("User profile shouldn't exist anymore", fromDB);

    try {
      UserRepository.deleteUser(getRegConnection(), userProfile);
      fail("expected exception at this point");
    } catch (final AeriusException e) {
      assertEquals("Reason", Reason.USER_DOES_NOT_EXIST, e.getReason());
    }
  }

  @Test
  public void testDeleteInUseUserProfile() throws SQLException, AeriusException {
    final AdminUserProfile userProfile = UserRepository.getAdminUserProfileByName(getRegConnection(), TEST_USER_IN_USER);
    assertNotNull("User profile should exist", userProfile);

    try {
      UserRepository.deleteUser(getRegConnection(), userProfile);
      fail("expected exception at this point");
    } catch (final AeriusException e) {
      assertEquals("Reason", Reason.USER_CANNOT_BE_DELETED, e.getReason());
    }
  }

  @Test
  public void testGetUserProfileWithPerms() throws SQLException, AeriusException {
    final AdminUserProfile userProfile = UserRepository.getAdminUserProfileByName(getRegConnection(), TEST2_USERNAME);
    assertFalse("At least one role should be present for this user", userProfile.getRoles().isEmpty());
    for (final UserRole role : userProfile.getRoles()) {
      assertNotEquals("Userrole id", 0, role.getId());
      assertNotNull("Userrole name", role.getName());
      assertNotNull("Userrole color", role.getColor());
    }
    assertFalse("At least one permission should be present for this user", userProfile.getPermissions().isEmpty());
  }

  @Test
  public void testGetAuthorityUserMap() throws SQLException {
    final Map<Authority, ArrayList<UserProfile>> returnedMap = UserRepository.getAuthorityUserMap(getRegConnection());
    assertFalse("Returned authority map shouldn't be empty.", returnedMap.isEmpty());
    boolean foundUsers = false;
    for (final Entry<Authority, ArrayList<UserProfile>> entry : returnedMap.entrySet()) {
      foundUsers = foundUsers || !entry.getValue().isEmpty();
      for (final UserProfile userProfile : entry.getValue()) {
        assertNotNull("User username", userProfile.getName());
        assertFalse("User username shouldn't be empty", userProfile.getName().isEmpty());
        assertNotNull("User first name", userProfile.getFirstName());
        assertNotNull("User last name", userProfile.getLastName());
        assertNotNull("User authority", userProfile.getAuthority());
        assertEquals("User authority should be equal to key", entry.getKey(), userProfile.getAuthority());
      }
    }
    assertTrue("Should be at least some users in the lists for authorities", foundUsers);
  }

  @Test
  public void testGetRoles() throws SQLException {
    final HashSet<AdminUserRole> roles = UserRepository.getRolesAndPermissions(getRegConnection());

    assertNotNull("Returned list shouldn't be null", roles);
    assertFalse("Returned list shouldn't be empty", roles.isEmpty());
  }

  @Test
  public void testGetUsersEmptyFilter() throws SQLException {
    assertFilterResults(new UserManagementFilter(), DEFAULT_USERFILTER_SIZE);
  }

  @Test
  public void testGetUsersDefault() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    filter.fillDefault();
    assertFilterResults(new UserManagementFilter(), DEFAULT_USERFILTER_SIZE);
  }

  @Test
  public void testGetUsersDisabled() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    filter.setEnabled(false);
    assertFilterResults(filter, 0);
  }

  @Test
  public void testGetUsersEnabled() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    filter.setEnabled(true);
    assertFilterResults(filter, DEFAULT_USERFILTER_SIZE);
  }

  @Test
  public void testGetUsersPartialName() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    filter.setUsername("ittes");
    assertFilterResults(filter, 2);
  }

  /**
   * lastname is username backwards for example user: UnitTester -> retseTtinU
   */
  @Test
  public void testGetUsersUserName() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    filter.setUsername("setti");
    assertFilterResults(filter, 2);
  }

  @Test
  public void testGetUsersGarbage() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    filter.setUsername("123$$8k");
    assertFilterResults(filter, 0);
  }

  @Test
  public void testGetUsersAuthority() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    final Authority testAuthority = exampleAuthorities(filter);
    final List<AdminUserProfile> users = assertFilterResultsSmaller(filter, 4);
    for (final UserProfile user : users) {
      assertEquals("Authority ID for user", testAuthority.getId(), user.getAuthority().getId());
    }
  }

  @Test
  public void testGetUsersUnknownAuthorities() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    exampleUnknownAutorities(filter);
    assertFilterResults(filter, 0);
  }

  @Test
  public void testGetUsersRoles() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    exampleRoles(filter);
    assertFilterResultsSmaller(filter, 3);
  }

  @Test
  public void testGetUsersUnknownRole() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    final HashSet<AdminUserRole> unknownRoles = new HashSet<>();
    unknownRoles.add(new AdminUserRole(0, null, null));
    filter.setRoles(unknownRoles);
    assertFilterResults(filter, 0);
  }

  @Test
  public void testGetUsersNoRoles() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    final HashSet<AdminUserRole> noRoles = new HashSet<>();
    filter.setRoles(noRoles);
    assertFilterResults(filter, DEFAULT_USERFILTER_SIZE);
  }

  /**
   * Just the 1 should be returned: TEST2_USERNAME. The other one doesn't have any roles.
   */
  @Test
  public void testGetUsersEverything() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    filter.setUsername("ittes");
    exampleAuthorities(filter);
    exampleRoles(filter);
    //
    assertFilterResults(filter, 1);
  }

  /**
   * Everything with unknown authority.
   */
  @Test
  public void testGetUsersEverythingUnAuth() throws SQLException {
    final UserManagementFilter filter = new UserManagementFilter();
    filter.setUsername("ittes");
    exampleUnknownAutorities(filter);
    exampleRoles(filter);
    assertFilterResults(filter, 0);
  }

  private Authority exampleAuthorities(final UserManagementFilter filter) {
    final HashSet<Authority> authorities = new HashSet<>();
    final Authority testAuthority = getExampleAuthority();
    authorities.add(testAuthority);
    filter.setOrganisations(authorities);
    return testAuthority;
  }

  private void exampleUnknownAutorities(final UserManagementFilter filter) {
    final HashSet<Authority> unknownAuthorities = new HashSet<>();
    final Authority unknownAuthority = new Authority();
    unknownAuthority.setId(-1);
    unknownAuthorities.add(unknownAuthority);
    filter.setOrganisations(unknownAuthorities);
  }

  private void exampleRoles(final UserManagementFilter filter) {
    final HashSet<AdminUserRole> roles = new HashSet<>();
    roles.add(new AdminUserRole(1, null, null));
    filter.setRoles(roles);
  }

  private List<AdminUserProfile> assertFilterResultsSmaller(final UserManagementFilter filter, final int expectedSize) throws SQLException {
    final List<AdminUserProfile> users = assertFilterResults(filter, expectedSize);
    assertTrue("Returned user list should be smaller then the usual", users.size() < DEFAULT_USERFILTER_SIZE);
    return users;
  }

  private List<AdminUserProfile> assertFilterResults(final UserManagementFilter filter, final int expectedSize) throws SQLException {
    final int limit = 100;
    final List<AdminUserProfile> users = UserRepository.getUserProfiles(getRegConnection(), limit, 0, filter);
    assertNotNull("Returned user list shouldn't be null", users);
    assertEquals("Returned user list size", expectedSize, users.size());
    validateUsers(users);
    return users;
  }

  @Test
  public void testValidateRegisterPermissions() throws SQLException {
    // empty filter
    final ArrayList<AdminUserProfile> users = UserRepository.getUserProfiles(getRegConnection(), 0, 0, new UserManagementFilter());
    assertNotNull("Returned user list shouldn't be null", users);
    assertFalse("Returned user list shouldn't be empty", users.isEmpty());
    validateUsers(users);
    for (final UserProfile user : users) {
      for (final String permission : user.getPermissions()) {
        final boolean enumExists = EnumUtils.isValidEnum(RegisterPermission.class, permission.toUpperCase())
            || EnumUtils.isValidEnum(AdminPermission.class, permission.toUpperCase());
        assertTrue("Permission from DB should exist", enumExists);
      }
    }
  }

  private void validateUsers(final List<AdminUserProfile> users) {
    final Set<Integer> userIds = new HashSet<>();
    for (final AdminUserProfile user : users) {
      if (userIds.contains(user.getId())) {
        fail("Found duplicate user ID" + user.getId());
      } else {
        userIds.add(user.getId());
      }
      assertNotNull("user name", user.getName());
      assertNotNull("user last name", user.getLastName());
      assertNotNull("user authority", user.getAuthority());
      assertNotNull("user email", user.getEmailAddress());
      assertNotNull("user roles", user.getRoles());
    }
  }

  private static AdminUserProfile getExampleAdminProfile(final String username, final String email) {
    return getExampleProfileFrom(new AdminUserProfile(), username, email);
  }

  private static <U extends UserProfile> U getExampleProfileFrom(final U profile, final String username, final String email) {
    profile.setName(username);
    profile.setLastName(StringUtils.reverse(username));
    profile.setEmailAddress(email);
    profile.setAuthority(getExampleAuthority());
    return profile;
  }

  private static Authority getExampleAuthority() {
    final Authority testAuthority = new Authority();
    testAuthority.setId(1);
    return testAuthority;
  }

}
