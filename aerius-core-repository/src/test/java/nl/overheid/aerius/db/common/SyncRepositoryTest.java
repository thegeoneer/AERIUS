/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import nl.overheid.aerius.db.common.SyncRepository.DevelopmentSpacesEntry;
import nl.overheid.aerius.db.common.SyncRepository.InitialAvailableDevelopmentSpacesEntry;
import nl.overheid.aerius.db.common.SyncRepository.ReservedDevelopmentSpacesEntry;

public class SyncRepositoryTest extends BaseDBTest {

  private static final String PERMIT_THRESHOLD_VALUES = "permit_threshold_values";
  private static final String DEVELOPMENT_SPACES = "development_spaces";
  private static final String RESERVED_DEVELOPMENT_SPACES = "reserved_development_spaces";
  private static final String INITIAL_AVAILABLE_DEVELOPMENT_SPACES = "initial_available_development_spaces";

  private static final String ENSURE_DEVELOPMENT_SPACE = "INSERT INTO development_spaces (segment, status, receptor_id, space) "
      + " SELECT 'projects', 'assigned', 4776053, 10.0 FROM reserved_development_spaces "
      + " LEFT JOIN development_spaces USING (receptor_id) "
      + " WHERE receptor_id = 4776053 AND development_spaces.receptor_id IS NULL "
      + " LIMIT 1 ";

  @Test
  public void testReadWritePermitThresholdValues() throws SQLException {
    final Connection conCalc = getCalcConnection();
    final Connection conReg = getRegConnection();

    // Read from Register
    final int rcSource = getRecordCount(conReg, PERMIT_THRESHOLD_VALUES);
    final Map<Integer, Double> values = SyncRepository.readPermitThresholdValues(conReg);
    assertFalse("No permit_threshold_values records were returned", values.isEmpty());
    assertEquals("Not all permit_threshold_values records read", rcSource, values.size());

    // Write to Calculator
    SyncRepository.writePermitThresholdValues(conCalc, values);
    final int rcTarget = getRecordCount(conCalc, PERMIT_THRESHOLD_VALUES);

    assertEquals("Not all permit_threshold_values records written", rcSource, rcTarget);
  }

  @Test
  public void testReadWriteDevelopmentSpaces() throws SQLException {
    ensureDevelopmentSpaceInRegister();
    final Connection conCalc = getCalcConnection();
    final Connection conReg = getRegConnection();

    // Read from Register
    final int rcSource = getRecordCount(conReg, DEVELOPMENT_SPACES);
    final List<DevelopmentSpacesEntry> records = SyncRepository.readDevelopmentSpaces(conReg);
    assertFalse("No development_spaces records were returned", records.isEmpty());
    assertEquals("Not all development_spaces records read", rcSource, records.size());

    // Take only first 100 for unit test speed
    final List<DevelopmentSpacesEntry> subRecords = records.subList(0, Math.min(100, records.size()));

    // Write to Calculator
    SyncRepository.writeDevelopmentSpaces(conCalc, subRecords);
    final int rcTarget = getRecordCount(conCalc, DEVELOPMENT_SPACES);

    assertEquals("Not all development_spaces records written", subRecords.size(), rcTarget);
  }

  @Test
  public void testReadWriteReservedAndInitialAvailableDevelopmentSpaces() throws SQLException {
    final Connection conCalc = getCalcConnection();
    final Connection conReg = getRegConnection();

    // Read from Register
    final int rcSourceReserved = getRecordCount(conReg, RESERVED_DEVELOPMENT_SPACES);
    final List<ReservedDevelopmentSpacesEntry> recordsReserved = SyncRepository.readReservedDevelopmentSpaces(conReg);
    assertFalse("No reserved_development_spaces records were returned", recordsReserved.isEmpty());
    assertEquals("Not all reserved_development_spaces records read", rcSourceReserved, recordsReserved.size());

    final int rcSourceInitialAvailable = getRecordCount(conReg, INITIAL_AVAILABLE_DEVELOPMENT_SPACES);
    final List<InitialAvailableDevelopmentSpacesEntry> recordsInitialAvailable = SyncRepository.readInitialAvailableDevelopmentSpaces(conReg);
    assertFalse("No initial_available_development_spaces records were returned", recordsInitialAvailable.isEmpty());
    assertEquals("Not all initial_available__development_spaces records read", rcSourceInitialAvailable, recordsInitialAvailable.size());

    assertEquals("reserved- and initial_available_development_spaces should contain the same amount of records",
        recordsReserved.size(), recordsInitialAvailable.size());

    // Take only the first 100 for unit test speed
    final List<ReservedDevelopmentSpacesEntry> subRecordsReserved = recordsReserved.subList(0, Math.min(100, recordsReserved.size()));
    final List<InitialAvailableDevelopmentSpacesEntry> subRecordsInitialAvailable =
        recordsInitialAvailable.subList(0, Math.min(100, recordsInitialAvailable.size()));

    // Write to Calculator
    SyncRepository.writeReservedAndInitialAvailableDevelopmentSpaces(conCalc, subRecordsReserved, subRecordsInitialAvailable);
    final int rcTargetReserved = getRecordCount(conCalc, RESERVED_DEVELOPMENT_SPACES);
    final int rcTargetInitialAvailable = getRecordCount(conCalc, INITIAL_AVAILABLE_DEVELOPMENT_SPACES);

    assertEquals("Not all reserved_development_spaces records written", subRecordsReserved.size(), rcTargetReserved);
    assertEquals("Not all initial_available__development_spaces records written", subRecordsInitialAvailable.size(), rcTargetInitialAvailable);
  }

  private int getRecordCount(final Connection con, final String table) throws SQLException {
    try (PreparedStatement ps = con.prepareStatement("SELECT COUNT(*) FROM " + table)) {
      final ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        return rs.getInt(1);
      }
    }
    return 0;
  }

  private void ensureDevelopmentSpaceInRegister() throws SQLException {
    try (PreparedStatement ps = getRegConnection().prepareStatement(ENSURE_DEVELOPMENT_SPACE)) {
      ps.executeUpdate();
    }
  }

}