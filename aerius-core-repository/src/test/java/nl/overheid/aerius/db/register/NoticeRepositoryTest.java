/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.reference.ReferenceUtil;

public class NoticeRepositoryTest extends RequestTestBase {

  public static final int TEST_NOTICE_ID = 21;
  public static final String TEST_NOTICE_REFERENCE = "1W21yi5oU";
  public static final int XCOORD_NOTICE = 247669;
  public static final int YCOORD_NOTICE = 566362;
  public static final int MAX_RADIUS = 100;

  private void validateNotice(final Notice notice) {
    assertNotNull("Notice shouldn't be null", notice);
    assertNotEquals("Notice ID", 0, notice.getId());
    assertNotNull("Location of the notice", notice.getPoint());
    assertNotNull("metadata", notice.getScenarioMetaData());
    assertNotNull("project name", notice.getScenarioMetaData().getProjectName());
    assertNotNull("corporation", notice.getScenarioMetaData().getCorporation());
    assertNotNull("Start date", notice.getReceivedDate());
    assertNotNull("Sector", notice.getSectorIcon());
  }

  private void validateNotices(final List<Notice> notices) {
    assertNotNull("Notices list shouldn't be null", notices);
    for (final Notice notice : notices) {
      validateNotice(notice);
      assertNull("Individual notice shouldn't have the province object", notice.getProvince());
    }
  }

  @Test
  public void testGetNotice() throws SQLException {
    final Notice notice = NoticeRepository.getNotice(getRegConnection(), TEST_NOTICE_ID);
    validateNotice(notice);
    assertNotNull("Individual notice should have the province", notice.getProvince());
  }

  @Test
  public void testGetNoticeWithAuditTrail() throws SQLException, AeriusException {
    Notice notice = NoticeRepository.getNoticeWithAuditTrail(getRegConnection(), TEST_NOTICE_ID);
    validateNotice(notice);
    assertTrue("Notice audit trail must be empty (not loaded)", notice.getChangeHistory().isEmpty());
    NoticeModifyRepository.confirmNotices(getRegConnection(), Arrays.asList(TEST_NOTICE_ID), getExampleUserProfile());
    notice = NoticeRepository.getNoticeWithAuditTrail(getRegConnection(), TEST_NOTICE_ID);
    validateNotice(notice);
    assertFalse("Notice audit trail must not be empty", notice.getChangeHistory().isEmpty());
  }

  @Test
  public void testGetNoticeIdByReference() throws SQLException {
    final int noticeId = NoticeRepository.getNoticeIdByReference(getRegConnection(), TEST_NOTICE_REFERENCE);
    assertNotEquals("Notice id found", -1, noticeId);
    assertEquals("Notice id", TEST_NOTICE_ID, noticeId);
  }

  @Test
  public void testGetNotices() throws SQLException {
    final ArrayList<Notice> notices = NoticeRepository.getNotices(getRegConnection(), 100, 0, new NoticeFilter());
    assertNotNull("Notices list shouldn't be null", notices);
    assertFalse("Notices list shouldn't be empty", notices.isEmpty());
    for (final Notice notice : notices) {
      validateNotice(notice);
    }
  }

  @Test
  public void testSearch() throws SQLException, AeriusException {
    List<Notice> foundNotices = NoticeRepository.search(getRegConnection(), "arch-str");
    validateNotices(foundNotices);
    assertTrue("Shouldn't have found it yet", foundNotices.isEmpty());

    final String reference = ReferenceUtil.generateMeldingReference();
    final int insertedNotice = insertNotice(reference, "corporation-string", new Point(XCOORD_NOTICE, YCOORD_NOTICE));
    foundNotices = NoticeRepository.search(getRegConnection(), reference.substring(2, 5));
    validateNotices(foundNotices);
    assertEquals("Should find one notice", 1, foundNotices.size());
    assertEquals("Found ID", insertedNotice, foundNotices.get(0).getId());

    foundNotices = NoticeRepository.search(getRegConnection(), "ation-str");
    validateNotices(foundNotices);
    assertEquals("Should find one notice", 1, foundNotices.size());
    assertEquals("Found ID", insertedNotice, foundNotices.get(0).getId());
  }

  @Test
  public void testSearchMultiHit() throws SQLException, AeriusException {
    final int insertedNotice = insertNotice(ReferenceUtil.generateMeldingReference(), "corp-multihit-string1",
        new Point(XCOORD_NOTICE, YCOORD_NOTICE));
    final int insertedNotice2 = insertNotice(ReferenceUtil.generateMeldingReference(), "corp-multihit-string2",
        new Point(XCOORD_NOTICE, YCOORD_NOTICE));
    final List<Notice> foundNotices = NoticeRepository.search(getRegConnection(), "multihit-string");
    validateNotices(foundNotices);
    assertEquals("Should find one notice", 2, foundNotices.size());

    // It's ordered by insert time, so the order should be preserved
    assertEquals("Found ID", insertedNotice, foundNotices.get(0).getId());
    assertEquals("Found ID", insertedNotice2, foundNotices.get(1).getId());
  }

  @Test
  public void testSearchMultiHitSameEntry() throws SQLException, AeriusException {
    final String reference = ReferenceUtil.generateMeldingReference();
    final int insertedNotice = insertNotice(reference, "corp-" + reference.substring(3, 6) + "-string", new Point(XCOORD_NOTICE, YCOORD_NOTICE));
    final List<Notice> foundNotices = NoticeRepository.search(getRegConnection(), reference.substring(3, 6));
    validateNotices(foundNotices);
    assertEquals("Should find one notice", 1, foundNotices.size());
    assertEquals("Found ID", insertedNotice, foundNotices.get(0).getId());
  }

  @Test
  public void testGetNoticesWithinRadius() throws SQLException, AeriusException {
    final int targetNoticeId = insertNotice(new Point(XCOORD_NOTICE, YCOORD_NOTICE));
    final int withinRadiusNoticeId = insertNotice(new Point(XCOORD_NOTICE + MAX_RADIUS, YCOORD_NOTICE));
    //outside radius
    insertNotice(new Point(XCOORD_NOTICE + MAX_RADIUS + 1, YCOORD_NOTICE));

    final ArrayList<Notice> notices = NoticeRepository.getNoticesWithinRadius(getRegConnection(), targetNoticeId, MAX_RADIUS);
    validateNotices(notices);
    assertEquals("Notices list size", 1, notices.size());
    assertEquals("Notice retrieved ID", withinRadiusNoticeId, notices.get(0).getId());
  }

  @Test
  public void testGetConfirmableNotices() throws SQLException, AeriusException {
    insertNotice(new Point(XCOORD_NOTICE, YCOORD_NOTICE));
    final ArrayList<Integer> confirmableNoticeIds = NoticeRepository.getConfirmableNoticesIds(getRegConnection(), 2);
    assertNotNull("Notices list shouldn't be null", confirmableNoticeIds);
    assertFalse("Confirmable notices list shouldn't be empty", confirmableNoticeIds.isEmpty());
  }

  @Test
  public void testGetAuthorityForNotice() throws SQLException {
    Authority authority = NoticeRepository.getAuthorityForNotice(getRegConnection(), new Point(XCOORD_NOTICE, YCOORD_NOTICE));
    assertNotNull("Authority for notice within NL", authority);
    assertEquals("Authority ID within NL", 2, authority.getId());
    assertEquals("Authority code within NL", "Drenthe", authority.getCode());
    authority = NoticeRepository.getAuthorityForNotice(getRegConnection(), new Point(180000, 300000));
    assertNotNull("Authority for notice outside NL", authority);
    assertEquals("Authority ID outside NL", 7, authority.getId());
    assertEquals("Authority ID outside NL", "Limburg", authority.getCode());
    authority = NoticeRepository.getAuthorityForNotice(getRegConnection(), new Point(Integer.MAX_VALUE, Integer.MAX_VALUE));
    assertNotNull("Authority for notice outside NL", authority);
    assertEquals("Authority ID outside NL", 6, authority.getId());
    assertEquals("Authority ID outside NL", "Groningen", authority.getCode());
  }

  private int insertNotice(final Point point) throws SQLException, AeriusException {
    return insertNotice(ReferenceUtil.generateMeldingReference(), null, point);
  }

  private int insertNotice(final String reference, final String corporation, final Point point) throws SQLException, AeriusException {
    final Notice notice = new Notice();
    TestDomain.fillExampleRequest(notice);
    notice.getScenarioMetaData().setReference(reference);
    notice.getScenarioMetaData().setCorporation(corporation);
    notice.setPoint(point);
    final int meldingId = NoticeModifyRepository.insertMelding(getRegConnection(), notice, getExampleUserProfile(), getFakeInsertRequestFile());
    addCalculation(notice);
    RequestModifyRepository.updateRequestToQueued(getRegConnection(), notice, getExampleUserProfile());
    NoticeModifyRepository.assignMelding(getRegConnection(), notice, getExampleUserProfile());
    return meldingId;
  }

}
