/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.geo.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

public class ReceptorRepositoryTest extends BaseDBTest {
  private static final Logger LOG = LoggerFactory.getLogger(ReceptorRepositoryTest.class);

  private static final int RECEPTOR_ID = 2793000;
  private static final double X_COORD = 198100.4155;
  private static final double Y_COORD = 394908.2347;
  private static final double AVERAGE_ROUGHNESS = 0.51979;
  private static final LandUse DOMINANT_LAND_USE = LandUse.DECIDUOUS_FOREST;
  private static final int[] LAND_USE_PERCENTAGES = new int[] { 11, 10, 0, 9, 59, 0, 10, 0, 1 };

  private static final int RECEPTOR_ID_SPECIAL_CASE = 4832573; // receptor where rounded percentages would add up to 98
  private static final int[] SPECIAL_CASE_LAND_USE_PERCENTAGES = new int[] { 4, 21, 0, 23, 17, 1, 2, 20, 12 };

  private static final String SQL_GET_LAND_USE_ENUM = "SELECT unnest(enum_range(NULL::land_use_classification))";

  @Test
  public void testSetTerrainData() throws SQLException {
    OPSReceptor point = new OPSReceptor(0);
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertEquals("Receptor average roughness, uninitialized", 0.1, point.getAverageRoughness(), 0.00001);
    assertEquals("Receptor dominant land use, uninitialized", LandUse.OTHER_NATURE, point.getLandUse());
    assertNull("Receptor land use fractions, uninitialized", point.getLandUses());

    point = new OPSReceptor(RECEPTOR_ID);
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertEquals("Receptor X coordinate, ID only", 0, point.getX(), 0.0001);
    assertEquals("Receptor Y coordinate, ID only", 0, point.getY(), 0.0001);
    assertEquals("Receptor average roughness, ID only", 0.1, point.getAverageRoughness(), 0.00001);
    assertEquals("Receptor dominant land use, ID only", LandUse.OTHER_NATURE, point.getLandUse());
    assertNull("Receptor land use fractions, ID only", point.getLandUses());

    point = new OPSReceptor(0, X_COORD - 1, Y_COORD + 1);
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertEquals("Receptor average roughness, right coordinates", AVERAGE_ROUGHNESS, point.getAverageRoughness(), 0.00001);
    assertEquals("Receptor dominant land use, right coordinates", DOMINANT_LAND_USE, point.getLandUse());
    assertArrayEquals("Receptor land use percentage, right coordinates", LAND_USE_PERCENTAGES, point.getLandUses());
    // setting terrain properties shouldn't set ID.
    assertEquals("Receptor ID, right coordinates", 0, point.getId());
    // and it shouldn't adjust coords.
    assertEquals("Receptor X coordinate, right coordinates", X_COORD - 1, point.getX(), 0.0001);
    assertEquals("Receptor Y coordinate, right coordinates", Y_COORD + 1, point.getY(), 0.0001);

    point = new OPSReceptor(RECEPTOR_ID_SPECIAL_CASE);
    RECEPTOR_UTIL.setAeriusPointFromId(point);
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertArrayEquals("Receptor land use percentages", SPECIAL_CASE_LAND_USE_PERCENTAGES, point.getLandUses());

    point = new OPSReceptor(0, 1, 1);
    // is within NL bounding box, but not defined in table.
    ReceptorRepository.setTerrainData(getCalcConnection(), RECEPTOR_UTIL, RECEPTOR_GRID_SETTINGS, point);
    assertEquals("Receptor average roughness, wrong coordinates", 0.1, point.getAverageRoughness(), 0.00001);
    assertEquals("Receptor dominant land use, wrong coordinates", LandUse.OTHER_NATURE, point.getLandUse());
    assertNull("Receptor land use fractions, wrong coordinates", point.getLandUses());
  }

  @Test
  public void testGetAssessmentAreaReceptors() throws SQLException {
    final List<OPSReceptor> points = ReceptorRepository.getAssessmentAreaReceptors(getRegConnection(), RECEPTOR_UTIL, TestDomain.BINNENVELD_ID);
    assertNotNull("Receptors", points);
    assertNotEquals("Number of receptors", 0, points.size());
  }

  @Test
  public void testLandUseEnum() throws SQLException {
    // Checks for typos in the Java enum values
    final LandUse[] expectedLandUses = LandUse.values();
    final List<LandUse> actualLandUses = new ArrayList<>();
    try (final PreparedStatement ps = getCalcConnection().prepareStatement(SQL_GET_LAND_USE_ENUM)) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        actualLandUses.add(LandUse.safeValueOf(rs.getString(1)));
      }
    }
    assertArrayEquals("Land use enum in Java and DB", expectedLandUses, actualLandUses.toArray());
  }
}
