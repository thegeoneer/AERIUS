/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import nl.overheid.aerius.db.util.OrderByClause.OrderType;

/**
 *
 */
public class QueryBuilderTest {

  private static final String TEST_TABLE = "SomeTable";

  private enum TestAttribute implements Attribute {
    TEST_COLUMN_1, TEST_COLUMN_2, TEST_COLUMN_3;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private enum TestOtherEnum {
    ONE_VALUE, TWO_VALUE
  }

  @Test
  public void testFrom() {
    Query query = QueryBuilder.from(TEST_TABLE).getQuery();
    assertEquals("Query with just from", "SELECT * FROM SomeTable", query.get());
    assertEquals("Index of attribute with just from", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));

    query = QueryBuilder.from(TEST_TABLE, TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("Query with from and with from-attributes", "SELECT * FROM SomeTable", query.get());
    assertEquals("Index of attribute with from and with from-attributes", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
  }

  @Test
  public void testSelectAttribute() {
    Query query = QueryBuilder.from(TEST_TABLE).select(TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("Query with 1 select", "SELECT test_column_1 FROM SomeTable", query.get());
    assertEquals("Index of attribute with 1 select", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));

    query = QueryBuilder.from(TEST_TABLE).select(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("Query with 2 selects", "SELECT test_column_1, test_column_2 FROM SomeTable", query.get());
    assertEquals("Index of attribute with 2 selects", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 selects", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));

    query = QueryBuilder.from(TEST_TABLE).select(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("Query with 3 selects", "SELECT test_column_1, test_column_3, test_column_2 FROM SomeTable", query.get());
    assertEquals("Index of attribute with 3 selects", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 3 selects", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Index of attribute with 3 selects", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }

  @Test
  public void testSelectWithClause() {
    Query query = QueryBuilder.from(TEST_TABLE).select(new SelectClause(TestAttribute.TEST_COLUMN_1)).getQuery();
    assertEquals("Query with 1 selectclause", "SELECT test_column_1 FROM SomeTable", query.get());
    assertEquals("Index of attribute with 1 selectclause", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));

    query = QueryBuilder.from(TEST_TABLE).select(new SelectClause(TestAttribute.TEST_COLUMN_1),
        new SelectClause(TestAttribute.TEST_COLUMN_2)).getQuery();
    assertEquals("Query with 2 selectclauses", "SELECT test_column_1, test_column_2 FROM SomeTable", query.get());
    assertEquals("Index of attribute with 2 selectclauses", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 selectclauses", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));

    query = QueryBuilder.from(TEST_TABLE).select(new SelectClause(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3),
        new SelectClause(TestAttribute.TEST_COLUMN_2)).getQuery();
    assertEquals("Query with 2 selectclauses with 1 attribute", "SELECT test_column_1, test_column_2 FROM SomeTable", query.get());
    assertEquals("Index of attribute with 2 selectclauses with 1 attribute", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 selectclauses with 1 attribute", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Index of attribute with 2 selectclauses with 1 attribute", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }

  @Test
  public void testJoin() {
    Query query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", TestAttribute.TEST_COLUMN_1)).getQuery();
    assertEquals("Query with 1 join", "SELECT * FROM SomeTable INNER JOIN OtherTable USING (test_column_1)", query.get());
    assertEquals("Index of attribute with 1 join", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));

    query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", TestAttribute.TEST_COLUMN_1),
        new JoinClause("AnotherTable", TestAttribute.TEST_COLUMN_2)).getQuery();
    assertEquals("Query with 2 joins",
        "SELECT * FROM SomeTable INNER JOIN OtherTable USING (test_column_1) INNER JOIN AnotherTable USING (test_column_2)", query.get());
    assertEquals("Index of attribute with 2 joins", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 joins", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));

    query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", "SomeJoin")).getQuery();
    assertEquals("Query with 1 string-based join", "SELECT * FROM SomeTable INNER JOIN OtherTable USING (SomeJoin)", query.get());

    query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3)).getQuery();
    assertEquals("Query with 1 attribute for join", "SELECT * FROM SomeTable INNER JOIN OtherTable USING (test_column_1)", query.get());
    assertEquals("Index of attribute with 1 attribute for join", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 1 attribute for join", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));

    query = QueryBuilder.from(TEST_TABLE).join(new JoinClause("OtherTable", "join1", TestAttribute.TEST_COLUMN_3),
        new JoinClause("AnotherTable", "join2", TestAttribute.TEST_COLUMN_1)).getQuery();
    assertEquals("Query with 2 attribute for joins",
        "SELECT * FROM SomeTable INNER JOIN OtherTable USING (join1) INNER JOIN AnotherTable USING (join2)", query.get());
    assertEquals("Index of attribute with 2 attribute for joins", 2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 attribute for joins", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }

  @Test
  public void testWhereAttribute() {
    Query query = QueryBuilder.from(TEST_TABLE).where(TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("Query with 1 where", "SELECT * FROM SomeTable WHERE test_column_1 = ?", query.get());
    assertEquals("Index of attribute with 1 where", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));

    query = QueryBuilder.from(TEST_TABLE).where(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("Query with 2 wheres", "SELECT * FROM SomeTable WHERE test_column_1 = ? AND test_column_2 = ?", query.get());
    assertEquals("Index of attribute with 2 wheres", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 wheres", 2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));

    query = QueryBuilder.from(TEST_TABLE).where(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("Query with 3 wheres", "SELECT * FROM SomeTable WHERE test_column_1 = ? AND test_column_3 = ? AND test_column_2 = ?", query.get());
    assertEquals("Index of attribute with 3 wheres", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 3 wheres", 3, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Index of attribute with 3 wheres", 2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }

  @Test
  public void testWhereWithClause() {
    final WhereClause whereClause1 = new WhereClause() {

      @Override
      public String toWherePart() {
        return "SomeWherePart";
      }

      @Override
      public Attribute[] getParamAttributes() {
        return new Attribute[0];
      }

    };
    Query query = QueryBuilder.from(TEST_TABLE).where(whereClause1).getQuery();
    assertEquals("Query with 1 whereclause", "SELECT * FROM SomeTable WHERE SomeWherePart", query.get());
    assertEquals("Index of attribute with 1 whereclause", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));

    final WhereClause whereClause2 = new WhereClause() {

      @Override
      public String toWherePart() {
        return "SomeOtherWherePart";
      }

      @Override
      public Attribute[] getParamAttributes() {
        return new Attribute[] {TestAttribute.TEST_COLUMN_2, TestAttribute.TEST_COLUMN_1};
      }

    };
    query = QueryBuilder.from(TEST_TABLE).where(whereClause1, whereClause2).getQuery();
    assertEquals("Query with 2 whereclauses", "SELECT * FROM SomeTable WHERE SomeWherePart AND SomeOtherWherePart", query.get());
    assertEquals("Index of attribute with 2 whereclauses", 2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 whereclauses", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));

  }

  @Test
  public void testGroupBy() {
    Query query = QueryBuilder.from(TEST_TABLE).groupBy(TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("Query with 1 groupby", "SELECT * FROM SomeTable GROUP BY test_column_1", query.get());
    assertEquals("Index of attribute with 1 groupby", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));

    query = QueryBuilder.from(TEST_TABLE).groupBy(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("Query with 2 groupbys", "SELECT * FROM SomeTable GROUP BY test_column_1, test_column_2", query.get());
    assertEquals("Index of attribute with 2 groupbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 groupbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));

    query = QueryBuilder.from(TEST_TABLE).groupBy(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("Query with 3 groupbys", "SELECT * FROM SomeTable GROUP BY test_column_1, test_column_3, test_column_2", query.get());
    assertEquals("Index of attribute with 3 groupbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 3 groupbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Index of attribute with 3 groupbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }

  @Test
  public void testOrderByAttribute() {
    Query query = QueryBuilder.from(TEST_TABLE).orderBy(TestAttribute.TEST_COLUMN_1).getQuery();
    assertEquals("Query with 1 orderby", "SELECT * FROM SomeTable ORDER BY test_column_1 ASC", query.get());
    assertEquals("Index of attribute with 1 orderby", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));

    query = QueryBuilder.from(TEST_TABLE).orderBy(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("Query with 2 orderbys", "SELECT * FROM SomeTable ORDER BY test_column_1 ASC, test_column_2 ASC", query.get());
    assertEquals("Index of attribute with 2 orderbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 orderbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));

    query = QueryBuilder.from(TEST_TABLE).orderBy(TestAttribute.TEST_COLUMN_1, TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2).getQuery();
    assertEquals("Query with 3 orderbys", "SELECT * FROM SomeTable ORDER BY test_column_1 ASC, test_column_3 ASC, test_column_2 ASC", query.get());
    assertEquals("Index of attribute with 3 orderbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 3 orderbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Index of attribute with 3 orderbys", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }

  @Test
  public void testOrderByWithClause() {
    Query query = QueryBuilder.from(TEST_TABLE).orderBy(new OrderByClause(TestAttribute.TEST_COLUMN_1)).getQuery();
    assertEquals("Query with 1 orderby clause", "SELECT * FROM SomeTable ORDER BY test_column_1 ASC", query.get());
    assertEquals("Index of attribute with 1 orderby clause", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));

    query = QueryBuilder.from(TEST_TABLE).orderBy(new OrderByClause(TestAttribute.TEST_COLUMN_1),
        new OrderByClause(TestAttribute.TEST_COLUMN_2, OrderType.DESC)).getQuery();
    assertEquals("Query with 2 orderby clauses", "SELECT * FROM SomeTable ORDER BY test_column_1 ASC, test_column_2 DESC", query.get());
    assertEquals("Index of attribute with 2 orderby clauses", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 2 orderby clauses", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));

    query = QueryBuilder.from(TEST_TABLE).orderBy(new OrderByClause(TestAttribute.TEST_COLUMN_1),
        new OrderByClause(TestAttribute.TEST_COLUMN_3, OrderType.ASC), new OrderByClause(TestAttribute.TEST_COLUMN_2, OrderType.DESC)).getQuery();
    assertEquals("Query with 3 orderby clauses", "SELECT * FROM SomeTable ORDER BY test_column_1 ASC, test_column_3 ASC, test_column_2 DESC", query.get());
    assertEquals("Index of attribute with 3 orderby clauses", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute with 3 orderby clauses", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Index of attribute with 3 orderby clauses", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }

  @Test
  public void testLimit() {
    Query query = QueryBuilder.from(TEST_TABLE).limit().getQuery();
    assertEquals("Query with limit", "SELECT * FROM SomeTable LIMIT ?", query.get());

    query = QueryBuilder.from(TEST_TABLE).limit().limit().getQuery();
    assertEquals("Query with 2 limits (no effect)", "SELECT * FROM SomeTable LIMIT ?", query.get());
  }

  @Test
  public void testLimitWithInt() {
    Query query = QueryBuilder.from(TEST_TABLE).limit(4).getQuery();
    assertEquals("Query with set limit", "SELECT * FROM SomeTable LIMIT 4", query.get());

    query = QueryBuilder.from(TEST_TABLE).limit(4).limit(8).getQuery();
    assertEquals("Query with 2 set limits (last should be used)", "SELECT * FROM SomeTable LIMIT 8", query.get());
  }

  @Test
  public void testOffset() {
    Query query = QueryBuilder.from(TEST_TABLE).offset().getQuery();
    assertEquals("Query with just offset (no effect)", "SELECT * FROM SomeTable", query.get());

    query = QueryBuilder.from(TEST_TABLE).limit().offset().getQuery();
    assertEquals("Query with limit and offset", "SELECT * FROM SomeTable LIMIT ? OFFSET ?", query.get());

    query = QueryBuilder.from(TEST_TABLE).limit().offset().offset().getQuery();
    assertEquals("Query with limit and 2x offset (no extra effect)", "SELECT * FROM SomeTable LIMIT ? OFFSET ?", query.get());
  }

  @Test
  public void testDistinct() {
    Query query = QueryBuilder.from(TEST_TABLE).distinct().getQuery();
    assertEquals("Query with distinct", "SELECT DISTINCT * FROM SomeTable", query.get());

    query = QueryBuilder.from(TEST_TABLE).distinct().distinct().getQuery();
    assertEquals("Query with 2x distinct (no extra effect)", "SELECT DISTINCT * FROM SomeTable", query.get());
  }

  @Test
  public void testGetQuery() {
    final Query query = QueryBuilder.from(TEST_TABLE, TestAttribute.TEST_COLUMN_1)
        .orderBy(TestAttribute.TEST_COLUMN_1)
        .groupBy(TestAttribute.TEST_COLUMN_2)
        .select(TestAttribute.TEST_COLUMN_1)
        .join(new JoinClause("SomeOtherView", TestAttribute.TEST_COLUMN_2, TestAttribute.TEST_COLUMN_2))
        .where(TestAttribute.TEST_COLUMN_3)
        .limit()
        .offset()
        .getQuery();
    //order in which the methods are called doesn't matter for the structure of the resulting query. Only matters for indexes of attributes.
    assertEquals("Query with all stuff used", "SELECT test_column_1 FROM SomeTable "
        + "INNER JOIN SomeOtherView USING (test_column_2) WHERE test_column_3 = ? GROUP BY test_column_2 ORDER BY test_column_1 ASC LIMIT ? OFFSET ?", query.get());
    assertEquals("Index of attribute for query with all stuff used", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Index of attribute for query with all stuff used", 2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Index of attribute for query with all stuff used", 3, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }

}
