/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResult;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResultList;
import nl.overheid.aerius.shared.domain.request.PotentialRequestInfo;
import nl.overheid.aerius.shared.domain.request.PotentialRequestType;

/**
 * Test class for {@link CalculationDevelopmentSpaceRepository}.
 */
public class CalculationDevelopmentSpaceRepositoryTest extends CalculationRepositoryTestBase {

  private static final String EXCEEDING_ASSESSMENT_AREA_NUM = "ExceedingAssessmentAreaNum";
  private static final String RECEPTOR_ID = "Receptor ID";
  private static final String REPORT_OBLIGATION = "ReportObligation";
  private static final String RESULT_SHOULDN_T_BE_NULL = "Result shouldn't be null";

  @Test
  public void testDetermineResultsByArea() throws SQLException {
    insertCalculationResultsNH3();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), calculation.getCalculationId(), 0, true);
    final Map<DevelopmentRule, Boolean> countVeluwe = new HashMap<>();
    final Map<DevelopmentRule, Boolean> countDuinenTexel = new HashMap<>();
    final HashSet<DevelopmentRuleResultList> result = CalculationDevelopmentSpaceRepository.determineResultsByArea(
        getCalcConnection(), calculation.getCalculationId());
    assertNotNull(RESULT_SHOULDN_T_BE_NULL, result);
    assertFalse("Result shouldn't be empty", result.isEmpty());
    assertEquals("Number of assessment areas", 2, result.size());
    for (final DevelopmentRuleResultList entry : result) {
      assertNotEquals("Area ID", 0, entry.getAssessmentArea().getId());
      assertNotNull("bounding box", entry.getAssessmentArea().getBounds());
      assertNotNull("assessment area name", entry.getAssessmentArea().getName());
      assertEquals("DevelopmentRuleResults size", DevelopmentRule.values().length, entry.getDevelopmentRuleResults().size());
      for (final DevelopmentRuleResult ruleResult : entry.getDevelopmentRuleResults()) {
        if (entry.getAssessmentArea().getId() == TestDomain.VELUWE_ID) {
          countVeluwe.put(ruleResult.getRule(), true);
          switch (ruleResult.getRule()) {
          case EXCEEDING_SPACE_CHECK:
            assertEquals("Veluwe exceeding space list size", 1, ruleResult.size());
            assertEquals(RECEPTOR_ID, RECEPTOR_POINT_ID_1, ruleResult.get(0).intValue());
            break;
          case NOT_EXCEEDING_SPACE_CHECK:
            assertTrue("Veluwe not exceeding space check", ruleResult.isEmpty());
            break;
          default:
            fail("Did not expect another check but got " + ruleResult.getRule());
            break;
          }
        } else if (entry.getAssessmentArea().getId() == TestDomain.DUINEN_TEXEL_ID) {
          countDuinenTexel.put(ruleResult.getRule(), true);
          switch (ruleResult.getRule()) {
          case EXCEEDING_SPACE_CHECK:
            assertTrue("Duinen Texel exceeding space check passed", ruleResult.isEmpty());
            break;
          case NOT_EXCEEDING_SPACE_CHECK:
            assertEquals("Duinen Texel not exceeding space demand list size", 1, ruleResult.size());
            assertEquals(RECEPTOR_ID, RECEPTOR_POINT_ID_2, ruleResult.get(0).intValue());
            break;
          default:
            fail("Did not expect another check but got " + ruleResult.getRule());
            break;
          }
        } else {
          fail("Unexpected assessment area id: " + entry.getAssessmentArea().getId());
        }
      }
    }
    assertEquals("Number of rules Veluwe doesn't match", DevelopmentRule.values().length, countVeluwe.size());
    assertEquals("Number of rules Duinen Text doesn't match", DevelopmentRule.values().length, countDuinenTexel.size());
  }

  // No useful results
  @Test
  public void testDPRINoUsefulResults() throws SQLException {
    insertCalculationResultPM10();
    final PotentialRequestInfo result =
        CalculationDevelopmentSpaceRepository.determinePotentialRequestInfo(getCalcConnection(), calculation.getCalculationId());
    assertNotNull(RESULT_SHOULDN_T_BE_NULL, result);
    assertEquals(EXCEEDING_ASSESSMENT_AREA_NUM, 0, result.getExceedingAssessmentAreaNum());
    assertEquals(REPORT_OBLIGATION, PotentialRequestType.NONE, result.getPotentialRequestType());
  }

  // Default sample calculation is a permit
  @Test
  public void testDPRIPermit() throws SQLException {
    insertCalculationResultsNH3();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), calculation.getCalculationId(), 0, true);
    final PotentialRequestInfo result =
        CalculationDevelopmentSpaceRepository.determinePotentialRequestInfo(getCalcConnection(), calculation.getCalculationId());
    assertNotNull(RESULT_SHOULDN_T_BE_NULL, result);
    assertEquals(EXCEEDING_ASSESSMENT_AREA_NUM, 2, result.getExceedingAssessmentAreaNum());
    assertEquals(REPORT_OBLIGATION, PotentialRequestType.PERMIT, result.getPotentialRequestType());
  }

  // Default scaled to melding level
  @Test
  public void testDPRIMelding() throws SQLException {
    calculationResultScale = 0.001;
    insertCalculationResults();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), calculation.getCalculationId(), 0, true);
    final PotentialRequestInfo result =
        CalculationDevelopmentSpaceRepository.determinePotentialRequestInfo(getCalcConnection(), calculation.getCalculationId());
    assertNotNull(RESULT_SHOULDN_T_BE_NULL, result);
    assertEquals(EXCEEDING_ASSESSMENT_AREA_NUM, 0, result.getExceedingAssessmentAreaNum());
    assertEquals(REPORT_OBLIGATION, PotentialRequestType.MELDING, result.getPotentialRequestType());
  }

  // Two situations (second situation is negligible so still permit)
  @Test
  public void testDPRI2SituationsPermit() throws SQLException {
    insertCalculationResults();
    final int calculationId1 = calculation.getCalculationId();
    newCalculation();
    calculationResultScale = 0.001;
    insertCalculationResults();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), calculationId1, calculation.getCalculationId(), true);
    final PotentialRequestInfo result =
        CalculationDevelopmentSpaceRepository.determinePotentialRequestInfo(getCalcConnection(), calculationId1, calculation.getCalculationId());
    assertNotNull(RESULT_SHOULDN_T_BE_NULL, result);
    assertEquals(EXCEEDING_ASSESSMENT_AREA_NUM, 2, result.getExceedingAssessmentAreaNum());
    assertEquals(REPORT_OBLIGATION, PotentialRequestType.PERMIT, result.getPotentialRequestType());
  }

  // Same two situations in reverse (would be negative)
  @Test
  public void testDPRI2SituationsReverse() throws SQLException {
    insertCalculationResults();
    final int calculationId1 = calculation.getCalculationId();
    newCalculation();
    calculationResultScale = 1.001;
    insertCalculationResults();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), calculation.getCalculationId(), calculationId1, true);
    final PotentialRequestInfo result =
        CalculationDevelopmentSpaceRepository.determinePotentialRequestInfo(getCalcConnection(), calculation.getCalculationId(), calculationId1);
    assertNotNull(RESULT_SHOULDN_T_BE_NULL, result);
    assertEquals(EXCEEDING_ASSESSMENT_AREA_NUM, 0, result.getExceedingAssessmentAreaNum());
    assertEquals(REPORT_OBLIGATION, PotentialRequestType.MELDING, result.getPotentialRequestType());
  }

  // Default scaled to BELOW melding level (will be stored as 0)
  @Test
  public void testDPRIBelowMelding() throws SQLException {
    calculationResultScale = 0.0001;
    insertCalculationResults();
    final PotentialRequestInfo result =
        CalculationDevelopmentSpaceRepository.determinePotentialRequestInfo(getCalcConnection(), calculation.getCalculationId());
    assertNotNull(RESULT_SHOULDN_T_BE_NULL, result);
    assertEquals(EXCEEDING_ASSESSMENT_AREA_NUM, 0, result.getExceedingAssessmentAreaNum());
    assertEquals(REPORT_OBLIGATION, PotentialRequestType.NONE, result.getPotentialRequestType());
  }

  // Second situation that brings permit to melding level
  @Test
  public void testDPRIPermitToMelding() throws SQLException {
    insertCalculationResults();
    final int calculationId1 = calculation.getCalculationId();
    newCalculation();
    calculationResultScale = 1.001;
    insertCalculationResults();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), calculation.getCalculationId(), calculationId1, true);
    final PotentialRequestInfo result =
        CalculationDevelopmentSpaceRepository.determinePotentialRequestInfo(getCalcConnection(), calculation.getCalculationId(), calculationId1);
    assertNotNull(RESULT_SHOULDN_T_BE_NULL, result);
    assertEquals(REPORT_OBLIGATION, PotentialRequestType.MELDING, result.getPotentialRequestType());
    assertEquals(EXCEEDING_ASSESSMENT_AREA_NUM, 0, result.getExceedingAssessmentAreaNum());
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }
}
