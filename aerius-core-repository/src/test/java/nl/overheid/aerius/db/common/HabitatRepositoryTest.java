/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.info.HabitatType;

/**
 * Test class for {@link HabitatRepository}.
 */
public class HabitatRepositoryTest extends BaseDBTest {

  private static final int OLDENZAAL_HABITATS = 8;
  private static final int OLDENZAAL_RELEVANT_HABITATS = 8;

  @Test
  public void testGetHabitatTypes() throws SQLException {
    final ArrayList<HabitatType> habitatTypes = HabitatRepository.getHabitatTypes(getCalcConnection(), getCalcMessagesKey());
    assertFalse("We have found habitats", habitatTypes.isEmpty());
  }

  @Test
  public void testGetHabitatInfoByAssessmentArea() throws SQLException {
    final List<HabitatInfo> infos = HabitatRepository.getHabitatInfo(getCalcConnection(), TestDomain.OLDENZAAL_ID, getCalcMessagesKey());
    assertNotNull("Habitat info should never be null", infos);
    assertEquals("Habitat info size for example area", OLDENZAAL_HABITATS, infos.size());
    for (final HabitatInfo info : infos) {
      validateHabitatInfoWooldseVeen(info);
      assertNotEquals("Habitat coverage shouldn't be 0. Habitat ID: " + info.getId(), 0.0, info.getCoverage(), 1E-3);
    }
  }

  @Test
  public void testGetRelevantHabitatInfoByAssessmentArea() throws SQLException {
    final List<HabitatInfo> infos = HabitatRepository.getRelevantHabitatInfo(getCalcConnection(), TestDomain.OLDENZAAL_ID, getCalcMessagesKey());
    assertNotNull("Habitat info should never be null", infos);
    assertEquals("Habitat info size for example area", OLDENZAAL_RELEVANT_HABITATS, infos.size());
    for (final HabitatInfo info : infos) {
      validateHabitatInfoWooldseVeen(info);
      assertNotEquals("Habitat coverage shouldn't be 0. Habitat ID: " + info.getId(), 0.0, info.getCoverage(), 1E-3);
    }
  }

  public static void validateHabitatInfoWooldseVeen(final HabitatInfo info) {
    assertNotNull("Habitat code should never be null. Habitat ID: " + info.getId(), info.getCode());
    assertNotNull("Habitat name should never be null. Habitat ID: " + info.getId(), info.getName());
    //ecology quality and goals can be null (no habitat properties for the habitat/assessment area combination).
    //such is the case for HTs with ID 118/124/274.
    if (info.getId() == 274) {
      assertFalse("Habitat shouldn't be designated. Habitat ID: " + info.getId(), info.isDesignated());
    } else {
      assertNotNull("Habitat ecology quality should never be null. Habitat ID: " + info.getId(), info.getEcologyQuality());
      assertNotNull("Habitat quality goal should never be null. Habitat ID: " + info.getId(), info.getQualityGoal());
      assertNotNull("Habitat extent goal should never be null. Habitat ID: " + info.getId(), info.getExtentGoal());
      assertTrue("Habitat should be designated. Habitat ID: " + info.getId(), info.isDesignated());
    }
  }

}
