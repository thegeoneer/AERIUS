/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public class RequestModifyRepositoryTest extends RequestTestBase {

  @Test
  public void testInsertNewRequest() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
    assertNotEquals("Request ID returned", 0, requestId);
    assertEquals("Request ID in object", 0, request.getId());
  }

  @Test(expected = AeriusException.class)
  public void testInsertDuplicateRequest() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
    assertNotEquals("Request ID returned", 0, requestId);
    assertEquals("Request ID in object", 0, request.getId());
    RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
  }

  @Test
  public void testInsertNewRequestAllSegments() throws SQLException, AeriusException {
    for (final SegmentType segment : SegmentType.values()) {
      final Request request = getExampleRequest();
      final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), segment, getAuthority(), request,
          getFakeInsertRequestFile());
      assertNotEquals("Request ID returned", 0, requestId);
      assertEquals("Request ID in object", 0, request.getId());
    }
  }

  @Test(expected = SQLException.class)
  public void testInsertNewRequestUnknownAuthority() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, new Authority(), request, getFakeInsertRequestFile());
  }

  @Test
  public void testInsertSituationCalculation() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
    request.setId(requestId);
    //Should be possible to add all situations, so might as well test for that right away.
    for (final SituationType situation : SituationType.values()) {
      final Calculation calculation = getExampleCalculation();
      RequestModifyRepository.insertSituationCalculation(getRegConnection(), situation, calculation.getCalculationId(), request);
    }
  }

  @Test(expected = SQLException.class)
  public void testInsertSituationCalculationUnknownRequest() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final Calculation calculation = getExampleCalculation();
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, calculation.getCalculationId(), request);
  }

  @Test(expected = SQLException.class)
  public void testInsertSituationCalculationUnknownCalculation() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
    request.setId(requestId);
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, 0, request);
  }

  @Test
  public void testDeleteFile() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
    request.setId(requestId);
    final RequestFileType testType = RequestFileType.PRIORITY_PROJECT_ACTUALISATION;
    boolean exists = hasFileRequestType(request.getReference(), testType);
    assertFalse("Shouldn't exist at first", exists);

    //checking that deleting without an existing file doesn't throw exception.
    RequestModifyRepository.deleteRequestFile(getRegConnection(), requestId, testType);

    final InsertRequestFile testFile = getFakeInsertRequestFile();
    testFile.setRequestFileType(testType);
    RequestModifyRepository.insertFileContent(getRegConnection(), requestId, testFile);
    exists = hasFileRequestType(request.getReference(), testType);
    assertTrue("Should exist after inserting", exists);

    RequestModifyRepository.deleteRequestFile(getRegConnection(), requestId, testType);
    exists = hasFileRequestType(request.getReference(), testType);
    assertFalse("Shouldn't exist after deleting", exists);
  }

  private boolean hasFileRequestType(final String reference, final RequestFileType requestFileType) throws SQLException {
    boolean foundType = false;
    for (final RequestFile file : RequestRepository.getExistingRequestFiles(getRegConnection(), reference)) {
      if (file.getRequestFileType() == requestFileType) {
        foundType = true;
      }
    }
    return foundType;
  }

  @Test
  public void testUpdateRequestToQueued() throws SQLException, AeriusException {
    Permit permit = getPermitWithCalculation();
    assertEquals("Permit state in DB", RequestState.INITIAL, permit.getRequestState());

    //updating from initial should work.
    RequestModifyRepository.updateRequestToQueued(getRegConnection(), permit, getExampleUserProfile());
    Permit updatedPermit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertEquals("Permit state in DB should have changed", RequestState.QUEUED, updatedPermit.getRequestState());
    assertEquals("Should have a new audit trail item", permit.getChangeHistory().size() + 1, updatedPermit.getChangeHistory().size());
    assertEquals("Should consist of 1 item", 1, updatedPermit.getChangeHistory().get(0).getItems().size());
    assertEquals("Item type", AuditTrailType.STATE, updatedPermit.getChangeHistory().get(0).getItems().get(0).getType());

    //updating to a different state first, then update again to QUEUED should NOT work.
    PermitModifyRepository.updateState(getRegConnection(), permit.getPermitKey(), RequestState.PENDING_WITHOUT_SPACE, getExampleUserProfile(),
        permit.getLastModified());
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertEquals("Permit state in DB now (sanity check)", RequestState.PENDING_WITHOUT_SPACE, permit.getRequestState());
    RequestModifyRepository.updateRequestToQueued(getRegConnection(), permit, getExampleUserProfile());
    updatedPermit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertEquals("Permit state in DB shouldn't have changed", RequestState.PENDING_WITHOUT_SPACE, updatedPermit.getRequestState());
    assertEquals("Shouldn't have a change history", permit.getChangeHistory().size(), updatedPermit.getChangeHistory().size());
  }

  @Test
  public void testRequestMarked() throws SQLException, AeriusException {
    Permit permit = getInsertedExamplePermit();
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertFalse("A new permit should not be marked by default", permit.isMarked());

    RequestModifyRepository.updateMarked(getRegConnection(), permit, true);

    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertTrue("The permit should be marked at this point", permit.isMarked());

    RequestModifyRepository.updateMarked(getRegConnection(), permit, false);

    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());
    assertFalse("The permit should not be marked at this point", permit.isMarked());

  }

  private Request getExampleRequest() throws SQLException {
    final Request request = new Request(SegmentType.PROJECTS);
    TestDomain.fillExampleRequest(request);
    return request;
  }

  private Authority getAuthority() {
    return TestDomain.getDefaultExampleAuthority();
  }

}
