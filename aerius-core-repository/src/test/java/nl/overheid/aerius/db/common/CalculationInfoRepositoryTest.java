/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultSetType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.info.DepositionMarker;
import nl.overheid.aerius.shared.domain.info.DepositionMarker.DepositionMarkerType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Needs a proper database to test against, testing DB operations for calculations.
 */
public class CalculationInfoRepositoryTest extends CalculationRepositoryTestBase {

  private static final int RECEPTOR_1_ASSESSMENT_AREA_ID = 57;
  private static final int RECEPTOR_2_ASSESSMENT_AREA_ID = 2;
  private static final int RECEPTOR_1_HABITAT_ID = 124;
  private static final int RECEPTOR_2_HABITAT_ID = 56;

  private static final Substance TEST_SUBSTANCE_2 = Substance.NH3;

  @Test
  public void testGetPartialCalculationResults() throws SQLException {
    final PartialCalculationResult customPointResults = getTestCustomPointResults();
    final PartialCalculationResult referenceResult = getTestReferenceResults(customPointResults);

    final PartialCalculationResult emptyResult = CalculationInfoRepository.getCalculationResults(getCalcConnection(), -1);
    assertNotNull("Calculation result", emptyResult);
    assertEquals("Number of receptor points in empty calculation result", 0, emptyResult.getResults().size());

    final PartialCalculationResult partialResult = CalculationInfoRepository.getCalculationResults(getCalcConnection(),
        calculation.getCalculationId());
    assertNotNull("Calculation result", partialResult);
    assertEquals("Number of result points", referenceResult.getResults().size(), partialResult.getResults().size());

    matchCalculationResults(referenceResult, customPointResults, partialResult);
  }

  @Test
  public void testGetPartialCalculationSectorResults() throws SQLException {
    final PartialCalculationResult customPointResults = getTestCustomPointResults();
    final PartialCalculationResult referenceResult = getTestReferenceResults(customPointResults);

    final PartialCalculationResult emptyResult = CalculationInfoRepository.getCalculationSectorResults(getCalcConnection(), -1,
        TEST_CALCULATION_RESULTS_SECTOR_ID);
    assertNotNull("Calculation result", emptyResult);
    assertEquals("Number of receptor points in empty calculation result", 0, emptyResult.getResults().size());

    final PartialCalculationResult partialResult = CalculationInfoRepository.getCalculationSectorResults(getCalcConnection(),
        calculation.getCalculationId(), TEST_CALCULATION_RESULTS_SECTOR_ID);
    assertNotNull("Calculation result", partialResult);
    assertEquals("Number of result points", referenceResult.getResults().size(), partialResult.getResults().size());

    matchCalculationResults(referenceResult, customPointResults, partialResult);
  }

  private PartialCalculationResult getTestCustomPointResults() throws SQLException {
    final PartialCalculationResult customPointResults = insertCalculationPoints(TEST_SUBSTANCE_2);
    //trick to set the right calculation point set id:
    //The calc id returned from insertCalculationPoints is actually the calculation point set ID...
    removeCalculationResults();
    createCalculation(customPointResults.getCalculationId());
    customPointResults.setCalculationId(calculation.getCalculationId());
    CalculationRepository.insertCalculationResults(getCalcConnection(), customPointResults.getCalculationId(), customPointResults.getResults());
    CalculationRepository.insertCalculationResults(getCalcConnection(), customPointResults.getCalculationId(), CalculationResultSetType.SECTOR,
        TEST_CALCULATION_RESULTS_SECTOR_ID, customPointResults.getResults());
    return customPointResults;
  }

  private PartialCalculationResult getTestReferenceResults(final PartialCalculationResult customPointResults) throws SQLException {
    insertCalculationResults();
    final PartialCalculationResult referenceResult = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    referenceResult.getResults().addAll(customPointResults.getResults());
    return referenceResult;
  }

  private void matchCalculationResults(final PartialCalculationResult referenceResult, final PartialCalculationResult customPointResults,
      final PartialCalculationResult partialCalculationResult) {
    int matched = 0;
    int matchedCustomResultPoints = 0;
    for (final AeriusResultPoint point : partialCalculationResult.getResults()) {
      for (final AeriusResultPoint referencePoint : referenceResult.getResults()) {
        if (referencePoint.getId() == point.getId()) {
          matched++;
          if (referencePoint.getPointType() == AeriusPointType.POINT) {
            matchedCustomResultPoints++;
            assertNotNull("Custom point label", point.getLabel());
            assertNotEquals("x coord custom point", 0.0, point.getX(), 1E-3);
            assertNotEquals("y coord custom point", 0.0, point.getX(), 1E-3);
          }
          assertEquals("Concentration", referencePoint.getEmissionResult(EmissionResultKey.NH3_CONCENTRATION),
              point.getEmissionResult(EmissionResultKey.NH3_CONCENTRATION), 1E-3);
          assertEquals("Deposition", referencePoint.getEmissionResult(EmissionResultKey.NH3_DEPOSITION),
              point.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 1E-3);
        }
      }
    }
    assertEquals("Number of matched points", referenceResult.getResults().size(), matched);
    assertEquals("Number of matched custom points", customPointResults.getResults().size(), matchedCustomResultPoints);
  }

  @Test
  public void testGetEmissionResults() throws SQLException {
    insertCalculationResults();

    final EmissionResults noEmissionResults = CalculationInfoRepository.getEmissionResults(getCalcConnection(), calculation.getCalculationId(), -1);
    assertTrue("No emission results for non-existing receptor ID", noEmissionResults.isEmpty());
    final EmissionResults emissionResults = CalculationInfoRepository.getEmissionResults(getCalcConnection(), calculation.getCalculationId(),
        RECEPTOR_POINT_ID_1);
    assertEquals("Emission results", RECEPTOR_1_DEPOSITION, emissionResults.get(EmissionResultKey.NH3_DEPOSITION), 0.00001);
  }

  @Test
  public void testDetermineBoundingBox() throws SQLException {
    BBox boundingBox = CalculationInfoRepository.determineBoundingBox(getCalcConnection(), calculation.getCalculationId());
    assertNotNull("boundingBox", boundingBox);
    assertEquals("Min x when no results", 0, boundingBox.getMinX(), 1);
    assertEquals("Max x when no results", 0, boundingBox.getMaxX(), 1);
    assertEquals("Min y when no results", 0, boundingBox.getMinY(), 1);
    assertEquals("Max y when no results", 0, boundingBox.getMaxY(), 1);

    insertCalculationResults();

    boundingBox = CalculationInfoRepository.determineBoundingBox(getCalcConnection(), calculation.getCalculationId());
    assertNotNull("boundingBox", boundingBox);

    assertEquals("Min x with results", 112174, boundingBox.getMinX(), 1);
    assertEquals("Max x with results", 187150, boundingBox.getMaxX(), 1);
    assertEquals("Min y with results", 463895, boundingBox.getMinY(), 1);
    assertEquals("Max y with results", 557060, boundingBox.getMaxY(), 1);
  }

  @Test
  public void testDetermineMarkers() throws SQLException {
    insertCalculationResults();
    List<DepositionMarker> markers = null;
    //depends a lot on getExampleOPSOutputData staying the same.
    //using NH3 in testInsertCalculationSetOptions to get a result
    //(NOXNH3 only gives result if both NH3 AND NOx are calculated
    //or we'd have to use getExampleOPSOutputData to insert both NOx and NH3).
    //Receptor id 7436099 (RECEPTOR_POINT_ID_2) with a filled database should return a marker with 2-combo: project deposition and development space percentage markertype.
    //Receptor id 4776053 (RECEPTOR_POINT_ID_1) with a filled database should return a marker with 3-combo markertype.
    //They are in different assessment areas so should return a marker for both.
    markers = CalculationInfoRepository.determineMarkers(getCalcConnection(), calculation.getCalculationId());
    assertNotNull("Result", markers);
    assertEquals("Result size", 2, markers.size());
    for (final DepositionMarker marker : markers) {
      if (marker.getReceptorId() == RECEPTOR_POINT_ID_2) {
        assertEquals("Marker assessment area ID", 2, marker.getAssessmentAreaId());
        assertNotEquals("Markervalue for highest calculated deposition", 0,
            marker.getMarkerValues().get(DepositionMarkerType.HIGHEST_CALCULATED_DEPOSITION), 0.1);
        assertNotEquals("Markervalue for highest total deposition", 0,
            marker.getMarkerValues().get(DepositionMarkerType.HIGHEST_TOTAL_DEPOSITION), 0.1);
      } else if (marker.getReceptorId() == RECEPTOR_POINT_ID_1) {
        assertEquals("Marker assessment area ID", 57, marker.getAssessmentAreaId());
        assertNotEquals("Markervalue for highest calculated deposition", 0,
            marker.getMarkerValues().get(DepositionMarkerType.HIGHEST_CALCULATED_DEPOSITION), 0.1);
        assertNotEquals("Markervalue for highest total deposition", 0,
            marker.getMarkerValues().get(DepositionMarkerType.HIGHEST_TOTAL_DEPOSITION), 0.1);
      } else {
        fail("Should not be another marker, but found one with receptor ID: " + marker.getReceptorId());
      }
    }
  }

  @Test
  public void testGetCalculationAssessmentHabitatReceptors() throws SQLException {
    insertCalculationResults();

    Map<Integer, Double> results = CalculationInfoRepository.getCalculationAssessmentHabitatReceptors(getCalcConnection(),
        calculation.getCalculationId(),
        RECEPTOR_1_ASSESSMENT_AREA_ID, 1);
    assertNotNull("Result with incorrect habitat id", results);
    assertTrue("Number of results with incorrect habitat id", results.isEmpty());

    results = CalculationInfoRepository.getCalculationAssessmentHabitatReceptors(getCalcConnection(), calculation.getCalculationId(),
        1, RECEPTOR_1_HABITAT_ID);
    assertNotNull("Result with incorrect assessment area id", results);
    assertTrue("Number of results with incorrect assessment area id", results.isEmpty());

    results = CalculationInfoRepository.getCalculationAssessmentHabitatReceptors(getCalcConnection(), calculation.getCalculationId(),
        RECEPTOR_1_ASSESSMENT_AREA_ID, RECEPTOR_1_HABITAT_ID);
    assertNotNull("Result", results);
    assertEquals("Number of results", 1, results.size());
    assertEquals("Receptor point found", RECEPTOR_POINT_ID_1, results.keySet().iterator().next().intValue());
    assertEquals("Surface for receptor 1", 10000.0, results.get(RECEPTOR_POINT_ID_1), 1);

    results = CalculationInfoRepository.getCalculationAssessmentHabitatReceptors(getCalcConnection(), calculation.getCalculationId(),
        RECEPTOR_2_ASSESSMENT_AREA_ID, RECEPTOR_2_HABITAT_ID);
    assertNotNull("Result for receptor 2", results);
    assertEquals("Number of results for receptor 2", 1, results.size());
    assertEquals("Receptor point found for receptor 2", RECEPTOR_POINT_ID_2, results.keySet().iterator().next().intValue());
    assertEquals("Surface for receptor 2", 830.943, results.get(RECEPTOR_POINT_ID_2), 1);
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }

}
