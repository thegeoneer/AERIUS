/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Test class for {@link PrioritySubProjectRepositoryTest}.
 */
public class PrioritySubProjectRepositoryTest extends RequestTestBase {

  private static final String TEST_REFERENCE = "pdp_Overijssel_10_4600";
  private static final PriorityProjectKey DEFAULT_PROJECT_KEY = new PriorityProjectKey(null, null);

  @Test
  public void testGetPrioritySubProject() throws SQLException {
    final PrioritySubProjectKey key = new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, TEST_REFERENCE);

    assertTrue(PrioritySubProjectRepository.prioritySubProjectExists(getRegConnection(), key));
    assertFalse(PrioritySubProjectRepository.prioritySubProjectExists(getRegConnection(), new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, "XYZ")));

    final PrioritySubProject psp1 = PrioritySubProjectRepository.getPrioritySubProjectByKey(getRegConnection(), key);
    assertRetrievedPrioritySubProject(psp1);
    assertEquals("Reference", psp1.getReference(), TEST_REFERENCE);
    assertFalse("Situations", psp1.getSituations().isEmpty());

    final PrioritySubProject psp2 = PrioritySubProjectRepository.getPrioritySubProject(getRegConnection(), psp1.getId());
    assertRetrievedPrioritySubProject(psp2);
    assertEquals("Reference", psp2.getReference(), TEST_REFERENCE);
    assertFalse("Situations", psp2.getSituations().isEmpty());

    final PrioritySubProject psp3 = PrioritySubProjectRepository.getSkinnedPrioritySubProjectByKey(getRegConnection(), key);
    assertRetrievedPrioritySubProject(psp3);
    assertEquals("Reference", psp3.getReference(), TEST_REFERENCE);
    assertTrue("Situations", psp3.getSituations().isEmpty());

    final PrioritySubProject psp4 = PrioritySubProjectRepository.getSkinnedPrioritySubProject(getRegConnection(), psp1.getId());
    assertRetrievedPrioritySubProject(psp4);
    assertEquals("Reference", psp4.getReference(), TEST_REFERENCE);
    assertTrue("Situations", psp4.getSituations().isEmpty());
  }

  @Test
  public void testGetPrioritySubProjects() throws SQLException {
    final List<PrioritySubProject> subProjects = PrioritySubProjectRepository.getPrioritySubProjects(getRegConnection(), Integer.MAX_VALUE, 0);
    assertFalse("List shouldn't be empty", subProjects.isEmpty());
    for (final PrioritySubProject subProject : subProjects) {
      assertRetrievedPrioritySubProject(subProject);
    }
  }

  @Test
  public void testGetParentProjectId() throws SQLException {
    final PrioritySubProject subProject = getTestProject();
    final int parentProjectId = PrioritySubProjectRepository.getParentProjectIdForSubProject(getRegConnection(), subProject.getId());
    assertTrue("Parent project ID shouldn't be negative or 0", parentProjectId > 0);
    assertNotEquals("Parent project ID shouldn't be same as sub projects ID", subProject.getId(), parentProjectId);
  }

  @Test
  public void testGetAssignedResultsInclSubProject() throws SQLException {
    final PrioritySubProject subProject = getTestProject();
    final List<AeriusResultPoint> results = PrioritySubProjectRepository.getAssignedResultsInclSubProject(getRegConnection(), subProject.getId());
    assertNotNull("Returned result list", results);
    assertFalse("Should be at least some results in the database.", results.isEmpty());
  }

  private PrioritySubProject getTestProject() throws SQLException {
    final PrioritySubProject subProject = PrioritySubProjectRepository.getPrioritySubProjectByKey(getRegConnection(), new PrioritySubProjectKey(
        DEFAULT_PROJECT_KEY, TEST_REFERENCE));
    assertNotNull("Test subproject should exist", subProject);
    return subProject;
  }

}
