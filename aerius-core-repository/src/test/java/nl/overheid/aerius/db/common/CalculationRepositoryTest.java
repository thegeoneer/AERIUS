/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Needs a proper database to test against, testing DB operations for calculations.
 */
public class CalculationRepositoryTest extends CalculationRepositoryTestBase {

  private static final double JUST_BELOW_THRESHOLD = 0.0499;
  private static final double ON_THRESHOLD = 0.05;
  private static final double JUST_ABOVE_THRESHOLD = 0.0501;
  private static final double ON_PERMIT_THRESHOLD = 1.0;
  private static final Substance TEST_SUBSTANCE_2 = Substance.NH3;
  private static final boolean DELETE_RESULTS_UNDER_THRESHOLD = true;

  @Test
  public void testInsertCalculation() throws SQLException {
    final Calculation calculation = new Calculation();
    calculation.setYear(TestDomain.YEAR);
    final Calculation createdCalculation = CalculationRepository.insertCalculation(getCalcConnection(), calculation, null);
    assertNotEquals("Calculation ID", 0, createdCalculation.getCalculationId());
  }

  @Test
  public void testGetCalculation() throws SQLException {
    Calculation fromDB = null;
    fromDB = CalculationRepository.getCalculation(getCalcConnection(), -1);
    assertNull("Calculation null if unknown ID", fromDB);
    fromDB = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertNotNull("Calculation", fromDB);
    assertEquals("Calculation ID", calculation.getCalculationId(), fromDB.getCalculationId());
    assertEquals("Calculation creation date", calculation.getCreationDate(), fromDB.getCreationDate());
    assertEquals("Calculation state", calculation.getState(), fromDB.getState());
  }

  @Test
  public void testRemoveCalculationResults() throws SQLException {
    CalculationRepository.removeCalculation(getCalcConnection(), calculation.getCalculationId());
    final Calculation delCalculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertNull("Calculation should not be in the database anymore for id:" + calculation.getCalculationId(), delCalculation);
  }

  @Test
  public void testInsertCalculationWithResults() throws SQLException, AeriusException {
    final int numberOfResults = 100;
    final Scenario scenario = new Scenario();
    scenario.addSources(createSources());
    scenario.addResultPoints(createResults(numberOfResults));
    final CalculatedScenario cs = CalculatedScenarioUtil.toCalculatedScenario(scenario, 0, -1);
    cs.setOptions(getOptions());
    cs.setYear(SharedConstants.getCurrentYear());
    CalculationRepository.insertCalculationWithResults(getCalcConnection(), cs);
    assertNotEquals("Should have an calculationid", 0, cs.getCalculations().get(0).getCalculationId());
    assertSame("Should have state completed", CalculationState.COMPLETED, cs.getCalculations().get(0).getState());
  }

  private CalculationSetOptions getOptions() {
    final CalculationSetOptions options = new CalculationSetOptions();
    options.getSubstances().add(Substance.NH3);
    options.getSubstances().add(Substance.NOX);
    options.getSubstances().add(Substance.NO2);
    options.getEmissionResultKeys().add(EmissionResultKey.NH3_DEPOSITION);
    options.getEmissionResultKeys().add(EmissionResultKey.NOX_DEPOSITION);
    options.setCalculationType(CalculationType.PAS);
    return options;
  }

  private static EmissionSourceList createSources() {
    final EmissionSourceList esl = new EmissionSourceList();
    final EmissionSource es = new GenericEmissionSource();
    esl.add(es);
    return esl;
  }

  @Ignore("We do not have a threshold value it is set to 0.0 - test not applicable")
  @Test
  public void testDeleteCalculationResultsUnderThreshold() throws SQLException {
    final int calculationId = insertResultsForDelete(1.0);
    CalculationRepository.deleteLowCalculationResults(getCalcConnection(), calculationId, 0, DELETE_RESULTS_UNDER_THRESHOLD);
    assertDeleteResult(calculationId, 2, ON_PERMIT_THRESHOLD);
  }

  @Test
  public void testKeepCalculationResultsUnderThreshold() throws SQLException {
    final int calculationId = insertResultsForDelete(1.0);
    CalculationRepository.deleteLowCalculationResults(getCalcConnection(), calculationId, 0, !DELETE_RESULTS_UNDER_THRESHOLD);
    assertKeepResult(calculationId, 4, ON_PERMIT_THRESHOLD);
  }

  @Ignore("We do not have a threshold value it is set to 0.0 - test not applicable")
  @Test
  public void testDeleteComparisonResultsUnderThreshold() throws SQLException {
    final int calculationId1 = insertResultsForDelete(ON_PERMIT_THRESHOLD);
    newCalculation();
    final double emission = 0.03;
    final int calculationId2 = insertResultsForDelete(emission);
    CalculationRepository.deleteLowCalculationResults(getCalcConnection(), calculationId1, calculationId2, DELETE_RESULTS_UNDER_THRESHOLD);
    assertDeleteResult(calculationId1, 2, ON_PERMIT_THRESHOLD);
    assertDeleteResult(calculationId2, 2, emission);
  }

  @Test
  public void testKeepComparisonResultsUnderThreshold() throws SQLException {
    final int calculationId1 = insertResultsForDelete(ON_PERMIT_THRESHOLD);
    newCalculation();
    final double emission = 0.03;
    final int calculationId2 = insertResultsForDelete(emission);
    CalculationRepository.deleteLowCalculationResults(getCalcConnection(), calculationId1, calculationId2, !DELETE_RESULTS_UNDER_THRESHOLD);
    assertKeepResult(calculationId1, 4, ON_PERMIT_THRESHOLD);
    assertKeepResult(calculationId2, 4, emission);
  }

  private int insertResultsForDelete(final double emission) throws SQLException {
    calculationResultScale = 1.0;
    final PartialCalculationResult pr = new PartialCalculationResult();
    pr.add(createResult(RECEPTOR_POINT_ID_1, null, EmissionResultKey.NH3_DEPOSITION, 0, JUST_BELOW_THRESHOLD, 0));
    pr.add(createResult(RECEPTOR_POINT_ID_1 + 1, null, EmissionResultKey.NH3_DEPOSITION, 0, ON_THRESHOLD, 0));
    pr.add(createResult(RECEPTOR_POINT_ID_1 + 2, null, EmissionResultKey.NH3_DEPOSITION, 0, JUST_ABOVE_THRESHOLD, 0));
    pr.add(createResult(RECEPTOR_POINT_ID_1 + 3, null, EmissionResultKey.NH3_DEPOSITION, 0, emission, 0));
    final int size = pr.getResults().size();
    insertResults(pr, size);
    final int calculationId = calculation.getCalculationId();
    final PartialCalculationResult resultsBefore = CalculationInfoRepository.getCalculationResults(getCalcConnection(), calculationId);
    assertEquals("Shoud be 4 receptors inserted", size, resultsBefore.getResults().size());
    return calculationId;
  }

  private void assertDeleteResult(final int calculationId, final int size, final double emission) throws SQLException {
    final PartialCalculationResult resultsAfter = CalculationInfoRepository.getCalculationResults(getCalcConnection(), calculationId);
    assertEquals("Shoud be " + Integer.toString(size) + " receptors after deletion", size, resultsAfter.getResults().size());
    assertEquals("The " + emission + " should be present", emission, resultsAfter.getResults().get(0).getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 0.0001);
    assertEquals("The 0.0501 should be present", 0.0501, resultsAfter.getResults().get(1).getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 0.0001);
  }

  private void assertKeepResult(final int calculationId, final int size, final double emission) throws SQLException {
    final PartialCalculationResult resultsAfter = CalculationInfoRepository.getCalculationResults(getCalcConnection(), calculationId);
    assertEquals("Shoud be " + Integer.toString(size) + " receptors after deletion", size, resultsAfter.getResults().size());
    assertEquals("The " + emission + " should be present", emission,
        resultsAfter.getResults().get(0).getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 0.0001);
    assertEquals("The 0.0499 should be present", 0.0499, resultsAfter.getResults().get(1).getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 0.0001);
    assertEquals("The 0.0501 should be present", 0.0501, resultsAfter.getResults().get(2).getEmissionResult(EmissionResultKey.NH3_DEPOSITION), 0.0001);
  }

  @Test
  public void testUpdateCalculationState() throws SQLException {
    Boolean updated = CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.RUNNING);
    assertNotNull("Updated", updated);
    assertTrue("Updated", updated);
    calculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertEquals("Status", CalculationState.RUNNING, calculation.getState());
    updated = CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.CANCELLED);
    assertTrue("Updated", updated);
    calculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertEquals("Status", CalculationState.CANCELLED, calculation.getState());
    updated = CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    assertTrue("Updated", updated);
    calculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertEquals("Status", CalculationState.COMPLETED, calculation.getState());
    updated = CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.INITIALIZED);
    assertTrue("Updated", updated);
    calculation = CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId());
    assertEquals("Status", CalculationState.INITIALIZED, calculation.getState());
    final boolean unknownUpdated = CalculationRepository.updateCalculationState(getCalcConnection(), -1, CalculationState.RUNNING);
    assertFalse("Update for non-existing calculation ID", unknownUpdated);
  }

  @Test
  public void testInsertCalculationResults() throws SQLException {
    insertCalculationResults();
  }

  @Test
  public void testInsertCalculationResultsUnsafe() throws SQLException {
    final PartialCalculationResult calculationResult1 =
        getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    final int inserted1 = CalculationRepository.insertCalculationResultsUnsafe(getCalcConnection(), calculation.getCalculationId(),
        calculationResult1.getResults());
    // 3 receptors * 2 result types = 6 results.
    assertEquals("Number of emission results inserted", 6, inserted1);

    //inserting a second time works, but only with different substances.
    final PartialCalculationResult calculationResult2 =
        getExampleOPSOutputData(EmissionResultKey.PM10_CONCENTRATION, EmissionResultKey.PM10_NUM_EXCESS_DAY);
    final int inserted2 = CalculationRepository.insertCalculationResultsUnsafe(getCalcConnection(), calculation.getCalculationId(),
        calculationResult2.getResults());
    // 3 receptors * 2 result types = 6 results. However, num excess days won't be persisted. Hence 3 results.
    assertEquals("Number of emission results inserted", 3, inserted2);

    //try to insert the first again, and it results in an exception.
    try {
      CalculationRepository.insertCalculationResultsUnsafe(getCalcConnection(), calculation.getCalculationId(),
          calculationResult1.getResults());
      fail("expected a SQL exception at this point");
    } catch (final SQLException e) {
      assertEquals("SQL state", "23505", e.getSQLState());
    }
  }

  @Test
  public void testGetCalculationResultSets() throws SQLException {
    insertCalculationResults();
    final Collection<EmissionResultKey> set =
        CalculationRepository.getCalculationResultSets(getCalcConnection(), calculation.getCalculationId());
    assertEquals("Should have 3 emission result types in database", 3, set.size());
  }

  @Test
  public void testInsertCalculationPoints() throws SQLException {
    final PartialCalculationResult partialCalculationResult = insertCalculationPoints(TEST_SUBSTANCE_2);
    // the ID returned is a serial, so can't guess what number it'll be besides not being 0.
    assertNotEquals("Number of emission results inserted", 0, partialCalculationResult.getCalculationId());
  }

  @Test
  public void testInsertCalculationPointResults() throws SQLException {
    final PartialCalculationResult partialCalculationResult = getExampleCalculationPointOutputData(TEST_SUBSTANCE_2);
    final int inserted = CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(),
        partialCalculationResult.getResults());
    // 6 results, 4 result types for one receptor, 2 for the other.
    assertEquals("Number of emission results inserted", partialCalculationResult.getResults().size() * 3, inserted);
  }

  @Test
  public void testinsertCalculationResultsBatch() throws SQLException {
    final int firstBatchSize = 10000;
    final int secondBatchSize = 500;

    // 10km is about 10000 entries.
    final List<AeriusResultPoint> results1 = createResults(firstBatchSize);
    final int inserted1 = CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(), results1);
    assertEquals("Number of inserted emission results", firstBatchSize * 2, inserted1); // Times 2 because 2 emission result types.

    final List<AeriusResultPoint> results2 = createResults(secondBatchSize);
    // This second batch should return 0 inserted as they are all already in the database.
    final int inserted2 = CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(), results2);
    assertEquals("Number of inserted emission results", firstBatchSize * 2, inserted1 + inserted2);
  }

  private static ArrayList<AeriusResultPoint> createResults(final int size) {
    final ArrayList<AeriusResultPoint> results = new ArrayList<>();
    for (int i = 1; i <= size; i++) {
      final AeriusResultPoint rp = new AeriusResultPoint();
      rp.setId(i);
      rp.setEmissionResult(EmissionResultKey.NH3_CONCENTRATION, RECEPTOR_1_CONCENTRATION + i);
      rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, RECEPTOR_1_DEPOSITION + i);
      results.add(rp);
    }
    return results;
  }

  @Test(expected = IllegalArgumentException.class)
  public void testinsertCalculationResultsWithoutOptions() throws SQLException {
    final PartialCalculationResult partialCalculationResult =
        getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);

    CalculationRepository.insertCalculationResults(getCalcConnection(), -1, partialCalculationResult.getResults());
    fail("Should throw an SQL exception because you can't insert a calculation result for a calculating without calculation options.");
  }

  @Test
  public void testInsertCalculationCustomJob() throws SQLException {
    CalculationRepository.insertCalculationBatchOptions(getCalcConnection(), calculation.getCalculationId(),
        "SomeDescription", "C:\\DUMBFILE.DMB;D:\\DOESNTMATTERMUCH");
    assertNotNull("Saving should be used (this assert is just here so there's something to test...).", calculation);
  }

  @Test
  public void testInsertDuplicatePartialCalculationResults() throws SQLException {
    insertCalculationResults();
    final PartialCalculationResult partialCalculationResult =
        getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    final AeriusResultPoint rp = new AeriusResultPoint();
    rp.setId(99);
    rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 160.0);
    partialCalculationResult.add(rp);
    // Shouldn't insert the previously inserted results again.
    final int inserted = CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(),
        partialCalculationResult.getResults());
    assertEquals("Number of emission results inserted", 1, inserted);
  }

  @Test
  public void testGetCalculatedPoints() throws SQLException {
    final PartialCalculationResult customPointResults = insertCalculationPoints(TEST_SUBSTANCE_2);
    //trick to set the right calculation point set id:
    //The calc id returned from insertCalculationPoints is actually the calculation point set ID...
    removeCalculationResults();
    createCalculation(customPointResults.getCalculationId());
    insertCalculationResults();
    CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(),
        customPointResults.getResults());
    final ArrayList<AeriusPoint> calculatedPoints = CalculationRepository.getCalculatedPoints(getCalcConnection(), calculation.getCalculationId());
    int calculationPoints = 0;
    int receptorPoints = 0;
    for (final AeriusPoint calculatedPoint : calculatedPoints) {
      assertNotEquals("ID should be set", 0, calculatedPoint.getId());
      assertNotEquals("X coord should be set", 0.0, calculatedPoint.getX(), 1E-8);
      assertNotEquals("Y coord should be set", 0.0, calculatedPoint.getY(), 1E-8);
      if (calculatedPoint.getPointType() == AeriusPointType.POINT) {
        calculationPoints++;
        assertNotNull("Label shouldn't be null", calculatedPoint.getLabel());
      } else if (calculatedPoint.getPointType() == AeriusPointType.RECEPTOR) {
        receptorPoints++;
      } else {
        fail("Did not expect point of type " + calculatedPoint.getPointType());
      }
    }
    assertEquals("Nr of receptors", 3, receptorPoints);
    assertEquals("Nr of calculation points", 2, calculationPoints);
  }

  @Test
  public void testInsertCalculationsWithoutPoints() throws SQLException {
    final CalculationPointList calculationPoints = new CalculationPointList();
    final CalculatedScenario cs = getExampleCalculatedScenario(calculationPoints);

    CalculationRepository.insertCalculations(getCalcConnection(), cs, true);
    assertNull("Without calculation points, no point set ID", cs.getCalculationPointSetId());
    assertPersistedCalculation(cs);
  }

  @Test
  public void testInsertCalculationsWithPoints() throws SQLException {
    final CalculationPointList calculationPoints = new CalculationPointList();
    calculationPoints.add(new AeriusPoint(1, AeriusPointType.POINT, 1, 1));
    calculationPoints.add(new AeriusPoint(2, AeriusPointType.POINT, 30, 40));
    final CalculatedScenario cs = getExampleCalculatedScenario(calculationPoints);

    CalculationRepository.insertCalculations(getCalcConnection(), cs, true);
    assertNotNull("With calculation list filled with points, there should be a calculation point set ID", cs.getCalculationPointSetId());
    assertPersistedCalculation(cs);
  }

  private void assertPersistedCalculation(final CalculatedScenario cs) throws SQLException {
    for (final Calculation calculation : cs.getCalculations()) {
      assertNotEquals("Calculation ID", 0, calculation.getCalculationId());
      assertNotNull("Calculation in database", CalculationRepository.getCalculation(getCalcConnection(), calculation.getCalculationId()));
    }
  }

  private CalculatedScenario getExampleCalculatedScenario(final CalculationPointList calculationPoints) {
    final Scenario scenario = new Scenario();
    scenario.addSources(createSources());
    scenario.addResultPoints(createResults(10));
    scenario.setCalculationPoints(calculationPoints);
    final CalculatedScenario cs = CalculatedScenarioUtil.toCalculatedScenario(scenario, 0, -1);
    cs.setOptions(getOptions());
    cs.setYear(SharedConstants.getCurrentYear());

    assertNull("Calculation point set ID", cs.getCalculationPointSetId());
    for (final Calculation calculation : cs.getCalculations()) {
      assertEquals("Calculation ID", 0, calculation.getCalculationId());
    }
    return cs;
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }

}
