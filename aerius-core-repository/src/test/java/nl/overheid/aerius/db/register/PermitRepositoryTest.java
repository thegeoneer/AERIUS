/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link PermitRepositoryTest}.
 */
public class PermitRepositoryTest extends RequestTestBase {

  @Test
  public void testGetPermit() throws SQLException, AeriusException, IOException {
    final Permit app = getInsertedExamplePermit();

    final Permit appFromDB = PermitRepository.getPermit(getRegConnection(), app.getId());
    assertPermitAfterInsert(appFromDB);
    assertEquals("PDF reference", app.getReference(), appFromDB.getReference());
    assertPermitComparison(app, appFromDB);
    assertEquals("Current state", app.getRequestState(), appFromDB.getRequestState());
    assertEquals("Corporation", app.getScenarioMetaData().getCorporation(), appFromDB.getScenarioMetaData().getCorporation());
    assertEquals("Project name", app.getScenarioMetaData().getProjectName(), appFromDB.getScenarioMetaData().getProjectName());

    final Permit appFromDB0 = PermitRepository.getSkinnedPermit(getRegConnection(), 0);
    assertNull("Permit with 0 ID", appFromDB0);
  }

  @Test
  public void testGetPermitKeyByReference() throws SQLException, AeriusException, IOException {
    final Permit permit = getInsertedExamplePermit();

    final PermitKey permitKeyFromDB = PermitRepository.getPermitKey(getRegConnection(), permit.getReference());
    assertEquals("Permit proposed emission values", permit.getAuthority().getCode(),
        permitKeyFromDB.getAuthorityCode());
    assertEquals("Permit proposed emission values", permit.getDossierMetaData().getDossierId(), permitKeyFromDB.getDossierId());

    final PermitKey permitKeyFromDBUnknown = PermitRepository.getPermitKey(getRegConnection(), "SomeUnknownRef");
    assertNull("getting app with unknown reference", permitKeyFromDBUnknown);
  }

  @Test
  public void testGetPermitByKey() throws SQLException, AeriusException, IOException {
    final Permit app = getInsertedExamplePermit();

    final PermitKey rightKey = app.getPermitKey();
    final Permit appFromDB = PermitRepository.getPermitByKey(getRegConnection(), rightKey);
    assertPermitAfterInsert(appFromDB);

    final PermitKey wrongKeyDossier = new PermitKey("SomeUnknownDossier", rightKey.getAuthorityCode());
    Permit appFromDBUnknown = PermitRepository.getSkinnedPermitByKey(getRegConnection(), wrongKeyDossier);
    assertNull("getting app with unknown dossier ID", appFromDBUnknown);

    final PermitKey wrongKeyAuthority = new PermitKey(rightKey.getDossierId(), "SomeUnknownAuthority");
    appFromDBUnknown = PermitRepository.getSkinnedPermitByKey(getRegConnection(), wrongKeyAuthority);
    assertNull("getting app with unknown authority", appFromDBUnknown);
  }

  @Test
  public void testGetPermitPDFContent() throws SQLException, AeriusException, IOException {
    final Permit permit = getExamplePermit();
    final InsertRequestFile insertRequestFile = getFakeInsertRequestFile();
    insertRequestFile.setFileContent(getPDFContent());
    PermitModifyRepository.insertNewPermit(getRegConnection(), permit, getExampleUserProfile(), insertRequestFile);

    final byte[] pdfContentFromDb = PermitRepository.getPermitPDFContent(getRegConnection(), permit.getPermitKey());
    assertEquals("PDF content length in DB", insertRequestFile.getFileContent().length, pdfContentFromDb.length);
    assertArrayEquals("PDF content in DB", insertRequestFile.getFileContent(), pdfContentFromDb);
  }

  @Test
  public void testGetPermits() throws SQLException, AeriusException, IOException {
    //assure 2 permits in the DB.
    final Permit olderPermit = getInsertedExamplePermit();
    final Permit app = getInsertedExamplePermit();

    List<Permit> permits = PermitRepository.getPermits(getRegConnection(), Integer.MAX_VALUE, 0, new PermitFilter());
    assertFalse("apps shouldn't be empty", permits.isEmpty());
    for (final Permit permit : permits) {
      assertRetrievedPermit(permit);
    }
    //last inserted should be top.
    assertEquals("PDF reference", app.getReference(), permits.get(0).getReference());
    permits = PermitRepository.getPermits(getRegConnection(), 0, 0, new PermitFilter());
    assertEquals("Number of permits in list with maxResults 0, offset 0", 0, permits.size());
    permits = PermitRepository.getPermits(getRegConnection(), 1, Integer.MAX_VALUE, new PermitFilter());
    assertEquals("Number of permits in list with maxResults 1, offset MAX_VALUE", 0, permits.size());
    permits = PermitRepository.getPermits(getRegConnection(), 1, 0, new PermitFilter());
    assertEquals("Number of permits in list with maxResults 1, offset 0", 1, permits.size());
    //last inserted should be top.
    assertEquals("PDF reference", app.getReference(), permits.get(0).getReference());
    //should have the same emissionvalues
    final EmissionValueKey keyNH3 = new EmissionValueKey(Substance.NH3);
    assertEquals("Emission NH3", app.getSituations().get(SituationType.PROPOSED).getTotalEmissionValues().getEmission(keyNH3),
        permits.get(0).getSituations().get(SituationType.PROPOSED).getTotalEmissionValues().getEmission(keyNH3), 0.0001);
    final EmissionValueKey keyNOX = new EmissionValueKey(Substance.NOX);
    assertEquals("Emission NOx", app.getSituations().get(SituationType.PROPOSED).getTotalEmissionValues().getEmission(keyNOX),
        permits.get(0).getSituations().get(SituationType.PROPOSED).getTotalEmissionValues().getEmission(keyNOX), 0.0001);
    permits = PermitRepository.getPermits(getRegConnection(), 1, 1, new PermitFilter());
    assertEquals("Number of permits in list with maxResults 1, offset 1", 1, permits.size());
    //olderPermit should be 2nd.
    assertEquals("PDF reference", olderPermit.getReference(), permits.get(0).getReference());
  }

  @Test
  public void testSearch() throws SQLException, AeriusException {
    List<Permit> foundPermits = PermitRepository.search(getRegConnection(), "arch-str");
    assertNotNull("Permit list shouldn't be null", foundPermits);
    assertTrue("Shouldn't find one permit", foundPermits.isEmpty());

    Permit insertedPermit = getInsertedExamplePermit();

    //search by partial reference.
    final String partialReference = insertedPermit.getReference().substring(2, insertedPermit.getReference().length() - 1);
    foundPermits = PermitRepository.search(getRegConnection(), partialReference);
    assertEquals("Should find one permit", 1, foundPermits.size());
    assertEquals("Found ID", insertedPermit.getId(), foundPermits.get(0).getId());

    //search by corporation
    foundPermits = PermitRepository.search(getRegConnection(), insertedPermit.getScenarioMetaData().getCorporation());
    assertFalse("Should find at least one permit",foundPermits.isEmpty());

    //search by project name
    foundPermits = PermitRepository.search(getRegConnection(), insertedPermit.getScenarioMetaData().getProjectName());
    assertFalse("Should find at least one permit",foundPermits.isEmpty());

    //update dossier ID
    insertedPermit = PermitRepository.getPermitByKey(getRegConnection(), insertedPermit.getPermitKey());
    final PermitKey key = insertedPermit.getPermitKey();
    insertedPermit.getDossierMetaData().setDossierId("SomeWeird_Dossier_ID");
    PermitModifyRepository.updatePermit(getRegConnection(), key, insertedPermit.getDossierMetaData(),
        getExampleUserProfile(), insertedPermit.getLastModified());

    foundPermits = PermitRepository.search(getRegConnection(), partialReference);
    assertEquals("Should find one permit (still found by reference)", 1, foundPermits.size());
    assertEquals("Found ID", insertedPermit.getId(), foundPermits.get(0).getId());

    foundPermits = PermitRepository.search(getRegConnection(), "ird_Dos");
    assertEquals("Should find one permit (by dossier ID)", 1, foundPermits.size());
    assertEquals("Found ID", insertedPermit.getId(), foundPermits.get(0).getId());

    Permit newPermit = getInsertedExamplePermit();
    final PermitKey newPermitKey = newPermit.getPermitKey();
    newPermit = PermitRepository.getPermitByKey(getRegConnection(), newPermitKey);
    newPermit.getDossierMetaData().setDossierId("AnotherWeird_Dossier_ID");
    PermitModifyRepository.updatePermit(getRegConnection(), newPermitKey, newPermit.getDossierMetaData(),
        getExampleUserProfile(), newPermit.getLastModified());
    foundPermits = PermitRepository.search(getRegConnection(), "ird_Dos");
    assertEquals("Should find two permits (existing + new one)", 2, foundPermits.size());
  }

  @Test
  public void testGetPermitsWithinRadius() throws SQLException, AeriusException, IOException {
    final int maxRadius = 100;

    //assure 3 permits in the DB.
    final Permit targetPermit = getInsertedExamplePermit();

    final Permit withinRadius = getExamplePermit();
    withinRadius.getPoint().setX(withinRadius.getPoint().getX() + maxRadius);
    PermitModifyRepository.insertNewPermit(getRegConnection(), withinRadius, getExampleUserProfile(), getFakeInsertRequestFile());

    final Permit outsideRadius = getExamplePermit();
    outsideRadius.getPoint().setX(outsideRadius.getPoint().getX() + maxRadius + 1);
    PermitModifyRepository.insertNewPermit(getRegConnection(), outsideRadius, getExampleUserProfile(), getFakeInsertRequestFile());

    final List<Permit> permits = PermitRepository.getPermitsWithinRadius(getRegConnection(), targetPermit.getId(), maxRadius);
    assertFalse("apps shouldn't be empty", permits.isEmpty());
    assertEquals("Number of permits retrieved", 1, permits.size());
    assertRetrievedPermit(permits.get(0));
    assertEquals("PDF reference", withinRadius.getReference(), permits.get(0).getReference());
  }

  @Test
  public void testIsPermitFinished() throws SQLException, AeriusException, IOException {
    final Permit request = getInsertedExamplePermit();

    boolean finished = PermitRepository.isCalculationFinished(getRegConnection(), request.getPermitKey());

    assertFalse("Calculation for permit shouldn't be finished right after insert", finished);

    final Calculation calculation1 = getExampleCalculation();
    final int calculationId1 = calculation1.getCalculationId();
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, calculationId1, request);
    final Calculation calculation2 = getExampleCalculation();
    final int calculationId2 = calculation2.getCalculationId();
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.CURRENT, calculationId2, request);

    finished = PermitRepository.isCalculationFinished(getRegConnection(), request.getPermitKey());
    assertFalse("Calculation for permit shouldn't be finished right after calculation has started", finished);
    CalculationRepository.updateCalculationState(getRegConnection(), calculationId1, CalculationState.COMPLETED);
    finished = PermitRepository.isCalculationFinished(getRegConnection(), request.getPermitKey());
    assertFalse("Calculation for permit shouldn't be finished if only one calculation has finished", finished);
    CalculationRepository.updateCalculationState(getRegConnection(), calculationId2, CalculationState.COMPLETED);
    finished = PermitRepository.isCalculationFinished(getRegConnection(), request.getPermitKey());
    assertTrue("Calculation for permit should be finished if all calculations are completed.", finished);
  }

  @Test
  public void testGetPermitWithoutDecreeAssigned() throws SQLException, AeriusException, IOException {
    testGetPermitWithoutDecree(RequestState.ASSIGNED);
  }

  @Test
  public void testGetPermitWithoutDecreeAssignedFinal() throws SQLException, AeriusException, IOException {
    testGetPermitWithoutDecree(RequestState.ASSIGNED_FINAL);
  }

  @Test
  public void testGetPermitWithoutDecreeRejectedWithoutSpace() throws SQLException, AeriusException, IOException {
    testGetPermitWithoutDecree(RequestState.REJECTED_WITHOUT_SPACE);
  }

  private void testGetPermitWithoutDecree(final RequestState target) throws SQLException, AeriusException, IOException {
    final Permit oldestPermit = PermitRepository.getOldestWithoutDecrees(getRegConnection(), RequestFileType.DECREE);

    final Permit newPermit = getInsertedExamplePermit();
    addCalculation(newPermit);
    updatePermitState(newPermit, RequestState.QUEUED);
    if (oldestPermit != null) {
      updatePermitReceivedDate(newPermit, new Date(oldestPermit.getDossierMetaData().getReceivedDate().getTime() - 10));
    }
    Permit currentOldestPermit = PermitRepository.getOldestWithoutDecrees(getRegConnection(), RequestFileType.DECREE);
    validateOldPermit("after inserting new queued permit", oldestPermit, currentOldestPermit);

    if (target == RequestState.REJECTED_WITHOUT_SPACE) {
      updatePermitState(newPermit, RequestState.PENDING_WITHOUT_SPACE);
    } else {
      updatePermitState(newPermit, RequestState.PENDING_WITH_SPACE);
    }
    currentOldestPermit = PermitRepository.getOldestWithoutDecrees(getRegConnection(), RequestFileType.DECREE);
    validateOldPermit("after switching to pending_with(out)_space", oldestPermit, currentOldestPermit);

    if (target == RequestState.ASSIGNED_FINAL) {
      updatePermitState(newPermit, RequestState.ASSIGNED);
    }

    updatePermitState(newPermit, target);
    currentOldestPermit = PermitRepository.getOldestWithoutDecrees(getRegConnection(), RequestFileType.DECREE);
    assertEquals("Permit retrieved should be the new permit after switching to " + target, newPermit.getId(), currentOldestPermit.getId());

    final InsertRequestFile insertRequestFile = getFakeInsertRequestFile();
    insertRequestFile.setRequestFileType(RequestFileType.DECREE);
    RequestModifyRepository.insertFileContent(getRegConnection(), newPermit.getId(), insertRequestFile);

    currentOldestPermit = PermitRepository.getOldestWithoutDecrees(getRegConnection(), RequestFileType.DECREE);
    validateOldPermit("after inserting decree", oldestPermit, currentOldestPermit);
    currentOldestPermit = PermitRepository.getOldestWithoutDecrees(getRegConnection(), RequestFileType.DETAIL_DECREE);
    validateOldPermit("when checking detail decree after inserting decree", newPermit, currentOldestPermit);
  }

  private void validateOldPermit(final String description, final Permit oldestExistingPermit, final Permit currentPermit) {
    if (oldestExistingPermit != null) {
      assertEquals("Expected the old existing permit " + description, oldestExistingPermit.getId(), currentPermit.getId());
    } else {
      assertNull("Expected no permit " + description, currentPermit);
    }
  }

  private void updatePermitReceivedDate(final Permit permit, final Date targetDate) throws SQLException, AeriusException {
    //reload to get the proper modified date
    final Permit reloadedPermit = PermitRepository.getPermitByKey(getRegConnection(), permit.getPermitKey());
    reloadedPermit.getDossierMetaData().setReceivedDate(targetDate);
    PermitModifyRepository.updatePermit(getRegConnection(), reloadedPermit.getPermitKey(), reloadedPermit.getDossierMetaData(),
        getExampleUserProfile(), reloadedPermit.getLastModified());
  }

  private void updatePermitState(final Permit permit, final RequestState state) throws SQLException, AeriusException {
    //reload to get the proper modified date
    final Permit reloadedPermit = PermitRepository.getPermitByKey(getRegConnection(), permit.getPermitKey());
    PermitModifyRepository.updateState(getRegConnection(), reloadedPermit.getPermitKey(), state,
        getExampleUserProfile(), reloadedPermit.getLastModified());
  }

}
