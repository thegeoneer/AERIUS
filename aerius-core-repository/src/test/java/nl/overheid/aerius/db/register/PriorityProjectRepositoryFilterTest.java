/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectFilter;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PriorityProjectProgress;
import nl.overheid.aerius.shared.domain.register.PriorityProjectSortableAttribute;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for querying the database for {@link PriorityProjectFilter}.
 */
public class PriorityProjectRepositoryFilterTest extends RequestFilterTestBase<PriorityProjectFilter, PriorityProject, PriorityProjectSortableAttribute> {

  private static final String TEST_REFERENCE = "pp_Utrecht_11_7530";

  private PriorityProject existingProject;

  @Override
  void init() throws SQLException, AeriusException {
    //Should be test priority projects in DB
    final PriorityProjectKey key = PriorityProjectRepository.getPriorityProjectKey(getRegConnection(), TEST_REFERENCE);

    existingProject = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
  }

  @Override
  PriorityProjectFilter getNewFilter() {
    return new PriorityProjectFilter();
  }

  @Override
  List<PriorityProject> getList(final PriorityProjectFilter filter) throws SQLException {
    return PriorityProjectRepository.getPriorityProjects(getRegConnection(), Integer.MAX_VALUE, 0, filter);
  }

  @Override
  Date getDateForRequest(final PriorityProject request) {
    return request.getDossierMetaData().getReceivedDate();
  }

  @Override
  protected Date getTestDate() {
    final Calendar calendar = new GregorianCalendar();
    calendar.setTime(existingProject.getDossierMetaData().getReceivedDate());
    calendar.add(Calendar.DATE, -1);
    calendar.add(Calendar.SECOND, -1);
    return calendar.getTime();
  }

  @Override
  Authority getAuthorityForRequest(final PriorityProject request) {
    return getTestAuthority();
  }

  @Override
  void fillForFullFilter(final PriorityProjectFilter filter) {
    filter.setProgressState(getProjectProgressState(existingProject));
    filter.getAuthorities().clear();
    filter.getAuthorities().add(getTestYetAnotherAuthority());
  }

  @Override
  Authority getTestAuthority() {
    final Authority testAuthority = new Authority();
    testAuthority.setId(1);
    return testAuthority;
  }

  @Override
  Province getTestProvince() {
    final Province testProvince = new Province();
    testProvince.setProvinceId(7);
    return testProvince;
  }

  @Override
  Sector getTestSector() {
    return existingProject.getSector();
  }

  @Override
  int getNewerRequestCalculationId() throws SQLException {
    return 0;
  }

  @Override
  @Test
  public void testFilterAssessmentArea() throws SQLException {
    //currently not possible to obtain less results (currently every priority project has a result everywhere due to test data)
    assertFilterAssessmentArea(false);
  }

  @Override
  @Test
  public void testFilterAuthority() throws SQLException {
    //specific test for Priority project filter
    final PriorityProjectFilter filter = getNewFilter();
    filter.getAuthorities().add(getTestYetAnotherAuthority());
    final List<PriorityProject> requests = getList(filter);
    defaultValidations("Authorities", requests);
    for (final PriorityProject request : requests) {
      assertEquals("Every authority should be the filtered one", getTestYetAnotherAuthority(), request.getAuthority());
    }

    //multiple authorities
    final Authority anotherAuthority = new Authority();
    anotherAuthority.setId(1);
    filter.getAuthorities().add(anotherAuthority);
    final List<PriorityProject> moreAuthoritiesPPs = getList(filter);
    assertFalse("Multiple states: requests shouldn't be empty", moreAuthoritiesPPs.isEmpty());
    assertTrue("Multiple states: Should be more requests now", moreAuthoritiesPPs.size() > requests.size());
    for (final PriorityProject request : moreAuthoritiesPPs) {
      assertTrue("Every authority should be in the filter", filter.getAuthorities().contains(request.getAuthority()));
    }
  }

  private Authority getTestYetAnotherAuthority() {
    final Authority testAuthority = new Authority();
    testAuthority.setId(11);
    return testAuthority;
  }

  @Override
  PriorityProjectSortableAttribute[] getPossibleSortAttributes() {
    return PriorityProjectSortableAttribute.values();
  }

  @Test
  public void testProgress() throws SQLException {
    //specific test for Priority project filter
    final PriorityProjectFilter filter = getNewFilter();
    //ensure we're filtering on an existing progress.
    final PriorityProjectProgress existingProgress = getProjectProgressState(existingProject);
    filter.setProgressState(existingProgress);
    final List<PriorityProject> requests = getList(filter);
    defaultValidations("PriorityProjectProgress", requests);
    for (final PriorityProject request : requests) {
      assertEquals("Every request should match the filtered progress", existingProgress, getProjectProgressState(request));
    }

    //special case for the 'done' one (it tests a different attribute...)
    filter.setProgressState(PriorityProjectProgress.DONE);
    final List<PriorityProject> donePriorityProjects = getList(filter);
    assertFalse("Expecting at least some (due to test data)", donePriorityProjects.isEmpty());
    for (final PriorityProject request : donePriorityProjects) {
      assertEquals("Every request should be done", PriorityProjectProgress.DONE, getProjectProgressState(request));
    }
  }

  private PriorityProjectProgress getProjectProgressState(final PriorityProject priorityProject) {
    return PriorityProjectProgress.determineProgress(priorityProject);
  }
}
