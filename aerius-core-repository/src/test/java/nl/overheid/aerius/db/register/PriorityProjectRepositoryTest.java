/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.register.AuditTrailChange;
import nl.overheid.aerius.shared.domain.register.AuditTrailItem;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectFilter;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link PriorityProjectRepositoryTest}.
 */
public class PriorityProjectRepositoryTest extends RequestTestBase {

  private static final String TEST_REFERENCE = "pp_Gelderland_5_3210";

  @Test
  public void testGetPriorityProject() throws SQLException, AeriusException, IOException {
    final PriorityProjectKey key = PriorityProjectRepository.getPriorityProjectKey(getRegConnection(), TEST_REFERENCE);
    assertEquals("Dossier ID", TEST_REFERENCE, key.getDossierId());
    assertEquals("Authority code", "Gelderland", key.getAuthorityCode());

    assertTrue("Priority project should exist by key",
        PriorityProjectRepository.priorityProjectExists(getRegConnection(), key));
    assertFalse("Bogus authority project shouldn't exist",
        PriorityProjectRepository.priorityProjectExists(getRegConnection(), new PriorityProjectKey(TEST_REFERENCE, "XYZ")));

    PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
    //ensure an audit trail exists for both the main project and the sub projects.
    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    RequestAuditRepository.addChange(changes, AuditTrailType.AERIUS_ID, "id nr 1", "id nr 2");
    RequestAuditRepository.saveAuditTrailItem(getRegConnection(), priorityProject, changes, getExampleUserProfile());
    for (final PrioritySubProject subProject : priorityProject.getSubProjects()) {
      final ArrayList<AuditTrailChange> subProjectChanges = new ArrayList<>();
      RequestAuditRepository.addChange(subProjectChanges, AuditTrailType.STATE, RequestState.INITIAL, RequestState.ASSIGNED);
      RequestAuditRepository.saveAuditTrailItem(getRegConnection(), subProject, subProjectChanges, getExampleUserProfile());
    }

    priorityProject = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
    validateFullPriorityProject("Normal by key", priorityProject);

    priorityProject = PriorityProjectRepository.getPriorityProject(getRegConnection(), priorityProject.getId(), getRegMessagesKey());
    validateFullPriorityProject("Normal by ID", priorityProject);

    priorityProject = PriorityProjectRepository.getSkinnedPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
    validateSkinnedPriorityProject("Skinned by key", priorityProject);

    priorityProject = PriorityProjectRepository.getSkinnedPriorityProject(getRegConnection(), priorityProject.getId(), getRegMessagesKey());
    validateSkinnedPriorityProject("Skinned by ID", priorityProject);
  }

  @Test
  public void testGetPriorityProjects() throws SQLException, AeriusException, IOException {
    final List<PriorityProject> projects = PriorityProjectRepository.getPriorityProjects(getRegConnection(), Integer.MAX_VALUE, 0,
        new PriorityProjectFilter());
    assertFalse("List shouldn't be empty", projects.isEmpty());
    for (final PriorityProject project : projects) {
      assertRetrievedPriorityProject(project);
    }
  }

  private void validateFullPriorityProject(final String description, final PriorityProject priorityProject) {
    assertRetrievedPriorityProject(priorityProject);
    assertEquals(description + ": Reference", priorityProject.getReference(), TEST_REFERENCE);
    assertFalse(description + ": Situations", priorityProject.getSituations().isEmpty());
    assertFalse(description + ": Subprojects", priorityProject.getSubProjects().isEmpty());
    assertFalse(description + ": Change history", priorityProject.getChangeHistory().isEmpty());

    final Set<AuditTrailType> foundTypes = new HashSet<>();
    for (final AuditTrailItem auditTrailItem : priorityProject.getChangeHistory()) {
      for (final AuditTrailChange change : auditTrailItem.getItems()) {
        foundTypes.add(change.getType());
      }
    }
    assertTrue(description + ": Found the priority project change", foundTypes.contains(AuditTrailType.AERIUS_ID));
    assertTrue(description + ": Found the subproject change", foundTypes.contains(AuditTrailType.STATE));

    assertNotNull(description + ": Should be indicative receptor info", priorityProject.getIndicativeReceptorInfo());
    assertNotNull(description + ": Should be indicative receptor", priorityProject.getIndicativeReceptorInfo().getReceptor());
    assertNotEquals(description + ": Max fraction used", 0.0, priorityProject.getIndicativeReceptorInfo().getMaxFractionUsed(), 1E-5);
    assertFalse(description + ": Indicative receptor area map", priorityProject.getIndicativeReceptorInfo().getAssessmentAreas().isEmpty());
    for (final Entry<AssessmentArea, ArrayList<HabitatType>> entry : priorityProject.getIndicativeReceptorInfo().getAssessmentAreas().entrySet()) {
      assertNotEquals(description + ": assessment area ID", 0, entry.getKey().getId());
      assertNotNull(description + ": assessment area name", entry.getKey().getName());
      assertFalse(description + ": habitats", entry.getValue().isEmpty());
      for (final HabitatType habitat : entry.getValue()) {
        assertNotEquals(description + ": habitat ID", 0, habitat.getId());
        assertNotNull(description + ": habitat code", habitat.getCode());
        assertNotNull(description + ": habitat name", habitat.getName());
      }
    }
  }

  private void validateSkinnedPriorityProject(final String description, final PriorityProject priorityProject) {
    assertEquals(description + ": Reference", priorityProject.getReference(), TEST_REFERENCE);
    assertTrue(description + ": Situations", priorityProject.getSituations().isEmpty());
    assertTrue(description + ": Subprojects", priorityProject.getSubProjects().isEmpty());
    assertNull(description + ": Shouldn't be indicative receptor info", priorityProject.getIndicativeReceptorInfo());
  }

}
