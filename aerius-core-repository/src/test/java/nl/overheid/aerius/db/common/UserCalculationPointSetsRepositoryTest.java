/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.scenario.ScenarioUserRepository;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.domain.user.UserCalculationPointSetMetadata;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link UserCalculationPointSetsRepository}.
*/
public class UserCalculationPointSetsRepositoryTest extends BaseDBTest {

  private static final String TEST_EMAIL = "aerius@example.com";
  private ScenarioUser scenarioUser;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    ScenarioUser tempScenarioUser = new ScenarioUser();
    tempScenarioUser.setApiKey("NotImportant");
    tempScenarioUser.setEmailAddress(TEST_EMAIL);
    tempScenarioUser.setEnabled(true);
    ScenarioUserRepository.createUser(getCalcConnection(), tempScenarioUser);
    scenarioUser = ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL);
    assertNotNull("Creating ScenarioUser failed.", scenarioUser);
  }

  @Test
  public void insertUserSingleCalculationPointSet() throws SQLException {
    final UserCalculationPointSetMetadata setMetadata = storeOneUCPS("BargerveenCPSet");
    checkStoredUCPS(setMetadata);
  }

  @Test
  public void getUserCalculationPointSets() throws Exception {
    List<UserCalculationPointSetMetadata> beforeList =
        UserCalculationPointSetsRepository.getUserCalculationPointSetsForUser(getCalcConnection(), scenarioUser.getId());
    assertTrue("List of UserCalculationPointSets should be empty.", beforeList.isEmpty());
    final List<UserCalculationPointSetMetadata> setMetadataList = new ArrayList<>();
    setMetadataList.add(storeOneUCPS("BargerveenCPSet"));
    setMetadataList.add(storeOneUCPS("NogEenBargerveenCPSet"));
    List<UserCalculationPointSetMetadata> list2 =
        UserCalculationPointSetsRepository.getUserCalculationPointSetsForUser(getCalcConnection(), scenarioUser.getId());
    assertEquals("List of UserCalculationPointSets should be two long.", setMetadataList.size(), list2.size());
  }

  @Test
  public void getUserCalculationPointSetIsFailed() throws Exception {
    final UserCalculationPointSetMetadata storedSetMetadata =
        UserCalculationPointSetsRepository.getUserCalculationPointSetByName(getCalcConnection(), scenarioUser.getId(), "SomeNameNotPresent");
    assertNull(storedSetMetadata);
  }

  /**
   * @return
   * @throws SQLException
   */
  private UserCalculationPointSetMetadata storeOneUCPS(final String setName) throws SQLException {
    final AeriusPoint calculationPoint = new AeriusPoint();
    calculationPoint.setId(1);
    calculationPoint.setLabel("Bargerveen");
    calculationPoint.setPointType(AeriusPointType.POINT);
    calculationPoint.setSrid(6406390);
    calculationPoint.setX(265755);
    calculationPoint.setY(521869);
    final List<AeriusPoint> calculationPoints = new ArrayList<>();
    calculationPoints.add(calculationPoint);
    final UserCalculationPointSetMetadata setMetadata = new UserCalculationPointSetMetadata();
    setMetadata.setName(setName);
    setMetadata.setUserId(scenarioUser.getId());
    setMetadata.setDescription("BargerveenDescription");
    try {
      UserCalculationPointSetsRepository.unsafeInsertUserCalculationPointSet(getCalcConnection(), setMetadata, calculationPoints);
    }
    catch (final AeriusException e) {
      assertNull("Inserting a set of one calculationpoint failed totally unexpectedly.", e);
    }
    return setMetadata;
  }

  /**
   * @param setMetadata
   * @throws SQLException
   */
  private void checkStoredUCPS(final UserCalculationPointSetMetadata setMetadata) throws SQLException {
    final UserCalculationPointSetMetadata storedSetMetadata =
        UserCalculationPointSetsRepository.getUserCalculationPointSetByName(getCalcConnection(), scenarioUser.getId(), setMetadata.getName());
    assertNotNull(storedSetMetadata);
  }
}
