/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.postgresql.util.PSQLException;

import com.google.gson.Gson;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.dashboard.NoticeDashboardFilter;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for FilterRepository.
 */
public class FilterRepositoryTest extends BaseDBTest {
  @SuppressWarnings("serial")
  private class NewPermitFilter extends PermitFilter {

    private Date valid;
    private int count;

    public Date getValid() {
      return valid;
    }

    @SuppressWarnings("unused")
    public void setValid(final Date valid) {
      this.valid = valid;
    }

    public int getCount() {
      return count;
    }

    @SuppressWarnings("unused")
    public void setCount(final int count) {
      this.count = count;
    }
  }

  @Test
  public void testPermitFilterRepository() throws SQLException, AeriusException {
    //some base information
    final UserProfile userProfile = TestDomain.getDefaultExampleProfile(getRegConnection());
    final PermitFilter filter = new PermitFilter();
    Date dateFrom = new GregorianCalendar(2015, 5, 21).getTime();
    final Date dateTill = new GregorianCalendar(2017, 2, 18).getTime();
    filter.setFrom(dateFrom);
    filter.setTill(dateTill);

    //test if inserting actually works.
    FilterRepository.insertOrUpdateFilter(getRegConnection(), filter, userProfile);
    //test if retrieving works.
    PermitFilter fromDB = FilterRepository.getFilter(getRegConnection(), userProfile, PermitFilter.class);
    assertNotNull("Returned filter", fromDB);
    assertEquals("Returned filter date from", dateFrom, fromDB.getFrom());
    assertEquals("Returned filter date till", dateTill, fromDB.getTill());
    assertTrue("Returned filter states set should be empty", fromDB.getStates().isEmpty());

    //test if updating works.
    dateFrom = new GregorianCalendar(2016, 6, 20).getTime();
    filter.setFrom(dateFrom);
    filter.getStates().add(RequestState.ASSIGNED);

    FilterRepository.insertOrUpdateFilter(getRegConnection(), filter, userProfile);
    fromDB = FilterRepository.getFilter(getRegConnection(), userProfile, PermitFilter.class);
    assertNotNull("Returned filter", fromDB);
    assertEquals("Returned filter date from", dateFrom, fromDB.getFrom());
    assertEquals("Returned filter date till", dateTill, fromDB.getTill());
    assertFalse("Returned filter states set shouldn't be empty", fromDB.getStates().isEmpty());
    assertTrue("Returned filter state should contain the assigned state", fromDB.getStates().contains(RequestState.ASSIGNED));
  }

  @Test
  public void testNoticeFilterRepository() throws SQLException, AeriusException {
    //some base information
    final UserProfile userProfile = TestDomain.getDefaultExampleProfile(getRegConnection());
    final NoticeFilter filter = new NoticeFilter();
    Date dateFrom = new GregorianCalendar(2015, 5, 21).getTime();
    final Date dateTill = new GregorianCalendar(2017, 2, 18).getTime();
    filter.setFrom(dateFrom);
    filter.setTill(dateTill);

    //test if inserting actually works.
    FilterRepository.insertOrUpdateFilter(getRegConnection(), filter, userProfile);
    //test if retrieving works.
    NoticeFilter fromDB = FilterRepository.getFilter(getRegConnection(), userProfile, NoticeFilter.class);
    assertNotNull("Returned filter", fromDB);
    assertEquals("Returned filter date from", dateFrom, fromDB.getFrom());
    assertEquals("Returned filter date till", dateTill, fromDB.getTill());
    assertTrue("Returned filter states set should be empty", fromDB.getProvinces().isEmpty());

    //test if updating works.
    dateFrom = new GregorianCalendar(2016, 6, 20).getTime();
    filter.setFrom(dateFrom);
    filter.getProvinces().add(new Province() {
      {
        setId(5);
        setName("Geel");
        setProvinceId(123);
      }
    });

    FilterRepository.insertOrUpdateFilter(getRegConnection(), filter, userProfile);
    fromDB = FilterRepository.getFilter(getRegConnection(), userProfile, NoticeFilter.class);
    assertNotNull("Returned filter", fromDB);
    assertEquals("Returned filter date from", dateFrom, fromDB.getFrom());
    assertEquals("Returned filter date till", dateTill, fromDB.getTill());
    assertFalse("Returned filter states set shouldn't be empty", fromDB.getProvinces().isEmpty());
  }

  @Test
  public void gsonTest()
  {
    // test if we can read a new gson object in old object and reverse

    final String newObject = "{\"valid\":\"Mar 18, 2018 12:00:00 AM\",\"count\":999,\"from\":\"Jun 21, 2015 12:00:00 AM\",\"till\":\"Mar 18, 2017 12:00:00 AM\",\"states\":[],\"provinceIds\":[],\"authorityIds\":[]}";
    final String oldObject = "{\"from\":\"Jun 21, 2015 12:00:00 AM\",\"till\":\"Mar 18, 2017 12:00:00 AM\",\"states\":[],\"provinceIds\":[],\"authorityIds\":[]}";
    final Date dateFrom = new GregorianCalendar(2015, 5, 21).getTime();
    final Date dateTill = new GregorianCalendar(2017, 2, 18).getTime();
    final Date validTill = new GregorianCalendar(2018, 2, 18).getTime();

    final Gson gson = new Gson();
    final PermitFilter filter = gson.fromJson(newObject, PermitFilter.class);
    assertNotNull("Returned filter", filter);
    assertEquals("Returned filter date from", dateFrom, filter.getFrom());
    assertEquals("Returned filter date till", dateTill, filter.getTill());

    final NewPermitFilter newFilter = gson.fromJson(newObject, NewPermitFilter.class);
    assertEquals("Returned filter date till", validTill, newFilter.getValid());
    assertEquals("Returned filter number", 999, newFilter.getCount());

    final NewPermitFilter oldFilter = gson.fromJson(oldObject, NewPermitFilter.class);
    assertEquals("Returned filter date till", null, oldFilter.getValid());
    assertEquals("Returned filter number", 0, oldFilter.getCount());

  }

  @Test
  public void testUnknownUser() throws SQLException {
    final PermitFilter fromDB = FilterRepository.getFilter(getRegConnection(), new UserProfile(), PermitFilter.class);
    assertNotNull("Filter for unknown user will still return an empty filter", fromDB);
    assertTrue("Filter should be of right type", fromDB instanceof PermitFilter);

    final PermitFilter filter = new PermitFilter();
    //test if inserting throws an exception as expected
    try {
      FilterRepository.insertOrUpdateFilter(getRegConnection(), filter, new UserProfile());
      fail("Expected exception with unknown user");
    } catch (final PSQLException e) {
      //expected.
    }
  }

  public void testOldRequestFilters() {
    final String oldNoticeFilterJson = "{\\\"from\\\":\\\"Aug 3, 2015 12:00:00 AM\\\",\\\"till\\\":\\\"Aug 22, 2015 12:00:00 AM\\\",\\\"provinces\\\":[]}";
    final Gson gson = new Gson();
    final NoticeFilter noticeFilter = gson.fromJson(oldNoticeFilterJson, NoticeFilter.class);
    assertNotNull("Returned notice filter", noticeFilter);
    assertNotNull("Returned notice filter from date", noticeFilter.getFrom());
    assertNotNull("Returned notice filter till date", noticeFilter.getTill());
    assertNotNull("Returned notice filter provinces", noticeFilter.getProvinces());
    assertTrue("Returned notice filter provinces should be empty", noticeFilter.getProvinces().isEmpty());

    final String oldPermitFilterJson = "{\\\"states\\\":[\\\"QUEUED\\\"],\\\"provinces\\\":[{\\\"provinceId\\\":3,\\\"name\\\":\\\"Flevoland\\\"}],"
        + "\\\"authorityIds\\\":[1]}";
    final PermitFilter permitFilter = gson.fromJson(oldPermitFilterJson, PermitFilter.class);
    assertNotNull("Returned permit filter", permitFilter);
    assertNotNull("Returned permit filter from date", permitFilter.getFrom());
    assertNotNull("Returned permit filter till date", permitFilter.getTill());
    assertNotNull("Returned permit filter provinces", permitFilter.getProvinces());
    assertFalse("Returned permit filter provinces shouldn't be empty", permitFilter.getProvinces().isEmpty());
    assertNotNull("Returned permit filter authorities", permitFilter.getAuthorities());
    assertTrue("Returned permit filter authorities should be empty (field name changed)", permitFilter.getAuthorities().isEmpty());
  }

  /**
   * Tests if it'll hurt when a filter's content is changed.
   * This is not the best unittest, it actually tests how gson behaves instead of our repository,
   * so it's pretty tied in with the inner workings of the repository.
   * Still, it does explain how the filters will react to refactorings:
   * If a filter is updated in a new version of the webapp, it won't cause exceptions in the repository.
   * @throws AeriusException
   */
  @Test
  public void testWrongPermitType() throws SQLException, AeriusException {
    //some base information
    final UserProfile userProfile = TestDomain.getDefaultExampleProfile(getRegConnection());
    final PermitFilter filter = new PermitFilter();
    final Date dateFrom = new GregorianCalendar(2015, 5, 21).getTime();
    final Date dateTill = new GregorianCalendar(2017, 2, 18).getTime();
    filter.setFrom(dateFrom);
    filter.setTill(dateTill);
    filter.getStates().add(RequestState.ASSIGNED);
    filter.getStates().add(RequestState.PENDING_WITH_SPACE);

    //insert the permit filter.
    FilterRepository.insertOrUpdateFilter(getRegConnection(), filter, userProfile);
    //manually update the type of the just inserted filter to a NoticeDashboardFilter
    //first delete any existing
    try (final PreparedStatement upsertPS =
        getRegConnection().prepareStatement("DELETE FROM user_filters WHERE user_id = ? AND filter_type = ?")) {
      int parameterIdx = 1;
      upsertPS.setInt(parameterIdx++, userProfile.getId());
      upsertPS.setString(parameterIdx++, NoticeDashboardFilter.class.getCanonicalName());

      upsertPS.executeUpdate();
    }
    //then update.
    try (final PreparedStatement upsertPS =
        getRegConnection().prepareStatement("UPDATE user_filters SET filter_type = ? WHERE user_id = ? AND filter_type = ?")) {
      int parameterIdx = 1;
      upsertPS.setString(parameterIdx++, NoticeDashboardFilter.class.getCanonicalName());
      upsertPS.setInt(parameterIdx++, userProfile.getId());
      upsertPS.setString(parameterIdx++, PermitFilter.class.getCanonicalName());

      upsertPS.executeUpdate();
    }
    //test if retrieving it works.
    final NoticeDashboardFilter fromDB = FilterRepository.getFilter(getRegConnection(), userProfile, NoticeDashboardFilter.class);
    assertNotNull("Returned filter shouldn't be null, it just should be empty...", fromDB);
    assertNull("Returned filter province ID should be null", fromDB.getProvinceId());

    //insert the permit filter again to test if changing enum values in a set hurts.
    FilterRepository.insertOrUpdateFilter(getRegConnection(), filter, userProfile);
    final Gson gson = new Gson();
    String jsonFilter = gson.toJson(filter);
    jsonFilter = jsonFilter.replaceAll("ASSIGNED", "SomeWeirdValue");
    jsonFilter = jsonFilter.replaceAll("PENDING_WITH_SPACE", "SomeOtherValue");
    jsonFilter = jsonFilter.replaceAll(",\\\"provinces\\\"\\:\\[\\]", "");
    //then update.
    updateJsonFilter(jsonFilter, userProfile, PermitFilter.class);
    //now retrieve and check
    PermitFilter permitFilterFromDB = FilterRepository.getFilter(getRegConnection(), userProfile, PermitFilter.class);
    assertNotNull("Returned filter", permitFilterFromDB);
    assertNotNull("Returned filter provinces set", permitFilterFromDB.getProvinces());
    assertTrue("Returned filter provinces set should be empty", permitFilterFromDB.getProvinces().isEmpty());
    assertFalse("You'd expect the set to be empty now, but it actually contains the value NULL...", permitFilterFromDB.getStates().isEmpty());
    assertTrue("As can be seen by this", permitFilterFromDB.getStates().contains(null));
    assertEquals("It's the only state too...", 1, permitFilterFromDB.getStates().size());

    //insert the permit filter again and update to incorrect json type.
    FilterRepository.insertOrUpdateFilter(getRegConnection(), filter, userProfile);
    String incorrectJsonFilter = gson.toJson(filter);
    incorrectJsonFilter = incorrectJsonFilter.replaceAll(",\\\"provinces\\\"\\:\\[\\]", ",\\\"provinces\\\":3");
    updateJsonFilter(incorrectJsonFilter, userProfile, PermitFilter.class);
    permitFilterFromDB = FilterRepository.getFilter(getRegConnection(), userProfile, PermitFilter.class);
    assertNotNull("Returned filter", permitFilterFromDB);
  }

  private void updateJsonFilter(final String jsonFilter, final UserProfile userProfile, final Class<?> filterClass) throws SQLException {
    try (final PreparedStatement upsertPS =
        getRegConnection().prepareStatement("UPDATE user_filters SET filter_content = ?::json WHERE user_id = ? AND filter_type = ?")) {
      int parameterIdx = 1;
      upsertPS.setString(parameterIdx++, jsonFilter);
      upsertPS.setInt(parameterIdx++, userProfile.getId());
      upsertPS.setString(parameterIdx++, filterClass.getCanonicalName());

      upsertPS.executeUpdate();
    }
  }
}
