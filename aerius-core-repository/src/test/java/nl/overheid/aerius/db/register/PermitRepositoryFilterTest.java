/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.PermitSortableAttribute;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for querying the database for {@link PermitFilter}.
 */
public class PermitRepositoryFilterTest extends RequestFilterTestBase<PermitFilter, Permit, PermitSortableAttribute> {

  private Permit olderPermit;
  private Permit newerPermit;

  @Override
  void init() throws SQLException, AeriusException {
    //assure 2 permits in the DB.
    olderPermit = getPermitWithCalculation();
    newerPermit = getPermitWithCalculation();

    //assure the olderPermit has a startdate before the other one
    olderPermit.getDossierMetaData().setReceivedDate(new GregorianCalendar(2008, 8, 8).getTime());
    PermitModifyRepository.updatePermit(getRegConnection(), olderPermit.getPermitKey(), olderPermit.getDossierMetaData(), getExampleUserProfile(),
        PermitRepository.getSkinnedPermit(getRegConnection(), olderPermit.getId()).getLastModified());
    //assure the newerPermit has a different state
    PermitModifyRepository.updateState(getRegConnection(), newerPermit.getPermitKey(), RequestState.QUEUED, getExampleUserProfile(),
        PermitRepository.getSkinnedPermit(getRegConnection(), newerPermit.getId()).getLastModified());
  }

  @Override
  PermitFilter getNewFilter() {
    return new PermitFilter();
  }

  @Override
  List<Permit> getList(final PermitFilter filter) throws SQLException {
    return PermitRepository.getPermits(getRegConnection(), Integer.MAX_VALUE, 0, filter);
  }

  @Override
  Date getDateForRequest(final Permit request) {
    return request.getDossierMetaData().getReceivedDate();
  }

  @Override
  protected Date getTestDate() {
    final Calendar calendar = new GregorianCalendar();
    calendar.setTime(newerPermit.getDossierMetaData().getReceivedDate());
    calendar.add(Calendar.DATE, -2);
    return calendar.getTime();
  }

  @Override
  Authority getAuthorityForRequest(final Permit request) {
    return request.getAuthority();
  }

  @Override
  void fillForFullFilter(final PermitFilter filter) {
    for (final RequestState state : RequestState.values()) {
      if (state != RequestState.REJECTED_FINAL) {
        filter.getStates().add(state);
      }
    }
  }

  @Override
  Authority getTestAuthority() {
    return newerPermit.getAuthority();
  }

  @Override
  Province getTestProvince() {
    final Province testProvince = new Province();
    testProvince.setProvinceId(7);
    return testProvince;
  }

  @Override
  Sector getTestSector() {
    return newerPermit.getSector();
  }

  @Override
  int getNewerRequestCalculationId() throws SQLException {
    return RequestRepository.getCalculationIds(getRegConnection(), newerPermit.getReference()).get(SituationType.PROPOSED);
  }

  @Test
  public void testState() throws SQLException {
    //specific tests for permit filter
    final PermitFilter filter = getNewFilter();
    filter.getStates().add(olderPermit.getRequestState());
    final List<Permit> requests = getList(filter);
    defaultValidations("State", requests);
    for (final Permit app : requests) {
      assertEquals("Every state should be the initial state", olderPermit.getRequestState(), app.getRequestState());
    }

    //multiple state
    filter.getStates().add(RequestState.QUEUED);
    final List<Permit> moreStatePermits = getList(filter);
    assertFalse("Multiple states: apps shouldn't be empty", moreStatePermits.isEmpty());
    assertTrue("Multiple states: Should be more apps now", moreStatePermits.size() > requests.size());
    for (final Permit app : moreStatePermits) {
      assertTrue("Every state should be in the filter", filter.getStates().contains(app.getRequestState()));
    }
  }

  @Override
  PermitSortableAttribute[] getPossibleSortAttributes() {
    return PermitSortableAttribute.values();
  }

}
