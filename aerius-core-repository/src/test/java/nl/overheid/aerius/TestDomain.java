/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestSituation;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;
import nl.overheid.aerius.shared.domain.sector.category.VehicleType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.FarmAdditionalLodgingSystem;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingCustomEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.domain.source.FarmReductiveLodgingSystem;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.NavigationDirection;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource.SourceCategoryType;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource.PlanEmission;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.domain.source.VehicleCustomEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleSpecificEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Convenience class to avoid having to write the same test code over and over again.
 *
 */
public class TestDomain {

  public static final DiurnalVariationSpecification DV_CONTINUOUS = new DiurnalVariationSpecification(0, "CONTINUOUS");
  public static final DiurnalVariationSpecification DV_INDUSTRIAL_ACTIVITY = new DiurnalVariationSpecification(1, "INDUSTRIAL_ACTIVITY");
  public static final DiurnalVariationSpecification DV_SPACE_HEATING = new DiurnalVariationSpecification(2, "SPACE_HEATING");
  public static final DiurnalVariationSpecification DV_SPACE_TRAFFIC = new DiurnalVariationSpecification(3, "TRAFFIC");
  public static final DiurnalVariationSpecification DV_SPACE_ANIMAL_HOUSING = new DiurnalVariationSpecification(4, "ANIMAL_HOUSING");
  public static final DiurnalVariationSpecification DV_SPACE_FERTILISER = new DiurnalVariationSpecification(5, "FERTILISER");

  public static final int XCOORD_1 = 136558;
  public static final int YCOORD_1 = 455251;
  public static final int XCOORD_2 = 208413;
  public static final int YCOORD_2 = 474162;
  public static final int XCOORD_3 = XCOORD_1;
  public static final int YCOORD_3 = YCOORD_2;

  public static final int YEAR = 2020;

  public static final int BINNENVELD_ID = 65;
  public static final int DWINGELDERVELD_ID = 30;
  public static final int VELUWE_ID = 57;
  public static final int DUINEN_TEXEL_ID = 2;
  public static final int DUINEN_VLIELAND_ID = 3;
  public static final int SOLLEVELD_ID = 99;
  public static final int WOOLDSE_VEEN_ID = 64;
  public static final int OLDENZAAL_ID = 50;
  public static final int SCHOORLSE_DUINEN_ID = 86;
  public static final int NOORDHOLLANDS_DUINRESERVAAT_ID = 87;

  public static final EmissionValueKey EVK_WITH_YEAR_NH3 =
      new EmissionValueKey(YEAR, Substance.NH3);
  public static final EmissionValueKey EVK_WITH_YEAR_NOX =
      new EmissionValueKey(YEAR, Substance.NOX);

  public static final EmissionResultKey DEFAULT_ERK_NH3 = EmissionResultKey.NH3_DEPOSITION;
  public static final EmissionResultKey DEFAULT_ERK_NOX = EmissionResultKey.NOX_DEPOSITION;

  public static final String USERROLE_REGISTER_SUPERUSER = "register_superuser";
  public static final String USERROLE_REGISTER_EDITOR = "register_editor";
  public static final String USERROLE_REGISTER_VIEWER = "register_viewer";

  private static final String INLAND_WATERWAY_CODE = "CEMT_VIb";

  private static final String LINESTRING = "LINESTRING(";
  private static final int FARM_SECTOR = 4120;

  private final SectorCategories categories;

  public TestDomain(final PMF pmf) throws SQLException {
    this.categories = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
  }

  public Sector getSectorById(final int id) {
    return categories.getSectorById(id);
  }

  public FarmEmissionSource getFarmEmissionSource() {
    final FarmEmissionSource source = new FarmEmissionSource();
    source.setSector(getSectorById(FARM_SECTOR));
    final FarmLodgingCategory animalCategory1 = categories.getFarmLodgingCategories().getFarmLodgingSystemCategories().get(0);
    final FarmLodgingStandardEmissions lodging1 = new FarmLodgingStandardEmissions();
    lodging1.setCategory(animalCategory1);
    lodging1.setAmount(3);

    final FarmLodgingCategory animalCategory2 = categories.getFarmLodgingCategories().getFarmLodgingSystemCategories().get(1);
    final FarmLodgingStandardEmissions lodging2 = new FarmLodgingStandardEmissions();
    lodging2.setCategory(animalCategory2);
    lodging2.setAmount(1000);
    lodging2.setSystemDefinition(animalCategory2.getFarmLodgingSystemDefinitions().get(0));

    final FarmAdditionalLodgingSystem additional1 = new FarmAdditionalLodgingSystem();
    additional1.setAmount(500);
    additional1.setCategory(categories.getFarmLodgingCategories().getFarmAdditionalLodgingSystemCategories().get(0));
    lodging2.getLodgingAdditional().add(additional1);
    final FarmAdditionalLodgingSystem additional2 = new FarmAdditionalLodgingSystem();
    additional2.setAmount(250);
    additional2.setCategory(categories.getFarmLodgingCategories().getFarmAdditionalLodgingSystemCategories().get(1));
    lodging2.getLodgingAdditional().add(additional2);

    final FarmReductiveLodgingSystem reductive1 = new FarmReductiveLodgingSystem();
    reductive1.setCategory(categories.getFarmLodgingCategories().getFarmReductiveLodgingSystemCategories().get(0));
    lodging2.getLodgingReductive().add(reductive1);

    final FarmReductiveLodgingSystem reductive2 = new FarmReductiveLodgingSystem();
    reductive2.setCategory(categories.getFarmLodgingCategories().getFarmReductiveLodgingSystemCategories().get(1));
    lodging2.getLodgingReductive().add(reductive2);

    final FarmLodgingCustomEmissions lodging3 = new FarmLodgingCustomEmissions();
    lodging3.setAmount(908);
    lodging3.setDescription("Schaap");
    lodging3.getEmissionFactors().setEmission(Substance.NH3, 2000.0);

    source.getEmissionSubSources().add(lodging1);
    source.getEmissionSubSources().add(lodging2);
    source.getEmissionSubSources().add(lodging3);
    return source;
  }

  public SRM2EmissionSource getSRM2EmissionSource() {
    final SRM2EmissionSource emissionSource = new SRM2EmissionSource();
    emissionSource.setSector(getSectorById(RoadType.FREEWAY.getSectorId()));
    final VehicleStandardEmissions lt = new VehicleStandardEmissions();

    lt.setStagnationFraction(0.2);
    lt.setVehicles(980, TimeUnit.DAY);
    final RoadEmissionCategory emissionCategory1 =
        categories.getRoadEmissionCategories().getRoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 100, null);
    lt.setEmissionCategory(emissionCategory1);

    final VehicleStandardEmissions hf = new VehicleStandardEmissions();

    hf.setVehicles(200, TimeUnit.DAY);
    final RoadEmissionCategory emissionCategory2 =
        categories.getRoadEmissionCategories().getRoadEmissionCategory(RoadType.FREEWAY, VehicleType.HEAVY_FREIGHT, false, 80, null);
    hf.setEmissionCategory(emissionCategory2);

    OnRoadMobileSourceCategory mobileSourceCategory1 = null;
    OnRoadMobileSourceCategory mobileSourceCategory2 = null;
    for (final OnRoadMobileSourceCategory sourceCategory : categories.getOnRoadMobileSourceCategories()) {
      if (mobileSourceCategory1 == null) {
        mobileSourceCategory1 = sourceCategory;
      } else {
        mobileSourceCategory2 = sourceCategory;
        break;
      }
    }
    final VehicleSpecificEmissions specific1 = new VehicleSpecificEmissions();
    specific1.setCategory(mobileSourceCategory1);
    specific1.setVehicles(30000, TimeUnit.DAY);
    specific1.setRoadType(RoadType.FREEWAY);
    final VehicleSpecificEmissions specific2 = new VehicleSpecificEmissions();
    specific2.setCategory(mobileSourceCategory2);
    specific2.setVehicles(15000, TimeUnit.DAY);
    specific2.setRoadType(RoadType.FREEWAY);

    final VehicleCustomEmissions custom = new VehicleCustomEmissions();
    custom.setDescription("Custom bike");
    custom.setVehicles(3244, TimeUnit.DAY);
    custom.setEmission(Substance.NOX, 0.04);

    emissionSource.getEmissionSubSources().add(lt);
    emissionSource.getEmissionSubSources().add(hf);
    emissionSource.getEmissionSubSources().add(specific1);
    emissionSource.getEmissionSubSources().add(specific2);
    emissionSource.getEmissionSubSources().add(custom);

    return emissionSource;
  }

  public OffRoadMobileEmissionSource getOffRoadMobileEmissionSource(final OffRoadMobileEmissionSource emissionSource) {
    final OffRoadMobileSourceCategory mobileSourceCategory1 = categories.getOffRoadMobileSourceCategories().get(0);
    final OffRoadMobileSourceCategory mobileSourceCategory2 = categories.getOffRoadMobileSourceCategories().get(1);
    final OffRoadVehicleEmissionSubSource vehicleEmissionValues = new OffRoadVehicleEmissionSubSource();
    vehicleEmissionValues.setId(1);
    vehicleEmissionValues.setStageCategory(mobileSourceCategory1);
    vehicleEmissionValues.setFuelage(30000);
    vehicleEmissionValues.setHeatContent(999);
    vehicleEmissionValues.setSpread(999);
    vehicleEmissionValues.setEmissionHeight(999);
    vehicleEmissionValues.setEmissionNOX(999);
    vehicleEmissionValues.setCategoryType(SourceCategoryType.STAGE_CLASS);
    vehicleEmissionValues.setName("My Little Offroader");
    emissionSource.getEmissionSubSources().add(vehicleEmissionValues);
    final OffRoadVehicleEmissionSubSource vehicleEmissionValuesCustom = new OffRoadVehicleEmissionSubSource();
    vehicleEmissionValuesCustom.setId(2);
    vehicleEmissionValuesCustom.setStageCategory(mobileSourceCategory2);
    vehicleEmissionValuesCustom.setFuelage(999);
    vehicleEmissionValuesCustom.setHeatContent(20);
    vehicleEmissionValuesCustom.setSpread(5);
    vehicleEmissionValuesCustom.setEmissionHeight(10);
    vehicleEmissionValuesCustom.setEmissionNOX(101010);
    vehicleEmissionValuesCustom.setCategoryType(SourceCategoryType.DEVIATING_CLASS);
    vehicleEmissionValuesCustom.setName("My Big Custom Bike");
    emissionSource.getEmissionSubSources().add(vehicleEmissionValuesCustom);
    return emissionSource;
  }

  public OffRoadMobileEmissionSource getOffRoadMobileToolsEmissionSource(final OffRoadMobileEmissionSource emissionSource) {
    final OffRoadMobileSourceCategory mobileSourceCategory1 = categories.getOffRoadMobileSourceCategories().get(0);
    final OffRoadMobileSourceCategory mobileSourceCategory2 = categories.getOffRoadMobileSourceCategories().get(1);
    final OffRoadVehicleEmissionSubSource vehicleEmissionValues = new OffRoadVehicleEmissionSubSource();
    vehicleEmissionValues.setId(1);
    vehicleEmissionValues.setStageCategory(mobileSourceCategory1);
    vehicleEmissionValues.setFuelage(30000);
    vehicleEmissionValues.setHeatContent(999);
    vehicleEmissionValues.setSpread(999);
    vehicleEmissionValues.setEmissionHeight(999);
    vehicleEmissionValues.setEmissionNOX(999);
    vehicleEmissionValues.setCategoryType(SourceCategoryType.STAGE_CLASS);
    vehicleEmissionValues.setName("Stage tooltje");
    emissionSource.getEmissionSubSources().add(vehicleEmissionValues);
    final OffRoadVehicleEmissionSubSource vehicleEmissionValuesCustom = new OffRoadVehicleEmissionSubSource();
    vehicleEmissionValuesCustom.setId(2);
    vehicleEmissionValuesCustom.setStageCategory(mobileSourceCategory2);
    vehicleEmissionValuesCustom.setFuelage(999);
    vehicleEmissionValuesCustom.setHeatContent(20);
    vehicleEmissionValuesCustom.setSpread(5);
    vehicleEmissionValuesCustom.setEmissionHeight(10);
    vehicleEmissionValuesCustom.setEmissionNOX(101010);
    vehicleEmissionValuesCustom.setCategoryType(SourceCategoryType.DEVIATING_CLASS);
    vehicleEmissionValuesCustom.setName("Deviating tooltje");
    emissionSource.getEmissionSubSources().add(vehicleEmissionValuesCustom);
    return emissionSource;
  }

  public MaritimeMooringEmissionSource getMooringMaritimeShipEmissionSource(final MaritimeMooringEmissionSource emissionSource) {
    final MooringMaritimeVesselGroup vesselGroupEmissionValues = new MooringMaritimeVesselGroup();
    vesselGroupEmissionValues.setId(0);
    vesselGroupEmissionValues.setCategory(categories.getMaritimeShippingCategories().get(0));
    vesselGroupEmissionValues.setName("Motorboot");
    vesselGroupEmissionValues.setNumberOfShips(30000, TimeUnit.YEAR);
    vesselGroupEmissionValues.setResidenceTime(100);

    final ShippingRoute route = new ShippingRoute();
    route.setId(1);
    route.setGeometry(new WKTGeometry(LINESTRING + XCOORD_2 + " " + YCOORD_2 + "," + XCOORD_3 + " " + YCOORD_3 + ")", 202));
    vesselGroupEmissionValues.setInlandRoute(route);
    final ShippingRoute maritimeRoute = new ShippingRoute();
    maritimeRoute.setId(3);
    maritimeRoute.setGeometry(new WKTGeometry(LINESTRING + XCOORD_3 + " " + YCOORD_3 + "," + XCOORD_2 + " " + YCOORD_2 + ")", 302));
    final MaritimeRoute mtr = new MaritimeRoute();
    mtr.setRoute(maritimeRoute);
    mtr.setShipMovements(vesselGroupEmissionValues.getNumberOfShipsPerYear() * 2, TimeUnit.YEAR);
    vesselGroupEmissionValues.getMaritimeRoutes().add(mtr);
    emissionSource.getEmissionSubSources().add(vesselGroupEmissionValues);

    final MooringMaritimeVesselGroup vesselGroupEmissionValues1 = new MooringMaritimeVesselGroup();
    vesselGroupEmissionValues1.setId(1);
    vesselGroupEmissionValues1.setCategory(categories.getMaritimeShippingCategories().get(1));
    vesselGroupEmissionValues1.setName("Puntertje");
    vesselGroupEmissionValues1.setNumberOfShips(15000, TimeUnit.YEAR);
    vesselGroupEmissionValues1.setResidenceTime(200);
    final ShippingRoute route1 = new ShippingRoute();
    route1.setId(2);
    route1.setGeometry(new WKTGeometry(LINESTRING + XCOORD_1 + " " + YCOORD_1 + "," + XCOORD_3 + " " + YCOORD_3 + ")", 202));
    vesselGroupEmissionValues1.setInlandRoute(route1);
    final ShippingRoute maritimeRoute1 = new ShippingRoute();
    maritimeRoute1.setId(4);
    maritimeRoute1.setGeometry(new WKTGeometry(LINESTRING + XCOORD_3 + " " + YCOORD_3 + "," + XCOORD_2 + " " + YCOORD_2 + ")", 50));
    final MaritimeRoute mtr1 = new MaritimeRoute();
    mtr1.setShipMovements(vesselGroupEmissionValues1.getNumberOfShipsPerYear() * 2, TimeUnit.YEAR);
    mtr1.setRoute(maritimeRoute1);
    vesselGroupEmissionValues1.getMaritimeRoutes().add(mtr1);
    emissionSource.getEmissionSubSources().add(vesselGroupEmissionValues1);
    return emissionSource;
  }

  public MaritimeRouteEmissionSource getShipEmissionRouteSource() {
    final MaritimeRouteEmissionSource emissionValues = new MaritimeRouteEmissionSource();
    final RouteMaritimeVesselGroup vesselGroupEmissionValues = new RouteMaritimeVesselGroup(ShippingMovementType.MARITIME);
    vesselGroupEmissionValues.setCategory(categories.getMaritimeShippingCategories().get(0));
    vesselGroupEmissionValues.setName("Motorboot");
    vesselGroupEmissionValues.setShipMovements(30000, TimeUnit.YEAR);
    emissionValues.getEmissionSubSources().add(vesselGroupEmissionValues);
    final RouteMaritimeVesselGroup vesselGroupEmissionValues1 = new RouteMaritimeVesselGroup(ShippingMovementType.MARITIME);
    vesselGroupEmissionValues1.setCategory(categories.getMaritimeShippingCategories().get(1));
    vesselGroupEmissionValues1.setName("Puntertje");
    vesselGroupEmissionValues1.setShipMovements(15000, TimeUnit.YEAR);
    emissionValues.getEmissionSubSources().add(vesselGroupEmissionValues1);
    return emissionValues;
  }

  public InlandRouteEmissionSource getInlandRouteEmissionValues() {
    final InlandRouteEmissionSource emissionValues = new InlandRouteEmissionSource();
    final InlandWaterwayType inlandWaterwayType = new InlandWaterwayType();
    inlandWaterwayType.setWaterwayCategory(categories.getInlandShippingCategories().getWaterwayCategoryByCode(INLAND_WATERWAY_CODE));
    inlandWaterwayType.setWaterwayDirection(WaterwayDirection.IRRELEVANT);
    emissionValues.setWaterwayCategory(inlandWaterwayType.getWaterwayCategory());
    emissionValues.setWaterwayDirection(inlandWaterwayType.getWaterwayDirection());
    final RouteInlandVesselGroup vesselGroupEmissionValues = new RouteInlandVesselGroup();
    vesselGroupEmissionValues.setCategory(categories.getInlandShippingCategories().getShipCategories().get(0));
    vesselGroupEmissionValues.setName("Duikboot");
    vesselGroupEmissionValues.setNumberOfShipsAtoB(50, TimeUnit.YEAR);
    vesselGroupEmissionValues.setNumberOfShipsBtoA(150, TimeUnit.YEAR);
    vesselGroupEmissionValues.setPercentageLadenAtoB(20);
    vesselGroupEmissionValues.setPercentageLadenBtoA(40);
    emissionValues.getEmissionSubSources().add(vesselGroupEmissionValues);
    final RouteInlandVesselGroup vesselGroupEmissionValues1 = new RouteInlandVesselGroup();
    vesselGroupEmissionValues1.setCategory(categories.getInlandShippingCategories().getShipCategories().get(1));
    vesselGroupEmissionValues1.setName("Veerpont");
    vesselGroupEmissionValues1.setNumberOfShipsAtoB(300, TimeUnit.YEAR);
    vesselGroupEmissionValues1.setNumberOfShipsBtoA(280, TimeUnit.YEAR);
    vesselGroupEmissionValues1.setPercentageLadenAtoB(80);
    vesselGroupEmissionValues1.setPercentageLadenBtoA(82);
    emissionValues.getEmissionSubSources().add(vesselGroupEmissionValues1);
    return emissionValues;
  }

  public InlandMooringEmissionSource getInlandMooringEmissionSource(final ShippingRoute shippingRoute) {
    final InlandMooringEmissionSource emissionSource = new InlandMooringEmissionSource();
    final InlandMooringVesselGroup vesselGroupEmissionValues = new InlandMooringVesselGroup();
    vesselGroupEmissionValues.setId(0);
    vesselGroupEmissionValues.setCategory(categories.getInlandShippingCategories().getShipCategories().get(0));
    vesselGroupEmissionValues.setName("Vliegende Hollander");
    vesselGroupEmissionValues.setResidenceTime(5);
    final InlandMooringRoute mooringRouteArrival = new InlandMooringRoute();
    mooringRouteArrival.setId(0);
    mooringRouteArrival.setDirection(NavigationDirection.ARRIVE);
    mooringRouteArrival.setPercentageLaden(20);
    mooringRouteArrival.setShipMovements(3500, TimeUnit.YEAR);
    final InlandWaterwayType mooringRouteArrivalWaterwayType = new InlandWaterwayType();
    mooringRouteArrivalWaterwayType.setWaterwayCategory(categories.getInlandShippingCategories().getWaterwayCategoryByCode(INLAND_WATERWAY_CODE));
    mooringRouteArrivalWaterwayType.setWaterwayDirection(WaterwayDirection.IRRELEVANT);
    shippingRoute.setWaterwayCategory(mooringRouteArrivalWaterwayType.getWaterwayCategory());
    shippingRoute.setWaterwayDirection(mooringRouteArrivalWaterwayType.getWaterwayDirection());
    mooringRouteArrival.setRoute(shippingRoute);
    vesselGroupEmissionValues.getRoutes().add(mooringRouteArrival);
    final InlandMooringRoute mooringRouteDepart = new InlandMooringRoute();
    mooringRouteDepart.setId(1);
    mooringRouteDepart.setDirection(NavigationDirection.DEPART);
    mooringRouteDepart.setPercentageLaden(64);
    mooringRouteDepart.setShipMovements(2050, TimeUnit.YEAR);
    mooringRouteDepart.setRoute(shippingRoute);
    final InlandWaterwayType mooringRouteDepartWaterwayType = new InlandWaterwayType();
    mooringRouteDepartWaterwayType.setWaterwayCategory(categories.getInlandShippingCategories().getWaterwayCategoryByCode(INLAND_WATERWAY_CODE));
    mooringRouteDepartWaterwayType.setWaterwayDirection(WaterwayDirection.IRRELEVANT);
    vesselGroupEmissionValues.getRoutes().add(mooringRouteDepart);

    emissionSource.getEmissionSubSources().add(vesselGroupEmissionValues);
    final InlandMooringVesselGroup vesselGroupEmissionValues1 = new InlandMooringVesselGroup();
    vesselGroupEmissionValues.setId(1);
    vesselGroupEmissionValues1.setCategory(categories.getInlandShippingCategories().getShipCategories().get(1));
    vesselGroupEmissionValues1.setName("Neeltje Jacoba");
    vesselGroupEmissionValues1.setResidenceTime(3);
    final InlandMooringRoute mooringRouteArrival1 = new InlandMooringRoute();
    mooringRouteArrival1.setId(0);
    mooringRouteArrival1.setDirection(NavigationDirection.ARRIVE);
    mooringRouteArrival1.setPercentageLaden(20);
    mooringRouteArrival1.setShipMovements(507, TimeUnit.YEAR);
    mooringRouteArrival1.setRoute(shippingRoute);
    final InlandWaterwayType mooringRouteArrivalWaterwayType1 = new InlandWaterwayType();
    mooringRouteArrivalWaterwayType1.setWaterwayCategory(categories.getInlandShippingCategories().getWaterwayCategoryByCode(INLAND_WATERWAY_CODE));
    mooringRouteArrivalWaterwayType1.setWaterwayDirection(WaterwayDirection.IRRELEVANT);
    vesselGroupEmissionValues1.getRoutes().add(mooringRouteArrival1);
    final InlandMooringRoute mooringRouteDepart1 = new InlandMooringRoute();
    mooringRouteDepart1.setId(1);
    mooringRouteDepart1.setDirection(NavigationDirection.DEPART);
    mooringRouteDepart1.setPercentageLaden(81);
    mooringRouteDepart1.setShipMovements(120, TimeUnit.YEAR);
    mooringRouteDepart1.setRoute(shippingRoute);
    final InlandWaterwayType mooringRouteDepartWaterwayType1 = new InlandWaterwayType();
    mooringRouteDepartWaterwayType1.setWaterwayCategory(categories.getInlandShippingCategories().getWaterwayCategoryByCode(INLAND_WATERWAY_CODE));
    mooringRouteDepartWaterwayType1.setWaterwayDirection(WaterwayDirection.IRRELEVANT);
    vesselGroupEmissionValues1.getRoutes().add(mooringRouteDepart1);
    emissionSource.getEmissionSubSources().add(vesselGroupEmissionValues1);
    return emissionSource;
  }

  public PlanEmissionSource getPlanEmissionSource() {
    final PlanCategory planCategory1 = categories.getPlanEmissionCategories().get(0);
    final PlanCategory planCategory2 = categories.getPlanEmissionCategories().get(1);
    final PlanEmissionSource source = new PlanEmissionSource();
    final PlanEmission planEmission = new PlanEmission();
    planEmission.setCategory(planCategory1);
    planEmission.setAmount(30000);
    planEmission.setName("Some appartments");
    source.getEmissionSubSources().add(planEmission);
    final PlanEmission planEmission1 = new PlanEmission();
    planEmission1.setCategory(planCategory2);
    planEmission1.setAmount(15000);
    planEmission1.setName("Some more appartments");
    source.getEmissionSubSources().add(planEmission1);
    return source;
  }

  public static GenericEmissionSource getGenericEmissionSource() {
    return getGenericEmissionSource(new GenericEmissionSource());
  }

  public static GenericEmissionSource getGenericEmissionSource(final GenericEmissionSource source) {
    source.setEmission(Substance.NH3, 657.0);
    return source;
  }

  public static <E extends EmissionSource> E getSource(final int id, final WKTGeometry geometry, final String label, final E source) {
    source.setId(id);
    source.setGeometry(geometry);
    try {
      final Point midPoint = GeometryUtil.middleOfGeometry(geometry);
      source.setX(midPoint.getX());
      source.setY(midPoint.getY());
    } catch (final AeriusException e) {
      //no worries, it's probably faulty for testing reasons.
    }
    source.setLabel(label);
    source.setSector(Sector.SECTOR_DEFAULT);
    //default characteristics for this sector.
    source.setSourceCharacteristics(getDefaultCharacteristics());
    return source;
  }

  public static OPSSourceCharacteristics getDefaultCharacteristics() {
    return new OPSSourceCharacteristics(0.28, 22, 0, 11, DV_INDUSTRIAL_ACTIVITY, 1800);
  }

  public static OPSSourceCharacteristics getNonDefaultCharacteristics() {
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics(564.584, 11.12, 5, 649.10, DV_INDUSTRIAL_ACTIVITY, 2);
    characteristics.setBuildingHeight(1.4);
    return characteristics;
  }

  public static EmissionSourceList getExampleSourceList() {
    final EmissionSourceList emissionSourceList = new EmissionSourceList();
    final Point point1 = new Point(XCOORD_1, YCOORD_1);
    emissionSourceList.add(getSource(1, new WKTGeometry(point1.toWKT(), 1), "ExampleSource1", getGenericEmissionSource()));
    final Point point2 = new Point(XCOORD_2, YCOORD_2);
    final GenericEmissionSource source2 = new GenericEmissionSource();
    source2.setEmission(Substance.NH3, 267.0);
    source2.setEmission(Substance.NOX, 901.0);
    emissionSourceList.add(getSource(2, new WKTGeometry(point2.toWKT(), 1), "ExampleSource2", source2));
    return emissionSourceList;
  }

  /**
   * Gets a profile from the DB, creating it in the DB if not there yet.
   * @throws AeriusException
   */
  public static UserProfile getExampleUserProfile(final Connection con, final AdminUserProfile userProfile) throws SQLException, AeriusException {
    UserProfile dbProfile = null;
    try {
      dbProfile = UserRepository.getUserProfileByName(con, userProfile.getName());
    } catch (final AeriusException e) {
      if (e.getReason() == Reason.USER_DOES_NOT_EXIST) {
        try {
          final AdminUserProfile createUser = userProfile.copyTo(new AdminUserProfile());

          UserRepository.createUser(con, createUser, "SuperUnknown");
          dbProfile = UserRepository.getUserProfileByName(con, userProfile.getName());
        } catch (final AeriusException e2) {
          //doesn't matter
        }
      } else {
        throw e;
      }
    }
    return dbProfile;
  }

  /**
   * Gets the default profile from the DB, creating it in the DB if not there yet.
   * @throws AeriusException
   */
  public static UserProfile getDefaultExampleProfile(final Connection con) throws SQLException, AeriusException {
    final AdminUserProfile profile = new AdminUserProfile();
    profile.setName("Boddicker");
    profile.setEmailAddress("something@example.com");
    profile.setAuthority(getDefaultExampleAuthority());
    profile.getRoles().add(UserRepository.getRole(con, TestDomain.USERROLE_REGISTER_SUPERUSER));
    return getExampleUserProfile(con, profile);
  }

  public static Authority getDefaultExampleAuthority() {
    final Authority testAuthority = new Authority();
    testAuthority.setId(1);
    testAuthority.setCode("Onbekend");
    return testAuthority;
  }

  public static void fillExamplePermit(final Permit permit, final String dossierId, final UserProfile handler) throws SQLException {
    fillExampleRequest(permit);
    final DossierMetaData dossierMetaData = new DossierMetaData();
    dossierMetaData.setDossierId(dossierId);
    dossierMetaData.setReceivedDate(new GregorianCalendar(2011, 5, 6).getTime());
    dossierMetaData.setHandler(handler);
    dossierMetaData.setRemarks("Serve the public trust, Protect the innocent, Uphold the law, ...");
    permit.setDossierMetaData(dossierMetaData);
    final EmissionValues emissionValues = new EmissionValues();
    emissionValues.setEmission(new EmissionValueKey(Substance.NH3), new Random().nextInt(1000000));
    emissionValues.setEmission(new EmissionValueKey(Substance.NOX), new Random().nextInt(1000000));
    emissionValues.setEmission(new EmissionValueKey(Substance.NOXNH3), new Random().nextInt(1000000));
    emissionValues.setEmission(new EmissionValueKey(Substance.PM10), new Random().nextInt(1000000));
    final RequestSituation situation = new RequestSituation();
    situation.setName("A Pickle");
    situation.setTotalEmissionValues(emissionValues);
    permit.getSituations().put(SituationType.PROPOSED, situation);
  }

  public static void fillExampleRequest(final Request request) throws SQLException {
    request.setStartYear(2020);
    request.setSegment(SegmentType.PROJECTS);
    final ScenarioMetaData metaData = new ScenarioMetaData();
    metaData.setReference(ReferenceUtil.generatePermitReference());
    metaData.setCorporation("Omni Consumer Products");
    metaData.setDescription("Building a better police force");
    metaData.setProjectName("RoboCop");
    request.setScenarioMetaData(metaData);
    final Point point = new Point(XCOORD_1, YCOORD_1);
    request.setPoint(point);
    request.setLastModified(new Date().getTime() - 10);
    request.setSector(Sector.SECTOR_DEFAULT);
    request.setAuthority(getDefaultExampleAuthority());
    request.setApplicationVersion("Application.This");
    request.setDatabaseVersion("Database.That");
  }

  public static AeriusResultPoint getExamplePointBinnenveld() { //point in Binnenveld with 2 habitat types.
    return new AeriusResultPoint(4286670, AeriusPointType.RECEPTOR, 167843, 447435);
  }

  public static AeriusResultPoint getExamplePointDuinen() {
    //point with 2 assessment areas (Noordhollands Duinreservaat (87) and Schoorlse Duinen(86)) with at least a habitat type each.
    return new AeriusResultPoint(6388705, AeriusPointType.RECEPTOR, 104481, 521277);
  }

  public static AeriusResultPoint getExamplePointDwingelderveld() {
    //point in Dwingelderveld with at least one species.
    return new AeriusResultPoint(6903101, AeriusPointType.RECEPTOR, 225832, 539330);
  }

  /**
   * @return
   */
  public SectorCategories getCategories() {
    return categories;
  }
}
