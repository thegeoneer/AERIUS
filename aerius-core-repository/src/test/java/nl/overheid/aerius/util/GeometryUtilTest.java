/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
*
*/
public class GeometryUtilTest {

  private static double EXAMPLE_POINT_X = 7.6;
  private static double EXAMPLE_POINT_Y = 9.1;

  @Test
  public void testMiddleOfGeometry() throws AeriusException {
    final Point pointMid = GeometryUtil.middleOfGeometry(getExamplePoint());
    assertEquals("Middlepoint of point X", 7.6, pointMid.getX(), 0.001);
    assertEquals("Middlepoint of point Y", 9.1, pointMid.getY(), 0.001);
    //for linestring, the point should be on the line.
    final Point lineStringMid = GeometryUtil.middleOfGeometry(new WKTGeometry("LINESTRING(100 0,100 25,175 25)", 100));
    assertEquals("Middlepoint of linestring X", 125, lineStringMid.getX(), 0.001);
    assertEquals("Middlepoint of linestring Y", 25, lineStringMid.getY(), 0.001);
    final Point polygonMid = GeometryUtil.middleOfGeometry(new WKTGeometry("POLYGON((1000 0,1100 100,1200 100,1100 0,1000 0))", 100 * 100));
    assertEquals("Middlepoint of polygon X", 1100, polygonMid.getX(), 0.001);
    assertEquals("Middlepoint of polygon Y", 50, polygonMid.getY(), 0.001);
  }

  @Test
  public void testMinDistance() throws AeriusException {
    final List<WKTGeometry> testCollection = new ArrayList<>();
    //a point at (0,0), a linestring from (0,100) to (0,200) and a polygon with sides of 100 at (0,300)
    testCollection.add(new WKTGeometry("POINT(0 0)", 1));
    testCollection.add(new WKTGeometry("LINESTRING(0 100,0 200)", 100));
    testCollection.add(new WKTGeometry("POLYGON((0 300,100 300, 100 400,0 400,0 300))", 100 * 100));
    assertEquals("Distance to point at (100,0)", 100, GeometryUtil.minDistance(new Point(100, 0), testCollection), 0.001);
    assertEquals("Distance to point at (100,100)", 100, GeometryUtil.minDistance(new Point(100, 100), testCollection), 0.001);
    assertEquals("Distance to point at (100,150)", 100, GeometryUtil.minDistance(new Point(100, 150), testCollection), 0.001);
    assertEquals("Distance to point at (100,300)", 0, GeometryUtil.minDistance(new Point(100, 300), testCollection), 0.001);
    assertEquals("Distance to point at (50,350)", 0, GeometryUtil.minDistance(new Point(50, 350), testCollection), 0.001);
    assertEquals("Distance to point at (100,275)", 25, GeometryUtil.minDistance(new Point(100, 275), testCollection), 0.001);
    assertEquals("Distance to point at (50,275)", 25, GeometryUtil.minDistance(new Point(100, 275), testCollection), 0.001);
    assertEquals("Distance to point at (-50,350)", 50, GeometryUtil.minDistance(new Point(-50, 300), testCollection), 0.001);
    assertEquals("Distance to point at (-3,296)", 5, GeometryUtil.minDistance(new Point(-3, 296), testCollection), 0.001);
    assertEquals("Distance to point with empty collection", 0, GeometryUtil.minDistance(new Point(100, 0), new ArrayList<WKTGeometry>()), 0.001);
  }

  @Test
  public void testValidWKT() {
    assertTrue("Valid WKT", GeometryUtil.validWKT(getExamplePoint().getWKT()));
    assertFalse("Invalid WKT string", GeometryUtil.validWKT(getExamplePoint().getWKT().substring(1)));
    assertFalse("Invalid WKT null", GeometryUtil.validWKT(null));
    assertFalse("Invalid WKT empty string", GeometryUtil.validWKT(""));
    assertFalse("Invalid WKT only space", GeometryUtil.validWKT(" "));
  }

  @Test
  public void testGetPoint() throws AeriusException {
    final Point point = GeometryUtil.getPoint(getExamplePoint().getWKT());
    assertNotNull("Point returned", point);
    assertEquals("X-coord", EXAMPLE_POINT_X, point.getX(), 0.0001);
    assertEquals("Y-coord", EXAMPLE_POINT_Y, point.getY(), 0.0001);
    try {
      GeometryUtil.getPoint("POINT(7.7 9.1");
      fail("Incorrect WKT should throw an exception");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception", Reason.GEOMETRY_INVALID, e.getReason());
    }
    try {
      GeometryUtil.getPoint(getExampleLineString().getWKT());
      fail("Linestring WKT should throw an exception");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception", Reason.GEOMETRY_INVALID, e.getReason());
    }
  }

  @Test
  public void testHasIntersections() throws AeriusException {
    final int side = 4000;
    //create normal linestring with points (0,0), (0,4000), (4000,4000), (0,4000)
    final String normalLineStringWKT = "LineString(0 0," + side + " 0," + side + " " + side + ",0 " + side + ")";
    assertFalse("Normal linestring geometry shouldn't count as intersected", GeometryUtil.hasIntersections(normalLineStringWKT));

    //create normal polygon with points (0,0), (0,4000), (4000,4000), (0,4000), (0,0)
    final String normalPolygonWKT = "POLYGON((0 0," + side + " 0," + side + " " + side + ",0 " + side + ",0 0))";
    assertFalse("Normal polygon geometry shouldn't count as intersected", GeometryUtil.hasIntersections(normalPolygonWKT));

    //create Bow-Tie linestring with points (0,0), (4000,4000), (4000,0), (0,4000)
    final String bowtieLineStringWKT = "LINESTRING(0 0," + side + " " + side + "," + side + " 0,0 " + side + ")";
    assertFalse("Bowtie linestring geometry should not count as intersected", GeometryUtil.hasIntersections(bowtieLineStringWKT));

    //create Bow-Tie polygon with points (0,0), (4000,4000), (4000,0), (0,4000), (0,0)
    final String bowtiePolygonWKT = "POLYGON((0 0," + side + " " + side + "," + side + " 0,0 " + side + ",0 0))";
    assertTrue("Bowtie polygon geometry should count as intersected", GeometryUtil.hasIntersections(bowtiePolygonWKT));

    try {
      final String invalidWKT = "POLYGON((0 0," + side + " " + side + "," + side + " 0,0 " + side + ",0 0)";
      GeometryUtil.hasIntersections(invalidWKT);
      fail("Incorrect WKT should throw an exception");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception", Reason.GEOMETRY_INVALID, e.getReason());
    }
  }

  @Test
  public void testDetermineLength() {
    final WKTGeometry lineString = getExampleLineString();
    final double determinedLength = GeometryUtil.determineLength(lineString.getWKT());
    assertEquals("Length of a line", lineString.getMeasure(), determinedLength, 0.500);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testDetermineLengthNullArgument() {
    GeometryUtil.determineLength(null);
  }

  @Test
  public void testDetermineArea() {
    final WKTGeometry polygon = getExampleWKTPolygon();
    final double determinedArea = GeometryUtil.determineArea(polygon.getWKT());
    assertEquals("Area of a surface", polygon.getMeasure(), determinedArea, 0.001);
  }

  @Test
  public void testLastPointFromWKT() throws AeriusException {
    Point point = GeometryUtil.lastPointFromWKT("LINESTRING(1 2,3 4,2 2)");
    assertEquals("X coordinate", 2, point.getX(), 0.001);
    assertEquals("Y coordinate", 2, point.getY(), 0.001);
    point = GeometryUtil.lastPointFromWKT("POINT(3 4)");
    assertEquals("X coordinate", 3, point.getX(), 0.001);
    assertEquals("Y coordinate", 4, point.getY(), 0.001);
    point = GeometryUtil.lastPointFromWKT("POLYGON((8 2,7 9,6 5,8 2))");
    assertEquals("X coordinate", 8, point.getX(), 0.001);
    assertEquals("Y coordinate", 2, point.getY(), 0.001);
  }

  @Test(expected = AeriusException.class)
  public void testLastPointFromWKTIncorrectWKT() throws AeriusException {
    GeometryUtil.lastPointFromWKT("LINESTRNG(1 2,4 5)");
  }

  @Test
  public void testGetClosestPoint() {
    final Point toPoint = new Point(10, 10);
    final ArrayList<Point> points = new ArrayList<>();
    points.add(new Point(4, 4));
    points.add(new Point(8, 100));
    final Point correctPoint = new Point(9, 11);
    points.add(correctPoint);
    points.add(new Point(16, 10));
    points.add(new Point(1, 0));
    final Point closestPoint = GeometryUtil.getClosestPoint(toPoint, points);
    assertNotNull("returned point", closestPoint);
    assertEquals("closest point found", correctPoint, closestPoint);
  }

  @Test
  public void testConvertToPoints() throws AeriusException {
    //create linestring with (0,0), (0,4000), (0,8000), (0,12000), (0,16000)
    final int numberOfCoordinates = 5;
    final StringBuilder lineString = new StringBuilder();
    lineString.append("LINESTRING(0 0");
    for (int i = 0; i < numberOfCoordinates; i++) {
      lineString.append(",0 ");
      lineString.append(4000 * i);
    }
    lineString.append(')');
    final WKTGeometry geometry = new WKTGeometry(lineString.toString());
    final double length = GeometryUtil.determineLength(geometry.getWKT());
    assertEquals("Length", (numberOfCoordinates - 1) * 4000, length, 1E-5);
    final Double maxSegementSize = 76.4;
    final List<Point> convertedPoints = GeometryUtil.convertToPoints(geometry, maxSegementSize);
    assertEquals("Number of segments",
        new BigDecimal(length).divide(new BigDecimal(maxSegementSize),
            RoundingMode.UP).intValue(),
        convertedPoints.size());
    final double segmentSize = new BigDecimal(length).divide(new BigDecimal(convertedPoints.size()), 4,
        RoundingMode.HALF_UP).doubleValue();
    for (int i = 0; i < convertedPoints.size(); i++) {
      final Point convertedPoint = convertedPoints.get(i);
      assertEquals("X-coord of point " + i, 0, convertedPoint.getX(), 1E-2);
      assertEquals("Y-coord of point " + i, i * segmentSize + segmentSize / 2.0, convertedPoint.getY(), 1E-2);
    }
  }

  @Test
  public void testConvertToPointsZigZag() throws AeriusException {
    //create linestring that zigs n zags
    final WKTGeometry geometry = new WKTGeometry("LINESTRING(0 0,0 4000,1000 4000,1000 12000,3000 12000)");
    final double length = GeometryUtil.determineLength(geometry.getWKT());
    final Double maxSegementSize = 76.4;
    final List<Point> convertedPoints = GeometryUtil.convertToPoints(geometry, maxSegementSize);
    assertEquals("Number of segments",
        new BigDecimal(length).divide(new BigDecimal(maxSegementSize), 0,
            RoundingMode.UP).intValue(),
        convertedPoints.size());
    for (int i = 0; i < convertedPoints.size(); i++) {
      final Point convertedPoint = convertedPoints.get(i);
      if (convertedPoint.getX() == 0) {
        assertTrue("Point wasn't on the line " + i + ", point: " + convertedPoint, convertedPoint.getY() >= 0);
        assertTrue("Point wasn't on the line " + i + ", point: " + convertedPoint, convertedPoint.getY() <= 4000);
      } else if (convertedPoint.getY() == 4000) {
        assertTrue("Point wasn't on the line " + i + ", point: " + convertedPoint, convertedPoint.getX() >= 0);
        assertTrue("Point wasn't on the line " + i + ", point: " + convertedPoint, convertedPoint.getX() <= 1000);
      } else if (convertedPoint.getX() == 1000) {
        assertTrue("Point wasn't on the line " + i + ", point: " + convertedPoint, convertedPoint.getY() >= 4000);
        assertTrue("Point wasn't on the line " + i + ", point: " + convertedPoint, convertedPoint.getY() <= 12000);
      } else if (convertedPoint.getY() == 12000) {
        assertTrue("Point wasn't on the line " + i + ", point: " + convertedPoint, convertedPoint.getX() >= 1000);
        assertTrue("Point wasn't on the line " + i + ", point: " + convertedPoint, convertedPoint.getX() <= 3000);
      } else {
        fail("Point wasn't on the line " + i + ", point: " + convertedPoint);
      }
    }
  }

  @Test
  public void testDetermineBoundingBox() {
    final List<Point> points = new ArrayList<Point>();
    //no points
    BBox boundingBox = GeometryUtil.determineBBox(points);
    assertNotNull("empty list shouldn't return null", boundingBox);
    assertEquals("No point maxX", 0.0, boundingBox.getMaxX(), 1E-3);
    assertEquals("No point maxY", 0.0, boundingBox.getMaxY(), 1E-3);
    assertEquals("No point minX", 0.0, boundingBox.getMinX(), 1E-3);
    assertEquals("No point minY", 0.0, boundingBox.getMinY(), 1E-3);
    //one point
    points.add(new Point(1053.4, 498.1));
    boundingBox = GeometryUtil.determineBBox(points);
    assertEquals("One point maxX", 1053.4, boundingBox.getMaxX(), 1E-3);
    assertEquals("One point maxY", 498.1, boundingBox.getMaxY(), 1E-3);
    assertEquals("One point minX", 1053.4, boundingBox.getMinX(), 1E-3);
    assertEquals("One point minY", 498.1, boundingBox.getMinY(), 1E-3);
    //multiple points
    points.add(new Point(345.5, 98707.1));
    points.add(new Point(773.40, 8.1));
    points.add(new Point(984.4, -498.1));
    points.add(new Point(1234.5, 870.5));
    points.add(new Point(1053.4, 498.1));
    points.add(new Point(955, 38.1));
    boundingBox = GeometryUtil.determineBBox(points);
    assertEquals("Multiple point maxX", 1234.5, boundingBox.getMaxX(), 1E-3);
    assertEquals("Multiple point maxY", 98707.1, boundingBox.getMaxY(), 1E-3);
    assertEquals("Multiple point minX", 345.5, boundingBox.getMinX(), 1E-3);
    assertEquals("Multiple point minY", -498.1, boundingBox.getMinY(), 1E-3);
  }

  @Test
  public void testDetermineCentroid() throws AeriusException {
    final List<WKTGeometry> testCollection = new ArrayList<>();
    final int srid = RDNew.SRID;
    Point centroid = GeometryUtil.determineCentroid(srid, testCollection);
    assertEquals("no geometries maxX", 0.0, centroid.getX(), 1E-3);
    assertEquals("no geometries maxY", 0.0, centroid.getY(), 1E-3);

    testCollection.add(new WKTGeometry("POINT(50 50)", 1));
    centroid = GeometryUtil.determineCentroid(srid, testCollection);
    assertEquals("1 point maxX", 50.0, centroid.getX(), 1E-3);
    assertEquals("1 point maxY", 50.0, centroid.getY(), 1E-3);

    testCollection.add(new WKTGeometry("POINT(100 50)", 1));
    centroid = GeometryUtil.determineCentroid(srid, testCollection);
    assertEquals("2 point maxX", 75.0, centroid.getX(), 1E-3);
    assertEquals("2 point maxY", 50.0, centroid.getY(), 1E-3);

    testCollection.add(new WKTGeometry("LINESTRING(0 0,0 100)", 100));
    centroid = GeometryUtil.determineCentroid(srid, testCollection);
    assertEquals("2 points and line maxX", 33.3333, centroid.getX(), 1E-3);
    assertEquals("2 points and line maxY", 50.0, centroid.getY(), 1E-3);

    testCollection.add(new WKTGeometry("POLYGON((0 300,100 300, 100 400,0 400,0 300))", 100 * 100));
    centroid = GeometryUtil.determineCentroid(srid, testCollection);
    assertEquals("2 points, line and polygon maxX", 48.8888888, centroid.getX(), 1E-3);
    assertEquals("2 points, line and polygon maxY", 212.2222222, centroid.getY(), 1E-3);
  }

  @Test
  public void testGetFixedBBox() {
    final BBox bbox = new BBox(5.0, 5.0, 10);
    final BBox fixedBBox1 = GeometryUtil.getFixedBBox(bbox, 100, 200);
    assertEquals("Case 1: Width", 10, fixedBBox1.getWidth(), 0.001);
    assertEquals("Case 1: Height", 20, fixedBBox1.getHeight(), 0.001);
    assertEquals("Case 1: minX", 0, fixedBBox1.getMinX(), 0.001);
    assertEquals("Case 1: maxX", 10, fixedBBox1.getMaxX(), 0.001);
    assertEquals("Case 1: minY", -5, fixedBBox1.getMinY(), 0.001);
    assertEquals("Case 1: maxY", 15, fixedBBox1.getMaxY(), 0.001);

    final BBox fixedBBox2 = GeometryUtil.getFixedBBox(bbox, 200, 100);
    assertEquals("Case 2: Width", 20, fixedBBox2.getWidth(), 0.001);
    assertEquals("Case 2: Height", 10, fixedBBox2.getHeight(), 0.001);
    assertEquals("Case 2: minX", -5, fixedBBox2.getMinX(), 0.001);
    assertEquals("Case 2: maxX", 15, fixedBBox2.getMaxX(), 0.001);
    assertEquals("Case 2: minY", 0, fixedBBox2.getMinY(), 0.001);
    assertEquals("Case 2: maxY", 10, fixedBBox2.getMaxY(), 0.001);

    final BBox bboxComplex = new BBox(3.0, 8.0, 12.0, 19.0);
    final BBox fixedBBox3 = GeometryUtil.getFixedBBox(bboxComplex, 33, 100);
    assertEquals("Case 3: Width", 9, fixedBBox3.getWidth(), 0.001);
    assertEquals("Case 3: Height", 27.27272727, fixedBBox3.getHeight(), 0.001);
    assertEquals("Case 3: minX", 3, fixedBBox3.getMinX(), 0.001);
    assertEquals("Case 3: maxX", 12, fixedBBox3.getMaxX(), 0.001);
    assertEquals("Case 3: minY", -0.136363636, fixedBBox3.getMinY(), 0.001);
    assertEquals("Case 3: maxY", 27.13636364, fixedBBox3.getMaxY(), 0.001);

    final BBox fixedBBox4 = GeometryUtil.getFixedBBox(bboxComplex, 100, 33);
    assertEquals("Case 4: Width", 33.33333333, fixedBBox4.getWidth(), 0.001);
    assertEquals("Case 4: Height", 11, fixedBBox4.getHeight(), 0.001);
    assertEquals("Case 4: minX", -9.166666667, fixedBBox4.getMinX(), 0.001);
    assertEquals("Case 4: maxX", 24.16666667, fixedBBox4.getMaxX(), 0.001);
    assertEquals("Case 4: minY", 8, fixedBBox4.getMinY(), 0.001);
    assertEquals("Case 4: maxY", 19, fixedBBox4.getMaxY(), 0.001);

    assertEquals("original: minX", 0, bbox.getMinX(), 0.001);
    assertEquals("original: minY", 0, bbox.getMinY(), 0.001);
    assertEquals("original: maxX", 10, bbox.getMaxX(), 0.001);
    assertEquals("original: maxY", 10, bbox.getMaxY(), 0.001);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetFixedBBoxNullArgument() {
    GeometryUtil.getFixedBBox(null, 100, 200);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetFixedBBoxWidthZero() {
    final BBox bbox = new BBox(5.0, 5.0, 10);
    GeometryUtil.getFixedBBox(bbox, 0, 200);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetFixedBBoxHeightZero() {
    final BBox bbox = new BBox(5.0, 5.0, 10);
    GeometryUtil.getFixedBBox(bbox, 100, 0);
  }

  @Test
  public void testAddPadding() {
    //box with 10 width, 20 height at 5,5.
    final BBox bbox1 = new BBox(0.0, -5.0, 10.0, 15.0);
    final BBox bboxPadded1 = GeometryUtil.addPadding(bbox1, 0.5);
    assertEquals("case 1: minX", -5, bboxPadded1.getMinX(), 0.001);
    assertEquals("case 1: minY", -15, bboxPadded1.getMinY(), 0.001);
    assertEquals("case 1: maxX", 15, bboxPadded1.getMaxX(), 0.001);
    assertEquals("case 1: maxY", 25, bboxPadded1.getMaxY(), 0.001);

    final BBox bbox2 = getExampleBBox1();
    final BBox bboxPadded2 = GeometryUtil.addPadding(bbox2, 0.25);
    assertEquals("case 2: minX", 85, bboxPadded2.getMinX(), 0.001);
    assertEquals("case 2: minY", -57.5, bboxPadded2.getMinY(), 0.001);
    assertEquals("case 2: maxX", 115, bboxPadded2.getMaxX(), 0.001);
    assertEquals("case 2: maxY", -42.5, bboxPadded2.getMaxY(), 0.001);

    //negative padding. Should this be an exception or should it be allowed?
    final BBox bboxPadded3 = GeometryUtil.addPadding(bbox2, -1);
    assertEquals("case 3: minX", 110, bboxPadded3.getMinX(), 0.001);
    assertEquals("case 3: minY", -45, bboxPadded3.getMinY(), 0.001);
    assertEquals("case 3: maxX", 90, bboxPadded3.getMaxX(), 0.001);
    assertEquals("case 3: maxY", -55, bboxPadded3.getMaxY(), 0.001);

    assertEquals("original: minX", 90, bbox2.getMinX(), 0.001);
    assertEquals("original: minY", -55, bbox2.getMinY(), 0.001);
    assertEquals("original: maxX", 110, bbox2.getMaxX(), 0.001);
    assertEquals("original: maxY", -45, bbox2.getMaxY(), 0.001);
  }

  @Test
  public void testEnsureMinimumWidthHeight() {
    final BBox bbox = getExampleBBox1();
    final BBox bboxMinWidth1 = GeometryUtil.ensureMinimumWidthHeight(bbox, 0, 20);
    assertEquals("case 1: minX", 90, bboxMinWidth1.getMinX(), 0.001);
    assertEquals("case 1: minY", -60, bboxMinWidth1.getMinY(), 0.001);
    assertEquals("case 1: maxX", 110, bboxMinWidth1.getMaxX(), 0.001);
    assertEquals("case 1: maxY", -40, bboxMinWidth1.getMaxY(), 0.001);

    final BBox bboxMinWidth2 = GeometryUtil.ensureMinimumWidthHeight(bbox, 100, 0);
    assertEquals("case 2: minX", 50, bboxMinWidth2.getMinX(), 0.001);
    assertEquals("case 2: minY", -55, bboxMinWidth2.getMinY(), 0.001);
    assertEquals("case 2: maxX", 150, bboxMinWidth2.getMaxX(), 0.001);
    assertEquals("case 2: maxY", -45, bboxMinWidth2.getMaxY(), 0.001);

    final BBox bboxMinWidth3 = GeometryUtil.ensureMinimumWidthHeight(bbox, 100, 50);
    assertEquals("case 3: minX", 50, bboxMinWidth3.getMinX(), 0.001);
    assertEquals("case 3: minY", -75, bboxMinWidth3.getMinY(), 0.001);
    assertEquals("case 3: maxX", 150, bboxMinWidth3.getMaxX(), 0.001);
    assertEquals("case 3: maxY", -25, bboxMinWidth3.getMaxY(), 0.001);

    assertEquals("original: minX", 90, bbox.getMinX(), 0.001);
    assertEquals("original: minY", -55, bbox.getMinY(), 0.001);
    assertEquals("original: maxX", 110, bbox.getMaxX(), 0.001);
    assertEquals("original: maxY", -45, bbox.getMaxY(), 0.001);
  }

  @Test
  public void testMerge() {
    final BBox bbox1 = getExampleBBox1();
    final BBox bbox2 = getExampleBBox2();
    final BBox merged = GeometryUtil.merge(bbox1, bbox2);
    assertEquals("case 1: minX", 90, merged.getMinX(), 0.001);
    assertEquals("case 1: minY", -85, merged.getMinY(), 0.001);
    assertEquals("case 1: maxX", 115, merged.getMaxX(), 0.001);
    assertEquals("case 1: maxY", -45, merged.getMaxY(), 0.001);
  }

  @Test
  public void testMergeCollection() {
    final BBox bbox1 = getExampleBBox1();
    final BBox bbox2 = getExampleBBox2();
    final List<BBox> collection = new ArrayList<>();
    collection.add(bbox1);
    collection.add(bbox2);
    BBox merged = GeometryUtil.merge(collection);
    assertEquals("case 1: minX", 90, merged.getMinX(), 0.001);
    assertEquals("case 1: minY", -85, merged.getMinY(), 0.001);
    assertEquals("case 1: maxX", 115, merged.getMaxX(), 0.001);
    assertEquals("case 1: maxY", -45, merged.getMaxY(), 0.001);

    final BBox bbox3 = new BBox(0.0, 0.0, 1000.0, 1000.0);
    collection.add(bbox3);
    merged = GeometryUtil.merge(collection);
    assertEquals("case 2: minX", 0, merged.getMinX(), 0.001);
    assertEquals("case 2: minY", -85, merged.getMinY(), 0.001);
    assertEquals("case 2: maxX", 1000, merged.getMaxX(), 0.001);
    assertEquals("case 2: maxY", 1000, merged.getMaxY(), 0.001);
  }

  private BBox getExampleBBox1() {
    //box with 20 width, 10 height at 100,-50.
    return new BBox(90.0, -55.0, 110.0, -45.0);
  }

  private BBox getExampleBBox2() {
    //box with 15 width, 10 height at 107.5,-80.
    return new BBox(100.0, -85.0, 115.0, -75.0);
  }

  @Test
  public void testDetermineBoundingBoxForGeometry() {
    final WKTGeometry point = new WKTGeometry("POINT(40 60)", 1);
    final BBox outerBounds1 = GeometryUtil.determineBBoxForGeometry(point);
    assertEquals("case 1: minX", 40, outerBounds1.getMinX(), 0.001);
    assertEquals("case 1: minY", 60, outerBounds1.getMinY(), 0.001);
    assertEquals("case 1: maxX", 40, outerBounds1.getMaxX(), 0.001);
    assertEquals("case 1: maxY", 60, outerBounds1.getMaxY(), 0.001);

    final WKTGeometry lineString = new WKTGeometry("LINESTRING(40 60,55 45)", 100);
    final BBox outerBounds2 = GeometryUtil.determineBBoxForGeometry(lineString);
    assertEquals("case 2: minX", 40, outerBounds2.getMinX(), 0.001);
    assertEquals("case 2: minY", 45, outerBounds2.getMinY(), 0.001);
    assertEquals("case 2: maxX", 55, outerBounds2.getMaxX(), 0.001);
    assertEquals("case 2: maxY", 60, outerBounds2.getMaxY(), 0.001);

    final WKTGeometry polygon = new WKTGeometry("POLYGON((0 300,100 300, 100 400,0 400,0 300))", 100 * 100);
    final BBox outerBounds3 = GeometryUtil.determineBBoxForGeometry(polygon);
    assertEquals("case 3: minX", 0, outerBounds3.getMinX(), 0.001);
    assertEquals("case 3: minY", 300, outerBounds3.getMinY(), 0.001);
    assertEquals("case 3: maxX", 100, outerBounds3.getMaxX(), 0.001);
    assertEquals("case 3: maxY", 400, outerBounds3.getMaxY(), 0.001);

    //can handle multipoints, though we don't normally support those
    final WKTGeometry multiPoint = new WKTGeometry("MULTIPOINT(10 40, 40 30, 20 20, 30 10)", 10);
    final BBox outerBounds4 = GeometryUtil.determineBBoxForGeometry(multiPoint);
    assertEquals("case 4: minX", 10, outerBounds4.getMinX(), 0.001);
    assertEquals("case 4: minY", 10, outerBounds4.getMinY(), 0.001);
    assertEquals("case 4: maxX", 40, outerBounds4.getMaxX(), 0.001);
    assertEquals("case 4: maxY", 40, outerBounds4.getMaxY(), 0.001);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testDetermineBoundingBoxForGeometryException() throws AeriusException {
    final WKTGeometry point = new WKTGeometry("PONT(40 60)", 1);
    GeometryUtil.determineBBoxForGeometry(point);
  }

  private WKTGeometry getExamplePoint() {
    return new WKTGeometry(
        "POINT(" + EXAMPLE_POINT_X + " " + EXAMPLE_POINT_Y + ")", 1);
  }

  private WKTGeometry getExampleLineString() {
    final double xCoord0 = 7.80;
    final double yCoord0 = 4.55;
    final double xCoord1 = 44.77;
    final double yCoord1 = 2.4;
    final String wktLineString = "LINESTRING ("
        + xCoord0 + " " + yCoord0 + ","
        + xCoord1 + " " + yCoord1 + ")";
    final double length = Math.sqrt(
        Math.pow(xCoord0 - xCoord1, 2)
            + Math.pow(yCoord0 - yCoord1, 2)
        );
    return new WKTGeometry(wktLineString, (int) Math.round(length));
  }

  private WKTGeometry getExampleWKTPolygon() {
    return new WKTGeometry("POLYGON ((0 0,20 0,20 100,0 100, 0 0))", 20 * 100);
  }

}
