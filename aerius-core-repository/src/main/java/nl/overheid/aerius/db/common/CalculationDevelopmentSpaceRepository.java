/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResultList;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.request.PotentialRequestDetails;
import nl.overheid.aerius.shared.domain.request.PotentialRequestDetails.AssessmentAreaPotentialRequestDetails;
import nl.overheid.aerius.shared.domain.request.PotentialRequestInfo;

/**
 * TODO document this class and the enum RepositoryAttribute.
 */
public final class CalculationDevelopmentSpaceRepository {

  private enum RepositoryAttribute implements Attribute {

    RULE,
    PASSED,

    /**
     * The max development space demand
     */
    MAX_DEVELOPMENT_SPACE_DEMAND,

    /**
     * Permit threshold value.
     */
    PERMIT_THRESHOLD_VALUE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final Query DETERMINE_RESULTS = DevelopmentRuleResultsHandler
      .getQueryBuilder("ae_development_rule_checks(?, ?, ?::segment_type)",
          QueryAttribute.PROPOSED_CALCULATION_ID, QueryAttribute.CURRENT_CALCULATION_ID, QueryAttribute.SEGMENT)
      .getQuery();

  private static final Query CALCULATION_DEMANDS = QueryBuilder
      .from("ae_assessment_area_calculation_demands(?,?)", QueryAttribute.PROPOSED_CALCULATION_ID, QueryAttribute.CURRENT_CALCULATION_ID)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.MAX_DEVELOPMENT_SPACE_DEMAND, RepositoryAttribute.PERMIT_THRESHOLD_VALUE)
      .getQuery();

  private static final String INSERT_CALCULATION_DEMANDS = "SELECT ae_insert_calculation_demands(?, ?, ?)";

  private CalculationDevelopmentSpaceRepository() {
    // don't instantiate.
  }

  public static HashSet<DevelopmentRuleResultList> determineResultsByArea(final Connection connection,
      final int calculationId) throws SQLException {
    return determineResultsByArea(connection, calculationId, 0);
  }

  public static HashSet<DevelopmentRuleResultList> determineResultsByArea(final Connection connection,
      final int proposedCalculationId, final int currentCalculationId) throws SQLException {
    final DevelopmentRuleResultsHandler handler = new DevelopmentRuleResultsHandler(connection) {

      @Override
      protected void setParameters(final PreparedStatement selectPS, final Query query) throws SQLException {
        query.setParameter(selectPS, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
        query.setParameter(selectPS, QueryAttribute.CURRENT_CALCULATION_ID, currentCalculationId == 0 ? null : currentCalculationId);
        query.setParameter(selectPS, QueryAttribute.SEGMENT, SegmentType.PROJECTS.getDbValue());
      }
    };
    handler.execute(connection, DETERMINE_RESULTS);
    return handler.getResults();
  }

  public static PotentialRequestInfo determinePotentialRequestInfo(final Connection connection, final int calculationId)
      throws SQLException {
    return determinePotentialRequestInfo(connection, calculationId, 0);
  }

  public static PotentialRequestInfo determinePotentialRequestInfo(final Connection connection, final int proposedCalculationId,
      final int currentCalculationId)
      throws SQLException {
    final double meldingThresholdValue = ConstantRepository.getDouble(connection, ConstantsEnum.PRONOUNCEMENT_THRESHOLD_VALUE);
    final double defaultPermitThresholdValue = ConstantRepository.getDouble(connection, ConstantsEnum.DEFAULT_PERMIT_THRESHOLD_VALUE);
    final PotentialRequestDetails potentialRequestDetails = new PotentialRequestDetails(meldingThresholdValue, defaultPermitThresholdValue);

    final AssessmentAreaPotentialRequestCollector collector = new AssessmentAreaPotentialRequestCollector(potentialRequestDetails);
    try (final PreparedStatement selectPS = connection.prepareStatement(CALCULATION_DEMANDS.get())) {
      CALCULATION_DEMANDS.setParameter(selectPS, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      CALCULATION_DEMANDS.setParameter(selectPS, QueryAttribute.CURRENT_CALCULATION_ID, currentCalculationId == 0 ? null : currentCalculationId);

      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          collector.collectEntity(rs);
        }
      }
    }

    return potentialRequestDetails.createPotentialRequestInfo();
  }

  /**
   * Insert calculation demands
   * @param connection database connection
   * @param proposedCalculationId id one proposed situation
   * @param currentCalculationId id two for current situation
   * @param zeroingOfDemandsUnderThreshold boolean determines if we change development_space_demand to 0 below threshold
   * @throws SQLException database exception
   */
  public static void insertCalculationDemands(final Connection connection, final int proposedCalculationId,
      final int currentCalculationId, final boolean zeroingOfDemandsUnderThreshold) throws SQLException {
    try (final PreparedStatement ps = connection.prepareStatement(INSERT_CALCULATION_DEMANDS)) {
      QueryUtil.setValues(ps, proposedCalculationId, currentCalculationId, zeroingOfDemandsUnderThreshold);
      ps.execute();
    }
  }

  private static class AssessmentAreaPotentialRequestCollector extends EntityCollector<AssessmentAreaPotentialRequestDetails> {

    private final PotentialRequestDetails potentialRequestDetails;

    public AssessmentAreaPotentialRequestCollector(final PotentialRequestDetails potentialRequestDetails) {
      this.potentialRequestDetails = potentialRequestDetails;
    }

    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
    }

    @Override
    public AssessmentAreaPotentialRequestDetails fillEntity(final ResultSet rs) throws SQLException {
      final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
      final double maxDemand = rs.getDouble(RepositoryAttribute.MAX_DEVELOPMENT_SPACE_DEMAND.attribute());
      final double permitThresholdValue = rs.getDouble(RepositoryAttribute.PERMIT_THRESHOLD_VALUE.attribute());
      return potentialRequestDetails.addAssessmentAreaDetails(assessmentAreaId, maxDemand, permitThresholdValue);
    }
  }

}
