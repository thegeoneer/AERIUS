/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.postgis.LineString;
import org.postgis.PGgeometry;

import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.shared.domain.sector.ShippingNode;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.EnumUtil;
import nl.overheid.aerius.util.NumberUtil;

/**
 * Database method related Maritime and Inland shipping.
 */
public final class ShippingRepository {

  private enum RepositoryAttribute implements Attribute {
    DIRECTION_TYPE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }
  private static final String GET_MARITIME_SHIPPING_SNAPPABLE_NODES =
      "SELECT shipping_node_id as id, ST_X(geometry) as x, St_Y(geometry) as y "
          + " FROM shipping_maritime_nodes WHERE snappable = true ";

  /**
   * Points for maritime ships on inland waters (used for route from dock to port).
   */
  private static final String GET_MARITIME_MOORING_INLAND_SHIPPING_ROUTE =
      "SELECT ST_X(point_geometry) as x, ST_Y(point_geometry) as y, length, maneuver_factor "
          + " FROM ae_maritime_mooring_inland_shipping_route(?,?,?) ";

  /**
   * Points for maritime ships on routes (used for maritime part for mooring situation or standalone routes).
   */
  private static final String GET_STANDALONE_MARITIME_SHIPPING_ROUTE =
      "SELECT ST_X(point_geometry) as x, ST_Y(point_geometry) as y, length, maneuver_factor "
          + " FROM ae_standalone_maritime_shipping_route(?,?) ";

  /**
   * Points for inland ships on routes, including lock factors.
   */
  private static final String GET_INLAND_SHIPPING_ROUTE =
      "SELECT ST_X(point_geometry) as x, ST_Y(point_geometry) as y, segment_length, lock_factor "
          + " FROM ae_shipping_inland_calculate_points(?) ";

  /**
   * Points for inland ships on routes, with waterways determined by the database.
   */
  private static final String GET_INLAND_SHIPPING_ROUTE_WITH_WATERWAY =
      "SELECT ST_X(point_geometry) as x, ST_Y(point_geometry) as y, segment_length, lock_factor, "
      + " shipping_inland_waterway_category_id, code, direction_type "
          + " FROM ae_shipping_inland_calculate_points_with_waterway(?) "
          + " INNER JOIN shipping_inland_waterway_categories USING (shipping_inland_waterway_category_id)";

  /**
   * Suggest a waterway for a geometry.
   */
  private static final String SUGGEST_INLAND_SHIPPING_ROUTE_WATERWAY =
      "SELECT shipping_inland_waterway_category_id, code, direction_type "
          + " FROM ae_shipping_inland_suggest_waterway(?)";

  private ShippingRepository() {
  }

  /**
   * Returns a list of all snappable nodes for shipping from the database.
   *
   * @param con Database connection
   * @return List with all snappable nodes for shipping.
   * @throws SQLException database related exception
   */
  public static ArrayList<ShippingNode> getShippingSnappableNodes(final Connection con) throws SQLException {
    final ArrayList<ShippingNode> snappableNodes = new ArrayList<>();
    try (final PreparedStatement stmt = con.prepareStatement(GET_MARITIME_SHIPPING_SNAPPABLE_NODES)) {
      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        final ShippingNode snappablePoint = new ShippingNode(rs.getInt("id"), rs.getDouble("x"), rs.getDouble("y"));
        snappableNodes.add(snappablePoint);
      }
    }
    return snappableNodes;
  }

  /**
   * @param con Database connection
   * @param vgev The vesselgroupemissionvalues to determine the route for (based on route and category).
   * @param maxSegmentSize The maximum segment size that each point will represent.
   * @return The pointsource emissions (without actual emissions) to use for the route, with the maneuver factor to use for the point.
   * @throws SQLException database related exception.
   * @throws AeriusException in case an invalid geometry was used. Only LineStrings are allowed.
   */
  public static List<MaritimeShippingRoutePoint> getMaritimeMooringShippingInlandRoute(final Connection con,
      final MooringMaritimeVesselGroup vgev, final double maxSegmentSize) throws SQLException, AeriusException {

    // ae_inland_shipping_route(category_id int, transfer_route geometry, shipping_node_start int, upstream boolean)
    final PGgeometry lineGeometry = toPostgis(con, vgev.getInlandRoute().getGeometry());
    return getShippingRoutePoints(con, GET_MARITIME_MOORING_INLAND_SHIPPING_ROUTE, vgev.getCategory().getId(), lineGeometry, maxSegmentSize);
  }

  /**
   * Get the points for a maritime-shipping route (either inland or on sea).
   * @param con Database connection
   * @param geometry The route the vessels are taking. Should be a linestring.
   * @param maxSegmentSize The maximum segment size that each point will represent.
   * @return The points to use for the emission, with the maneuver factor for that point.
   * @throws SQLException database related exception.
   * @throws AeriusException in case an invalid geometry was used. Only LineStrings are allowed.
   */
  public static List<MaritimeShippingRoutePoint> getMaritimeShippingRoutePoints(final Connection con, final WKTGeometry geometry,
      final double maxSegmentSize) throws SQLException, AeriusException {
    // ae_inland_shipping_route(transfer_route geometry, shipping_node_start int, upstream boolean)
    return getShippingRoutePoints(con, GET_STANDALONE_MARITIME_SHIPPING_ROUTE, toPostgis(con, geometry), maxSegmentSize);
  }

  /**
   * Generic method for getMaritime(Mooring)ShippingRoutePoints methods
   * @param con Database connection
   * @param query the actual query
   * @param values the values to be put on the query, order should be handled by caller.
   * @return The points to use for the emission, with the maneuver factor for that point.
   * @throws SQLException database related exception.
   * @throws AeriusException in case an invalid geometry was used. Only LineStrings are allowed.
   */
  private static List<MaritimeShippingRoutePoint> getShippingRoutePoints(final Connection con, final String query, final Object... values)
      throws SQLException, AeriusException {
    final List<MaritimeShippingRoutePoint> routePoints = new ArrayList<>();
    try (final PreparedStatement stmt = con.prepareStatement(query)) {
      QueryUtil.setValues(stmt, values);
      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        final int xCoord = Math.round(rs.getFloat("x"));
        final int yCoord = Math.round(rs.getFloat("y"));
        final double measure = NumberUtil.safeFloat2Double(rs.getFloat("length"));
        final double maneuverFactor = NumberUtil.safeFloat2Double(rs.getFloat("maneuver_factor"));
        routePoints.add(new MaritimeShippingRoutePoint(xCoord, yCoord, measure, maneuverFactor));
      }
    }
    return routePoints;
  }

  /**
   * Converts a inland shipping route to individual points.
   * @param con Database connection
   * @param geometry line geometry of the route
   * @return List of {@link InlandShippingRoutePoint} of the line in points. Waterway type and direction are left null.
   * @throws SQLException database related exception
   * @throws AeriusException in case an invalid geometry was used. Only LineStrings are allowed.
   */
  public static List<InlandShippingRoutePoint> getInlandShippingRoutePoints(final Connection con, final WKTGeometry geometry)
      throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints = new ArrayList<>();
    final PGgeometry lineGeometry = toPostgis(con, geometry);
    try (final PreparedStatement stmt = con.prepareStatement(GET_INLAND_SHIPPING_ROUTE)) {
      // ae_shipping_inland_calculate_points(transfer_route geometry)
      stmt.setObject(1, lineGeometry);
      final ResultSet rs = stmt.executeQuery();
      // x, y, segment_length, lock_factor
      while (rs.next()) {
        final int xCoord = Math.round(rs.getFloat("x"));
        final int yCoord = Math.round(rs.getFloat("y"));
        final double measure = NumberUtil.safeFloat2Double(rs.getFloat("segment_length"));
        final double lockFactor = NumberUtil.safeFloat2Double(rs.getFloat("lock_factor"));
        routePoints.add(new InlandShippingRoutePoint(xCoord, yCoord, measure, lockFactor));
      }
    }
    return routePoints;
  }

  /**
   * Converts a inland shipping route to individual points.
   * @param con Database connection
   * @param geometry line geometry of the route
   * @return List of {@link InlandShippingRoutePoint} of the line in points
   * @throws SQLException database related exception
   * @throws AeriusException in case an invalid geometry was used. Only LineStrings are allowed.
   */
  public static List<InlandShippingRoutePoint> getInlandShippingRoutePointsWithWaterways(final Connection con, final WKTGeometry geometry)
      throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints = new ArrayList<>();
    final PGgeometry lineGeometry = toPostgis(con, geometry);
    try (final PreparedStatement stmt = con.prepareStatement(GET_INLAND_SHIPPING_ROUTE_WITH_WATERWAY)) {
      // ae_shipping_inland_calculate_points_with_waterway(transfer_route geometry)
      stmt.setObject(1, lineGeometry);
      final ResultSet rs = stmt.executeQuery();
      // x, y, segment_length, lock_factor, shipping_inland_waterway_category_id, code, direction_type
      while (rs.next()) {
        final int xCoord = Math.round(rs.getFloat("x"));
        final int yCoord = Math.round(rs.getFloat("y"));
        final double measure = NumberUtil.safeFloat2Double(rs.getFloat("segment_length"));
        final double lockFactor = NumberUtil.safeFloat2Double(rs.getFloat("lock_factor"));
        final int waterwayCategoryId = rs.getInt("shipping_inland_waterway_category_id");
        final String waterwayCategoryCode = rs.getString("code");
        final WaterwayDirection direction = EnumUtil.get(WaterwayDirection.class, rs.getString("direction_type"));
        routePoints.add(new InlandShippingRoutePoint(xCoord, yCoord, measure, lockFactor, waterwayCategoryId, waterwayCategoryCode, direction));
      }
    }
    return routePoints;
  }

  /**
   * Suggest a waterway type based on input geometry.
   * @param con Database connection
   * @param geometry line geometry of the route
   * @return List of shipping inland waterway types.
   * @throws SQLException database related exception
   * @throws AeriusException in case an invalid geometry was used. Only LineStrings are allowed.
   */
  public static ArrayList<InlandWaterwayType> suggestInlandShippingWaterway(final Connection con, final WKTGeometry geometry)
      throws SQLException, AeriusException {
    final ArrayList<InlandWaterwayType> waterWays = new ArrayList<>();
    final PGgeometry lineGeometry = toPostgis(con, geometry);
    try (final PreparedStatement stmt = con.prepareStatement(SUGGEST_INLAND_SHIPPING_ROUTE_WATERWAY)) {
      stmt.setObject(1, lineGeometry);
      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        final InlandWaterwayCategory inlandWaterwayCategory = new InlandWaterwayCategory();
        inlandWaterwayCategory.setId(rs.getInt("shipping_inland_waterway_category_id"));
        inlandWaterwayCategory.setCode(rs.getString("code"));
        final InlandWaterwayType inlandWaterwayType = new InlandWaterwayType();
        inlandWaterwayType.setWaterwayCategory(inlandWaterwayCategory);
        inlandWaterwayType.setWaterwayDirection(QueryUtil.getEnum(rs, RepositoryAttribute.DIRECTION_TYPE, WaterwayDirection.class));
        waterWays.add(inlandWaterwayType);
      }
    }
    return waterWays;
  }

  private static void sanityCheckRoute(final WKTGeometry geometry) throws AeriusException {
    //sanity check
    if (geometry.getType() != TYPE.LINE) {
      throw new AeriusException(Reason.SHIPPING_ROUTE_GEOMETRY_NOT_ALLOWED);
    }
  }

  private static PGgeometry toPostgis(final Connection con, final WKTGeometry geometry) throws SQLException, AeriusException {
    final int srid = ReceptorGridSettingsRepository.getSrid(con);
    sanityCheckRoute(geometry);
    final LineString line = new LineString(geometry.getWKT());
    line.setSrid(srid);
    return new PGgeometry(line);
  }

}
