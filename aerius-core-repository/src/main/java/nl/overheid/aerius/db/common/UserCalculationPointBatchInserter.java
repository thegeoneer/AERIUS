/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import nl.overheid.aerius.db.BatchInserter;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 *
 */
public class UserCalculationPointBatchInserter<T extends AeriusPoint> extends BatchInserter<T>
{

  private final Connection connection;
  private final int calculationPointSetId;
  private final ReceptorGridSettings rgs;
  private final ReceptorUtil receptorUtil;

  /**
   * @throws SQLException
   *
   */
  public UserCalculationPointBatchInserter(final Connection con, final int calculationPointSetId) throws SQLException {
    this.connection = con;
    this.calculationPointSetId = calculationPointSetId;
    this.rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(connection);
    this.receptorUtil = new ReceptorUtil(rgs);

  }

  @Override
  public void setParameters(final PreparedStatement ps, final T calculationPoint) throws SQLException {
    final int nearestReceptorId = receptorUtil.getReceptorIdFromCoordinate(calculationPoint.getX(), calculationPoint.getY(), rgs.getZoomLevel1());
    int paramIdx = 1;
    ps.setInt(paramIdx, calculationPointSetId);
    paramIdx++;
    ps.setInt(paramIdx, calculationPoint.getId());
    paramIdx++;
    ps.setString(paramIdx, calculationPoint.getLabel());
    paramIdx++;
    ps.setInt(paramIdx, nearestReceptorId);
    paramIdx++;
    ps.setDouble(paramIdx, calculationPoint.getX());
    paramIdx++;
    ps.setDouble(paramIdx, calculationPoint.getY());
    paramIdx++;
    if (calculationPoint instanceof OPSReceptor && ((OPSReceptor) calculationPoint).getAverageRoughness() != null) {
      final OPSReceptor opsReceptor = (OPSReceptor) calculationPoint;
      final Double avg = opsReceptor.getAverageRoughness();
      // The man rcp file has empty landuse columns.
      ps.setDouble(paramIdx, avg);
      paramIdx++;
      ps.setInt(paramIdx, opsReceptor.getLandUse().getOption());
      paramIdx++;
      final int[] landUses = opsReceptor.getLandUses();
      final Integer[] landUsesAsObjects = new Integer[landUses.length];
      for (int count = 0; count < landUses.length; count++) {
        landUsesAsObjects[count] = landUses[count];
      }
      ps.setArray(paramIdx++, connection.createArrayOf("integer", landUsesAsObjects));
      ps.setString(paramIdx, calculationPoint.getClass().getName());
    } else {
      addNulls(ps, paramIdx, calculationPoint);
    }
  }

  /**
   * @param ps
   * @param paramIdx
   * @param isReceptor
   * @throws SQLException
   */
  private void addNulls(final PreparedStatement ps, int paramIdx, final AeriusPoint calculationPoint) throws SQLException {
    ps.setNull(paramIdx, Types.DOUBLE);
    paramIdx++;
    ps.setNull(paramIdx, Types.INTEGER);
    paramIdx++;
    ps.setNull(paramIdx++, Types.ARRAY);
    ps.setString(paramIdx, calculationPoint.getClass().getName());
  }
}
