/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.AuditTrailChange;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Functionality for modifying permits in the database, e.g. inserting.
 *
 * A note about user permissions:
 * When calling these functions, the permissions should already haven been sorted out. So the deletePermit() function will not check
 * for DELETE_PERMIT permissions, this should already have been done. HOWEVER, there are certain checks that are most
 * efficiently done by the database. This is why some permission booleans are forwarded to the database function. The database
 * function will then throw an exception if the permissions fail.
 * So to summarize: when calling a repository function, its database function will always be called regardless of permissions. The
 * function might still fail when "subpermissions" do not check out.
 */
public final class PermitModifyRepository {

  private enum RepositoryAttribute implements Attribute {

    OLD_STATUS,
    NEW_STATUS;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(PermitModifyRepository.class);

  private static final JoinClause LOCK_JOIN_CLAUSE = new JoinClause("permits", RegisterAttribute.REQUEST_ID);

  private static final String INSERT_PERMIT =
      "INSERT INTO permits "
          + " (request_id, handler_id, dossier_id, received_date, remarks) "
          + " VALUES (?, ?, ?, ?, ?) ";

  private static final String UPDATE_PERMIT =
      "UPDATE permits SET "
          + " dossier_id = ?, received_date = ?, remarks = ?, handler_id = ? "
          + " WHERE request_id = ? ";

  private static final String UPDATE_PERMIT_STATUS =
      "SELECT ae_change_permit_state(?, ?::request_status_type) ";

  private static final String DELETE_PERMIT =
      "SELECT ae_delete_permit(?, ?) ";

  private static final Query DEQUEUE_PERMITS = QueryBuilder.from("ae_dequeue_permits()")
      .select(RegisterAttribute.REQUEST_ID, RepositoryAttribute.OLD_STATUS, RepositoryAttribute.NEW_STATUS).getQuery();

  private static final String RECEIVED_DATE_FORMAT = "dd-MM-yyyy HH:mm";

  private PermitModifyRepository() {
    //Not allowed to instantiate.
  }

  /**
   * @param con The connection to use.
   * @param permit The permit to insert.
   * @param insertedBy Which user wants to insert this permit.
   * @param pdfFileContent The content of the PDF.
   * @return updated permit with new key
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the permit already exists (by reference).
   */
  public static Permit insertNewPermit(final Connection con, final Permit permit, final UserProfile insertedBy,
      final InsertRequestFile insertRequestFile) throws SQLException, AeriusException {
    //check for existing permit based on key and reference.
    ensureNew(con, permit);

    //insert the request (ensure it's an application).
    insertRequestFile.setRequestFileType(RequestFileType.APPLICATION);
    final int requestId = RequestModifyRepository.insertNewRequest(con, SegmentType.PROJECTS, insertedBy.getAuthority(),
        permit, insertRequestFile);
    permit.setId(requestId);
    permit.setRequestState(RequestState.INITIAL);

    insertNewPermit(con, permit, requestId, insertedBy);

    RequestModifyRepository.insertSituations(con, permit.getSituations(), requestId);

    //insert the starting audit trail.
    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    RequestAuditRepository.addChange(changes, AuditTrailType.DOSSIER_ID, null, permit.getDossierMetaData().getDossierId());
    RequestAuditRepository.addNewRequestChanges(changes, permit.getReference(), SegmentType.PROJECTS);
    RequestAuditRepository.saveAuditTrailItem(con, permit, changes, insertedBy);

    return PermitRepository.getPermitByKey(con, permit.getPermitKey());
  }

  private static void insertNewPermit(final Connection con, final Permit permit, final int requestId, final UserProfile insertedBy)
      throws SQLException, AeriusException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_PERMIT)) {
      // (request_id, handler_id, dossier_id, received_date, remarks)
      QueryUtil.setValues(stmt,
          requestId,
          permit.getDossierMetaData().getHandler().getId(),
          permit.getDossierMetaData().getDossierId(),
          new Timestamp(permit.getDossierMetaData().getReceivedDate().getTime()),
          permit.getDossierMetaData().getRemarks());
      stmt.executeUpdate();
    } catch (final PSQLException e) {
      if (PMF.UNIQUE_VIOLATION.equals(e.getSQLState())) {
        LOG.error("PERMIT_ALREADY_EXISTS: {}", permit.getReference(), e);
        throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, permit.getReference());
      } else {
        throw e;
      }
    }
  }

  /**
   * @param con The connection to use.
   * @param key The key of the permit to update.
   * @param dossierMetaData The metaData to update.
   * @param editedBy Which user is performing the update.
   * @param lastModified When the entry is last modified according to the client (so we can detect if another change is already made)
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the permit does not exist (by id).
   */
  public static void updatePermit(final Connection con, final DossierAuthorityKey key, final DossierMetaData dossierMetaData,
      final UserProfile editedBy, final long lastModified) throws AeriusException {
    final Permit databasePermit = getExistingPermit(con, key, "updating");

    try {
      final Permit dossierPermit = PermitRepository.getSkinnedPermitByKey(con, new PermitKey(dossierMetaData.getDossierId(),
          dossierMetaData.getHandler().getAuthority().getCode()));
      if (dossierPermit != null && dossierPermit.getId() != databasePermit.getId()) {
        throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, dossierMetaData.getDossierId(),
            dossierMetaData.getHandler().getAuthority().getCode());
      }

      final LockRequestTransaction lock = new LockRequestTransaction(con, databasePermit.getId(), lastModified, LOCK_JOIN_CLAUSE);
      try {
        final ArrayList<AuditTrailChange> changes = getChanges(databasePermit.getDossierMetaData(), dossierMetaData);
        if (!databasePermit.getAuthority().equals(dossierMetaData.getHandler().getAuthority())) {
          RequestModifyRepository.updateRequestAuthority(con, databasePermit, dossierMetaData.getHandler().getAuthority());
        }
        try (final PreparedStatement updatePS = con.prepareStatement(UPDATE_PERMIT)) {
          int parameterIndex = 1;
          updatePS.setString(parameterIndex++, dossierMetaData.getDossierId());
          updatePS.setTimestamp(parameterIndex++, new Timestamp(dossierMetaData.getReceivedDate().getTime()));
          updatePS.setString(parameterIndex++, dossierMetaData.getRemarks());
          updatePS.setInt(parameterIndex++, dossierMetaData.getHandler().getId());
          updatePS.setInt(parameterIndex++, databasePermit.getId());
          updatePS.executeUpdate();
        }
        RequestModifyRepository.updateLastModified(con, databasePermit);
        RequestAuditRepository.saveAuditTrailItem(con, databasePermit, changes, editedBy);
      } catch (final SQLException se) {
        lock.rollback();
        throw DatabaseErrorCode.createAeriusException(se, LOG, "Error updating permit: " + key, key.getDossierId());
      } finally {
        lock.commit();
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error updating permit: " + key, key.getDossierId());
    }
  }

  private static ArrayList<AuditTrailChange> getChanges(final DossierMetaData existingMetaData, final DossierMetaData newMetaData) {
    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    RequestAuditRepository.addChange(changes, AuditTrailType.DOSSIER_ID,
        existingMetaData.getDossierId(),
        newMetaData.getDossierId());
    RequestAuditRepository.addChange(changes, AuditTrailType.HANDLER,
        toAuditChangeRepresentation(existingMetaData.getHandler()),
        toAuditChangeRepresentation(newMetaData.getHandler()));
    RequestAuditRepository.addChange(changes, AuditTrailType.DATE_RECEIVED,
        toAuditChangeRepresentation(existingMetaData.getReceivedDate()),
        toAuditChangeRepresentation(newMetaData.getReceivedDate()));
    return changes;
  }

  private static String toAuditChangeRepresentation(final UserProfile userProfile) {
    return userProfile.getLastName() + ", " + userProfile.getInitials() + " (" + userProfile.getAuthority().getDescription() + ")";
  }

  private static String toAuditChangeRepresentation(final Date date) {
    return new SimpleDateFormat(RECEIVED_DATE_FORMAT, LocaleUtils.getDefaultLocale()).format(date);
  }

  /**
   * @param con The connection to use.
   * @param key The key of the permit to update.
   * @param state The state to update.
   * @param editedBy Which user is performing the update.
   * @param lastModified When the entry is last modified according to the client (so we can detect if another change is already made)
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the permit does not exist (by id).
   */
  public static void updateState(final Connection con, final DossierAuthorityKey key, final RequestState state,
      final UserProfile editedBy, final long lastModified) throws AeriusException {
    final Permit databasePermit = getExistingPermit(con, key, "updating permit state");
    try {
      final LockRequestTransaction lock = new LockRequestTransaction(con, databasePermit.getId(), lastModified, LOCK_JOIN_CLAUSE);
      try {
        final ArrayList<AuditTrailChange> changes = new ArrayList<>();
        RequestAuditRepository.addChange(changes, AuditTrailType.STATE, databasePermit.getRequestState().getDbValue(), state.getDbValue());

        // REJECTED_FINAL will be skipped because it is a nonexisting state in the database. However,
        // all the audit logging should commence as normal.
        if (state != RequestState.REJECTED_FINAL) {
          try (final PreparedStatement updatePS = con.prepareStatement(UPDATE_PERMIT_STATUS)) {
            QueryUtil.setValues(updatePS, databasePermit.getId(), state.getDbValue());
            updatePS.execute();
          }
        }

        RequestAuditRepository.saveAuditTrailItem(con, databasePermit, changes, editedBy);

        // Remove old decree files (unless we're going from ASSIGNED to ASSIGNED_FINAL).
        // Yes in cases like INITIAL to QUEUED and such this will not do anything useful, but this makes the code WAY simpler
        //  while not degrading performance.
        if (RequestState.ASSIGNED != databasePermit.getRequestState() || RequestState.ASSIGNED_FINAL != state) {
          RequestModifyRepository.deleteRequestFile(con, databasePermit.getId(), RequestFileType.DECREE);
          RequestModifyRepository.deleteRequestFile(con, databasePermit.getId(), RequestFileType.DETAIL_DECREE);
        }
      } catch (final SQLException se) {
        lock.rollback();
        throw DatabaseErrorCode.createAeriusException(se, LOG, "Error updating permit state: " + key, key.getDossierId());
      } finally {
        lock.commit();
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Transaction error during updating permit state: " + key, key.getDossierId());
    }
  }

  /**
   * @param con The connection to use.
   * @param permit The permit to delete.
   * @param deletedBy Which user is performing the delete.
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the permit does not exist (by key).
   */
  public static void deletePermit(final Connection con, final Permit permit, final UserProfile deletedBy) throws AeriusException {
    final Permit databasePermit = getExistingPermit(con, permit.getPermitKey(), "deleting");
    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    RequestAuditRepository.addChange(changes, AuditTrailType.DELETE, databasePermit.getRequestState(), null);

    // Check for limited deletion permissions. Function will test this.
    final boolean onlyInactiveInitial =
        UserProfileUtil.hasPermissionForAuthority(deletedBy, permit.getAuthority(), RegisterPermission.DELETE_PERMIT_INACTIVE_INITIAL)
            && !UserProfileUtil.hasPermissionForAuthority(deletedBy, permit.getAuthority(), RegisterPermission.DELETE_PERMIT_ALL)
            && permit.getRequestState() != RequestState.REJECTED_FINAL;

    try {
      try (final PreparedStatement ps = con.prepareStatement(DELETE_PERMIT)) {
        QueryUtil.setValues(ps, databasePermit.getId(), onlyInactiveInitial);
        ps.executeQuery();
      }
      RequestAuditRepository.saveAuditTrailItem(con, databasePermit, changes, deletedBy);
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error deleting permit: " + permit.getPermitKey(), permit.getPermitKey().getDossierId());
    }

    //indicate permit is deleted by setting id = 0
    permit.setId(0);
  }

  private static Permit getExistingPermit(final Connection con, final DossierAuthorityKey key, final String action) throws AeriusException {
    try {
      final Permit permit = PermitRepository.getSkinnedPermitByKey(con, key);
      if (permit == null) {
        LOG.error("Unknown permit ID when {}: {}", action, key);
        throw new AeriusException(Reason.PERMIT_UNKNOWN, key.getDossierId());
      }
      return permit;
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error getting exisitng permit: " + key, key.getDossierId());
    }
  }

  /**
   * Dequeue permits, changing state based on their received date and available deposition space.
   * @param con The connection to use.
   * @param userProfile The userprofile that is executing this dequeueing bit.
   * @throws AeriusException In case of database errors.
   */
  public static void dequeuePermits(final Connection con, final UserProfile userProfile) throws AeriusException {
    try {
      final Transaction transaction = new Transaction(con);
      try (final PreparedStatement selectPS = con.prepareStatement(DEQUEUE_PERMITS.get());
          final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          final String oldStatus = QueryUtil.getString(rs, RepositoryAttribute.OLD_STATUS);
          final String newStatus = QueryUtil.getString(rs, RepositoryAttribute.NEW_STATUS);
          if (!oldStatus.equals(newStatus)) {
            final int requestId = QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID);
            final ArrayList<AuditTrailChange> changes = new ArrayList<>();
            RequestAuditRepository.addChange(changes, AuditTrailType.STATE,
                oldStatus, newStatus);

            final Permit databasePermit = PermitRepository.getPermit(con, requestId);

            RequestAuditRepository.saveAuditTrailItem(con, databasePermit, changes, userProfile);
          }
        }
      } catch (final SQLException le) {
        transaction.rollback();
        throw DatabaseErrorCode.createAeriusException(le, LOG, "Error dequeuing permits");
      } finally {
        transaction.commit();
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error with transaction");
    }
  }

  private static void ensureNew(final Connection con, final Permit permit) throws SQLException, AeriusException {
    final Permit existingPermitByKey = PermitRepository.getSkinnedPermitByKey(con, permit.getPermitKey());
    final PermitKey existingKeyByReference = PermitRepository.getPermitKey(con, permit.getReference());
    if (existingPermitByKey != null || existingKeyByReference != null) {
      throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, permit.getPermitKey().getDossierId(),
          permit.getPermitKey().getAuthorityCode());
    }
  }
}
