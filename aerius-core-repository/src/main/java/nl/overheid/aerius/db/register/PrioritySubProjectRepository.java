/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.db.common.EmissionResultRepositoryUtil;
import nl.overheid.aerius.db.util.ILikeWhereClause;
import nl.overheid.aerius.db.util.OrderByClause;
import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SearchWhereClause;
import nl.overheid.aerius.db.util.SpecialAttribute;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Functionality for fetching priority subproject information from the database.
 */
public final class PrioritySubProjectRepository {

  private static final int UNKNOWN_PRIORITY_SUBPROJECT_ID = -1;

  private static final Query GET_PRIORITY_SUBPROJECT_ID_BY_KEY = getPrioritySubProjectBase()
      .where(RegisterAttribute.REFERENCE).getQuery();

  private static final Query GET_PRIORITY_SUBPROJECT = getPrioritySubProjectFullInfo()
      .where(RegisterAttribute.REQUEST_ID).getQuery();

  private static final Query GET_PRIORITY_SUBPROJECTS_LIST = getPrioritySubProjectFullInfo()
      .orderBy(new OrderByClause(RegisterAttribute.INSERT_DATE, OrderType.ASC),
          new OrderByClause(RegisterAttribute.REQUEST_ID, OrderType.DESC))
      .limit().offset().getQuery();

  private static final Query GET_PARENT_PROJECT_ID = getPrioritySubProjectBase()
      .select(RegisterAttribute.PRIORITY_PROJECT_REQUEST_ID)
      .where(RegisterAttribute.REQUEST_ID).getQuery();

  private static final Query GET_ASSIGNED_RESULTS = QueryBuilder.from("request_demands_gml_export_view")
      .select(QueryAttribute.RECEPTOR_ID).select(EmissionResultRepositoryUtil.REQUIRED_ATTRIBUTES)
      .where(RegisterAttribute.REQUEST_ID).getQuery();

  private static final Query SEARCH_PRIORITY_SUBPROJECTS = getPrioritySubProjectFullInfo()
      .where(new SearchWhereClause(
          RegisterAttribute.REFERENCE, RegisterAttribute.PROJECT_NAME, RegisterAttribute.CORPORATION))
      .getQuery();

  // Not allowed to instantiate.
  private PrioritySubProjectRepository() {
  }

  private static QueryBuilder getPrioritySubProjectBase() {
    return QueryBuilder.from("priority_subprojects_view")
        .select(RegisterAttribute.REQUEST_ID);
  }

  private static QueryBuilder getPrioritySubProjectFullInfo() {
    final QueryBuilder builder = getPrioritySubProjectBase();
    RequestRepository.addRequiredAttributes(builder);
    return builder.select(RegisterAttribute.STATUS, RegisterAttribute.SEGMENT, RegisterAttribute.INSERT_DATE,
        RegisterAttribute.PRIORITY_PROJECT_DOSSIER_ID, RegisterAttribute.PRIORITY_PROJECT_AUTHORITY, QueryAttribute.LABEL);
  }

  /**
   * @param con The connection to use.
   * @param key The PrioritySubProjectKey to check
   * @return True if the priority subproject exists, false if not.
   * @throws SQLException In case of a database error.
   */
  public static boolean prioritySubProjectExists(final Connection con, final PrioritySubProjectKey key) throws SQLException {
    return getPrioritySubProjectIdByKey(con, key) != UNKNOWN_PRIORITY_SUBPROJECT_ID;
  }

  /**
   * Get a skinned priority subproject without detailed information such as situations, audit trail or validation rules.
   * @param con Connection to use.
   * @param key PrioritySubProjectKey used to retrieve the priority subproject.
   * @return A skinned priority subproject
   * @throws SQLException In case of a database error.
   */
  public static PrioritySubProject getSkinnedPrioritySubProjectByKey(final Connection con, final PrioritySubProjectKey key) throws SQLException {
    return getSkinnedPrioritySubProject(con, getPrioritySubProjectIdByKey(con, key));
  }

  /**
   * Get a priority subproject by its key.
   * @param con Connection to use.
   * @param key PrioritySubProjectKey used to retrieve the priority subproject.
   * @return A priority subproject
   * @throws SQLException In case of a database error.
   */
  public static PrioritySubProject getPrioritySubProjectByKey(final Connection con, final PrioritySubProjectKey key) throws SQLException {
    return getPrioritySubProject(con, getPrioritySubProjectIdByKey(con, key));
  }

  /**
   * Get an priority subproject without detailed information such as situations, audit trail or validation rules.
   * @param con Connection to use.
   * @param prioritySubProjectId Priority subproject id to retrieve.
   * @return A skinned priority subproject
   * @throws SQLException In case of a database error.
   */
  public static PrioritySubProject getSkinnedPrioritySubProject(final Connection con, final int prioritySubProjectId) throws SQLException {
    return getPrioritySubProject(con, prioritySubProjectId, false);
  }

  /**
   * Get a priority subproject with all information such as situations, audit trail or validation rules.
   * @param con Connection to use.
   * @param prioritySubProjectId Priority subproject id to retrieve.
   * @return A skinned priority subproject
   * @throws SQLException In case of a database error.
   */
  protected static PrioritySubProject getPrioritySubProject(final Connection con, final int prioritySubProjectId) throws SQLException {
    return getPrioritySubProject(con, prioritySubProjectId, true);
  }

  /**
   * @param con The connection to use.
   * @param prioritySubProjectId DB Id of the priority subproject to retrieve.
   * @param fetchAdditionalInfo Whether to also fetch emission values, development rules, result information and change history.
   * @return The proper priority subproject (or null if not found).
   * @throws SQLException In case of a database error.
   */
  private static PrioritySubProject getPrioritySubProject(final Connection con, final int prioritySubProjectId, final boolean fetchAdditionalInfo)
      throws SQLException {
    PrioritySubProject prioritySubProject = null;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_PRIORITY_SUBPROJECT.get())) {
      GET_PRIORITY_SUBPROJECT.setParameter(selectPS, RegisterAttribute.REQUEST_ID, prioritySubProjectId);

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          prioritySubProject = getPrioritySubProjectFromResultSet(con, rs);
        }
      }
    }

    if (prioritySubProject != null && fetchAdditionalInfo) {
      prioritySubProject.setSituations(RequestRepository.getSituations(con, prioritySubProjectId));
      //TODO: load parent project too? Beware of loops!
    }

    return prioritySubProject;
  }

  /**
   * @param con The connection to use.
   * @param key The PrioritySubProjectKey to retrieve the (DB) priority subproject id for.
   * @return The proper priority subproject id or -1 if not found
   * @throws SQLException In case of a database error.
   */
  static int getPrioritySubProjectIdByKey(final Connection con, final PrioritySubProjectKey key) throws SQLException {
    int prioritySubProjectId = UNKNOWN_PRIORITY_SUBPROJECT_ID;

    if (key != null) {
      try (final PreparedStatement selectPS = con.prepareStatement(GET_PRIORITY_SUBPROJECT_ID_BY_KEY.get())) {
        GET_PRIORITY_SUBPROJECT_ID_BY_KEY.setParameter(selectPS, RegisterAttribute.REFERENCE, key.getReference());

        try (final ResultSet rs = selectPS.executeQuery()) {
          if (rs.next()) {
            prioritySubProjectId = QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID);
          }
        }
      }
    }

    return prioritySubProjectId;
  }

  /**
   * @param con The connection to use.
   * @param maxResults The maximum results to return.
   * @param offset The offset of the results (to be used for paging).
   * @return A list of priority subprojects.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<PrioritySubProject> getPrioritySubProjects(final Connection con, final int maxResults, final int offset)
      throws SQLException {
    final ArrayList<PrioritySubProject> prioritySubProjects = new ArrayList<>();
    try (final PreparedStatement selectPS = con.prepareStatement(GET_PRIORITY_SUBPROJECTS_LIST.get())) {
      GET_PRIORITY_SUBPROJECTS_LIST.setParameter(selectPS, SpecialAttribute.LIMIT, maxResults);
      GET_PRIORITY_SUBPROJECTS_LIST.setParameter(selectPS, SpecialAttribute.OFFSET, offset);

      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          prioritySubProjects.add(getPrioritySubProjectFromResultSet(con, rs));
        }
      }
    }
    return prioritySubProjects;
  }

  private static PrioritySubProject getPrioritySubProjectFromResultSet(final Connection con, final ResultSet rs) throws SQLException {
    final PrioritySubProject prioritySubProject = new PrioritySubProject();
    prioritySubProject.setId(QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID));
    prioritySubProject.setLabel(QueryUtil.getString(rs, QueryAttribute.LABEL));
    prioritySubProject.setRequestState(QueryUtil.getEnum(rs, RegisterAttribute.STATUS, RequestState.class));
    prioritySubProject.setReceivedDate(rs.getTimestamp(RegisterAttribute.INSERT_DATE.attribute()));
    final PriorityProjectKey parentKey = new PriorityProjectKey(
        QueryUtil.getString(rs, RegisterAttribute.PRIORITY_PROJECT_DOSSIER_ID),
        QueryUtil.getString(rs, RegisterAttribute.PRIORITY_PROJECT_AUTHORITY));
    prioritySubProject.setParentKey(parentKey);
    RequestRepository.fillRequestFromResultSet(con, rs, prioritySubProject);
    return prioritySubProject;
  }

  /**
   * @param con The connection to use.
   * @param prioritySubProjectId DB Id of the priority subproject to retrieve the matching priority project's ID for.
   * @return The proper priority project ID, the 'koepelproject' (or null if not found).
   * @throws SQLException In case of a database error.
   */
  public static int getParentProjectIdForSubProject(final Connection con, final int prioritySubProjectId)
      throws SQLException {
    int parentProjectId = UNKNOWN_PRIORITY_SUBPROJECT_ID;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_PARENT_PROJECT_ID.get())) {
      GET_PARENT_PROJECT_ID.setParameter(selectPS, RegisterAttribute.REQUEST_ID, prioritySubProjectId);

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          parentProjectId = QueryUtil.getInt(rs, RegisterAttribute.PRIORITY_PROJECT_REQUEST_ID);
        }
      }
    }

    return parentProjectId;
  }

  /**
   * @param con The connection to use.
   * @param requestId The ID of the subproject to get assigned results for.
   * Assigned results in this case are all results for the supplied request (even if not assigned)
   * + any results for other subprojects of the same priority project that are assigned.
   * @return The AeriusResultPoints containing the 'assigned' emission results (deposition only, per substance).
   * @throws SQLException In case of database errors.
   */
  public static List<AeriusResultPoint> getAssignedResultsInclSubProject(final Connection con, final int requestId) throws SQLException {
    return RequestRepository.getResultsForExport(con, requestId, GET_ASSIGNED_RESULTS);
  }

  /**
   * @param con The connection to use.
   * @param searchString The string to search for.
   * @return Any priority subprojects that match the string.
   * @throws SQLException In case of exception
   */
  public static ArrayList<PrioritySubProject> search(final Connection con, final String searchString) throws SQLException {
    final ArrayList<PrioritySubProject> prioritySubProjects = new ArrayList<>();
    final String sqlSearchString = ILikeWhereClause.getSearchString(searchString);
    try (final PreparedStatement selectPS = con.prepareStatement(SEARCH_PRIORITY_SUBPROJECTS.get())) {
      SEARCH_PRIORITY_SUBPROJECTS.setParameter(selectPS, RegisterAttribute.REFERENCE, sqlSearchString);
      SEARCH_PRIORITY_SUBPROJECTS.setParameter(selectPS, RegisterAttribute.PROJECT_NAME, sqlSearchString);
      SEARCH_PRIORITY_SUBPROJECTS.setParameter(selectPS, RegisterAttribute.CORPORATION, sqlSearchString);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        prioritySubProjects.add(getPrioritySubProjectFromResultSet(con, rs));
      }
    }

    return prioritySubProjects;
  }

  /**
   * Returns the oldest PrioritySubProject in the database that has no decree file yet and has state 'assigned' or 'assigned_final'.
   * @param con The connection to use.
   * @return null if all priority sub projects that should have a decree file actually have one.
   * @throws SQLException In case of a database error.
   */
  public static PrioritySubProject getOldestWithoutDecrees(final Connection con, final RequestFileType fileType) throws SQLException {
    final Integer id = RequestRepository.getOldestRequestIdWithoutDecrees(con, fileType, SegmentType.PRIORITY_SUBPROJECTS);
    return id == null ? null : getPrioritySubProject(con, id);
  }

}
