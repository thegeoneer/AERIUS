/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.InsertBuilder;
import nl.overheid.aerius.db.util.InsertClause;
import nl.overheid.aerius.db.util.PGisUtils;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.importer.ImportedImaerFile;
import nl.overheid.aerius.shared.domain.importer.UserGeoLayer;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.UuidUtil;

/**
 * Provides CRUD functionality for User GEO Layers.
 */
public class UserGeoLayerRepository {

  private enum RepositoryAttribute implements Attribute {

    IMPORTED_IMAER_FILE_ID,
    VERSION,
    FEATURE,
    IMAER_VERSION,
    IMAER_FEATURE,
    IMPORTED_IMAER_FEATURE_ID,
    USER_GEO_LAYER_ID;

    @Override
    public String attribute() {
      return name().toLowerCase(Locale.ENGLISH);
    }

  }

  private static final Logger LOG = LoggerFactory.getLogger(UserGeoLayerRepository.class);

  private static final String IMPORTED_IMAER_FILES = "imported_imaer_files";
  private static final String IMPORTED_IMAER_FEATURES = "imported_imaer_features";
  private static final String USER_GEO_LAYERS = "user_geo_layers";
  private static final String USER_GEO_LAYER_FEATURES = "user_geo_layer_features";
  private static final String AE_SELECT_IMAER_FEATURES = "ae_select_imaer_features(?, ?, ?, ?)";

  private static final Query INSERT_IMPORTED_IMAER_FILE =
      InsertBuilder.into(IMPORTED_IMAER_FILES)
        .insert(RepositoryAttribute.VERSION, QueryAttribute.FILENAME).getQuery();

  private static final Query SELECT_IMPORTED_IMAER_FILE =
      QueryBuilder.from(IMPORTED_IMAER_FILES)
          .select(RepositoryAttribute.IMPORTED_IMAER_FILE_ID, RepositoryAttribute.VERSION, QueryAttribute.FILENAME)
          .where(RepositoryAttribute.IMPORTED_IMAER_FILE_ID).getQuery();

  private static final Query INSERT_IMPORTED_IMAER_FEATURE =
      InsertBuilder.into(IMPORTED_IMAER_FEATURES)
        .insert(RepositoryAttribute.IMPORTED_IMAER_FILE_ID, RepositoryAttribute.FEATURE, QueryAttribute.ID)
        .insert(new InsertClause(QueryAttribute.GEOMETRY.attribute(), "ST_SetSRID(?, ae_get_srid())", QueryAttribute.GEOMETRY)).getQuery();

  private static final Query SELECT_IMPORTED_IMAER_FEATURE_ID =
      QueryBuilder.from(IMPORTED_IMAER_FEATURES)
        .select(RepositoryAttribute.IMPORTED_IMAER_FEATURE_ID)
        .where(RepositoryAttribute.IMPORTED_IMAER_FILE_ID, QueryAttribute.ID).getQuery();

  private static final Query SELECT_IMAER_FEATURES_FOR_POINT =
      QueryBuilder.from(AE_SELECT_IMAER_FEATURES,
          RepositoryAttribute.IMPORTED_IMAER_FILE_ID, QueryAttribute.X_COORD, QueryAttribute.Y_COORD, QueryAttribute.BUFFER)
      .getQuery();

  private static final Query INSERT_USER_GEO_LAYER =
      InsertBuilder.into(USER_GEO_LAYERS)
        .insert(RepositoryAttribute.IMPORTED_IMAER_FILE_ID, QueryAttribute.KEY, QueryAttribute.TYPE).getQuery();

  private static final Query SELECT_USER_GEO_LAYER_ID =
      QueryBuilder.from(USER_GEO_LAYERS)
        .select(RepositoryAttribute.USER_GEO_LAYER_ID)
        .where(RepositoryAttribute.IMPORTED_IMAER_FILE_ID, QueryAttribute.TYPE).getQuery();

  private static final Query SELECT_USER_GEO_LAYER =
      QueryBuilder.from(USER_GEO_LAYERS)
        .select(QueryAttribute.KEY, QueryAttribute.TYPE)
        .where(RepositoryAttribute.IMPORTED_IMAER_FILE_ID).getQuery();

  private static final Query INSERT_USER_GEO_LAYER_FEATURE =
      InsertBuilder.into(USER_GEO_LAYER_FEATURES)
        .insert(RepositoryAttribute.USER_GEO_LAYER_ID, RepositoryAttribute.IMPORTED_IMAER_FEATURE_ID, QueryAttribute.VALUE).getQuery();

  private static final String QUERY_DELETE_IMPORTED_IMAER_FILE = "SELECT ae_delete_imported_imaer_file(?)";

  private static final String QUERY_IMPORTED_IMAER_FILES_WITH_MIN_AGE =
      "SELECT imported_imaer_file_id"
          + " FROM " + IMPORTED_IMAER_FILES
          + "  WHERE insert_date <= now() - (interval '1 day' * ?)";

  public static int insertImaerFile(final Connection con, final String version, final String filename) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_IMPORTED_IMAER_FILE.get(), Statement.RETURN_GENERATED_KEYS)) {
      INSERT_IMPORTED_IMAER_FILE.setParameter(stmt, RepositoryAttribute.VERSION, version);
      INSERT_IMPORTED_IMAER_FILE.setParameter(stmt, QueryAttribute.FILENAME, filename);

      stmt.executeUpdate();
      final ResultSet rst = stmt.getGeneratedKeys();
      if (rst.next()) {
        return rst.getInt(1);
      } else {
        throw new SQLException("No generated key obtained while inserting new imported IMAER file.");
      }
    }
  }

  public static ImportedImaerFile getImportedImaerFile(final Connection con, final int importedImaerFileId) throws SQLException {
    ImportedImaerFile file = null;

    try (final PreparedStatement stmt = con.prepareStatement(SELECT_IMPORTED_IMAER_FILE.get())) {
      SELECT_IMPORTED_IMAER_FILE.setParameter(stmt, RepositoryAttribute.IMPORTED_IMAER_FILE_ID, importedImaerFileId);

      final ResultSet rst = stmt.executeQuery();
      if (rst.next()) {
        file = new ImportedImaerFile()
          .setId(QueryUtil.getInt(rst, RepositoryAttribute.IMPORTED_IMAER_FILE_ID))
          .setVersion(QueryUtil.getString(rst, RepositoryAttribute.VERSION))
          .setFilename(QueryUtil.getString(rst, QueryAttribute.FILENAME));
      }
    }

    return file;
  }

  public static int insertImaerFeature(final Connection con, final int importedImaerFileId, final String feature, final String featureId,
      final WKTGeometry geom) throws SQLException, AeriusException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_IMPORTED_IMAER_FEATURE.get(), Statement.RETURN_GENERATED_KEYS)) {
      INSERT_IMPORTED_IMAER_FEATURE.setParameter(stmt, RepositoryAttribute.IMPORTED_IMAER_FILE_ID, importedImaerFileId);
      INSERT_IMPORTED_IMAER_FEATURE.setParameter(stmt, RepositoryAttribute.FEATURE, feature);
      INSERT_IMPORTED_IMAER_FEATURE.setParameter(stmt, QueryAttribute.ID, featureId);
      INSERT_IMPORTED_IMAER_FEATURE.setParameter(stmt, QueryAttribute.GEOMETRY, PGisUtils.getPGgeometry(geom.getWKT()));

      stmt.executeUpdate();
      final ResultSet rst = stmt.getGeneratedKeys();
      if (rst.next()) {
        return rst.getInt(1);
      } else {
        throw new SQLException("No generated key obtained while inserting IMAER feature.");
      }
    }
  }

  public static Integer getImaerFeatureId(final Connection con, final int importedImaerFileId, final String featureId) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(SELECT_IMPORTED_IMAER_FEATURE_ID.get())) {
      SELECT_IMPORTED_IMAER_FEATURE_ID.setParameter(stmt, RepositoryAttribute.IMPORTED_IMAER_FILE_ID, importedImaerFileId);
      SELECT_IMPORTED_IMAER_FEATURE_ID.setParameter(stmt, QueryAttribute.ID, featureId);

      final ResultSet rst = stmt.executeQuery();

      return rst.next() ? QueryUtil.getInt(rst, RepositoryAttribute.IMPORTED_IMAER_FEATURE_ID) : null;
    }
  }

  public static int insertUserGeoLayer(final Connection con, final int importedImaerFileId, final String type) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_USER_GEO_LAYER.get(), Statement.RETURN_GENERATED_KEYS)) {
      INSERT_USER_GEO_LAYER.setParameter(stmt, RepositoryAttribute.IMPORTED_IMAER_FILE_ID, importedImaerFileId);
      INSERT_USER_GEO_LAYER.setParameter(stmt, QueryAttribute.KEY, UuidUtil.getStripped());
      INSERT_USER_GEO_LAYER.setParameter(stmt, QueryAttribute.TYPE, type);

      stmt.executeUpdate();
      final ResultSet rst = stmt.getGeneratedKeys();
      if (rst.next()) {
        return rst.getInt(1);
      } else {
        throw new SQLException("No generated key obtained while inserting user geo layer.");
      }
    }
  }

  public static Integer getUserGeoLayerId(final Connection con, final int importedImaerFileId, final String type) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(SELECT_USER_GEO_LAYER_ID.get())) {
      SELECT_USER_GEO_LAYER_ID.setParameter(stmt, RepositoryAttribute.IMPORTED_IMAER_FILE_ID, importedImaerFileId);
      SELECT_USER_GEO_LAYER_ID.setParameter(stmt, QueryAttribute.TYPE, type);

      final ResultSet rst = stmt.executeQuery();

      return rst.next() ? QueryUtil.getInt(rst, RepositoryAttribute.USER_GEO_LAYER_ID) : null;
    }
  }

  public static ArrayList<UserGeoLayer> getUserGeoLayer(final Connection con, final int importedImaerFileId) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(SELECT_USER_GEO_LAYER.get())) {
      SELECT_USER_GEO_LAYER.setParameter(stmt, RepositoryAttribute.IMPORTED_IMAER_FILE_ID, importedImaerFileId);

      final ArrayList<UserGeoLayer> layers = new ArrayList<>();
      final ResultSet rst = stmt.executeQuery();
      while (rst.next()) {
        layers.add(new UserGeoLayer()
              .setKey(QueryUtil.getString(rst, QueryAttribute.KEY))
              .setType(QueryUtil.getString(rst, QueryAttribute.TYPE))
        );
      }

      return layers;
    }
  }

  public static int insertUserGeoLayerFeature(final Connection con, final int userGeoLayerId, final int importedImaerFeatureId, final String value)
      throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_USER_GEO_LAYER_FEATURE.get(), Statement.RETURN_GENERATED_KEYS)) {
      INSERT_USER_GEO_LAYER_FEATURE.setParameter(stmt, RepositoryAttribute.USER_GEO_LAYER_ID, userGeoLayerId);
      INSERT_USER_GEO_LAYER_FEATURE.setParameter(stmt, RepositoryAttribute.IMPORTED_IMAER_FEATURE_ID, importedImaerFeatureId);
      INSERT_USER_GEO_LAYER_FEATURE.setParameter(stmt, QueryAttribute.VALUE, value);

      stmt.executeUpdate();
      final ResultSet rst = stmt.getGeneratedKeys();
      if (rst.next()) {
        return rst.getInt(1);
      } else {
        throw new SQLException("No generated key obtained while inserting user geo layer feature.");
      }
    }
  }

  public static ArrayList<String> getImaerFeaturesForPoint(final Connection con, final int importedImaerFileId, final AeriusPoint point,
      final int buffer) throws SQLException {
    final ArrayList<String> sources = new ArrayList<>();

    try (final PreparedStatement stmt = con.prepareStatement(SELECT_IMAER_FEATURES_FOR_POINT.get())) {
      SELECT_IMAER_FEATURES_FOR_POINT.setParameter(stmt, RepositoryAttribute.IMPORTED_IMAER_FILE_ID, importedImaerFileId);
      SELECT_IMAER_FEATURES_FOR_POINT.setParameter(stmt, QueryAttribute.X_COORD, (int) Math.round(point.getX()));
      SELECT_IMAER_FEATURES_FOR_POINT.setParameter(stmt, QueryAttribute.Y_COORD, (int) Math.round(point.getY()));
      SELECT_IMAER_FEATURES_FOR_POINT.setParameter(stmt, QueryAttribute.BUFFER, buffer);

      final ResultSet rst = stmt.executeQuery();
      while (rst.next()) {
        sources.add(QueryUtil.getString(rst, RepositoryAttribute.IMAER_FEATURE));
      }
    }

    return sources;
  }

  private static boolean removeImportedImaerFile(final Connection con, final int importId) {
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_DELETE_IMPORTED_IMAER_FILE)) {
      QueryUtil.setValues(stmt, importId);
      stmt.execute();
    } catch (final SQLException e) {
      LOG.error("Error removing Imported IMAER file id {}", importId, e);
      return false;
    }

    return true;
  }

  public static int removeImportsWithMinAge(final Connection con, final int ageInDays) {
    int amountRemoved = 0;
    try (final PreparedStatement stmt = con.prepareStatement(QUERY_IMPORTED_IMAER_FILES_WITH_MIN_AGE)) {
      QueryUtil.setValues(stmt, ageInDays);
      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        if (removeImportedImaerFile(con, QueryUtil.getInt(rs, RepositoryAttribute.IMPORTED_IMAER_FILE_ID))) {
          amountRemoved++;
        }
      }
    } catch (final SQLException e) {
      LOG.error("Fetching imported IMAER files to remove failed. Hard.", e);
    }

    return amountRemoved;
  }
}
