/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Filter;
import nl.overheid.aerius.shared.domain.user.UserProfile;

/**
 *
 */
public final class FilterRepository {

  private static final Logger LOG = LoggerFactory.getLogger(FilterRepository.class);

  private enum RepositoryAttribute implements Attribute {

    FILTER_TYPE,
    FILTER_CONTENT;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final String UPDATE_FILTER = "UPDATE user_filters SET filter_content = ?::json WHERE user_id = ? AND filter_type = ?";
  private static final String INSERT_FILTER = "INSERT INTO user_filters (user_id, filter_type, filter_content) SELECT ?, ?, ?::json ";
  private static final String UPSERT_FILTER = " WITH upsert AS (" + UPDATE_FILTER + " RETURNING *) " + INSERT_FILTER
      + " WHERE NOT EXISTS (SELECT * FROM upsert);";

  private static final Query GET_FILTER = QueryBuilder.from("user_filters")
      .select(RepositoryAttribute.FILTER_CONTENT)
      .where(QueryAttribute.USER_ID, RepositoryAttribute.FILTER_TYPE)
      .getQuery();

  private FilterRepository() {}

  /**
   * Insert or update a filter for a user.
   * If there is no filter of that type for the user in the database, it will be inserted. If there is, it'll be updated.
   *
   * @param con The connection to use.
   * @param filter The filter to insert or update.
   * @param userProfile The UserProfile that is using the filter.
   * @throws SQLException In case of database exceptions.
   */
  public static void insertOrUpdateFilter(final Connection con, final Filter filter, final UserProfile userProfile) throws SQLException {
    final String filterType = filter.getClass().getCanonicalName();
    final Gson gson = new Gson();
    final String jsonFilter = gson.toJson(filter);
    try (final PreparedStatement upsertPS = con.prepareStatement(UPSERT_FILTER)) {
      int parameterIdx = 1;
      //update part
      upsertPS.setString(parameterIdx++, jsonFilter);
      upsertPS.setInt(parameterIdx++, userProfile.getId());
      upsertPS.setString(parameterIdx++, filterType);
      //insert part
      upsertPS.setInt(parameterIdx++, userProfile.getId());
      upsertPS.setString(parameterIdx++, filterType);
      upsertPS.setString(parameterIdx++, jsonFilter);

      upsertPS.executeUpdate();
    }
  }

  /**
   * @param con The connection to use.
   * @param userProfile The user profile to get the filter for.
   * @param filterClass The actual filter class to retrieve.
   * @param <T> The type of the filter.
   * @return The filter for the user or new instance if not found.
   * @throws SQLException In case of database exceptions.
   */
  public static <T extends Filter> T getFilter(final Connection con, final UserProfile userProfile, final Class<T> filterClass) throws SQLException {
    T filter = null;
    try (final PreparedStatement stmt = con.prepareStatement(GET_FILTER.get())) {
      GET_FILTER.setParameter(stmt, QueryAttribute.USER_ID, userProfile.getId());
      GET_FILTER.setParameter(stmt, RepositoryAttribute.FILTER_TYPE, filterClass.getCanonicalName());

      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        final Gson gson = new Gson();
        filter = gson.fromJson(QueryUtil.getString(rs, RepositoryAttribute.FILTER_CONTENT), filterClass);
      }
    } catch (final JsonSyntaxException e) {
      LOG.error("Could not convert Json to {} for user with ID {}, falling back to default", filterClass.getCanonicalName(), userProfile.getId(), e);
    }

    if (filter == null) {
      try {
        filter = filterClass.newInstance();
        filter.fillDefault();
      } catch (final ReflectiveOperationException e) {
        LOG.error("Could not instantiate {}", filterClass, e);
      }
    }

    return filter;
  }

}
