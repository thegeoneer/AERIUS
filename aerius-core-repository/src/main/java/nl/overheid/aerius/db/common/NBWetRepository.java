/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;

/**
 * Wrapper class to be able to stub repository classes.
 */
public class NBWetRepository {

  private static final String RECEPTOR_COUNT_QUERY =
      "SELECT count(receptor_id) FROM receptors_to_assessment_areas_on_relevant_habitat_view where assessment_area_id = ?";

  private static final String ASSESMENT_AREA_DISTANCES =
      "SELECT assessment_area_id, name, ST_Distance(geometry, ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()))::real as distance"
          + " FROM assessment_areas WHERE type = 'natura2000_area'"
          + " ORDER BY ST_Distance(geometry, ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()))::real";

  private static final String MAX_CALCULATION_RESULT_BY_ASSESSMENT_AREA =
      "SELECT max(deposition) as max, count(deposition) as count FROM "
          + " receptors_to_assessment_areas"
          + " JOIN permit_required_receptors_view USING (receptor_id)"
          + " JOIN calculation_summed_deposition_results_view USING (receptor_id) "
          + " WHERE assessment_area_id = ? AND calculation_id = ?";

  /**
   *
   * @param con
   * @param assessmentAreaId
   * @return
   * @throws SQLException
   */
  public int getNumberOfReceptors(final Connection con, final int assessmentAreaId) throws SQLException {
    try (PreparedStatement statement = con.prepareStatement(RECEPTOR_COUNT_QUERY)) {
      statement.setInt(1, assessmentAreaId);
      try (final ResultSet result = statement.executeQuery()) {
        return result.next() ? result.getInt(1) : 0;
      }
    }
  }

  /**
   *
   * @param con
   * @param assessmentAreaId
   * @param calculationId
   * @return
   * @throws SQLException
   */
  public AResult getCalculationResultsForAssessmentArea(final Connection con, final int assessmentAreaId,
      final int calculationId)
      throws SQLException {
    final AResult returnResult;
    try (PreparedStatement statement = con.prepareStatement(MAX_CALCULATION_RESULT_BY_ASSESSMENT_AREA)) {
      QueryUtil.setValues(statement, assessmentAreaId, calculationId);
      try (final ResultSet result = statement.executeQuery()) {
        if (result.next()) {
          returnResult = new AResult(result.getDouble("max"), result.getInt("count"));
        } else {
          returnResult = new AResult(0, 0);
        }
      }
    }
    return returnResult;
  }

  /**
   *
   * @param con
   * @param ru
   * @param assessmentAreaId
   * @param offset
   * @param limit
   * @return
   * @throws SQLException
   */
  public List<OPSReceptor> getAssessmentAreaReceptorsWithLimit(final Connection con, final ReceptorUtil ru, final int assessmentAreaId,
      final int offset, final int limit) throws SQLException {
    return ReceptorRepository.getAssessmentAreaReceptorsWithLimit(con, ru, assessmentAreaId, offset, limit);
  }

  /**
   *
   * @param con
   * @param point
   * @return
   * @throws SQLException
   */
  public Map<Integer, Integer> getDistanceSetForPoint(final Connection con, final Point point) throws SQLException {
    final Map<Integer, Integer> set = new HashMap<>();
    try (PreparedStatement statement = con.prepareStatement(ASSESMENT_AREA_DISTANCES)) {
      QueryUtil.setValues(statement, point.getX(), point.getY(), point.getX(), point.getY());
      try (final ResultSet result = statement.executeQuery()) {
        while (result.next()) {
          final int aaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(result);
          final int distance = QueryAttribute.DISTANCE.getInt(result);
          set.put(aaId, distance);
        }
      }
    }
    return set;
  }

  /**
   * Data object to store the highest deposition value and the number of receptors calculated (in a specific natura 2000 area).
   */
  public static class AResult implements Serializable {
    private static final long serialVersionUID = 3139175147458006539L;

    private final double max;
    private final int count;

    public AResult(final double max, final int count) {
      this.max = max;
      this.count = count;
    }

    public int getCount() {
      return count;
    }

    public double getMax() {
      return max;
    }
  }
}
