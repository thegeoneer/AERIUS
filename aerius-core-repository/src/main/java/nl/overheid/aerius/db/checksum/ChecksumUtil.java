/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.checksum;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import nl.overheid.aerius.db.PMF;

/**
 * Util class for working with the checksums on the database tables.
 */
public final class ChecksumUtil {

  private static final String RESOURCE_CHECKSUM_EXCLUDE_TABLES = "%s_exclude_tables.txt";
  private static final String STRUCTURE_LABEL = "structure";

  private static final String SEPARATOR = "=";

  private ChecksumUtil() {
    //not allowed to instantiate.
  }

  public static void writeChecksums(final PMF pmf, final String filename) throws IOException, SQLException {
    final Map<String, Long> sortedChecksums = new TreeMap<>(fetchChecksums(pmf));

    try (BufferedWriter writer = Files.newBufferedWriter(new File(filename).toPath(), StandardCharsets.UTF_8)) {
      for (final Entry<String, Long> checksumEntry : sortedChecksums.entrySet()) {
        writer.write(checksumEntry.getKey() + SEPARATOR + checksumEntry.getValue());
        writer.newLine();
      }
    }
  }

  public static boolean validateChecksums(final PMF pmf, final String filename) throws IOException, SQLException {
    final Map<String, Long> checksums = fetchChecksums(pmf);

    final Map<String, Long> storedChecksums = new HashMap<>();
    try (final BufferedReader reader = Files.newBufferedReader(new File(filename).toPath(), StandardCharsets.UTF_8)) {
      String line;
      while ((line = reader.readLine()) != null) {
        final String[] parts = line.trim().split(SEPARATOR);
        if (parts.length == 2) {
          storedChecksums.put(parts[0], Long.valueOf(parts[1]));
        }
      }
    }

    return checksums.equals(storedChecksums);
  }

  private static Map<String, Long> fetchChecksums(final PMF pmf) throws IOException, SQLException {
    // Read exclude tables from resource
    final ArrayList<String> excludeTables = new ArrayList<>();
    final String filename = String.format(RESOURCE_CHECKSUM_EXCLUDE_TABLES, pmf.getProductType().name().toLowerCase());
    try (final InputStream stream = ChecksumUtil.class.getResourceAsStream(filename);
        final BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
      String line;
      while ((line = reader.readLine()) != null) {
        line = line.trim();
        if (!line.isEmpty()) {
          excludeTables.add(line);
        }
      }
    }

    // Get table data checksums
    final Map<String, Long> checksums = ChecksumRepository.getTablesContentChecksum(pmf.getConnection(), excludeTables);

    // Append database structure checksums
    final Long structureChecksum = ChecksumRepository.getDatabaseStructureChecksum(pmf.getConnection());
    checksums.put(STRUCTURE_LABEL, structureChecksum);

    return checksums;
  }

}
