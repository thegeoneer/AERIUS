/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.postgis.PGgeometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.AuthorityRepository;
import nl.overheid.aerius.db.common.EmissionResultRepositoryUtil;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.ArrayWhereClause;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.EnumWhereClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.JoinClause.JoinType;
import nl.overheid.aerius.db.util.OrderByClause;
import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.db.util.PGisUtils;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.db.util.WhereClause;
import nl.overheid.aerius.geo.shared.Geometry;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestSituation;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Functionality for fetching generic request information from the database.
 */
public final class RequestRepository {

  enum RepositoryAttribute implements Attribute {

    FILE_FORMAT_TYPE,
    FILE_NAME,

    TOTAL_SIZE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  //The logger.
  private static final Logger LOG = LoggerFactory.getLogger(RequestRepository.class);

  protected static final int MAX_REQUESTS_RETRIEVED = 100;

  public static final String REQUESTS = "requests";

  private static final OrderByClause[] ORDER_BY_INSERT_DATE = new OrderByClause[] {
      new OrderByClause(RegisterAttribute.INSERT_DATE, OrderType.ASC), new OrderByClause(RegisterAttribute.REQUEST_ID, OrderType.ASC),
  };

  private static final WhereClause WHERE_FILE_TYPE =
      new StaticWhereClause(RegisterAttribute.FILE_TYPE.attribute() + " = ?::request_file_type", RegisterAttribute.FILE_TYPE);

  private static final Query GET_OLDEST_REQUEST_WITH_STATUS_WITHOUT_DECREE = QueryBuilder.from("requests")
      .join(new JoinClause("decree_requests_without_decree_file_view", RegisterAttribute.REQUEST_ID))
      .where(new StaticWhereClause(RegisterAttribute.FILE_TYPE.attribute() + " = ?::request_file_type", RegisterAttribute.FILE_TYPE),
          new StaticWhereClause(RegisterAttribute.SEGMENT.attribute() + " = ?::segment_type", RegisterAttribute.SEGMENT))
      .orderBy(ORDER_BY_INSERT_DATE)
      .limit(1)
      .getQuery();

  private static final Query GET_AUTHORITY_FOR_REQUEST = QueryBuilder.from(REQUESTS)
      .select(QueryAttribute.AUTHORITY_ID)
      .where(RegisterAttribute.REFERENCE).getQuery();

  private static final JoinClause JOIN_REQUESTS = new JoinClause(REQUESTS, RegisterAttribute.REQUEST_ID);

  private static final Query GET_REQUEST_FILE_CONTENT = QueryBuilder.from("request_files")
      .join(JOIN_REQUESTS)
      .select(RegisterAttribute.CONTENT)
      .where(RegisterAttribute.REFERENCE)
      .where(WHERE_FILE_TYPE).getQuery();

  private static final Query GET_EXISTING_REQUEST_FILES = getRequestFileQuery().getQuery();

  private static final Query GET_SPECIFIC_REQUEST_FILE = getRequestFileQuery()
      .where(WHERE_FILE_TYPE).getQuery();

  private static final Query GET_REQUESTS_FILES_STATS = QueryBuilder.from("request_files")
      .select(new SelectClause("SUM(octet_length(content))", RepositoryAttribute.TOTAL_SIZE.attribute()))
      .where(new ArrayWhereClause(RegisterAttribute.REQUEST_ID)).getQuery();

  private static final Query GET_REQUEST_CALCULATIONS = QueryBuilder.from("request_situation_calculations")
      .join(JOIN_REQUESTS)
      .select(RegisterAttribute.SITUATION, QueryAttribute.CALCULATION_ID)
      .where(RegisterAttribute.REFERENCE).getQuery();

  private static final Query GET_SITUATION_PROPERTIES = QueryBuilder.from("request_situation_properties")
      .select(RegisterAttribute.SITUATION, QueryAttribute.NAME)
      .where(RegisterAttribute.REQUEST_ID).getQuery();

  private static final Query GET_SITUATION_TOTAL_EMISSIONS = QueryBuilder.from("request_situation_emissions")
      .select(QueryAttribute.SUBSTANCE_ID, RegisterAttribute.TOTAL_EMISSION)
      .where(RegisterAttribute.REQUEST_ID)
      .where(new StaticWhereClause(RegisterAttribute.SITUATION.attribute() + " = ?::situation_type", RegisterAttribute.SITUATION)).getQuery();

  private static final Query GET_RESULTS_FOR_EXPORT = QueryBuilder.from("request_demands_gml_export_view")
      .select(QueryAttribute.RECEPTOR_ID).select(EmissionResultRepositoryUtil.REQUIRED_ATTRIBUTES)
      .where(RegisterAttribute.REQUEST_ID).getQuery();

  private static final Query GET_OLDEST_REQUEST_WITHOUT_CALCULATIONS = QueryBuilder.from(REQUESTS)
      .join(new JoinClause("request_situation_calculations", RegisterAttribute.REQUEST_ID, JoinType.LEFT))
      .select(RegisterAttribute.REQUEST_ID)
      .where(new StaticWhereClause("calculation_id is NULL"))
      .where(new EnumWhereClause<>(RegisterAttribute.STATUS, RequestState.INITIAL))
      .where(new StaticWhereClause(RegisterAttribute.SEGMENT.attribute() + " = ?::segment_type", RegisterAttribute.SEGMENT))
      .orderBy(ORDER_BY_INSERT_DATE)
      .limit(1)
      .getQuery();

  private static final Query GET_REQUEST = getRequestQuery().where(RegisterAttribute.REQUEST_ID).getQuery();

  private RequestRepository() {
    //Not allowed to instantiate.
  }

  /**
   * Returns the oldest request in the database that has no decree file yet and has state 'assigned' or 'assigned_final'.
   * @param con The connection to use.
   * @return null if all permits that should have a decree file actually have one.
   * @throws SQLException In case of a database error.
   */
  static Integer getOldestRequestIdWithoutDecrees(final Connection con, final RequestFileType fileType, final SegmentType segment)
      throws SQLException {
    final Integer id;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_OLDEST_REQUEST_WITH_STATUS_WITHOUT_DECREE.get())) {
      GET_OLDEST_REQUEST_WITH_STATUS_WITHOUT_DECREE.setParameter(selectPS, RegisterAttribute.FILE_TYPE, fileType.name().toLowerCase());
      GET_OLDEST_REQUEST_WITH_STATUS_WITHOUT_DECREE.setParameter(selectPS, RegisterAttribute.SEGMENT, segment.getDbValue());
      final ResultSet rs = selectPS.executeQuery();
      if (rs.next()) {
        id = rs.getInt(RegisterAttribute.REQUEST_ID.attribute());
      } else {
        id = null;
      }
    }
    return id;
  }

  /**
   * @param con The connection to use.
   * @param reference The reference of the request to get the authority for.
   * @return The authority (or null if not found).
   * @throws SQLException In case of database exceptions.
   */
  public static Authority getAuthorityForRequestWithReference(final Connection con, final String reference) throws SQLException {
    Authority authority = null;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_AUTHORITY_FOR_REQUEST.get())) {
      GET_AUTHORITY_FOR_REQUEST.setParameter(selectPS, RegisterAttribute.REFERENCE, reference);

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          authority = AuthorityRepository.getSkinnedAuthority(con, QueryAttribute.AUTHORITY_ID.getInt(rs));
        }
      }
    }
    return authority;
  }

  /**
   * @param con The connection to use.
   * @param reference The reference of the request to retrieve the file for.
   * @param fileType The type of the file to retrieve.
   * @return The proper file content (or null if not found).
   * @throws SQLException In case of a database error.
   */
  public static byte[] getRequestFileContent(final Connection con, final String reference, final RequestFileType fileType) throws SQLException {
    byte[] content = null;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_REQUEST_FILE_CONTENT.get())) {
      GET_REQUEST_FILE_CONTENT.setParameter(selectPS, RegisterAttribute.REFERENCE, reference);
      GET_REQUEST_FILE_CONTENT.setParameter(selectPS, RegisterAttribute.FILE_TYPE, fileType.name().toLowerCase());

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          content = rs.getBytes(RegisterAttribute.CONTENT.attribute());
        }
      }
    }
    return content;
  }

  public static long getRequestsFileSize(final Connection con, final Collection<Integer> requests) throws SQLException {
    long filesize = -1;

    try (final PreparedStatement selectPS = con.prepareStatement(GET_REQUESTS_FILES_STATS.get())) {
      GET_REQUESTS_FILES_STATS.setParameter(selectPS, RegisterAttribute.REQUEST_ID, QueryUtil.toNumericSQLArray(con, requests));

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          filesize = QueryUtil.getLong(rs, RepositoryAttribute.TOTAL_SIZE);
        }
      }
    }

    return filesize;
  }

  /**
   * @param con The connection to use.
   * @param reference The reference of the request to retrieve the RequestFiles for.
   * @return All the RequestFiles for the request.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<RequestFile> getExistingRequestFiles(final Connection con, final String reference) throws SQLException {
    final ArrayList<RequestFile> requestFiles = new ArrayList<>();
    try (final PreparedStatement selectPS = con.prepareStatement(GET_EXISTING_REQUEST_FILES.get())) {
      GET_EXISTING_REQUEST_FILES.setParameter(selectPS, RegisterAttribute.REFERENCE, reference);

      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          final RequestFile requestFile = getRequestFile(rs);
          requestFiles.add(requestFile);
        }
      }
    }
    return requestFiles;
  }

  /**
   * @param con The connection to use.
   * @param reference The reference of the request to retrieve the file for.
   * @param fileType The type of the file to retrieve.
   * @return The proper RequestFile(or null if not found).
   * @throws SQLException In case of a database error.
   */
  public static RequestFile getSpecificRequestFile(final Connection con, final String reference, final RequestFileType requestFileType)
      throws SQLException {
    final RequestFile requestFile;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_SPECIFIC_REQUEST_FILE.get())) {
      GET_SPECIFIC_REQUEST_FILE.setParameter(selectPS, RegisterAttribute.REFERENCE, reference);
      GET_SPECIFIC_REQUEST_FILE.setParameter(selectPS, RegisterAttribute.FILE_TYPE, requestFileType.name().toLowerCase());

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          requestFile = getRequestFile(rs);
        } else {
          requestFile = null;
        }
      }
    }
    return requestFile;
  }

  private static RequestFile getRequestFile(final ResultSet rs) throws SQLException {
    final RequestFile requestFile = new RequestFile();
    requestFile.setRequestFileType(QueryUtil.getEnum(rs, RegisterAttribute.FILE_TYPE, RequestFileType.class));
    requestFile.setFileFormat(QueryUtil.getEnum(rs, RepositoryAttribute.FILE_FORMAT_TYPE, FileFormat.class));
    requestFile.setFileName(QueryUtil.getString(rs, RepositoryAttribute.FILE_NAME));
    return requestFile;
  }

  /**
   * @param con The connection to use.
   * @param reference The reference of the request to retrieve the calculations for.
   * @return Map containing calculation IDs per situation type.
   * @throws SQLException In case of a database error.
   */
  public static HashMap<SituationType, Integer> getCalculationIds(final Connection con, final String reference) throws SQLException {
    final HashMap<SituationType, Integer> calculationMap = new HashMap<>();
    try (final PreparedStatement selectPS = con.prepareStatement(GET_REQUEST_CALCULATIONS.get())) {
      GET_REQUEST_CALCULATIONS.setParameter(selectPS, RegisterAttribute.REFERENCE, reference);

      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          calculationMap.put(QueryUtil.getEnum(rs, RegisterAttribute.SITUATION, SituationType.class),
              QueryAttribute.CALCULATION_ID.getInt(rs));
        }
      }
    }
    return calculationMap;
  }

  /**
   * Fetch the situations for a request. Gets the name and also the total emissions.
   * @param con The connection to use.
   * @param requestId The id of the request to retrieve the situations for.
   * @return Map containing situation objects per situation type.
   * @throws SQLException In case of a database error.
   */
  public static HashMap<SituationType, RequestSituation> getSituations(final Connection con, final int requestId) throws SQLException {
    final HashMap<SituationType, RequestSituation> returnMap = new HashMap<>();
    try (final PreparedStatement selectPS = con.prepareStatement(GET_SITUATION_PROPERTIES.get())) {
      GET_SITUATION_PROPERTIES.setParameter(selectPS, RegisterAttribute.REQUEST_ID, requestId);
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          final SituationType situation = QueryUtil.getEnum(rs, RegisterAttribute.SITUATION, SituationType.class);
          final RequestSituation permitSituation = new RequestSituation();
          permitSituation.setName(QueryAttribute.NAME.getString(rs));
          permitSituation.setTotalEmissionValues(getTotalEmissions(con, requestId, situation));
          returnMap.put(situation, permitSituation);
        }
      }
    }
    return returnMap;
  }

  /**
   * Fetch the total emissions of a situation type for a request.
   * @param con The connection to use.
   * @param requestId The id of the request to retrieve the emission values for.
   * @param situationType Situation type to retrieve the emission values for.
   * @return Emission values.
   * @throws SQLException In case of a database error.
   */
  private static EmissionValues getTotalEmissions(final Connection con, final int requestId, final SituationType situationType) throws SQLException {
    final EmissionValues emissionValues = new EmissionValues(false);
    try (final PreparedStatement selectPS = con.prepareStatement(GET_SITUATION_TOTAL_EMISSIONS.get())) {
      GET_SITUATION_TOTAL_EMISSIONS.setParameter(selectPS, RegisterAttribute.REQUEST_ID, requestId);
      GET_SITUATION_TOTAL_EMISSIONS.setParameter(selectPS, RegisterAttribute.SITUATION, situationType.getDBName());
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          final EmissionValueKey key = new EmissionValueKey(Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs)));
          emissionValues.setEmission(key, QueryUtil.getDouble(rs, RegisterAttribute.TOTAL_EMISSION));
        }
      }
    }
    return emissionValues;
  }

  /**
   * @param con The connection to use.
   * @param requestId The ID of the request to get deposition resuls for.
   * @return A list of aerius result points, containing deposition results per substance.
   * @throws SQLException In case of a database error.
   */
  public static List<AeriusResultPoint> getResultsForExport(final Connection con, final int requestId) throws SQLException {
    return getResultsForExport(con, requestId, GET_RESULTS_FOR_EXPORT);
  }

  /**
   * @param con The connection to use.
   * @param requestId The ID of the request, will be set based on the query.
   * @param query The query to get results for export. Should contain all required attributes and a where clause on request_id.
   * @return The proper results.
   * @throws SQLException In case of database errors.
   */
  static List<AeriusResultPoint> getResultsForExport(final Connection con, final int requestId, final Query query) throws SQLException {
    final ArrayList<AeriusResultPoint> receptors = new ArrayList<>();
    final ReceptorUtil ru = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(con));
    try (final PreparedStatement ps = con.prepareStatement(query.get())) {
      query.setParameter(ps, RegisterAttribute.REQUEST_ID, requestId);

      final HashMap<Integer, AeriusResultPoint> receptorMap = new HashMap<>();
      try (final ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          addResultsToReceptorMap(rs, ru, receptorMap);
        }
      }
      receptors.addAll(receptorMap.values());
    }

    return receptors;
  }

  /**
   * Returns the oldest permit in the database that has no calculation results yet and has state 'initial'.
   * @param con The connection to use.
   * @return null if all permits have calculation results or the Permit with no results
   * @throws SQLException In case of a database error.
   */
  public static Request getOldestRequestWithoutCalculations(final Connection con, final SegmentType segment) throws SQLException {
    final Integer id;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_OLDEST_REQUEST_WITHOUT_CALCULATIONS.get())) {
      GET_OLDEST_REQUEST_WITHOUT_CALCULATIONS.setParameter(selectPS, RegisterAttribute.SEGMENT, segment.getDbValue());
      final ResultSet rs = selectPS.executeQuery();
      if (rs.next()) {
        id = rs.getInt(RegisterAttribute.REQUEST_ID.attribute());
      } else {
        id = null;
      }
    }
    return id == null ? null : getRequest(con, id, segment);
  }

  private static Request getRequest(final Connection con, final int requestId, final SegmentType segment) throws SQLException {
    Request request = null;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_REQUEST.get())) {
      GET_REQUEST.setParameter(selectPS, RegisterAttribute.REQUEST_ID, requestId);

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          request = new Request(segment);
          request.setId(QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID));
          fillRequestFromResultSet(con, rs, request);
        }
      }
    }
    return request;
  }

  private static void addResultsToReceptorMap(final ResultSet rs, final ReceptorUtil ru, final HashMap<Integer, AeriusResultPoint> receptorMap)
      throws SQLException {
    final AeriusResultPoint point = new AeriusResultPoint(QueryAttribute.RECEPTOR_ID.getInt(rs));
    ru.setAeriusPointFromId(point);

    EmissionResultRepositoryUtil.addResultsToPoint(rs, receptorMap, point);
  }

  static void addRequiredAttributes(final QueryBuilder queryBuilder) {
    queryBuilder.select(RegisterAttribute.REFERENCE, RegisterAttribute.CORPORATION, RegisterAttribute.PROJECT_NAME, QueryAttribute.DESCRIPTION,
        QueryAttribute.SECTOR_ID, RegisterAttribute.START_YEAR, RegisterAttribute.TEMPORARY_PERIOD, RegisterAttribute.HAS_MULTIPLE_SECTORS,
        QueryAttribute.AUTHORITY_ID, RegisterAttribute.APPLICATION_VERSION, RegisterAttribute.DATABASE_VERSION, RegisterAttribute.LAST_MODIFIED,
        QueryAttribute.GEOMETRY, RegisterAttribute.MARKED);
  }

  static void fillRequestFromResultSet(final Connection con, final ResultSet rs, final Request request) throws SQLException {
    final ScenarioMetaData metaData = new ScenarioMetaData();
    metaData.setReference(QueryUtil.getString(rs, RegisterAttribute.REFERENCE));
    metaData.setCorporation(QueryUtil.getString(rs, RegisterAttribute.CORPORATION));
    metaData.setProjectName(QueryUtil.getString(rs, RegisterAttribute.PROJECT_NAME));
    metaData.setDescription(QueryAttribute.DESCRIPTION.getString(rs));
    request.setScenarioMetaData(metaData);

    request.setStartYear(QueryUtil.getInt(rs, RegisterAttribute.START_YEAR));
    request.setTemporaryPeriod((Integer) QueryUtil.getObject(rs, RegisterAttribute.TEMPORARY_PERIOD));
    request.setLastModified(rs.getTimestamp(RegisterAttribute.LAST_MODIFIED.attribute()).getTime());
    request.setMarked(QueryUtil.getBoolean(rs, RegisterAttribute.MARKED));

    request.setAuthority(AuthorityRepository.getSkinnedAuthority(con, QueryAttribute.AUTHORITY_ID.getInt(rs)));

    request.setApplicationVersion(QueryUtil.getString(rs, RegisterAttribute.APPLICATION_VERSION));
    request.setDatabaseVersion(QueryUtil.getString(rs, RegisterAttribute.DATABASE_VERSION));

    setRequestPoint(rs, request);

    setRequestSector(con, rs, request);
  }

  static void fillDossierMetaDataFromResultSet(final Connection con, final ResultSet rs, final DossierMetaData dossierMetaData)
      throws SQLException {
    dossierMetaData.setDossierId(QueryUtil.getString(rs, RegisterAttribute.DOSSIER_ID));
    dossierMetaData.setReceivedDate(rs.getTimestamp(RegisterAttribute.RECEIVED_DATE.attribute()));
    dossierMetaData.setRemarks(QueryUtil.getString(rs, RegisterAttribute.REMARKS));
  }

  private static void setRequestPoint(final ResultSet rs, final Request request) throws SQLException {
    final Geometry geometry = PGisUtils.getGeometry((PGgeometry) QueryAttribute.GEOMETRY.getObject(rs));
    if (geometry instanceof Point) {
      request.setPoint((Point) geometry);
    } else {
      LOG.error("Got geometry {} which was not a point for request {}", geometry, request.getId());
    }
  }

  private static void setRequestSector(final Connection con, final ResultSet rs, final Request request) throws SQLException {
    final int sectorId = QueryAttribute.SECTOR_ID.getInt(rs);
    Sector foundSector = Sector.SECTOR_UNDEFINED;
    for (final Sector sector : SectorRepository.getSectors(con, new DBMessagesKey(ProductType.REGISTER, LocaleUtils.getDefaultLocale()))) {
      if (sector.getSectorId() == sectorId) {
        foundSector = sector;
      }
    }
    if (foundSector == Sector.SECTOR_UNDEFINED) {
      LOG.error("Could not find sector with ID {} for notice {}", sectorId, request.getId());
    }
    request.setSector(foundSector);
    request.setMultipleSectors(QueryUtil.getBoolean(rs, RegisterAttribute.HAS_MULTIPLE_SECTORS));
  }

  private static QueryBuilder getRequestQuery() {
    final QueryBuilder builder = QueryBuilder.from(REQUESTS).select(RegisterAttribute.REQUEST_ID);
    addRequiredAttributes(builder);
    return builder;
  }

  private static QueryBuilder getRequestFileQuery() {
    return QueryBuilder.from("request_files")
        .join(JOIN_REQUESTS)
        .select(RegisterAttribute.REQUEST_ID, RegisterAttribute.FILE_TYPE, RepositoryAttribute.FILE_FORMAT_TYPE, RepositoryAttribute.FILE_NAME)
        .where(RegisterAttribute.REFERENCE);
  }

}
