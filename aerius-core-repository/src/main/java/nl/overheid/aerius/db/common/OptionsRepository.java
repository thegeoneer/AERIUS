/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.Option;
import nl.overheid.aerius.shared.domain.OptionType;

/**
 *
 */
public final class OptionsRepository {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(OptionsRepository.class);

  private enum RepositoryAttribute implements Attribute {
    OPTION_TYPE,
    VALUE,
    LABEL,
    DEFAULT_VALUE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final Query GET_OPTIONS = QueryBuilder.from("system.options")
      .select(RepositoryAttribute.VALUE, RepositoryAttribute.LABEL, RepositoryAttribute.DEFAULT_VALUE)
      .where(new StaticWhereClause(RepositoryAttribute.OPTION_TYPE.attribute() + " = ?::system.option_type", RepositoryAttribute.OPTION_TYPE))
      .orderBy(RepositoryAttribute.LABEL).getQuery();

  private OptionsRepository() {
  }

  /**
   * @param con The connection to use.
   * @param optionType The options type to get the list of options for.
   * @param t The class of the option values. The OptionType enum should have a class declared that should work for the values of that option type.
   * @param <T> The type of the values for the options.
   * @return The list of options for the specified options type.
   * @throws SQLException In case of database errors.
   */
  public static <T extends Object> ArrayList<Option<T>> getOptions(final Connection con, final OptionType optionType, final Class<T> t)
      throws SQLException {
    final ArrayList<Option<T>> returnList = new ArrayList<Option<T>>();
    try (final PreparedStatement stmt = con.prepareStatement(GET_OPTIONS.get())) {
      GET_OPTIONS.setParameter(stmt, RepositoryAttribute.OPTION_TYPE, optionType.name().toLowerCase());

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        final Option<T> option = new Option<>();
        final String value = QueryUtil.getString(rs, RepositoryAttribute.VALUE);
        try {
          option.setValue(t.getConstructor(new Class[] {String.class}).newInstance(value));
        } catch (final ReflectiveOperationException e) {
          LOG.error("[getObject] Could not invoke String constructor for Object type [" + t.getName() + "]");

          // Stacktrace will be logged on a higher level
          throw new SQLException(e);
        }
        option.setLabel(QueryUtil.getString(rs, RepositoryAttribute.LABEL));
        option.setDefaultValue(QueryUtil.getBoolean(rs, RepositoryAttribute.DEFAULT_VALUE));
        returnList.add(option);
      }
    }
    return returnList;
  }

}
