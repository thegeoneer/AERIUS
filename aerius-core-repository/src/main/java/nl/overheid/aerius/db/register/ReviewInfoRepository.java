/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.OrderByClause;
import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.developmentspace.BaseReviewInfo;
import nl.overheid.aerius.shared.domain.developmentspace.PermitReviewInfo;
import nl.overheid.aerius.shared.domain.developmentspace.PrioritySubProjectReviewInfo;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;

/**
 * Functionality for fetching review info from the database.
 */
public final class ReviewInfoRepository {

  private enum RepositoryAttribute implements Attribute {

    TOTAL_DEVELOPMENT_SPACE_DEMAND,
    AVAILABLE_EXCLUSIVE,
    SHORTAGE_INCLUSIVE,
    SHORTAGE_COUNT_INCLUSIVE,
    ONLY_EXCEEDING;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final Query GET_PERMIT_REVIEW_INFO = QueryBuilder
      .from("permit_assessment_area_review_info_view")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.ASSESSMENT_AREA_NAME,
          RepositoryAttribute.ONLY_EXCEEDING, RepositoryAttribute.TOTAL_DEVELOPMENT_SPACE_DEMAND,
          RepositoryAttribute.AVAILABLE_EXCLUSIVE, RepositoryAttribute.SHORTAGE_INCLUSIVE,
          RepositoryAttribute.SHORTAGE_COUNT_INCLUSIVE)
      .where(RegisterAttribute.REQUEST_ID)
      .orderBy(
          new OrderByClause(RepositoryAttribute.TOTAL_DEVELOPMENT_SPACE_DEMAND, OrderType.DESC),
          new OrderByClause(QueryAttribute.ASSESSMENT_AREA_NAME))
      .getQuery();

  private static final Query GET_PRIORITY_SUBPROJECT_REVIEW_INFO = QueryBuilder
      .from("priority_subproject_assessment_area_review_info_view")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.ASSESSMENT_AREA_NAME,
          RepositoryAttribute.SHORTAGE_INCLUSIVE, RepositoryAttribute.SHORTAGE_COUNT_INCLUSIVE)
      .where(RegisterAttribute.REQUEST_ID)
      .orderBy(
          new OrderByClause(RepositoryAttribute.SHORTAGE_INCLUSIVE, OrderType.DESC),
          new OrderByClause(QueryAttribute.ASSESSMENT_AREA_NAME))
      .getQuery();

  private ReviewInfoRepository() {
    // Not allowed to instantiate.
  }

  public static ArrayList<PermitReviewInfo> getPermitReviewInfo(final Connection connection,
      final DossierAuthorityKey key) throws SQLException {
    final int permitId = PermitRepository.getPermitIdByKey(connection, key);
    final ArrayList<PermitReviewInfo> result = new ArrayList<>();
    final PermitReviewInfoCollector collector = new PermitReviewInfoCollector(result);

    try (final PreparedStatement selectPS = connection.prepareStatement(GET_PERMIT_REVIEW_INFO.get())) {
      GET_PERMIT_REVIEW_INFO.setParameter(selectPS, RegisterAttribute.REQUEST_ID, permitId);
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          collector.collectEntity(rs);
        }
      }
    }
    return result;
  }

  public static ArrayList<PrioritySubProjectReviewInfo> getPrioritySubProjectReviewInfo(final Connection connection,
      final PrioritySubProjectKey key) throws SQLException {
    final int subProjectId = PrioritySubProjectRepository.getPrioritySubProjectIdByKey(connection, key);
    final ArrayList<PrioritySubProjectReviewInfo> result = new ArrayList<>();
    final PrioritySubProjectReviewInfoCollector collector = new PrioritySubProjectReviewInfoCollector(result);

    try (final PreparedStatement selectPS = connection.prepareStatement(GET_PRIORITY_SUBPROJECT_REVIEW_INFO.get())) {
      GET_PRIORITY_SUBPROJECT_REVIEW_INFO.setParameter(selectPS, RegisterAttribute.REQUEST_ID, subProjectId);
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          collector.collectEntity(rs);
        }
      }
    }
    return result;
  }

  private abstract static class BaseReviewInfoCollector extends EntityCollector<AssessmentArea> {
    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
    }

    protected AssessmentArea fillBaseReviewInfo(final ResultSet rs, final BaseReviewInfo info) throws SQLException {
      final AssessmentArea assessmentArea = new AssessmentArea();
      assessmentArea.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
      assessmentArea.setName(QueryAttribute.ASSESSMENT_AREA_NAME.getString(rs));
      info.setAssessmentArea(assessmentArea);
      info.setShortageInclusive(QueryUtil.getDouble(rs, RepositoryAttribute.SHORTAGE_INCLUSIVE));
      info.setNumReceptorsShortageInclusive(QueryUtil.getInt(rs, RepositoryAttribute.SHORTAGE_COUNT_INCLUSIVE));
      return assessmentArea;
    }
  }

  private static class PermitReviewInfoCollector extends BaseReviewInfoCollector {
    private final ArrayList<PermitReviewInfo> result;

    PermitReviewInfoCollector(final ArrayList<PermitReviewInfo> result) {
      this.result = result;
    }

    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs) * (QueryUtil.getBoolean(rs, RepositoryAttribute.ONLY_EXCEEDING) ? 1 : -1);
    }

    @Override
    public AssessmentArea fillEntity(final ResultSet rs) throws SQLException {
      final PermitReviewInfo info = new PermitReviewInfo();
      final AssessmentArea assessmentArea = fillBaseReviewInfo(rs, info);
      info.setAvailableExclusive(QueryUtil.getDouble(rs, RepositoryAttribute.AVAILABLE_EXCLUSIVE));
      info.setRequiredSpaceForPermit(QueryUtil.getDouble(rs, RepositoryAttribute.TOTAL_DEVELOPMENT_SPACE_DEMAND));
      info.setOnlyExceeding(QueryUtil.getBoolean(rs, RepositoryAttribute.ONLY_EXCEEDING));

      result.add(info);
      return assessmentArea;
    }
  }

  private static class PrioritySubProjectReviewInfoCollector extends BaseReviewInfoCollector {
    private final ArrayList<PrioritySubProjectReviewInfo> result;

    PrioritySubProjectReviewInfoCollector(final ArrayList<PrioritySubProjectReviewInfo> result) {
      this.result = result;
    }

    @Override
    public AssessmentArea fillEntity(final ResultSet rs) throws SQLException {
      final PrioritySubProjectReviewInfo info = new PrioritySubProjectReviewInfo();
      final AssessmentArea assessmentArea = fillBaseReviewInfo(rs, info);
      result.add(info);
      return assessmentArea;
    }
  }
}
