/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;

/**
 * Service class for OnRoadMobileSourceCategory and OffRoadMobileSourceCategory.
 */
public final class MobileSourceCategoryRepository {

  private enum RepositoryAttribute implements Attribute {

    MOBILE_SOURCE_OFF_ROAD_CATEGORY_ID,

    MOBILE_SOURCE_ON_ROAD_CATEGORY_ID,
    ROAD_TYPE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final Query OFF_ROAD_MOBILE_SOURCE_CATEGORIES_QUERY = QueryBuilder.from("mobile_source_off_road_categories")
      .join(new JoinClause("mobile_source_off_road_category_emission_factors", RepositoryAttribute.MOBILE_SOURCE_OFF_ROAD_CATEGORY_ID))
      .select(RepositoryAttribute.MOBILE_SOURCE_OFF_ROAD_CATEGORY_ID, QueryAttribute.SUBSTANCE_ID, QueryAttribute.EMISSION_FACTOR)
      .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES).getQuery();

  private static final Query ON_ROAD_MOBILE_SOURCE_CATEGORIES_QUERY = QueryBuilder
      .from("mobile_source_on_road_categories")
      .join(new JoinClause("mobile_source_on_road_category_emission_factors", RepositoryAttribute.MOBILE_SOURCE_ON_ROAD_CATEGORY_ID))
      .select(RepositoryAttribute.MOBILE_SOURCE_ON_ROAD_CATEGORY_ID, QueryAttribute.SUBSTANCE_ID, RepositoryAttribute.ROAD_TYPE,
          QueryAttribute.EMISSION_FACTOR)
      .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES).getQuery();

  private MobileSourceCategoryRepository() {
  }

  /**
   * Returns all Off Road Mobile Source Categories from the database.
   *
   * @param con Connection to use
   * @param messagesKey DBMessagesKey to use for i18n stuff
   * @return ArrayList with all OffRoadMobileSourceCategories
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static ArrayList<OffRoadMobileSourceCategory> findOffRoadMobileSourceCategories(final Connection con,
      final DBMessagesKey messagesKey) throws SQLException {
    final AbstractCategoryCollector<OffRoadMobileSourceCategory> collector =
        new AbstractCategoryCollector<OffRoadMobileSourceCategory>(RepositoryAttribute.MOBILE_SOURCE_OFF_ROAD_CATEGORY_ID, messagesKey) {

          @Override
          OffRoadMobileSourceCategory getNewCategory() {
            return new OffRoadMobileSourceCategory();
          }

          @Override
          void setDescription(final OffRoadMobileSourceCategory category, final ResultSet rs) {
            DBMessages.setMobileSourceOffRoadCategoryMessages(category, messagesKey);
          }

          @Override
          public void appendEntity(final OffRoadMobileSourceCategory entity, final ResultSet rs) throws SQLException {
            final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
            final double emissionFactor = QueryAttribute.EMISSION_FACTOR.getDouble(rs);
            entity.setEmissionFactor(substance, emissionFactor);
          }

        };
    try (final PreparedStatement statement = con.prepareStatement(OFF_ROAD_MOBILE_SOURCE_CATEGORIES_QUERY.get())) {
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        collector.collectEntity(rs);
      }
    }
    return collector.getEntities();
  }

  /**
   * Returns all On Road Mobile Source Categories from the database.
   *
   * @param con Connection to use
   * @param messagesKey DBMessagesKey to use for i18n stuff
   * @return ArrayList with all OnRoadMobileSourceCategories
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static ArrayList<OnRoadMobileSourceCategory> findOnRoadMobileSourceCategories(final Connection con,
      final DBMessagesKey messagesKey) throws SQLException {
    final AbstractCategoryCollector<OnRoadMobileSourceCategory> collector =
        new AbstractCategoryCollector<OnRoadMobileSourceCategory>(RepositoryAttribute.MOBILE_SOURCE_ON_ROAD_CATEGORY_ID, messagesKey) {

      @Override
      OnRoadMobileSourceCategory getNewCategory() {
        return new OnRoadMobileSourceCategory();
      }

      @Override
      void setDescription(final OnRoadMobileSourceCategory category, final ResultSet rs) {
        DBMessages.setMobileSourceOnRoadCategoryMessages(category, messagesKey);
      }

      @Override
      public void appendEntity(final OnRoadMobileSourceCategory entity, final ResultSet rs) throws SQLException {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final RoadType roadType = QueryUtil.getEnum(rs, RepositoryAttribute.ROAD_TYPE, RoadType.class);
        final double emissionFactor = QueryAttribute.EMISSION_FACTOR.getDouble(rs);
        entity.setEmissionFactor(substance, roadType, emissionFactor);
      }

    };

    try (final PreparedStatement statement = con.prepareStatement(ON_ROAD_MOBILE_SOURCE_CATEGORIES_QUERY.get())) {
      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        collector.collectEntity(rs);
      }
    }
    return collector.getEntities();
  }
}
