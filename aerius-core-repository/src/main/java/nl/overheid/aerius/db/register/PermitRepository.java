/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.ILikeWhereClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.OrderByClause;
import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SearchWhereClause;
import nl.overheid.aerius.db.util.SpecialAttribute;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;

/**
 * Functionality for fetching permit information from the database.
 */
public final class PermitRepository {

  private enum RepositoryAttribute implements Attribute {

    POTENTIALLY_REJECTABLE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final int UNKNOWN_PERMIT_ID = -1;

  private static final Query GET_PERMIT_KEY_BY_REFERENCE = getPermitBase()
      .select(RegisterAttribute.DOSSIER_ID, QueryAttribute.CODE)
      .join(new JoinClause("authorities", QueryAttribute.AUTHORITY_ID))
      .where(QueryAttribute.REFERENCE).getQuery();

  private static final Query GET_PERMIT_ID_BY_KEY = getPermitBase()
      .join(new JoinClause("authorities", QueryAttribute.AUTHORITY_ID))
      .where(RegisterAttribute.DOSSIER_ID, QueryAttribute.CODE).getQuery();

  private static final Query GET_PERMIT = getPermitFullInfo()
      .where(RegisterAttribute.REQUEST_ID).getQuery();

  private static final Query SEARCH_PERMITS = getPermitFullInfo()
      .where(new SearchWhereClause(
          RegisterAttribute.REFERENCE, RegisterAttribute.DOSSIER_ID, RegisterAttribute.PROJECT_NAME, RegisterAttribute.CORPORATION))
      .getQuery();

  private static final Query GET_PERMITS_WITHIN_RADIUS = getPermitFullInfo()
      .join(new JoinClause("ae_requests_within_distance(?, ?)", RegisterAttribute.REQUEST_ID, RegisterAttribute.REQUEST_ID,
          QueryAttribute.MAX_DISTANCE))
      .orderBy(QueryAttribute.DISTANCE).limit().getQuery();

  private static final Query IS_PERMIT_FINISHED = QueryBuilder.from("request_calculation_status_view")
      .select(RegisterAttribute.FINISHED)
      .where(RegisterAttribute.REQUEST_ID).getQuery();

  // Not allowed to instantiate.
  private PermitRepository() {
  }

  /**
   * Returns the oldest permit in the database that has no decree file yet and has state 'assigned' or 'assigned_final'.
   * @param con The connection to use.
   * @return null if all permits that should have a decree file actually have one.
   * @throws SQLException In case of a database error.
   */
  public static Permit getOldestWithoutDecrees(final Connection con, final RequestFileType fileType) throws SQLException {
    final Integer id = RequestRepository.getOldestRequestIdWithoutDecrees(con, fileType, SegmentType.PROJECTS);
    return id == null ? null : getPermit(con, id);
  }

  /**
   * Get the PermitKey for a specific reference.
   * @param con The connection to use.
   * @param reference The reference to check
   * @return PermitKey for the permit if one exists with the reference. Null if it doesn't exist.
   * @throws SQLException In case of a database error.
   */
  public static PermitKey getPermitKey(final Connection con, final String reference) throws SQLException {
    PermitKey permitKey = null;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_PERMIT_KEY_BY_REFERENCE.get())) {
      GET_PERMIT_KEY_BY_REFERENCE.setParameter(selectPS, QueryAttribute.REFERENCE, reference);

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          permitKey = new PermitKey(QueryUtil.getString(rs, RegisterAttribute.DOSSIER_ID), QueryAttribute.CODE.getString(rs));
        }
      }
    }
    return permitKey;
  }

  /**
   * @param con The connection to use.
   * @param key The key of the permit to check
   * @return True if the permit exists, false if not.
   * @throws SQLException In case of a database error.
   */
  public static boolean permitExists(final Connection con, final DossierAuthorityKey key) throws SQLException {
    return getPermitIdByKey(con, key) != UNKNOWN_PERMIT_ID;
  }

  /**
   * Get a skinned permit without detailed information such as situations, audit trail or validation rules.
   * @param con Connection to use.
   * @param key Permit key used to retrieve the permit.
   * @return A skinned permit
   * @throws SQLException In case of a database error.
   */
  public static Permit getSkinnedPermitByKey(final Connection con, final DossierAuthorityKey key) throws SQLException {
    return getSkinnedPermit(con, getPermitIdByKey(con, key));
  }

  /**
   * Get an permit by its key.
   * @param con Connection to use.
   * @param key Permit key used to retrieve the permit.
   * @return An permit
   * @throws SQLException In case of a database error.
   */
  public static Permit getPermitByKey(final Connection con, final DossierAuthorityKey key) throws SQLException {
    return getPermit(con, getPermitIdByKey(con, key));
  }

  /**
   * Get a permit without detailed information such as situations and audit trail.
   * @param con Connection to use.
   * @param permitId Permit id to retrieve.
   * @return A skinned permit (or null if not found)
   * @throws SQLException In case of a database error.
   */
  public static Permit getSkinnedPermit(final Connection con, final int permitId) throws SQLException {
    try (final PreparedStatement selectPS = con.prepareStatement(GET_PERMIT.get())) {
      GET_PERMIT.setParameter(selectPS, RegisterAttribute.REQUEST_ID, permitId);

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          return getPermitFromResultSet(con, rs);
        }
      }
    }
    return null;
  }

  /**
   * Get an permit with all information such as situations (emission values) and audit trail.
   * @param con Connection to use.
   * @param permitId Permit id to retrieve.
   * @return A permit (or null if not found)
   * @throws SQLException In case of a database error.
   */
  protected static Permit getPermit(final Connection con, final int permitId) throws SQLException {
    final Permit permit = getSkinnedPermit(con, permitId);
    if (permit != null) {
      permit.setSituations(RequestRepository.getSituations(con, permitId));
      permit.setChangeHistory(RequestAuditRepository.getAuditTrail(con, permitId));
    }
    return permit;
  }

  /**
   * @param con The connection to use.
   * @param key The key of the permit to retrieve the (DB) permit id for.
   * @return The proper permit id or -1 if not found
   * @throws SQLException In case of a database error.
   */
  static int getPermitIdByKey(final Connection con, final DossierAuthorityKey key) throws SQLException {
    int permitId = UNKNOWN_PERMIT_ID;

    if (key != null) {
      try (final PreparedStatement selectPS = con.prepareStatement(GET_PERMIT_ID_BY_KEY.get())) {
        GET_PERMIT_ID_BY_KEY.setParameter(selectPS, RegisterAttribute.DOSSIER_ID, key.getDossierId());
        GET_PERMIT_ID_BY_KEY.setParameter(selectPS, QueryAttribute.CODE, key.getAuthorityCode());

        try (final ResultSet rs = selectPS.executeQuery()) {
          if (rs.next()) {
            permitId = QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID);
          }
        }
      }
    }

    return permitId;
  }

  /**
   * @param con The connection to use.
   * @param key The key of the permit to retrieve.
   * @return The proper permit PDF content (or null if not found).
   * @throws SQLException In case of a database error.
   */
  public static byte[] getPermitPDFContent(final Connection con, final DossierAuthorityKey key) throws SQLException {
    final Permit permit = getSkinnedPermitByKey(con, key);
    return permit == null ? null : RequestRepository.getRequestFileContent(con, permit.getReference(), RequestFileType.APPLICATION);
  }

  /**
   * @param con The connection to use.
   * @param maxResults The maximum results to return.
   * @param offset The offset of the results (to be used for paging).
   * @param filter The filter to use when retrieving results.
   * @return A list of permits.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<Permit> getPermits(final Connection con, final int maxResults, final int offset, final PermitFilter filter)
      throws SQLException {
    final EntityCollector<Permit> collector = new EntityCollector<Permit>() {

      @Override
      public int getKey(final ResultSet rs) throws SQLException {
        return QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID);
      }

      @Override
      public Permit fillEntity(final ResultSet rs) throws SQLException {
        return getPermitFromResultSet(con, rs);
      }
    };
    final Query query = getPermitsQuery(filter);
    try (final PreparedStatement selectPS = con.prepareStatement(query.get())) {
      if (!filter.getStates().isEmpty()) {
        query.setParameter(selectPS, RegisterAttribute.STATUS, QueryUtil.toSQLArray(con, filter.getStates()));
      }
      RequestFilterUtil.setRequestFilterParams(con, query, selectPS, filter);

      query.setParameter(selectPS, SpecialAttribute.LIMIT, maxResults);
      query.setParameter(selectPS, SpecialAttribute.OFFSET, offset);

      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          collector.collectEntity(rs);
        }
      }
    }
    final ArrayList<Permit> permits = collector.getEntities();
    for (final Permit permit : permits) {
      permit.setSituations(RequestRepository.getSituations(con, permit.getId()));
    }

    return permits;
  }

  /**
   * @param con The connection to use.
   * @param searchString The string to search for.
   * @return Any permits that fit the string.
   * @throws SQLException In case of exception
   */
  public static ArrayList<Permit> search(final Connection con, final String searchString) throws SQLException {
    final ArrayList<Permit> permits = new ArrayList<>();
    final String sqlSearchString = ILikeWhereClause.getSearchString(searchString);
    try (final PreparedStatement selectPS = con.prepareStatement(SEARCH_PERMITS.get())) {
      SEARCH_PERMITS.setParameter(selectPS, RegisterAttribute.REFERENCE, sqlSearchString);
      SEARCH_PERMITS.setParameter(selectPS, RegisterAttribute.DOSSIER_ID, sqlSearchString);
      SEARCH_PERMITS.setParameter(selectPS, RegisterAttribute.PROJECT_NAME, sqlSearchString);
      SEARCH_PERMITS.setParameter(selectPS, RegisterAttribute.CORPORATION, sqlSearchString);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        permits.add(getPermitFromResultSet(con, rs));
      }
    }

    return permits;
  }

  /**
   * @param con The connection to use.
   * @param requestId The request ID to get the permits within a certain radius for (excluding request itself).
   * @param maxRadius The maximum radius (in m) to look for permits.
   * @return A list of permits, which should be located within the max radius.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<Permit> getPermitsWithinRadius(final Connection con, final int requestId, final int maxRadius)
      throws SQLException {
    final ArrayList<Permit> permits = new ArrayList<>();
    try (final PreparedStatement selectPS = con.prepareStatement(GET_PERMITS_WITHIN_RADIUS.get())) {
      GET_PERMITS_WITHIN_RADIUS.setParameter(selectPS, RegisterAttribute.REQUEST_ID, requestId);
      GET_PERMITS_WITHIN_RADIUS.setParameter(selectPS, QueryAttribute.MAX_DISTANCE, maxRadius);
      //hardcoded limit for now, to avoid retrieving a gazillion requests (should that volume ever be reached).
      GET_PERMITS_WITHIN_RADIUS.setParameter(selectPS, SpecialAttribute.LIMIT, RequestRepository.MAX_REQUESTS_RETRIEVED);

      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          permits.add(getPermitFromResultSet(con, rs));
        }
      }
    }

    return permits;
  }

  /**
   * @param con The connection to use.
   * @param key The key of the permit to check for status.
   * @return True if all calculations for an permit are finished.
   * @throws SQLException In case of a database error.
   */
  public static boolean isCalculationFinished(final Connection con, final DossierAuthorityKey key) throws SQLException {
    boolean finished = false;
    final Permit permit = getSkinnedPermitByKey(con, key);
    if (permit != null) {
      try (final PreparedStatement selectPS = con.prepareStatement(IS_PERMIT_FINISHED.get())) {
        IS_PERMIT_FINISHED.setParameter(selectPS, RegisterAttribute.REQUEST_ID, permit.getId());

        try (final ResultSet rs = selectPS.executeQuery()) {
          if (rs.next()) {
            finished = QueryUtil.getBoolean(rs, RegisterAttribute.FINISHED);
          }
        }
      }
    }
    return finished;
  }

  private static Permit getPermitFromResultSet(final Connection con, final ResultSet rs)
      throws SQLException {
    final Permit permit = new Permit();
    permit.setId(QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID));
    permit.setRequestState(QueryUtil.getEnum(rs, RegisterAttribute.STATUS, RequestState.class));
    permit.setPotentiallyRejectable(QueryUtil.getBoolean(rs, RepositoryAttribute.POTENTIALLY_REJECTABLE));

    RequestRepository.fillRequestFromResultSet(con, rs, permit);

    final DossierMetaData dossierMetaData = new DossierMetaData();
    RequestRepository.fillDossierMetaDataFromResultSet(con, rs, dossierMetaData);
    dossierMetaData.setHandler(UserRepository.getUserProfile(con, QueryUtil.getInt(rs, RegisterAttribute.HANDLER_ID)));
    permit.setDossierMetaData(dossierMetaData);

    return permit;
  }

  private static Query getPermitsQuery(final PermitFilter filter) {
    final QueryBuilder builder = getPermitFullInfo();
    if (!filter.getStates().isEmpty()) {
      builder.where(new StaticWhereClause(RegisterAttribute.STATUS.attribute() + " = ANY (?::request_status_type[])", RegisterAttribute.STATUS));
    }
    RequestFilterUtil.addRequestFilter(builder, filter, RegisterAttribute.RECEIVED_DATE);
    RegisterOrderByUtil.setOrderByParams(builder, filter.getSortAttribute(), filter.getSortDirection());
    builder.orderBy(new OrderByClause(RegisterAttribute.RECEIVED_DATE, OrderType.ASC),
        new OrderByClause(RegisterAttribute.REQUEST_ID, OrderType.DESC))
        .limit().offset();
    return builder.getQuery();
  }

  private static QueryBuilder getPermitBase() {
    return QueryBuilder.from("permits_view")
        .select(RegisterAttribute.REQUEST_ID);
  }

  private static QueryBuilder getPermitFullInfo() {
    final QueryBuilder builder = getPermitBase();
    RequestRepository.addRequiredAttributes(builder);
    return builder.select(RegisterAttribute.DOSSIER_ID, RegisterAttribute.RECEIVED_DATE, RegisterAttribute.STATUS,
        RegisterAttribute.SEGMENT, RegisterAttribute.HANDLER_ID, RegisterAttribute.REMARKS, RepositoryAttribute.POTENTIALLY_REJECTABLE);
  }

}
