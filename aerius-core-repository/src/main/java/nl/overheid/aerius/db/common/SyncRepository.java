/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.db.BatchInserter;
import nl.overheid.aerius.db.util.InsertBuilder;
import nl.overheid.aerius.db.util.InsertClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.TruncateBuilder;
import nl.overheid.aerius.shared.domain.register.DevelopmentSpaceStatusType;
import nl.overheid.aerius.shared.domain.register.SegmentType;

/**
 * These methods are used by the worker that syncs Register table data to
 * Calculator. There are no pretty domain objects used here, all we care about
 * is a simple and fast data dump from one table to the other, where both
 * tables have the same structure as well.
 */
public class SyncRepository {

  private static final int BATCH_SIZE = 500;

  private static final String PERMIT_THRESHOLD_VALUES = "permit_threshold_values";
  private static final String DEVELOPMENT_SPACES = "development_spaces";
  private static final String RESERVED_DEVELOPMENT_SPACES = "reserved_development_spaces";
  private static final String INITIAL_AVAILABLE_DEVELOPMENT_SPACES = "initial_available_development_spaces";

  private static final Query READ_PERMIT_THRESHOLD_VALUES =
      QueryBuilder.from(PERMIT_THRESHOLD_VALUES)
          .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.VALUE).getQuery();

  private static final Query TRUNCATE_PERMIT_THRESHOLD_VALUES =
      TruncateBuilder.truncate(PERMIT_THRESHOLD_VALUES).getQuery();

  private static final Query WRITE_PERMIT_THRESHOLD_VALUES =
      InsertBuilder.into(PERMIT_THRESHOLD_VALUES)
          .insert(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.VALUE).getQuery();

  private static final Query READ_DEVELOPMENT_SPACES =
      QueryBuilder.from(DEVELOPMENT_SPACES)
          .select(QueryAttribute.SEGMENT, QueryAttribute.STATUS, QueryAttribute.RECEPTOR_ID, QueryAttribute.SPACE).getQuery();

  private static final Query TRUNCATE_DEVELOPMENT_SPACES =
      TruncateBuilder.truncate(DEVELOPMENT_SPACES).getQuery();

  private static final Query WRITE_DEVELOPMENT_SPACES =
      InsertBuilder.into(DEVELOPMENT_SPACES)
          .insert(new InsertClause(QueryAttribute.SEGMENT.attribute(), "?::segment_type", QueryAttribute.SEGMENT))
          .insert(new InsertClause(QueryAttribute.STATUS.attribute(), "?::development_space_state", QueryAttribute.STATUS))
          .insert(QueryAttribute.RECEPTOR_ID, QueryAttribute.SPACE).getQuery();

  private static final Query READ_RESERVED_DEVELOPMENT_SPACES =
      QueryBuilder.from(RESERVED_DEVELOPMENT_SPACES)
          .select(QueryAttribute.SEGMENT, QueryAttribute.RECEPTOR_ID, QueryAttribute.SPACE, QueryAttribute.BORROWED)
          .orderBy(QueryAttribute.SEGMENT, QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query READ_INITIAL_AVAILABLE_SPACES =
      QueryBuilder.from(INITIAL_AVAILABLE_DEVELOPMENT_SPACES)
          .select(QueryAttribute.SEGMENT, QueryAttribute.RECEPTOR_ID, QueryAttribute.SPACE)
          .orderBy(QueryAttribute.SEGMENT, QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query TRUNCATE_RESERVED_AND_INITIAL_AVAILABLE_DEVELOPMENT_SPACES =
      TruncateBuilder.truncate(RESERVED_DEVELOPMENT_SPACES, INITIAL_AVAILABLE_DEVELOPMENT_SPACES).getQuery();

  private static final Query WRITE_RESERVED_DEVELOPMENT_SPACES =
      InsertBuilder.into(RESERVED_DEVELOPMENT_SPACES)
          .insert(new InsertClause(QueryAttribute.SEGMENT.attribute(), "?::segment_type", QueryAttribute.SEGMENT))
          .insert(QueryAttribute.RECEPTOR_ID, QueryAttribute.SPACE, QueryAttribute.BORROWED).getQuery();

  private static final Query WRITE_INITIAL_AVAILABLE_DEVELOPMENT_SPACES =
      InsertBuilder.into(INITIAL_AVAILABLE_DEVELOPMENT_SPACES)
          .insert(new InsertClause(QueryAttribute.SEGMENT.attribute(), "?::segment_type", QueryAttribute.SEGMENT))
          .insert(QueryAttribute.RECEPTOR_ID, QueryAttribute.SPACE).getQuery();

  public static class DevelopmentSpacesEntry {

    private final SegmentType segment;
    private final DevelopmentSpaceStatusType status;
    private final int receptorId;
    private final double space;

    public DevelopmentSpacesEntry(final ResultSet rs) throws SQLException {
      segment = SegmentType.valueOf(QueryAttribute.SEGMENT.getString(rs).toUpperCase());
      status = DevelopmentSpaceStatusType.valueOf(QueryAttribute.STATUS.getString(rs).toUpperCase());
      receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
      space = QueryAttribute.SPACE.getDouble(rs);
    }

    public void setParameters(final Query query, final PreparedStatement ps) throws SQLException {
      query.setParameter(ps, QueryAttribute.SEGMENT, segment.getDbValue());
      query.setParameter(ps, QueryAttribute.STATUS, status.getDbValue());
      query.setParameter(ps, QueryAttribute.RECEPTOR_ID, receptorId);
      query.setParameter(ps, QueryAttribute.SPACE, space);
    }
  }

  public static class InitialAvailableDevelopmentSpacesEntry {
    private final SegmentType segment;
    private final int receptorId;
    private final double space;

    public InitialAvailableDevelopmentSpacesEntry(final ResultSet rs) throws SQLException {
      segment = SegmentType.valueOf(QueryAttribute.SEGMENT.getString(rs).toUpperCase());
      receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
      space = QueryAttribute.SPACE.getDouble(rs);
    }

    public void setParameters(final Query query, final PreparedStatement ps) throws SQLException {
      query.setParameter(ps, QueryAttribute.SEGMENT, segment.getDbValue());
      query.setParameter(ps, QueryAttribute.RECEPTOR_ID, receptorId);
      query.setParameter(ps, QueryAttribute.SPACE, space);
    }
  }

  public static class ReservedDevelopmentSpacesEntry extends InitialAvailableDevelopmentSpacesEntry {

    private final double borrowed;

    public ReservedDevelopmentSpacesEntry(final ResultSet rs) throws SQLException {
      super(rs);

      borrowed = QueryAttribute.BORROWED.getDouble(rs);
    }

    @Override
    public void setParameters(final Query query, final PreparedStatement ps) throws SQLException {
      super.setParameters(query, ps);

      query.setParameter(ps, QueryAttribute.BORROWED, borrowed);
    }
  }

  public static Map<Integer, Double> readPermitThresholdValues(final Connection con) throws SQLException {
    // Read (small) table into map
    final Map<Integer, Double> values = new HashMap<>();
    try (PreparedStatement ps = con.prepareStatement(READ_PERMIT_THRESHOLD_VALUES.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        values.put(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs), QueryAttribute.VALUE.getDouble(rs));
      }
    }
    return values;
  }

  public static void writePermitThresholdValues(final Connection con, final Map<Integer, Double> values) throws SQLException {
    // Truncate
    try (PreparedStatement ps = con.prepareStatement(TRUNCATE_PERMIT_THRESHOLD_VALUES.get())) {
      ps.executeUpdate();
    }

    // Batch insert
    final BatchInserter<Entry<Integer, Double>> inserter = new BatchInserter<Entry<Integer, Double>>() {
      @Override
      public void setParameters(final PreparedStatement ps, final Entry<Integer, Double> entry) throws SQLException {
        WRITE_PERMIT_THRESHOLD_VALUES.setParameter(ps, QueryAttribute.ASSESSMENT_AREA_ID, entry.getKey());
        WRITE_PERMIT_THRESHOLD_VALUES.setParameter(ps, QueryAttribute.VALUE, entry.getValue());
      }
    };
    inserter.setBatchSize(BATCH_SIZE);
    final int inserted = inserter.insertBatch(con, WRITE_PERMIT_THRESHOLD_VALUES.get(), values.entrySet());
    if (inserted != values.size()) {
      throw new SQLException("Batch insert error.\nInserted: " + inserted + "\nExpected: " + values.size());
    }
  }

  public static List<DevelopmentSpacesEntry> readDevelopmentSpaces(final Connection con) throws SQLException {
    // Get count so that arraylist does not have to resize
    int size = 0;
    try (PreparedStatement ps = con.prepareStatement(READ_DEVELOPMENT_SPACES.toRecordCountQuery(QueryAttribute.COUNT).get())) {
      final ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        size = QueryAttribute.COUNT.getInt(rs);
      }
    }

    // Read table into arraylist
    final List<DevelopmentSpacesEntry> records = new ArrayList<>(size);
    try (PreparedStatement ps = con.prepareStatement(READ_DEVELOPMENT_SPACES.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        records.add(new DevelopmentSpacesEntry(rs));
      }
    }
    return records;
  }

  public static void writeDevelopmentSpaces(final Connection con, final List<DevelopmentSpacesEntry> records) throws SQLException {
    // Truncate
    try (PreparedStatement ps = con.prepareStatement(TRUNCATE_DEVELOPMENT_SPACES.get())) {
      ps.executeUpdate();
    }

    // Batch insert
    final BatchInserter<DevelopmentSpacesEntry> inserter = new BatchInserter<DevelopmentSpacesEntry>() {
      @Override
      public void setParameters(final PreparedStatement ps, final DevelopmentSpacesEntry entry) throws SQLException {
        entry.setParameters(WRITE_DEVELOPMENT_SPACES, ps);
      }
    };
    inserter.setBatchSize(BATCH_SIZE);
    final int inserted = inserter.insertBatch(con, WRITE_DEVELOPMENT_SPACES.get(), records);
    if (inserted != records.size()) {
      throw new SQLException("Batch insert error.\nInserted: " + inserted + "\nExpected: " + records.size());
    }
  }

  public static List<ReservedDevelopmentSpacesEntry> readReservedDevelopmentSpaces(final Connection con) throws SQLException {
    // Get count so that arraylist does not have to resize
    final int size = getRecordCount(con, READ_RESERVED_DEVELOPMENT_SPACES);

    // Read table into arraylist
    final List<ReservedDevelopmentSpacesEntry> records = new ArrayList<>(size);
    try (PreparedStatement ps = con.prepareStatement(READ_RESERVED_DEVELOPMENT_SPACES.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        records.add(new ReservedDevelopmentSpacesEntry(rs));
      }
    }
    return records;
  }

  public static List<InitialAvailableDevelopmentSpacesEntry> readInitialAvailableDevelopmentSpaces(final Connection con) throws SQLException {
    // Get count so that arraylist does not have to resize
    final int size = getRecordCount(con, READ_INITIAL_AVAILABLE_SPACES);

    // Read table into arraylist
    final List<InitialAvailableDevelopmentSpacesEntry> records = new ArrayList<>(size);
    try (PreparedStatement ps = con.prepareStatement(READ_INITIAL_AVAILABLE_SPACES.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        records.add(new InitialAvailableDevelopmentSpacesEntry(rs));
      }
    }
    return records;
  }

  public static void writeReservedAndInitialAvailableDevelopmentSpaces(final Connection con,
      final List<ReservedDevelopmentSpacesEntry> recordsReserved, final List<InitialAvailableDevelopmentSpacesEntry> recordsInitialAvailable)
      throws SQLException {
    // Truncate (initial available has reserved as foreign key, so truncate simultaneously)
    try (PreparedStatement ps = con.prepareStatement(TRUNCATE_RESERVED_AND_INITIAL_AVAILABLE_DEVELOPMENT_SPACES.get())) {
      ps.executeUpdate();
    }

    // Batch insert reserved development spaces
    final BatchInserter<ReservedDevelopmentSpacesEntry> inserterReserved = new BatchInserter<ReservedDevelopmentSpacesEntry>() {
      @Override
      public void setParameters(final PreparedStatement ps, final ReservedDevelopmentSpacesEntry entry) throws SQLException {
        entry.setParameters(WRITE_RESERVED_DEVELOPMENT_SPACES, ps);
      }
    };
    inserterReserved.setBatchSize(BATCH_SIZE);
    final int insertedReserved = inserterReserved.insertBatch(con, WRITE_RESERVED_DEVELOPMENT_SPACES.get(), recordsReserved);
    if (insertedReserved != recordsReserved.size()) {
      throw new SQLException("Reserved - Batch insert error.\nInserted: " + insertedReserved + "\nExpected: " + recordsReserved.size());
    }

    // Batch insert initial available development spaces
    final BatchInserter<InitialAvailableDevelopmentSpacesEntry> inserterInitialAvailable =
        new BatchInserter<InitialAvailableDevelopmentSpacesEntry>() {
      @Override
      public void setParameters(final PreparedStatement ps, final InitialAvailableDevelopmentSpacesEntry entry) throws SQLException {
        entry.setParameters(WRITE_INITIAL_AVAILABLE_DEVELOPMENT_SPACES, ps);
      }
    };
    inserterInitialAvailable.setBatchSize(BATCH_SIZE);
    final int insertedInitialAvailable =
        inserterInitialAvailable.insertBatch(con, WRITE_INITIAL_AVAILABLE_DEVELOPMENT_SPACES.get(), recordsInitialAvailable);
    if (insertedInitialAvailable != recordsInitialAvailable.size()) {
      throw new SQLException(
          "Initial Available - Batch insert error.\nInserted: " + insertedInitialAvailable + "\nExpected: " + recordsInitialAvailable.size());
    }
  }

  private static int getRecordCount(final Connection con, final Query query) throws SQLException {
    int size = 0;

    try (PreparedStatement ps = con.prepareStatement(query.toRecordCountQuery(QueryAttribute.COUNT).get())) {
      final ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        size = QueryAttribute.COUNT.getInt(rs);
      }
    }

    return size;
  }
}
