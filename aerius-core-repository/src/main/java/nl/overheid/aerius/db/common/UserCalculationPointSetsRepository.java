/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.BatchInserter;
import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.LandUse;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.user.UserCalculationPointSetMetadata;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

public final class UserCalculationPointSetsRepository {

  private static final Logger LOG = LoggerFactory.getLogger(UserCalculationPointSetsRepository.class);

  private static final int BATCH_SIZE = 500;

  private static final String INSERT_USER_CALCULATION_POINT_SET =
      "INSERT INTO user_calculation_point_sets (user_id, name, description) VALUES (?, ?, ?)";

  private static final String AE_DELETE_USER_CALCULATION_POINT_SET =
      "SELECT ae_delete_user_calculation_point_set_and_points(?)";

  private static final String SELECT_USER_CALCULATION_POINT_SET_USER_ID =
      "SELECT * FROM user_calculation_point_sets WHERE user_id = ?";

  private static final String SELECT_USER_CALCULATION_POINT_SET_USERID_NAME =
      "SELECT * FROM user_calculation_point_sets WHERE user_id = ? AND name = ?";

  private static final String INSERT_USER_CALCULATION_POINTS =
      "INSERT INTO user_calculation_points (user_calculation_point_set_id, user_calculation_point_id, label, nearest_receptor_id, geometry,"
          + " average_roughness, dominant_land_use, land_uses, class_name)"
          + " VALUES (?, ?, ?, ?, ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()), ?, ?, ?, ?)";

  private static final Query GET_USER_CALCULATION_POINTS = QueryBuilder.from("user_calculation_points")
      .select(QueryAttribute.USER_CALCULATION_POINT_ID, QueryAttribute.LABEL,
          QueryAttribute.AVERAGE_ROUGHNESS, QueryAttribute.DOMINANT_LAND_USE,
          QueryAttribute.LAND_USES, QueryAttribute.CLASS_NAME)
      .select(new SelectClause("ST_X(geometry)", QueryAttribute.X_COORD.attribute()),
          new SelectClause("ST_Y(geometry)", QueryAttribute.Y_COORD.attribute()))
      .distinct()
      .where(QueryAttribute.USER_CALCULATION_POINT_SET_ID).getQuery();

  private UserCalculationPointSetsRepository() {

  }

  /**
   * Creates a Receptorset. The Receptorset object's id will be updated.
   *
   * @param con The connection to use.
   * @param user The user to create (id is ignored / max concurrent jobs is automatically filled).
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the user already exists.
   */
  public static <T extends AeriusPoint> void insertUserCalculationPointSet(final Connection con, final UserCalculationPointSetMetadata setMetadata,
      final List<T> calculationPoints) throws SQLException, AeriusException {
    final Transaction transaction = new Transaction(con);
    try {
      unsafeInsertUserCalculationPointSet(con, setMetadata, calculationPoints);
    } catch (final SQLException e) {
      transaction.rollback();
      throw e;
    } catch (final AeriusException ae) {
      transaction.rollback();
      throw ae;
    } finally {
      transaction.commit();
    }
  }

  /**
   * @param con
   * @param setMetadata
   * @param calculationPoints
   * @throws SQLException
   * @throws AeriusException
   */
  static <T extends AeriusPoint> void unsafeInsertUserCalculationPointSet(final Connection con,
      final UserCalculationPointSetMetadata setMetadata, final List<T> calculationPoints)
      throws SQLException, AeriusException {
    try (final PreparedStatement ps = con.prepareStatement(INSERT_USER_CALCULATION_POINT_SET)) {
      QueryUtil.setValues(ps, setMetadata.getUserId(), setMetadata.getName(), setMetadata.getDescription());
      ps.executeUpdate();

      final UserCalculationPointSetMetadata newSetMetadata = getUserCalculationPointSetByName(con, setMetadata.getUserId(), setMetadata.getName());
      if (newSetMetadata == null) { // Should never happen.
        LOG.error("Unexpected error after successfully inserting user_calculation_point_set record {} for user {} in database, rolling back.",
            setMetadata.getName(), setMetadata.getUserId());
        throw new AeriusException(Reason.INTERNAL_ERROR);
      } else {
        insertUserCalculationPointsForSet(con, INSERT_USER_CALCULATION_POINTS, newSetMetadata.getSetId(),
            calculationPoints);
      }
    }
  }

  /**
   * Retrieves AeriusPoint / OPSReceptor sets from the database.
   *
   * @param connection
   * @param setId
   * @return
   * @throws SQLException
   * @throws ClassNotFoundException
   * @throws IllegalAccessException
   * @throws InstantiationException
   */
  public static List<AeriusPoint> getUserCalculationPointsFromSet(final Connection connection, final int setId)
      throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    final List<AeriusPoint> pointList = new ArrayList<>();
    try (final PreparedStatement ps = connection.prepareStatement(GET_USER_CALCULATION_POINTS.get())) {
      GET_USER_CALCULATION_POINTS.setParameter(ps, QueryAttribute.USER_CALCULATION_POINT_SET_ID, setId);

      try (final ResultSet rs = ps.executeQuery()) {

        while (rs.next()) {
          pointList.add(createAndFillPoint(rs));
        }
      }
    }
    return pointList;
  }

  /**
   * @param rs
   * @return New AeriusPoint/OPSReceptor.
   * @throws SQLException
   * @throws ClassNotFoundException
   * @throws IllegalAccessException
   * @throws InstantiationException
   */
  private static AeriusPoint createAndFillPoint(final ResultSet rs)
      throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    final int calculationPointId = QueryAttribute.USER_CALCULATION_POINT_ID.getInt(rs);
    final String className = QueryAttribute.CLASS_NAME.getString(rs);
    final AeriusPoint calculationPoint = (AeriusPoint) Class.forName(className).newInstance();
    calculationPoint.setId(calculationPointId);
    calculationPoint.setX(QueryUtil.getDouble(rs, QueryAttribute.X_COORD));
    calculationPoint.setY(QueryUtil.getDouble(rs, QueryAttribute.Y_COORD));
    calculationPoint.setLabel(QueryAttribute.LABEL.getString(rs));
    calculationPoint.setPointType(AeriusPointType.POINT);
    final Double averageRoughness = QueryAttribute.AVERAGE_ROUGHNESS.getDouble(rs);
    if ((calculationPoint instanceof OPSReceptor) && !rs.wasNull()) {
      final OPSReceptor opsReceptor = (OPSReceptor) calculationPoint;
      opsReceptor.setAverageRoughness(averageRoughness);
      opsReceptor.setLandUse(LandUse.getByOption(QueryAttribute.DOMINANT_LAND_USE.getInt(rs)));
      final Array landUses = QueryAttribute.LAND_USES.getArray(rs);
      final Integer[] landUseArray = (Integer[]) landUses.getArray();
      opsReceptor.setLandUses(Arrays.stream(landUseArray).mapToInt(Integer::intValue).toArray());
    }
    return calculationPoint;
  }

  /**
  * @param connection
  * @param destinationQuery
  * @param calculationPointSetId
  * @param calculationPoints
  * @return
  * @throws SQLException
  */
  static <T extends AeriusPoint> int insertUserCalculationPointsForSet(final Connection connection, final String destinationQuery,
      final int calculationPointSetId, final List<T> calculationPoints) throws SQLException {
    final BatchInserter<T> inserter = new UserCalculationPointBatchInserter<T>(connection, calculationPointSetId);
    inserter.setBatchSize(BATCH_SIZE);
    final int inserted = inserter.insertBatch(connection, destinationQuery, calculationPoints);
    if (inserted != calculationPoints.size()) {
      throw new SQLException("Did not save all calculation points! calculation point set id: " + calculationPointSetId + ". Inserted points: "
          + inserted + ". Expected: " + calculationPoints.size());
    }
    return calculationPointSetId;
  }

  /**
  * @param con The connection to use.
  * @param userId The id of the user.
  * @param name of the userCalculationPointSet to delete.
  * @throws SQLException In case of a database error.
  * @throws AeriusException When the pointSet to delete did not exist.
  */
  public static void deleteUserCalculationPointSet(final Connection con, final int setId) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(AE_DELETE_USER_CALCULATION_POINT_SET)) {
      QueryUtil.setValues(stmt, setId);

      stmt.execute();
    }
  }

  /**
   * Select name of ReceptorSet point
   * @param con
   * @param name
   * @return
   * @throws SQLException
   */
  public static List<UserCalculationPointSetMetadata> getUserCalculationPointSetsForUser(final Connection con, final int userId) throws SQLException {
    final List<UserCalculationPointSetMetadata> resultList = new ArrayList<>();
    try (PreparedStatement statement = con.prepareStatement(SELECT_USER_CALCULATION_POINT_SET_USER_ID)) {
      QueryUtil.setValues(statement, userId);

      try (final ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          resultList.add(getMetadataFromResultSet(rs));
        }
      }
    }
    return resultList;
  }

  public static UserCalculationPointSetMetadata getUserCalculationPointSetByName(final Connection con, final int userId, final String name)
      throws SQLException {
    UserCalculationPointSetMetadata userCalculationPointSet = null;
    try (final PreparedStatement stmt = con.prepareStatement(SELECT_USER_CALCULATION_POINT_SET_USERID_NAME)) {
      QueryUtil.setValues(stmt, userId, name);

      try (final ResultSet rs = stmt.executeQuery()) {
        if (rs.next()) {
          userCalculationPointSet = getMetadataFromResultSet(rs);
        }
      }
    }

    return userCalculationPointSet;
  }

  private static UserCalculationPointSetMetadata getMetadataFromResultSet(final ResultSet rs) throws SQLException {
    final UserCalculationPointSetMetadata userCalculationPointSet = new UserCalculationPointSetMetadata();

    userCalculationPointSet.setSetId(QueryAttribute.USER_CALCULATION_POINT_SET_ID.getInt(rs));
    userCalculationPointSet.setUserId(QueryAttribute.USER_ID.getInt(rs));
    userCalculationPointSet.setName(QueryAttribute.NAME.getString(rs));
    userCalculationPointSet.setDescription(QueryAttribute.DESCRIPTION.getString(rs));

    return userCalculationPointSet;
  }
}
