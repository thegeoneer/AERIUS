/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.test.TestIDMelding;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.melding.widget.FileUploadPanel;

@Singleton
public class MeldingExtraInformationViewImpl extends Composite implements MeldingExtraInformationView {
  interface NoticeExtraInformationViewImplUiBinder extends UiBinder<Widget, MeldingExtraInformationViewImpl> {}

  private static final NoticeExtraInformationViewImplUiBinder UI_BINDER = GWT.create(NoticeExtraInformationViewImplUiBinder.class);

  public interface MeldingExtraInformationDriver extends SimpleBeanEditorDriver<MeldingInformation, MeldingExtraInformationViewImpl> {}

  //  @Ignore @UiField Label coreInformationCalculation;
  //  @Ignore @UiField Label coreInformationReference;
  //  @Ignore @UiField Label coreInformationEmissionNOX;
  //  @Ignore @UiField Label coreInformationEmissionNH3;
  //  @Ignore @UiField Label coreInformationHighestDeposition;

  @Ignore @UiField RadioButton existingPermitNo;
  @UiField RadioButton existingPermitPresent;
  @UiField TextValueBox existingPermitReference;
  @Ignore @UiField Label checkSourceLabel;
  @Ignore @UiField FlowPanel existingSourcePermitPanel;
  @UiField RadioButton existingSourcePermitPresent;
  @Ignore @UiField RadioButton existingSourcePermitNo;

  @UiField(provided = true) FileUploadPanel substantiationFilesEditor;
  @UiField TextArea substantiation;
  @Ignore @UiField CheckBox correctInformationConfirmation;

  private Presenter presenter;

  public MeldingExtraInformationViewImpl() {
    substantiationFilesEditor = new FileUploadPanel(M.messages().meldingSubstantiationFiles(),  SharedConstants.IMPORT_FILE_FIELD_NAME, 4);
    initWidget(UI_BINDER.createAndBindUi(this));

    existingPermitReference.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return existingPermitPresent.getValue() ? M.messages().validatorExistingPermitReference() : null;
      }
    });

    StyleUtil.I.setPlaceHolder(existingPermitReference, M.messages().meldingExistingPermitReference());
    StyleUtil.I.setPlaceHolder(substantiation, M.messages().meldingFoundationDescription());

    existingPermitNo.ensureDebugId(TestIDMelding.NB_PERMIT_NO);
    existingPermitPresent.ensureDebugId(TestIDMelding.NB_PERMIT_YES);

    existingSourcePermitNo.ensureDebugId(TestIDMelding.NB_SOURCE_PERMIT_NO);
    existingSourcePermitPresent.ensureDebugId(TestIDMelding.NB_SOURCE_PERMIT_YES);

    substantiationFilesEditor.addValidator(new Validator<ArrayList<String>>() {
      @Override
      public String validate(final ArrayList<String> value) {
        final String string = (existingPermitNo.getValue() || existingPermitPresent.getValue() && existingSourcePermitNo.getValue())
            && substantiationFilesEditor.getValue().isEmpty() ? M.messages().meldingSubstantiationFilesRequired() : null;
            return string;
      }
    });

    existingPermitReference.ensureDebugId(TestIDMelding.PROVINCE_REF);
    substantiation.ensureDebugId(TestIDMelding.REFERENCE_DESCRIPTION);

    correctInformationConfirmation.ensureDebugId(TestIDMelding.TRUTH);
  }

  @UiHandler({"existingPermitNo", "existingPermitPresent"})
  public void onExistingPermitValueChange(final ValueChangeEvent<Boolean> e) {
    final boolean existingPermit = existingPermitPresent.getValue();
    hideAndClear(existingPermitReference, existingPermit);
    existingSourcePermitPanel.setVisible(existingPermit);
    substantiationFilesEditor.setVisible(!existingPermit || existingSourcePermitNo.getValue());
  }

  @UiHandler({"existingSourcePermitNo", "existingSourcePermitPresent"})
  public void onExistingSourcePermitValueChange(final ValueChangeEvent<Boolean> e) {
    substantiationFilesEditor.setVisible(existingSourcePermitNo.getValue());
  }

  private void hideAndClear(final TextValueBox textBox, final boolean visible) {
    if (!visible) {
      textBox.setText(null);
    }
    textBox.setVisible(visible);
  }

  @UiHandler("correctConfirmationCheckBoxLabel")
  public void onCorrectConfirmationCheckBoxLabelClick(final ClickEvent e) {
    correctInformationConfirmation.setValue(!correctInformationConfirmation.getValue(), true);
    presenter.setCheckBoxEnabled(correctInformationConfirmation.getValue());
  }

  @UiHandler("correctInformationConfirmation")
  public void onCorrectConfirmationCheckBoxClick(final ClickEvent e) {
    presenter.setCheckBoxEnabled(correctInformationConfirmation.getValue());
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
    substantiationFilesEditor.setFileUploadActions(presenter);
    // Always reset question to ask if entered for real.
    correctInformationConfirmation.setValue(false, true);
  }
}
