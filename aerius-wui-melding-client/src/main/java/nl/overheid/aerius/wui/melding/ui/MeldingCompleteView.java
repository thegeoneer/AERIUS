/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.ui;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.wui.melding.context.MeldingAppContext.MeldingSubmitState;

public interface MeldingCompleteView extends Editor<MeldingInformation>, IsWidget {
  interface Presenter {
  }

  void setState(MeldingSubmitState submitState);
}
