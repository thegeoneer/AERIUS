/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.widget;

import java.util.List;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.editor.client.HasEditorErrors;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;

import nl.overheid.aerius.shared.domain.melding.OrganisationInformation;
import nl.overheid.aerius.shared.test.TestIDMelding;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.ui.validation.EmailValidator;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.util.StyleUtil;

/**
 *
 */
public abstract class AbstractOrganisationInformationEditor<T extends OrganisationInformation>
    extends Composite implements Editor<T>, HasEditorErrors<T> {

  @UiField TextValueBox organisationName;
  @UiField TextValueBox organisationContactPerson;
  @UiField TextValueBox organisationAddress;
  @UiField TextValueBox organisationPostcode;
  @UiField TextValueBox organisationCity;
  @UiField TextValueBox organisationEmail;

  private boolean editorEnabled = true;

  protected void addValidators() {
    organisationName.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().validatorOrganisationName();
      }
    });
    organisationContactPerson.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().validatorOrganisationContactPerson();
      }
    });
    organisationAddress.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().validatorOrganisationAddress();
      }
    });
    organisationPostcode.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().validatorOrganisationPostcode();
      }
    });
    organisationCity.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().validatorOrganisationCity();
      }
    });
    organisationEmail.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().validatorOrganisationEmail();
      }
    });
    organisationEmail.addValidator(new EmailValidator());
  }

  protected void setPlaceHolders() {
    StyleUtil.I.setPlaceHolder(organisationName, M.messages().placeHolderOrganisationName());
    StyleUtil.I.setPlaceHolder(organisationContactPerson, M.messages().placeHolderOrganisationContactPerson());
    StyleUtil.I.setPlaceHolder(organisationAddress, M.messages().placeHolderOrganisationAddress());
    StyleUtil.I.setPlaceHolder(organisationPostcode, M.messages().placeHolderOrganisationPostcode());
    StyleUtil.I.setPlaceHolder(organisationCity, M.messages().placeHolderOrganisationCity());
    StyleUtil.I.setPlaceHolder(organisationEmail, M.messages().placeHolderOrganisationEmail());
  }

  public void clear() {
    organisationName.setText(null);
    organisationContactPerson.setText(null);
    organisationAddress.setText(null);
    organisationPostcode.setText(null);
    organisationCity.setText(null);
    organisationEmail.setText(null);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    organisationName.ensureDebugId(baseID + TestIDMelding.ORG_NAME);
    organisationContactPerson.ensureDebugId(baseID + TestIDMelding.ORG_CONTACT);
    organisationAddress.ensureDebugId(baseID + TestIDMelding.ORG_ADDR);
    organisationPostcode.ensureDebugId(baseID + TestIDMelding.ORG_POST);
    organisationCity.ensureDebugId(baseID + TestIDMelding.ORG_CITY);
    organisationEmail.ensureDebugId(baseID + TestIDMelding.ORG_EMAIL);
  }

  public void setEditorEnabled(final boolean editorEnabled) {
    this.editorEnabled = editorEnabled;
    if (!editorEnabled) {
      clear();
    }
  }

  @Override
  public void showErrors(final List<EditorError> errors) {
    if (!editorEnabled) {
      for (final EditorError error : errors) {
        error.setConsumed(true);
      }
    }
  }

}
