/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.ui;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.context.MeldingUserContext;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.service.MeldingServiceAsync;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.melding.context.MeldingAppContext;
import nl.overheid.aerius.wui.melding.context.MeldingAppContext.MeldingSubmitState;
import nl.overheid.aerius.wui.melding.place.MeldingCompletePlace;


public class MeldingCompleteActivity extends BaseMeldingActivity<MeldingCompleteView> implements MeldingCompleteView.Presenter {
  private final MeldingCompleteView view;
  private final MeldingServiceAsync service;
  private final MeldingAppContext appContext;

  @Inject
  public MeldingCompleteActivity(final BaseMeldingView baseView, @Assisted final MeldingCompletePlace place, final MeldingCompleteView view,
      final PlaceController placeController, final MeldingAppContext appContext, final MeldingUserContext userContext, final MeldingServiceAsync service) {
    super(baseView, place, placeController, userContext, null);
    this.view = view;
    this.appContext = appContext;
    this.service = service;
  }

  @Override
  protected MeldingCompleteView getContentView() {
    submitMelding();
    return view;
  }

  private void submitMelding() {
    if (!appContext.isSend()) {
      appContext.setSubmitState(MeldingSubmitState.IN_PROGRESS);
      final MeldingInformation meldingInformation = userContext.getMeldingInformation();

      service.submitMelding(meldingInformation, new AppAsyncCallback<Void>() {
        @Override
        public void onSuccess(final Void result) {
          appContext.setSubmitState(MeldingSubmitState.SUCCESS);
          setViewState();
        }

        @Override
        public void onFailure(final Throwable caught) {
          appContext.setSubmitState(MeldingSubmitState.FAILED);
          setViewState();
        }
      });
    } else {
      setViewState();
    }
  }

  private void setViewState() {
    view.setState(appContext.getSubmitState());
  }

  @Override
  protected Place getPreviousPlace() {
    return place; // there is no previous place, this is the last stop.
  }

  @Override
  protected Place getNextPlace() {
    return place; // there is no next place. This is the last stop...
  }

  @Override
  public void deleteFile(final String filename, final AsyncCallback<Void> callback) {
    //no-op
  }

  @Override
  public void fileExist(final String filename, final AsyncCallback<Boolean> callback) {
    //no-op
  }

  @Override
  public void startSubmit(final String uploadFileName) {
    //no-op
  }

  @Override
  public void onSubmitComplete() {
    //no-op
  }

  @Override
  public String getUUID() {
    return userContext.getMeldingInformation().getUUID();
  }

  @Override
  public boolean isFileNameInList(final String file) {
    return false;
  }

}
