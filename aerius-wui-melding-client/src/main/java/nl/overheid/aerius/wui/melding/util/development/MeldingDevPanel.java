/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.util.development;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.MeldingUserContext;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.wui.main.util.development.EmbeddedDevPanel;
import nl.overheid.aerius.wui.melding.place.MeldingInitiatorPlace;

public class MeldingDevPanel extends Composite implements EmbeddedDevPanel {
  interface MeldingDevPanelUiBinder extends UiBinder<Widget, MeldingDevPanel> {}

  private static final MeldingDevPanelUiBinder UI_BINDER = GWT.create(MeldingDevPanelUiBinder.class);
  private final MeldingUserContext userContext;
  private final PlaceController placeController;

  @Inject
  public MeldingDevPanel(final MeldingUserContext userContext, final PlaceController placeController) {
    this.userContext = userContext;
    this.placeController = placeController;

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @UiHandler("fillIn")
  void onDefaultFormClick(final ClickEvent e) {
    final MeldingInformation information = new MeldingInformation();
    information.setUUID(userContext.getMeldingInformation().getUUID());

    information.getBenefactorInformation().setOrganisationName("AERIUS");
    information.getBenefactorInformation().setOrganisationContactPerson("Henk driéën Benefactor");
    information.getBenefactorInformation().setOrganisationAddress("Croeselaan 15");
    information.getBenefactorInformation().setOrganisationPostcode("1234 AB");
    information.getBenefactorInformation().setOrganisationCity("Utrecht");
    information.getBenefactorInformation().setOrganisationEmail("test1@aerius.nl");

    information.setBenefactorCustom(false);

    information.getBeneficiaryInformation().setOrganisationName("AERIUS");
    information.getBeneficiaryInformation().setOrganisationContactPerson("John driéën Beneficiary");
    information.getBeneficiaryInformation().setOrganisationAddress("Paardenhoflaan 69");
    information.getBeneficiaryInformation().setOrganisationPostcode("4321 AB");
    information.getBeneficiaryInformation().setOrganisationCity("Deventer");
    information.getBeneficiaryInformation().setOrganisationEmail("test2@aerius.nl");

    information.setBenefactorEmailConfirmation(true);

    userContext.setMeldingInformation(information);

    placeController.goTo(new MeldingInitiatorPlace());
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        placeController.goTo(new MeldingInitiatorPlace());
      }
    });
  }
}
