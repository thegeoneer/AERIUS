/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.MeldingUserContext;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.melding.place.MeldingPlace;

/**
 * Base class for Melding Activity classes.
 * @param <V> The view for the sub Activity class
 */
public abstract class BaseMeldingActivity<V extends Editor<MeldingInformation> & IsWidget>
extends AbstractActivity implements BaseMeldingView.Presenter {

  private final BaseMeldingView baseView;
  private final SimpleBeanEditorDriver<MeldingInformation, V> driver;
  protected final MeldingPlace place;
  protected final PlaceController placeController;
  protected final MeldingUserContext userContext;

  private boolean needsSave;
  private boolean checkBoxStatus;
  private boolean busyUploading;
  private String uploadFileName;

  @Inject
  protected BaseMeldingActivity(final BaseMeldingView baseView, final MeldingPlace place, final PlaceController placeController,
      final MeldingUserContext userContext, final SimpleBeanEditorDriver<MeldingInformation, V> driver) {
    this.baseView = baseView;
    this.place = place;
    this.placeController = placeController;
    this.userContext = userContext;
    this.driver = driver;
  }

  @Override
  public final void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    panel.setWidget(baseView);

    baseView.setPresenter(this);

    baseView.setNextEnabled(place.hasNextPlace());
    baseView.setPreviousEnabled(place.hasPreviousPlace());
    baseView.setNextButtonText(place.getProgressStep());
    baseView.setShowButtons(place.hasNextPlace() || place.hasPreviousPlace());
    baseView.setProgress(place.getProgressStep());

    final V contentView = getContentView();
    baseView.setWidget(contentView);
    if (driver != null) {
      final MeldingInformation meldingInformation = userContext.getMeldingInformation();

      driver.initialize(contentView);
      driver.edit(meldingInformation);
      needsSave = true;
    }
  }

  protected abstract V getContentView();

  @Override
  public String mayStop() {
    return busyUploading ? M.messages().meldingBusyUploadingConfirmIgnore(getUploadFileName())
        : driver != null && needsSave && driver.isDirty() ? M.messages().meldingConfirmIgnoreChanges() : null;
  }

  @Override
  public final void onStop() {
    saveCurrent();
  }

  @Override
  public final void onCancel() {
    saveCurrent();
  }

  private void saveCurrent() {
    needsSave = false;
  }

  @Override
  public final void goToPrevious() {
    needsSave = false;
    if (driver != null) {
      userContext.setMeldingInformation(driver.flush());
    }
    placeController.goTo(getPreviousPlace());
  }

  protected abstract Place getPreviousPlace();

  protected abstract Place getNextPlace();

  @Override
  public final void goToNext() {
    if (driver == null) {
      placeController.goTo(getNextPlace());
    } else {
      final MeldingInformation flush = driver.flush();

      if (driver.hasErrors()) {
        ErrorPopupController.addErrors(driver.getErrors());
        return;
      } else {
        needsSave = false;
        // Clear all errors.
        ErrorPopupController.clearWidgets();
        userContext.setMeldingInformation(flush);
        placeController.goTo(getNextPlace());
      }
    }
  }

  public boolean isCheckBoxStatus() {
    return checkBoxStatus;
  }

  public void setCheckBoxStatus(final boolean checkBoxStatus) {
    this.checkBoxStatus = checkBoxStatus;
  }

  public boolean isBusyUploading() {
    return busyUploading;
  }

  public void setBusyUploading(final boolean busyUploading) {
    this.busyUploading = busyUploading;
  }

  public String getUploadFileName() {
    return uploadFileName;
  }

  public void setUploadFileName(final String uploadFileName) {
    this.uploadFileName = uploadFileName;
  }
}
