/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.test.TestIDMelding;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.melding.widget.BeneficiaryInformationEditor;
import nl.overheid.aerius.wui.melding.widget.FileUploadPanel;
import nl.overheid.aerius.wui.melding.widget.OrganisationInformationEditor;

@Singleton
public class MeldingInitiatorViewImpl extends Composite implements MeldingInitiatorView {
  interface MeldingInitiatorViewImplUiBinder extends UiBinder<Widget, MeldingInitiatorViewImpl> {}

  private static final MeldingInitiatorViewImplUiBinder UI_BINDER = GWT.create(MeldingInitiatorViewImplUiBinder.class);

  public interface MeldingInformationDriver extends SimpleBeanEditorDriver<MeldingInformation, MeldingInitiatorViewImpl> {}

  @Ignore @UiField RadioButton benefactorCustom;
  LeafValueEditor<Boolean> benefactorCustomEditor = new LeafValueEditor<Boolean>() {
    @Override
    public void setValue(final Boolean value) {
      benefactorCustom.setValue(value, true);
    }

    @Override
    public Boolean getValue() {
      return benefactorCustom.getValue();
    }
  };

  @Ignore @UiField RadioButton executorCustom;
  LeafValueEditor<Boolean> executorCustomEditor = new LeafValueEditor<Boolean>() {
    @Override
    public void setValue(final Boolean value) {
      executorCustom.setValue(value, true);
    }

    @Override
    public Boolean getValue() {
      return executorCustom.getValue();
    }
  };

  @Ignore @UiField RadioButton benefactorSame;
  @UiField FlowPanel benefactorContainer;
  @UiField(provided = true) FileUploadPanel authorizationFilesEditor;

  @Ignore @UiField RadioButton executorSame;
  @UiField FlowPanel executorContainer;

  @UiField OrganisationInformationEditor benefactorInformation;
  @UiField BeneficiaryInformationEditor beneficiaryInformation;
  @UiField OrganisationInformationEditor executorInformation;

  @UiField CheckBox benefactorEmailConfirmation;
  @UiField CheckBox executorEmailConfirmation;

  public MeldingInitiatorViewImpl() {
    authorizationFilesEditor = new FileUploadPanel(M.messages().meldingAddAuthorizationFiles(), SharedConstants.IMPORT_FILE_FIELD_NAME, 2);
    initWidget(UI_BINDER.createAndBindUi(this));

    beneficiaryInformation.ensureDebugId(TestIDMelding.BENEFICIARY);

    benefactorSame.ensureDebugId(TestIDMelding.BENEFACTOR + TestIDMelding.SAME);
    benefactorCustom.ensureDebugId(TestIDMelding.BENEFACTOR + TestIDMelding.CUSTOM);
    benefactorInformation.ensureDebugId(TestIDMelding.BENEFACTOR);
    benefactorEmailConfirmation.ensureDebugId(TestIDMelding.BENEFACTOR + TestIDMelding.SEND_CONFIRMATION);
    authorizationFilesEditor.addValidator(new Validator<ArrayList<String>>() {
      @Override
      public String validate(final ArrayList<String> value) {
        return benefactorCustom.getValue() && authorizationFilesEditor.getValue().isEmpty()
            ? M.messages().meldingFilesRequired() : null;
      }
    });

    executorSame.ensureDebugId(TestIDMelding.EXECUTOR + TestIDMelding.SAME);
    executorCustom.ensureDebugId(TestIDMelding.EXECUTOR + TestIDMelding.CUSTOM);
    executorInformation.ensureDebugId(TestIDMelding.EXECUTOR);
    executorEmailConfirmation.ensureDebugId(TestIDMelding.EXECUTOR + TestIDMelding.SEND_CONFIRMATION);
  }

  @UiHandler({"benefactorSame", "benefactorCustom"})
  public void onBeneficiaryInformationSelectionChange(final ValueChangeEvent<Boolean> e) {
    final boolean benefactorEnabled = benefactorCustom.getValue();
    if (!benefactorEnabled) {
      ErrorPopupController.clearWidgets();
    }
    benefactorContainer.setVisible(benefactorEnabled);
    benefactorInformation.setEditorEnabled(benefactorEnabled);
    authorizationFilesEditor.setEditorEnabled(benefactorEnabled);
  }

  @UiHandler({"executorSame", "executorCustom"})
  public void onExecutorInformationSelectionChange(final ValueChangeEvent<Boolean> e) {
    final boolean executorEnabled = executorCustom.getValue();
    if (!executorEnabled) {
      ErrorPopupController.clearWidgets();
    }
    executorContainer.setVisible(executorEnabled);
    executorInformation.setEditorEnabled(executorEnabled);
  }

  @UiHandler("benefactorConfirmationCheckBoxLabel")
  public void onBeneficiaryConfirmationCheckBoxLabelClick(final ClickEvent e) {
    benefactorEmailConfirmation.setValue(!benefactorEmailConfirmation.getValue(), true);

  }

  @UiHandler("executorConfirmationCheckBoxLabel")
  public void onExecutorConfirmationCheckBoxLabelClick(final ClickEvent e) {
    executorEmailConfirmation.setValue(!executorEmailConfirmation.getValue(), true);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    authorizationFilesEditor.setFileUploadActions(presenter);
  }
}
