# Vagrant - aerius-database-devbox

## Setting up the devbox
- Install Vagrant.
- Open the command prompt and go to the Vagrant folder (the folder where the Vagrantfile is located).
- Run `vagrant up`.
- After vagrant up is completed, login using username `vagrant` and password `vagrant` and enjoy developing!

## Devbox info
Project directory is located at `/home/vagrant/project-data/`.

Scripts:
- db-data sync: `ruby ../aerius-database-build/bin/SyncDBData.rb settings.rb --from-sftp --to-local`
- test-structure: `ruby ../aerius-database-build/bin/Build.rb test_structure.rb settings.rb -v structure`
- build database: `ruby ../aerius-database-build/bin/Build.rb default settings.rb -v#`

## Known issues / todo's
- vboxadd-service.service (VirtualBox guest tools) could not be installed properly (yet) and could not be started at boot.
- Find a better way to detect that Vagrant provisioning has been done.

## Useful links
- Installing Vagrant - https://www.vagrantup.com/docs/installation/index.html
- Debugging Vagrant - https://www.vagrantup.com/docs/other/debugging.html