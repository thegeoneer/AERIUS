/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.Event;

import nl.overheid.aerius.shared.domain.auth.Permission;
import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.context.AdminUserContext;
import nl.overheid.aerius.wui.main.context.AppContext;
import nl.overheid.aerius.wui.main.util.NotificationUtil;

/**
 * Activity implementation that optionally does a permission check.
 * If access is not allowed it will request to go to the default place.
 *
 * Also contains some convenience methods that a lot of the activities use regularly.
 */
public abstract class AdminAbstractActivity<C extends AppContext<? extends AdminContext, ? extends AdminUserContext>> extends AbstractActivity {

  protected C appContext;
  protected EventBus eventBus;
  private Permission permissionNeeded;

  /**
   * No checks constructor.
   */
  public AdminAbstractActivity(final C appContext) {
    this.appContext = appContext;
  }

  /**
   * Permission check constructor.
   */
  public AdminAbstractActivity(final C appContext, final Permission permissionNeeded) {
    this(appContext);

    this.permissionNeeded = permissionNeeded;
  }

  @Override
  public final void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    if (isAllowedToStart()) {
      this.eventBus = eventBus;

      start(panel);
    } else {
      GWT.log("Not allowed to be in current activity - redirecting to default place");

      Scheduler.get().scheduleFinally(new ScheduledCommand() {
        @Override
        public void execute() {
          appContext.goToDefaultPlace();
        }
      });
    }
  }

  public abstract void start(AcceptsOneWidget panel);

  protected void goTo(final Place place) {
    appContext.getPlaceController().goTo(place);
  }

  protected void broadcastNotificationMessage(final String message) {
    NotificationUtil.broadcastMessage(eventBus, message);
  }

  protected void fireEvent(final Event<?> event) {
    eventBus.fireEvent(event);
  }

  private boolean isAllowedToStart() {
    return permissionNeeded == null || appContext.getUserContext().getUserProfile().hasPermission(permissionNeeded);
  }

}
