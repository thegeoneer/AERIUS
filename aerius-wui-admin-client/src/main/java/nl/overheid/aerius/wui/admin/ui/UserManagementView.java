/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.ui;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.wui.admin.widget.UserManagementEditor;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;

public interface UserManagementView extends IsWidget, Editor<UserProfile>, IsInteractiveDataTable<AdminUserProfile>, HasTitle {

  interface Presenter {

    void editUser(AdminUserProfile user);

    void removeUser(AdminUserProfile profile);

    void createUser(AdminUserProfile value);

    void onAdaptFilter();

    void resetPassword();

    void submitUserEdit();

    AdminUserProfile getDefaultUserProfile();
  }

  void setPresenter(UserManagementView.Presenter presenter);

  void createUser(AdminUserProfile profile);

  void userCreateComplete(boolean success);

  UserManagementEditor getCreationEditor();

  UserManagementEditor getModificationEditor();

}
