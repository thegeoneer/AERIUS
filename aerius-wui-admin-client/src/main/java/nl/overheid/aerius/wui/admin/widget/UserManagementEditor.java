/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.widget;

import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserRole;
import nl.overheid.aerius.shared.test.TestIDAdmin;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.ui.validation.EmailValidator;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.widget.LabelledWidget;
import nl.overheid.aerius.wui.main.widget.MultiSelectListBox;
import nl.overheid.aerius.wui.main.widget.SelectOption;
import nl.overheid.aerius.wui.main.widget.SelectTextOptionFactory;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public class UserManagementEditor extends Composite implements Editor<AdminUserProfile> {

  interface UserManagementEditorUiBinder extends UiBinder<Widget, UserManagementEditor> {}

  private static final UserManagementEditorUiBinder UI_BINDER = GWT.create(UserManagementEditorUiBinder.class);

  @UiField LabelledWidget usernameContainer;
  @UiField TextValueBox name;
  @UiField TextValueBox initials;
  @UiField TextValueBox firstName;
  @UiField TextValueBox lastName;
  @UiField(provided = true) ListBoxEditor<Authority> authority;
  @UiField TextValueBox emailAddress;
  @UiField CheckBox enabled;

  @Ignore @UiField MultiSelectListBox<AdminUserRole> roles;
  final LeafValueEditor<HashSet<UserRole>> rolesEditor = new LeafValueEditor<HashSet<UserRole>>() {
    @Override
    public void setValue(final HashSet<UserRole> value) {
      roles.setValue(convertToAdminRole(value));
    }

    @Override
    public HashSet<UserRole> getValue() {
      return convertToUserRole(roles.getValue());
    }
  };
  @UiField(provided = true) WidgetFactory<AdminUserRole, SelectOption<AdminUserRole>> roleOptionTemplate = new SelectTextOptionFactory<AdminUserRole>() {
    @Override
    public String getItemText(final AdminUserRole value) {
      return value.getName();
    }
  };

  private final AdminContext adminContext;

  @Inject
  public UserManagementEditor(final AdminContext adminContext) {
    this.adminContext = adminContext;

    authority = new ListBoxEditor<Authority>() {
      @Override
      protected String getLabel(final Authority value) {
        return value.getDescription();
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    name.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().adminUsersUsernameValidator();
      }
    });
    initials.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().adminUsersInitialsValidator();
      }
    });
    firstName.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().adminUsersFirstNameValidator();
      }
    });
    lastName.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().adminUsersLastNameValidator();
      }
    });
    emailAddress.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().adminUsersEmailAddressValidator();
      }
    });
    emailAddress.addValidator(new EmailValidator());
  }

  @Override
  protected void onLoad() {
    authority.clear();
    roles.deselect();
    roles.clear();
    authority.addItems(adminContext.getAuthorities());
    roles.setValues(adminContext.getRoles());
  }

  @Override
  protected void onUnload() {
    authority.clear();
    roles.deselect();
    roles.clear();
  }

  public void setUsernameEnabled(final boolean enable) {
    name.setEnabled(enable);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    name.ensureDebugId(baseID + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT_NAME);
    initials.ensureDebugId(baseID + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT_INITIALS);
    firstName.ensureDebugId(baseID + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT_FIRSTNAME);
    lastName.ensureDebugId(baseID + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT_LASTNAME);
    authority.ensureDebugId(baseID + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT_ORGANISATION);
    emailAddress.ensureDebugId(baseID + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT_EMAIL);
    roles.ensureDebugId(baseID + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT_ROLES);
    enabled.ensureDebugId(baseID + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT_ENABLED);
  }

  private HashSet<AdminUserRole> convertToAdminRole(final HashSet<UserRole> roles) {
    final HashSet<AdminUserRole> convert = new HashSet<AdminUserRole>();

    for (final UserRole role : roles) {
      convert.add(role.copyTo(new AdminUserRole()));
    }

    return convert;
  }

  private HashSet<UserRole> convertToUserRole(final HashSet<AdminUserRole> roles) {
    final HashSet<UserRole> convert = new HashSet<UserRole>();

    for (final UserRole role : roles) {
      convert.add(role.copyTo(new UserRole()));
    }

    return convert;
  }
}
