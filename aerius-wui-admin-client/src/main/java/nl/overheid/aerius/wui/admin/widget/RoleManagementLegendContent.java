/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.user.AdminUserRole;

public abstract class RoleManagementLegendContent extends Composite {
  interface RoleManagementLegendContentUiBinder extends UiBinder<Widget, RoleManagementLegendContent> {}

  private static final RoleManagementLegendContentUiBinder UI_BINDER = GWT.create(RoleManagementLegendContentUiBinder.class);

  @UiField(provided = true) RoleManagementEditor editor;

  @Inject
  public RoleManagementLegendContent(final AdminUserRole object) {
    editor = new RoleManagementEditor();
    editor.setValue(object);

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @UiHandler("buttonCancel")
  public void onCancelClick(final ClickEvent e) {
    onCancel();
  }

  @UiHandler("buttonSubmit")
  public void onSubmitClick(final ClickEvent e) {
    onSubmit(editor.getValue());
  }

  abstract void onCancel();

  abstract void onSubmit(AdminUserRole value);
}
