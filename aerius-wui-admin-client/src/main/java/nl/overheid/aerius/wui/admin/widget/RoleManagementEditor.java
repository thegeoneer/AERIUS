/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.widget.ColorPicker;

public class RoleManagementEditor extends Composite implements LeafValueEditor<AdminUserRole> {
  interface RoleManagementLegendTableUiBinder extends UiBinder<Widget, RoleManagementEditor> {}

  private static final RoleManagementLegendTableUiBinder UI_BINDER = GWT.create(RoleManagementLegendTableUiBinder.class);

  @UiField TextValueBox roleName;
  @UiField ColorPicker roleColor;

  private AdminUserRole value;

  public RoleManagementEditor() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public void setValue(final AdminUserRole value) {
    this.value = value;

    roleName.setValue(value.getName());
    roleColor.setSelectedValue(value.getColor(), true);
  }

  @Override
  public AdminUserRole getValue() {
    value.setColor(roleColor.getValue());
    value.setName(roleName.getValue());

    return value;
  }
}
