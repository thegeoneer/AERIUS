/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.Place;

import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.wui.admin.place.RoleManagementPlace;
import nl.overheid.aerius.wui.admin.place.UserManagementPlace;
import nl.overheid.aerius.wui.admin.ui.RoleManagementActivity;
import nl.overheid.aerius.wui.admin.ui.UserManagementActivity;
import nl.overheid.aerius.wui.main.AsyncActivityFactory;
import nl.overheid.aerius.wui.main.AsyncActivityMapper;
import nl.overheid.aerius.wui.main.context.AppContext;
import nl.overheid.aerius.wui.main.util.AppRunAsyncCallback;

/**
 */
public class AdminActivityMapper<F extends AdminActivityFactory, A extends AppContext<C, UC>, C extends Context, UC extends UserContext> extends AsyncActivityMapper<F, A, C, UC> {
  public AdminActivityMapper(final F factory, final A context) {
    super(factory, context);
  }

  @Override
  public Activity getActivity(final Place place) {
    Activity presenter;

    if (place instanceof UserManagementPlace) {
      presenter = factory.createAsyncActivity(new AsyncActivityFactory() {
        @Override
        public Activity createActivity() {
          return factory.createUserManagementActivity((UserManagementPlace) place);
        }

        @Override
        public void doRunAsync() {
          GWT.runAsync(UserManagementActivity.class, new AppRunAsyncCallback() {
            @Override
            public void onSuccess() {
              sideloadActivity();
            }
          });
        }

      });
    } else if (place instanceof RoleManagementPlace) {
      presenter = factory.createAsyncActivity(new AsyncActivityFactory() {
        @Override
        public Activity createActivity() {
          return factory.createRoleManagementActivity((RoleManagementPlace) place);
        }

        @Override
        public void doRunAsync() {
          GWT.runAsync(RoleManagementActivity.class, new AppRunAsyncCallback() {
            @Override
            public void onSuccess() {
              sideloadActivity();
            }
          });
        }

      });
    } else {
      presenter = null;
    }

    return presenter;
  }
}
