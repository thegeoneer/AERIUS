/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.wui.admin.widget.RoleManagementDataTable;
import nl.overheid.aerius.wui.admin.widget.RoleManagementLegendTable;
import nl.overheid.aerius.wui.admin.widget.RoleManagementLegendTable.RoleChangeHandler;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.table.SelectionModelConnector;

/**
 * View for User management
 */
@Singleton
public class RoleManagementViewImpl extends Composite implements RoleManagementView, RoleChangeHandler {
  interface RoleManagementViewImplUiBinder extends UiBinder<Widget, RoleManagementViewImpl> {}

  private static final RoleManagementViewImplUiBinder UI_BINDER = GWT.create(RoleManagementViewImplUiBinder.class);

  @UiField(provided = true) RoleManagementDataTable roleTable;
  @UiField(provided = true) RoleManagementLegendTable roleLegend;

  private Presenter presenter;

  @Inject
  public RoleManagementViewImpl(final RoleManagementDataTable roleTable, final RoleManagementLegendTable roleLegend) {
    this.roleTable = roleTable;
    this.roleLegend = roleLegend;
    roleLegend.setPresenter(this);

    initWidget(UI_BINDER.createAndBindUi(this));

    final SelectionModelConnector<AdminUserRole> connector = new SelectionModelConnector<AdminUserRole>();
    connector.connect(roleTable);
    connector.connect(roleLegend);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public String getTitleText() {
    return M.messages().adminMenuRoleManagementTitle();
  }

  @Override
  public void submit(final AdminUserRole role) {
    // TODO Fetch and update changed permissions.

    presenter.save(role);
  }

  @Override
  public void refresh() {
    roleTable.refresh();
    roleLegend.refresh();
  }
}
