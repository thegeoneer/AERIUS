/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.ui;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.auth.AdminPermission;
import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.context.AdminUserContext;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.wui.admin.context.AdminAppContext;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;

public class RoleManagementActivity extends AdminAbstractActivity<AdminAppContext<AdminContext, AdminUserContext>>
    implements RoleManagementView.Presenter {

  private final RoleManagementView view;

  private final AsyncCallback<Void> updateRoleCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      appContext.getContext().getRoles().remove(updatedRole);
      appContext.getContext().getRoles().add(updatedRole);

      view.refresh();
    }
  };

  private AdminUserRole updatedRole;

  @Inject
  public RoleManagementActivity(final RoleManagementView view, final AdminAppContext<AdminContext, AdminUserContext> appContext) {
    super(appContext, AdminPermission.ADMINISTER_USERS);

    this.view = view;
  }

  @Override
  public void start(final AcceptsOneWidget panel) {
    panel.setWidget(view);

    view.setPresenter(this);
  }

  @Override
  public void save(final AdminUserRole role) {
    this.updatedRole = role;

    appContext.getAdminService().updateRole(role, updateRoleCallback);
  }
}
