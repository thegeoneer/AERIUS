/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.melding;

public class BeneficiaryInformation extends OrganisationInformation {
  private static final long serialVersionUID = -2276046063006948579L;
  private boolean correspondenceCustom;
  private String correspondenceAddress;
  private String correspondencePostcode;
  private String correspondenceCity;

  public boolean isCorrespondenceCustom() {
    return correspondenceCustom;
  }

  public void setCorrespondenceCustom(final boolean correspondenceCustom) {
    this.correspondenceCustom = correspondenceCustom;
  }

  public String getCorrespondenceAddress() {
    return correspondenceAddress;
  }

  public void setCorrespondenceAddress(final String correspondenceAddress) {
    this.correspondenceAddress = correspondenceAddress;
  }

  public String getCorrespondencePostcode() {
    return correspondencePostcode;
  }

  public void setCorrespondencePostcode(final String correspondencePostcode) {
    this.correspondencePostcode = correspondencePostcode;
  }

  public String getCorrespondenceCity() {
    return correspondenceCity;
  }

  public void setcorrespondenceCity(final String correspondenceCity) {
    this.correspondenceCity = correspondenceCity;
  }

  @Override
  public String toString() {
    return "BeneficiaryInformation [organisationName=" + getOrganisationName() + ", organisationContactPerson="
        + getOrganisationContactPerson() + ", organisationAddress=" + getOrganisationAddress() + ", organisationPostcode="
        + getOrganisationPostcode() + ", organisationCity=" + getOrganisationCity() + ", organisationEmail=" + getOrganisationEmail()
        + ", correspondenceCustom=" + correspondenceCustom + ", correspondenceAddress=" + correspondenceAddress + ", correspondencePostcode="
        + correspondencePostcode + ", correspondenceCity=" + correspondenceCity + "]";
  }
}
