/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;

/**
 *
 */
public class IndicativeReceptorInfo implements Serializable {

  private static final long serialVersionUID = -5114299487088009263L;

  private AeriusPoint receptor;
  private float maxFractionUsed;
  private HashMap<AssessmentArea, ArrayList<HabitatType>> assessmentAreas = new HashMap<>();

  public AeriusPoint getReceptor() {
    return receptor;
  }

  public void setReceptor(final AeriusPoint receptor) {
    this.receptor = receptor;
  }

  public float getMaxFractionUsed() {
    return maxFractionUsed;
  }

  public void setMaxFractionUsed(final float maxFractionUsed) {
    this.maxFractionUsed = maxFractionUsed;
  }

  public HashMap<AssessmentArea, ArrayList<HabitatType>> getAssessmentAreas() {
    return assessmentAreas;
  }

  public void setAssessmentAreas(final HashMap<AssessmentArea, ArrayList<HabitatType>> assessmentAreas) {
    this.assessmentAreas = assessmentAreas;
  }

}
