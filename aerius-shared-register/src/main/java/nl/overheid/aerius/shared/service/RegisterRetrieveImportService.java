/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.exception.AeriusException;

@RemoteServiceRelativePath("aerius-retrieve-register-import")
public interface RegisterRetrieveImportService extends RemoteService {

  String UUID_PREFIX_PP = "register_pp_";
  String UUID_PREFIX_PP_SUBPROJECT = "register_pp_subproject_";
  String UUID_PREFIX_PP_FACTSHEET = "register_pp_factsheet_";
  String UUID_PREFIX_PP_ACTUALISATION = "register_pp_actualisation_";
  String UUID_PREFIX_PERMIT = "register_permit_";

  /**
   * Returns the result of a Permit import that is stored in the user's session.
   * @throws AeriusException if there was an error while trying to import
   */
  PermitKey getPermitResult(String key) throws AeriusException;

  /**
   * Returns the result of a PriorityProject import that is stored in the user's session.
   * @throws AeriusException if there was an error while trying to import
   */
  PriorityProjectKey getPriorityProjectResult(String key) throws AeriusException;

  /**
   * Returns the result of a PrioritySubProject import that is stored in the user's session.
   * @throws AeriusException if there was an error while trying to import
   */
  PrioritySubProjectKey getPrioritySubProjectResult(String key) throws AeriusException;

  /**
   * Returns the result of a Priority Project Factsheet import that is stored in the user's session.
   * There is no real result though (null). This call can be used to fetch the error if present.
   * @throws AeriusException if there was an error while trying to import
   */
  PriorityProjectKey getPriorityProjectFactsheetResult(String key) throws AeriusException;

  /**
   * Returns the result of a Priority Project Actualisation import that is stored in the user's session.
   * There is no real result though (null). This call can be used to fetch the error if present.
   * @throws AeriusException if there was an error while trying to import
   */
  PriorityProjectKey getPriorityProjectActualisationResult(String key) throws AeriusException;
}
