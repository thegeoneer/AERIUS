/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.EmissionValues;

/**
 * A situation in a permit.
 */
public class RequestSituation implements Serializable {

  private static final long serialVersionUID = -6835174634419295852L;

  private String name;
  private EmissionValues totalEmissionValues;

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public EmissionValues getTotalEmissionValues() {
    return totalEmissionValues;
  }

  public void setTotalEmissionValues(final EmissionValues totalEmissionValues) {
    this.totalEmissionValues = totalEmissionValues;
  }
}
