/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

/**
 * Enum with priority project progress states.
 */
public enum PriorityProjectProgress {

  NO_RESERVATION_PRESENT(), NONE(Double.NaN, 0.0), UNDER_HALF(0.0, 0.5), OVER_HALF(0.5, 1.0), FULL(1.0, Double.NaN), DONE();

  private final double lowerLimit;
  private final double higherLimit;

  PriorityProjectProgress() {
    this(Double.NaN, Double.NaN);
  }

  PriorityProjectProgress(final double lowerLimit, final double higherLimit) {
    this.lowerLimit = lowerLimit;
    this.higherLimit = higherLimit;
  }

  public double getLowerLimit() {
    return lowerLimit;
  }

  public double getHigherLimit() {
    return higherLimit;
  }

  public boolean hasLowerLimit() {
    return !Double.isNaN(lowerLimit);
  }

  public boolean hasHigherLimit() {
    return !Double.isNaN(higherLimit);
  }

  public static PriorityProjectProgress determineProgress(final PriorityProject object) {
    final PriorityProjectProgress progress;
    if (object == null) {
      progress = null;
    } else if (object.getRequestState() == RequestState.INITIAL)  {
      progress = NO_RESERVATION_PRESENT;
    } else if (object.isAssignCompleted()) {
      progress = DONE;
    } else {
      progress = fractionedProgress(object.getFractionAssigned());
    }
    return progress;
  }

  private static PriorityProjectProgress fractionedProgress(final double fractionAssigned) {
    final PriorityProjectProgress progress;
    if (fractionAssigned >= FULL.getLowerLimit()) {
      progress = FULL;
    } else if (fractionAssigned > OVER_HALF.getLowerLimit()) {
      progress = OVER_HALF;
    } else if (fractionAssigned > UNDER_HALF.getLowerLimit()) {
      progress = UNDER_HALF;
    } else {
      progress = NONE;
    }
    return progress;
  }
}
