/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

/**
 * Setting for authorities: how to handle confirmations of No-Permit-Notices.
 */
public enum NoticeConfirmationSetting {

  /**
   * No-permit-notices should be automatically confirmed.
   */
  AUTO_CONFIRM,
  /**
   * No-permit-notices can be confirmed in one big batch.
   */
  BATCH_CONFIRM,
  /**
   * No-permit-notices have to be manually confirmed, 1 by 1.
   */
  ONE_BY_ONE_CONFIRM;

  /**
   * Safely returns a NoticeConfirmationSetting. It is case independent and returns the default (AUTO_CONFIRM) in
   * case the input was null or the NoticeConfirmationSetting could not be found.
   *
   * @param value value to convert
   * @return The proper NoticeConfirmationSetting.
   */
  public static NoticeConfirmationSetting safeValueOf(final String value) {
    //default value = auto confirm.
    NoticeConfirmationSetting returnValue = NoticeConfirmationSetting.AUTO_CONFIRM;
    if (value != null) {
      try {
        returnValue = valueOf(value.toUpperCase());
      } catch (final IllegalArgumentException e) {
        //safeValueOf
      }
    }
    return returnValue;
  }

}
