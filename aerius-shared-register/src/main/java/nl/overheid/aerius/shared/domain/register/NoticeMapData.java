/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class to store map data for notices.
 */
public class NoticeMapData implements Serializable {

  private static final long serialVersionUID = -4663988528633443296L;

  private ArrayList<Notice> notices;
  private int noticeId;

  public NoticeMapData() {
  }

  public NoticeMapData(final ArrayList<Notice> notices) {
    setNotices(notices);
  }

  public ArrayList<Notice> getNotices() {
    return notices;
  }
  public void setNotices(final ArrayList<Notice> notices) {
    this.notices = notices;
  }
  public int getNoticeId() {
    return noticeId;
  }
  public void setNoticeId(final int noticeId) {
    this.noticeId = noticeId;
  }

}
