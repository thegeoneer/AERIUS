/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;

public class MeldingUserContext implements UserContext, Serializable {
  private static final long serialVersionUID = -8453838807287170750L;

  private MeldingInformation meldingInformation = new MeldingInformation();

  @Override
  public EmissionValueKey getEmissionValueKey() {
    return null;
  }

  @Override
  public LayerProps getBaseLayer() {
    return null;
  }

  @Override
  public ArrayList<LayerProps> getLayers() {
    return null;
  }

  @Override
  public EmissionResultValueDisplaySettings getEmissionResultValueDisplaySettings() {
    return null;
  }

  public void setMeldingInformation(final MeldingInformation meldingInformation) {
    this.meldingInformation = meldingInformation;
  }

  /**
   * @return the meldingInformation
   */
  public MeldingInformation getMeldingInformation() {
    return meldingInformation;
  }
}
