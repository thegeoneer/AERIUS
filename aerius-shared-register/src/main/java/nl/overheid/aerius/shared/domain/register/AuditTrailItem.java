/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import nl.overheid.aerius.shared.domain.user.UserProfile;

/**
 * Contains changes the application had to endure(!).
 */
public class AuditTrailItem implements Serializable {

  private static final long serialVersionUID = -2125232601672054897L;

  private long id;
  private Date date;
  private UserProfile editedBy;
  private SegmentType segment;
  private String reference;

  private ArrayList<AuditTrailChange> items = new ArrayList<AuditTrailChange>();

  // Default constructor as needed by GWT.
  public AuditTrailItem() {
  }

  /**
   * Convenience constructor to set almost everything at once.
   * @param date The date
   * @param editedBy The user who made the changes.
   * @param items The changes made
   */
  public AuditTrailItem(final Date date, final UserProfile editedBy, final ArrayList<AuditTrailChange> items) {
    this.date = date;
    this.editedBy = editedBy;
    this.items = items;
  }

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(final Date date) {
    this.date = date;
  }

  public ArrayList<AuditTrailChange> getItems() {
    return items;
  }

  public void setItems(final ArrayList<AuditTrailChange> items) {
    this.items = items;
  }

  public UserProfile getEditedBy() {
    return editedBy;
  }

  public void setEditedBy(final UserProfile editedBy) {
    this.editedBy = editedBy;
  }

  public SegmentType getSegment() {
    return segment;
  }

  public void setSegment(final SegmentType segment) {
    this.segment = segment;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(final String reference) {
    this.reference = reference;
  }

  @Override
  public String toString() {
    return "AuditTrailItem [id=" + id + ", date=" + date + ", editedBy=" + editedBy + ", segment=" + segment + ", reference=" + reference
        + ", items=" + items + "]";
  }
}
