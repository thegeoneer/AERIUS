/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.export;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;

/**
 *
 */
public class PriorityProjectExportData extends ExportData {

  private static final long serialVersionUID = 4028123165660379268L;

  /**
   * Type of data to export for a priority project and it's subprojects.
   */
  public enum PriorityProjectExportType {
    /**
     * GML files containing results and/or sources.
     */
    GML_FILES,
    /**
     * The PDF files ('bijlage bij berekening' and 'bijlage bij besluit' files).
     */
    PDF_FILES,
    /**
     * A ZIP file consisting of the reservation GML, a GML containing the sum of all the assigned subprojects (being the utilisation) and 
     *  per subproject the result GML file because we like being verbose.
     */
    UTILISATION_FILES;
  }

  private PriorityProjectKey priorityProjectKey;
  private String priorityProjectReference;
  private ArrayList<PriorityProjectExportType> exportTypes = new ArrayList<>();

  public PriorityProjectExportData() {
    getAdditionalOptions().add(ExportAdditionalOptions.RETURN_FILE);
    getAdditionalOptions().remove(ExportAdditionalOptions.EMAIL_USER);
  }

  public PriorityProjectKey getPriorityProjectKey() {
    return priorityProjectKey;
  }

  public void setPriorityProjectKey(final PriorityProjectKey priorityProjectKey) {
    this.priorityProjectKey = priorityProjectKey;
  }

  public ArrayList<PriorityProjectExportType> getExportTypes() {
    return exportTypes;
  }

  public void setExportTypes(final ArrayList<PriorityProjectExportType> exportTypes) {
    this.exportTypes = exportTypes;
  }

  public void setPriorityProjectReference(final String reference) {
    this.priorityProjectReference = reference;
  }

  public String getPriorityProjectReference() {
    return priorityProjectReference;
  }
}
