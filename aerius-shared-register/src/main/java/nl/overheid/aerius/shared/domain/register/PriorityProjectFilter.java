/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

/**
 * Filter for the priority project overview page.
 */
public class PriorityProjectFilter extends RequestFilter<PriorityProjectSortableAttribute> {

  private static final long serialVersionUID = 4146867118057445835L;

  private PriorityProjectProgress progressState;

  public PriorityProjectProgress getProgressState() {
    return progressState;
  }

  public void setProgressState(final PriorityProjectProgress progressState) {
    this.progressState = progressState;
  }

  @Override
  public void fillDefault() {
    super.fillDefault();

    progressState = null;
  }

  @Override
  public String toString() {
    return "PriorityProjectFilter [progressState=" + progressState + "] " + super.toString();
  }
}
