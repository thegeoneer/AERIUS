/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.melding;

import java.io.Serializable;

public class OrganisationInformation implements Serializable {
  private static final long serialVersionUID = 118667551918518773L;

  private String organisationName;
  private String organisationContactPerson;
  private String organisationAddress;
  private String organisationPostcode;
  private String organisationCity;
  private String organisationEmail;

  /**
   * @return the organisationName
   */
  public String getOrganisationName() {
    return organisationName;
  }

  /**
   * @return the organisationAddress
   */
  public String getOrganisationAddress() {
    return organisationAddress;
  }

  /**
   * @return the organisationPostcode
   */
  public String getOrganisationPostcode() {
    return organisationPostcode;
  }

  /**
   * @return the organisationCity
   */
  public String getOrganisationCity() {
    return organisationCity;
  }

  /**
   * @return the organisationEmail
   */
  public String getOrganisationEmail() {
    return organisationEmail;
  }

  /**
   * @return the getOrganisationContactPerson
   */
  public String getOrganisationContactPerson() {
    return organisationContactPerson;
  }

  /**
   * @param organisationContactPerson the organisationContactPerson to set
   */
  public void setOrganisationContactPerson(final String organisationContactPerson) {
    this.organisationContactPerson = organisationContactPerson;
  }

  /**
   * @param organisationName the organisationName to set
   */
  public void setOrganisationName(final String organisationName) {
    this.organisationName = organisationName;
  }

  /**
   * @param organisationAddress the organisationAddress to set
   */
  public void setOrganisationAddress(final String organisationAddress) {
    this.organisationAddress = organisationAddress;
  }

  /**
   * @param organisationPostcode the organisationPostcode to set
   */
  public void setOrganisationPostcode(final String organisationPostcode) {
    this.organisationPostcode = organisationPostcode;
  }

  /**
   * @param organisationCity the organisationCity to set
   */
  public void setOrganisationCity(final String organisationCity) {
    this.organisationCity = organisationCity;
  }

  /**
   * @param organisationEmail the organisationEmail to set
   */
  public void setOrganisationEmail(final String organisationEmail) {
    this.organisationEmail = organisationEmail;
  }

  @Override
  public String toString() {
    return "OrganisationInformation [organisationName=" + organisationName + ", organisationContactPerson="
        + organisationContactPerson + ", organisationAddress=" + organisationAddress + ", organisationPostcode="
        + organisationPostcode + ", organisationCity=" + organisationCity + ", organisationEmail=" + organisationEmail
        + "]";
  }
}
