/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.io.Serializable;
import java.util.Date;

import nl.overheid.aerius.shared.domain.user.UserProfile;

/**
 * Dossier metadata that can be updated by the authority.
 */
public class DossierMetaData implements Serializable {

  private static final long serialVersionUID = -3701873354537464690L;

  private String dossierId; // I didn't make the String part up
  private Date receivedDate;
  private UserProfile handler;
  private String remarks;

  public String getDossierId() {
    return dossierId;
  }

  public void setDossierId(final String dossierId) {
    this.dossierId = dossierId;
  }

  public Date getReceivedDate() {
    return receivedDate;
  }

  public void setReceivedDate(final Date receivedDate) {
    this.receivedDate = receivedDate;
  }

  public UserProfile getHandler() {
    return handler;
  }

  public void setHandler(final UserProfile handler) {
    this.handler = handler;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(final String remarks) {
    this.remarks = remarks;
  }

  @Override
  public String toString() {
    return "DossierMetaData [dossierId=" + dossierId + ", receivedDate=" + receivedDate + ", handler=" + handler
        + ", remarks=" + remarks + "]";
  }
}
