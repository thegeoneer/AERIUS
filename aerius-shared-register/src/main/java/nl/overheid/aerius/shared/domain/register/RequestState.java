/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

/**
 * Contains all the states a request can have.
 */
public enum RequestState {
  /**
   * A request's initial state. The PDF is imported and the calculation is being performed.
   * Once complete, it will immediately move on to QUEUED.
   */
  INITIAL,

  /**
   * The request is on the queue (and will remain there until the time period expires).
   */
  QUEUED,

  /**
   * The request is out of the queue and now pending, and when it was dequeued there was enough
   * development room.
   */
  PENDING_WITH_SPACE,

  /**
   * The request is out of the queue and now pending, and when it was dequeued there was NOT
   * enough development room.
   */
  PENDING_WITHOUT_SPACE,

  /**
   * The development space for the request has been assigned.
   */
  ASSIGNED,

  /**
   * The request was rejected when it was on PENDING_WITHOUT_SPACE: no space is actually reserved.
   */
  REJECTED_WITHOUT_SPACE,

  /**
   * The assigned state of the request has been irrevocably approved.
   */
  ASSIGNED_FINAL,

  /**
   * The request has been irrevocably rejected.
   * Note that this is a UI enum value only, it does not exists in the database enum. The user
   * can select it, but the result is the complete deletion of the request. So the state is
   * not persisted.
   */
  REJECTED_FINAL;

  /**
   * All states available for priority subprojects.
   */
  public static final RequestState[] PRIORITY_PROJECT_VALUES = { QUEUED, ASSIGNED, ASSIGNED_FINAL, REJECTED_WITHOUT_SPACE, REJECTED_FINAL };

  /**
   * All states for which the permit summary / review info tab is available.
   */
  public static final RequestState[] PERMIT_SUMMARY_VALUES = { QUEUED, PENDING_WITH_SPACE, PENDING_WITHOUT_SPACE };

  /**
   * All states for which the priority subproject review info tab is available.
   */
  public static final RequestState[] PRIORITY_SUBPROJECT_REVIEW_VALUES = { QUEUED };

  /**
   * All states when a subproject is considered complete.
   */
  public static final RequestState[] PRIORITY_SUBPROJECT_IS_COMPLETED = { REJECTED_FINAL, ASSIGNED_FINAL };

  /**
   * All states when a priority project and subproject is considered assigned for utilisation export.
   */
  public static final RequestState[] PRIORITY_PROJECT_UTILISATION_EXPORT_VALUES = { ASSIGNED, ASSIGNED_FINAL };

  /**
   * All states that can be filtered on. In practice this is everything except REJECTED_FINAL, as that state is
   * immediately followed by a delete.
   */
  public static final RequestState[] FILTER_VALUES = { INITIAL, QUEUED, PENDING_WITH_SPACE, PENDING_WITHOUT_SPACE,
      ASSIGNED, REJECTED_WITHOUT_SPACE, ASSIGNED_FINAL };

  /**
   * All states when the DECREE_DOWNLOAD_STATUS is available.
   */
  public static final RequestState[] DECREE_DOWNLOAD_STATUS = { ASSIGNED, REJECTED_WITHOUT_SPACE, ASSIGNED_FINAL, REJECTED_FINAL };

  /**
   * Safely returns a requestState. It is case independent and returns null in
   * case the input was null or the RequestState could not be found.
   *
   * @param value value to convert
   * @return requestState or null if no valid input
   */
  public static RequestState safeValueOf(final String value) {
    if (value == null) {
      return null;
    }
    try {
      return valueOf(value.toUpperCase());
    } catch (final Exception e) {
      return null;
    }
  }

  public String getDbValue() {
    return name().toLowerCase();
  }

}
