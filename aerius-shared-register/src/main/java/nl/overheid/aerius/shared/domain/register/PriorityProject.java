/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class to store a priority project and related data.
 */
public class PriorityProject extends Request {

  private static final long serialVersionUID = 2956513199731531858L;

  private DossierMetaData dossierMetaData;
  private double fractionAssigned;
  private boolean assignCompleted;

  private IndicativeReceptorInfo indicativeReceptorInfo;

  private ArrayList<PrioritySubProject> subProjects = new ArrayList<>();

  public PriorityProject() {
    super(SegmentType.PRIORITY_PROJECTS);
  }

  public PriorityProjectKey getKey() {
    return getAuthority() == null ? null : new PriorityProjectKey(dossierMetaData.getDossierId(), getAuthority().getCode());
  }

  public boolean hasNoPendingSubProjects() {
    boolean result = true;

    for (final PrioritySubProject subProject : getSubProjects()) {
      if (!Arrays.asList(RequestState.PRIORITY_SUBPROJECT_IS_COMPLETED).contains(subProject.getRequestState())) {
        result = false;
        break;
      }
    }

    return result;
  }

  public DossierMetaData getDossierMetaData() {
    return dossierMetaData;
  }

  public void setDossierMetaData(final DossierMetaData dossierMetaData) {
    this.dossierMetaData = dossierMetaData;
  }

  public double getFractionAssigned() {
    return fractionAssigned;
  }

  public void setFractionAssigned(final double fractionAssigned) {
    this.fractionAssigned = fractionAssigned;
  }

  public boolean isAssignCompleted() {
    return assignCompleted;
  }

  public void setAssignCompleted(final boolean assignCompleted) {
    this.assignCompleted = assignCompleted;
  }

  public IndicativeReceptorInfo getIndicativeReceptorInfo() {
    return indicativeReceptorInfo;
  }

  public void setIndicativeReceptorInfo(final IndicativeReceptorInfo indicativeReceptorInfo) {
    this.indicativeReceptorInfo = indicativeReceptorInfo;
  }

  public ArrayList<PrioritySubProject> getSubProjects() {
    return subProjects;
  }

  public void setSubProjects(final ArrayList<PrioritySubProject> subProjects) {
    this.subProjects = subProjects;
  }

  @Override
  public String toString() {
    return "PriorityProject [dossierMetaData=" + dossierMetaData + "] " + super.toString();
  }

}
