/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.io.Serializable;

/**
 * Used to identify (database) objects using a reference string (instead of an id).
 */
public class ReferenceKey implements Serializable {

  private static final long serialVersionUID = 6673351805576716768L;

  private String reference;

  protected ReferenceKey() { /* GWT-needed constructor */
  }

  public ReferenceKey(final String reference) {
    this.reference = reference;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(final String reference) {
    this.reference = reference;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((reference == null) ? 0 : reference.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    boolean equals = false;
    if (obj != null && this.getClass() == obj.getClass()) {
      final ReferenceKey other = (ReferenceKey) obj;
      equals = ((this.reference != null && this.reference.equals(other.reference))
          || (this.reference == null && other.reference == null));
    }
    return equals;
  }

  @Override
  public String toString() {
    return "ReferenceKey [reference=" + reference + "]";
  }

}
