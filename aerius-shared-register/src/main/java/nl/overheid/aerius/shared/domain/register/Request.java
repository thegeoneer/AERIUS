/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.user.Authority;

/**
 * Class to store a permit and related data.
 */
public class Request implements Serializable, HasId {

  private static final long serialVersionUID = 9015454292232820576L;

  private int id;
  private SegmentType segment;
  private Point point;
  private int startYear;
  private Integer temporaryPeriod;
  private RequestState requestState;
  private ScenarioMetaData scenarioMetaData;
  private Sector sector;
  private boolean multipleSectors;
  private boolean marked;
  private Authority authority;
  private Province province;
  private String applicationVersion;
  private String databaseVersion;
  private long lastModified;
  private HashMap<RequestFileType, DownloadRequestFile> files = new HashMap<>();

  private HashMap<SituationType, RequestSituation> situations = new HashMap<>();

  private ArrayList<AuditTrailItem> changeHistory = new ArrayList<>();

  protected Request() { /* GWT-needed constructor */
  }

  public Request(final SegmentType segment) {
    this.segment = segment;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  public Point getPoint() {
    return point;
  }

  public void setPoint(final Point point) {
    this.point = point;
  }

  public int getStartYear() {
    return startYear;
  }

  public void setStartYear(final int targetYear) {
    this.startYear = targetYear;
  }

  public Integer getTemporaryPeriod() {
    return temporaryPeriod;
  }

  public void setTemporaryPeriod(final Integer temporaryPeriod) {
    this.temporaryPeriod = temporaryPeriod;
  }

  public ScenarioMetaData getScenarioMetaData() {
    return scenarioMetaData;
  }

  public void setScenarioMetaData(final ScenarioMetaData scenarioMetaData) {
    this.scenarioMetaData = scenarioMetaData;
  }

  public RequestState getRequestState() {
    return requestState;
  }

  public void setRequestState(final RequestState requestState) {
    this.requestState = requestState;
  }

  public ArrayList<AuditTrailItem> getChangeHistory() {
    return changeHistory;
  }

  public void setChangeHistory(final ArrayList<AuditTrailItem> changeHistory) {
    this.changeHistory = changeHistory;
  }

  public String getReference() {
    return getScenarioMetaData() == null ? null : getScenarioMetaData().getReference();
  }

  /**
   * Convenience method for old time sake.
   * @return The sector icon for this request.
   */
  public SectorIcon getSectorIcon() {
    return sector == null ? null : sector.getProperties().getIcon();
  }

  public Sector getSector() {
    return sector;
  }

  public void setSector(final Sector sector) {
    this.sector = sector;
  }

  public boolean isMultipleSectors() {
    return multipleSectors;
  }

  public void setMultipleSectors(final boolean multipleSectors) {
    this.multipleSectors = multipleSectors;
  }

  public Authority getAuthority() {
    return authority;
  }

  public void setAuthority(final Authority authority) {
    this.authority = authority;
  }

  public Province getProvince() {
    return province;
  }

  public void setProvince(final Province province) {
    this.province = province;
  }

  public String getApplicationVersion() {
    return applicationVersion;
  }

  public void setApplicationVersion(final String applicationVersion) {
    this.applicationVersion = applicationVersion;
  }

  public String getDatabaseVersion() {
    return databaseVersion;
  }

  public void setDatabaseVersion(final String databaseVersion) {
    this.databaseVersion = databaseVersion;
  }

  public long getLastModified() {
    return lastModified;
  }

  public void setLastModified(final long lastModified) {
    this.lastModified = lastModified;
  }

  public HashMap<RequestFileType, DownloadRequestFile> getFiles() {
    return files;
  }

  public void setFiles(final HashMap<RequestFileType, DownloadRequestFile> files) {
    this.files = files;
  }

  public HashMap<SituationType, RequestSituation> getSituations() {
    return situations;
  }

  public void setSituations(final HashMap<SituationType, RequestSituation> situations) {
    this.situations = situations;
  }

  /**
   * Convenience method to get the proposed emission values for the request.
   * @return The proposed emission values.
   */
  public EmissionValues getProposedEmissionValues() {
    return situations.containsKey(SituationType.PROPOSED) ? situations.get(SituationType.PROPOSED).getTotalEmissionValues()
        : new EmissionValues();
  }

  public boolean hasRequestFile(final RequestFileType requestFileType) {
    return files.containsKey(requestFileType);
  }

  public String getUrl(final RequestFileType requestFileType) {
    return hasRequestFile(requestFileType) ? files.get(requestFileType).getUrl() : null;
  }

  public SegmentType getSegment() {
    return segment;
  }

  public void setSegment(final SegmentType segment) {
    this.segment = segment;
  }

  public boolean isMarked() {
    return marked;
  }

  public void setMarked(final boolean marked) {
    this.marked = marked;
  }

  @Override
  public String toString() {
    return "Request [id=" + id + ", point=" + point + ", startYear=" + startYear + ", temporaryPeriod="
        + temporaryPeriod + ", requestState=" + requestState + ", scenarioMetaData=" + scenarioMetaData + ", sector="
        + sector + ", multipleSectors=" + multipleSectors + ", marked=" + marked + ", applicationVersion="
        + applicationVersion + ", databaseVersion=" + databaseVersion + ", lastModified=" + lastModified + ", changeHistory="
        + changeHistory + ", files=" + files + ", situations=" + situations + "]";
  }

}
