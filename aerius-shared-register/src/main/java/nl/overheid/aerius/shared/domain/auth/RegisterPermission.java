/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.auth;

/**
 * Authorization options for the Register Application.
 *
 * These permissions' names _must not_ deviate from the values in the database (or the getName() method must return an appropriate value).
 */
public enum RegisterPermission implements Permission {


  /**
   * Can view dashboard (main menu item visible).
   */
  VIEW_DASHBOARD,
  /**
   * Can view permits (main menu item visible).
   */
  VIEW_PERMITS,
  /**
   * Can add a new permit (segment 2).
   */
  ADD_NEW_PERMIT,

  /**
   * Can view priority projects (main menu item visible).
   */
  VIEW_PRIORITY_PROJECTS,

  /**
   * Can add a new priority project (segment 1, reservation - used for debugging reasons).
   */
  ADD_NEW_PRIORITY_PROJECT_RESERVATION,

  /**
   * Can add a new priority project (segment 1, actualisation).
   */
  ADD_NEW_PRIORITY_PROJECT,

  /**
   * Can add a new priority sub project (segment 1, actual permit).
   */
  ADD_NEW_PRIORITY_SUBPROJECT,

  /**
   * Can update the application date.
   */
  UPDATE_PERMIT_DATE,
  /**
   * Can update the application treator.
   */
  UPDATE_PERMIT_HANDLER,
  /**
   * Can update the application dossier number.
   */
  UPDATE_PERMIT_DOSSIER_NUMBER,
  /**
   * Can update the application remarks.
   */
  UPDATE_PERMIT_REMARKS,

  /**
   * Can mark and unmark a permit.
   */
  UPDATE_PERMIT_MARK,

  /**
   * Can change the state of the permit. (Without this permission, state can not be changed at all.)
   * It covers only regular permit state changes. Additional permissions for special cases are defined below.
   */
  UPDATE_PERMIT_STATE,
  /**
   * Can change permit state from 'queued' to 'pending with/without space'. Requires UPDATE_PERMIT_STATE.
   */
  UPDATE_PERMIT_STATE_DEQUEUE,
  /**
   * Can change permit state from 'pending with/without space' to 'queued'. Requires UPDATE_PERMIT_STATE.
   */
  UPDATE_PERMIT_STATE_ENQUEUE,
  /**
   * Can change permit state from 'rejected (without space)' to 'queued'. Requires UPDATE_PERMIT_STATE.
   */
  UPDATE_PERMIT_STATE_ENQUEUE_REJECTED_WITHOUT_SPACE,
  /**
   * Can irrevocably reject a permit and also deletes the permit. Requires UPDATE_PERMIT_STATE. Does *not* require DELETE_PERMIT.
   */
  UPDATE_PERMIT_STATE_REJECT_FINAL,

  /**
   * Can delete a permit with state INITIAL and no running calculations.
   */
  DELETE_PERMIT_INACTIVE_INITIAL,
  /**
   * Can delete a permit, always and always. 'Superuser version' - without restriction.
   */
  DELETE_PERMIT_ALL,

  /**
   * Can update properties of priority project.
   */
  UPDATE_PRIORITY_PROJECT,
  /**
   * Can delete a priority project and all of its underlying subprojects with assigned space.
   */
  DELETE_PRIORITY_PROJECT,
  /**
   * Can revert assign complete flag of priority project.
   */
  REVERT_PRIORITY_PROJECT_ASSIGN_COMPLETE,

  /**
   * Can change the state of the priority subproject. (Without this permission, state can not be changed at all.)
   * It covers only regular priority subproject state changes. Additional permissions for special cases are defined below.
   */
  UPDATE_PRIORITY_SUBPROJECT_STATE,
  /**
   * Can change priority subproject state from 'irrevocably assigned' to 'rejected'. Requires UPDATE_PERMIT_STATE.
   */
  UPDATE_PRIORITY_SUBPROJECT_STATE_ASSIGNED_FINAL_TO_REJECTED,
  /**
   * Can irrevocably reject a priority subproject and also deletes the priority subproject. Requires UPDATE_PRIORITY_SUBPROJECT_STATE.
   * Does *not* require DELETE_PRIORITY_SUBPROJECT.
   */
  UPDATE_PRIORITY_SUBPROJECT_STATE_REJECT_FINAL,
  /**
   * Can delete a priority subproject and subtract its assigned space.
   */
  DELETE_PRIORITY_SUBPROJECT,

  /**
   * Can exceed the total OR reservation for priority projects when adding a new priority project or assigning a priority subproject.
   * This does NOT mean that the subproject can exceed the priority projects' reservation.
   */
  OVERRIDE_PRIORITY_PROJECTS_RESERVATION,

  /**
   * Can view meldingen (main menu item visible).
   */
  VIEW_PRONOUNCEMENTS,

  /**
   * Can confirm a melding.
   */
  CONFIRM_PRONOUNCEMENT,
  /**
   * Can delete a melding.
   */
  DELETE_PRONOUNCEMENT,

  /**
   * Can export requests.
   */
  EXPORT_REQUESTS;

  @Override
  public String getName() {
    return name().toLowerCase();
  }
}
