/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

/**
 *
 */
public class DownloadRequestFile extends RequestFile {

  private static final long serialVersionUID = 9156737415714546540L;

  private String url;

  public DownloadRequestFile() {
    //NO-OP, needed for GWT
  }

  public DownloadRequestFile(final RequestFile requestFile, final String url) {
    setFileName(requestFile.getFileName());
    setFileFormat(requestFile.getFileFormat());
    setRequestFileType(requestFile.getRequestFileType());
    this.url = url;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }
}
