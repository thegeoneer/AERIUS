/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.io.Serializable;

/**
 * Contains a single change that is done to an application.
 */
public class AuditTrailChange implements Serializable {

  private static final long serialVersionUID = -484495787667282807L;

  private AuditTrailType type;
  private String oldValue;
  private String newValue;

  // Default constructor as needed by GWT.
  public AuditTrailChange() {
  }

  public String getOldValue() {
    return oldValue;
  }

  public void setOldValue(final String oldValue) {
    this.oldValue = oldValue;
  }

  public String getNewValue() {
    return newValue;
  }

  public void setNewValue(final String newValue) {
    this.newValue = newValue;
  }

  public AuditTrailType getType() {
    return type;
  }

  public void setType(final AuditTrailType type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "AuditTrailChange [type=" + type + ", oldValue=" + oldValue + ", newValue=" + newValue + "]";
  }
}
