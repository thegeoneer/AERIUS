/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.auth;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.subject.Subject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link NetIQFilter}.
 */
public class NetIQFilterTest {

  private NetIQFilter filter;
  private HttpServletRequest request;
  private ServletResponse response;
  private ResultSet rsUsers;
  private ResultSet rsUsersView;

  @Before
  public void before() throws SQLException {
    request = mock(HttpServletRequest.class);
    response = mock(ServletResponse.class);
    final Connection con = mock(Connection.class);
    final PreparedStatement preparedStatement = mock(PreparedStatement.class);
    final PreparedStatement ppUsers = mock(PreparedStatement.class);
    final PreparedStatement ppUsersView = mock(PreparedStatement.class);
    doAnswer(new Answer<PreparedStatement>() {
      @Override
      public PreparedStatement answer(final InvocationOnMock invocation) throws Throwable {
        final String param1 = invocation.getArgumentAt(0, String.class);
        return param1.contains(" users ")
            ? ppUsers : param1.contains("users_view")
                ? ppUsersView : preparedStatement;
      }
    }).when(con).prepareStatement(anyString());
    final ResultSet resultSet = mockResultSet(preparedStatement);
    rsUsers = mockResultSet(ppUsers);
    rsUsersView = mockResultSet(ppUsersView);
    final Subject subject = mock(Subject.class);
    filter = new NetIQFilter() {
      @Override
      protected Connection getDBConnection() throws SQLException {
        return con;
      };

      @Override
      protected Subject getSubject(final ServletRequest request, final ServletResponse response) {
        return subject;
      }

      @Override
      public boolean onAccessDenied(final ServletRequest request, final ServletResponse response) throws Exception {
        return super.onAccessDenied(request, response);
      }

      @Override
      public boolean isAccessAllowed(final ServletRequest request, final ServletResponse response, final Object mappedValue) {
        return super.isAccessAllowed(request, response, mappedValue);
      }
    };
  }

  @Test(expected = AeriusException.class)
  public void testOnAccessDeniedNoEmail() throws Exception {
    filter.onAccessDenied(request, response);
    fail("No e-mail should throw error");
  }

  @Test(expected = AeriusException.class)
  public void testOnAccessDeniedUserUnknown() throws Exception {
    when(request.getHeader("X-Mail")).thenReturn("test123");
    filter.onAccessDenied(request, response);
    fail("Expected an exception");
  }

  @Test
  public void testOnAccessDeniedOk() throws Exception {
    when(request.getHeader("X-Mail")).thenReturn("test123");
    mockLoopResultSet(rsUsers, 1);
    mockLoopResultSet(rsUsersView, 1);
    when(rsUsersView.getString("email_address")).thenReturn("test123");
    when(rsUsersView.getBoolean("enabled")).thenReturn(Boolean.TRUE);
    assertTrue("User should be allowed", filter.onAccessDenied(request, response));
  }

  private ResultSet mockResultSet(final PreparedStatement preparedStatement) throws SQLException {
    final ResultSet resultSet = mock(ResultSet.class);
    when(preparedStatement.executeQuery()).thenReturn(resultSet);
    return resultSet;
  }

  private void mockLoopResultSet(final ResultSet resultSet, final int max) throws SQLException {
    doAnswer(new Answer<Boolean>() {
      private int cnt;
      @Override
      public Boolean answer(final InvocationOnMock invocation) throws Throwable {
        return cnt++ < max;
      }
    }).when(resultSet).next();
  }

}
