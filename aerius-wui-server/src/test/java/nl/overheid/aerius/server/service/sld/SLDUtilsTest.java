/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service.sld;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

/**
 *
 */
public class SLDUtilsTest {

  private static final String USED_EXTENSION = ".xml";
  private static final String SIMPLE_SLD = "simple_sld";
  private static final String RULES_SLD = "rules_in_sld";
  private static final String RULES_ZOOMLVLS_SLD = "rules_and_zoomlevels_in_sld";
  private static final String INCORRECT_RULE_SLD = "incorrect_rule_sld";

  @Test
  public void testSimplestSLD() throws IOException {
    final WMSLayer layer = new WMSLayer(1, 1, "aeirus:someLayer");
    final String sldContent = SLDUtils.getAsSLD(layer, "http://www.test.com/testapp/");
    testContent(sldContent, SIMPLE_SLD);
  }

  @Test
  public void testSLDWithZoomLevels() throws IOException {
    //zoom levels have no effect without rules...
    final WMSLayer layer = new WMSLayer(1, 1, "aeirus:someLayer");
    addZoomLevels(layer);
    final String sldContent = SLDUtils.getAsSLD(layer, "http://www.test.com/testapp/");
    testContent(sldContent, SIMPLE_SLD);
  }

  @Test
  public void testSLDWithRules() throws IOException {
    final WMSLayer layer = new WMSLayer(1, 1, "aeirus:someLayer");
    addRules(layer);
    final String sldContent = SLDUtils.getAsSLD(layer, "http://www.test.com/testapp/");
    testContent(sldContent, RULES_SLD);
  }

  @Test
  public void testSLDWithIncorrectRules() throws IOException {
    final WMSLayer layer = new WMSLayer(1, 1, "aeirus:someLayer");
    //the condition (2nd param) is being parsed. This one can't be parsed correctly
    //tbh there should be something done about the colors too... but ah well...
    layer.addRule(new SLDRule(99, "someweirdcond", "nocolors", "reallynocolors"));
    final String sldContent = SLDUtils.getAsSLD(layer, "http://www.test.com/testapp/");
    testContent(sldContent, INCORRECT_RULE_SLD);
  }

  @Test
  public void testSLDWithRulesAndZoomLevels() throws IOException {
    final WMSLayer layer = new WMSLayer(1, 1, "aeirus:someLayer");
    addRules(layer);
    addZoomLevels(layer);
    final String sldContent = SLDUtils.getAsSLD(layer, "http://www.test.com/testapp/");
    testContent(sldContent, RULES_ZOOMLVLS_SLD);
  }

  private void testContent(final String generatedContent, final String correspondingFile) throws IOException {
    assertNotNull("Content shouldn't be null", generatedContent);
    assertEquals("Content", getFileContent(correspondingFile), generatedContent);
  }

  private void addZoomLevels(final WMSLayer layer) {
    layer.addZoomLevel(new WMSZoomLevel(1, 100, 200, 1));
    layer.addZoomLevel(new WMSZoomLevel(2, 200, 1000, 2));
    layer.addZoomLevel(new WMSZoomLevel(3, 1000, 10000, 3));
  }

  private void addRules(final WMSLayer layer) {
    layer.addRule(new SLDRule("someImg.png"));
    layer.addRule(new SLDRule("<sld:LineSymbolizer><sld:Stroke><sld:CssParameter name=\"stroke\">#001122</sld:CssParameter></sld:Stroke></sld:LineSymbolizer>",
        "<ogc:PropertyIsNull><ogc:PropertyName>someProperty</ogc:PropertyName></ogc:PropertyIsNull>"));
    layer.addRule(new SLDRule(1, "something <= 4", "123456", "654321"));
    layer.addRule(new SLDRule(2, "somethingElse = 8", "000000", "FFFFFF", null, null, null));
  }

  private String getFileContent(final String fileName) throws IOException {
    final File file = new File(getClass().getResource(fileName + USED_EXTENSION).getFile());
    final StringBuilder result = new StringBuilder();
    try (final BufferedReader br = new BufferedReader(new FileReader(file))) {
      String line;

      while ((line = br.readLine()) != null) {
        result.append(line);
      }
    }
    return result.toString();
  }

}
