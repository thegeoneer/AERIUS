/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.HttpClientManager;

/**
 * Util class to call GetCapabilities on remote server.
 */
public final class GeoGetCapabilitiesUtil {

  private static final Logger LOG = LoggerFactory.getLogger(GeoGetCapabilitiesUtil.class);

  private static final String GET_CAPABILITIES_VERSION_1_1_1 = "service=wms&version=1.1.1&request=GetCapabilities";
  private static final int DEFAULT_TIMEOUT = 10; // in seconds

  private GeoGetCapabilitiesUtil() {
    // util class
  }

  public static String fetchCapabilities(final String url) throws AeriusException {
    final String wmsProxyUrl;
    final String internalWmsProxyUrl;

    try (final Connection con = ServerPMF.getInstance().getConnection()) {
      wmsProxyUrl = ConstantRepository.getString(con, SharedConstantsEnum.SHARED_WMS_PROXY_URL);
      internalWmsProxyUrl = ConstantRepository.getString(con, SharedConstantsEnum.INTERNAL_WMS_PROXY_URL);
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "GeoGetCapabilitiesUtil fetching constant failed");
    }

    final String urlToProxy = wmsProxyUrl.equals(url) ? internalWmsProxyUrl : url;

    try (CloseableHttpClient client = HttpClientManager.getHttpClient(DEFAULT_TIMEOUT)) {
      final String capabilitiesUrl = getCompleteUrl(urlToProxy);
      LOG.debug("Proxying this: {} with: {}", urlToProxy, capabilitiesUrl);

      return new String(HttpClientProxy.getRemoteContent(client, capabilitiesUrl));
    } catch (final IOException | ParseException | HttpException | URISyntaxException e) {
      LOG.error("GeoProxyFilter filter failed:", e);
      throw new AeriusException(Reason.FAULTY_REQUEST);
    }
  }

  private static String getCompleteUrl(final String capabilitiesUrl) {
    // FIXME This behavior is inconsistent; some of these url's are postfixed with the queryString indicator automatically, which is why we need to check for it.
    return capabilitiesUrl + (capabilitiesUrl.contains("?") ? "" : '?') + GET_CAPABILITIES_VERSION_1_1_1;
  }
}
