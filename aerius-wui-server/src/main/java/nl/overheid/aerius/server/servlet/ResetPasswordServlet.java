/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.crypto.hash.format.Shiro1CryptFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.i18n.AeriusMessages;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.MailTo;
import nl.overheid.aerius.mail.MessageTaskClient;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.user.PasswordResetUtil;
import nl.overheid.aerius.user.PasswordUtil;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.util.SaltDigestUtil;

/**
 * Servlet to reset a user's password.
 */
@WebServlet(urlPatterns = { "/resetPassword" })
public class ResetPasswordServlet extends HttpServlet {
  private static final Logger LOG = LoggerFactory.getLogger(ResetPasswordServlet.class);

  private static final long serialVersionUID = -6523163500055798637L;

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    final String urlEncodedHash = request.getParameter("conf");
    final String combinedHash = URLDecoder.decode(urlEncodedHash, StandardCharsets.UTF_8.toString());
    final String email = SaltDigestUtil.extractSalt(combinedHash);

    try (final Connection con = ServerPMF.getInstance().getConnection()) {
      final String hash = PasswordResetUtil.generateHash(email, UserRepository.getUserPasswordHashed(con, email));

      if (hash.equals(combinedHash)) {
        final String password = PasswordUtil.createPassword();

        // Encrypt the password using shiro's default password service
        final DefaultPasswordService dps = new DefaultPasswordService();
        dps.setHashFormat(new Shiro1CryptFormat());
        final String encryptedPassword = dps.encryptPassword(password);

        UserRepository.updateUserPassword(con, email, encryptedPassword);

        //compose a mail and send it to the user
        final Locale locale = LocaleUtils.getDefaultLocale();
        final MailMessageData messageData = new MailMessageData(MessagesEnum.PASSWORD_RESET_CONFIRM_SUBJECT,
            MessagesEnum.PASSWORD_RESET_CONFIRM_BODY, locale, new MailTo(email));
        messageData.setReplacement(ReplacementToken.PASSWORD_RESET_PLAIN_TEXT_NEW, password);

        MessageTaskClient.startMessageTask(TaskClientFactory.getInstance(), messageData);

        setSuccess(request);
      } else {
        setStaleError(request);
      }
    } catch (final AeriusException e) {
      LOG.error("Could not find user.", e);
      setError(request);
    } catch (final NoSuchAlgorithmException e) {
      LOG.error("Could not find digest algorithm.", e);
      throw new ServletException(e);
    } catch (final SQLException e) {
      LOG.error("SQL Exception occurred.", e);
      setError(request);
    }

    request.getRequestDispatcher("WEB-INF/jsp/misc/message.jsp").forward(request, response);
  }

  private void setSuccess(final HttpServletRequest request) {
    request.setAttribute("messageTitle", getMessages(request).getString("passwordForgottenResetSuccesfulTitle"));
    request.setAttribute("messageBody", getMessages(request).getString("passwordForgottenResetSuccesfulText"));
  }

  private void setError(final HttpServletRequest request) {
    request.setAttribute("messageTitle", getMessages(request).getString("passwordForgottenResetErrorTitle"));
    request.setAttribute("messageBody", getMessages(request).getString("passwordForgottenResetErrorText"));
    request.setAttribute("isError", true);
  }

  private void setStaleError(final HttpServletRequest request) {
    request.setAttribute("messageTitle", getMessages(request).getString("passwordForgottenResetErrorStaleTitle"));
    request.setAttribute("messageBody", getMessages(request).getString("passwordForgottenResetErrorStaleText"));
    request.setAttribute("isError", true);
  }

  private AeriusMessages getMessages(final HttpServletRequest request) {
    return new AeriusMessages(Internationalizer.getLocaleFromCookie(request));
  }
}
