/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.i18n.AeriusMessages;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.MailTo;
import nl.overheid.aerius.mail.MessageTaskClient;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.constants.Constants;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.user.PasswordResetUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Servlet to help users who forgot their passwords.
 */
@WebServlet(urlPatterns = { "/forgotten" })
public class ForgottenServlet extends HttpServlet {

  private static final Logger LOG = LoggerFactory.getLogger(ForgottenServlet.class);
  private static final long serialVersionUID = -6523163500055798637L;

  private static final String PARAM_EMAIL = "email";

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    request.getRequestDispatcher("WEB-INF/jsp/auth/forgotten.jsp").forward(request, response);
  }

  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    final String email = request.getParameter(PARAM_EMAIL);
    if (StringUtils.isEmpty(email)) {
      // Act like we couldn't send the mail. This will hint the user that the email address might be wrong.
      setError(request, "passwordForgottenEmailSentErrorTitle", "passwordForgottenEmailSentErrorText");
    } else {
      try (final Connection con = ServerPMF.getInstance().getConnection()) {
        final String hash = PasswordResetUtil.generateHash(email, UserRepository.getUserPasswordHashed(con, email));
        final String urlEncodedHash = URLEncoder.encode(hash, StandardCharsets.UTF_8.toString());

        // Compose mail
        final String resetUrl = Constants.getString(SharedConstantsEnum.AERIUS_HOST) + request.getContextPath() + "/resetPassword?conf="
            + urlEncodedHash;

        // Compose a mail and send it to the user
        final Locale locale = LocaleUtils.getDefaultLocale();

        final MailMessageData messageData = new MailMessageData(MessagesEnum.PASSWORD_RESET_REQUEST_SUBJECT,
            MessagesEnum.PASSWORD_RESET_REQUEST_BODY, locale, new MailTo(email));
        messageData.setReplacement(ReplacementToken.PASSWORD_RESET_URL, resetUrl);

        MessageTaskClient.startMessageTask(TaskClientFactory.getInstance(), messageData);
        setSuccess(request);
      } catch (final AeriusException e) {
        // Act like we couldn't send the mail. This will hint the user that the email address might be wrong.
        setError(request, "passwordForgottenEmailSentErrorTitle", "passwordForgottenEmailSentErrorText");
        LOG.error("Could not get user password.", e);
      } catch (final NoSuchAlgorithmException e) {
        LOG.error("Could not find digest algorithm.");
        setError(request);
      } catch (final SQLException e) {
        LOG.error("SQL Exception occurred.", e);
        setError(request);
      }
    }

    request.getRequestDispatcher("/WEB-INF/jsp/misc/message.jsp").forward(request, response);
  }

  private void setSuccess(final HttpServletRequest request) {
    request.setAttribute("messageTitle", getMessages(request).getString("passwordForgottenEmailSentTitle"));
    request.setAttribute("messageBody", getMessages(request).getString("passwordForgottenEmailSentText"));
  }

  private void setError(final HttpServletRequest request) {
    setError(request, "passwordForgottonErrorGenericTitle", "passwordForgottonErrorGenericText");
  }

  private void setError(final HttpServletRequest request, final String titleKey, final String bodyKey) {
    request.setAttribute("messageTitle", getMessages(request).getString(titleKey));
    request.setAttribute("messageBody", getMessages(request).getString(bodyKey));
    request.setAttribute("isError", true);
  }

  private AeriusMessages getMessages(final HttpServletRequest request) {
    return new AeriusMessages(Internationalizer.getLocaleFromCookie(request));
  }
}
