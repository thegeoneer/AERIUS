/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;

abstract class AbstractDownloadServlet extends HttpServlet {

  private static final long serialVersionUID = -5662624966478443323L;

  protected static final String MIMETYPE_PDF = "application/pdf";
  protected static final String MIMETYPE_ZIP = "application/zip";
  protected static final String MIMETYPE_GML = "application/gml";

  protected abstract Logger getLogger();

  protected void internalError(final HttpServletResponse response) {
    errorOccured(
        response,
        HttpStatus.SC_INTERNAL_SERVER_ERROR,
        "Internal error",
        "Internal error occurred.");
  }

  protected void badRequestError(final HttpServletResponse response) {
    errorOccured(
        response,
        HttpStatus.SC_BAD_REQUEST,
        "Bad request",
        "Bad request");
  }

  protected void missingRequiredParameter(final HttpServletResponse response) {
    getLogger().error("Bad link or guessing attempt? Required parameter is not present.");
    badRequestError(response);
  }

  protected void errorOccured(final HttpServletResponse response, final int statusCode, final String messageToUser, final String logMessage,
      final Object ... logArgs) {
    try {
      response.sendError(statusCode, messageToUser);
    } catch (final IOException e) {
      // Eat error.
    }

    getLogger().warn(logMessage, logArgs);
  }

  protected void writeMessageToUser(final HttpServletResponse response, final String messageToUser, final String logMessage,
      final Object ... logArgs) {
    try {
      response.getWriter().println(messageToUser);
    } catch (final IOException e) {
      // Eat error.
    }
    getLogger().warn(logMessage, logArgs);
  }
}
