/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service.sld;

/**
 * An entity representing a row in the table system.sld_rule.
 * <p>
 * {@link #customConditionSLD} will contain the full XML version of the rule part and will
 *  override {@link #condition} if specified.
 * {@link #customDrawSLD} will contain the full XML version of the draw part (symbolizer and content)
 *  and will override {@link #fillColor} and {@link #strokeColor} if specified.
 *  <p>
 *  If there custom members are not specified, the condition and draw part will be generated based on {@link #rule},
 *   {@link #fillColor} and {@link #strokeColor}.
 */
public class SLDRule {

  private final int sldRuleId;
  private final String condition;
  private final String fillColor;
  private final String strokeColor;
  private final String imageURL;
  private final String customDrawSLD;
  private final String customConditionSLD;

  /**
   * @param sldRuleId The database ID of this rule.
   * @param condition The rule from which to generate the rule part of.
   * Will be overridden by {@link #customConditionSLD} if specified.
   * @param fillColor The fill color to use when generating the draw part.
   * Will be overridden by {@link #customDrawSLD}/{@link #imageURL} if specified.
   * @param strokeColor The color to use for the borders.
   * @param imageURL The URL to the image.
   * Will be overridden by {@link #customDrawSLD} if specified. Should be relative to the webapp.
   * @param customDrawSLD The custom XML used in the draw part for this rule in the SLD.
   * It should contain the symbolizer + content..
   * @param customConditionSLD The custom XML used in the rule condition for this rule in the SLD.
   * It should contain the content of the ogc:Filter tag.
   */
  public SLDRule(final int sldRuleId, final String condition, final String fillColor, final String strokeColor, final String imageURL,
      final String customDrawSLD, final String customConditionSLD) {
    this.sldRuleId = sldRuleId;
    this.condition = condition;
    this.fillColor = fillColor;
    this.strokeColor = strokeColor;
    this.imageURL = imageURL;
    this.customDrawSLD = customDrawSLD;
    this.customConditionSLD = customConditionSLD;
  }

  SLDRule(final int sldRuleId, final String condition, final String fillColor, final String strokeColor) {
    this(sldRuleId, condition, fillColor, strokeColor, null, null, null);
  }

  SLDRule(final String customDrawSLD, final String customConditionSLD) {
    this(0, null, null, null, null, customDrawSLD, customConditionSLD);
  }

  SLDRule(final String imageURL) {
    this(0, null, null, null, imageURL, null, null);
  }

  public String getCondition() {
    return condition;
  }

  public String getFillColor() {
    return fillColor;
  }

  public String getStrokeColor() {
    return strokeColor;
  }

  public String getCustomDrawSLD() {
    return customDrawSLD;
  }

  public String getCustomConditionSLD() {
    return customConditionSLD;
  }

  public int getSldRuleId() {
    return sldRuleId;
  }

  public String getImageURL() {
    return imageURL;
  }

}
