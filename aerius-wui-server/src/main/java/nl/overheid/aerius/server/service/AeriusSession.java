/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.util.Locale;

import javax.servlet.http.HttpSession;

/**
 * An AeriusSession provides access to request/session based parameters in a type-safe way without exposing the request or session objects.
 */
public interface AeriusSession {
  /**
   * @return The Locale as found in a cookie, or the default Locale.
   */
  Locale getLocale();

  /**
   * Closes the underlying session if it exists.
   */
  void close();

  /**
   * @return The current session.
   */
  HttpSession getSession();

}
