/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service.sld;

/**
 * An entity representing a row in the table system.wmszoomlevel.
 */
public class WMSZoomLevel {

  private final int wmsZoomLevelId;
  private final int minScale;
  private final int maxScale;
  private final int zoomLevel;

  /**
   * @param wmsZoomLevelId The database zoom level SET ID.
   * @param minScale The minimum scale for this level.
   * @param maxScale The maximum scale for this level.
   * @param zoomLevel The zoom level.
   */
  public WMSZoomLevel(final int wmsZoomLevelId, final int minScale, final int maxScale, final int zoomLevel) {
    this.wmsZoomLevelId = wmsZoomLevelId;
    this.minScale = minScale;
    this.maxScale = maxScale;
    this.zoomLevel = zoomLevel;
  }

  public int getWmsZoomLevelId() {
    return wmsZoomLevelId;
  }

  public int getMinScale() {
    return minScale;
  }

  public int getMaxScale() {
    return maxScale;
  }

  public int getZoomLevel() {
    return zoomLevel;
  }

}
