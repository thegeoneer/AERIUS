/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service.sld;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.constants.Constants;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;

/**
 * A plain HttpServlet to retrieve custom SLD's to be used with the WMS server.
 */
@WebServlet("/aerius/sld")
public class SLDServlet extends HttpServlet {

  private static final long serialVersionUID = -8822028314436197223L;

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(SLDServlet.class);

  // The layer key parameter.
  private static final String PARAMETER_LAYERKEY = "layer";

  @Override
  public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
    final String layerKey = request.getParameter(PARAMETER_LAYERKEY);

    // Validate the parameters.
    if (StringUtils.isEmpty(layerKey)) {
      errorOccured(
          response,
          "Bad request",
          "Bad link or guessing attempt? layerKey: " + layerKey);
      return;
    }

    try (final Connection con = ServerPMF.getInstance().getConnection()) {
      final WMSLayer wmsLayer = SLDService.getWMSLayer(con, layerKey);
      if (wmsLayer == null) {
        errorOccured(
            response,
            "Layer doesn't exists",
            "A SLD layer is called which doesn't exists: " + layerKey);
        return;
      }

      final String sldContents = SLDUtils.getAsSLD(wmsLayer,
          Constants.getString(SharedConstantsEnum.AERIUS_HOST) + "/" + Constants.getString(SharedConstantsEnum.WEBAPPNAME_CALCULATOR));
      response.getWriter().write(sldContents);
    } catch (final IOException e) {
      errorOccured(response, "Internal error", "Could not serve SLD: " + e.getMessage());
    } catch (final SQLException e) {
      errorOccured(response, "Internal error", "Could not serve SLD cause of an SQL exception: " + e.getMessage());
    }
    LOG.debug("SLD served: " + layerKey);
  }

  private void errorOccured(final HttpServletResponse response, final String messageToUser, final String logMessage) {
    try {
      response.sendError(HttpStatus.SC_FORBIDDEN, messageToUser);
    } catch (final IOException e) {
      // Eat error.
    }
    LOG.warn(logMessage);
  }

}
