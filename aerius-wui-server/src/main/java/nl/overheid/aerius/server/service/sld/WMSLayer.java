/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service.sld;

import java.util.ArrayList;
import java.util.List;

/**
 * An entity representing a row in the table system.wms_layer with the join to system.wms_zoomlevel and system.sld_rule.
 */
public class WMSLayer {

  private final int wmsLayerId;
  private final int sldId;
  private final String layerKey;
  private final List<WMSZoomLevel> zoomLevels = new ArrayList<WMSZoomLevel>();
  private final List<SLDRule> rules = new ArrayList<SLDRule>();

  /**
   * @param wmsLayerId The database ID of the layer.
   * @param sldId The SLD id to use.
   * @param layerKey The (unique) key for the layer.
   */
  public WMSLayer(final int wmsLayerId, final int sldId, final String layerKey) {
    this.wmsLayerId = wmsLayerId;
    this.sldId = sldId;
    this.layerKey = layerKey;
  }

  public int getWmsLayerId() {
    return wmsLayerId;
  }

  public String getLayerKey() {
    return layerKey;
  }

  /**
   * @param zoomLevel The zoom level to add.
   */
  public void addZoomLevel(final WMSZoomLevel zoomLevel) {
    zoomLevels.add(zoomLevel);
  }

  /**
   * @param rule The rule to add.
   */
  public void addRule(final SLDRule rule) {
    rules.add(rule);
  }

  public List<WMSZoomLevel> getZoomLevels() {
    return zoomLevels;
  }

  public List<SLDRule> getRules() {
    return rules;
  }

  public int getSldId() {
    return sldId;
  }
}
