/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.task;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionEnum;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.configuration.ConnectionConfigurationBean;

/**
 * Factory class to get static instance of a {@link TaskManagerClient}.
 */
public final class TaskClientFactory {

  private static TaskManagerClient taskClient;
  private static BrokerConnectionFactory factory;

  private TaskClientFactory() {
    // Util class
  }

  /**
   * @param con database connection
   * @param executor executor service
   * @throws SQLException database errors getting connection details
   * @throws IOException connection to client service
   */
  public static void init(final Connection con, final ExecutorService executor) throws SQLException {
    synchronized (TaskClientFactory.class) {
      if (taskClient == null) {
        final String host = ConstantRepository.getString(con, BrokerConnectionEnum.TASK_BROKER_HOST);
        final Integer port = ConstantRepository.getNumber(con, BrokerConnectionEnum.TASK_BROKER_PORT, Integer.class, false);
        final String username = ConstantRepository.getString(con, BrokerConnectionEnum.TASK_BROKER_USERNAME);
        final String password = ConstantRepository.getString(con, BrokerConnectionEnum.TASK_BROKER_PASSWORD);
        final ConnectionConfigurationBean config = new ConnectionConfigurationBean(host, port, username, password);
        factory = new BrokerConnectionFactory(executor, config);
        taskClient = new TaskManagerClient(factory);
      }
    }
  }

  /**
   * Returns instance. Should be initialized before call.
   * @return taskClient instance
   * @throws IOException exception when not initialized
   */
  public static TaskManagerClient getInstance() throws IOException {
    if (taskClient == null) {
      throw new IOException("Taskclient not initialized. #init must be called first.");
    }
    return taskClient;

  }

  /**
   * Shut down the Taskmanager client.
   */
  public static void shutdown() {
    if (factory != null) {
      factory.shutdown();
    }
  }
}
