/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.constants;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.server.ServerPMF;

/**
 * Utility class to retrieve server-specific constants.
 */
public final class Constants {

  // Not allowed to instantiate. 
  private Constants() {
  }

  /**
   * Get a constant value from the database. Runtime exception if not found.
   * @param constantEnum The constant to get the value for.
   * @return The value of the constant.
   */
  public static String getString(Enum<?> constantEnum) {
    return getString(constantEnum, true);
  }

  /**
   * Get a constant value from the database.
   * @param constantEnum The constant to get the value for.
   * @param mustExist If true, the constant must exist, else a runtime exception will be thrown.
   * @return The value of the constant.
   */
  public static String getString(Enum<?> constantEnum, boolean mustExist) {
    return getConstant(constantEnum, mustExist);
  }

  private static String getConstant(Enum<?> key, boolean mustExist) {
    return ConstantRepository.getString(ServerPMF.getInstance(), key, mustExist);
  }

}
