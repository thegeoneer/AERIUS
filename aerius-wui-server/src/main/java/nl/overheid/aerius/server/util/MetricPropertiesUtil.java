/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.metrics.MetricFactory;

public final class MetricPropertiesUtil {
  private MetricPropertiesUtil() {}

  /**
   * Retrieve the metric properties from either the System properties or the database.
   */
  public static Properties fetchMetricProperties(final Connection con) throws SQLException {
    final Properties props = new Properties();

    extract(con, props, MetricFactory.GRAPHITE_HOST_PROP, ConstantsEnum.METRICS_GRAPHITE_HOST);
    extract(con, props, MetricFactory.GRAPHITE_PORT_PROP, ConstantsEnum.METRICS_GRAPHITE_PORT);
    extract(con, props, MetricFactory.GRAPHITE_REPORT_INTERVAL, ConstantsEnum.METRICS_GRAHPITE_INTERVAL);
    extract(con, props, MetricFactory.GRAPHITE_REPORT_KEY_PREFIX, ConstantsEnum.METRICS_GRAPHITE_PREFIX);

    return props;
  }

  private static void extract(final Connection con, final Properties props, final String graphiteProp, final Enum<?> dbProp)
      throws SQLException {
    final String prop = getProperty(con, graphiteProp, dbProp);
    if (prop != null) {
      props.setProperty(graphiteProp, prop);
    }
  }

  /**
   * Retrieve a property from either the environment variables or the database. Database takes precedence.
   */
  private static String getProperty(final Connection con, final String graphiteProp, final Enum<?> dbProp) throws SQLException {
    final String prop1 = System.getProperty(graphiteProp);
    final String prop2 = ConstantRepository.getString(con, dbProp, false);
    return prop2 == null ? prop1 : prop2;
  }
}
