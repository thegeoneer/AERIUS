/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

/**
 * Filter that prevents ClickJacking by setting X-FRAME-OPTIONS.
 */
@WebFilter("/*")
public class ClickJackFilter implements Filter {

  private static final String HEADER_XFRAMEOPTIONS = "X-Frame-Options";
  private static final String XFRAMEOPTIONS_MODE = "SAMEORIGIN";

  @Override
  public void init(final FilterConfig filterConfig) throws ServletException {
    /* No-op. */
  }

  @Override
  public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
    final HttpServletResponse res = (HttpServletResponse) response;
    res.addHeader(HEADER_XFRAMEOPTIONS, XFRAMEOPTIONS_MODE);

    chain.doFilter(request, response);
  }

  @Override
  public void destroy() {
    /* No-op. */
  }

}
