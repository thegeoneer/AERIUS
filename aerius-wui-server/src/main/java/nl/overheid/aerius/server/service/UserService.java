/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AuthorizationException;

/**
 * Service class to get user profile via Security Subject.
 */
public final class UserService {

  private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

  private UserService() {
    //don't instantiate.
  }

  /**
   * Get the currently logged in user profile, or null if the user/subject
   * is not available (either not remembered or authenticated).
   * @param con database connection.
   * @return UserProfile of an existing user. Or null if it does not exist.
   * @throws AeriusException An SQL Error.
   */
  public static UserProfile getCurrentUserProfile(final Connection con) throws AeriusException {
    return getCurrentUserProfileFromSubject(con, SecurityUtils.getSubject());
  }

  /**
   * Get the currently logged in user profile or throw an error if the user/subject
   * is not authenticated.
   *
   * @param pmf database connection manager.
   * @return UserProfile of an existing user.
   * @throws AeriusException If the user is not logged in.
   */
  public static UserProfile getCurrentUserProfileStrict(final PMF pmf) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      return getCurrentUserProfileStrict(connection);
    } catch (final SQLException e) {
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  /**
   * Get the currently logged in user profile or throw an error if the user/subject
   * is not authenticated.
   *
   * @param con database connection.
   * @return UserProfile of an existing user.
   * @throws AeriusException If the user is not logged in.
   */
  public static UserProfile getCurrentUserProfileStrict(final Connection con) throws AeriusException {
    final Subject subject = SecurityUtils.getSubject();
    if (!subject.isAuthenticated()) {
      // The subject is not authenticated; throw auth error.
      throw new AuthorizationException();
    }

    return getCurrentUserProfileFromSubject(con, subject);
  }

  private static UserProfile getCurrentUserProfileFromSubject(final Connection connection, final Subject subject) throws AeriusException {
    UserProfile userProfile = null;

    // For now retrieve the current user's profile from the local DB.
    // this might be changed in the (near) future.
    final String principal = subject.getPrincipal() instanceof String ? (String) subject.getPrincipal() : "";
    try {
      userProfile = UserRepository.getAdminUserProfileByName(connection, principal);
    } catch (final SQLException e) {
      LOG.error(e.getMessage(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
    if (userProfile == null) {
      throw new AeriusException(Reason.USER_DOES_NOT_EXIST, principal);
    }
    return userProfile;
  }
}
