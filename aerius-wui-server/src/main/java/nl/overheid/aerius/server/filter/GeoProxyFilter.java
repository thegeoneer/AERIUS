/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.filter;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.service.sld.SLDService;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;

/**
 * Proxy to communicate with GeoServer. Extra checks are build in to avoid security problems.
 */
@WebFilter("/aerius-geo-wms")
public class GeoProxyFilter implements Filter {

  public static final String ATTRIBUTE_SLD = GeoProxyFilter.class.getCanonicalName() + ".ATTR_SLD";

  private static final Logger LOG = LoggerFactory.getLogger(GeoProxyFilter.class);

  private static final String PARAM_LAYERS = "LAYERS";
  private static final String SERVICE_PATH = "sld?layer={0}";

  @Override
  public void init(final FilterConfig filterConfig) throws ServletException {
    /* No-op. */
  }

  @Override
  public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
      throws IOException, ServletException {
    if (!StringUtils.isEmpty(request.getParameter(PARAM_LAYERS))) {
      try (final Connection con = ServerPMF.getInstance().getConnection()) {
        final String url = ConstantRepository.getString(con, SharedConstantsEnum.SHARED_SLD_URL);
        final String layerName = request.getParameter(PARAM_LAYERS);
        final String actualSLD = request.getParameter(SharedConstants.PARAM_INTERNAL_SLD);
        final String sldName;
        if (actualSLD == null) {
          if (SLDService.hasWMSLayer(con, layerName)) {
            sldName = layerName;
          } else {
            sldName = null;
          }
        } else {
          sldName = actualSLD;
        }
        if (sldName != null) {
          request.setAttribute(ATTRIBUTE_SLD, url + MessageFormat.format(SERVICE_PATH, sldName));
          LOG.debug("'{}' got equipped with SLD attribute: '{}'", layerName, sldName);
        }
      } catch (final SQLException e) {
        /* We are not going to provide a SLD in case of an error. */
      }
    }

    chain.doFilter(request, response);
  }

  @Override
  public void destroy() {
    /* No-op. */
  }

}
