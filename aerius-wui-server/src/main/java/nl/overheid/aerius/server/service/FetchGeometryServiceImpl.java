/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import nl.overheid.aerius.db.common.GeometryRepository;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.util.GeoGetCapabilitiesUtil;
import nl.overheid.aerius.shared.domain.geo.AreaType;
import nl.overheid.aerius.shared.domain.geo.WKTGeometryWithBox;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.FetchGeometryService;

/**
 * Implementation of the FetchGeometryService.
 */
@WebServlet("/aerius/aerius-fetch-geometry")
public class FetchGeometryServiceImpl extends RemoteServiceServlet implements FetchGeometryService {
  private static final long serialVersionUID = -8008331749097840325L;

  private static final Logger LOG = LoggerFactory.getLogger(FetchGeometryServiceImpl.class);

  @Override
  public WKTGeometryWithBox getGeometry(final AreaType areaType, final int areaId) throws AeriusException {
    return getGeometry(areaType, areaId, null, null, null);
  }

  @Override
  public WKTGeometryWithBox getGeometry(final AreaType areaType, final int areaId, final String nameGuide)
      throws AeriusException {
    return getGeometry(areaType, areaId, null, null, nameGuide);
  }

  @Override
  public WKTGeometryWithBox getSimplifiedGeometry(final AreaType areaType, final int areaId, final float tolerance)
      throws AeriusException {
    return getGeometry(areaType, areaId, null, tolerance, null);
  }

  @Override
  public WKTGeometryWithBox getSimplifiedGeometry(final AreaType areaType, final int areaId, final float tolerance,
      final String nameGuide) throws AeriusException {
    return getGeometry(areaType, areaId, null, tolerance, nameGuide);
  }

  @Override
  public WKTGeometryWithBox getGeometry(final AreaType areaType, final Point pointInArea) throws AeriusException {
    return getGeometry(areaType, null, pointInArea, null, null);
  }

  @Override
  public WKTGeometryWithBox getGeometry(final AreaType areaType, final Point pointInArea, final String nameGuide)
      throws AeriusException {
    return getGeometry(areaType, null, pointInArea, null, nameGuide);
  }

  @Override
  public WKTGeometryWithBox getSimplifiedGeometry(final AreaType areaType, final Point pointInArea,
      final float tolerance) throws AeriusException {
    return getGeometry(areaType, null, pointInArea, tolerance, null);
  }

  @Override
  public WKTGeometryWithBox getSimplifiedGeometry(final AreaType areaType, final Point pointInArea,
      final float tolerance, final String nameGuide) throws AeriusException {
    return getGeometry(areaType, null, pointInArea, tolerance, nameGuide);
  }

  private WKTGeometryWithBox getGeometry(final AreaType areaType, final Integer areaId, final Point areaPoint, final Float tolerance,
      final String nameGuide) throws AeriusException {
    try {
      final ServerPMF pmf = ServerPMF.getInstance();
      if (tolerance == null) {
        return GeometryRepository.getGeometry(pmf, areaType, areaId, areaPoint, nameGuide);
      } else {
        return GeometryRepository.getSimplifiedGeometry(pmf, areaType, areaId, areaPoint, tolerance, nameGuide);
      }
    } catch (final SQLException e) {
      LOG.error(e.getMessage(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public String fetchGetCapabilities(final String url) throws AeriusException {
    return GeoGetCapabilitiesUtil.fetchCapabilities(url);
  }
}
