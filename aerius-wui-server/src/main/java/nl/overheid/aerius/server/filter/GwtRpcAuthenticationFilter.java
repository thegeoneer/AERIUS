/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.filter;

import java.io.PrintWriter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.web.filter.authc.AuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.SharedConstants;

/**
 * Shiro filter that sends a {@link SharedConstants.AUTHENTICATION_EXPIRED} response if the user authentication expired.
 */
public class GwtRpcAuthenticationFilter extends AuthenticationFilter {
  private static final Logger LOG = LoggerFactory.getLogger(GwtRpcAuthenticationFilter.class);
  private static final String ERROR_MSG = "Authentication expired.";

  @Override
  protected boolean onAccessDenied(final ServletRequest request, final ServletResponse response) throws Exception {
    final HttpServletRequest httpRequest = (HttpServletRequest) request;
    final HttpServletResponse httpResponse = (HttpServletResponse) response;

    if (LOG.isDebugEnabled()) {
      LOG.debug("Denied access {} {}.", httpRequest.getMethod(), httpRequest.getRequestURI());
    }

    // It would be nice to use httpResponse.sendError(), but it seems not to work.
    httpResponse.setContentType("text/plain");
    httpResponse.setStatus(SharedConstants.AUTHENTICATION_EXPIRED);
    try (final PrintWriter out = httpResponse.getWriter()) {
      out.println(ERROR_MSG);
    }
    return false;
  }
}

