<%@page import="java.sql.Connection, nl.overheid.aerius.shared.constants.SharedConstantsEnum,
nl.overheid.aerius.db.common.ConstantRepository, nl.overheid.aerius.server.ServerPMF"%><%
Connection con = null;
try {
  con = ServerPMF.getInstance().getConnection();
  if (!ConstantRepository.getBoolean(con, SharedConstantsEnum.ANALYTICS_DISABLED)) {
    final String analyticsDomainName = ConstantRepository.getString(con, SharedConstantsEnum.ANALYTICS_DOMAIN_NAME);
    final String analyticsAccountId = ConstantRepository.getString(con, SharedConstantsEnum.ANALYTICS_ACCOUNT_ID);
    final String versionNumber = nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber();
    %><!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["setCookieDomain", "*.<%=analyticsDomainName%>"]);
  _paq.push(["setDomains", ["*.<%=analyticsDomainName%>"]]);
  _paq.push(["setDocumentTitle", "<%=versionNumber%>/" + document.title]);

  _paq.push(['enableHeartBeatTimer', 60]);

  _paq.push(["setCustomVariable", 1, "App", "${initParam.applicationTitle}", "visit"]);
  _paq.push(["setCustomVariable", 2, "Version", "<%=versionNumber%>", "visit"]);
  _paq.push(["setCustomVariable", 3, "Locale", "<%=lang%>", "visit"]);
  _paq.push(['trackPageView']);

  _paq.push(['setLinkTrackingTimer', 750]);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//statistiek.rijksoverheid.nl/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '<%=analyticsAccountId%>']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//statistiek.rijksoverheid.nl/piwik/piwik.php?idsite=<%=analyticsAccountId%>&rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code --><%
  }
} catch (final java.sql.SQLException e) {
  throw new IllegalArgumentException(e);
} finally {
  if (con != null) {
    try {
      con.close();
    } catch (final java.sql.SQLException e) { /* Eat error. */ }
  }
}
%>
