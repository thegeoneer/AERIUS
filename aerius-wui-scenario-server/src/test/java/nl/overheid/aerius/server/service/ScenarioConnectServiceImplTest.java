/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import static org.junit.Assert.assertEquals;
import io.swagger.util.Json;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.connect.domain.CalculateRequest;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.server.util.ConnectCalculationSessionUtil;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile.Situation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.ExportType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation.UtilType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.ScenarioConnectService;

/**
 * Test class for {@link ScenarioConnectServiceImpl}.
 */
public class ScenarioConnectServiceImplTest extends BaseDBTest {

  private static final String INPUT_FILE = "../uploadtest.gml";
  private static final String INPUT_RESULT_FILE = "../uploadresulttest.gml";
  private static final String CONNECT_CALCULATE_1 = "connect_calculate_1.json";
//  private static final String CONNECT_REPORT_1 = "connect_report_1.json";
  private static final String CONNECT_UTIL_VALIDATE_1 = "connect_util_validate_1.json";
  private static final String CONNECT_UTIL_CONVERT_1 = "connect_util_convert_1.json";
  private static final String CONNECT_UTIL_DELTA_1 = "connect_util_delta_1.json";
  private static final String CONNECT_UTIL_HIGHEST_1 = "connect_util_highest_1.json";
  private static final String CONNECT_UTIL_TOTAL_1 = "connect_util_total_1.json";


  @Test
  public void testSubmitCalculation() throws ClientProtocolException, IOException, AeriusException, URISyntaxException {
    final HttpServletRequest request = mockHttpServletRequest();
    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(request.getSession());

    final ConnectCalculationInformation cci = new ConnectCalculationInformation();
    cci.setExportType(ExportType.GML);
    cci.add(addFile(util, null, false, INPUT_FILE, "1"));
    cci.add(addFile(util, null, true, INPUT_FILE, "2"));
    assertSubmitCalculation(request, cci, CONNECT_CALCULATE_1);
  }

//  @Test
//  public void testSubmitReport() throws ClientProtocolException, IOException, AeriusException, URISyntaxException {
//    final HttpServletRequest request = mockHttpServletRequest();
//    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(request.getSession());
//
//    final ConnectCalculationInformation cci = new ConnectCalculationInformation();
//    cci.setExportType(ExportType.PDF_DEVELOPMENT_SPACES);
//    cci.add(addFile(util, Situation.CURRENT, false, INPUT_FILE, "1"));
//    cci.add(addFile(util, Situation.PROPOSED, false, INPUT_FILE, "2"));
//    assertSubmitCalculation(request, cci, CONNECT_REPORT_1);
//  }

  @Test
  public void testSubmitValidation() throws ClientProtocolException, IOException, AeriusException, URISyntaxException {
    final HttpServletRequest request = mockHttpServletRequest();
    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(request.getSession());

    final ConnectUtilInformation cui = new ConnectUtilInformation();
    cui.setUtilType(UtilType.VALIDATE);
    cui.add(addFile(util, null, false, INPUT_FILE, "1"));
    assertSubmitUtil(request, cui, CONNECT_UTIL_VALIDATE_1);
  }

  @Test
  public void testSubmitConvert() throws ClientProtocolException, IOException, AeriusException, URISyntaxException {
    final HttpServletRequest request = mockHttpServletRequest();
    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(request.getSession());

    final ConnectUtilInformation cui = new ConnectUtilInformation();
    cui.setUtilType(UtilType.CONVERT);
    cui.add(addFile(util, null, false, INPUT_FILE, "1"));
    assertSubmitUtil(request, cui, CONNECT_UTIL_CONVERT_1);
  }

  @Test
  public void testSubmitDelta() throws ClientProtocolException, IOException, AeriusException, URISyntaxException {
    final HttpServletRequest request = mockHttpServletRequest();
    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(request.getSession());

    final ConnectUtilInformation cui = new ConnectUtilInformation();
    cui.setUtilType(UtilType.DELTA_VALUE);
    cui.setOnlyIncreasement(false);
    cui.add(addFile(util, Situation.CURRENT, false, INPUT_RESULT_FILE, "1"));
    cui.add(addFile(util, Situation.PROPOSED, false, INPUT_RESULT_FILE, "2"));
    assertSubmitUtil(request, cui, CONNECT_UTIL_DELTA_1);
  }

  @Test
  public void testSubmitHighestValue() throws ClientProtocolException, IOException, AeriusException, URISyntaxException {
    final HttpServletRequest request = mockHttpServletRequest();
    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(request.getSession());

    final ConnectUtilInformation cui = new ConnectUtilInformation();
    cui.setUtilType(UtilType.HIGHEST_VALUE);
    cui.add(addFile(util, null, false, INPUT_RESULT_FILE, "1"));
    cui.add(addFile(util, null, false, INPUT_RESULT_FILE, "2"));
    assertSubmitUtil(request, cui, CONNECT_UTIL_HIGHEST_1);
  }

  @Test
  public void testSubmitTotalValue() throws ClientProtocolException, IOException, AeriusException, URISyntaxException {
    final HttpServletRequest request = mockHttpServletRequest();
    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(request.getSession());

    final ConnectUtilInformation cui = new ConnectUtilInformation();
    cui.setUtilType(UtilType.TOTAL_VALUE);
    cui.add(addFile(util, null, false, INPUT_RESULT_FILE, "1"));
    cui.add(addFile(util, null, false, INPUT_RESULT_FILE, "2"));
    assertSubmitUtil(request, cui, CONNECT_UTIL_TOTAL_1);
  }

  public void assertSubmitCalculation(final HttpServletRequest request, final ConnectCalculationInformation cci, final String expected)
      throws ClientProtocolException, IOException, AeriusException {
    final CloseableHttpResponse mockResponse = Mockito.mock(CloseableHttpResponse.class);
    Mockito.when(mockResponse.getEntity()).thenReturn(Mockito.mock(HttpEntity.class));
    final StatusLine mockValidStatusline = Mockito.mock(StatusLine.class);
    Mockito.when(mockValidStatusline.getStatusCode()).thenReturn(HttpStatus.SC_OK);
    Mockito.when(mockResponse.getStatusLine()).thenReturn(mockValidStatusline);
    final HttpPost post = new HttpPost();
    final ScenarioConnectService scc = getScenarioConnectService(mockResponse, request.getSession(), post);
    cci.setCalculationType(ConnectCalculationInformation.CalculationType.NBWET);
    cci.setApiKey("MY_API_KEY");
    cci.setYear(2020);
    scc.submitCalculation(cci);
    final CalculateRequest cr = Json.mapper().readValue(IOUtils.toString(post.getEntity().getContent(), StandardCharsets.UTF_8),
        CalculateRequest.class);
    assertEquals("Check expected data to be sent to connect", IOUtils.toString(getClass().getResourceAsStream(expected)),
        IOUtils.toString(post.getEntity().getContent(), StandardCharsets.UTF_8));
  }

  public void assertSubmitUtil(final HttpServletRequest request, final ConnectUtilInformation cui, final String expected)
      throws ClientProtocolException, IOException, AeriusException {
    final CloseableHttpResponse mockResponse = Mockito.mock(CloseableHttpResponse.class);
    Mockito.when(mockResponse.getEntity()).thenReturn(Mockito.mock(HttpEntity.class));
    final StatusLine mockValidStatusline = Mockito.mock(StatusLine.class);
    Mockito.when(mockValidStatusline.getStatusCode()).thenReturn(HttpStatus.SC_OK);
    Mockito.when(mockResponse.getStatusLine()).thenReturn(mockValidStatusline);
    final HttpPost post = new HttpPost();
    final ConnectDataConverter converter = new ConnectDataConverter();
    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(request.getSession());
    final ScenarioConnectServiceImpl scc = getScenarioConnectServiceImpl(mockResponse, request.getSession(), post);
    switch (cui.getUtilType()) {
    case VALIDATE:
      scc.submitUtilValidate(converter.convertToValidateRequest(cui), util);
      break;
    case CONVERT:
      scc.submitUtilConvert(converter.convertToConvertRequest(cui), util);
      break;
    case DELTA_VALUE:
      scc.submitUtilDeltaValuePerHexagonValue(converter.convertToDeltaValuePerHexagonRequest(cui), util);
      break;
    case HIGHEST_VALUE:
      scc.submitUtilHighestValuePerHexagonValue(converter.convertToHighestValuePerHexagonRequest(cui), util);
      break;
    case TOTAL_VALUE:
      scc.submitUtilTotalValuePerHexagonValue(converter.convertToTotalValuePerHexagonRequest(cui), util);
      break;
    default:
      // not known
    }

    assertEquals("Check expected data to be sent to connect", IOUtils.toString(getClass().getResourceAsStream(expected)),
        IOUtils.toString(post.getEntity().getContent(), StandardCharsets.UTF_8));
  }

  private ConnectCalculationFile addFile(final ConnectCalculationSessionUtil util, final Situation situation, final boolean researchArea,
      final String fileName, final String uuid) throws URISyntaxException {
    final String uploadPath = new File(getClass().getResource(fileName).toURI()).getAbsolutePath();
    util.registerFileUpload(uuid, uploadPath);
    return createConnectCalculationFile(situation, researchArea, uuid);
  }

  private HttpServletRequest mockHttpServletRequest() {
    final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    final HttpSession session = Mockito.mock(HttpSession.class);
    Mockito.when(request.getSession()).thenReturn(session);
    final Map<String, Object> sessionMap = new HashMap<>();
    Mockito.when(session.getAttribute(Mockito.anyString())).then(new Answer<Object>() {
      @Override
      public Object answer(final InvocationOnMock invocation) throws Throwable {
        return sessionMap.get(invocation.getArgumentAt(0, String.class));
      }
    });
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        sessionMap.put(invocation.getArgumentAt(0, String.class), invocation.getArgumentAt(1, Object.class));
        return null;
      }
    }).when(session).setAttribute(Mockito.anyString(), Mockito.any());
    return request;
  }

  private ConnectCalculationFile createConnectCalculationFile(final Situation situation, final boolean researchArea, final String uuid) {
    final ConnectCalculationFile ccf = new ConnectCalculationFile();
    ccf.setSituation(situation);
    ccf.setUuid(uuid);
    return ccf;
  }

  private ScenarioConnectService getScenarioConnectService(final CloseableHttpResponse mockResponse, final HttpSession mockHttpSession,
      final HttpPost postRequest) throws ClientProtocolException, IOException {
    final CloseableHttpClient mockHttpClient = Mockito.mock(CloseableHttpClient.class);
    Mockito.doAnswer(new Answer<CloseableHttpResponse>() {
      @Override
      public CloseableHttpResponse answer(final InvocationOnMock invocation) throws Throwable {
        postRequest.setEntity(invocation.getArgumentAt(0, HttpPost.class).getEntity());
        return mockResponse;
      }
    }).when(mockHttpClient).execute(Mockito.any());
    final ScenarioSession session = new ScenarioSession() {
      @Override
      public HttpSession getSession() {
        return mockHttpSession;
      }

      @Override
      public Locale getLocale() {
        return Locale.ENGLISH;
      }
      @Override
      public void close() {
      }
    };
    return new ScenarioConnectServiceImpl(getCalcPMF(), session) {
      @Override
      protected CloseableHttpClient getHttpClient() {
        return mockHttpClient;
      };
    };
  }

  private ScenarioConnectServiceImpl getScenarioConnectServiceImpl(final CloseableHttpResponse mockResponse, final HttpSession mockHttpSession,
      final HttpPost postRequest) throws ClientProtocolException, IOException {
    final CloseableHttpClient mockHttpClient = Mockito.mock(CloseableHttpClient.class);
    Mockito.doAnswer(new Answer<CloseableHttpResponse>() {
      @Override
      public CloseableHttpResponse answer(final InvocationOnMock invocation) throws Throwable {
        postRequest.setEntity(invocation.getArgumentAt(0, HttpPost.class).getEntity());
        return mockResponse;
      }
    }).when(mockHttpClient).execute(Mockito.any());
    final ScenarioSession session = new ScenarioSession() {
      @Override
      public HttpSession getSession() {
        return mockHttpSession;
      }

      @Override
      public Locale getLocale() {
        return Locale.ENGLISH;
      }

      @Override
      public void close() {
      }
    };
    return new ScenarioConnectServiceImpl(getCalcPMF(), session) {
      @Override
      protected CloseableHttpClient getHttpClient() {
        return mockHttpClient;
      };
    };
  }
}
