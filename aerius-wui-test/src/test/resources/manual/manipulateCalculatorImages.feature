@calculator-manual-edit
Feature: Image manipulation steps adding labels for the AERIUS CALCULATOR manual

  Background: 
    Given process images for product 'calculator'

  @calculator-manipulate-gebruikkaart-zoomfunctie
  Scenario: 638 Zoomfunctie
    Given manipulate image '638_GebruikKaart-Zoomfunctie-ZoomenEnSchuiven-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 375, 110
    And add image 'B' at position 375, 28

  @calculator-manipulate-gebruikkaart-zoekfunctie
  Scenario: 639 Uitleg Zoekbalk
    Given manipulate image '639_GebruikKaart-Zoekfunctie-UitlegZoekbalk-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 825, 16
    And add image 'B' at position 825, 95

  @calculator-manipulate-gebruikkaart-zoekfunctie-natuurgebied
  Scenario: 668 Zoekfunctie natuurgebied
    Given manipulate image '668_GebruikKaart-Zoekfunctie-NatuurgebiedZoeken-Stap_3'
    Then crop starting at 160, 0
    And add image 'A' at position 535, 125

  @calculator-manipulate-gebruikkaart-zoekfunctie-postcode
  Scenario: 907 Zoekfunctie postcode
    Given manipulate image '907_GebruikKaart-Zoekfunctie-PostcodeZoeken-Stap_2'
    Then crop starting at 160, 0
    And add image 'A' at position 560, 16
    And add image 'B' at position 540, 95

  @calculator-manipulate-gebruikkaart-zoekfunctie-navigatie
  Scenario: 1276 Zoekfunctie navigatie
    Given manipulate image '1276_GebruikKaart-Zoekfunctie-Navigatie-Stap_4'
    Then crop starting at 160, 0
    And add image 'A' at position 725, 390

  @calculator-manipulate-gebruikkaart-berichtencentrum
  Scenario: 640 Uitleg Zoekbalk
    Given manipulate image '640_GebruikKaart-Berichtencentrum-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 610, 225
    And add image 'B' at position 963, 16

  @calculator-manipulate-menufunties-basis
  Scenario: 1291 Menufuncties basis
    Given manipulate image '1291_MenuFuncties-Hoofdmenu-Stap_1'
    And add image 'A' at position 110, 135
    And add image 'B' at position 90, 195

  Scenario: 628
    Given manipulate image '628_MenuFuncties-Helpfunctie-Stap_1'
    And add image 'A' at position 50, 300

  Scenario: 747
    Given manipulate image '747_MenuFuncties-Helpfunctie-Voorbeeld-Stap_2'
    And add image 'Muis' at position 445, 80
    And add image 'A' at position 50, 307
    And add image 'B' at position 504, 92

  Scenario:  630
    Given manipulate image '630_MenuFuncties-Themaswitch-Stap_1'
    And add image 'Muis' at position 307, 92
    And add image 'A' at position 110, 67
    And add image 'B' at position 290, 140

  Scenario: 1573
    Given manipulate image '1573_MenuFuncties-Rekenvoorkeuren-Stap_1'

  Scenario: 629
    Given manipulate image '629_MenuFuncties-UitlegJaartal-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 22, 40

  Scenario: 1948
    Given manipulate image '1948_MenuFuncties-Themaswitch-Stap_1'
    And add image 'Muis' at position 307, 92
    And add image 'A' at position 110, 67
    And add image 'B' at position 290, 140

  @calculator-manipulate-menufunties-informatieknop
  Scenario: 633 Menufunctie Informatieknop
    Given manipulate image '633_MenuFuncties-Informatieknop-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 237, 40
    And add image 'B' at position 343, 280
    And add image 'C' at position 260, 432

  Scenario: 908
    Given manipulate image '908_MenuFuncties-Informatieknop-Stap_2'
    Then crop starting at 160, 0
    And add image 'A' at position 343, 165
    And add image 'B' at position 343, 321
    And add image 'C' at position 343, 390

  Scenario: 634
    Given manipulate image '634_MenuFuncties-Informatieknop-Stap_3'
    Then crop starting at 160, 0
    And add image 'Muis' at position 200, 625
    And add image 'A' at position 16, 333
    And add image 'B' at position 305, 715

  @calculator-manipulate-menufuncties-kaartlagen
  Scenario: 2053 Menufunctie Kaartlagen
    Given manipulate image '2053_MenuFuncties-Kaartlagen-Stap_4'
    Then crop starting at 160, 0
    And add image 'LosPaneel' at position 295, 200
    And add image 'A' at position 435, 200
    And add image 'B' at position 625, 200

  Scenario: 636
    Given manipulate image '636_MenuFuncties-Kaartlagen-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 295, 40
    And add image 'B' at position 235, 315

  Scenario: 637
    Given manipulate image '637_MenuFuncties-Kaartlagen-Stap_2'
    Then crop starting at 160, 0
    And add image 'Muis' at position 220, 360
    And add image 'Move' at position 190, 565
    And add image 'A' at position 8, 327
    And add image 'B' at position 250, 369
    And add image 'C' at position 230, 565

  Scenario: 1275
    Given manipulate image '1275_MenuFuncties-Kaartlagen-Stap_3'
    Then crop starting at 160, 0
    And add image 'Habitattypes' at position 55, 357
    And add image 'Muis' at position 490, 540
    And add image 'A' at position 250, 297

  @calculator-manipulate-railverkeer
  Scenario: 1930 Railverkeer
    Given manipulate image '1930_Sectoren-Railverkeer-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 5, 272
    And add image 'B' at position 5, 372

  @calculator-manipulate-menufunties-rekenpunten
  Scenario: 1938 Menufunctie Rekenpunten
    Given manipulate image '1938_MenuFuncties-Rekenconfiguratie-Rekenpunten-Stap_2'
    Then crop starting at 160, 0
    And add image 'A' at position 345, 195
    And add image 'B' at position 200, 248

  Scenario: 1940
    Given manipulate image '1940_MenuFuncties-Rekenconfiguratie-Rekenpunten-Stap_3'
    Then crop starting at 160, 0
    And add image 'A' at position 20, 407
    And add image 'B' at position 602, 337
    And add image 'C' at position 565, 378
    And add image 'D' at position 565, 444

  @calculator-manipulate-luchtverkeer
  Scenario: 1944 Luchtverkeer
    Given manipulate image '1944_Sectoren-Luchtvaart-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 5, 272
    And add image 'B' at position 5, 372

  Scenario: 697
    Given manipulate image '697_MenuFuncties-Rekenconfiguratie-BekijkBewerk-Stap_3'
    Then crop starting at 160, 0
    And add image 'A' at position 595, 390
    And add image 'B' at position 8, 162
    And add image 'C' at position 185, 162
    And add image 'D' at position 315, 330

  Scenario: 1278
    Given manipulate image '1278_MenuFuncties-Rekenconfiguratie-BekijkBewerkSelectie-Stap_5'
    Then crop starting at 160, 0
    And add image 'Muis' at position 85, 300
    And add image 'A' at position 330, 130
    And add image 'B' at position 85, 260

  Scenario: 1279
    Given manipulate image '1279_MenuFuncties-Rekenconfiguratie-Importeer rekenpunt-Stap_6'
    Then crop starting at 160, 0
    And add image 'Muis' at position 165, 218
    And add image 'A' at position 110, 162

  @calculator-manipulate-algemeen-starten
  Scenario: 780 Algemeen Starten
    Given manipulate image '780_Algemeen-Starten-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 145, 235
    And add image 'B' at position 325, 235

  Scenario: 1684
    Given manipulate image '1684_Algemeen-Starten-Stap_2'
    Then crop starting at 160, 0
    And add image 'A' at position 156, 800
    And add image 'Refresh' at position 210, 255
    And add image 'B' at position 600, 335

  @calculator-manipulate-algemeen-handmatigeinvoer
  Scenario: 662 Algemeen Handmatige invoer
    Given manipulate image '662_Algemeen-HandmatigeInvoer-Stap_1'
    Then crop starting at 160, 0
    And add image 'Muis' at position 690, 405
    And add image 'A' at position 90, 165
    And add image 'B' at position 170, 237
    And add image 'C' at position 710, 420
    And add image 'D' at position 270, 295

  Scenario: 670
    Given manipulate image '670_Algemeen-HandmatigeInvoer-Stap_1.1'
    Then crop starting at 160, 0
    And add image 'A' at position 82, 160
    And add image 'B' at position 530, 255
    And add image 'C' at position 270, 265

  Scenario: 1480
    Given manipulate image '1480_Algemeen-HandmatigeInvoer-Stap_1.2'
    Then crop starting at 160, 0
    And add image 'A' at position 132, 160
    And add image 'B' at position 680, 390
    And add image 'C' at position 270, 330

  Scenario: 654
    Given manipulate image '654_Algemeen-HandmatigeInvoer-Stap_2'
    Then crop starting at 160, 0
    And add image 'SectorGroups' at position 17, 283
    And add image 'Muis' at position 165, 352
    And add image 'A' at position 285, 135
    And add image 'B' at position 162, 200
    And add image 'C' at position 270, 330

  Scenario: 658
    Given manipulate image '658_Algemeen-HandmatigeInvoer-Stap_3'
    Then crop starting at 160, 0
    And add image 'Sectors' at position 17, 328
    And add image 'Muis' at position 165, 397
    And add image 'A' at position 150, 250
    And add image 'B' at position 270, 382

  @calculator-manipulate-ops-handmatigeinvoer
  Scenario: 657
    Given manipulate image '657_Algemeen-HandmatigeInvoer_Kenmerken-Stap_4'
    Then crop starting at 160, 0
    And add image 'Muis' at position 35, 355
    And add image 'A' at position 160, 340
    And add image 'B' at position 340, 470
    And add image 'C' at position 160, 410
    And add image 'D' at position 340, 370
  
  @calculator-manipulate-ops-handmatigeinvoer  
  Scenario: 2031
    Given manipulate image '2031_Algemeen-HandmatigeInvoer-Geforceerde-Uitstroom-Stap_5'
    Then crop starting at 160, 0
    And add image 'Muis' at position 230, 390
    And add image 'A' at position 330, 480
  
  @calculator-manipulate-ops-handmatigeinvoer    
  Scenario: 4124
    Given manipulate image '4124_Algemeen-HandmatigeInvoer-Gebouwinvloed-Stap_6'
    Then crop starting at 160, 0
    And add image 'Muis' at position 35, 425
    And add image 'A' at position 330, 480

  Scenario: 748
    Given manipulate image '748_Algemeen-HandmatigeInvoer-Stap_7'
    Then crop starting at 160, 0
    And add image 'A' at position 125, 385
    And add image 'B' at position 339, 793

  Scenario: 659
    Given manipulate image '659_Algemeen-HandmatigeInvoer-Stap_8'
    Then crop starting at 160, 0
    And add image 'Muis' at position 260, 355
    And add image 'A' at position 335, 215
    And add image 'B' at position 555, 315
    And add image 'C' at position 290, 325

  Scenario: 967
    Given manipulate image '967_Algemeen-HandmatigeInvoer-Stap_9'
    Then crop starting at 160, 0
    And add image 'A' at position 345, 295
    And add image 'B' at position 115, 270
    And add image 'C' at position 16, 270

  Scenario: 968
    Given manipulate image '968_Algemeen-HandmatigeInvoer-Stap_10'
    Then crop starting at 160, 0
    And add image 'A' at position 325, 443
    And add image 'B' at position 145, 443
    And add image 'C' at position 145, 400

  @calculator-manipulate-bestandenimporteren
  Scenario: 1433 Algemeen Bestanden Importeren
    Given manipulate image '1433_Algemeen-BestandenImporteren-Stap_5'
    Then crop starting at 160, 0
    And add image 'ImporterenSlepen' at position 150, 200
    And add image 'Bestand' at position 665, 430
    And add image 'A' at position 830, 565
    And add image 'B' at position 185, 410

  Scenario: 677
    Given manipulate image '677_Algemeen-BestandenImporteren-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 335, 210
    And add image 'B' at position 185, 410

  Scenario: 678
    Given manipulate image '678_Algemeen-BestandenImporteren-Stap_2'
    Then crop starting at 160, 0
    And add image 'A' at position 185, 410
    And add image 'BestandOpenen' at position 360, 200

  Scenario: 679
    Given manipulate image '679_Algemeen-BestandenImporteren-Stap_3'
    Then crop starting at 160, 0
    And add image 'A' at position 450, 410
    And add image 'B' at position 560, 300

  Scenario: 680
    Given manipulate image '680_Algemeen-BestandenImporteren-Stap_4'
    Then crop starting at 160, 0
    And add image 'A' at position 950, 45
    And add image 'B' at position 275, 360

  @calculator-manipulate-bestandenimporteren-handmatig
  Scenario: 1693 Algemeen Bestanden Importeren en Handmatig
    Given manipulate image '1693_Algemeen-BestandenImporteren-Stap_6'
    Then crop starting at 160, 0
    And add image 'A' at position 8, 250
    And add image 'B' at position 105, 250
    And add image 'B' at position 340, 275

  @calculator-manipulate-sectoren-eenvoudig
  Scenario: 1924 Sectoren, eenvoudige sectoren
    Given manipulate image '1924_Sectoren-Energie-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 145, 290
    And add image 'B' at position 145, 495

  Scenario: 1927
    Given manipulate image '1927_Sectoren-WonenEnWerken-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 145, 290
    And add image 'B' at position 145, 340
    And add image 'C' at position 145, 555

  Scenario: 1934
    Given manipulate image '1934_Sectoren-Industrie-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 145, 290
    And add image 'B' at position 145, 340
    And add image 'C' at position 145, 550

  Scenario: 1937
    Given manipulate image '1937_Sectoren-Anders-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 135, 292

  @calculator-manipulate-sectoren-landbouw
  Scenario: 2030 Sectoren, landbouw
    Given manipulate image '2030_Sectoren-Landbouw-Stap_1'
    Then crop starting at 160, 0
    And add image 'SectorsLandbouw' at position 20, 330
    And add image 'A' at position 280, 290
    And add image 'B' at position 280, 336

  Scenario: 664
    Given manipulate image '664_Sectoren-Landbouw-Stap_2'
    Then crop starting at 160, 0
    And add image 'Lodgingbwl' at position 282, 583
    And add image 'Muis' at position 328, 600
    And add image 'A' at position 125, 520
    And add image 'C' at position 215, 520
    And add image 'B' at position 315, 520

  Scenario: 2001
    Given manipulate image '2001_Sectoren-Landbouw-Stap_3'
    Then crop starting at 160, 0
    And add image 'Muis' at position 90, 580
    And add image 'A' at position 17, 515
    And add image 'B' at position 345, 600
    And add image 'C' at position 285, 515

  Scenario: 2011
    Given manipulate image '2011_Sectoren-Landbouw-Stap_4'
    Then crop starting at 160, 0
    And add image 'Muis' at position 90, 590
    And add image 'A' at position 180, 535
    And add image 'B' at position 345, 600

  Scenario: 665
    Given manipulate image '665_Sectoren-Landbouw-Stap_5'
    Then crop starting at 160, 0
    And add image 'Muis' at position 290, 600
    And add image 'A' at position 335, 405
    And add image 'B' at position 90, 537
    And add image 'C' at position 325, 537
    And add image 'D' at position 325, 585

  @calculator-manipulate-sectoren-mobielewerktuigen
  Scenario: 1272 Sectoren, mobiele werktuigen
    Given manipulate image '1272_Sectoren-MobieleWerktuigen-Stap_1'
    Then crop starting at 160, 0
    And add image 'MobieleWerktuigen' at position 192, 140
    And add image 'A' at position 135, 380
    And add image 'B' at position 345, 455
    And add image 'C' at position 345, 500

  Scenario: 1273
    Given manipulate image '1273_Sectoren-MobieleWerktuigen-Stap_2'
    Then crop starting at 160, 0
    And add image 'A' at position 145, 370
    And add image 'B' at position 310, 405
    And add image 'C' at position 345, 444

  Scenario: 1422
    Given manipulate image '1422_Sectoren-MobieleWerktuigen-Stap_3'
    Then crop starting at 160, 0
    And add image 'A' at position 660, 305
    And add image 'B' at position 660, 372
    And add image 'C' at position 660, 500
    And add image 'D' at position 420, 538

  Scenario: 1424
    Given manipulate image '1424_Sectoren-MobieleWerktuigen-Stap_4'
    Then crop starting at 160, 0
    And add image 'A' at position 660, 305
    And add image 'B' at position 660, 372
    And add image 'C' at position 660, 458
    And add image 'D' at position 420, 496

  @calculator-manipulate-sectoren-plan
  Scenario: 1267 Sectoren Plan
    Given manipulate image '1267_Sectoren-Plan-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 160, 160
    And add image 'B' at position 800, 315
    And add image 'C' at position 250, 260

  Scenario: 1268
    Given manipulate image '1268_Sectoren-Plan-Stap_2'
    Then crop starting at 160, 0
    And add image 'SectorsPlan' at position 19, 280
    And add image 'A' at position 255, 155
    And add image 'B' at position 160, 200
    And add image 'C' at position 160, 435

  Scenario: 1269
    Given manipulate image '1269_Sectoren-Plan-Stap_3'
    Then crop starting at 160, 0
    And add image 'Plannen' at position 30, 407
    And add image 'A' at position 195, 295
    And add image 'B' at position 183, 421
    And add image 'C' at position 265, 365

  Scenario: 1270
    Given manipulate image '1270_Sectoren-Plan-Stap_4'
    Then crop starting at 160, 0
    And add image 'A' at position 20, 305
    And add image 'B' at position 195, 305
    And add image 'C' at position 250, 305
    And add image 'D' at position 305, 305

  Scenario: 1271
    Given manipulate image '1271_Sectoren-Plan-Stap_5'
    Then crop starting at 160, 0
    And add image 'A' at position 335, 235
    And add image 'B' at position 8, 260
    And add image 'C' at position 98, 260
    And add image 'D' at position 186, 260
    And add image 'E' at position 315, 740

  @calculator-manipulate-vergelijker-varianten
  Scenario: 682 Vergelijker Varianten
    Given manipulate image '682_Vergelijker-Varianten-Stap_1'
    Then crop starting at 160, 0
    And add image 'Muis' at position 175, 181
    And add image 'A' at position 218, 152

  Scenario: 683
    Given manipulate image '683_Vergelijker-Varianten-Stap_2'
    Then crop starting at 160, 0
    And add image 'Muis' at position 225, 320
    And add image 'A' at position 180, 270
    And add image 'B' at position 205, 160

  Scenario: 684
    Given manipulate image '684_Vergelijker-Varianten-Stap_3'
    Then crop starting at 160, 0
    And add image 'A' at position 162, 200
    And add image 'B' at position 140, 335
    And add image 'C' at position 140, 390
    And add image 'D' at position 285, 125
    And add image 'E' at position 340, 788

  Scenario: 686
    Given manipulate image '686_Vergelijker-Varianten-Stap_4'
    Then crop starting at 160, 0
    And add image 'A' at position 290, 158
    And add image 'D' at position 320, 788
    And add image 'C' at position 85, 147
    And add image 'B' at position 775, 360

  Scenario: 687
    Given manipulate image '687_Vergelijker-Varianten-Stap_5'
    Then crop starting at 160, 0
    And add image 'A' at position 10, 232
    And add image 'B' at position 140, 800
    And add image 'C' at position 650, 405

  @calculator-manipulate-depositieberekening-berekenen
  Scenario: 692 Depositieberekening Berekenen
    Given manipulate image '692_Depositieberekening-Berekenen-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 10, 235
    And add image 'B' at position 350, 800
    And add image 'C' at position 950, 40

  Scenario: 763
    Given manipulate image '763_Depositieberekening-Berekenen-Stap_2'
    Then crop starting at 160, 0
    And add image 'A' at position 10, 235
    And add image 'B' at position 950, 40
    And add image 'C' at position 150, 795

  Scenario: 1280
    Given manipulate image '1280_Depositieberekening-Berekenen-Stap_3'
    Then crop starting at 160, 0
    And add image 'A' at position 260, 365
    And add image 'B' at position 205, 275
    And add image 'C' at position 310, 65

  Scenario: 1281
    Given manipulate image '1281_Depositieberekening-Berekenen-Stap_4'
    Then crop starting at 160, 0
    And add image 'Muis' at position 60, 521
    And add image 'A' at position 165, 224
    And add image 'B' at position 345, 264
    And add image 'C' at position 10, 350
    And add image 'D' at position 345, 40
    And add image 'D' at position 850, 340

  @AER-1727
  Scenario: 699
    Given manipulate image '699_Depositieberekening-Exporteren-Stap_1'
    Then crop starting at 160, 0
    And add image 'Muis' at position 470, 500
    And add image 'A' at position 150, 800
    And add image 'B' at position 215, 160
    And add image 'C' at position 575, 700

  @calculator-manipulate-sectoren-verkeerenvervoer
  Scenario: 1264 Sector Wegverkeer
    Given manipulate image '1264_Sectoren-VerkeerEnVervoer-VerkeerEnVervoer-Stap_1'
    Then crop starting at 160, 0
    And add image 'Muis' at position 200, 480
    And add image 'A' at position 285, 300
    And add image 'B' at position 175, 390
    And add image 'C' at position 225, 465

  Scenario: 1949
    Given manipulate image '1949_Sectoren-VerkeerEnVervoer-VerkeersEmissies-Stap_2'
    Then crop starting at 160, 0
    And add image 'Muis' at position 215, 552
    And add image 'A' at position 49, 340
    And add image 'B' at position 150, 390
    And add image 'C' at position 340, 420
    And add image 'D' at position 338, 495
    And add image 'E' at position 300, 540

  Scenario: 1950
    Given manipulate image '1950_Sectoren-VerkeerEnVervoer-MeerdereVerkeerstypen-Stap_3'
    Then crop starting at 160, 0
    And add image 'Muis' at position 245, 780
    And add image 'A' at position 155, 380
    And add image 'B' at position 340, 380
    And add image 'C' at position 355, 795

  @calculator-manipulate-sectoren-verkeerenvervoer-import
  Scenario: 1958 Sector Wegverkeer
    Given manipulate image '1958_Sectoren-VerkeerEnVervoer-Importeren-Stap_1'
    Then crop starting at 160, 0
    And add image 'A' at position 145, 235
    And add image 'B' at position 325, 235

  Scenario: 1959
    Given manipulate image '1959_Sectoren-VerkeerEnVervoer-Importeren-Stap_2'
    Then crop starting at 160, 0
    And add image 'A' at position 16, 240
    And add image 'B' at position 105, 240
    And add image 'C' at position 202, 240
    And add image 'D' at position 255, 240

  Scenario: 1960
    Given manipulate image '1960_Sectoren-VerkeerEnVervoer-Importeren-Stap_3'
    Then crop starting at 160, 0
    And add image 'Muis' at position 170, 222
    And add image 'Muis' at position 345, 540
    And add image 'A' at position 153, 175
    And add image 'B' at position 680, 290
    And add image 'C' at position 330, 495
    And add image 'D' at position 250, 685

  @calculator-manipulate-sectoren-scheepvaart
  Scenario: 1575 Sector Scheepvaart
    Given manipulate image '1575_Sectoren-Scheepvaart-Stap_1'
    Then crop starting at 160, 0
    And add image 'SectorsScheepvaart' at position 19, 330
    And add image 'Muis' at position 345, 320
    And add image 'A' at position 160, 335
    And add image 'B' at position 160, 365
    And add image 'C' at position 160, 395

  Scenario: 1262
    Given manipulate image '1262_Sectoren-Scheepvaart-Stap_2'
    Then crop starting at 160, 0
    And add image 'Zeeroute' at position 29, 647
    And add image 'Muis' at position 175, 695
    And add image 'A' at position 170, 385
    And add image 'B' at position 95, 485
    And add image 'C' at position 210, 560
    And add image 'D' at position 135, 650

  Scenario: 1263
    Given manipulate image '1263_Sectoren-Scheepvaart-Stap_3'
    Then crop starting at 160, 0
    And add image 'A' at position 160, 360
    And add image 'B' at position 320, 410

  Scenario: 1574
    Given manipulate image '1574_Sectoren-Scheepvaart-Stap_4'
    Then crop starting at 160, 0
    And add image 'A' at position 95, 413
    And add image 'B' at position 348, 413

  Scenario: 1576
    Given manipulate image '1576_Sectoren-Scheepvaart-Stap_5'
    Then crop starting at 160, 0
    And add image 'SectorsScheepvaart' at position 19, 330
    And add image 'A' at position 140, 415
    And add image 'B' at position 140, 445

  Scenario: 1429
    Given manipulate image '1429_Sectoren-Scheepvaart-Stap_6'
    Then crop starting at 160, 0
    And add image 'A' at position 95, 413
    And add image 'B' at position 290, 413
    And add image 'C' at position 178, 490
    And add image 'D' at position 178, 615

  Scenario: 1261
    Given manipulate image '1261_Sectoren-Scheepvaart-Stap_7'
    Then crop starting at 160, 0
    And add image 'A' at position 340, 548
    And add image 'B' at position 155, 625
    And add image 'C' at position 340, 625
    And add image 'D' at position 340, 735

  @calculator-manipulate-binnenvaart
  Scenario: 9990 Kaartlaag binnenvaart
    Given manipulate image '3778_Sectoren-Scheepvaart-Stap_6.1'
    Then crop starting at 160, 0
    And add image 'A' at position 350, 154

  @calculator-manipulate-binnenvaart
  Scenario: 9991 Binnenvaart lijnbron
    Given manipulate image '3779_Sectoren-Scheepvaart-Stap_6.2'
    Then crop starting at 160, 0
    And add image 'A' at position 113, 390
    And add image 'B' at position 355, 390

#  @calculator-manipulate-binnenvaart @AER-1718
#  Scenario: 9992 Priority Radius
#    Given manipulate image '3780_MenuFuncties-Rekenconfiguratie-Rekenpunten-Stap_1.2'
#    Then crop starting at 160, 0
#    And add image 'Muis' at position 30, 241

  @calculator-manipulate-binnenvaart
  Scenario: 9993 Bronspecificatie
    Given manipulate image '1261_Sectoren-Scheepvaart-Stap_7'
    Then crop starting at 160, 0
    And add image 'A' at position 345, 551
    And add image 'B' at position 75, 645
    And add image 'C' at position 250, 645
    And add image 'D' at position 345, 743
