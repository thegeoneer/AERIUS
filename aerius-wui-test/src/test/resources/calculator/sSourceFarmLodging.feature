@single @nightly @farmlodging
Feature: Add, modify and calculate sources of category FARM lodging

 @farmtest
  Scenario: Work with different RAV-codes and systems
    # Set location
    Given AERIUS CALCULATOR is open
    When the search for 'loevestein' is started
    And the search suggestion 'Loevestein, Pompveld & Kornsche Boezem' is selected
    ##
    # Add source
    ##
    When the startup is set to input sources
    And the source location is set to a 'polygon' with coordinates 129662 418727 130223 418710 130176 419349 129719 419302 129726 418912 129659 418895
    And the source label is set to 'Farmer John'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    # Add items
    And the farming emission is added a rav-code 'A 1.28' with bwl-code 'BWL 2001.28.V1' and an amount of 100
    And the farming fodder measure is added 'PAS 2015.09-01'
    And the source sub item is saved

  @farmlodging1 @AER-1713 @AER-1720 @AER-1727
  Scenario: Work with different RAV-codes and systems
    # Set location
    Given AERIUS CALCULATOR is open
    When the search for 'loevestein' is started
    And the search suggestion 'Loevestein, Pompveld & Kornsche Boezem' is selected
    ##
    # Add source
    ##
    When the startup is set to input sources
    And the source location is set to a 'polygon' with coordinates 129662 418727 130223 418710 130176 419349 129719 419302 129726 418912 129659 418895
    And the source label is set to 'Farmer John'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    # Add items
    And the farming emission is added a rav-code 'D 1.1.2' with bwl-code 'Groen Label BB 94.06.021V3' and an amount of 50
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a rav-code 'E 2.100' with bwl-code 'BWL 2001.07' and an amount of 9
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a rav-code 'F 1.100' with bwl-code 'Overig' and an amount of 450
    And the source sub item is saved
    # Delete items
    And the source sub item with label 'E 2.100 (Dieraantal: 9)' is selected and deleted
    And the source sub item with label 'F 1.100 (Dieraantal: 450)' is selected and deleted
    # Add items
    And the source is added a new sub item
    And the farming emission is added a rav-code 'E 2.12.1' with bwl-code 'BWL 2004.11' and an amount of 9
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a rav-code 'F 1.2' with bwl-code 'BWL 2006.02.V3' and an amount of 60
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a rav-code 'A 1.28' with bwl-code 'BWL 2001.28.V1' and an amount of 30
    And the source sub item is saved
    # Add / handle  custom lodges
    And the source is added a new sub item
    And the farming emission is added a custom lodge 'Chickens' with an emissionfactor of 0.35 and an amount of 60
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a custom lodge 'Turkeys' with an emissionfactor of 0.65 and an amount of 33
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a custom lodge 'Cows' with an emissionfactor of 1.5 and an amount of 15
    And the source sub item is saved
    #
    And the source sub item with label 'Turkeys' is selected and deleted
    And the source sub item with label 'Cows' is selected and deleted
    #
    And the source is added a new sub item
    And the farming emission is added a custom lodge 'Birds' with an emissionfactor of 0.55 and an amount of 60
    And the source sub item is saved
    And the source is added a new sub item 
    And the farming emission is added a custom lodge 'Black Sheep' with an emissionfactor of 1.2 and an amount of 13
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a custom lodge 'Alien Ants' with an emissionfactor of 0.06 and an amount of 9000
    And the source sub item is saved
    # Delete items
    And the source sub item with label 'D 1.1.2 (Dieraantal: 50)' is selected and deleted
    # Set characteristics
    And the source is saved
    #
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Farmer John' with emission row label 'E 2.12.1' should be 0,6
    Then the source emission of source 'Farmer John' with emission row label 'F 1.2' should be 3,0
    Then the source emission of source 'Farmer John' with emission row label 'A 1.28' should be 180,0
    Then the source emission of source 'Farmer John' with emission row label 'Chickens' should be 21,0
    Then the source emission of source 'Farmer John' with emission row label 'Birds' should be 33,0
    Then the source emission of source 'Farmer John' with emission row label 'Black Sheep' should be 15,6
    Then the source emission of source 'Farmer John' with emission row label 'Alien Ants' should be 540,0
    #
    And the map is zoomed out 3 times
    # Add source
    When a new source is added
    And the source location is set to a point with coordinates X 129918 and Y 419196
    And the source label is set to 'Farmer John Turkey'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a rav-code 'F 1.7' with bwl-code 'BWL 2010.13.V4' and an amount of 30
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a rav-code 'F 1.4' with bwl-code 'BWL 2011.03.V1' and an amount of 50
    And the source sub item is saved
    And the source is added a new sub item    
    And the farming emission is added a custom lodge 'Christmas Turkey' with an emissionfactor of 0.11 and an amount of 60
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a custom lodge 'Super Turkey' with an emissionfactor of 0.08 and an amount of 60
    And the source sub item is saved
    #
    And the source sub item with label 'Super Turkey' is selected and deleted
    And the source is saved
    #
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Farmer John Turkey' with emission row label 'F 1.7' should be 1,5
    Then the source emission of source 'Farmer John Turkey' with emission row label 'F 1.4' should be 2,5
    Then the source emission of source 'Farmer John Turkey' with emission row label 'Christmas Turkey' should be 6,6
    # Add source
    When a new source is added
    And the source location is set to a 'polygon' with coordinates 129854 419606 129810 420214 130126 420247 130156 419565 129982 419548 129971 419612
    And the source label is set to 'Farmer John Cow Droppings'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4130
    And the source emission is set to NH3 300, NOX 150, PM10 0
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Farmer John Cow Droppings' with emission row label 'NOx' should be 150,0
    Then the source emission of source 'Farmer John Cow Droppings' with emission row label 'NH3' should be 300,0
    #Copy source and edit
    When the source with label 'Farmer John Turkey' is selected
    And the selected source is copied
    And the source label is set to 'Farmer John Kangaroos'
    And the source sub item with label 'F 1.7 (Dieraantal: 30)' is selected and deleted
    And the source sub item with label 'F 1.4 (Dieraantal: 50)' is selected and deleted
    And the source sub item with label 'Christmas Turkey' is selected and deleted
    And the farming emission is added a custom lodge 'Kangaroos' with an emissionfactor of 0.9 and an amount of 25
    And the source sub item is saved
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    #
    Then the source emission of source 'Farmer John Kangaroos' with emission row label 'Kangaroos' should be 22,5
    # Perform calculation on short range, should not give results in table or filter
    # removed nature areas option (temporarily?)
#    When the taskbar options-panel is opend
#    And the calculation scope is set to Nature Areas
#    And the calculation range is set to 3 km
#    And the taskbar panel is closed
    When the notifications are all deleted
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    When the menu item -results- is selected
#    And the sub menu item -results filter- is selected
#    And the taskbar substance is set to 'NH3'
#    And the taskbar substance is set to 'NOx'
#    And the taskbar substance is set to 'NOx+NH3'
    Then the notification panel contains no errors
    And the notifications are all deleted
    # Perform calculation on short range, should not give results in table or filter
    # Selecting emission sources after map zoom to work around the issue where clicks
    # on the calculate button are blocked by the HabitatFilterGraph in results panel
    When the map is zoomed out 5 times
    And the menu item -sources- is selected
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    When the menu item -results- is selected
#    And the sub menu item -results filter- is selected
#    And the taskbar substance is set to 'NH3'
#    And the taskbar substance is set to 'NOx'
#    And the menu themeswitch is set to 'Luchtkwaliteit'
    When the menu item -results- is selected
    And the sub menu item -results table- is selected
    And the taskbar substance is set to 'NOx+NH3'
    And the taskbar info-panel is opened
    And the taskbar panel is closed
    Then the notification panel contains no errors
    Given an export to PAA OWN USE is started, with coorporation 'Farm Company', project 'Moo Cows', location 'Brabantstraat, 2233 HG, Brabantcity', description 'If a calf is drowned, one mutes the pit'
    Then the notification panel contains no errors

  @comparison @farmlodging2 @AER-1727
  Scenario: Work with different sources
    # Set location
    Given AERIUS CALCULATOR is open
    When the search for 'loevestein' is started
    And the search suggestion 'Loevestein, Pompveld & Kornsche Boezem' is selected
    ##
    # Add source
    ##
    When the startup is set to input sources
    And the source location is set to a 'polygon' with coordinates 129662 418727 130223 418710 130176 419349 129719 419302 129726 418912 129659 418895
    And the source label is set to 'Farmer John'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    # Add item normal farm
    And the farming emission is added a rav-code 'D 1.1.2' with bwl-code 'Groen Label BB 94.06.021V3' and an amount of 100
    # Add additional measure
    And the farming additional is added 'E 6.4.1.a' and amount of 101
    And the farming additional is added 'E 6.4.1.b' and amount of 61
    And the farming additional is added 'E 6.4.2.a' and amount of 102
    And the farming additional is added 'E 6.4.2.b' and amount of 62
    And the farming additional is added 'E 6.1.a' and amount of 100
    # delete additional
    And the farming emission additional at row 2 is deleted
    And the farming emission additional at row 2 is deleted
    And the farming emission additional at row 2 is deleted
    # add additional
    And the farming additional is added 'E 6.4.2.a' and amount of 102
    # Add reductive measure
    And the farming reductive is added 'A 4.5.1'
    And the farming reductive is added 'A 4.5.2'
#    And the farming reductive is added 'D1.1.14.2'
    And the farming reductive is added 'A 4.6'
    # Delete reductive
    And the farming emission reductive at row 2 is deleted
    And the farming emission reductive at row 3 is deleted
    # add reductive
    And the farming reductive is added 'A 4.5.2'
    And the farming emission reductive at row 2 is deleted
    And the source sub item is saved
    #
    And the source is added a new sub item
    And the farming emission is added a rav-code 'E 1.5.1' with bwl-code 'voormalig Groen Label BB 93.06.008' and an amount of 200
    And the farming additional is added 'E 6.5.a' and amount of 100
    And the source sub item is saved
    # copy subitem
    And the source sub item with label 'E 1.5.1 (Dieraantal: 200)' is selected to copy
    And the farming emission is added a rav-code 'E 1.5.2' with bwl-code 'Groen Label BB 97.07.058' and an amount of 200
    And the source sub item is saved
    And the source sub item with label 'E 1.5.2 (Dieraantal: 200)' is selected to edit
    And the farming emission additional at row 1 is deleted
    And the farming additional is added 'E 6.6.a' and amount of 200
    And the farming reductive is added 'E 1.12'
    And the source sub item is saved
    And the source is saved
    #
    Then the source emission of source 'Farmer John' with emission row label 'D 1.1.2' should be 0,3
    Then the source emission of source 'Farmer John' with emission row label 'E 1.5.1' should be 4,9
    Then the source emission of source 'Farmer John' with emission row label 'E 1.5.2' should be 1,4
    # create alternate
    When the alternate situation is created
    And the source with label 'Farmer John' is selected
    And the selected source is edited
    And the source label is set to 'Farmer Richard'
    And the source sub item with label 'E 1.5.1 (Dieraantal: 200)' is selected and deleted
    #
    And the source sub item with label 'E 1.5.2 (Dieraantal: 200)' is selected to copy
    And the farming emission is added a rav-code 'E 1.5.3' with bwl-code 'BWL 2001.31.V2' and an amount of 400
    And the farming emission reductive at row 1 is deleted
    And the farming emission additional at row 1 is deleted
    And the farming additional is added 'E 6.7.a' and amount of 200
    And the source sub item is saved
    #
    And the source sub item with label 'D 1.1.2 (Dieraantal: 100)' is selected to edit
    And the farming emission reductive at row 1 is deleted
    And wait for 1 seconds
    And the source sub item is saved
    And the source is saved
    #
    Then the source emission of source 'Farmer Richard' with emission row label 'D 1.1.2' should be 0,3
    Then the source emission of source 'Farmer Richard' with emission row label 'E 1.5.2' should be 1,4
    Then the source emission of source 'Farmer Richard' with emission row label 'E 1.5.3' should be 1,4
    # copy source
    When the source with label 'Farmer Richard' is selected
    And the selected source is copied
    And the source label is set to 'Farmer Richards Wive'
    And the source sub item with label 'D 1.1.2 (Dieraantal: 100)' is selected and deleted
    And the source sub item with label 'E 1.5.2 (Dieraantal: 200)' is selected and deleted
    # add new subsources
    And the source is added a new sub item
    And the farming emission is added a rav-code 'K 2' with bwl-code 'Overig' and an amount of 75
    And the farming reductive is added 'F 1.4'
    And the source sub item is saved
    # add custom lodge
    And the source is added a new sub item
    And the farming emission is added a custom lodge 'Hold your horses' with an emissionfactor of 1.63 and an amount of 69
    And the source sub item is saved
    And the source is saved
    #
    Then the source emission of source 'Farmer Richards Wive' with emission row label 'E 1.5.3' should be 1,4
    Then the source emission of source 'Farmer Richards Wive' with emission row label 'K 2.100' should be 47,3
    Then the source emission of source 'Farmer Richards Wive' with emission row label 'Hold your horses' should be 112,5
    # calculate
    # (ab)using priority project radius to limit calculation time
    When the taskbar options-panel is opened
    # TODO: Obsolete step priority project for item @AER-1718.
    And the calculation option priority project radius is enabled
    And the calculation option priority project type is set to 'Prioritair project Hoofdwegennet'
    And the taskbar panel is closed
#    And the calculation is started
#    Then the calculation ends
#    # change substance to view different results
#    When the taskbar substance is set to 'NOx'
#    And the taskbar substance is set to 'NH3'
#    And the taskbar substance is set to 'NOx+NH3'
#    Then the map is zoomed out 2 times
#    #
#    And the notification panel contains no errors
