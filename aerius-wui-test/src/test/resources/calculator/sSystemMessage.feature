@system-message-calculator @nightly
Feature: The system message allows an administrator to add messages to the AERIUS applications to inform users about certain events
    System messages can contain a link/html and adding/removing messages requires a unique key to authorize as administrator
    Messages support both dutch and english locale, anything else is applied as dutch by default
    All system messages can be deleted in one go by ticking the check box 

  Background: 
    Given AERIUS CALCULATOR is open
    Given the system message entry page is opened 

  @system-message-multilingual
  Scenario Outline: As an administrator i can set a multilingual system message using a unique authorization key
    Given the administrator enters message text '<message>'
    And the administrator enters locale '<locale>'
    And the administrator enters UUID 'bb8f8051-8e93-4f65-9177-689be4fe2897'
    When the system message is submitted
    Then the system message confirmation is shown
    # Check for message with user
    When AERIUS CALCULATOR is open
    Then the system message is shown with text '<message>'

    Examples: All testworthy combinations
    | message                                                   | locale | 
    | The quick brown fox jumps over the lazy dog...            | en     |
    | De snelle bruine vos springt over de luie hond...         | nl     |

  @system-message-link
  Scenario: As an administrator i can set a system message containing a URL 
    Given the administrator enters message text 'Voor meer informatie zie ook de <a href="http://www.aerius.nl/" target="_blank">AERIUS Website</a>'
    And the administrator enters locale 'nl'
    And the administrator enters UUID 'bb8f8051-8e93-4f65-9177-689be4fe2897'
    When the system message is submitted
    Then the system message confirmation is shown
    # Check for message and link with user
    When AERIUS CALCULATOR is open
    Then the system message is shown with link text 'AERIUS Website' pointing to url 'http://www.aerius.nl/'


  @system-message-delete
  Scenario: As an administrator i can delete all system messages
    Given the administrator ticks the delete all messages check box
    And the administrator enters UUID 'bb8f8051-8e93-4f65-9177-689be4fe2897'
    When the system message is submitted
    Then the system message confirmation is shown
    # Check for message absence with user
    When AERIUS CALCULATOR is open
    Then the system message is not shown

  @system-message-error
  Scenario: Using an invalid authorization key will cause the sytem to show an error  
    Given the administrator enters message text 'This should mot be shown'
    And the administrator enters locale 'nl'
    And the administrator enters UUID 'This-Is-Not-A-Valid-Key'
    When the system message is submitted
    Then the system message confirmation contains an error
    # Check for message absence with user
    When AERIUS CALCULATOR is open
    Then the system message is not shown


