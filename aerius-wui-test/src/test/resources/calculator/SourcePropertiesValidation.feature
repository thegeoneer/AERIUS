  @source-properties @nightly
Feature: Testing the default and boundary values of the source properties

  Background:
    Given AERIUS CALCULATOR is open
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 129918 and Y 419196

  @source-properties-default-values @AER-1770 @AER-1852
  Scenario Outline: Checking the default values of the input fields from the source properties
    Given the source sector is set to sectorgroup <Sector>
    And the source characteristics is toggled

    #Not forced without building influence
    Then check that source property field 'Emission height' is <EH default>
    And check that source property field 'Heat content' is <HC default>
    
    #Not forced with building influence
    When building influence is selected
    Then check that source property field 'Building length' is 10,0
    And check that source property field 'Building width' is 1,5
    And check that source property field 'Building height' is 0,0
    And check that source property field 'Building orientation' is 0
    And check that source property field 'Emission height' is <EH influence>
    And check that source property field 'Heat content' is 0,000
    
    #Forced without building influence
    When the properties are toggled to forced
    Then check that source property field 'Emission height' is <EH default>
    And check that source property field 'Emission temperature' is 11,85
    And check that source property field 'Outflow diameter' is 0,1
    And check that source property field 'Outflow exit direction' is Verticaal
    And check that source property field 'Outflow velocity' is 0,0
    
    #Forced with building influence
    When building influence is selected
    Then check that source property field 'Building length' is 10,0
    And check that source property field 'Building width' is 1,5
    And check that source property field 'Building height' is 0,0
    And check that source property field 'Building orientation' is 0
    And check that source property field 'Emission height' is <EH influence>
    And check that source property field 'Emission temperature' is 11,85
    And check that source property field 'Outflow diameter' is 0,1
    And check that source property field 'Outflow exit direction' is Verticaal
    And check that source property field 'Outflow velocity' is 0,0
    
    #EH = Emission height, HC = Heat content
    Examples: Variable input fields per sector
    | Sector                         | EH default | EH influence | HC default |
    | ENERGY with no sector          | 40,0       | 20,0         | 0,220      |
    | AGRICULTURE with sector 4110   | 5,0        | 5,0          | 0,000      |
    | AGRICULTURE with sector 4120   | 5,0        | 5,0          | 0,000      |
    | AGRICULTURE with sector 4130   | 0,5        | 0,5          | 0,000      |
    | AGRICULTURE with sector 4140   | 0,5        | 0,5          | 0,000      |
    | AGRICULTURE with sector 4320   | 8,0        | 8,0          | 0,400      |
    | AGRICULTURE with sector 4600   | 9,0        | 9,0          | 0,000      |
    | LIVE_AND_WORK with sector 8200 | 1,0        | 1,0          | 0,000      |
    | LIVE_AND_WORK with sector 8210 | 1,0        | 1,0          | 0,000      |
    | LIVE_AND_WORK with sector 8640 | 11,0       | 11,0         | 0,014      |
    | INDUSTRY with sector 1050      | 3,5        | 3,5          | 0,500      |
    | INDUSTRY with sector 1100      | 15,0       | 15,0         | 0,340      |
    | INDUSTRY with sector 1300      | 12,0       | 12,0         | 0,175      |
    | INDUSTRY with sector 1400      | 17,0       | 17,0         | 0,440      |
    | INDUSTRY with sector 1500      | 13,0       | 13,0         | 0,050      |
    | INDUSTRY with sector 1700      | 10,0       | 10,0         | 0,000      |
    | INDUSTRY with sector 1800      | 22,0       | 20,0         | 0,280      |

  @source-properties-validation @AER-1770 @AER-1852
  Scenario: Testing boundry values from not forced properties without Building influence
    Given the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the source characteristics is toggled
    When the source property emission height is set to 4999,05
    Then the popup error message shows 'Uittreedhoogte moet een getal zijn groter of gelijk aan 0 en kleiner dan of gelijk aan 4999,0'
    When the source property emission height is set to 4999,04
    Then there are no validation errors
    When the source property heat content is set to 999,0005
    Then the popup error message shows 'Warmteinhoud moet een getal zijn groter of gelijk aan 0 en kleiner dan of gelijk aan 999'
    When the source property heat content is set to 999
    Then there are no validation errors

  @source-properties-validation @AER-1770 @AER-1852
  Scenario: Testing boundry values from not forced properties with Building influence
    Given the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the source characteristics is toggled
    And building influence is selected

    #Building length
    When the source property building length is set to 9,94
    Then the warning message for building influence is shown
    When the source property building length is set to 9,95
    Then the warning message for building influence is not shown
    When the source property building length is set to 105,05
    And the source property building width is set to 30
    Then the warning message for building influence is shown
    When the source property building length is set to 105,04
    Then the warning message for building influence is not shown

    #Building width
    When the source property building length is set to 100
    And the source property building width is set to 14,94
    Then the warning message for building influence is shown
    When the source property building width is set to 14,95
    Then the warning message for building influence is not shown
    When the source property building width is set to 100,05
    Then the warning message for building influence is shown
    When the source property building width is set to 100,04
    Then the warning message for building influence is not shown

    #Building height
    When the source property building height is set to 20,05
    Then the warning message for building influence is shown
    When the source property building height is set to 20,04
    Then the warning message for building influence is not shown

    #Building orientation
    When the source property building orientation is set to 180
    Then the popup error message shows 'Oriëntatie moet een getal zijn groter of gelijk aan 0 en kleiner dan of gelijk aan 179'
    When the source property building orientation is set to 179
    Then there are no validation errors
    
    #Emission height
    When the source property emission height is set to 20,05
    Then the warning message for building influence is shown
    And there are no validation errors
    When the source property emission height is set to 20,04
    Then the warning message for building influence is not shown
    And there are no validation errors
    
  @source-properties-validation @AER-1770 @AER-1852
  Scenario: Testing boundry values from forced properties without Building influence
    Given the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the source characteristics is toggled
    And the properties are toggled to forced
    
    #Emission height
    When the source property emission height is set to 4999,05
    Then the popup error message shows 'Uittreedhoogte moet een getal zijn groter of gelijk aan 0 en kleiner dan of gelijk aan 4999,0'
    When the source property emission height is set to 4999,04
    Then there are no validation errors
    
    #Emission temperature
    When the source property emission temperature is set to 2000,01
    Then the popup error message shows 'Temperatuur emissie moet een getal zijn groter of gelijk aan 0,00 en kleiner dan of gelijk aan 2000,00'
    When the source property emission temperature is set to 2000,00
    Then there are no validation errors
    
    #Outflow diameter
    When the source property outflow diameter is set to 0,04
    Then the popup error message shows 'Uittreeddiameter moet een getal zijn groter of gelijk aan 0,1 en kleiner dan of gelijk aan 30,0'
    When the source property outflow diameter is set to 0,05
    Then there are no validation errors
    When the source property outflow diameter is set to 30,05
    Then the popup error message shows 'Uittreeddiameter moet een getal zijn groter of gelijk aan 0,1 en kleiner dan of gelijk aan 30,0'
    When the source property outflow diameter is set to 30,04
    Then there are no validation errors
    
    #Outflow velocity
    When the source property outflow velocity is set to 50,05
    Then the popup error message shows 'Uittreedsnelheid moet een getal zijn groter of gelijk aan 0 en kleiner dan of gelijk aan 50,0'
    When the source property outflow velocity is set to 50,04
    Then there are no validation errors
    
  @source-properties-validation @AER-1770
  Scenario: Testing boundry values from forced properties with Building influence
    Given the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the source characteristics is toggled
    And the properties are toggled to forced
    And building influence is selected

    #Building length
    When the source property building length is set to 9,94
    Then the warning message for building influence is shown
    When the source property building length is set to 9,95
    Then the warning message for building influence is not shown
    When the source property building length is set to 105,05
    And the source property building width is set to 30
    Then the warning message for building influence is shown
    When the source property building length is set to 105,04
    Then the warning message for building influence is not shown

    #Building width
    When the source property building length is set to 100
    And the source property building width is set to 14,94
    Then the warning message for building influence is shown
    When the source property building width is set to 14,95
    Then the warning message for building influence is not shown
    When the source property building width is set to 100,05
    Then the warning message for building influence is shown
    When the source property building width is set to 100,04
    Then the warning message for building influence is not shown

    #Building height
    When the source property building height is set to 20,05
    Then the warning message for building influence is shown
    And there are no validation errors
    When the source property building height is set to 20,04
    Then the warning message for building influence is not shown
    And there are no validation errors

    #Building orientation
    When the source property building orientation is set to 180
    Then the popup error message shows 'Oriëntatie moet een getal zijn groter of gelijk aan 0 en kleiner dan of gelijk aan 179' 
    When the source property building orientation is set to 179
    Then there are no validation errors
   
    #Emission height
    When the source property emission height is set to 20,05
    Then the warning message for building influence is shown
    And there are no validation errors
    When the source property emission height is set to 20,04
    Then the warning message for building influence is not shown
    And there are no validation errors
    
    #Outflow diameter
    When the source property outflow diameter is set to 0,04
    Then the warning message for building influence is shown
    When the source property outflow diameter is set to 0,05
    Then the warning message for building influence is not shown
    Then there are no validation errors
    When the source property outflow diameter is set to 5,05
    Then the warning message for building influence is shown
    When the source property outflow diameter is set to 5,04
    Then there are no validation errors
    Then the warning message for building influence is not shown
    
    #Outflow velocity
    When the source property outflow velocity is set to 8,45
    Then the warning message for building influence is shown
    When the source property outflow velocity is set to 8,44
    Then the warning message for building influence is not shown
    Then there are no validation errors
