@single @delete-all @nightly @aer-1429
Feature: A confirmation dialog is shown where the user is required to confirm deletion of all sources 

  Background: 
    Given AERIUS CALCULATOR is open
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 90210 and Y 454545
    And the source label is set to 'Deletable Source'
    And the source sector is set to sectorgroup ENERGY with sector -
    And the source emission is set to NH3 90, NOX 60, PM10 0
    And the source is saved

  @delete-all-cancel
  Scenario: A user can click the DELETE ALL sources button and CANCEL the operation from a confirmation dialog
    Given the user clicks on delete all sources button
    When the user cancels the delete all sources dialog
    Then the source emission of source 'Deletable Source' with emission row label 'NOx' should be 60,0
    And the source emission of source 'Deletable Source' with emission row label 'NH3' should be 90,0

  @delete-all-confirm
  Scenario: A user can click the DELETE ALL sources button and CONFIRM the operation from a confirmation dialog
    Given the user clicks on delete all sources button
    When the user confirms the delete all sources dialog
    Then the source with label 'Deletable Source' is unavailable

  @comparison @delete-all-confirm
  Scenario: Deleting all sources in situation 2 should leave sources in situation 1 untouched
    Given the alternate situation is created
    And the user clicks on delete all sources button
    And the user confirms the delete all sources dialog
    And the source with label 'Deletable Source' is unavailable
    When situation 1 is selected
    Then the source emission of source 'Deletable Source' with emission row label 'NOx' should be 60,0
    And the source emission of source 'Deletable Source' with emission row label 'NH3' should be 90,0
