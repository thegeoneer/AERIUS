@bug-retest @nightly
Feature: Bug retest

  Background: 
    Given AERIUS CALCULATOR is open

  Scenario: BUG: Internal error when working with calculation points
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 113526 and Y 464340
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 100, NOX 2222, PM10 0
    And the source is saved
    And the menu item -calculationpoints- is selected
    And the calculationpoint location is set to coordinates X 105246 and Y 461545
    And the calculationpoint is saved
    And a new calculation point 'Rekenpunt b' is added at coordinates X 105646 Y 461245
    And a new calculation point 'Rekenpunt c' is added at coordinates X 105846 Y 460245
    And the calculationpoint with label 'Rekenpunt a' is deleted
    And the calculationpoint with label 'Rekenpunt c' is edited
    And the calculationpoint location is set to coordinates X 105246 and Y 461545
    And the calculationpoint is saved
    And the calculationpoint with label 'Rekenpunt c' is deleted
    And the calculationpoint with label 'Rekenpunt b' is selected
    Then the notification panel contains no errors

  @comparison
  Scenario: BUG: Internal error when 2 situations have no valid calculation results
    Given the startup is set to import a file
    When the GML file 'importBugRetest_NoResults.gml' is imported
    Then the notification panel contains no errors
    When the alternate situation is created
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors

  Scenario: BUG: Source not selectable after editing location
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 110730 and Y 463265
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 0, NOX 1200, PM10 0
    And the source is saved
    And the source with label 'Bron 1' is selected
    And the selected source is edited
    And the source location is edited
    And the source location is set to a point with coordinates X 110730 and Y 464265
    And the source is saved
    And the source with label 'Bron 1' is selected
    Then the selected source is edited

  Scenario: BUG: Calculation aborts when only one source with sector traffic is entered
    Given the search for 'mantinger' is started
    When the search suggestion 'Mantingerzand' is selected
    And the startup is set to input sources
    And the source location is set to a 'LINE' with coordinates 234283 533569 234512 533045
    And the source sector is set to sectorgroup ROAD_TRANSPORTATION with sector 3111
    And the road emission max speed is set to 120
    And the road emission is added a new STANDARD specification with category 'LIGHT_TRAFFIC' and the vehicle amount of 10000 and trafficjam 33
    And the source sub item is saved
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors

  Scenario: BUG: Max speed is empty when editing road source
    Given the search for 'binnenveld' is started
    When the search suggestion 'Binnenveld' is selected
    And the startup is set to input sources
    And the source location is set to a 'LINE' with coordinates 168887 447331 168789 447338
    And the source sector is set to sectorgroup ROAD_TRANSPORTATION with sector 3111
    And the road emission max speed is set to 80
    And the road emission is added a new EUROCLASS specification with category 'Bestelauto' and the vehicle amount of 15000
    And the source sub item is saved
    And the source is saved
    And a new source is added
    And the source location is set to a 'LINE' with coordinates 168860 447468 168772 447478
    And the source sector is set to sectorgroup ROAD_TRANSPORTATION with sector 3112
    And the road emission is added a new EUROCLASS specification with category 'Bestelauto' and the vehicle amount of 15000
    And the source sub item is saved
    And the source is saved
    And the source with label 'Bron 1' is selected
    And the selected source is edited
    And the source is saved
    Then no validation message is showing
    And the notification panel contains no errors

  @comparison @cal19ignore
  Scenario: BUG: Error on selecting resultstable after switching to theme air quality
    Given the search for 'binnenveld' is started
    When the search suggestion 'Binnenveld' is selected
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 167721 and Y 446675
    And the source label is set to 'Energy - NOx'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 60, NOX 30, PM10 0
    And the source is saved
    And the alternate situation is created
    And a new source is added
    And the source location is set to a point with coordinates X 167640 and Y 446964
    And the source label is set to 'Energy - NoNOx'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 90, NOX 0, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    When the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    And the menu themeswitch is set to 'Luchtkwaliteit'
    And the menu item -results- is selected
    And the sub menu item -results table- is selected
    And the sub menu item -results filter- is selected
    Then the notification panel contains no errors
    And the notifications are all deleted

  @comparison @cal19ignore
  Scenario: BUG: 1695 - Missing select option 'Developmentroom' in results when NH3 emission  
    Given the startup is set to input sources
    And the source location is set to a point with coordinates X 81686 and Y 459636
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 60, NOX 30, PM10 0
    And the source label is set to 'Energy'
    And the source is saved
    And the alternate situation is created
    And the source with label 'Energy' is selected
    And the selected source is edited
    And the source emission is set to NH3 63, NOX 36, PM10 0
    Then the source is saved
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    And the results table is set to display as Deposition space
    And the situations are switched
    And the results table is set to display as Deposition space

  @comparison
  Scenario: BUG: 1507 - Onvoorspelbaar gedrag geselecteerde bronn
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 90616 and Y 451337
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 90, NOX 180, PM10 0
    And the source label is set to 'Energy'
    And the source is saved
    And the alternate situation is created
    And a new source is added
    And the source location is set to a point with coordinates X 90616 and Y 454337
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 90, NOX 180, PM10 0
    And the source label is set to 'Energy 2'
    And the source is saved
    And the source with label 'Energy 2' is selected
    And situation 1 is selected
    Then the overview edit button is disabled

  @shipping
  Scenario: BUG: After importing an inland mooring source for editing, stream direction should not throw validation errors upon saving 
    Given the startup is set to import a file
    When the GML file 'AERIUS_inland_mooring.gml' is imported
    Then the notification panel contains the message 'Uw bestand is geïmporteerd.Er is 1 bron toegevoegd.'
    And the notification panel contains no errors
    When the source with label 'Nautical_Heavy' is edited
    And the source sub item with label 'BulkM9' is selected to edit
    And the source sub item is saved
    And the source is saved
    Then there are no validation errors
    And the notification panel contains no errors

  Scenario: BUG: AER-1278 - OPS Spread value only has merit for polygon sources and should be absent for point and line sources
    Given the startup is set to import a file
    When the GML file 'AERIUS_OPS_Spread.gml' is imported
    Then the notification panel contains the message 'Uw bestand is geïmporteerd.Er zijn 3 bronnen toegevoegd.'
    And the notification panel contains no errors
    # Line source
    When the source with label 'Lijn (+Spreiding)' is edited
    And the source sub item with label 'Spreaded' is selected to edit
    Then the source input field for OPS spread is absent
    And the source sub item is saved
    And the source is saved
    # Point source
    When the source with label 'Punt (+Spreiding)' is edited
    And the source sub item with label 'Spreading' is selected to edit
    Then the source input field for OPS spread is absent
    And the source sub item is saved
    And the source is saved
    # Polygon source
    When the source with label 'Oppervlak (+Spreiding)' is edited
    And the source sub item with label 'Sandwich Spread' is selected to edit
    Then the source input field for OPS spread is present
    And the source sub item is saved
    And the source is saved

  Scenario Outline: BUG: AER-557 - Traffic line sources specified as generic in GML are not calculated 
    Given the startup is set to import a file
    And the GML file '<import_file>' is imported
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors

    Examples: Test files with generic line source for each traffic sector type 
      | import_file         | 
      | GenericLine3111.gml |
      | GenericLine3112.gml |
      | GenericLine3113.gml |

  @rainy
  Scenario Outline: BUG: AER-557 - Attempts to calculate traffic as generic point or polygon sources should result in an error message 
    Given the startup is set to import a file
    And the GML file '<import_file>' is imported
    When the calculation is started
    And wait until the page is loaded
    Then the notification panel contains the message 'Een weg of weg netwerk mag geen punt- of vlakbron bevatten.'

    Examples: Test files with generic point and polygon source for each traffic sector type 
      | import_file          | 
      | GenericPoint3111.gml |
      | GenericPoint3112.gml |
      | GenericPoint3113.gml |
      | GenericPoly3111.gml  |
      | GenericPoly3112.gml  |
      | GenericPoly3113.gml  |

  @rainy
  Scenario Outline: BUG: AER-557 - Attempts to calculate shipping sources with invalid traffic sector should result in an error message 
    Given the startup is set to import a file
    And the GML file '<import_file>' is imported
    When the calculation is started
    And wait until the page is loaded
    Then the notification panel contains the message 'voor de scheepvaartbron;is een ongeldig sector'

    Examples: Test files with invalid shipping source for each traffic sector type  
      | import_file                | 
      | GenericLineCarShip3111.gml |
      | GenericLineCarShip3112.gml |
      | GenericLineCarShip3113.gml |

  @AER-1307 @AER-1670 @AER-1713 @AER-1737
  Scenario: BUG: AER-1307 - No calculation results for assessment points where a road source is combined with another source category
    Given the startup is set to import a file
    And the user imports file 'RoadAndEnergySource.gml'
    And the taskbar options-panel is opened
    And the calculation scope is set to Calculation Points
    And the taskbar panel is closed
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    And the result for calculationpoint with label 'Westduinpark & Wapendal (7 km)' is '0,01'
    And the result for calculationpoint with label 'Coepelduynen (10 km)' is '0,00'
    And the result for calculationpoint with label 'Solleveld & Kapittelduinen (10 km)' is '0,00'
    And the result for calculationpoint with label 'Meijendel & Berkheide (1 km)' is '0,07'
    And the result for calculationpoint with label 'De Wilck (11 km)' is '0,00'

  @AER-1754 @AER-1756
  Scenario: Check to see if the error message contains a comma instead of a dot
    Given the startup is set to import a file
    And the user imports file 'AERIUS_NoCalculationResult.gml'
    And the total emission label shows '< 0,1 ton/j'
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    And the results table shows error message: 'Er zijn geen rekenresultaten hoger dan 0,00 mol/ha/j.'

  @AER-1910
  Scenario: BUG: OPS aggregation error when calculating with industry with forced properties
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 129438 and Y 446922
    And the source sector is set to sectorgroup INDUSTRY with sector 1050
    And the source emission is set to NH3 0, NOX 100000, PM10 0
    And the source characteristics is toggled
    And the properties are toggled to forced
    And the source is saved
    And a new source is added
    And the source location is set to a point with coordinates X 129768 and Y 446895
    And the source sector is set to sectorgroup INDUSTRY with sector 1050
    And the source emission is set to NH3 0, NOX 100000, PM10 0
    And the source characteristics is toggled
    And the properties are toggled to forced
    And the source is saved
    And the menu item -calculationpoints- is selected
    And the calculationpoint location is set to coordinates X 168750 and Y 570919
    And the calculationpoint is saved
    And the taskbar options-panel is opened
    And the calculation scope is set to Calculation Points
    And the taskbar panel is closed
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
