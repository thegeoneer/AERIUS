@single @generic @nightly
Feature: Sectors Test

  Background: 
    Given AERIUS CALCULATOR is open

  Scenario Outline: Add a single generic source and start a calculation
    When the startup is set to input sources
    And the source location is set to a point with coordinates X <xcoordinate> and Y <ycoordinate>
    And the source label is set to 'Source A'
    And the source sector is set to sectorgroup <sectorgroup> with sector <sectornumber>
    And the source emission is set to NH3 <nh3>, NOX <nox>, PM10 <pm10>
    And the source is saved
    # removed nature areas option (temporarily?)
#    And the taskbar options-panel is opend
#    And  the calculation scope is set to Nature Areas
#    And the calculation range is set to <calc_range> km
#    And the taskbar panel is closed
    And the calculation is started
    Then the calculation ends
    And the notification panel contains no errors

    Examples: Single generic sources should be calculated correctly for reach available sector
      | xcoordinate | ycoordinate | sectorgroup         | sectornumber | nh3 | nox | pm10 | calc_range |
      | 188574      | 452082      | INDUSTRY            | 1400         | 250 | 600 | 100  | 1          |
      | 80290       | 449287      | ENERGY              | -            | 300 | 250 | 0    | 4          |
      | 91290       | 449287      | INDUSTRY            | 1800         | 250 | 600 | 100  | 1          |
      | 91290       | 449287      | INDUSTRY            | 1800         | 250 | 0   | 0    | 1          |
      | 91290       | 449287      | INDUSTRY            | 1800         | 0   | 600 | 0    | 1          |
      | 271794      | 586106      | LIVE_AND_WORK       | 8210         | 250 | 600 | 100  | 3          |
      | 192337      | 584117      | INDUSTRY            | 1800         | 250 | 600 | 100  | 1          |
      | 192337      | 584117      | INDUSTRY            | 1050         | 250 | 600 | 100  | 1          |
      | 192337      | 584117      | LIVE_AND_WORK       | 8640         | 250 | 600 | 100  | 2          |
      | 108794      | 515519      | ENERGY              | -            | 0   | 250 | 100  | 4          |
      | 108794      | 515519      | AGRICULTURE         | 4130         | 100 | 250 | 100  | 4          |
      | 108794      | 515519      | RAIL_TRANSPORTATION | 3720         | 100 | 250 | 100  | 4          |
