@calculator-manual-capture
Feature: Capture screenshots for the AERIUS CALCULATOR manual

  Background: 
    Given AERIUS CALCULATOR is open sized 1191 x 911

  @calculator-manual-gebruikkaart-zoomfunctie
  Scenario: Zoomfunctie (638)
    And take a screenshot for the 'calculator' manual named '638_GebruikKaart-Zoomfunctie-ZoomenEnSchuiven-Stap_1'

  @calculator-manual-gebruikkaart-zoekfunctie
  Scenario: Uitleg zoekbalk (639)
    When the startup is set to input sources
    And the search for 'wierden' is started
    And wait for 2 seconds
    And take a screenshot for the 'calculator' manual named '639_GebruikKaart-Zoekfunctie-UitlegZoekbalk-Stap_1'

  @calculator-manual-gebruikkaart-zoekfunctie-natuurgebied
  Scenario: Zoekfunctie natuurgebied (668)
    When the startup is set to input sources
    And the search for 'wierden' is started
    And the search suggestion 'Wierden' is expanded
    And wait for 1 seconds
    Then take a screenshot for the 'calculator' manual named '668_GebruikKaart-Zoekfunctie-NatuurgebiedZoeken-Stap_3'

  @calculator-manual-gebruikkaart-zoekfunctie-postcode
  Scenario: Zoekfunctie postcode (907)
    When the startup is set to input sources
    And the search for '2632' is started
    And the search suggestion '2632 (Nootdorp) - 8 km (15)' is expanded
    And wait for 1 seconds
    Then take a screenshot for the 'calculator' manual named '907_GebruikKaart-Zoekfunctie-PostcodeZoeken-Stap_2'

  @calculator-manual-gebruikkaart-zoekfunctie-navigatie
  Scenario: Zoekfunctie navigatie (1276)
    When the startup is set to input sources
    And the search for 'wierden' is started
    And the search suggestion 'Wierdense Veld' is selected
    Then take a screenshot for the 'calculator' manual named '1276_GebruikKaart-Zoekfunctie-Navigatie-Stap_4'

  @comparison @calculator-manual-gebruikkaart-berichtencentrum
  Scenario: Berichtencentrum (640)
    When the search for 'botshol' is started
    And the search suggestion 'Botshol' is selected
    And the map is panned left
    And the map is panned left
    And the map is panned left
    And the map is panned left
    And the map is panned left
    When the startup is set to input sources
    And the source location is set to a point with coordinates X 122031 and Y 473866
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source label is set to 'Oude bron'
    And the source emission is set to NH3 1500, NOX 3800, PM10 0
    And the source is saved
    And the alternate situation is created
    And the source with label 'Oude bron' is selected
    And the selected source is edited
    And the source label is set to 'Nieuwe bron'
    And the source emission is set to NH3 3500, NOX 6800, PM10 0
    And the source is saved
    And the namelabel switch for sources is toggled
    And the calculation is started
    And the calculation ends
    And the notification panel contains no errors
    And the calculation is started
    And the notifications panel is opened
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '640_GebruikKaart-Berichtencentrum-Stap_1'

  @calculator-manual-menufunties-basis
  Scenario: Menufunctie Basis (1291, 628, 747, 630, 1948, 1573, 629)
    When wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '1291_MenuFuncties-Hoofdmenu-Stap_1'
    And take a screenshot for the 'calculator' manual named '628_MenuFuncties-Helpfunctie-Stap_1'
    And the menu help is toggled on or off
    And the taskbar layer-panel is clicked
    And the taskbar layer-panel is clicked
    And take a screenshot for the 'calculator' manual named '747_MenuFuncties-Helpfunctie-Voorbeeld-Stap_2'
    And the menu help is toggled on or off
    When the taskbar substance list is opened
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '630_MenuFuncties-Themaswitch-Stap_1'
    And take a screenshot for the 'calculator' manual named '1948_MenuFuncties-Themaswitch-Stap_1'
    And the taskbar options-panel is opened
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '1573_MenuFuncties-Rekenvoorkeuren-Stap_1'
    And the taskbar panel is closed
    And the search for 'wierden' is started
    And the search suggestion 'Wierdense Veld' is selected
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 231414 and Y 488278
    And the taskbar year list is opened
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '629_MenuFuncties-UitlegJaartal-Stap_1'

  @calculator-manual-menufuncties-informatieknop
  Scenario: Menufunctie Informatieknop (633, 908, 634)
    And the search for 'wierden' is started
    And the search suggestion 'Wierdense Veld' is selected
    And the map is panned left
    And the map is clicked in the center to select a point
    And the taskbar info-panel is opened
    And the taskbar info-panel is toggeled for Habitatype
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '633_MenuFuncties-Informatieknop-Stap_1'
    And take a screenshot for the 'calculator' manual named '908_MenuFuncties-Informatieknop-Stap_2'
    And the taskbar info-panel is toggeled for area 'Wierdense Veld'
    And wait for 1 seconds
    And the taskbar info-panel is toggeled for habitat 'H7120ah'
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '634_MenuFuncties-Informatieknop-Stap_3'

  @calculator-manual-menufuncties-kaartlagen
  Scenario: Menufunctie Kaartlagen (2053, 636, 637, 1275)
    And take a screenshot for the 'calculator' manual named '2053_MenuFuncties-Kaartlagen-Stap_4'
    And the search for 'wierden' is started
    And the search suggestion 'Wierdense Veld' is selected
    And the map is clicked in the center to select a point
    And the map is zoomed out 5 times
    And the taskbar layer-panel is opened
    And take a screenshot for the 'calculator' manual named '636_MenuFuncties-Kaartlagen-Stap_1'
    And the map is zoomed in 4 times
    And the layer with the name 'Stikstofgevoelige habitattypen' is toggled on or off
    And the layer with the name 'Stikstofgevoelige habitattypen' is toggled collapse
    And the layer with the name 'Stikstofgevoelige habitattypen' has its opacity set to 65
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '637_MenuFuncties-Kaartlagen-Stap_2'
    And the layer with the name 'Stikstofgevoelige habitattypen' is toggled on or off
    And the layer with the name 'Stikstofgevoelige habitattypen' is toggled collapse
    And the layer with the name 'Habitattypen' is toggled on or off
    And the map is panned down
    And the map is panned down
    And the map is panned down
    And the map is panned down
    And the map is panned down
    And the habitat with code 'H7120ah: Herstellende hoogvenen, actief hoogveen' is selected from the filter
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '1275_MenuFuncties-Kaartlagen-Stap_3'

  @calculator-manual-menufunties-rekenpunten
  Scenario: Menufunctie Rekenpunten (1938, 697, 1940, 1278, 1279)
    When the search for 'wierdense' is started
    And the search suggestion 'Wierdense Veld' is selected
    And the map is zoomed out
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 231624 and Y 487978
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 0, NOX 1050, PM10 0
    And the source is saved
    And the menu item -calculationpoints- is selected
    And the calculationpoint label is set to 'Rekenpunt rand Wierdense Veld'
    And the calculationpoint location is set to coordinates X 230468 and Y 488160
    And take a screenshot for the 'calculator' manual named '1938_MenuFuncties-Rekenconfiguratie-Rekenpunten-Stap_2'
    And the calculationpoint is saved
    And the namelabel switch for calculation points is toggled
    And take a screenshot for the 'calculator' manual named '697_MenuFuncties-Rekenconfiguratie-BekijkBewerk-Stap_3'
    And all calculationpoints are deleted
    And the menu item -sources- is selected
    And the menu item -calculationpoints- is selected
    And the menu item -calculationpoints- is selected
    And the calculationpoint calculator is opened
    And the calculationpoint calculator boundary is set to '10'
    And the calculationpoint calculator starts calculation
    And wait for 3 seconds
    And take a screenshot for the 'calculator' manual named '1940_MenuFuncties-Rekenconfiguratie-Rekenpunten-Stap_3'
    And the calculationpoint calculator is closed
    And the map is zoomed out 2 times
    And the map is panned left
    And the map is panned left
    And the map is panned left
    And the map is panned left
    And take a screenshot for the 'calculator' manual named '1278_MenuFuncties-Rekenconfiguratie-BekijkBewerkSelectie-Stap_5'
    And all calculationpoints are deleted
    When the search for 'wierdense' is started
    And wait for 2 seconds
    And the search suggestion 'Wierdense Veld' is selected
    And wait for 5 seconds
    And take a screenshot for the 'calculator' manual named '1279_MenuFuncties-Rekenconfiguratie-Importeer rekenpunt-Stap_6'

  @calculator-manual-algemeen-starten
  Scenario: Algemeen Starten (780, 1684)
    And take a screenshot for the 'calculator' manual named '780_Algemeen-Starten-Stap_1'
    When the search for 'wierdense' is started
    And the search suggestion 'Wierdense Veld' is selected
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 231624 and Y 487978
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 0, NOX 1050, PM10 0
    And the source is saved
    And take a screenshot for the 'calculator' manual named '1684_Algemeen-Starten-Stap_2'

  @calculator-manual-algemeen-handmatigeinvoer
  Scenario: Algemeen Handmatige invoer (670, 1480, 662, 654, 658, 657, 2031, 4124, 748, 659, 967, 968)
    And the search for coordinates X 162115 and Y 462866 is started
    And the map is zoomed out 3 times
    And the startup is set to input sources
    And the source location is entered as a 'LINE' with coordinates 162245.62 462871.25,161960.02 462908.21,161967.16 462978.35
    And the source location is set to edit mode
    And take a screenshot for the 'calculator' manual named '670_Algemeen-HandmatigeInvoer-Stap_1.1'
    And the menu item -sources- is selected
    And the search for coordinates X 177197 and Y 454018 is started
    And the map is zoomed out 3 times
    And the map is panned right
    And a new source is added
    And the source location is entered as a 'POLYGON' with coordinates 177226.4 454069.24,177170.96 454010.44,177422.96 453773.56,177557.36 453879.4,177374.24 454067.56,177226.4 454069.24
    And take a screenshot for the 'calculator' manual named '1480_Algemeen-HandmatigeInvoer-Stap_1.2'
    And the menu item -sources- is selected
    And a new source is added
    And the search for coordinates X 178017 and Y 443831 is started
    And the map is zoomed out 3 times
    And wait for 5 seconds
    And take a screenshot for the 'calculator' manual named '662_Algemeen-HandmatigeInvoer-Stap_1'
    And the source location is set to a point with coordinates X 178017 and Y 443831
    And the source label is set to 'Industrie'
    And take a screenshot for the 'calculator' manual named '654_Algemeen-HandmatigeInvoer-Stap_2'
    And the source sector is set to sectorgroup INDUSTRY with no sector
    And take a screenshot for the 'calculator' manual named '658_Algemeen-HandmatigeInvoer-Stap_3'
    And the source sector is set to sectorgroup INDUSTRY with sector 1400
    And the source characteristics is toggled
    And wait for 2 seconds
    And take a screenshot for the 'calculator' manual named '657_Algemeen-HandmatigeInvoer_Kenmerken-Stap_4'
    And the properties are toggled to forced
    And take a screenshot for the 'calculator' manual named '2031_Algemeen-HandmatigeInvoer-Geforceerde-Uitstroom-Stap_5'
    And building influence is selected
    And take a screenshot for the 'calculator' manual named '4124_Algemeen-HandmatigeInvoer-Gebouwinvloed-Stap_6'
    And the source is saved
    And the source with label 'Industrie' is selected
    And the selected source is edited
    And the source emission is set to NH3 0, NOX 1000, PM10 0
    And take a screenshot for the 'calculator' manual named '748_Algemeen-HandmatigeInvoer-Stap_7'
    And the source is saved
    And the namelabel switch for sources is toggled
    And the source emission of source 'Industrie' with emission row label 'NOx' should be 1000,0
    And wait for 5 seconds
    And take a screenshot for the 'calculator' manual named '659_Algemeen-HandmatigeInvoer-Stap_8'
    And the source with label 'Industrie' is clicked on ID-tag
    And the menu item -sources- is selected
    And the map is zoomed out 3 times
    And wait for 5 seconds
    And take a screenshot for the 'calculator' manual named '967_Algemeen-HandmatigeInvoer-Stap_9'
    And take a screenshot for the 'calculator' manual named '968_Algemeen-HandmatigeInvoer-Stap_10'

  @calculator-manual-bestandenimporteren
  Scenario: Algemeen Bestanden Importeren (1433, 677, 678, 679, 680)
    And take a screenshot for the 'calculator' manual named '1433_Algemeen-BestandenImporteren-Stap_5'
    And the startup is set to import a file
    And wait for 2 seconds
    And take a screenshot for the 'calculator' manual named '677_Algemeen-BestandenImporteren-Stap_1'
    And take a screenshot for the 'calculator' manual named '678_Algemeen-BestandenImporteren-Stap_2'
    And the GML file 'importExample.gml' is imported
    And take a screenshot for the 'calculator' manual named '679_Algemeen-BestandenImporteren-Stap_3'
    And wait for 10 seconds
    And the map is zoomed in
    And take a screenshot for the 'calculator' manual named '680_Algemeen-BestandenImporteren-Stap_4'

  @calculator-manual-bestandenimporteren-handmatig
  Scenario: Algemeen Bestanden Importeren en Handmatig (1693)
    When the search for 'arkansasdreef' is started
    And the search suggestion 'Arkansasdreef, Utrecht' is selected
    And the map is zoomed out
    And the map is panned right
    And the map is panned right
    And the map is panned right
    And the map is panned right
    And the map is panned right
    And the map is panned right
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 134872 and Y 460202
    And the source label is set to 'Appartementen'
    And the source sector is set to sectorgroup LIVE_AND_WORK with sector 8200
    And the source emission is set to NH3 0, NOX 1000, PM10 0
    And the source is saved
    And the namelabel switch for sources is toggled
    And the source with label 'Appartementen' is selected
    And take a screenshot for the 'calculator' manual named '1693_Algemeen-BestandenImporteren-Stap_6'

  @calculator-manual-sectoren-eenvoudig
  Scenario: Sectoren, eenvoudige sectoren (1924, 1927, 1934, 1937)
    When the taskbar layer-panel is opened
    And the layer with the name 'Natuurgebieden' is toggled on or off
    And the taskbar panel is closed
    And the search for coordinates X 178017 and Y 443831 is started
    And the map is zoomed out
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 178017 and Y 443831
    And the source label is set to 'Energie'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source characteristics is toggled
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '1924_Sectoren-Energie-Stap_1'
    And the source is saved
    And all sources are deleted
    And a new source is added
    And the source location is set to a 'POLYGON' with coordinates 177925.02 443800.55,177996 443932.85,178180.38 443852.63,178093.44 443726.21,177925.02 443800.55
    And the source label is set to 'Woonhuizen'
    And the source sector is set to sectorgroup LIVE_AND_WORK with sector 8200
    And the source characteristics is toggled
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '1927_Sectoren-WonenEnWerken-Stap_1'
    And the source is saved
    And all sources are deleted
    And a new source is added
    And the source location is set to a point with coordinates X 178017 and Y 443831
    And the source label is set to 'Industrie'
    And the source sector is set to sectorgroup INDUSTRY with sector 1400
    And the source characteristics is toggled
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '1934_Sectoren-Industrie-Stap_1'
    And the source is saved
    And all sources are deleted
    When the search for coordinates X 98286 and Y 469651 is started
    And the map is zoomed out
    And a new source is added
    And the source location is set to a 'POLYGON' with coordinates 98218.18 469659.96,98283.7 469602.84,98252.62 469565.88,98188.78 469632.24,98218.18 469659.96
    And the source label is set to 'Jachthaven'
    And the source sector is set to sectorgroup OTHER with no sector
    And the source characteristics is toggled
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '1937_Sectoren-Anders-Stap_1'

  @calculator-manual-sectoren-landbouw
  Scenario: Sectoren, landbouw (2030, 664, 2001, 2011, 665)
    When the taskbar layer-panel is opened
    And the layer with the name 'Natuurgebieden' is toggled on or off
    And the taskbar panel is closed
    And the search for coordinates X 136058 and Y 434645 is started
    And the map is zoomed out 3 times
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 136058 and Y 434645
    And the source label is set to 'Nieuwe_stal'
    And the source sector is set to sectorgroup AGRICULTURE with no sector
    And take a screenshot for the 'calculator' manual named '2030_Sectoren-Landbouw-Stap_1'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a rav-code 'E 1.10' with bwl-code 'BWL 2006.02.V3' and an amount of 100
    And take a screenshot for the 'calculator' manual named '664_Sectoren-Landbouw-Stap_2'
    And the menu item -sources- is selected
    And a new source is added
    And the source location is set to a point with coordinates X 136058 and Y 434645
    And the source label is set to 'Nieuwe_stal'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a rav-code 'E 1.5.1' with bwl-code 'voormalig Groen Label BB 93.06.008' and an amount of 100
    And the farming additional code is set to 'dro' to open the list
    And take a screenshot for the 'calculator' manual named '2001_Sectoren-Landbouw-Stap_3'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the menu item -sources- is selected
    And a new source is added
    And the source location is set to a point with coordinates X 136058 and Y 434645
    And the source label is set to 'Nieuwe_stal'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a rav-code 'E 1.5.1' with bwl-code 'voormalig Groen Label BB 93.06.008' and an amount of 10
    And the farming reductive code '12' to open te list
    And take a screenshot for the 'calculator' manual named '2011_Sectoren-Landbouw-Stap_4'
    And the menu item -sources- is selected
    And a new source is added
    And the source location is set to a point with coordinates X 136058 and Y 434645
    And the source label is set to 'Nieuwe_stal'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a custom lodge 'Ganzen' with an emissionfactor of 0.32 and an amount of 200
    And take a screenshot for the 'calculator' manual named '665_Sectoren-Landbouw-Stap_5'

  @calculator-manual-sectoren-mobielewerktuigen
  Scenario: Sectoren, mobiele werktuigen (1272, 1273, 1422, 1424)
    When the startup is set to import a file
    And wait for 2 seconds
    And the GML file 'importManualMobileSources.gml' is imported
    And wait for 5 seconds
    And the source with label 'Zandwinning' is clicked on ID-tag
    And the map is zoomed out 4 times
    And the source with label 'Zandwinning' is selected
    And the selected source is edited
    And the source sub item with label 'Dumper 1' is selected to edit
    And take a screenshot for the 'calculator' manual named '1272_Sectoren-MobieleWerktuigen-Stap_1'
    And the menu item -sources- is selected
    And the source with label 'Zandwinning' is selected
    And the selected source is edited
    And the source sub item with label 'Dumper 2' is selected to edit
    And take a screenshot for the 'calculator' manual named '1273_Sectoren-MobieleWerktuigen-Stap_2'
    And the offroad emission item calculator is opened 
    And the offroad emission item calculator is set for LOAD-USAGE with machinery 'asfalt afwerkinstallaties 20 kW, bouwjaar vanaf 2002', fueltype 'Diesel', power 240, load 100, usage 1400, emissionfactor 3.3
    And the offroad emission item calculator is set for LOAD-USAGE with machinery 'Anders', fueltype 'Diesel', power 240, load 100, usage 1400, emissionfactor 3.3
    And take a screenshot for the 'calculator' manual named '1422_Sectoren-MobieleWerktuigen-Stap_3'
    And the offroad emission item calculator is set for FUEL-USAGE with machinery 'asfalt afwerkinstallaties 20 kW, bouwjaar vanaf 2002', fueltype 'Diesel', fuelusage 100000, efficiency 250, emissionfactor 3.3
    And the offroad emission item calculator is set for FUEL-USAGE with machinery 'Anders', fueltype 'Diesel', fuelusage 100000, efficiency 250, emissionfactor 3.3
    And take a screenshot for the 'calculator' manual named '1424_Sectoren-MobieleWerktuigen-Stap_4'

  @calculator-manual-sectoren-plan
  Scenario: Sectoren Plan (1267, 1268, 1269, 1270, 1271)
    When the search for 'arkansasdreef' is started
    And the search suggestion 'Arkansasdreef, Utrecht' is selected
    And the map is zoomed out
    And the map is panned right
    And the map is panned right
    And the map is panned right
    And the map is panned right
    And the map is panned right
    And the map is panned right
    And the startup is set to input sources
    And the source location is entered as a 'POLYGON' with coordinates 134707.08 460218.04,134824.68 460360.84,135025.44 460222.24,134874.24 460046.68,134707.08 460218.04
    And the source location is set to edit mode
    And take a screenshot for the 'calculator' manual named '1267_Sectoren-Plan-Stap_1'
    And the source location is saved
    And the source label is set to 'Nieuwbouw_Utrecht'
    And take a screenshot for the 'calculator' manual named '1268_Sectoren-Plan-Stap_2'
    And the source sector is set to sectorgroup PLAN with no sector
    And the plan emission item is given name 'Appartementen Utrecht', category '1' and amount 20
    And take a screenshot for the 'calculator' manual named '1269_Sectoren-Plan-Stap_3'
    And the source sub item is saved
    And the source is added a new sub item
    And the plan emission item is given name 'Winkels', category '6' and amount 20
    And the source sub item is saved
    And the source sub item with label 'Appartementen Utrecht' is only selected
    And take a screenshot for the 'calculator' manual named '1270_Sectoren-Plan-Stap_4'
    And the source is saved
    And the namelabel switch for sources is toggled
    And the source with label 'Nieuwbouw_Utrecht' is selected
    And take a screenshot for the 'calculator' manual named '1271_Sectoren-Plan-Stap_5'

  @comparison @calculator-manual-vergelijker-varianten
  Scenario: Vergelijker Varianten (682, 683, 684, 686, 687)
    When the taskbar layer-panel is opened
    And the layer with the name 'Natuurgebieden' is toggled on or off
    And the taskbar panel is closed
    When the startup is set to input sources
    And the search for coordinates X 178017 and Y 443831 is started
    And wait for 5 seconds
    And the source location is set to a point with coordinates X 178017 and Y 443831
    And the source label is set to 'Oude bron'
    And the source sector is set to sectorgroup INDUSTRY with sector 1400
    And the source emission is set to NH3 0, NOX 1000, PM10 0
    And the source is saved
    And the namelabel switch for sources is toggled
    And the source with label 'Oude bron' is selected
    And take a screenshot for the 'calculator' manual named '682_Vergelijker-Varianten-Stap_1'
    And the alternate situation is created
    And the source with label 'Oude bron' is selected
    And take a screenshot for the 'calculator' manual named '683_Vergelijker-Varianten-Stap_2'
    And the selected source is edited
    And the source label is set to 'Nieuwe bron'
    And the source sector is set to sectorgroup INDUSTRY with sector 1700
    And the source emission is set to NH3 100, NOX 500, PM10 105
    And the source is saved
    And the source with label 'Nieuwe bron' is selected
    And the selected source is edited
    And wait for 2 seconds
    And take a screenshot for the 'calculator' manual named '684_Vergelijker-Varianten-Stap_3'
    And the source is saved
    And the comparison tab is selected
    And take a screenshot for the 'calculator' manual named '686_Vergelijker-Varianten-Stap_4'
    And the map is zoomed out 2 times
    And the calculation is started
    And the calculation ends
    And the sub menu item -results graph- is selected
    And take a screenshot for the 'calculator' manual named '687_Vergelijker-Varianten-Stap_5'

#  @calculator-manual-depositieberekening-berekenen @AER-1727
  Scenario: Depositieberekening Berekenen (692, 763, 1280, 1281, 699)
    When the taskbar layer-panel is opened
    And the layer with the name 'Natuurgebieden' is toggled on or off
    And the taskbar panel is closed
    When the startup is set to input sources
    And the search for coordinates X 256170 and Y 463358 is started
    And wait for 5 seconds
    And the map is zoomed out 3 times
    # Avoiding the internal error message for this specific hexagon (under investigation) 
#    And the source location is set to a point with coordinates X 256170 and Y 463358
    And the source location is set to a point with coordinates X 256170 and Y 463359
    And the source label is set to 'Industrie'
    And the source sector is set to sectorgroup INDUSTRY with sector 1400
    And the source emission is set to NH3 1000, NOX 1500, PM10 1000
    And the source is saved
    And the calculation is started
    And take a screenshot for the 'calculator' manual named '692_Depositieberekening-Berekenen-Stap_1'
    And the calculation ends
    And the sub menu item -results graph- is selected
    And the notifications panel is opened
    And wait for 2 seconds
    And take a screenshot for the 'calculator' manual named '763_Depositieberekening-Berekenen-Stap_2'
    And the notifications panel is closed
    And the sub menu item -results table- is selected
    And the results table is set to display as Average
    And the results table is set to show the area 'Witte Veen'
    And wait for 1 seconds
    And take a screenshot for the 'calculator' manual named '1280_Depositieberekening-Berekenen-Stap_3'
    #Steps skipped because of removed filter option
#    And the sub menu item -results filter- is selected
#    And the results filter is set to show the area 'Witte Veen'
#    And the results filter is set to show the habitat 'H4030 - Droge heiden'
#    And the results filter lower sliderknob is moved up 3 bars
#    And the results filter upper sliderknob is moved down 5 bars
    And take a screenshot for the 'calculator' manual named '1281_Depositieberekening-Berekenen-Stap_4'
    #Steps skipped because of removed PDF Export.
    And the sub menu item -results graph- is selected
    And the export button is clicked
    And the export option PAA OWN USE is selected
    And wait for 2 seconds
    And take a screenshot for the 'calculator' manual named '699_Depositieberekening-Exporteren-Stap_1'

  @calculator-manual-sectoren-verkeerenvervoer
  Scenario: Sector Wegverkeer (1264, 1949, 1950)
    When the search for coordinates X 86867 and Y 444128 is started
    And wait for 5 seconds
    And the map is zoomed out 3 times
    And the startup is set to input sources
    And the source location is set to a 'LINE' with coordinates 86917.4 443912.12,86855.24 444167.48
    And the source label is set to 'Wegdeel 1'
    And the source sector is set to sectorgroup ROAD_TRANSPORTATION with sector 3111
    And the road emission max speed is set to 80
    And the road emission strict enforcement is toggled
    And take a screenshot for the 'calculator' manual named '1264_Sectoren-VerkeerEnVervoer-VerkeerEnVervoer-Stap_1'
    And the road emission is added a new STANDARD specification with category 'LIGHT_TRAFFIC' and the vehicle amount of 68001 and trafficjam 0
    And take a screenshot for the 'calculator' manual named '1949_Sectoren-VerkeerEnVervoer-VerkeersEmissies-Stap_2'
    And the source sub item is saved
    And the source sub item with label 'Licht verkeer' is only selected
    And take a screenshot for the 'calculator' manual named '1950_Sectoren-VerkeerEnVervoer-MeerdereVerkeerstypen-Stap_3'

  @calculator-manual-sectoren-verkeerenvervoer-import
  Scenario: Sector Wegverkeer (1958, 1959, 1960)
    And take a screenshot for the 'calculator' manual named '1958_Sectoren-VerkeerEnVervoer-Importeren-Stap_1'
    And the startup is set to import a file
    And wait for 2 seconds
    And the CSV file 'ImporterenVerkeersnetwerk.csv' is imported
    And wait for 5 seconds
    And the namelabel switch for sources is toggled
    And the source with label 'ImporterenVerkeersnetwerk.csv' is clicked on ID-tag
    And the map is zoomed out 5 times
    And take a screenshot for the 'calculator' manual named '1959_Sectoren-VerkeerEnVervoer-Importeren-Stap_2'
    And the source with label 'ImporterenVerkeersnetwerk.csv' is selected
    And the selected source is edited
    And the road network tabel is selected at row '1'
    And take a screenshot for the 'calculator' manual named '1960_Sectoren-VerkeerEnVervoer-Importeren-Stap_3'

  @calculator-manual-sectoren-scheepvaart
  Scenario: Sector Scheepvaart (1262, 1263, 1574, 1429, 1261, 1575, 1576)
    When the taskbar layer-panel is opened
    And the layer with the name 'Natuurgebieden' is toggled on or off
    And the taskbar panel is closed
    And the startup is set to import a file
    And the GML file 'maritime_mooring_v0.5.gml' is imported
    And the source with label 'Aanlegplaatsbedrijf' is clicked on ID-tag
    And the map is zoomed out 5 times
    And the source with label 'Aanlegplaatsbedrijf' is selected
    And the selected source is edited
    And the source sub item with label 'Tankers' is selected to edit
    And take a screenshot for the 'calculator' manual named '1262_Sectoren-Scheepvaart-Stap_2'
    When the search for coordinates X 70027 and Y 442174 is started
    And the map is zoomed out 5 times
    And the menu item -sources- is selected
    And all sources are deleted
    And a new source is added
    And the source location is set to a 'LINE' with coordinates 71438.214567362 440951.20543264,68548.614567362 443330.08543264
    And the source label is set to 'Binnengaats route'
    And the source sector is set to sectorgroup SHIPPING with sector 7520
    And the maritime route ship name is set to 'Zeeschip'
    And the maritime route ship amount  is set to '1500'
    And the maritime route ship category is set to 'c'
    And take a screenshot for the 'calculator' manual named '1263_Sectoren-Scheepvaart-Stap_3'
    And the menu item -sources- is selected
    And a new source is added
    And the search for coordinates X 67729 and Y 448612 is started
    And wait for 5 seconds
    And the map is zoomed out 5 times
    And the map is panned left
    And the map is panned left
    And the source location is set to a 'LINE' with coordinates 64798.84 448033.84,67231.48 450694.96
    And the source label is set to 'Zeeroute'
    And the source sector is set to sectorgroup SHIPPING with sector 7530
    And the maritime route ship name is set to 'Zeeschip'
    And the maritime route ship category is set to 'Bulkschepen GT: 100-1599'
    And the maritime route ship amount  is set to '1000'
    And take a screenshot for the 'calculator' manual named '1574_Sectoren-Scheepvaart-Stap_4'
    And the menu item -sources- is selected
    And all sources are deleted
    And an import is started
    And the GML file 'inland_mooring_v0.5.gml' is imported
    And wait for 3 seconds
    And the source with label 'Aanlegplaats industrie' is clicked on ID-tag
    And the map is zoomed out 3 times
    And the map is panned up
    And the map is panned up
    And the map is panned up
    And the map is panned left
    And the map is panned left
    And the map is panned left
    And the source with label 'Aanlegplaats industrie' is selected
    And the selected source is edited
    And the source sub item with label 'Zandschepen' is selected to edit
    And take a screenshot for the 'calculator' manual named '1429_Sectoren-Scheepvaart-Stap_6'
    And the menu item -sources- is selected
    And all sources are deleted
    And an import is started
    And the GML file 'inland_route_v0.5.gml' is imported
    And wait for 5 seconds
    And the source with label 'Vaarroute' is clicked on ID-tag
    And the map is zoomed out 3 times
    And the source with label 'Vaarroute' is selected
    And the selected source is edited
    And the source sub item with label 'Zandschepen' is selected to edit
    And take a screenshot for the 'calculator' manual named '1261_Sectoren-Scheepvaart-Stap_7'
    And the menu item -sources- is selected
    And all sources are deleted
    And a new source is added
    And the search for coordinates X 65464 and Y 443239 is started
    And wait for 5 seconds
    And the map is zoomed out 6 times
    And the source location is set to a point with coordinates X 65464 and Y 443239
    And the source label is set to 'Scheepvaart'
    And the source sector is set to sectorgroup SHIPPING with no sector
    And take a screenshot for the 'calculator' manual named '1575_Sectoren-Scheepvaart-Stap_1'
    And the menu item -sources- is selected
    And all sources are deleted
    And a new source is added
    And the search for coordinates X 103608 and Y 422331 is started
    And wait for 5 seconds
    And the map is zoomed out 5 times
    And the source location is set to a point with coordinates X 103608 and Y 422331
    And the source label is set to 'Binnenvaart'
    And the source sector is set to sectorgroup SHIPPING with no sector
    And take a screenshot for the 'calculator' manual named '1576_Sectoren-Scheepvaart-Stap_5'

  @calculator-manual-sectoren-binnenvaart
  Scenario: Sector Binnenvaart (x, y, z)
    When the taskbar layer-panel is opened
    And the direct search for 'x:128434 y:425891' is started
    And the map is zoomed out 3 times
    And the layer with the name 'Binnenvaart' is toggled on or off
    And wait for 2 seconds
    And take a screenshot for the 'calculator' manual named '3778_Sectoren-Scheepvaart-Stap_6.1'
    And the taskbar panel is closed
    # draw line source
    And the startup is set to input sources
    And the source location is set to a 'line' with coordinates 128431.48 425891 128923.72 425899.4
    And the source label is set to 'Inland Shipping'
    And the source sector is set to sectorgroup SHIPPING with sector 7620
    And wait for 2 seconds
    And take a screenshot for the 'calculator' manual named '3779_Sectoren-Scheepvaart-Stap_6.2'
    # enable priority radius option
    # TODO: Commented out the steps for item @AER-1718.
#    And the taskbar options-panel is opened
#    And the calculation option priority project radius is enabled
#    And wait for 2 seconds
#    And take a screenshot for the 'calculator' manual named '3780_MenuFuncties-Rekenconfiguratie-Rekenpunten-Stap_1.2'
#    And the taskbar panel is closed
    # enter source specifics
    And the inland route ship is defined as 'Zandschepen' of type 'M8' making '40' round trips a day
    And the inland route ship A to B load percentage is set to '80'
    And the inland route ship B to A load percentage is set to '40'
    And take a screenshot for the 'calculator' manual named '1261_Sectoren-Scheepvaart-Stap_7'

  @scenario-railverkeer
  Scenario: Capture screenshot for rail traffic
    When the startup is set to input sources
    And the source location is set to a 'line' with coordinates 116334 459542 116818 460080
    And the source sector is set to sectorgroup RAIL_TRANSPORTATION with sector 3720
    And the map is zoomed in 3 times
    And the taskbar layer-panel is opened
    And the layer with the name 'Habitattypen' is toggled on or off
    And the taskbar panel is closed
    And wait for 2 seconds
    Then take a screenshot for the 'calculator' manual named '1930_Sectoren-Railverkeer-Stap_1'

  @scenario-luchtverkeer
  Scenario: Capture screenshot for aviation
    When the startup is set to input sources
    When the source location is set to a 'line' with coordinates 116334 459542 116818 460080
    And the source sector is set to sectorgroup AVIATION with sector 3620
    And the map is zoomed in 3 times
    And the taskbar layer-panel is opened
    And the layer with the name 'Habitattypen' is toggled on or off
    And the taskbar panel is closed
    And wait for 2 seconds
    Then take a screenshot for the 'calculator' manual named '1944_Sectoren-Luchtvaart-Stap_1'
