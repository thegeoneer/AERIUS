@nightly @calculator2019
Feature: Test that are specifically for Calculator 2019 version

  Background:
    Given AERIUS CALCULATOR is open

  @AER-1712
  Scenario: Background deposition should be the same for every year
    Given the search for '181163 470128' is started
    And the search suggestion 'x: 181163 y: 470128' is selected
    And the map is panned left
    And the map is clicked in the center to select a point
    When the taskbar info-panel is opened
    And the taskbar info-panel is toggeled for Depositie
    Then the taskbar year is set to year and the background deposition is '1996,72'
      | 2014 |
      | 2015 |
      | 2016 |
      | 2017 |
      | 2018 |
      | 2019 |
      | 2020 |
      | 2021 |
      | 2022 |
      | 2023 |
      | 2024 |
      | 2025 |
      | 2026 |
      | 2027 |
      | 2028 |
      | 2029 |
      | 2030 |

  @AER-1740
  Scenario: Check if the correct calculation markers are visible
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 181163 and Y 470128
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 10, NOX 10, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    When the search for '181163 470128' is started
    And the search suggestion 'x: 181163 y: 470128' is selected
    And the map is panned left
    And the map is clicked in the center to select a point
    When the taskbar info-panel is opened
    And the taskbar info-panel is toggeled for area 'Veluwe'
    Then the cursor pointer for 'highest contribution' is visible with image 'MAAAAASUVORK5CYII='
    And the cursor pointer for 'highest deposition' is visible with image '0AAAAASUVORK5CYII='
    And the map is zoomed out 3 times
    And the cursor pointer for 'highest contribution' is visible on the map with image 'YAAAAASUVORK5CYII='
    And the cursor pointer for 'highest deposition' is visible on the map with image 'UAAAAASUVORK5CYII='

    @AER-1797
    Scenario: Check if Nature Areas without habitats are not visible in the results
    Given the startup is set to import a file
    When the GML file 'AER-1797_VoornesDuin.gml' is imported
    Then the notification panel contains no errors
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    And the results panel only contains '14' label IDs
