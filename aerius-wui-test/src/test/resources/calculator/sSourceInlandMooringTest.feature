@nautical @nautical-inland @single
Feature: The UI for nautical emission sources provides a large set of input options for both inland and offshore sources.
  This feature covers the inland scenario's for @inland-mooring and @inland-route including input validation messages

  # These tests are not in the nightly due to a selenium doubleclick issue not allowing us to draw on the map
  Background: 
    Given AERIUS CALCULATOR is open
    Given the startup is set to input sources

  @inland-mooring @AER-1727
  Scenario: Full cycle inland mooring sources: add, modify, calculate, export, delete
    Given the source location is set to a 'line' with coordinates 62561 439180 64523 439879
    And the source label is set to 'inland.mooring(7610)'
    And the source sector is set to sectorgroup SHIPPING with sector 7610
    # test if the empty compulsory fields are caught by form validation 
    When the source sub item is saved
    Then a validation message is showing with the text 'Scheepvaart category '' niet geldig'
    And the inland mooring ship is a 'inland ship 1' of category 10 staying 24 hours
    # test if inland routes are compulsory by saving before entering inland routes
    When the source sub item is saved
#    Then a validation message is showing with the text 'Scheepvaart category '' niet geldig'
    And a validation message is showing with the text 'Geen route aangegeven'
    # continue adding mooring route
    And the inland mooring route at 'ARRIVE' row 0 is drawn with coordinates 65000 440000 65800 443800
    Then the inland mooring route row 1 must be 'visible'
    # test save should give error message, because other compulsory fields are empty
    And the inland mooring ship movements at 'ARRIVE' row 0 is set to 333
    When the source sub item is saved
    And the inland mooring direction at row 0 is set to 'Depart'
    And the source sub item is saved
    And a validation message is showing with the text 'Vaarbewegingen moet groter dan 0 zijn als een route is ingevuld'
    # test save should give error message, because other non-optional fields not entered.
    When the inland mooring ship movements at 'ARRIVE' row 0 is set to 334
    And the inland mooring load at 'ARRIVE' row 0 is set to 50
    And the source sub item is saved
    And the source sub item with label 'inland ship 1' is selected
    Then the inland mooring residence time must be 24
    And the inland mooring route at 0 must be 'A'
    And the inland mooring direction at row 0 must be 'Depart'
    And the inland mooring ship movements at 'ARRIVE' row 0 must be 334
    And the inland mooring load at 'ARRIVE' row 0 must be 50
    # save the source and check emission
    And the source sub item is saved
    And the source is saved
    And the taskbar substance is set to 'NOx'
    And the total emission of the source with label 'inland.mooring(7610)' should be 788
    # setup and initiate calculation
    Then the taskbar options-panel is opened
    And the calculation scope is set to Radius
    And the calculation range is set to 4 km
    And the taskbar panel is closed
    And the calculation is started
    And the calculation ends
    And the notification panel contains no errors
    # export a source only GML and a permit application PDF
    When an export to GML with sources only is started
    Then the notification panel contains no errors
    When an export to PAA OWN USE is started, with coorporation 'Inland Mooring (7610)', project 'Docker', location 'Havenbedrijf, 1234AB, Rotterdam', description 'Dock and cover...'
    Then the notification panel contains no errors

  @inland-mooring2 @AER-1727
  Scenario: Full cycle inland mooring sources: add, modify, calculate, export, delete
    Given the source location is set to a 'line' with coordinates 62561 439180 64523 439879
    And the source label is set to 'inland.mooring(7610)'
    And the source sector is set to sectorgroup SHIPPING with sector 7610
    # test if the empty compulsory fields are caught by form validation 
    When the source sub item is saved
    Then a validation message is showing with the text 'Scheepvaart category '' niet geldig'
    And the inland mooring ship is a 'inland ship 1' of category 10 staying 24 hours
    # test if inland routes are compulsory by saving before entering inland routes
    When the source sub item is saved
    And a validation message is showing with the text 'Geen route aangegeven'
    # continue adding mooring route
    And the inland mooring route at 'ARRIVE' row 0 is drawn with coordinates 65000 440000 65800 443800
    Then the inland mooring route row 1 must be 'visible'
    # test save should give error message, because other compulsory fields are empty
    When the source sub item is saved
    And the inland mooring direction at row 0 is set to 'Depart'
    And the source sub item is saved
    And a validation message is showing with the text 'Vaarbewegingen moet groter dan 0 zijn als een route is ingevuld'
    # test save should give error message, because other non-optional fields not entered.
    When the inland mooring ship movements at 'ARRIVE' row 0 is set to 334
    And the inland mooring load at 'ARRIVE' row 0 is set to 50
    And the source sub item is saved
    And the source sub item with label 'inland ship 1' is selected
    Then the inland mooring residence time must be 24
    And the inland mooring route at 0 must be 'A'
    And the inland mooring direction at row 0 must be 'Depart'
    And the inland mooring ship movements at 'ARRIVE' row 0 must be 334
    And the inland mooring load at 'ARRIVE' row 0 must be 50
    # save the source and check emission
    And the source sub item is saved
    And the source is saved
    And the taskbar substance is set to 'NOx'
    And the total emission of the source with label 'inland.mooring(7610)' should be 788
    # setup and initiate calculation
    Then the taskbar options-panel is opened
    And the calculation scope is set to Radius
    And the calculation range is set to 4 km
    And the taskbar panel is closed
    And the calculation is started
    And the calculation ends
    And the notification panel contains no errors
    # export a source only GML and a permit application PDF
    When an export to GML with sources only is started
    Then the notification panel contains no errors

  @inland-route @AER-1727
  Scenario: Test all shipping sectors and their specific subset of data entry options 
    Given the source location is set to a 'line' with coordinates 62561 439180 64523 439879
    And the source label is set to 'inland.route(7620)'
    And the source sector is set to sectorgroup SHIPPING with sector 7620
    # test if the empty compulsory fields are caught by form validation 
    When the source sub item is saved
    Then a validation message is showing with the text 'Scheepvaart category '' niet geldig'
    # continue defining the ship route
    When the inland route ship is defined as 'inland route 1' of type 'BI' making '6' round trips a day
    And the source sub item is saved
    Then the inland mooring route row 1 must be 'visible'
    # make a copy, edit attributes, save source and delete the original
    When the source sub item with label 'inland route 1' is selected to copy
    And the inland route ship is defined as 'inland route copy' of type 'BI' making '12' round trips a day
    And the source sub item is saved
    And the source sub item with label 'inland route 1' is selected and deleted
    And the source sub item with label 'inland route copy' is selected to edit
    # check what we have left
    Then the inland route ship type must be 'BI'
    And the inland route ship A to B movement must be '12'
    And the inland route ship A to B load percentage must be '65'
    And the inland route ship B to A movement must be '12'
    And the inland route ship B to A load percentage must be '65'
    # save the source and check emission
    When the source sub item is saved
    And the source is saved
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'inland.route(7620)' with emission row label 'inland route copy' should be 3196,6
    # setup and initiate calculation
    When the taskbar options-panel is opened
    And the calculation scope is set to Radius
    And the calculation range is set to 4 km
    And the taskbar panel is closed
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    # export a source only GML and a permit application PDF
    When an export to GML with sources only is started
    Then the notification panel contains no errors
