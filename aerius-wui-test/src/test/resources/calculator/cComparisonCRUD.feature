@composite @comparison @nightly
Feature: Correct calculation and export when situations are modified mutliple times

  # TODO: Obsolete step priority project for item @AER-1718.
  @AER-1718
  Scenario: Set up the basic scenario and limit calculation time by enabling radius option  
    Given AERIUS CALCULATOR is open
    And the taskbar options-panel is opened
    And the calculation option priority project radius is enabled
    And the calculation option priority project type is set to 'Prioritair project Hoofdwegennet'
    And the taskbar panel is closed
    And the taskbar year is set to 2020
    And the startup is set to input sources
    # Add sources

    When the source location is set to a point with coordinates X 179058 and Y 332120
    And the source label is set to 'Bouwterrein 1'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 139, NOX 150, PM10 0
    And the source is saved
    Then the source emission of source 'Bouwterrein 1' with emission row label 'NH3' should be 139,0
    And the source emission of source 'Bouwterrein 1' with emission row label 'NOx' should be 150,0
    And the source with label 'Bouwterrein 1' is clicked on ID-tag
    And the map is zoomed out 2 times
    # Add second source

    When a new source is added
    And the source location is set to a point with coordinates X 178833 and Y 332319
    And the source label is set to 'Bouwterrein 2'
    And the source sector is set to sectorgroup INDUSTRY with sector 1050
    And the source emission is set to NH3 175, NOX 240, PM10 0
    And the source is saved
    Then the source emission of source 'Bouwterrein 2' with emission row label 'NH3' should be 175,0
    And the source emission of source 'Bouwterrein 2' with emission row label 'NOx' should be 240,0
    # Add third source

    When a new source is added
    And the source location is set to a point with coordinates X 178995 and Y 331616
    And the source label is set to 'Stallen'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a rav-code 'E 2.12.1' with bwl-code 'BWL 2004.11' and an amount of 945
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a rav-code 'D 1.3.100' with bwl-code 'Overige' and an amount of 36
    And the source sub item is saved
    And the source characteristics is toggled
    And the source characteristics are set to height 4,0, heat 0,5
    And the source is saved
    Then the source emission of source 'Stallen' with emission row label 'E 2.12.1' should be 64,3
    And the source emission of source 'Stallen' with emission row label 'D 1.3.100' should be 151,2
    # Add fourth source

    When a new source is added
    And the source location is set to a point with coordinates X 179986 and Y 331371
    And the source label is set to 'Voertuigen'
    And the source sector is set to sectorgroup MOBILE_EQUIPMENT with sector 3230
    And the offroad emission item is set to STAGE class with name 'Landbouwvoertuigen', category 7 and fuel amount 11350
    And the source sub item is saved
    And the source is added a new sub item
    And the offroad emission item is set to CUSTOM class with name 'Trekker', height 1, heat 10 and NOX 130
    And the source sub item is saved
    And the source is saved
    Then the source emission of source 'Voertuigen' with emission row label 'Landbouwvoertuigen' should be 198,3
    And the source emission of source 'Voertuigen' with emission row label 'Trekker' should be 130,0

  Scenario: Add calculation points and calculate for year 2019 followed by an export to GML with sources
    Given the menu item -calculationpoints- is selected
    # Add calculation points (add button absent on first point)

    And the calculationpoint location is set to coordinates X 179025 and Y 331133
    And the calculationpoint label is set to 'CalcPoint A'
    And the calculationpoint is saved
    And a new calculation point 'CalcPoint B' is added at coordinates X 180120 Y 332208
    And a new calculation point 'CalcPoint C' is added at coordinates X 180906 Y 328599
    # Calculate

    When the taskbar year is set to 2019
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    # Export

    When an export to GML with sources only is started
    Then the notification panel contains no errors

  Scenario: Set up the second situation
    # Create and modify alternate situation
    # Test calculating with an empty alternate situation (buggy)

    Given the menu item -sources- is selected
    When the alternate situation is created
    And all sources are deleted
    And the taskbar year is set to 2020
    And the calculation is started
    Then the calculation ends
    And the notification panel contains no errors
    When the export button is clicked
    Then the notification panel contains the message 'Er zijn geen bronnen aangemaakt voor deze situatie. Hierdoor kunt u nu niet exporteren.'
    When the menu item -sources- is selected
    And situation 2 is deleted
    Then the notification panel contains no errors

  Scenario: Work with situations
    # Create, rename, switch and delete situations

    Given the menu item -sources- is selected
    And the notifications are all deleted
    When the alternate situation is created
    And situation 2 is renamed to 'Sit B'
    And situation 1 is renamed to 'Sit A'
    And the situations are switched
    And the situations are switched
    And situation 2 is deleted
    #
    And the alternate situation is created
    And all sources are deleted
    And the situations are switched
    And situation 1 is deleted
    #
    When the menu item -calculationpoints- is selected
    And all calculationpoints are deleted
    # Add calculation points

    And a new calculation point 'CalcPoint A' is added at coordinates X 179025 Y 331133
    And a new calculation point 'CalcPoint B' is added at coordinates X 180120 Y 332208
    And a new calculation point 'CalcPoint C' is added at coordinates X 180906 Y 328599
    #

    When the menu item -sources- is selected
    And the alternate situation is created
    And situation 2 is renamed to 'Sit B'
    Then the notification panel contains no errors

  Scenario: Edit sources in situation 2
    Given the menu item -sources- is selected
    And the notifications are all deleted
    # Edit source 1

    When the source with label 'Bouwterrein 1' is selected
    And the selected source is edited
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 139, NOX 120, PM10 0
    And the source is saved
    Then the source emission of source 'Bouwterrein 1' with emission row label 'NH3' should be 139,0
    And the source emission of source 'Bouwterrein 1' with emission row label 'NOx' should be 120,0
    # Edit source 2

    When the source with label 'Bouwterrein 2' is selected
    And the selected source is edited
    And the source emission is set to NH3 150, NOX 125, PM10 0
    And the source is saved
    Then the source emission of source 'Bouwterrein 2' with emission row label 'NH3' should be 150,0
    And the source emission of source 'Bouwterrein 2' with emission row label 'NOx' should be 125,0
    # Edit source 3

    When the source with label 'Stallen' is selected
    And the selected source is edited
    And the source is added a new sub item
    And the farming emission is added a rav-code 'H 1.1' with bwl-code 'Overig' and an amount of 10
    And the source sub item is saved
    And the source sub item with label 'D 1.3.100 (Dieraantal: 36)' is selected and deleted
    And the source is added a new sub item
    And the farming emission is added a custom lodge 'Vuurkikker' with an emissionfactor of 0.003 and an amount of 1135
    And the source sub item is saved
    And the source is saved
    Then the source emission of source 'Stallen' with emission row label 'E 2.12.1' should be 64,3
    And the source emission of source 'Stallen' with emission row label 'H 1.1' should be 5,8
    And the source emission of source 'Stallen' with emission row label 'Vuurkikker' should be 3,4
    # Replace source 4

    When the source with label 'Voertuigen' is selected
    And the selected source is deleted
    And a new source is added
    And the source location is set to a 'LINE' with coordinates 90442 446206 90250 446478
    And the source label is set to 'Parkeerterrein'
    And the source sector is set to sectorgroup ROAD_TRANSPORTATION with sector 3112
    And the road emission is added a new EUROCLASS specification with category 'Bestelauto LPG - Euro 3' and the vehicle amount of 500
    And the source sub item is saved
    And the source is added a new sub item
    And the road emission is added a new EUROCLASS specification with category 'Trekker' and the vehicle amount of 50
    And the source sub item is saved
    And the source is added a new sub item
    And the road emission is added a new EUROCLASS specification with category 'Vrachtauto diesel 10-20 ton GVW' and the vehicle amount of 101
    And the source sub item is saved
    And the source is saved
    Then the source emission of source 'Parkeerterrein' with emission row label 'Bestelauto LPG - Euro 3' should be 18,1
    And the source emission of source 'Parkeerterrein' with emission row label 'Trekker diesel licht (gemiddeld 19 ton GVW) - Euro 3' should be 57,9
    And the source emission of source 'Parkeerterrein' with emission row label 'Vrachtauto diesel 10-20 ton GVW - Euro 3' should be 75,9
    When the source with label 'Parkeerterrein' is selected
    And the selected source is edited
    And the source sub item with label 'Bestelauto LPG - Euro 3' is selected to edit
    And the source sub item is saved
    And the source is saved
    Then the source emission of source 'Parkeerterrein' with emission row label 'Bestelauto LPG - Euro 3' should be 18,1
    And no validation message is showing
    And the notification panel contains no errors
    And the notifications are all deleted

  Scenario: Calculating comparison of two situations
    Given the menu item -sources- is selected
    And the taskbar year is set to 2019
    And the calculation is started
    When the calculation ends
    And the map is zoomed out 2 times
    Then the notification panel contains no errors
    And the notifications are all deleted

  @AER-1727
  Scenario: Exporting comparison to different exports
    Given an export to GML with sources only is started
    Then the notification panel contains no errors
    Given an export to GML with results is started
    Then the notification panel contains no errors
#    Given an export to PAA is started, with coorporation 'FoodCompany', project 'Grow some fruits', location 'Kleine Staat 1, 6211 ED, Maastricht', description 'Comparison for FoodCompany'
#    Then the notification panel contains no errors
    Given an export to PAA OWN USE is started, with coorporation 'FoodCompany', project 'Grow some fruits Own Use', location 'Kleine Staat 12, 6211 ED, Maastricht', description 'Comparison for FoodCompany'

  @AER-1713 @AER-1738 @AER-1743 @AER-1788
  Scenario: Checking the calculation results for the comparison
    #check result table has become available
    Given the menu item -results- is selected
    And the sub menu item -results table- is selected
    And the situations are switched
    And situation 1 is selected
    And situation 2 is selected
    And the situations are switched
    And the comparison tab is selected
    #check result filter has become available
#    Given the sub menu item -results filter- is selected
#    And the situations are switched
#    And situation 1 is selected
#    And situation 2 is selected
#    And the situations are switched
#    And the comparison tab is selected
    #check results in table
    Given the sub menu item -results table- is selected
    And the comparison tab is selected
    And the results table is set to display as Percentage
    And the results table is set to display as Maximum increase
    #
    And the results table is set to show the area 'Geleenbeekdal'
    Then the result for Total Deposition in HabitatArea 'H91E0C' is '-0,02'
    And the result for Total Deposition in HabitatArea 'H9120' is '-0,02'
    And the result for Total Deposition in HabitatArea 'ZGH9120' is '-0,02'
    #
    And the results table is set to display as Average
    And the result for Average in HabitatArea 'ZGH9160B' is '-0,02'
    And the result for Average in HabitatArea 'H91E0C' is '-0,02'
    #
    And the notification panel contains no errors
