@not
Feature: Make a simple PAA

  Background: 
    Given AERIUS CALCULATOR is open

  @not
  Scenario: refactor
    When the startup is set to input sources
    And the source location is set to a point with coordinates X 111275 and Y 464164
    And the source label is set to 'Source A'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 100, NOX 100, PM10 0
    And the source is saved
    And the taskbar options-panel is opened
    And the calculation range is set to 5 km
    And the taskbar panel is closed
    And the calculation is started
    And the calculation ends
    And the menu item -results- is selected
    And the sub menu item -results overview- is selected
    And the sub menu item -results graph- is selected
    And the sub menu item -results table- is selected
    And the sub menu item -results filter- is selected

  @not
  Scenario: Try to upload a file
    And the startup is set to import a file
    And the file 'C:\\aerius.gml' is imported
    Then the notification panel contains no errors

  @not
  Scenario Outline: Add two generic sources and start an export
    When the startup is set to input sources
    And the source location is set to a point with coordinates X 176132 and Y 394952
    And the source label is set to 'Source A'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 <nh3>, NOX <nox>, PM10 <pm10>
    And the source is saved
    And an export to PAA is started, with coorporation '<projectname>', project '<projectname>', location '<projectname>', description '<projectname>'
    And the notification panel contains no errors

    Examples: 
      | nh3  | nox  | pm10 | projectname |
      | 300  | 250  | 0    | Project 1   |
      | 300  | 260  | 300  | Project 2   |
      | 300  | 250  | 0    | Project 3   |
      | 600  | 250  | 0    | Project 4   |
      | 300  | 450  | 0    | Project 5   |
      | 370  | 250  | 0    | Project 6   |
      | 300  | 250  | 0    | Project 7   |
      | 3890 | 450  | 0    | Project 8   |
      | 300  | 250  | 0    | Project 9   |
      | 300  | 1250 | 112  | Project 10  |
      | 700  | 250  | 0    | Project 11  |
      | 1300 | 250  | 0    | Project 12  |
      | 300  | 250  | 0    | Project 13  |
      | 300  | 4250 | 0    | Project 14  |
      | 3300 | 4250 | 0    | Project 15  |
