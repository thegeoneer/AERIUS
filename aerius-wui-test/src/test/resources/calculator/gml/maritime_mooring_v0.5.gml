<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:imaer="http://imaer.aerius.nl/0.5" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/0.5 http://imaer.aerius.nl/0.5/IMAER.xsd">
    <imaer:aeriusCalculatorMetaData>
        <imaer:year>2015</imaer:year>
        <imaer:version>1.0-SNAPSHOT_20150204_43846bf11c</imaer:version>
        <imaer:databaseVersion>1.0-SNAPSHOT_20150204_43846bf11c</imaer:databaseVersion>
        <imaer:situationName>Situatie 1</imaer:situationName>
    </imaer:aeriusCalculatorMetaData>
    <imaer:featureMembers>
        <imaer:MooringMaritimeShippingEmissionSource sectorId="7510" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Aanlegplaatsbedrijf</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.CURVE">
                            <gml:posList>66008.38 440837.04 65605.18 441240.24</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>19665.160271856363</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:mooringMaritimeShipping>
                <imaer:MooringMaritimeShipping shipType="OO1600">
                    <imaer:description>Tankers</imaer:description>
                    <imaer:shipsPerYear>100</imaer:shipsPerYear>
                    <imaer:averageResidenceTime>200</imaer:averageResidenceTime>
                    <imaer:inlandRoute>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="NL.IMAER.InlandRoute.1_0_0">
                            <gml:posList>65806.854567362 441038.56543264 65114.62 441784.56 66028.54 443881.2 60014.14 446784.24 60000.0 446773.0</gml:posList>
                        </gml:LineString>
                    </imaer:inlandRoute>
                    <imaer:maritimeRoute>
                        <imaer:MaritimeShippingRoute>
                            <imaer:shippingMovements>200</imaer:shippingMovements>
                            <imaer:route>
<gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="NL.IMAER.MaritimeRoute.1_0_0">
    <gml:posList>60000.0 446773.0 54651.58 449176.56</gml:posList>
</gml:LineString>
                            </imaer:route>
                        </imaer:MaritimeShippingRoute>
                    </imaer:maritimeRoute>
                </imaer:MooringMaritimeShipping>
            </imaer:mooringMaritimeShipping>
        </imaer:MooringMaritimeShippingEmissionSource>
    </imaer:featureMembers>
</imaer:FeatureCollectionCalculator>
