@prioprojects
Feature: tmp

#  Background: 
#    Given AERIUS CALCULATOR is open

  Scenario: GE_380027_ind_Kleefse Waard_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GE_380027_ind_Kleefse Waard_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Gelderland', project 'Industrieterrein', location 'Arnhem', description 'uitbreiding/vestiging bedrijven'

  Scenario: GE_380098_ind_De Aam_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GE_380098_ind_De Aam_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Gelderland', project 'Industrieterrein', location 'Elst Gld.', description 'uitbreiding/vestiging bedrijven'

  Scenario: GE_380127_ind_Roelofshoeve 2_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GE_380127_ind_Roelofshoeve 2_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Gelderland', project 'Industrieterrein', location 'Duiven', description 'uitbreiding/vestiging bedrijven'

  Scenario: GE_380133_ind_De Mars Zutphen_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GE_380133_ind_De Mars Zutphen_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Gelderland', project 'Industrieterrein', location 'Zutphen', description 'uitbreiding/vestiging bedrijven'

  Scenario: GE_380134_ind_Ecofactorij Apeldoorn_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GE_380134_ind_Ecofactorij Apeldoorn_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Gelderland', project 'Industrieterrein', location 'Apeldoorn', description 'uitbreiding/vestiging bedrijven'

  Scenario: GE_380136_ind_Medel Rivierenland_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GE_380136_ind_Medel Rivierenland_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Gelderland', project 'Industrieterrein', location 'Tiel', description 'uitbreiding/vestiging bedrijven'

  Scenario: GE_380139_ind_lorentz III_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GE_380139_ind_lorentz III_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Gelderland', project 'Industrieterrein', location 'Harderwijk', description 'uitbreiding/vestiging bedrijven'

  Scenario: GE_380155_ind_Bedrijvenpark Medel II_april2014_20141031.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GE_380155_ind_Bedrijvenpark Medel II_april2014_20141031.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Gelderland', project 'Industrieterrein', location 'Tiel', description 'uitbreiding/vestiging bedrijven'

  Scenario: GR_590001_IND_RWE_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GR_590001_IND_RWE_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Groningen', project 'energiecentrale', location 'Gemeente Eemsmond, Eemshaven', description 'nieuwe centrale 1600 Mw ( E )'

  Scenario: GR_590002_IND_Nuon_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GR_590002_IND_Nuon_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Groningen', project 'energiecentrale', location 'Gemeente Eemsmond, Eemshaven', description 'nieuwe centrale 1200 Mw ( E )'

  Scenario: GR_590003_IND_EemsmondEnergie_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GR_590003_IND_EemsmondEnergie_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Groningen', project 'energiecentrale', location 'Gemeente Eemsmond, Eemshaven', description 'realisatie nieuwe centrale 1200 Mw ( E )'

  Scenario: GR_590004_IND_Suikerunie_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GR_590004_IND_Suikerunie_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Groningen', project 'suikerfabriek', location 'Gemeente Groningen', description 'uitbreiding capaciteit tot 25.000 ton/d (150 d/j en 24 u/d)'

  Scenario: GR_590005_IND_EnecoOosterhorn_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GR_590005_IND_EnecoOosterhorn_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Groningen', project 'biomassacentrale', location 'Gemeente Delfzijl, Oosterhorn', description 'nieuwe centrale 49.9 Mw (E) en 147 Mw (W)'

  Scenario: GR_590006_IND_EonEnergyDelfzijl_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GR_590006_IND_EonEnergyDelfzijl_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Groningen', project 'biomassacentrale', location 'Gemeente Delfzijl, Oosterhorn', description 'nieuwe centrale 24 Mw (E) en 128,7 (W)'

  Scenario: GR_590007_IND_Woodspirit_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GR_590007_IND_Woodspirit_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Groningen', project 'bioethanolfabriek', location 'Gemeente Delfzijl, Oosterhorn', description 'realisatie  bioethanolfabriek'

  Scenario: LB_350015_landbouw_LOGwitveldweg_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'LB_350015_landbouw_LOGwitveldweg_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Limburg', project 'Landbouwontwikkelingsgebied', location 'Gemeente Horst aan de Maas', description 'Uitbreidingslocatie Landbouw'

  Scenario: LB_350019_landbouw_nunhemszaden_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'LB_350019_landbouw_nunhemszaden_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Limburg', project 'Kassenontwikkeling', location 'Gemeente Leudal', description 'Uitbreidingslocatie kassen'

  Scenario: LB_XXXXXX_industrie_greenportvenlo_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'LB_XXXXXX_industrie_greenportvenlo_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Limburg', project 'Bedrijventerrein, kassen', location 'Gemeente Venlo', description 'Vestiging bedrijven'

  Scenario: LB_XXXXXX_industrie_ooijenwanssum_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'LB_XXXXXX_industrie_ooijenwanssum_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Limburg', project 'Gebiedsontwikkeling, wegen, bedrijven, haven', location 'Gemeente Venray', description 'Diverse integrale ontwikkelingen'

  Scenario: NB_370001_industrie_Moerdijk Chemie kavel 1_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_370001_industrie_Moerdijk Chemie kavel 1_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Moerdijk Chemie kavel 1', location 'Gemeente Moerdijk', description 'Nieuwe bedrijfsvestiging'

  Scenario: NB_370003_industrie_Moerdijk Chemie kavel 3_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_370003_industrie_Moerdijk Chemie kavel 3_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Moerdijk Chemie kavel 3', location 'Gemeente Moerdijk', description 'Nieuwe bedrijfsvestiging'

  Scenario: NB_370008_industriie_Moerdijk Chemie kavel 8_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_370008_industriie_Moerdijk Chemie kavel 8_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Moerdijk Chemie kavel 8', location 'Gemeente Moerdijk', description 'Nieuwe bedrijfsvestiging'

  Scenario: NB_370009_industrie_Moerdijk Chemie kavel 9_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_370009_industrie_Moerdijk Chemie kavel 9_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Moerdijk Chemie kavel 9', location 'Gemeente Moerdijk', description 'Nieuwe bedrijfsvestiging'

  Scenario: NB_370010_industrie_Moerdijk Procestechniek kavel 10_april2014_EXTRA.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_370010_industrie_Moerdijk Procestechniek kavel 10_april2014_EXTRA.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Moerdijk Procestechniek kavel 10', location 'Gemeente Moerdijk', description 'Nieuwe bedrijfsvestiging'

  Scenario: NB_370011_industrie_Moerdijk Procestechniek kavel 11_april2014_EXTRA.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_370011_industrie_Moerdijk Procestechniek kavel 11_april2014_EXTRA.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Moerdijk Procestechniek kavel 11', location 'Gemeente Moerdijk', description 'Nieuwe bedrijfsvestiging'

  Scenario: NB_370126_industrie_woensdrecht rest- en zoekruimte aviolanda_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_370126_industrie_woensdrecht rest- en zoekruimte aviolanda_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Woensdrecht rest- en zoekruimte aviolanda', location 'Gemeente Moerdijk', description 'Nieuwe bedrijfsvestiging'

  Scenario: NB_370145_industire_moerdijk industrieterrein moerdijk (voormalig Shell)_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_370145_industire_moerdijk industrieterrein moerdijk (voormalig Shell)_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Moerdijk industrieterrein moerdijk (voormalig Shell)', location 'Gemeente Moerdijk', description 'Nieuwe bedrijfsvestiging'

  Scenario: NB_370217_industrie_Cranendonck duurzaam industriepark cranendonck_april2014_aanpassing oktober 2014-NEW.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_370217_industrie_Cranendonck duurzaam industriepark cranendonck_april2014_aanpassing oktober 2014-NEW.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Cranendonck duurzaam industriepark', location 'Gemeente Cranendonck', description 'Nieuwe bedrijfsvestiging'

  Scenario: NB_570193_landbouw_V (gement)_april2014_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_570193_landbouw_V (gement)_april2014_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Verplaatsing V', location 'Gemeente Vucht', description 'Verplaatsing ondernemer'

  Scenario: NB_NEW_industrie_Cranendonck vergroten haven bij Nyrstar_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_NEW_industrie_Cranendonck vergroten haven bij Nyrstar_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Cranendonck vergroten haven', location 'Gemeente Cranendonck', description 'Vergroten haven'

  Scenario: NB_NEW_landbouw_D1 (nieuwe locatie)_april2014_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NB_NEW_landbouw_D1 (nieuwe locatie)_april2014_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Brabant', project 'Verplaatsing D1', location 'Gemeente Deurne', description 'Verplaatsing ondernemer'

  Scenario: NH_410012_enina_BoekelermeerFase1en2Heiloo_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NH_410012_enina_BoekelermeerFase1en2Heiloo_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Holland', project 'bedrijventerreinen Boekelermeer Alkmaar en Heiloo', location 'gemeente Heiloo en Alkmaar', description 'uitbreiding bestaande bedrijventerreinen'

  Scenario: NH_410018_enina_NieuwBedrijventerreinRegionaalHavengebondenBedrijventerreinHollandsKroon_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NH_410018_enina_NieuwBedrijventerreinRegionaalHavengebondenBedrijventerreinHollandsKroon_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Holland', project 'Regionaal Havengebonden Bedrijventerrein Hollands Kroon', location 'gemeente Hollands Kroon', description 'nieuw bedrijventerrein'

  Scenario: NH_410034_enina_GroteHoutVelsen_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NH_410034_enina_GroteHoutVelsen_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Holland', project 'bedrijventerrein Grote Hout', location 'gemeente Velsen', description 'uitbreiding bestaand bedrijventerrein'

  Scenario: NH_616001_616004_enina_UitbreidingAverijhaven_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NH_616001_616004_enina_UitbreidingAverijhaven_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Holland', project 'Averijhaven', location 'Gemeente Velsen', description 'nieuw havenbekken voor board-to board overslag en capaciteitsuitbreiding'

  Scenario: NH_616005_616011_enina_HoogtijZaanstad_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NH_616005_616011_enina_HoogtijZaanstad_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Holland', project 'bedrijventerrein Hoogtij', location 'gemeente Zaanstad', description 'uitbreiding bestaand bedrijventerrein'

  Scenario: NH_616013_616061_enina_UitbreidingWestpoort.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NH_616013_616061_enina_UitbreidingWestpoort.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Holland', project 'bedrijventerrein Westpoort', location 'gemeente Amsterdam', description 'uitbreiding bestaand bedrijventerrein'

  Scenario: NH_950001_enina_UitbreidingTataSteel_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'NH_950001_enina_UitbreidingTataSteel_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Noord-Holland', project 'staalproductiebedrijf', location 'gemeente Velsen', description 'wijziging bedrijfsvoering'

  Scenario: OV_340038_industrie_021_Dollegoor3_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340038_industrie_021_Dollegoor3_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein Dollegoor 3', location 'Gemeente Almelo', description 'Herstructurering bedrijventerrein'

  Scenario: OV_340046_industrie_029_Haatland3_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340046_industrie_029_Haatland3_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein Haatland 3', location 'Gemeente Kampen', description 'Herstructurering bedrijventerrein'

  Scenario: OV_340049_industrie_032_Havengebied_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340049_industrie_032_Havengebied_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein Havengebied', location 'Gemeente Enschede', description 'Herstructurering bedrijventerrein'

  Scenario: OV_340086_industrie_069_Voorst_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340086_industrie_069_Voorst_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein Voorst', location 'Gemeente Zwolle', description 'Herstructurering bedrijventerrein'

  Scenario: OV_340093_industrie_076_XLBusinesspark_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340093_industrie_076_XLBusinesspark_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein XL Businesspark', location 'Gemeente Almelo', description 'Uitbreiding / nieuw bedrijventerrein'

  Scenario: OV_340099_industrie_082_Zomerdijk_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340099_industrie_082_Zomerdijk_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein Zomerdijk', location 'Gemeente Zwartewaterland', description 'Herstructurering bedrijventerrein'

  Scenario: OV_340101_industrie_084_Zwartewater_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340101_industrie_084_Zwartewater_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein Zwartewater', location 'Gemeente Zwartewaterland', description 'Herstructurering bedrijventerrein'

  Scenario: OV_340104_industrie_087_ZeggeVII_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340104_industrie_087_ZeggeVII_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein Zegge VII', location 'Gemeente Raalte', description 'uitbreiding / nieuw bedrijventerrein'

  Scenario: OV_340908_industrie_042_Meppelerdiep_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340908_industrie_042_Meppelerdiep_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein Meppelerdiep', location 'Gemeente Zwartewaterland', description 'Herstructurering bedrijventerrein'

  Scenario: OV_340909_industrie_059_MarkeloTwentekanaal_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340909_industrie_059_MarkeloTwentekanaal_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein Markelo Twentekanaal', location 'Gemeente Hof van Twente', description 'Herstructurering bedrijventerrein'

  Scenario: OV_340912_industrie_116_HessenpoortI-IIenZuid_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'OV_340912_industrie_116_HessenpoortI-IIenZuid_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Overijssel', project 'Bedrijventerrein HessenpoortI-II en Zuid', location 'Gemeente Zwolle', description 'Uitbreiding / nieuw bedrijventerrein'

  Scenario: UT_360002_consumenten_WijkbijDuurstede_april2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'UT_360002_consumenten_WijkbijDuurstede_april2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Utrecht', project 'binnen- en buitenstedelijke uitbreiding', location 'Gemeente Wijk bij Duurstede', description 'Bouwen van circa 250 woningen'

  Scenario: ZH_420007_consumenten_DenHaagKijkduin_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_420007_consumenten_DenHaagKijkduin_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Kijkduin te Den Haag', location 'gemeente Den Haag', description 'Realisatie ca 750 nieuwe woningen'

  Scenario: ZH_420032_consumenten_ValkenburgWassenaar_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_420032_consumenten_ValkenburgWassenaar_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Valkenburg te Wassenaar', location 'gemeente Wassenaar', description 'Realisatie ca 5000 nieuwe woningen'

  Scenario: ZH_420035_consumenten_Zeehospicium_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_420035_consumenten_Zeehospicium_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Zeehospicium te Katwijk', location 'gemeente Katwijk', description 'Realisatie ca 500 nieuwe woningen'

  Scenario: ZH_420055_enina_GrooteHaarGorichem_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_420055_enina_GrooteHaarGorichem_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Bedrijventerrein Groote Haar te Gorinchem', location 'gemeente Gorinchem', description 'Inrichting en gebruik industriegebied Groote Haar te Gorinchem; maximale milieucategorie 5.'

  Scenario: ZH_xxxxxx_consumenten_DeElementen_Spijkenisse_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_xxxxxx_consumenten_DeElementen_Spijkenisse_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw De Elementen te Spijkenisse', location 'gemeente Spijkenisse', description 'Realisatie circa 1077 woningen.'

  Scenario: ZH_xxxxxx_consumenten_Duingeest_Westland_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_xxxxxx_consumenten_Duingeest_Westland_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Duingeest te Westland', location 'gemeente Westland', description 'Realisatie ca 300 nieuwe woningen'

  Scenario: ZH_xxxxxx_consumenten_HoekVanHollandRotterdam_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_xxxxxx_consumenten_HoekVanHollandRotterdam_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Hoek van Holland te Rotterdam', location 'gemeente Rotterdam', description 'realisatie circa 4000 woningen'

  Scenario: ZH_xxxxxx_consumenten_MolenpolderNumansdorpRecreatieWoongebied_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_xxxxxx_consumenten_MolenpolderNumansdorpRecreatieWoongebied_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Molenpolder (recreatie woongebied) te Numansdorp', location 'gemeente Cromstrijen', description 'Realisatie circa 860 woningen'

  Scenario: ZH_xxxxxx_consumenten_MonsterNoordMonster_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_xxxxxx_consumenten_MonsterNoordMonster_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Monster-Noord te Monster', location 'gemeente Westland', description 'Realisatie circa 475 woningen'

  Scenario: ZH_xxxxxx_consumenten_VroonlaanDenHaag_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_xxxxxx_consumenten_VroonlaanDenHaag_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Vroonlaan te Den Haag', location 'gemeente Den Haag', description 'Realisatie circa 970 woningen'

  Scenario: ZH_xxxxxx_consumenten_WestmadeDenHaag_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_xxxxxx_consumenten_WestmadeDenHaag_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Westmade te Westland', location 'gemeente Westland', description 'Realisatie ca 1188 woningen'

  Scenario: ZH_xxxxxx_consumenten_ZuidelijkeStrandHoekVanHolland_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_xxxxxx_consumenten_ZuidelijkeStrandHoekVanHolland_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Woningbouw Zuidelijk strand te Hoek van Holland', location 'gemeente Rotterdam', description 'Realisatie circa 640 woningen'

  Scenario: ZH_xxxxxx_enina_WeverseindeVmSuikerfabriekPuttershoek_juli2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ZH_xxxxxx_enina_WeverseindeVmSuikerfabriekPuttershoek_juli2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Provincie Zuid-Holland', project 'Bedrijventerrein voormalig suikerunieterrein Puttershoek te Binnenmaas;', location 'gemeente Binnenmaas', description 'Inrichting en gebruik industriegebied te Binnenmaas met maximale milieucategorie 5'

  Scenario: Def_prj01_hdo_kazerne09B50_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj01_hdo_kazerne09B50_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging Nieuwe haven', location 'Nieuwe Haven, Den Helder', description 'Inpassing Joost Dourlein kazerne in Nieuwe Haven.'

  Scenario: Def_prj03_hdo_kazerne52A01_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj03_hdo_kazerne52A01_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging Luitenant generaal Bestkazerne', location 'Ripseweg, Vredepeel', description 'Inbreiding kazerne'

  Scenario: Def_prj04_hdo_kantoor44C03_okt2014_02.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj04_hdo_kantoor44C03_okt2014_02.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging Commando luchtstrijdkrachten', location 'Luchtmachtplein, Breda', description 'Inpassing DVD Tilburg in Commando Luchtstrijdkrachten'

  Scenario: Def_prj05_hdo_kazerne50B05_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj05_hdo_kazerne50B05_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging Trip van Zoudtlandkazerne', location 'De la Reijweg, Breda', description 'Inbreiding kazerne'

  Scenario: Def_prj06_hdo_kazerne32C25_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj06_hdo_kazerne32C25_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging Camp New Amsterdam', location 'Dolderseweg, Huis ter Heide, Utrecht', description 'Inbreiding kazerne'

  Scenario: Def_prj07_hdo_kazerne30E12_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj07_hdo_kazerne30E12_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging MC Maaldrift', location 'Ammonslaantje, Wassenaar', description 'Inbreiding kazerne'

  Scenario: Def_prj08_hdo_kazerne12C01_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj08_hdo_kazerne12C01_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging MMC Veenhuizen', location 'Norgerweg, Venhuizen', description 'Inbreiding MMC Veenhuizen'

  Scenario: Def_prj09_hdo_kazerne37H10_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj09_hdo_kazerne37H10_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging Van Ghentkazerne', location 'Toepad, Rotterdam', description 'Inbreiding kazerne'

  Scenario: Def_prj10_hdo_kazerne45H03_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj10_hdo_kazerne45H03_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herindeling Air Operations Control', location 'Vliegbasis Volkel, Volkel', description 'Inpassing deel AOCS Nieuw Milligen in Vliegbasis Volkel'

  Scenario: Def_prj11_hdo_kazerne27A03_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj11_hdo_kazerne27A03_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging Luitenant Kolonel Tonnetkazerne', location 'Eperweg, \'t Harde', description 'Inbreiding kazerne'

  Scenario: Def_prj12_hdo_kazerne27B09_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj12_hdo_kazerne27B09_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Herbelegging Legerplaats bij Olderboek', location 'Eperweg, \'t Harde', description 'Inbreiding kazerne'

  Scenario: Def_prj13_luchtvaart_ontwikkelingWoensdrecht_okt2014.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj13_luchtvaart_ontwikkelingWoensdrecht_okt2014.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Ontwikkeling Vliegbasis Woensdrecht', location 'Kooijweg, Hoogerheide', description 'Ontwikkeling Vliegbasis Woensdrecht'

  Scenario: Def_prj15_hdo_kantoor30G92_okt2014_02.gml
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'Def_prj15_hdo_kantoor30G92_okt2014_02.gml.zip' is imported
    And an export to PAA is started, with coorporation 'Ministerie van Defensie', project 'Uitbreiding NCIA', location 'Oude Waalsdorperweg, Deh Haag', description 'Uitbreiding NCIA'
