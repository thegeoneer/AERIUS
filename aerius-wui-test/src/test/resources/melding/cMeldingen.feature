@nightly
Feature: Meldingen module test

  #available previous melding reference numbers in testdata
  #12JWr4AxnS - used
  #1W21yi5oU  - used
  #12CPy1gBGK  - used
  #122exRfQP1 - used
  #12Avg1pjbd - used
  #12FpqpvRQr- used
  #12oqKYcdYV
  Background: 
    Given AERIUS CALCULATOR is open

  Scenario: Melding happy scenario with use of all input fields
    # prepare a melding by setting up a source and calculating it's results  
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 210033 and Y 382958
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 150, NOX 90, PM10 0
    And the source is saved
    Then the calculation is started
    And the calculation ends
    And the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Centrale EnerKo'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_01@gmail.com'
    # optional extra - initiator correspondence address details
    And the melding beneficiary correspondence details is set to other
    And the melding beneficiary correspondence address is set to 'Internetroad'
    And the melding beneficiary correspondence postcode is set to '1111 BB'
    And the melding beneficiary correspondence city is set to 'Arkansas'
    #
    # 2nd form - optional agent contact details
    And the melding benefactor is set to other
    And the melding benefactor organisation is set to 'Benefactor Org'
    And the melding benefactor contact is set to 'Benefactor Contact'
    And the melding benefactor address is set to 'Benefactor Street'
    And the melding benefactor postcode is set to '6666 BC'
    And the melding benefactor city is set to 'Benefactor City'
    And the melding benefactor email is set to 'aeriusmail+benefactor@gmail.com'
    And the melding benefactor email confirmation is toggled
    And the melding benefactor PDF-file 'machtigingsformulier.pdf' is added
    #
    # 3rd form - optional executor contact details
    And the melding executor is set to other
    And the melding executor organisation is set to 'Executor Org'
    And the melding executor contact is set to 'Executor Contact'
    And the melding executor address is set to 'Executor Street'
    And the melding executor postcode is set to '4444 EC'
    And the melding executor city is set to 'Executor City'
    And the melding executor email is set to 'aeriusmail+executor@gmail.com'
    And the melding executor email confirmation is toggled
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to YES
    And the melding has permit for current situation reference number is set to 'kaas'
    And the melding has current situation included is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
    And the melding truth check is toggled
#    PREVIOUS MELDING DISABLED (temporary permanently)
#    And the melding has previous melding is set to YES
#    And the melding has previous melding reference number is set to '12Avg1pjbd'
    Then the melding module is continued to the next step
    And the melding is sent successfully
    # IMPORTANT TO CLOSE MELDING MODULE
    And the melding module window is closed

  Scenario: Melding scenario: only beneficiary address
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 123826 and Y 473157
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 150, NOX 90, PM10 0
    And the source is saved
    Then the calculation is started
    And the calculation ends
    And the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Centrale KolenKo'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_02@gmail.com'
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
    And the melding truth check is toggled
#     And the melding has previous melding is set to NO
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: beneficiary address + benefactor address
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 123923 and Y 475209
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup INDUSTRY with sector 1050
    And the source emission is set to NH3 200, NOX 150, PM10 0
    And the source is saved
    Then the calculation is started
    And the calculation ends
    And the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Indus Inc.'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_03@gmail.com'
    #
    # 2nd form - optional agent contact details
    And the melding benefactor is set to other
    And the melding benefactor organisation is set to 'Benefactor Org'
    And the melding benefactor contact is set to 'Benefactor Contact'
    And the melding benefactor address is set to 'Benefactor Street'
    And the melding benefactor postcode is set to '6666 BC'
    And the melding benefactor city is set to 'Benefactor City'
    And the melding benefactor email is set to 'aeriusmail+benefactor@gmail.com'
    And the melding benefactor email confirmation is toggled
    And the melding benefactor PDF-file 'machtigingsformulier.pdf' is added
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
    And the melding truth check is toggled
#     And the melding has previous melding is set to NO
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: beneficiary address + executor address
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 122377 and Y 473784
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup INDUSTRY with sector 1100
    And the source emission is set to NH3 150, NOX 90, PM10 0
    And the source is saved
    Then the calculation is started
    And the calculation ends
    And the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Indus Plus'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_04@gmail.com'
    #
    # 3rd form - optional executor contact details
    And the melding executor is set to other
    And the melding executor organisation is set to 'Executor Org'
    And the melding executor contact is set to 'Executor Contact'
    And the melding executor address is set to 'Executor Street'
    And the melding executor postcode is set to '4444 EC'
    And the melding executor city is set to 'Executor City'
    And the melding executor email is set to 'aeriusmail+executor@gmail.com'
    And the melding executor email confirmation is toggled
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
    And the melding truth check is toggled
#     And the melding has previous melding is set to NO
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: beneficiary address + benefactor + executor address
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 131920 and Y 474782
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup INDUSTRY with sector 1300
    And the source emission is set to NH3 150, NOX 90, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Indus Max'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_05@gmail.com'
    #
    # 2nd form - optional agent contact details
    And the melding benefactor is set to other
    And the melding benefactor organisation is set to 'Benefactor Org'
    And the melding benefactor contact is set to 'Benefactor Contact'
    And the melding benefactor address is set to 'Benefactor Street'
    And the melding benefactor postcode is set to '6666 BC'
    And the melding benefactor city is set to 'Benefactor City'
    And the melding benefactor email is set to 'aeriusmail+benefactor@gmail.com'
    And the melding benefactor email confirmation is toggled
    And the melding benefactor PDF-file 'machtigingsformulier.pdf' is added
    #
    # 3rd form - optional executor contact details
    And the melding executor is set to other
    And the melding executor organisation is set to 'Executor Org'
    And the melding executor contact is set to 'Executor Contact'
    And the melding executor address is set to 'Executor Street'
    And the melding executor postcode is set to '4444 EC'
    And the melding executor city is set to 'Executor City'
    And the melding executor email is set to 'aeriusmail+executor@gmail.com'
    And the melding executor email confirmation is toggled
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
#     And the melding has previous melding is set to NO
    And the melding truth check is toggled
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: beneficiary address, no current permit, no previous melding
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 135542 and Y 476624
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup INDUSTRY with sector 1400
    And the source emission is set to NH3 200, NOX 1600, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Indus Pro B.V.'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_06@gmail.com'
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
    And the melding truth check is toggled
#     And the melding has previous melding is set to NO
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: beneficiary address, yes current permit, no current situation included, no previous melding
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 132681 and Y 467989
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup INDUSTRY with sector 1500
    And the source emission is set to NH3 300, NOX 180, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Indus en Friends'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_07@gmail.com'
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to YES
    And the melding has permit for current situation reference number is set to 'kaas'
    And the melding has current situation included is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
    And the melding truth check is toggled
#     And the melding has previous melding is set to NO
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: beneficiary address, yes current permit, yes current situation included, no previous melding
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 110932 and Y 457990
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup INDUSTRY with sector 1800
    And the source emission is set to NH3 300, NOX 180, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given  the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Chemical Brothers'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_08@gmail.com'
    Then the melding module is continued to the next step
    #
    # continue to final form
    And the melding has permit for current situation is set to YES
    And the melding has permit for current situation reference number is set to 'kaas'
    And the melding has current situation included is set to YES
#     And the melding has previous melding is set to NO
    And the melding truth check is toggled
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: beneficiary address, yes current permit, yes current situation included, yes previous melding
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 115474 and Y 461121
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup RAIL_TRANSPORTATION with sector 3710
    And the source emission is set to NH3 300, NOX 180, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Amateur Rail'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_09@gmail.com'
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to YES
    And the melding has permit for current situation reference number is set to 'kaas'
    And the melding has current situation included is set to YES
#    And the melding has previous melding is set to YES
#    And the melding has previous melding reference number is set to '12JWr4AxnS'
    And the melding truth check is toggled
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: beneficiary address, yes current permit, no current situation included, yes previous melding
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 116550 and Y 464280
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup AVIATION with sector 3620
    And the source emission is set to NH3 300, NOX 180, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Luchthaven Snel'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_10@gmail.com'
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to YES
    And the melding has permit for current situation reference number is set to 'kaas'
    And the melding has current situation included is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
#    And the melding has previous melding is set to YES
#    And the melding has previous melding reference number is set to '12CPy1gBGK'
    And the melding truth check is toggled
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: beneficiary address, no current permit, yes previous melding
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 116550 and Y 464280
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup OTHER with no sector
    And the source emission is set to NH3 300, NOX 180, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Octo Activi'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_11@gmail.com'
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
#    And the melding has previous melding is set to YES
#    And the melding has previous melding reference number is set to '122exRfQP1'
    And the melding truth check is toggled
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  Scenario: Melding scenario: agricultural comparison, beneficiary address, yes current permit, no current situation included, yes previous melding
    Given the startup is set to input sources
    When the source location is set to a 'polygon' with coordinates 129466 420210,129583 420222,129590 420112,129483 420099,129466 420210
    And the source label is set to 'Farmer John'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    # specify the initial situation sources
    And the farming emission is added a rav-code 'D 1.1.2' with bwl-code 'Groen Label BB 94.06.021V3' and an amount of 1000
    And the source sub item is saved
    And the source is added a new sub item
    And the farming emission is added a rav-code 'E 2.1' with bwl-code 'BWL 2001.07' and an amount of 1
    And the source sub item is saved
    Then the source is saved
    # specify an alternate situation for comparison
    When the alternate situation is created
    And the source with label 'Farmer John' is selected
    And the selected source is edited
    And the source is added a new sub item
    And the farming emission is added a rav-code 'F 1.100' with bwl-code 'Overig' and an amount of 2500
    And the source sub item is saved
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    Given the melding module is started and authenticated
    When the melding beneficiary organisation is set to 'Boer Harm'
    And the melding beneficiary contact is set to 'Dr. Robot'
    And the melding beneficiary address is set to 'Digistreet'
    And the melding beneficiary postcode is set to '8888 AA'
    And the melding beneficiary city is set to 'San Franz-Sisqo'
    And the melding beneficiary email is set to 'aeriusmail+beneficiary_12@gmail.com'
    #
    # 3rd form - optional executor contact details
    And the melding executor is set to other
    And the melding executor organisation is set to 'Executor Org'
    And the melding executor contact is set to 'Executor Contact'
    And the melding executor address is set to 'Executor Street'
    And the melding executor postcode is set to '4444 EC'
    And the melding executor city is set to 'Executor City'
    And the melding executor email is set to 'aeriusmail+executor@gmail.com'
    And the melding executor email confirmation is toggled
    Then the melding module is continued to the next step
    #
    # continue to final form
    When the melding has permit for current situation is set to YES
    And the melding has permit for current situation reference number is set to 'kaas'
    And the melding has current situation included is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
#    And the melding has previous melding is set to YES
#    And the melding has previous melding reference number is set to '12FpqpvRQr'
    And the melding truth check is toggled
    Then the melding module is continued to the next step
    And the melding is sent successfully
    And the melding module window is closed

  @melding-validation @fire
  Scenario: Rainy day Melding scenario to test form validation messages for all field requirements
    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 112773 and Y 460254
    And the source label is set to 'Energie Centrale'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 300, NOX 180, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    Then the menu item -melding- is selected
    #
    # 1st form - initiator contact details
    #
    Given the melding module is started and authenticated
    # name
    When the melding module is continued to the next step
    Then a validation message is showing with the text 'De Bedrijfsnaam moet ingevuld worden.'
    # contact
    When the melding beneficiary organisation is set to 'Selenium Valley'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Er moet een contactpersoon worden opgegeven.'
    # address
    When the melding beneficiary contact is set to 'Contactee'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Het adres moet ingevuld worden.'
    # zipcode
    When the melding beneficiary address is set to 'Addressee'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De postcode moet ingevuld worden.'
    # city
    When the melding beneficiary postcode is set to '1337BB'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De plaats moet ingevuld worden.'
    # email
    When the melding beneficiary city is set to 'Vice City'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Het e-mail adres wordt gebruikt voor versturen van documenten en is daarom verplicht.'
    # email invalid
    When the melding beneficiary email is set to 'InvalidEmail'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'InvalidEmail is geen geldig e-mailadres.'
    #
    # optional extra - initiator correspondence address details
    #
    When the melding beneficiary email is set to 'aeriusmail+valid@gmail.com'
    And the melding beneficiary correspondence details is set to other
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Het adres moet ingevuld worden.'
    # correspondence zipcode
    When the melding beneficiary correspondence address is set to 'CorrespondenceAddressee'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De postcode moet ingevuld worden.'
    # correspondence city
    When the melding beneficiary correspondence postcode is set to '2525 BB'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De plaats moet ingevuld worden.'
    #
    # 2nd form - optional agent contact details
    #
    When the melding beneficiary correspondence city is set to 'CorrespondenceCitee'
    And the melding benefactor is set to other
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Er moet ten minste één bestand worden toegevoegd.'
    # agent company
    When the melding benefactor PDF-file 'machtigingsformulier.pdf' is added
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De Bedrijfsnaam moet ingevuld worden.'
    # agent contact
    When the melding benefactor organisation is set to 'The Agency'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Er moet een contactpersoon worden opgegeven.'
    # agent address
    When the melding benefactor contact is set to 'Agent Smith'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Het adres moet ingevuld worden.'
    # agent zipcode
    When the melding benefactor address is set to 'Agent Street'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De postcode moet ingevuld worden.'
    # agent city
    When the melding benefactor postcode is set to '007 JB'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De plaats moet ingevuld worden.'
    # agent email 
    When the melding benefactor city is set to 'Sim City'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Het e-mail adres wordt gebruikt voor versturen van documenten en is daarom verplicht.'
    # agent email invalid
    When the melding benefactor email is set to 'InvalidEmail'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'InvalidEmail is geen geldig e-mailadres.'
    #
    # 3rd form - optional executor contact details
    #
    When the melding benefactor email is set to 'aeriusmail+agent@gmail.com'
    And the melding benefactor email confirmation is toggled
    And the melding executor is set to other
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De Bedrijfsnaam moet ingevuld worden.'
    # executor contact
    When the melding executor organisation is set to 'Executor Org'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Er moet een contactpersoon worden opgegeven.'
    # executor address
    When the melding executor contact is set to 'Executor Contact'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Het adres moet ingevuld worden.'
    # executor zipcode
    When the melding executor address is set to 'Executor Street'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De postcode moet ingevuld worden.'
    # executor city
    When the melding executor postcode is set to '4444 EC'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'De plaats moet ingevuld worden.'
    # executor email
    When the melding executor city is set to 'Executor City'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Het e-mail adres wordt gebruikt voor versturen van documenten en is daarom verplicht.'
    # executor email invalid
    When the melding executor email is set to 'InvalidEmail'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'InvalidEmail is geen geldig e-mailadres.'
    # 
    # continue to final form
    #
    When the melding executor email is set to 'aeriusmail+executor@gmail.com'
    And the melding executor email confirmation is toggled
    And the melding module is continued to the next step
    Then no validation message is showing
    # reference
    When the melding has permit for current situation is set to YES
    And the melding truth check is toggled
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'Bij bestaande vergunningen moet het kenmerk van het bevoegd gezag van uw vergunning worden opgegeven.'
    # substantiation
    When the melding has permit for current situation reference number is set to 'Vergunner'
    And the melding module is continued to the next step
    Then a validation message is showing with the text 'U bent wettelijk verplicht onderbouwings documenten toe te voegen.'
    # submit melding
    When the melding has current situation included is set to NO
    And the melding has current situation reference document PDF 'onderbouwingsdocument.pdf' is added
    And the melding module is continued to the next step
    Then no validation message is showing
    And the melding is sent successfully
    And the melding module window is closed
