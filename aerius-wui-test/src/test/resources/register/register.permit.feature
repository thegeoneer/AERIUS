@permit-applications @nightly @ignore-permit
Feature: Authentication for user roles and functionalities to process submitted applications
  A viewer should only be allowed to view submitted applications
  An editor should be able to view and edit applications, except status transition and deleting
  An administrator should be able to do everything

  Background:
    Given AERIUS REGISTER is open

  @applications-import-bc @rainy
  Scenario: As a user with import permission i get notified when the import file is outdated and/or unsupported <imaer v0.2>
    Given the user is logged in as 'EDITOR'
    And the menu item 'applications' is selected
    And the application import window is opened
    And the application import dossier id is set to 'OutdatedApplication'
    And the application import date is set to '21-12-2112 21:12'
    And the application import file 'AERIUS_BC_IMAERv0_2.pdf' is selected
    And the application import window is confirmed to check for errors
    Then the application import window contains error message 'Het GML bestand is van een onbekende of niet ondersteunde IMAER versie. Deze informatie over de versie werd gevonden: http://imaer.aerius.nl/0.2.'

  @applications-import-bc @rainy
  Scenario: As a user with import permission i get notified when the import file is invalid <imaer v0.5>
    Given the user is logged in as 'EDITOR'
    And the menu item 'applications' is selected
    And the application import window is opened
    And the application import dossier id is set to 'Invalid permit application!'
    And the application import date is set to '21-12-2112 21:12'
    And the application import file 'AERIUS_BC_IMAERv0_5_NoStreet.pdf' is selected
    And the application import window is confirmed to check for errors
    Then the application import window contains error message 'Het GML meta data veld "streetAddress" is verplicht maar niet ingevuld.'

  @applications-import-bc
  Scenario: As a user with import permission i can import an old import file which is valid <imaer v0.5>
    Given the user is logged in as 'EDITOR'
    And the menu item 'applications' is selected
    When the application with filename 'AERIUS_BC_IMAERv0_5.pdf' is imported with dossierID 'Ancient_IMAER_v0.5!'
    Then the notification panel contains no errors
    When wait until the application dossier status is 'In de wachtrij'
    And the application is deleted
    Then the notification panel contains the message 'De aanvraag met zaaknummer "Ancient_IMAER_v0.5!" is succesvol verwijderd'

  @applications-import-bc
  Scenario: As a user with import permission i can import an old import file which is valid <imaer v1.0>
    Given the user is logged in as 'EDITOR'
    And the menu item 'applications' is selected
    When the application with filename 'AERIUS_BC_IMAERv1_0.pdf' is imported with dossierID 'Ancient_IMAER_v1.0!'
    Then the notification panel contains no errors
    When wait until the application dossier status is 'In de wachtrij'
    And the application is deleted
    Then the notification panel contains the message 'De aanvraag met zaaknummer "Ancient_IMAER_v1.0!" is succesvol verwijderd'

  @applications-import
  Scenario Outline: As an editor, superuser and admin i can import a new application, edit the dossier, assign a new handler and delete
    Given the user is logged in as '<user_role>'
    And the menu item 'applications' is selected
    And the application with filename '<import_file>' is imported with dossierID '<dossier_id>'
    And the application dossier status is 'Ingeladen'
    And the application dossier number has the value '<dossier_id>'
    And the application dossier remarks has the value 'Geen'
    And the application dossier handler has the value '<app_handler>'
    When the application is set to edit mode
    And the application dossier number is set to '<dossier_edit>'
    And the application remarks is set to '<app_remarks>'
    And the application handler is set to username '<new_handler>'
    And the application is saved
    And wait until the page is loaded
    Then the application dossier number has the value '<dossier_edit>'
    And the application dossier remarks has the value '<app_remarks>'
    And the application dossier handler has the value '<new_handler>'
    And the application with id '<dossier_edit>' is removed

    Examples: User roles that are allowed to handle permit applications
      | user_role | import_file                  | dossier_id    | dossier_edit      | app_handler | new_handler | app_remarks |
      | EDITOR    | AERIUS_Various_Editor.pdf    | ST-Editor     | ST-Editor-edit    | Test3, A.   | Test4, A.   | Cuke Editor |
      | SUPERUSER | AERIUS_Various_Superuser.pdf | ST-Superuser  | ST-Superuser-edit | Test4, A.   | Test1, A.   | Cuke Super  |
      | ADMIN     | AERIUS_Various_Admin.pdf     | ST-Admin      | ST-Admin-edit     | Test1, A.   | Test3, A.   | Cuke Admin  |

  @applications-cycle
  Scenario Outline: Import dossiers with specific IDs needed for testing state transitions
    Given the user is logged in as 'EDITOR'
    And the menu item 'applications' is selected
    When the application with filename '<permit_pdf>' is imported with dossierID '<permit_id>'
    Then the application dossier status is 'Ingeladen'
    And the notification panel contains no errors

    Examples: Pdf's and dossier id's used by state transition scenario
      | permit_pdf                                    | permit_id             |
      | AERIUS_pending_with_space.pdf                 | PendingWithSpace1     |
      | AERIUS_pending_with_space1.pdf                | PendingWithSpace2     |
      | AERIUS_pending_with_space2.pdf                | PendingWithSpace3     |
      | AERIUS_pending_without_space.pdf              | PendingWithoutSpace1  |

  @applications-cycle
  Scenario Outline: Change states with superuser for the state transition scenario to provide the initial situation
    Given the user is logged in as 'SUPERUSER'
    And the search for '<permit_id>' is started
    And the search suggestion '<permit_id>' is selected
    And wait until the application dossier status is 'In de wachtrij'
    When the application dossier status is set to '<permit_status>'
    And wait for 1 seconds
    Then the application dossier status is '<permit_status>'
    And the notification panel contains no errors

    Examples: Dossier id's and permit statuses used by state transition scenario
      | permit_id             | permit_status         |
      | PendingWithSpace1     | PENDING_WITH_SPACE    |
      | PendingWithSpace2     | PENDING_WITH_SPACE    |
      | PendingWithSpace3     | PENDING_WITH_SPACE    |
      | PendingWithoutSpace1  | PENDING_WITHOUT_SPACE |

  # 1.1 pending_without_space -> rejected_without_space
  # 1.1.1 rejected_without_space -> rejected_final
  # 2.1.2.1 assigned -> rejected_final

  @applications-cycle
  Scenario Outline: As an editor i can only change the application status in accordance with the state transition diagram
    Given the user is logged in as 'SUPERUSER'
    And the search for '<permit_id>' is started
    And the search suggestion '<permit_id>' is selected
    And the application dossier status is '<initial_status>'
    And the application dossier status is set to '<result_status>'
    And wait for 3 seconds
    Then the notification panel contains no errors

    Examples: All state transitions covering the paths defined by the state transition diagram
      | permit_id             | initial_status         | result_status          |
      | PendingWithoutSpace1  | PENDING_WITHOUT_SPACE  | REJECTED_WITHOUT_SPACE |
      | PendingWithoutSpace1  | REJECTED_WITHOUT_SPACE | QUEUED                 |
      | PendingWithoutSpace1  | QUEUED                 | PENDING_WITHOUT_SPACE  |
      | PendingWithoutSpace1  | PENDING_WITHOUT_SPACE  | REJECTED_WITHOUT_SPACE |
      | PendingWithoutSpace1  | REJECTED_WITHOUT_SPACE | REJECTED_FINAL         |
      | PendingWithSpace2     | PENDING_WITH_SPACE     | ASSIGNED               |
      | PendingWithSpace2     | ASSIGNED               | REJECTED_FINAL         |
      | PendingWithSpace3     | PENDING_WITH_SPACE     | ASSIGNED               |
      | PendingWithSpace3     | ASSIGNED               | ASSIGNED_FINAL         |

  @applications-cycle
  Scenario Outline: As an editor i can delete applications
    Given the user is logged in as 'EDITOR'
    And the menu item 'applications' is selected
    When the application with description '<permit_id>' is deleted
    Then the notification panel contains no errors
    And the notification panel contains the message 'De aanvraag met zaaknummer "<permit_id>" is succesvol verwijderd'

    Examples: All residual permit requests that need to be removed after state transition tests
      | permit_id         |
      | PendingWithSpace1 |
      | PendingWithSpace3 |
