@system-message-register @nightly
Feature: The system message allows an administrator to add messages to the AERIUS applications to inform users about certain events
    This feature is the same for all products so this test only sets and checks a single message for AERIUS Reigister 
    Full test coverage of this feature is performed on AERIUS Calculator

  Background: 
    Given AERIUS REGISTER is open
    Given the user is logged in as 'ADMIN'
    Given the system message entry page is opened 


  @system-message-link
  Scenario: As an administrator i can set a system message containing a URL 
    Given the administrator enters message text 'Voor meer informatie zie ook de <a href="http://www.aerius.nl/" target="_blank">AERIUS Website</a>'
    And the administrator enters locale 'nl'
    And the administrator enters UUID 'bb8f8051-8e93-4f65-9177-689be4fe2897'
    When the system message is submitted
    Then the system message confirmation is shown
    # Check for message and link with user
    When AERIUS REGISTER is open
    Then the system message is shown with link text 'AERIUS Website' pointing to url 'http://www.aerius.nl/'

  @system-message-delete
  Scenario: Cleaning up any messages to prevent them from showing up in the manual images
    Given the administrator ticks the delete all messages check box
    And the administrator enters UUID 'bb8f8051-8e93-4f65-9177-689be4fe2897'
    When the system message is submitted
    Then the system message confirmation is shown

