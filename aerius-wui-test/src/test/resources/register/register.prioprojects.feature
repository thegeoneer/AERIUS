@priority-projects @nightly
Feature: The priority projects page provides an overview and functionality to manage segment 1 projects
  New priority projects are created by importing a (zipped) reservation gml
  Sub projects and actualisation gml files can be imported from the priority project page

  Background:
    Given AERIUS REGISTER is open

  ################################################
  # Checking the accessibility for all user roles
  ################################################
  @project-cycle @project-import @admin
  Scenario Outline: As an SUPERUSER i can create new PRIORITY projects by IMPORTING a reservation file
    The intitial status transition for any priority project is from 'Ingeladen' to 'In uitvoering'
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    Then the user can import '<project_zip>' with dossierID '<dossier_id>' as a new priority project
    And the notification panel contains no errors
    And the priority project status button is 'enabled'
    And the history contains entry: 'Status → In uitvoering'
    And the history contains entry: 'Status → Ingeladen Segment → Prioritaire projecten Zaaknummer → <dossier_id> Kenmerk → <reference>'

    Examples: Zipped gmls with priority project reservation data
      | project_zip                               | dossier_id            | reference    |
      | AERIUS_SET01_000 - PP01 - RESERVERING.zip | CukePriorityProject01 | 24BFVangAN5R |
      | AERIUS_SET01_000 - PP02 - RESERVERING.zip | CukePriorityProject02 | 24BFVamvxMCC |
      | AERIUS_SET01_000 - PP03 - RESERVERING.zip | CukePriorityProject03 | 24BFVapVbLjT |

  @project-cycle @project-import-partial @admin
  Scenario Outline: As an EDITOR, ADMIN or SUPERUSER i can import PARTIAL projects for a PRIORITY project
    The priority project status can not be changed while there are unapproved partial projects
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Project' tab of the priority project
    And the partial project file '<sub_project_zip>' is imported
    Then the application dossier status is 'Toetsbaar'
    And the notification panel contains no errors
    And the user switches to the 'Dossier' tab of the priority project
    And the priority project status button is 'disabled'
    And the history contains entry: 'Deelaanvraag: <reference> Status → Ingeladen Segment → Prioritaire subprojecten Kenmerk → <reference>'
    And the history contains entry: 'Deelaanvraag: <reference> Status → Toetsbaar'

    Examples: Zipped gmls with priority sub projects
      | user_role | dossier_id            | sub_project_zip                                          | reference    |
      | EDITOR    | CukePriorityProject01 | AERIUS_SET01_01 - PDP01.01 - Dijkslootweg.gml.zip        | 24BFVaoDg4Lj |
#      | EDITOR    | CukePriorityProject01 | AERIUS_SET01_02 - PDP01.02 - Aansluiting N371.gml.zip    | 24BFVaodgA1N |
#      | EDITOR    | CukePriorityProject01 | AERIUS_SET01_03 - PDP01.03 - Overschrijding.gml.zip      | 24BFVao924Rz |
#      | EDITOR    | CukePriorityProject02 | AERIUS_SET01_01 - PDP02.01 - Kalfjes.gml.zip             | 24BFVaooyZYD |
#      | EDITOR    | CukePriorityProject02 | AERIUS_SET01_02 - PDP02.02 - Beweiding.gml.zip           | 24BFVao9uEUj |
#      | EDITOR    | CukePriorityProject02 | AERIUS_SET01_03 - PDP02.03 - Overschrijding.gml.zip      | 24BFVaqjdvHt |
#      | SUPERUSER | CukePriorityProject03 | AERIUS_SET01_01 - PDP03.01 - Centrale.gml.zip            | 24BFVamyXZxT |
#      | SUPERUSER | CukePriorityProject03 | AERIUS_SET01_01 - PDP03.02 - Afvoer en transport.gml.zip | 24BFVambzpEz |
#      | SUPERUSER | CukePriorityProject03 | AERIUS_SET01_01 - PDP03.03 - Overschrijding.gml.zip      | 24BFVamNaBiZ |

  ##############################
  # assigned -> assigned final # *
  # assigned -> rejected       # *
  # assigned -> rejected final # *
  # rejected -> rejected final # *
  # rejected -> assigned       # *
  ##############################
  @project-cycle @partial-state-transition @admin
  Scenario Outline: As an EDITOR, ADMIN or SUPERUSER i can CHANGE a partial project STATUS from 'queued' to 'assigned' and 'assigned' to 'final'
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    And the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Project' tab of the priority project
    When the user opens priority sub project with dossierID '<sub_dossier_id>'
    And the application dossier status is set to '<sub_status>'
    And the user switches to the 'Project' tab of the priority project
    And the user opens priority sub project with dossierID '<sub_dossier_id>'
    Then the application dossier status is '<status_text>'
    And the status change is confirmed by notification: 'De status van de aanvraag met zaaknummer "<reference>" is succesvol aangepast.'
    And the notification panel contains no errors
    When the notifications are all deleted
    And the user switches to the 'Dossier' tab of the priority project
    Then the history contains entry: 'Deelaanvraag: <reference> Status → <status_text>'

    Examples: Covering all user roles to change partial project status from queued to assigned and assigned final
      | user_role | dossier_id            | sub_dossier_id   | reference    | sub_status     | status_text                   |
#      | EDITOR    | CukePriorityProject01 | Dijkslootweg     | 24BFVaoDg4Lj | ASSIGNED       | Ruimte afgeboekt |
#      | EDITOR    | CukePriorityProject01 | Dijkslootweg     | 24BFVaoDg4Lj | ASSIGNED_FINAL | Definitief besluit            |
#      | ADMIN     | CukePriorityProject01 | Aansluiting N371 | 24BFVaodgA1N | ASSIGNED       | Ruimte afgeboekt |
#      | ADMIN     | CukePriorityProject01 | Aansluiting N371 | 24BFVaodgA1N | ASSIGNED_FINAL | Definitief besluit            |
      | SUPERUSER | CukePriorityProject02 | Stallen Kalfjes  | 24BFVaooyZYD | ASSIGNED       | Ruimte afgeboekt |
      | SUPERUSER | CukePriorityProject02 | Stallen Kalfjes  | 24BFVaooyZYD | ASSIGNED_FINAL | Definitief besluit            |

  @project-cycle @partial-state-transition @admin
  Scenario Outline: As an EDITOR, ADMIN or SUPERUSER i can CHANGE a partial project STATUS from 'assigned' to 'rejected' and vice versa
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    And the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Project' tab of the priority project
    When the user opens priority sub project with dossierID '<sub_dossier_id>'
    And the application dossier status is set to '<sub_status>'
    And the user switches to the 'Project' tab of the priority project
    And the user opens priority sub project with dossierID '<sub_dossier_id>'
    Then the application dossier status is '<status_text>'
    And wait for 1 seconds
    When the notification panel contains the message 'De status van de aanvraag met zaaknummer "<reference>" wordt aangepast.'
    And the status change is confirmed by notification: 'De status van de aanvraag met zaaknummer "<reference>" is succesvol aangepast.'
    And the user switches to the 'Dossier' tab of the priority project
    Then the notification panel contains no errors
    And the history contains entry: 'Deelaanvraag: <reference> Status → <status_text>'

    Examples: Covering all user roles to change partial project status from assigned to rejected and vice versa
      | user_role | dossier_id            | sub_dossier_id       | reference    | sub_status             | status_text                   |
      | EDITOR    | CukePriorityProject02 | Stallen Beweiding    | 24BFVao9uEUj | ASSIGNED               | Ruimte afgeboekt |
      | EDITOR    | CukePriorityProject02 | Stallen Beweiding    | 24BFVao9uEUj | REJECTED_WITHOUT_SPACE | Afgewezen                     |
      | EDITOR    | CukePriorityProject02 | Stallen Beweiding    | 24BFVao9uEUj | ASSIGNED               | Ruimte afgeboekt |
      | ADMIN     | CukePriorityProject03 | Kencentrale en Afval | 24BFVamyXZxT | ASSIGNED               | Ruimte afgeboekt |
      | ADMIN     | CukePriorityProject03 | Kencentrale en Afval | 24BFVamyXZxT | REJECTED_WITHOUT_SPACE | Afgewezen                     |
      | ADMIN     | CukePriorityProject03 | Kencentrale en Afval | 24BFVamyXZxT | ASSIGNED               | Ruimte afgeboekt |
      | SUPERUSER | CukePriorityProject03 | Opslag en transport  | 24BFVambzpEz | ASSIGNED               | Ruimte afgeboekt |
      | SUPERUSER | CukePriorityProject03 | Opslag en transport  | 24BFVambzpEz | REJECTED_WITHOUT_SPACE | Afgewezen                     |
      | SUPERUSER | CukePriorityProject03 | Opslag en transport  | 24BFVambzpEz | ASSIGNED               | Ruimte afgeboekt |

  @project-cycle @partial-state-transition @admin
  Scenario Outline: As an EDITOR, ADMIN or SUPERUSER i can CHANGE a partial project STATUS from 'assigned' to 'rejected final'
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    And the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Project' tab of the priority project
    When the user opens priority sub project with dossierID '<sub_dossier_id>'
    And the application dossier status is set to '<sub_status>'
    Then the status change is confirmed by notification: 'Prioritair deelproject met kenmerk "<reference>" is succesvol verwijderd'
    And the notification panel contains no errors

    Examples: Covering all user roles to change partial project status from assigned to rejected final
      | user_role | dossier_id            | sub_dossier_id       | reference    | sub_status     | status_text          |
#      | EDITOR    | CukePriorityProject02 | Stallen Beweiding    | 24BFVao9uEUj | REJECTED_FINAL | Definitief afgewezen |
#      | ADMIN     | CukePriorityProject03 | Kencentrale en Afval | 24BFVamyXZxT | REJECTED_FINAL | Definitief afgewezen |
      | SUPERUSER | CukePriorityProject03 | Opslag en transport  | 24BFVambzpEz | REJECTED_FINAL | Definitief afgewezen |

  @project-cycle @partial-state-transition @admin
  Scenario Outline: As an EDITOR, ADMIN or SUPERUSER i can CHANGE a partial project status from 'queued' to 'rejected'
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    And the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Project' tab of the priority project
    When the user opens priority sub project with dossierID '<sub_dossier_id>'
    And the application dossier status is set to '<sub_status>'
    And the user switches to the 'Project' tab of the priority project
    And the user opens priority sub project with dossierID '<sub_dossier_id>'
    Then the application dossier status is '<status_text>'
    And the status change is confirmed by notification: 'De status van de aanvraag met zaaknummer "<reference>" is succesvol aangepast.'
    And the notification panel contains no errors
    When the user switches to the 'Dossier' tab of the priority project
    Then the history contains entry: 'Deelaanvraag: <reference> Status → <status_text>'

    Examples: Covering all user roles to change partial project status from queued to rejected
      | user_role | dossier_id            | sub_dossier_id                 | reference    | sub_status             | status_text |
      | SUPERUSER | CukePriorityProject01 | Overschrijding                 | 24BFVao924Rz | REJECTED_WITHOUT_SPACE | Afgewezen   |
#      | ADMIN     | CukePriorityProject02 | Stallen Tilburg Overschrijding | 24BFVaqjdvHt | REJECTED_WITHOUT_SPACE | Afgewezen   |
      | SUPERUSER | CukePriorityProject03 | Industrie Overschrijding       | 24BFVamNaBiZ | REJECTED_WITHOUT_SPACE | Afgewezen   |

  @project-cycle @partial-state-transition @admin
  Scenario Outline: As an EDITOR, ADMIN or SUPERUSER i can change a partial project STATUS from 'rejected' to 'rejected final'
    With the remaining partial projects approved, the priority project status can be changed only by an admin or superuser 
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    And the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Project' tab of the priority project
    And the priority project status button is 'disabled'
    When the user opens priority sub project with dossierID '<sub_dossier_id>'
    And the application dossier status is set to '<sub_status>'
    Then the priority project status button is '<status_button>'
    And the notification panel contains no errors

    Examples: Covering all user roles to change partial project status from rejected to rejected final
      | user_role | dossier_id            | sub_dossier_id                 | reference    | sub_status     | status_button |
#      | EDITOR    | CukePriorityProject01 | Overschrijding                 | 24BFVao924Rz | REJECTED_FINAL | disabled      |
#      | ADMIN     | CukePriorityProject02 | Stallen Tilburg Overschrijding | 24BFVaqjdvHt | REJECTED_FINAL | enabled       |
      | SUPERUSER | CukePriorityProject03 | Industrie Overschrijding       | 24BFVamNaBiZ | REJECTED_FINAL | enabled       |

  @project-cycle @project-import-actualization @admin
  Scenario Outline: As a SUPERUSER or ADMIN i can ADD an ACTUALIZATION file to a priority project
    Actualization prevents status changes (given there were no unapproved partial projects) 
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Actualisatie' tab of the priority project
    And the actualization project file '<actualization_project_zip>' is imported
    Then the notification panel contains no errors
    And the user switches to the 'Dossier' tab of the priority project
    And the priority project status button is 'disabled'
    And the history contains entry: 'Actualisatie bestand → <actualization_project_zip>'

    Examples: Zipped gmls to import as actualization for priority projects
      | user_role | actualization_project_zip                  | dossier_id            |
      | SUPERUSER | AERIUS_SET01_001 - PP01 - ACTUALISATIE.zip | CukePriorityProject01 |
#      | ADMIN     | AERIUS_SET01_001 - PP02 - ACTUALISATIE.zip | CukePriorityProject02 |
      | SUPERUSER | AERIUS_SET01_001 - PP03 - ACTUALISATIE.zip | CukePriorityProject03 |

  @project-cycle @project-replace-actualization @admin
  Scenario Outline: As a SUPERUSER or ADMIN i can REPLACE the ACTUALIZATION file of a priority project
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Actualisatie' tab of the priority project
    And the actualization project file is replaced by '<actualization_project_zip>'
    Then the notification panel contains no errors
    And the user switches to the 'Dossier' tab of the priority project
    And the priority project status button is 'disabled'
    And the history contains entry: 'Actualisatie bestand → <actualization_project_zip>'

    Examples: Zipped gmls to import as replacement actualization for priority projects
      | user_role | actualization_project_zip                  | dossier_id            |
#      | ADMIN     | AERIUS_SET02_001 - PP01 - ACTUALISATIE.zip | CukePriorityProject01 |
      | SUPERUSER | AERIUS_SET02_001 - PP02 - ACTUALISATIE.zip | CukePriorityProject02 |
#      | ADMIN     | AERIUS_SET02_001 - PP03 - ACTUALISATIE.zip | CukePriorityProject03 |

  @project-cycle @project-browse-dossier
  Scenario Outline: All users can VIEW the priority project DOSSIER
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    Then the field 'Projectnaam' has value '<project_naam>'
    And the field 'Project-ID' has value '<dossier_id>'
    And the field 'Bevoegd gezag' has value '<authority>'
    And the field 'Opmerkingen' has value '<remarks>'

    Examples: Priority project dossier info and the user roles involved
      | user_role | dossier_id            | project_naam            | authority                                   | remarks |
      | VIEWER    | CukePriorityProject01 | Wegverkeer Dwingeloo 01 | Ministerie van Infrastructuur en Waterstaat | Geen    |
      | EDITOR    | CukePriorityProject02 | Stallen Tilburg 01      | Ministerie van Infrastructuur en Waterstaat | Geen    |
      | SUPERUSER | CukePriorityProject03 | Industrie Utrecht 01    | Ministerie van Infrastructuur en Waterstaat | Geen    |
      | ADMIN     | CukePriorityProject01 | Wegverkeer Dwingeloo 01 | Ministerie van Infrastructuur en Waterstaat | Geen    |

  @project-cycle @project-browse-details
  Scenario Outline: All users can VIEW the PRIORITY project DETAILS
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Project' tab of the priority project
    Then the field 'Projectnaam' has value '<project_naam>'
    And the field 'Reservering' has value '<reservering>'
    And the field 'Maatgevend hexagon' has value '<maatgevend_hexagon>'

    Examples: Priority project details and the user roles involved
      | user_role | dossier_id            | project_naam            | reservering  | actualisatie                               | percentage   | maatgevend_hexagon  |
      | VIEWER    | CukePriorityProject01 | Wegverkeer Dwingeloo 01 | 24BFVangAN5R | AERIUS_SET02_001 - PP01 - ACTUALISATIE.zip | toegekend 0% | x: 225088 y: 539438 |
      | EDITOR    | CukePriorityProject02 | Stallen Tilburg 01      | 24BFVamvxMCC | AERIUS_SET02_001 - PP02 - ACTUALISATIE.zip | toegekend 0% | x: 147382 y: 398293 |
      | SUPERUSER | CukePriorityProject03 | Industrie Utrecht 01    | 24BFVapVbLjT | AERIUS_SET02_001 - PP03 - ACTUALISATIE.zip | toegekend 0% | x: 135285 y: 461585 |
      | ADMIN     | CukePriorityProject01 | Wegverkeer Dwingeloo 01 | 24BFVangAN5R | AERIUS_SET02_001 - PP01 - ACTUALISATIE.zip | toegekend 0% | x: 225088 y: 539438 |

  @project-cycle @project-browse-partial
  Scenario Outline: All users can VIEW the PARTIAL project DETAILS
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Project' tab of the priority project
    And the user opens priority sub project with dossierID '<sub_dossier_id>'
    And the field 'Deelproject' has value '<sub_dossier_id>'
    And the field 'Referentie' has value '<reference>'
    And the field 'Initiatiefnemer' has value '<initiator>'

    Examples: Partial project details and the user roles involved
      | user_role | dossier_id            | sub_dossier_id                 | reference    | initiator |
      | VIEWER    | CukePriorityProject01 | Dijkslootweg                   | 24BFVaoDg4Lj | MinIenM   |
      | EDITOR    | CukePriorityProject01 | Aansluiting N371               | 24BFVaodgA1N | MinIenM   |
      | SUPERUSER | CukePriorityProject02 | Stallen Kalfjes                | 24BFVaooyZYD | MinIenM   |
      | ADMIN     | CukePriorityProject01 | Dijkslootweg                   | 24BFVaoDg4Lj | MinIenM   |

  @project-cycle @project-browse-actualization
  Scenario Outline: All users can VIEW the priority project ACTUALIZATION
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Actualisatie' tab of the priority project
    And the field 'Actualisatie' has value '<actualisatie>'

    Examples: Priority project details and the user roles involved
      | user_role | dossier_id            | actualisatie                               |
      | VIEWER    | CukePriorityProject01 | AERIUS_SET02_001 - PP01 - ACTUALISATIE.zip |
      | EDITOR    | CukePriorityProject02 | AERIUS_SET02_001 - PP02 - ACTUALISATIE.zip |
      | SUPERUSER | CukePriorityProject03 | AERIUS_SET02_001 - PP03 - ACTUALISATIE.zip |
      | ADMIN     | CukePriorityProject01 | AERIUS_SET02_001 - PP01 - ACTUALISATIE.zip |

  @project-cycle @project-delete-partial @admin
  Scenario Outline: As an EDITOR, ADMIN or SUPERUSER i can DELETE PARTIAL projects from a priority project
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Project' tab of the priority project
    And the user opens priority sub project with dossierID '<sub_dossier_id>'
    And the priority sub project is deleted
    Then the notification panel contains the message 'Prioritair deelproject met kenmerk ";" is succesvol verwijderd'
    And the notification panel contains no errors

    Examples: Partial project dossier ids to delete
      | user_role | dossier_id            | sub_dossier_id   |
#      | EDITOR    | CukePriorityProject01 | Dijkslootweg     |
#      | ADMIN     | CukePriorityProject01 | Aansluiting N371 |
      | SUPERUSER | CukePriorityProject02 | Stallen Kalfjes  |

  @project-cycle @project-delete-actualization @admin
  Scenario Outline: As a SUPERUSER or ADMIN i can DELETE an ACTUALIZATION file from a priority project
    Removing actualization re-enables status changes (given there are no unapproved partial projects) 
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    And the user switches to the 'Actualisatie' tab of the priority project
    And the actualization is deleted
    Then the notification panel contains no errors
    When the user switches to the 'Dossier' tab of the priority project
    Then the priority project status button is 'enabled'
    And the history contains entry: 'Actualisatie bestand verwijderd'

    Examples: Priority projects to remove actualization from
      | user_role | dossier_id            |
      | SUPERUSER | CukePriorityProject01 |
#      | ADMIN     | CukePriorityProject02 |
      | SUPERUSER | CukePriorityProject03 |

  @project-cycle @project-delete @admin
  Scenario Outline: As a SUPERUSER or ADMIN i can DELETE priority projects
    Given the user is logged in as '<user_role>'
    And the menu item 'prioritary projects' is selected
    And the search for '<dossier_id>' is started
    When the search suggestion '<dossier_id>' is selected
    And the priority project is deleted
    Then the notification panel contains the message 'Prioritair project met project-ID "<dossier_id>" is succesvol verwijderd'
    And the notification panel contains no errors

    Examples: Priority project dossier ids to delete
      | user_role | dossier_id            |
      | SUPERUSER | CukePriorityProject01 |
#      | ADMIN     | CukePriorityProject02 |
      | SUPERUSER | CukePriorityProject03 |

  @project-navigation @AER-309
  Scenario: A priority project without applications shows a disabled application button
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    When the user can import 'AERIUS_SET01_000 - PP01 - RESERVERING.zip' with dossierID 'PP_Navigation' as a new priority project
    Then the priority project navigation button 'Aanvraag' is 'disabled'

  @project-navigation @AER-309
  Scenario: A priority project with a single application can be accessed directly by clicking the application button
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    And the search for 'PP_Navigation' is started
    When the search suggestion 'PP_Navigation' is selected
    And the user switches to the 'Project' tab of the priority project
    And the partial project file 'AERIUS_SET01_01 - PDP01.01 - Dijkslootweg.gml.zip' is imported
    And the user switches to the 'Project' tab of the priority project
    Then the priority project navigation button 'Aanvraag' is 'enabled'
    And the priority sub project can be opened by clicking the 'Aanvraag' button
    And the user switches to the 'Project' tab of the priority project

  @project-navigation @AER-309
  Scenario: A priority project with multiple applications shows a disabled application button on the project overview page
            The application button is enabled after the user opens an application from the list
            Clicking on the project button here returns the user to the main project page where the button is disabled again 
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    And the search for 'PP_Navigation' is started
    When the search suggestion 'PP_Navigation' is selected
    And the user switches to the 'Project' tab of the priority project
    And the partial project file 'AERIUS_SET01_01 - PDP02.01 - Kalfjes.gml.zip' is imported
    And the user switches to the 'Project' tab of the priority project
    And the priority project navigation button 'Aanvraag' is 'disabled'
    And the user opens priority sub project with dossierID 'Stallen Kalfjes'
    Then the user switches to the 'Project' tab of the priority project
    And the priority project is deleted

  @project-completion @AER-1315 @AER-1428
  Scenario: A priority project can be completed by a superuser without option to reopen
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    When the user can import 'AERIUS_SET01_000 - PP01 - RESERVERING.zip' with dossierID 'PP_Completion' as a new priority project
    And the priority project status button is 'enabled'
    # After admin imports the project the superuser can login and complete it
    # Active URL is remembered so navigation steps can be skipped for convenience

    Given the user is logged in as 'SUPERUSER'
    And the priority project status button is 'enabled'
    When the priority project status is set to 'Voltooid'
    Then the priority project status button is 'disabled'

  @project-completion @AER-1315 @AER-1428
  Scenario: A previously completed priority project can be reopened and completed by an administrator
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    And the search for 'PP_Completion' is started
    When the search suggestion 'PP_Completion' is selected
    Then the priority project status button is 'enabled'
    And the priority project status is set to 'Voltooien'
    And the priority project is deleted

  @project-authority @AER-1313 @AER-1426
  Scenario: The competent authority of the currently logged in user is applied instead of submitter field in the uploaded GML file
            Value in submitter field should no longer be shown since implementation AER-1426
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    When the user can import 'AERIUS_RESERVERING_NoSubmitter.zip' with dossierID 'PP_NoSubmitter' as a new priority project
    And the notification panel contains no errors
    And the priority project status button is 'enabled'
    And the history contains entry: 'Status → In uitvoering'
    And the history contains entry: 'Status → Ingeladen Segment → Prioritaire projecten Zaaknummer → PP_NoSubmitter Kenmerk → S4pEsyxxgQ5K'
    Then the field 'Projectnaam' has value 'Bevoegde Verantwoordelijke'
    And the field 'Project-ID' has value 'PP_NoSubmitter'
    And the field 'Bevoegd gezag' has value 'Ministerie van Infrastructuur en Waterstaat'
    And the field 'Verantwoordelijk PAS partner' is not present
    And the priority project is deleted

  @project-history @AER-1326 @AER-1649
  Scenario: The priority project history should log activity regardless of duplicate [filename] entries
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    And the user can import 'AERIUS_SET01_000 - PP01 - RESERVERING.zip' with dossierID 'PP_History' as a new priority project
    When the user switches to the 'Actualisatie' tab of the priority project
    And the actualization project file 'AERIUS_SET01_001 - PP01 - ACTUALISATIE.zip' is imported
    And the actualization project file is replaced by 'AERIUS_SET01_001 - PP01 - ACTUALISATIE.zip'
    And the fact sheet file 'AERIUS_FactSheet.pdf' is imported
    And the fact sheet file is replaced by 'AERIUS_FactSheet.pdf'
    And the user switches to the 'Dossier' tab of the priority project
    Then the history contains '2' identical entries with text: 'Actualisatie bestand → AERIUS_SET01_001 - PP01 - ACTUALISATIE.zip'
    And the history contains '2' identical entries with text: 'Uitgangspuntenrapport bestand → AERIUS_FactSheet.pdf'
    And the priority project is deleted

  @project-actualization @AER-1423 @AER-1427 @rainy
  Scenario: The priority project actualization import should not allow forbidden sector 9000 plan
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    When the user can import 'AERIUS_SourceOnlySector9000.gml' with dossierID 'Actualize Illegal Plan' as a new actualization project with errors
    Then the user is confronted with error message: 'Een bron met sector 9000 "Plan" is gevonden. Deze is niet toegestaan in een prioritair project.'
    And the user cancels the file import

  @project-actualization @AER-1423 @AER-1427 @rainy
  Scenario: The priority project actualization import should not allow forbidden sector 9999 other...
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    When the user can import 'AERIUS_SourceOnlySector9999.gml' with dossierID 'Actualize Illegal Other' as a new actualization project with errors
    Then the user is confronted with error message: 'Een bron met sector 9999 "Anders..." is gevonden. Deze is niet toegestaan in een prioritair project.'
    And the user cancels the file import

  @project-actualization @AER-1423 @AER-1427 @rainy
  Scenario: The priority project reservation import should not allow forbidden sector 9000 plan
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    When the user can import 'AERIUS_SourceResultsSector9000.gml' with dossierID 'Reserve Illegal Plan' as a new priority project with errors
    Then the user is confronted with error message: 'Een bron met sector 9000 "Plan" is gevonden. Deze is niet toegestaan in een prioritair project.'
    And the user cancels the file import

  @project-actualization @AER-1423 @AER-1427 @rainy
  Scenario: The priority project reservation import should not allow forbidden sector 9999 other...
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    When the user can import 'AERIUS_SourceResultsSector9999.gml' with dossierID 'Reserve Illegal Other' as a new priority project with errors
    Then the user is confronted with error message: 'Een bron met sector 9999 "Anders..." is gevonden. Deze is niet toegestaan in een prioritair project.'
    And the user cancels the file import

  @AER-1360
  Scenario: The partial project import dialog accepts a custom description
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    And the user can import 'AERIUS_Reservering_AER-1360.gml' with dossierID 'PP_CustomDescription' as a new priority project
    When the user switches to the 'Project' tab of the priority project
    And the partial project file 'AERIUS_Deelaanvraag_AER-1360.gml' is imported with description 'Partially Customized'
    And the user switches to the 'Project' tab of the priority project
    And the user opens priority sub project with dossierID 'Partially Customized'
    Then the field 'Eigen beschrijving' has value 'Partially Customized'
    And the user switches to the 'Project' tab of the priority project
    And the priority project is deleted

  @AER-1528
  Scenario: As a ADMIN i can ADD a NEW APPLICATION file to a priority project
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    And the user can import 'AERIUS_Reservering_AER-1528_GeenGrenswaarde.gml' with dossierID 'PP_NoBoundary' as a new priority project
    When the user switches to the 'Project' tab of the priority project
    And the partial project file 'AERIUS_Aanvraag_AER-1528_GeenGrenswaarde_GeenResultaat.gml' is imported with description 'Geen Grenswaarde'
    When the user switches to the 'Project' tab of the priority project
    And the partial project file 'AERIUS_Aanvraag_AER-1528_WelGrenswaarde_GeenResultaat.gml' is imported with description 'Wel Grenswaarde'
    Then wait until the application dossier status is 'Toetsbaar'

  @AER-1528 @cal19ignore
  Scenario Outline: Check the Assesment overview
    Given the user is logged in as 'ADMIN'
    And the menu item 'prioritary projects' is selected
    And the search for 'PP_NoBoundary' is started
    When the search suggestion 'PP_NoBoundary' is selected
    When the user switches to the 'Project' tab of the priority project
    And the user opens priority sub project with dossierID '<sub_project>'
    Then the user switches to the Assessment tab of the priority sub project
    And contains Nature area '<nature_area>' with '<dev_icon>' and '<dev_room_message>'

    Examples:
      | sub_project      | nature_area             | dev_icon           | dev_room_message                     |
      | Geen Grenswaarde | Oostelijke Vechtplassen | AAAABJRU5ErkJggg== | empty                                |
      | Wel Grenswaarde  | Oostelijke Vechtplassen | UAAAAASUVORK5CYII= | gemiddeld < 1 mol/j tekort op 646 ha |

  @AER-1587
  Scenario Outline: The priority project overview should show the appropriate progress icon for percentage assigned
    Given the user is logged in as 'SUPERUSER'
    And the menu item 'prioritary projects' is selected
    And the user can import 'AERIUS_PP_reservation_AER_1587.gml' with dossierID 'AER-1587 - progress icon' as a new priority project
    And the user switches to the 'Project' tab of the priority project
    And the partial project file '<file>' is imported with description '<description>'
    And the user switches to the 'Project' tab of the priority project
    When the user opens priority sub project with dossierID '<description>'
    And the application dossier status is set to 'ASSIGNED'
    And the user switches to the 'Project' tab of the priority project
    Then the priority project progress bar shows '<percentage>' assigned
    And the priority project progress bar title is '<progress_title>'
    And the priority project indicative hexagon is '<indicative_hexagon>'
    When the menu item 'prioritary projects' is selected
    Then the priority project 'AER-1587 - progress icon' progress indicator is '<indicator>'
    And the direct search for 'AER-1587 - progress icon' is started
    And the priority project is deleted
    
    Examples: Partial projects required to test all possible usage icons
	    | file                                | description      | percentage | progress_title              | indicative_hexagon                                       | indicator   |
	    | AERIUS_PP_request_AER_1587_50.gml   | AER-1587 - 50%   | 50%        | Percentage toegekend: 50,0  | x: 211408 y: 458147 (50% uitgegeven) - hexagon: 4592704  | UNDER_FIFTY |
	    | AERIUS_PP_request_AER_1587_50.1.gml | AER-1587 - 50.1% | 50.1%      | Percentage toegekend: 50,1  | x: 211408 y: 458147 (50% uitgegeven) - hexagon: 4592704  | ABOVE_FIFTY |
	    | AERIUS_PP_request_AER_1587_100.gml  | AER-1587 - 100%  | 100%       | Percentage toegekend: 100,0 | x: 211408 y: 458147 (100% uitgegeven) - hexagon: 4592704 | HUNDRED     |
