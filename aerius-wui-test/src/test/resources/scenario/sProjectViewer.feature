@nightly @scenarios @scenarios-viewer @cal19ignore
Feature: Special viewer permissions can be setup in order for the API key to get access to specific projects in Register
  If permission is granted a user can login and request result sets for a project and partial projects with status assigned
  Once the import is completed the project deposition results can be viewed from the scenario viewer

  Background:
    Given AERIUS SCENARIO is open

  @project-request
  Scenario: An authorized user with viewer permission can request priority project result sets
    Given the menu item -scenarios- is selected
    And the user is logged in using api key '7a60054572184f40b5e01436547be09b'
    When the user requests results for project key 'pp_Flevoland_3_1700'
    And wait for 1 seconds
    Then the notification panel contains the message 'Verzoek om een export van projectreferentienummer pp_Flevoland_3_1700 te starten is verzonden.'
    And the scenarios overview contains job id 'pp_Flevoland_3_1700' with status 'Wachtrij'
    And the notification panel contains no errors

  @project-request @rainy
  Scenario: An authorized user without viewer permission receives an `insufficient permissions` notification
    Given AERIUS SCENARIO is open
    And the menu item -scenarios- is selected
    And the user is logged in using api key '7a60054572184f40b5e01436547be09b'
    When the user requests results for project key 'pp_Zuid-Holland_13_9000'
    Then the notification panel contains the message 'The user does not have the permission to export given priority project.'
    And the notification panel contains no errors
