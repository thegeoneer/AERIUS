@system-message-scenario @nightly
Feature: The system message allows an administrator to add messages to the AERIUS applications to inform users about certain events
    This feature is the same for all products so this test only confirms presence of the message entry page for AERIUS scenario 
    Full test coverage of this feature is performed on AERIUS Calculator


  @system-message-page-presence
  Scenario: As an administrator i can open the system message set up page 
    When AERIUS SCENARIO is open
    Then the system message entry page is opened 

