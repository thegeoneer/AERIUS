@nightly @import
Feature: Menu options to switch between start, emission sources and results pages  
  Search functionality to focus on areas on the map and options to change year and substance settings 
  Importing sources and results from a (zipped) GML file in SCENARIO

  Background:
    Given AERIUS SCENARIO is open

  @menu-items
  Scenario: As a user i have access to all available menu items
    When the menu item -results- is selected
    When the menu item -start- is selected
    When the menu item -emission sources- is selected

  @switch-year
  Scenario: As a user i can change the calculation year
    Given the taskbar year is 2016
    When the taskbar year is set to 2030
    Then the taskbar year is 2030

  @switch-substances
  Scenario: As a user i can switch between substances NOx+NH3, NOx and NH3
    Given the taskbar substance is 'NOx+NH3'
    When the taskbar substance is set to 'NOx'
    Then the taskbar substance is 'NOx'
    When the taskbar substance is set to 'NH3'
    Then the taskbar substance is 'NH3'
    When the taskbar substance is set to 'NOx+NH3'
    Then the taskbar substance is 'NOx+NH3'

  @search-import-compare @AER-1788
  Scenario: As a user i can search for a specific area and import a zipped GML
    Given the search for 'nieuwkoop' is started
    And the search suggestion 'Nieuwkoopse Plassen & De Haeck' is selected
    And the startup is set to import a file
    When the ZIP file 'comparison_results.zip' is imported
    And the scenario import ends
    Then the notification panel contains no errors
    And the notification panel contains the message 'Uw bestand is geïmporteerd.Er zijn 2 bronnen toegevoegd.'
    And the notification panel contains the message 'Het rekenjaar is gewijzigd aan de hand van het geïmporteerde bestand.'
    And the situations are switched
    And the situations are switched
    #
    And situation 1 is selected
    And the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Nieuwkoopse Plassen & De Haeck'
    And the result for Maximum in HabitatArea 'H3150baz' is '3,48'
    And the result for Maximum in HabitatArea 'H91D0' is '1,69'
    And the results table is set to show the area 'Botshol'
    And the result for Maximum in HabitatArea 'H7210' is '0,17'
    And the results table is set to show the area 'Nieuwkoopse Plassen & De Haeck'
    #
    And situation 2 is selected
    And the results table is set to show the area 'Kennemerland-Zuid'
    And the result for Maximum in HabitatArea 'H2180A' is '0,05'
    #
    And the comparison tab is selected
    And the result for Maximum in HabitatArea 'H2180A' is '0,01'
