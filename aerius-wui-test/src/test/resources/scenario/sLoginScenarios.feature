@nightly @scenarios @scenarios-login
Feature: The primary requirement to use Connect from scenario is a login page to authenticate the user and a menu item to get there
  Validations are performed on e-mail and API-key to determine if a user is allowed to access scenario connect features 
  Any erroneous or invalid input should be detected and brought to the attention of the user with validation/error-mesaages

  @request-apikey-valid
  Scenario Outline: As a user i can request an API key by supplying a valid e-mail address  
    Given AERIUS SCENARIO is open
    And the menu item -scenarios- is selected
    When the user requests an api key for e-mail address '<email>'
    Then the notification panel contains the message 'Verzoek om een API-Key te genereren is succesvol verzonden, u ontvangt een e-mail op <email> wanneer het verzoek is verwerkt.'
    And the notification panel contains no errors

    Examples: Valid e-mail address(es) to request API key(s) for
      | email                         |
      | aeriusmail+connario@gmail.com |


  @request-apikey-invalid
  Scenario Outline: As a user i get an error message when requesting an API key using an invalid e-mail address  
    Given AERIUS SCENARIO is open
    And the menu item -scenarios- is selected
    When the user requests an api key for e-mail address '<email>'
    Then a validation message is showing with the text '<email> is geen geldig e-mailadres.'
    And the notification panel contains no errors

    Examples: Invalid e-mail addresses used to test input validation 
      | email                | 
      | aeriusm@il@gm@il.com |
      | aeriusmailgmail.com  |
      | aerius.mail@gmail    |

  @login-apikey-valid
  Scenario: As a user i can authenticate for Connect by providing a valid API key  
    Given AERIUS SCENARIO is open
    And the menu item -scenarios- is selected
    When the user authenticates with api key '7a60054572184f40b5e01436547be09b'
    Then the notification panel contains no errors
    And the scenarios overview page is shown

  @login-apikey-invalid
  Scenario: An error message is shown when authenticating with an invalid API key  
    Given AERIUS SCENARIO is open
    And the menu item -scenarios- is selected
    When the user authenticates with api key 'ab657dc8821f4270a97f037e46b927e3'
    Then the notification panel contains the message 'The API key ""ab657dc8821f4270a97f037e46b927e3"" is invalid.;Code: 40007'

  @login-apikey-nokey
  Scenario: A validation error message is shown when authenticating using anything else but an API key  
    Given AERIUS SCENARIO is open
    And the menu item -scenarios- is selected
    When the user authenticates with api key 'd3d00rU1ng3t03t5t3c0d3150nju15t'
    Then a validation message is showing with the text 'd3d00rU1ng3t03t5t3c0d3150nju15t is geen geldig UID.'
