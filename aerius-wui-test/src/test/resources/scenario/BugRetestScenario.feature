@nightly @bug-retest
Feature: Bug retest for scenario 

  Background: Open scenario calculator to login and open new calculation setup window
    Given AERIUS SCENARIO is open

  @AER-1766
  Scenario: A check to see that situation 1 and 2 and difference tab is still visible after switching menu items
    Given the startup is set to import a file
    And the ZIP file 'comparison_results.zip' is imported
    And the menu item -utils- is selected
    When the menu item -results- is selected
    Then the tabs Situation 1, 2 and Comparison are visible
    And the comparison tab is selected
    And the tabs Situation 1, 2 and Comparison are visible
