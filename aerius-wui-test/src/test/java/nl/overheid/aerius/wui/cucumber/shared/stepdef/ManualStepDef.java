/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

/**
 * Step definitions for manipulating images.
 */
public class ManualStepDef {

  private static final String PNG = "png";
  private static final String MANUAL_PATH = "manual/";
  private static final String IMAGES = "images/";
  private static final String PROCESSED_PATH = "/processed";

  private String product;
  private File processedFile;

  @Given("^process images for product '(.+)'$")
  public void process_images_for_product(final String product) {
    this.product = product;
  }

  @Given("^clean processed images$")
  public void clean_processed_images() {
    final File path = getProcessedPath();
    if (path.exists()) {
      path.delete();
    }
    path.mkdirs();
  }

  @Given("^manipulate image '(.+)'$")
  public void name(final String name) throws IOException {
    final File path = getProcessedPath();
    final String pngImage = name + "." + PNG;
    processedFile = new File(path, pngImage);
    if (processedFile.exists()) {
      processedFile.delete();
    }
    final BufferedImage read = ImageIO.read(new File(getImagesPath(), pngImage));
    final BufferedImage bi = new BufferedImage(read.getWidth(), read.getHeight(), read.getType());
    final Graphics2D graphics2D = bi.createGraphics();
    graphics2D.drawImage(read, 0, 0, null);
    graphics2D.dispose();
    ImageIO.write(bi, PNG, processedFile);
  }

  @Then("^crop starting at (\\d+), *(\\d+)$")
  public void crop_image(final int x, final int y) throws IOException {
    final BufferedImage baseImage = ImageIO.read(processedFile);
    final BufferedImage dest = baseImage.getSubimage(x, y, baseImage.getWidth() - x, baseImage.getHeight() - y);
    ImageIO.write(dest, PNG, processedFile);
  }

  @Then("^crop starting at (\\d+), *(\\d+) with size (\\d+), *(\\d+)$")
  public void crop_image(final int x, final int y, final int width, final int height) throws IOException {
    final BufferedImage baseImage = ImageIO.read(processedFile);
    final BufferedImage dest = baseImage.getSubimage(x, y, width, height);
    ImageIO.write(dest, PNG, processedFile);
  }

  @Then("^add image '(.+)' at position (\\d+), *(\\d+)$")
  public void add_image(final String added, final int x, final int y) throws IOException {
    final BufferedImage baseImage = ImageIO.read(processedFile);
    final BufferedImage addedImage = ImageIO.read(new File(ManualStepDef.class.getProtectionDomain().getCodeSource().getLocation().getPath()
        + MANUAL_PATH + IMAGES +  added + ".png"));
    baseImage.createGraphics().drawImage(addedImage, x, y, null);
    ImageIO.write(baseImage, PNG, processedFile);
  }

  private File getProcessedPath() {
    final File path = getPath(true);
    if (!path.exists()) {
      path.mkdirs();
    }
    return path;
  }

  private File getImagesPath() {
    return getPath(false);
  }

  private File getPath(final boolean processed) {
    return new File(ManualStepDef.this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath(),
        MANUAL_PATH + product + (processed ? PROCESSED_PATH : ""));
  }
}
