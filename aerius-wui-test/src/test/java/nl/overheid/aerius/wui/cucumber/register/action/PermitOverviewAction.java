/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.action;

import java.time.Duration;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for generating an export using
 * the export dialog.
 */
public class PermitOverviewAction {
  private static final Logger LOG = LoggerFactory.getLogger(PermitOverviewAction.class);

  private final FunctionalTestBase base;

  public PermitOverviewAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Selects permit from the permit overview table
   */
  public void eSelectPermitFromOverview(final String permitDescription) {

    boolean permitSelected = false;
    base.buttonHover(TestIDRegister.PERMIT_REFRESH_BUTTON);
    base.browserWait(Duration.ofSeconds(3));

    final WebElement permitOverviewTable = base.buttonHover(TestIDRegister.CELLTABLE_APPLICATIONS);

    final List<WebElement> permits = permitOverviewTable.findElements(By.xpath("//div[contains(@class, 'Label')]"));
    for (final WebElement permit : permits) {
      if (permit.getText().trim().contains(permitDescription)) {
        permit.click();
        permitSelected = true;
        LOG.info("Permit selected:'" + permitDescription + "'");
        base.browserWaitForDrawAnimation();
        break;
      }
    }
    if (!permitSelected) {
      Assert.fail("No permit found with description: " + permitDescription);
    }
  }

}
