/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.shared.action.ImportAction;

/**
 * This class contains all the methods applicable for Priority Projects
 */
public class PriorityProjectAction {
  private static final Logger LOG = LoggerFactory.getLogger(PriorityProjectAction.class);

  private final FunctionalTestBase base;
  private final ImportAction imports;

  public PriorityProjectAction(final FunctionalTestBase testRun) {
    base = testRun;
    imports = new ImportAction(base);
  }

  /**
   * Clicks the new application import button
   */
  public void clickImportButton() {
    base.buttonClick(TestIDRegister.BUTTON_NEW_APPLICATION);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Clicks the new priority project import button
   */
  public void clickNewPriorityProjectButton() {
    base.buttonClick(TestIDRegister.PRIORITYPROJECT_NEW_BUTTON);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Clicks the add new actualization project button in priority projects overview page
   */
  public void clickAddActualizationProjectButton() {
    base.buttonClick(TestIDRegister.ACTUALISATION_NEW_BUTTON);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Clicks the add actualization button in actualization tab of existing priority project
   */
  public void clickAddActualisationButton() {
    base.buttonClick(TestIDRegister.PRIORITYPROJECT_ACTUALIZATION_ADD_BUTTON);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Clicks the replace actualization button in actualization tab of existing priority project
   */
  public void clickReplaceActualizationButton() {
    base.buttonClick(TestIDRegister.PRIORITYPROJECT_ACTUALIZATION_REPLACE_BUTTON);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Clicks the add / replace fact sheet button in priority projects
   * Searching buttons using text() due to absence of debug id's
   */
  public void clickAddFactSheetButton() {
    base.buttonClick(base.waitForElementVisible(base.xpath("//button[text()='Uitgangspuntenrapport toevoegen' or text()='Add factsheet']")));
    base.browserWaitForDrawAnimation();
  }

  public void clickReplaceFactSheetButton() {
    base.buttonClick(base.waitForElementVisible(base.xpath("//button[text()='Uitgangspuntenrapport vervangen' or text()='Replace factsheet']")));
    base.browserWaitForDrawAnimation();
  }

  /**
   * Clicks the delete actualization button in priority projects
   */
  public void clickDeleteActualizationButton() {
    base.buttonClick(TestIDRegister.PRIORITYPROJECT_ACTUALIZATION_DELETE_BUTTON);
    base.browserAlertAccept();
    base.waitForElementInvisible(base.id(TestIDRegister.PRIORITYPROJECT_ACTUALIZATION_DELETE_BUTTON));
    LOG.debug("Actualization file was succesfully deleted.");
  }

  /**
   * Clicks the new permit request button in priority projects
   */
  public void clickNewRequestButton() {
    final WebElement newRequestButton = base.getElementByXPath("//button[contains(text(),'Nieuwe aanvraag')]");
    base.buttonClick(newRequestButton);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Check whether the priority project history contains a specific entry
   * @param expectedEntry
   */
  public void checkProjectHistory(final String expectedEntry) {
    // collect all historical entries (all span elements containing a →)
    // or match full expectedEntry text for inconsistency in actualization deleted entries
    final List<WebElement> entries = base.waitForElementList(base.xpath("//span[contains(.,'→')] | //span[contains(.,'" + expectedEntry + "')]"));

    boolean matchFound = false;
    for (final WebElement entry : entries) {
      // replace any newlines by spaces to match our expectedEntry string
      final String actualText = entry.getText().replaceAll("\\n", " ");
      if (expectedEntry.equalsIgnoreCase(actualText)) {
        matchFound = true;
      }
    }
    assertTrue("Unable to find text: " + expectedEntry, matchFound);
  }

  /**
   * Check the amount of occurrences of a specific entry in project history
   * @param expectedEntry
   */
  public void checkProjectHistoryOccurences(final String expectedEntry, final int expectedAmount) {
    final List<WebElement> entries = base.waitForElementList(base.xpath("//span[contains(.,'" + expectedEntry + "')]"));
    assertEquals(expectedAmount, entries.size());
  }

  /**
   * Select and submit a priority project fact sheet file in an open import window
   * @param fileName
   */
  public void importFactSheet(final String fileName) {
    importFile(fileName);
    LOG.info("Imported fact sheet file: '{}'", fileName);
  }

  /**
   * Select and submit a priority project actualization file in an open import window
   * @param fileName
   */
  public void importActualization(final String fileName) {
    importFile(fileName);
    LOG.info("Imported actualization file: '{}'", fileName);
  }

  /**
   * Select and submit a partial project file in an open import window
   * @param fileName
   */
  public void importPartialProject(final String fileName, final String description) {
    imports.importSetDescription(description);
    importFile(fileName);
    LOG.info("Imported partial project file '{}' with description '{}'", fileName, description);
  }

  /**
   * Select and submit a priority project reservation file with a dossier id
   * @param fileName
   * @param dossierId
   */
  public void importPriorityProject(final String fileName, final String dossierId) {
    imports.importSetApplicationId(dossierId);
    importFile(fileName);
    LOG.info("Imported priority project file '{}' for dossier id '{}'", fileName, dossierId);
  }

  public void importProjectWithErrors(final String fileName, final String dossierId) {
    imports.importSetApplicationId(dossierId);
    importFileWithErrors(fileName);
    LOG.info("Imported erroneous priority project file '{}' for dossier id '{}'", fileName, dossierId);
  }

  public void importFile(final String fileName) {
    final ImportType type = ImportType.determineByFilename(fileName);
    imports.importSelectFile(fileName, type);
    imports.importConfirm();
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
    base.browserWaitForPopUpPanel();
  }

  public void importFileWithErrors(final String fileName) {
    final ImportType type = ImportType.determineByFilename(fileName);
    imports.importSelectFile(fileName, type);
    imports.importConfirm();
  }

  public void importCancel() {
    imports.importCancel();
    base.browserWaitForPopUpPanel();
  }

  public void openProjectWithId(final String projectId) {
    final String xpath = "//div[3][contains(text(),'" + projectId + "')]";
    base.buttonClick(base.waitForElementVisible(base.xpath(xpath)));
  }

  public void openSubProjectWithId(final String projectId) {
    final String xpath = "//div[contains(text(),'" + projectId + "')]";
    base.buttonClickJs(base.waitForElementVisible(base.xpath(xpath)));
  }

  public void switchToPage(final String page) {
    final String xpath = "//div[contains(text(),'" + page + "')]";
    base.buttonClick(base.waitForElementVisible(base.xpath(xpath)));
  }

  public void switchToTab(final String tab) {
    final String xpath = "//label[contains(text(),'" + tab + "')]";
    base.buttonClickJs(base.waitForElementVisible(base.xpath(xpath)));
  }
  
  public void switchToAssessmentTab() {
    base.buttonClick(base.waitForElementVisible(base.id(TestIDRegister.APPLICATION_TAB_SUB_PROJECT_DOSSIER_REVIEW)));
  }

  /**
   * Search for span radio-button using given text and assert it's expected state
   * @param buttonText a string identifying the button
   * @param expectedState string <code>"enabled"</code> or <code>"disabled"</code> 
   */
  public void buttonStateIs(final String buttonText, final String expectedState) {
    final String xpath = "//span[contains(@class,'gwt-RadioButton')]";
    final List<WebElement> buttons = base.getDriver().findElements(base.xpath(xpath));
    LOG.debug("Found {} buttons using xpath: '{}'", buttons.size(), xpath);

    for (final WebElement button : buttons) {
      LOG.trace("Found radio button with text '{}'.", button.getText());
      if (buttonText.equals(button.getText())) {
        final boolean enabled = button.findElement(base.xpath("./input")).isEnabled();
        assertTrue("Navigation button should be " + expectedState, enabled == "enabled".equals(expectedState));
        break;
      }
    }
  }

  public void checkFieldValue(final String label, final String value) {
    // First wait for the expected value to occur at least somewhere on the page
    base.waitForElementVisible(base.xpath("//div[*[contains(text(), '" + value + "')]]"));
    // XPath looks for label and targets the div next to it which should contain the expected value
    final String xpath = "//div[*[contains(text(), '" + label + "')]]/div[2]";
    String fieldValue;
    if ("empty".equals(value)) {
      // Workaround for empty string in feature file
      fieldValue = "";
    } else {
      fieldValue = value;
    }
    final WebElement tableElement = base.waitForElementVisible(base.xpath(xpath));
    final String foundText = tableElement.getText();
    final String assertMessage = "Found text '" + foundText + "' but should be '" + fieldValue + "'";
    // Finally assert that the expected value is found at the correct position on the page
    assertTrue(assertMessage, foundText.startsWith(fieldValue));
  }

  public void checkFieldNotPresent(final String label) {
    base.waitForElementInvisible(base.xpath("//div[text()='" + label + "']"));
  }

  public void buttonStatusStateis(final String expectedState) {
    final WebElement button = base.waitForElementVisible(base.id(TestIDRegister.PRIORITYPROJECT_ASSIGNCOMPLETE_BUTTON));
    assertTrue("Button should be " + expectedState, button.isEnabled() == "enabled".equals(expectedState));
  }

  public void buttonStatusSet(final String projectStatus) {
    // Avoid 'other element would receive the click'-exception by applying a JS clicks here 
    final WebElement button = base.waitForElementVisible(base.id(TestIDRegister.PRIORITYPROJECT_ASSIGNCOMPLETE_BUTTON));
    base.buttonClickJs(button);
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.BUTTON_PANEL_OK)));
    base.waitForElementInvisible(base.id(TestIDRegister.BUTTON_PANEL_OK));
    assertEquals(projectStatus, button.getText());
  }

  public void togglePriorityFilterPanel() {
    final String xpath = "//div[contains(@id,'" + TestID.BUTTON_COLLAPSEPANEL_PLUSMINUS + "')]";
    base.buttonClick(base.waitForElementVisible(base.xpath(xpath)));
    base.waitForElementVisible(base.id(TestID.BUTTON_FILTER_APPLY));
  }

  public void priorityFilterOptionContains(String filter, List<String> options) {
    final String xpath = "//div[text()='" + filter + "']/following-sibling::div";
    base.buttonClick(base.waitForElementVisible(base.xpath(xpath)));
    List<String> missingOptions = new ArrayList<String>();
    for (final String option : options) {
      LOG.trace("Checking '{}' filter for presence of option '{}'", filter, option);
      final List<WebElement> foundOptions = base.waitForElementList(base.xpath("//div[@class='popupContent']//div[text()=\"" + option + "\"]"));
      final int count = foundOptions.size();
      if (count < 1) {
        missingOptions.add(option);
      }
      // Check provided options and log those containing double white spaces (primarily aimed at nature areas) 
      if (option.contains("  ")) {
        LOG.debug("Found {} filter option containing double whitespace: \"" + option + "\"", filter);
      }
      // Check for double options in the filter option pop up (primarily aimed at nature areas)
      if (count > 1) {
        LOG.debug("Found {} entries for area: \"" + option + "\"", count);
      }
    }
    assertTrue("Missing filter option(s): " + missingOptions.toString(), missingOptions.size() == 0);
  }

  public void checkNatureAreaValue(final String natureArea, final String devIcon, final String devRoomMessage) {
    //Check the Nature area text
    final WebElement el = base.waitForElementVisible(base.xpath("//div[text()= '" + natureArea + "']"));
    assertEquals("Incorrect Nature Area", natureArea, el.getText());

    //Check the Development room icon
    assertTrue("Can't find " + devIcon, el.findElement(base.xpath("./following-sibling::div/div/img")).getAttribute("src").endsWith(devIcon));

    //Check the Development room text, 
    //Workaround for empty string in feature file
    String fieldValue;
    if ("empty".equals(devRoomMessage)) {
      fieldValue = "";
    } else {
      fieldValue = devRoomMessage;
    }

    assertEquals("Incorrect Nature Area", fieldValue, el.findElement(base.xpath("./following-sibling::div/div/div")).getText());
  }

  public void checkProgressBarPercentage(final String expectedPercentage) {
    // The first element in this list contains the assigned percentage
    final String progressBarElements = "//div[contains(@title, 'Percentage')]";
    final String elementStyle = base.waitForElementList(By.xpath(progressBarElements)).get(0).getAttribute("style");
    assertTrue("Progress bar percentage is not " + expectedPercentage, elementStyle.contains("width: " + expectedPercentage));
  }

  public void checkProgressBarTitle(final String expectedTitle) {
    final String progressBarElements = "//div[@title='" + expectedTitle + "']";
    base.waitForElementVisible(By.xpath(progressBarElements));
  }

  public void checkIndicativeHexagon(final String expectedIndication) {
    // TODO: Remove compensating trailing space once the corresponding UI element is properly trimmed
    final String progressBarElements = "//div[text()='" + expectedIndication + " ']";
    base.waitForElementVisible(By.xpath(progressBarElements));
  }

  private String getIconFingerprint(final String percentage) {
    // TODO: Remove this hideous workaround as soon as images are accessible by id
    switch (percentage) {
    case "ZERO": return "NS4yMS05TDIwLjcxLDdaIi8+PC9zdmc+";
    case "UNDER_FIFTY": return "OCAxOS4zNSAxNS44OCIvPjwvc3ZnPg==";
    case "ABOVE_FIFTY": return "OCAxOS4yNiAxNS44OCIvPjwvc3ZnPg==";
    case "HUNDRED": return "MDQgMTAuMzEgNy4wNCIvPjwvc3ZnPg==";
    case "COMPLETE": return "OSAxNC44NyAyMC42MiIvPjwvc3ZnPg==";
    default: return "Unexpected input percentage: " + percentage;
    }
  }

  public void checkProgressIndicator(final String projectName, final String expectedIndicator) {
    final String progressIndicator = "//div[text()='" + projectName + "']/parent::div/child::div[2]/img";
    final String imageSource = base.waitForElementVisible(By.xpath(progressIndicator)).getAttribute("src");
    assertTrue("Progress indicator does not match", imageSource.endsWith(getIconFingerprint(expectedIndicator)));
  }

}
