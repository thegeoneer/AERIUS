/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.action;

import nl.overheid.aerius.shared.test.TestIDAdmin;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.shared.action.MenuAction.MenuHasTestId;

/**
 * Enumeration of all calculator usable menu options.
 */
public enum RegisterMenuOption implements MenuHasTestId {

  DASHBOARD(TestIDRegister.MENU_DASHBOARD),
  APPLICATIONS(TestIDRegister.MENU_APPLICATIONS),
  MESSAGES(TestIDRegister.MENU_MESSAGES),
  PRIORITARY_PROJECTS(TestIDRegister.MENU_PRIORITARY_PROJECTS),
  USER_MANAGEMENT(TestIDAdmin.MENU_USER_MANAGEMENT),
  ROLE_MANAGEMENT(TestIDAdmin.MENU_ROLE_MANAGEMENT);

  private final String testId;

  private RegisterMenuOption(final String testId) {
    this.testId = TestIDRegister.MENU_ITEMS + "-" + testId;
  }

  @Override
  public String getTestId() {
    return testId;
  }

  public enum RegisterSubMenuOption implements MenuHasTestId {

    APPLICATION_DOSSIER(TestIDRegister.APPLICATION_TAB_DOSSIER),
    APPLICATION_DETAILS(TestIDRegister.APPLICATION_TAB_DETAILS),
    APPLICATION_PERMIT_RULES(TestIDRegister.APPLICATION_TAB_PERMIT_RULES),
    APPLICATION_DEVELOPMENT_SPACE(TestIDRegister.APPLICATION_TAB_DEVELOPMENT_SPACE);

    private final String testId;

    private RegisterSubMenuOption(final String testId) {
      this.testId = testId;
    }

    @Override
    public String getTestId() {
      return testId;
    }

  }

}
