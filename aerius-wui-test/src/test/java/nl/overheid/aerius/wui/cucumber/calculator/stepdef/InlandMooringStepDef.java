/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;

import org.junit.Assert;

import cucumber.api.Transform;
import cucumber.api.Transformer;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.NavigationDirection;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionEditorInlandMooring;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionValuesEditorShippingAction;
import nl.overheid.aerius.wui.cucumber.shared.action.MapAction;

/**
 * Set Definition class for Inland Mooring detail screen
 */
public class InlandMooringStepDef {

  private final FunctionalTestBase base;
  private final EmissionEditorInlandMooring iSources;
  private final MapAction map;
  private final EmissionValuesEditorShippingAction shippingEditor;

  public InlandMooringStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    iSources = new EmissionEditorInlandMooring(base);
    map = new MapAction(base);
    shippingEditor = new EmissionValuesEditorShippingAction(base);
  }

  @When("^the inland waterway classification is set to '(.+)'$")
  public void the_inland_waterway_classification_is_set_to(final String waterwayClass) {
    shippingEditor.setWaterwayClass(waterwayClass);
  }

  @When("^the inland mooring ship is a '(.+)' of category (.+) staying (\\d+) hours$")
  public void the_inland_mooring_ship_is_a_(final String name, final String categoryId, final int residenceTime) {
    iSources.setSubItemShippingData(name, categoryId, String.valueOf(residenceTime));
  }

  @Then("^the inland ship name is set to '(.+)'$")
  public void the_inland_ship_name_is_set_to(final String shipName) {
    iSources.setSubItemShipType(shipName);
  }

  @When("^the inland route ship is defined as '(.+)' of type '(.+)' making '(.+)' round trips a day$")
  public void the_inland_route_ship_is_defined_as_a(final String name, final String type, final String trips) {
    iSources.setSubItemInlandRouteData(name, type, trips);
  }

  @Then("^the inland route ship type is set to '(.+)'$")
  public void the_inland_route_ship_type_is_set_to(final String shipType) {
    iSources.setSubItemShipType(shipType);
  }

  @When("^the inland mooring route at '(.+)' row (\\d+) is drawn with coordinates (.+)$")
  public void the_inland_mooring_route_at_row_is_drawn_with_coordinates(final NavigationDirection navDir, final int row, final String coordinates) {
    iSources.subItemInlandRouteSetNew(navDir, row);
    map.clickLineOnMap(coordinates);
  }

  @When("^the inland mooring direction at row (\\d+) is set to '(.*)'$")
  public void the_inland_mooring_direction_at_row_is_set_to(final int row, @Transform(DirectionTransformer.class) final NavigationDirection dir) {
    iSources.setSubItemRouteDirection(row, dir);
  }

  @When("^the inland mooring ship movements at '(.+)' row (\\d+) is set to (\\d+)$")
  public void the_inland_mooring_ship_movements_at_row_is_set_to(final NavigationDirection navDir, final int row, final int movements) {
    iSources.setSubItemRouteMovements(navDir, row, String.valueOf(movements));
  }

  @When("^the inland mooring load at '(.+)' row (\\d+) is set to (\\d+)$")
  public void the_inland_mooring_load_at_row_is_set_to(final NavigationDirection navDir, final int row, final int load) {
    iSources.setSubItemRouteLoad(navDir, row, String.valueOf(load));
  }

  @Then("^the inland waterway classification must be '(.+)'$")
  public void the_inland_waterway_classification_must_be(final String waterwayClass) {
    Assert.assertEquals("WaterWay classification not as expected", waterwayClass, shippingEditor.getWaterwayClass());
  }

  @Then("^the inland mooring route at '(.+)' row (\\d+) is set to '(.+)'$")
  public void the_inland_mooring_route_at_row_is_set_to(final NavigationDirection navDir, final int row, final String route) {
    iSources.setInlandMooringRouteNumberAtRow(navDir, row, route);
  }

  @Then("^the inland mooring route at row (\\d+) must be empty$")
  public void the_inland_mooring_route_at_row_must_be_empty(final int row) {
    Assert.assertEquals("Check inland route number", 0, iSources.getInlandMooringRouteNumberAtRow(row));
  }

  @Then("^the inland mooring route row (\\d+) must be (.+)$")
  public void the_inland_mooring_inland_route_row_must_be_visible(final int row, final String visiblity) {
    final boolean visible = "visible".equals(visiblity);
    assert !visible && "invisible".equals(visiblity) : "feature field should be 'visible' or 'invisible'";
    Assert.assertEquals("Check inland route row visibility", visible, iSources.isInlandMooringRowVisible(row));
  }

  @Then("^the inland mooring residence time is set to (\\d+)$")
  public void the_inland_mooring_residence_time_is_set_to(final int shipResidenceTime) {
    iSources.setSubItemResidenceTime(String.valueOf(shipResidenceTime));
  }

  @Then("^the inland mooring residence time must be (\\d+)$")
  public void the_inland_mooring_residence_time_must_be(final int residenceTime) {
    Assert.assertEquals("Check inland ship residence time", String.valueOf(residenceTime), iSources.getSubItemResidenceTime());
  }

  @Then("^the inland mooring route at (\\d+) must be '(.)'$")
  public void the_inland_mooring_route_at_must_be(final int row, final char inlandRouteNumber) {
    Assert.assertEquals("Check inland route number", inlandRouteNumber, iSources.getInlandMooringRouteNumberAtRow(row));
  }

  @Then("^the inland mooring direction at row (\\d+) must be '(.+)'$")
  public void the_inland_mooring_direction_at_row_must_be(final int row, @Transform(DirectionTransformer.class) final NavigationDirection dir) {
    Assert.assertEquals("Check inland direction at row " + row, dir, iSources.getInlandMooringDirectionAtRow(row));
  }

  @Then("^the inland mooring ship movements at '(.+)' row (\\d+) must be (\\d+)$")
  public void the_inland_mooring_ship_movements_at_row_must_be(final NavigationDirection navDir, final int row, final int movements) {
    Assert.assertEquals("Check ship movements at row " + row, String.valueOf(movements), iSources.getInlandMooringMovementsAtRow(navDir, row));
  }

  @Then("^the inland mooring load at '(.+)' row (\\d+) must be (\\d+)$")
  public void the_inland_mooring_load_at_row_must_be(final NavigationDirection navDir, final String row, final String laden) {
    Assert.assertEquals("Check ships at row " + row, String.valueOf(laden), iSources.getInlandMooringLoadAtRow(navDir, row));
  }

  @Then("^the inland route ship type must be '(.+)'$")
  public void the_inland_route_ship_type_must_be(final String shipType) {
    Assert.assertEquals("Check inland ship type", shipType, iSources.getSubItemShipType());
  }

  @Then("^the inland route ship A to B movement must be '(.+)'$")
  public void the_inland_route_ship_atob_movement_must_be(final String trips) {
    Assert.assertEquals("Check inland ship AtoB movements", trips, iSources.getSubItemMovementAtoB());
  }

  @Then("^the inland route ship B to A movement must be '(.+)'$")
  public void the_inland_route_ship_btoa_movement_must_be(final String trips) {
    Assert.assertEquals("Check inland ship BtoA movements", trips, iSources.getSubItemMovementAtoB());
  }

  @Then("^the inland route ship A to B load percentage is set to '(.+)'$")
  public void the_inland_route_ship_atob_is_set_to(final String loadPercentage) {
    iSources.getSubItemLoadPercentAtoB(loadPercentage);
  }

  @Then("^the inland route ship B to A load percentage is set to '(.+)'$")
  public void the_inland_route_ship_btoa_load_is_set_to(final String loadPercentage) {
    iSources.setSubItemLoadPercentBtoA(loadPercentage);
  }

  @Then("^the inland route ship A to B load percentage must be '(.+)'$")
  public void the_inland_route_ship_atob_load_must_be(final String load) {
    Assert.assertEquals("Check inland ship AtoB load", load, iSources.getSubItemLoadPercentAtoB());
  }

  @Then("^the inland route ship B to A load percentage must be '(.+)'$")
  public void the_inland_route_ship_btoa_load_must_be(final String load) {
    Assert.assertEquals("Check inland ship BtoA load", load, iSources.getSubItemLoadPercentBtoA());
  }

  public static class DirectionTransformer extends Transformer<NavigationDirection> {
    @Override
    public NavigationDirection transform(final String direction) {
      return NavigationDirection.valueOf(direction.toUpperCase());
    }
  }

}
