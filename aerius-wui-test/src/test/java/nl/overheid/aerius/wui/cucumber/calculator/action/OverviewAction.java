/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;


import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;

/**
 * This class contains all the methods that apply for handling sources and
 * calculation points from the overview page.
 */
public class OverviewAction {
  private static final Logger LOG = LoggerFactory.getLogger(OverviewAction.class);

  protected final FunctionalTestBase base;

  public OverviewAction(final FunctionalTestBase testRun) {
    this.base = testRun;
  }

  /**
   * Adds a source from the EmissionSourceOverview.
   * Postcondition: source editor is shown
   */
  public void clickAddSourceButton() {
    base.buttonClick(TestID.BUTTON_SOURCE_ADD);
  }

  /**
   * Clicks copy source button in the overview.
   */
  public void clickCopySourceButton() {
    base.buttonClick(TestID.BUTTON_SOURCE_COPY);
  }

  /**
   * Clicks edit source button in the overview.
   */
  public void clickEditSourceButton() {
    base.buttonClick(TestID.BUTTON_SOURCE_EDIT);
  }

  /**
   * Clicks delete source button in the overview.
   */
  public void clickDeleteSourceButton() {
    base.buttonClick(TestID.BUTTON_SOURCE_DELETE);
  }

  /**
   * Delete all calculation points by clicking the purge-button.
   * Precondition: Application displays calculation points overview.
   */
  public void clickDeleteAllCalcPointsButton() {
    base.buttonClick(TestID.BUTTON_PURGE_CALCPOINTS);
  }

  /**
   * Select a source from the source-overview and click the edit button.
   * Precondition: Application displays sources overview.
   * Postcondition: Application selected a source and displays source details.
   * @param label source label
   */
  public void editSourceByLabel(final String label) {
    selectSourceByLabel(label);
    base.buttonClick(TestID.BUTTON_SOURCE_EDIT);
  }

  /**
   * Selects a source from the source-overview and clicks the copy button.
   * Precondition: Application displays sources overview.
   * Postcondition: Application selected a source, copied it and displays source details.
   * @param label source label
   */
  public void copySourceByLabel(final String label) {
    selectSourceByLabel(label);
    base.buttonClick(TestID.BUTTON_SOURCE_COPY);
    base.inputCheckValue(TestID.INPUT_LABEL, label);
  }

  /**
   * Selects a source from the source-overview and clicks the delete button.
   * Precondition: Application displays sources overview.
   * Postcondition: Application selected a source, deleted it.
   * @param label source label
   */
  public void deleteSourceByLabel(final String label) {
    selectSourceByLabel(label);
    base.buttonClick(TestID.BUTTON_SOURCE_DELETE);
  }

  /**
   * Adds a calculation point from the EmissionSourceOverview.
   * Precondition: Application displays calculation point overview.
   * Postcondition: Application added a calculation point and displays source details.
   */
  public void clickAddCalcPointButton() {
    base.buttonClick(TestID.BUTTON_CP_ADD);
  }

  /**
   * Clicks copy-button from the calculation point-overview.
   * Precondition: Application displays calculation point overview.
   * Postcondition: Application displays calculation point details.
   */
  public void clickCopyCalcPointButton() {
    base.buttonClick(TestID.BUTTON_CP_COPY);
  }

  /**
   * Clicks edit-button from the calculation point-overview.
   * Precondition: Application displays calculation point overview.
   * Postcondition: Application displays calculation point details.
   */
  public void clickEditCalcPointButton() {
    base.buttonClick(TestID.BUTTON_CP_EDIT);
  }

  /**
   * Clicks delete-button from the calculation point-overview.
   * Precondition: Application displays calculation point overview.
   * Postcondition: Application deleted calculation point.
   */
  public void clickDeleteCalcPointButton() {
    base.buttonClick(TestID.BUTTON_CP_DELETE);
  }

  /**
   * Select calculation point in calculation point-overview.
   * Precondition: Application displays calculation point overview.
   * Postcondition: Application displays calculation point overview, calculationp oint is selected.
   * @param calculationpointLabel label of calculation point to select
   */
  public void selectCalculationPoint(final String calculationpointLabel) {
    base.buttonHover(TestID.BUTTON_CP_ADD);
    base.overviewSelectCalcpointByName(calculationpointLabel);
  }

  /**
   * Selects a calculation point from the source-overview and clicks the edit button.
   * Precondition: Application displays sources overview.
   * Postcondition: Application selected a calculation point and displays source details.
   * @param label calculation point label
   */
  public void editCalculationPointByLabel(final String label) {
    selectCalculationPoint(label);
    clickEditCalcPointButton();
  }

  /**
   * Selects a calculation point from the source-overview and clicks the copy button.
   * Precondition: Application displays sources overview.
   * Postcondition: Application selected a calculation point, copied it and displays calculation point details.
   * @param label calculation point label
   */
  public void copyCalculationPointByLabel(final String label) {
    selectCalculationPoint(label);
    clickCopyCalcPointButton();
  }

  /**
   * Selects a calculation point from the source-overview and clicks the delete button.
   * Precondition: Application displays sources overview.
   * Postcondition: Application selected a calculation point and deleted it.
   * @param label calculation point label
   */
  public void deleteCalculationPointByLabel(final String label) {
    selectCalculationPoint(label);
    clickDeleteCalcPointButton();
  }

  /**
   * Selects a source and zooms in on it by clicking the source-id tag in the overview.
   * Precondition: Application displays sources overview.
   * Postcondition: Application selected a source, and zoomed in on it
   * @param label source label
   */
  public void clickSourceIdTag(final String label) {
    base.buttonHover(TestID.BUTTON_CALCULATE);
    base.overviewClickIDSourceByName(label);
  }

  /**
   * Deletes all sources by clicking the purge-button.
   * Precondition: Application displays sources overview.
   * Postcondition: Application deleted all sources.
   */
  public void clickDeleteAllSourcesButton() {
    base.buttonClick(TestID.BUTTON_PURGE_SOURCES);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Confirm delete all sources dialog
   */
  public void confirmDeleteAllSources() {
    base.buttonClick(TestID.BUTTON_PANEL_OK);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Cancel delete all sources dialog
   */
  public void cancelDeleteAllSources() {
    base.buttonClick(TestID.BUTTON_DIALOG_CANCEL);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Select source in source overview.
   * Precondition: Application displays source overview.
   * Postcondition: Application displays source overview.
   * @param sourceLabel label of source to select
   */
  public void selectSourceByLabel(final String sourceLabel) {
    final String elementId = TestID.TOOLBOX_SOURCE_PREFIX + "-" + TestID.TOOLBOX;
    base.mouseMoveToElement(elementId);
    base.overviewSelectSourceByName(sourceLabel);
  }

  /**
   * Check availability of a source by label 
   * @param sourceLabel
   * @return true if available or false if not found
   */
  public boolean checkSourceAvailableByLabel(final String sourceLabel) {
    final String xpath = "//div[contains(@id, '" + TestID.DIVTABLE_SOURCE + "-" + TestID.TABLE + "')]";
    return base.waitForElementList(base.xpath(xpath)).size() > 0;
  }

  /**
   * Checks the source emission in the source details viewer pop up from the overview
   * @param sourceLabel the label of the source to check
   * @param emissionRowLabel the label (description) of the emission row to check
   * @param expectedEmission the expected emission value
   *
   */
  public void sourceDetailViewerCheckEmission(final String sourceLabel, final String emissionRowLabel, final String expectedEmission) {

    String rowEmissionValue = "N/A";

    //click on source ID tag to activate details viewer (popup)
    selectSourceByLabel(sourceLabel);
    base.browserWaitForDrawAnimation();

    final WebElement popUpPanel = base.waitForElementVisible(By.xpath("//div[contains(@class, 'popupContent')]"));
    Assert.assertNotNull("PopupPanel WebElement should not be null!", popUpPanel);

    //get list of rows (table sources sector viewer)
    final List<WebElement> allEmissionRows = popUpPanel.findElements(By.xpath("//div[contains(@id, '" + TestID.TABLE_SOURCES_SECTOR_VIEWER + "')]"));
    Assert.assertNotNull("List of WebElements should not be null!", allEmissionRows);
    LOG.debug("Amount of emission rows found: {}", Integer.toString(allEmissionRows.size()));

    for (final WebElement emissionRow : allEmissionRows) {
      // get all columns within current row
      final List<WebElement> allEmissionCols = emissionRow.findElements(By.xpath(".//div[contains(@class, 'Label')]"));
      // search through all columns for the given label
      for (final WebElement emissionCol : allEmissionCols) {
        // if the row label equals the given label the value of the last column in this row should contain the emission value
        if (emissionRowLabel.equals(emissionCol.getText().trim())) {
          rowEmissionValue = allEmissionCols.get(allEmissionCols.size() - 1).getText();
          LOG.debug("Found expected emission value '{}' for source '{}' at row '{}'", rowEmissionValue, sourceLabel, emissionRowLabel);
          break;
        }
      }
    }
    Assert.assertEquals("Source '" + sourceLabel + "' - details emission value check for emission label '" + emissionRowLabel + "'", expectedEmission
        + base.getRunParameterAsString(SeleniumDriverWrapper.UNITS_SOURCE_EMISSION), rowEmissionValue);
  }

  /**
   * Checks the text from the total NOX emission label.
   * @param emissionText
   */
  public void totalEmissionText(final String emissionText) {
    assertEquals("The total NOX emission text is incorrect.", emissionText, base.waitForElementVisible(base.id(TestID.DIV_TOTAL_NOX_EMISSION)).getText());
   }
}
