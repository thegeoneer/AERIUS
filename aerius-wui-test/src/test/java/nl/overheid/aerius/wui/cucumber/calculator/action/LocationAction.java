/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.shared.action.MapAction;

/**
 * This class contains all the methods that apply for setting a source location in the location page
 */
public class LocationAction {

  private static final String LABEL_POSTFIX = "-label";
  protected final FunctionalTestBase base;

  public LocationAction(final FunctionalTestBase testRun) {
    this.base = testRun;
  }

  /**
   * Sets calculation point x coordinate.
   * Precondition: Application displays calculation points overview.
   * @param xCoordinate xCoordinate
   */
  public void setCoordinateX(final String xCoordinate) {
    base.inputSetValue(base.id(TestID.INPUT_X_COORDINATE), xCoordinate);
  }

  /**
   * Sets calculation point y coordinate.
   * Precondition: Application displays calculation points overview.
   * @param yCoordinate yCoordinate
   */
  public void setCoordinateY(final String yCoordinate) {
    base.inputSetValue(base.id(TestID.INPUT_Y_COORDINATE), yCoordinate);
  }

  /**
   * Draws a line on the map given a String of x-y coordinates separated by spaces.
   * Precondition: Application displays location picker, which is shown after using addSource()
   * Postcondition: Application entered source-location and displays source details.
   * @param coordinates
   */
  public void setCoordinatesLine(final String coordinates) {
    base.buttonClick(TestID.BUTTON_LINESOURCE + LABEL_POSTFIX);
    new MapAction(base).clickLineOnMap(coordinates);
  }

  /**
   * Draws a polygon on the map given a String of x-y coordinates separated by spaces.
   * Precondition: Application displays location picker, which is shown after using addSource()
   * Postcondition: Application entered source-location and displays source details.
   * @param coordinates
   */
  public void setAreaCoordinates(final String coordinates) {
    base.buttonClick(TestID.BUTTON_POLYGONSOURCE + LABEL_POSTFIX);
    new MapAction(base).clickLineOnMap(coordinates);
  }

  /**
   * Clicks Next step button.
   */
  public void clickButtonNext() {
    base.buttonClick(TestID.BUTTON_NEXTSTEP);
  }

  /**
   * Sets location for a line-source by coordinates.
   * Precondition: Application displays location picker, which is shown after using addSource()
   * Postcondition: Application entered line-source-location and displays source details.
   * @param stringCoordinates
   */
  public void setLineSourceByCoordinates(final String stringCoordinates) {
    base.buttonClick(TestID.BUTTON_LINESOURCE + LABEL_POSTFIX);
    base.inputSetValue(TestID.INPUT_COORDINATES, stringCoordinates);
  }

  /**
   * Sets location for a polygon-source by coordinates.
   * Precondition: Application displays location picker, which is shown after using addSource()
   * Postcondition: Application entered polygon-source-location and displays source details.
   * @param stringCoordinates
   */
  public void setPolygonSourceByCoordinates(final String stringCoordinates) {
    base.buttonClick(TestID.BUTTON_POLYGONSOURCE + LABEL_POSTFIX);
    base.inputSetValue(TestID.INPUT_COORDINATES, stringCoordinates);
  }

  /**
   * Sets location for a source by entering XY-coordinates in the input boxes and clicks Next Step.
   * Precondition: Application displays location picker, which is shown after using addSource().
   * Postcondition: Application entered source-location and displays source details.
   * @param xCoordinate x-coordinate of source
   * @param yCoordinate y-coordinate of source
   */
  public void setCoordinatesXY(final String xCoordinate, final String yCoordinate) {
    setCoordinateX(xCoordinate);
    setCoordinateY(yCoordinate);
  }

 }
