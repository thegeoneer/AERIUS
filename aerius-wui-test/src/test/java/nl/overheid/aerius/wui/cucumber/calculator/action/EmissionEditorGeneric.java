/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.action;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for the emission values editor for generic sectors,
 * which means setting the amount emission for each substance directly.
 */
public class EmissionEditorGeneric extends SourceAction {

  public EmissionEditorGeneric(final FunctionalTestBase testRun) {
    super(testRun);
  }

  /**
   * Sets the emission for PM10.
   * Precondition: Application displays source details.
   * @param pm10 amount
   */
  public void setEmissionPM10(final String pm10) {
    base.inputSetValue(TestID.INPUT_EMISSION_PM10, pm10);
  }

  /**
   * Sets the emission for NOx.
   * Precondition: Application displays source details.
   * @param nox amount
   */
  public void setEmissionNOx(final String nox) {
    base.inputSetValue(TestID.INPUT_EMISSION_NOX, nox);
  }

  /**
   * Sets the emission for NH3.
   * Precondition: Application displays source details.
   * @param nh3 amount
   */
  public void setEmissionNH3(final String nh3) {
    base.inputSetValue(TestID.INPUT_EMISSION_NH3, nh3);
  }

  /**
   * Sets the emission for all possible substances.
   * Precondition: Application displays source details.
   * @param nh3 amount
   * @param nox amount
   * @param pm10 amount
   */
  public void setGenericEmissions(final String nh3, final String nox, final String pm10) {
    base.inputSetValue(base.idPrefix(TestID.INPUT_EMISSION_NH3), nh3);
    base.inputSetValue(base.idPrefix(TestID.INPUT_EMISSION_NOX), nox);
//    Disabled setting of PM10 since it's removed from themes where it's considered irrelevant 
//    It might still be needed in the future so it's not removed from the tests just yet 
//    base.inputSetValue(TestID.INPUT_EMISSION_PM10, emissionPM10);
  }

  public void clickEditSource() {
    base.buttonClick(TestID.BUTTON_SOURCE_EDIT);
  }

}
