/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.IOException;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.CalculationAction;
import nl.overheid.aerius.wui.cucumber.shared.action.CalculationAction.CalculationScope;
import nl.overheid.aerius.wui.cucumber.shared.action.NotificationAction;

public class CalculationStepDef {

  private final FunctionalTestBase base;
  private final CalculationAction calculationPage;
  private final NotificationAction notificationPage;

  public CalculationStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    calculationPage = new CalculationAction(base);
    notificationPage = new NotificationAction(base);
  }

  @When("^the calculation range is set to (.+) km$")
  public void when_calculation_range_is_set(final String range) {
    calculationPage.setCalculationMaxRange(range);
  }

  @When("^the calculation is started$")
  public void when_calculation_is_started() {
    calculationPage.clickStartCalculationButton();
  }

  @When("^the calculation scope is set to Nature Areas$")
  public void when_calculation_scope_is_set_nature_areas() {
    calculationPage.setCalculationScope(CalculationScope.NATURE_AREAS);
  }

  @When("^the calculation scope is set to Radius$")
  public void when_calculation_scope_is_set_radius() {
    calculationPage.setCalculationScope(CalculationScope.RADIUS);
  }

  @When("^the calculation scope is set to Calculation Points$")
  public void when_calculation_scope_is_set_calcpoints() {
    calculationPage.setCalculationScope(CalculationScope.CALCULATION_POINTS);
  }

  //TODO: Commented out the stepdef for priority project radius because of item @AER-1718.
  @When("^the calculation option priority project radius is enabled$")
  public void when_calculation_priority_project_radius_enabled() {
//    calculationPage.setCalculationOptionEnabled();
  }

  //TODO: Commented out the stepdef for priority project radius because of item @AER-1718.
  @When("^the calculation option priority project type is set to '(.+)'$")
  public void when_calculation_priority_project_type_is_set(final String calculationOptionType) {
//    calculationPage.setCalculationOptionType(calculationOptionType);
  }

  @When("^the calculation project type is set to temporary$")
  public void when_calculation_project_set_temporary() {
    base.buttonClick(TestID.CHECKBOX_TEMPORARY_PROJECT + "-label");
  }

  @When("^the calculation project duration is set to (\\d+) years$")
  public void when_calculation_project_set_year(final String projectDuration) {
    base.inputSelectListItem(TestID.YEARS_LISTBOX, projectDuration, true);
  }

  @Then("^the calculation ends$")
  public void then_calculation_ends() {
    notificationPage.waitForNotificationDone("voltooid", SeleniumDriverWrapper.getInstance().getSettingCalculationTimeout());
  }

  @Then("^the scenario import ends$")
  public void then_scenario_import_ends() {
    notificationPage.waitForNotificationDone("Uw bestand is geïmporteerd", base.getSettingPageLoadTimeout().getSeconds());
  }

}
