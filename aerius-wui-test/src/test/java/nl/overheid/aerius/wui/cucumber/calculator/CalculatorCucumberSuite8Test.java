/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator;

import org.junit.runner.RunWith;

import nl.overheid.aerius.wui.cucumber.base.CucumberBase;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * This is the runner class for cucumber tests for the AERIUS CALCULATOR
 *
 * # general
 * @run: run own tests
 * @single: run regression-test on single features
 * @composite: run composite regression-tests
 * @bug-retest: run bug retest
 *
 * # specific sub tags
 * @source-properties: run tests for source properties
 * @source-properties-default-values: run tests for source properties default value checks
 */
@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {
        "pretty", "html:target/cucumber/nightly/calculator8",
        "json:target/cucumber/nightly/calculator2019/nightly_report.json"
        },
    features = {"src/test/resources/calculator/Calculator2019.feature",
        "src/test/resources/calculator/SourceProperties.feature",
        "src/test/resources/calculator/SourcePropertiesValidation.feature"},
    glue = {
        "nl.overheid.aerius.wui.cucumber.calculator.stepdef", 
        "nl.overheid.aerius.wui.cucumber.shared.action",
        "nl.overheid.aerius.wui.cucumber.shared.stepdef"
        },
    tags = {"@nightly"}
    )
public class CalculatorCucumberSuite8Test extends CucumberBase {

}
