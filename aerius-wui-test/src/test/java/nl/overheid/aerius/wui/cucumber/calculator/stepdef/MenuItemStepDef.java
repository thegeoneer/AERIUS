/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.CalculatorMenuOption;
import nl.overheid.aerius.wui.cucumber.calculator.action.CalculatorMenuOption.CalculatorSubMenuOption;
import nl.overheid.aerius.wui.cucumber.shared.action.MenuAction;

import cucumber.api.java.en.When;

public class MenuItemStepDef {

  private final FunctionalTestBase base;
  private final MenuAction menuItem;

  public MenuItemStepDef() throws IOException, InterruptedException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    menuItem = new MenuAction(base);
  }

  @When("^the menu item -sources- is selected$")
  public void when_menu_select_sources() throws InterruptedException {
    menuItem.selectMenuOption(CalculatorMenuOption.EMISSION_SOURCES);
  }

  @When("^the menu item -calculationpoints- is selected$")
  public void when_menu_select_calculationpoints() throws InterruptedException {
    menuItem.selectMenuOption(CalculatorMenuOption.CALCULATION_POINTS);
  }

  @When("^the menu item -results- is selected$")
 public void when_menu_select_results() throws InterruptedException {
    menuItem.selectMenuOption(CalculatorMenuOption.RESULTS);
  }

  @When("^the menu item -melding- is selected$")
  public void when_menu_select_melding() throws InterruptedException {
     menuItem.selectMenuOption(CalculatorMenuOption.MELDING);
   }

  @When("^the sub menu item -results overview- is selected$")
  public void when_menu_select_results_overview() throws InterruptedException {
    menuItem.selectSubMenuOptionCalculator(CalculatorSubMenuOption.RESULTS_OVERVIEW);
  }

  @When("^the sub menu item -results graph- is selected$")
  public void when_menu_select_results_graph() throws InterruptedException {
    menuItem.selectSubMenuOptionCalculator(CalculatorSubMenuOption.RESULTS_GRAPH);
  }

  @When("^the sub menu item -results table- is selected$")
  public void when_menu_select_results_table() throws InterruptedException {
    menuItem.selectSubMenuOptionCalculator(CalculatorSubMenuOption.RESULTS_TABLE);
  }

  @When("^the sub menu item -results filter- is selected$")
  public void when_menu_select_results_filter() throws InterruptedException {
    menuItem.selectSubMenuOptionCalculator(CalculatorSubMenuOption.RESULTS_FILTER);
  }

  @When("^the menu themeswitch is set to '(.+)'$")
  public void when_menu_set_themeswitch(final String themeSwitchOption) throws InterruptedException {
    menuItem.selectCalculatorThemeSwitchOption(themeSwitchOption);
  }

  @When("^the menu help is toggled on or off$")
  public void when_menu_toggle_help() throws InterruptedException {
    base.buttonClick(TestID.HELP_BUTTON_TOGGLE + "-label");
  }

}
