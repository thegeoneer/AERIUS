/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.scenario.stepdef;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.scenario.stepdef.ScenarioMenuOption.ScenarioSubMenuOption;
import nl.overheid.aerius.wui.cucumber.shared.action.MenuAction;

import cucumber.api.java.en.When;


public class MenuItemStepDef {

  private final FunctionalTestBase base;
  private final MenuAction menuItem;


  public MenuItemStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    menuItem = new MenuAction(base);
  }

  @When("^the menu item -start- is selected$")
  public void when_menu_select_sources() {
    menuItem.selectMenuOption(ScenarioMenuOption.START);
  }

  @When("^the menu item -emission sources- is selected$")
  public void when_menu_select_calculationpoints() {
    menuItem.selectMenuOption(ScenarioMenuOption.EMISSIONSOURCES);
  }

  @When("^the menu item -results- is selected$")
  public void when_menu_select_results() {
    menuItem.selectMenuOption(ScenarioMenuOption.RESULTS);
  }

  @When("^the menu item -scenarios- is selected$")
  public void when_menu_select_scenarios() {
    menuItem.selectMenuOption(ScenarioMenuOption.SCENARIOS);
  }

  @When("^the menu item -utils- is selected$")
  public void when_menu_select_utils() {
    menuItem.selectMenuOption(ScenarioMenuOption.UTILS);
  }

  @When("^the sub menu item -results overview- is selected$")
  public void when_menu_select_results_overview() {
    menuItem.selectSubMenuOptionScenario(ScenarioSubMenuOption.RESULTS_OVERVIEW);
  }

  @When("^the sub menu item -results graph- is selected$")
  public void when_menu_select_results_graph() {
    menuItem.selectSubMenuOptionScenario(ScenarioSubMenuOption.RESULTS_GRAPH);
  }

  @When("^the sub menu item -results table- is selected$")
  public void when_menu_select_results_table() {
    menuItem.selectSubMenuOptionScenario(ScenarioSubMenuOption.RESULTS_TABLE);
  }

  @When("^the sub menu item -results filter- is selected$")
  public void when_menu_select_results_filter() {
    menuItem.selectSubMenuOptionScenario(ScenarioSubMenuOption.RESULTS_FILTER);
  }

  @When("^the menu themeswitch is set to '(.+)'$")
  public void when_menu_set_themeswitch(final String themeSwitchOption) {
    menuItem.selectCalculatorThemeSwitchOption(themeSwitchOption);
  }

  @When("^the menu help is toggled on or off$")
  public void when_menu_toggle_help() {
    base.buttonClick(TestID.HELP_BUTTON_TOGGLE + "-label");
  }

}
