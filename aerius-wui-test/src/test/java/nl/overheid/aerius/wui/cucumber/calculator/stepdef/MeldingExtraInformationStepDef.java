/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;

import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDMelding;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.ImportAction;

import cucumber.api.java.en.When;

public class MeldingExtraInformationStepDef {

  private final FunctionalTestBase base;
  private final ImportAction importFile;

  public MeldingExtraInformationStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    importFile = new ImportAction(base);
  }

  /*
   * HAS PERMIT CURRENT SITUATION
   */
  @When("^the melding has permit for current situation is set to NO$")
  public void when_melding_permit_current_sitution_no() {
    base.buttonClick(TestIDMelding.NB_PERMIT_NO + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding has permit for current situation is set to YES$")
  public void when_melding_permit_current_sitution_yes() {
    base.buttonClick(TestIDMelding.NB_PERMIT_YES + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding has permit for current situation reference number is set to '(.+)'$")
  public void when_melding_permit_current_sitution_reference(final String provinceReference) {
    base.waitForElementVisible(base.id(TestIDMelding.PROVINCE_REF));
    base.inputSetValue(base.id(TestIDMelding.PROVINCE_REF), provinceReference);
  }

  /*
   * CURRENT SITUATION INCLUDED IN MELDING
   */
  @When("^the melding has current situation included is set to NO$")
  public void when_melding_current_situation_included_no() {
    base.buttonClick(TestIDMelding.NB_SOURCE_PERMIT_NO + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding has current situation included is set to YES$")
  public void when_melding_current_situation_included_yes() {
    base.buttonClick(TestIDMelding.NB_SOURCE_PERMIT_YES + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding has current situation reference document PDF '(.+)' is added$")
  public void when_melding_benefactor_add_file(final String fileName) {
    importFile.importSelectFile(fileName, ImportType.PAA);
    base.buttonHover(TestID.INPUT_IMPORTFILE);
  }

  /*
   * PREVIOUS MELDING
   */
  @When("^the melding has previous melding is set to NO$")
  public void when_melding_previous_melding_no() {
    base.buttonClick(TestIDMelding.PREV_MELDING_NO + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding has previous melding is set to YES$")
  public void when_melding_previous_melding_yes() {
    base.buttonClick(TestIDMelding.PREV_MELDING_YES + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding has previous melding reference number is set to '(.+)'$")
  public void when_melding_previous_melding_reference(final String previousMeldingReference) {
    base.inputSetValue(base.id(TestIDMelding.PREV_MELDING_REF), previousMeldingReference);
  }

  /*
   * GENERIC
   */
  @When("^the melding reference description is set to '(.+)'$")
  public void when_melding_reference_description_set(final String referenceDescription) {
    base.inputSetValue(base.id(TestIDMelding.REFERENCE_DESCRIPTION), referenceDescription);
  }

  @When("^the melding truth check is toggled$")
  public void when_melding_truth_check_toggle() {
    base.buttonClick(TestIDMelding.TRUTH + "-" + base.getBrowserCheckboxSetStyle());
  }

}
