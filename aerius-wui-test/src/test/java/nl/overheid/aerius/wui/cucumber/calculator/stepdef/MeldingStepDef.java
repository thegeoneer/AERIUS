/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDMelding;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;

public class MeldingStepDef {
  private static final Logger LOG = LoggerFactory.getLogger(MeldingStepDef.class);

  private final FunctionalTestBase base;

  public MeldingStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
  }

  @When("^the melding forward is initiated from calculator$")
  public void when_click_melding_button() {
    base.buttonClick(TestID.BUTTON_MELDING);
  }

  @When("^the melding forward is confirmed from calculator$")
  public void when_click_melding_forward_button() {
    base.buttonClick(TestID.BUTTON_MELDING_SUBMIT);
    // Accept any dialog popping up at this point
    base.browserAlertAccept();
  }

  @When("^the melding module window is selected$")
  public void when_activate_melding_module() {
    base.browserSwitchToLastWindow();
  }

  @When("^the melding module authentication is done$")
  public void when_authenticate_melding_module() {

    final String eHerkenningModule = "Simulator AD 1.11";
    LOG.debug("Selecting eHerkenning module: '{}'", eHerkenningModule);

    // Select the eHerkenning simulator version
    base.inputSelectListItemByText(By.id("authnServiceId"), eHerkenningModule);

    // Ensure the remember option check box is not checked
    final WebElement checkBox = base.waitForElementVisible(By.id("rememberAuthnService"));
    if (checkBox.isSelected()) {
      LOG.debug("Disabling remember selected eHerkenning module option");
      base.buttonClick(checkBox);
    }

    // Click through the authentication message flow
    LOG.debug("Start authentication message flow");
    base.buttonClick(base.waitForElementVisible(By.name("start")));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//input[@id='theButton']"));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//button[@id='theButton']"));
    base.buttonClick(By.xpath("//input[@id='theButton']"));

    // Wait for the melding module to be loaded completely
    LOG.debug("Completed eHerkenning authentication and loading Melding module");
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  @When("^the melding module is started and authenticated$")
  public void when_melding_is_started_and_authenticated() {
    when_click_melding_button();
    when_click_melding_forward_button();
    when_activate_melding_module();
    when_authenticate_melding_module();
  }

  @When("^the melding module window is closed$")
  public void when_melding_is_closed() {
    base.browserClose();
  }

  @When("^the melding module is continued to the next step$")
  public void when_melding_go_to_next_step() {
    base.buttonClick(TestIDMelding.BUTTON_NEXT_PAGE);
  }

  @When("^the melding module is set to the previous page$")
  public void when_melding_go_to_previous_step() {
    base.buttonClick(TestIDMelding.BUTTON_NEXT_PAGE);
  }

  @When("^the melding is sent successfully$")
  public void when_melding_sent_succesfully() {
    base.browserWait(Duration.ofSeconds(2));
    base.browserCheckForText("2015", true);
  }

}
