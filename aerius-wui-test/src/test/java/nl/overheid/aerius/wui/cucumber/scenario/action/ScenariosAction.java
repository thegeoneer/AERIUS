/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.scenario.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Duration;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.shared.action.ImportAction;

/**
 * This class contains all the methods applicable for Priority Projects
 */
public class ScenariosAction {
  private static final Logger LOG = LoggerFactory.getLogger(ScenariosAction.class);

  private final FunctionalTestBase base;
  private final ImportAction fileImport;

  public ScenariosAction(final FunctionalTestBase testRun) {
    base = testRun;
    fileImport = new ImportAction(base);
  }

  /**
   * Enter e-mail address and confirm the API-key request
   * @param email
   */
  public void requestApiKey(final String email) {
    base.inputSetValue(TestIDScenario.CONNECT_GENERATE_TEXT, email);
    base.buttonClick(TestIDScenario.CONNECT_GENERATE_BUTTON);
  }

  /**
   * Enter API-key and confirm the authentication attempt
   * @param apiKey
   */
  public void authenticateApiKey(final String apiKey) {
    base.waitForElementVisible(base.id(TestIDScenario.CONNECT_LOGIN_TEXT)).sendKeys(apiKey);
    base.buttonClick(TestIDScenario.CONNECT_LOGIN_BUTTON);
  }

  /**
   * Assert that the overview page is shown based on presence of a new scenario button
   * @throws InterruptedException
   */
  public void checkOverviewPageShown() {
    base.waitForElementVisible(base.id(TestIDScenario.CONNECT_OVERVIEW_NEXT));
  }

  /**
   * Ensure that the expected Connect job status is shown in the overview for the given job id.
   * @param jobId the location of the row to test
   * @param jobStatus the expected status text
   */
  public void checkOverviewContainsJobStatus(final String jobId, final String jobStatus) {
    final String xpathRow = "//div[@id='gwt-debug-connect-overview-table-0']";
    final String xpathJobId = "/div[contains(text(),'" + jobId + "')]";
    final String xpathStatus = "/div[contains(text(),'" + jobStatus + "')]";

    // Check the top row in the overview for expected JobId and Status
    base.waitForElementVisible(base.xpath(xpathRow + xpathJobId), base.getSettingPageLoadTimeout());
    base.waitForElementVisible(base.xpath(xpathRow + xpathStatus), base.getSettingPageLoadTimeout());
  }

  /**
   * Ensure that the expected Connect job status is shown in the overview for the given job id.
   * Checks for one or more expected statusses to prevent flakey test performance 
   * @param jobId the location of the row to test
   * @param jobStatus the expected status text
   */
  public void checkOverviewContainsJobStatus(final String jobId, final List<String> jobStatus) {
    final String xpathRow = "//div[@id='gwt-debug-connect-overview-table-0']";
    final String xpathJobId = "/div[contains(text(),'" + jobId + "')]";

    base.waitForElementVisible(base.xpath(xpathRow + xpathJobId), base.getSettingPageLoadTimeout());
    final String actualStatus = base.waitForElementList(base.id(TestIDScenario.CONNECT_OVERVIEW_TABLE_INFO)).get(0).getText();
    Assert.assertTrue("Expected status '" + jobStatus + "' but actual status is: " + actualStatus, jobStatus.contains(actualStatus));
  }

  /**
   * Click on the new scenario button to open the scenario setup page
   * @throws InterruptedException
   */
  public void openNewScenarioPage() {
    base.buttonClick(TestIDScenario.CONNECT_OVERVIEW_NEXT);
  }

  /**
   * Check if setup page is shown by checking presence of required elements
   */
  public void checkSetupPageShown() {
    base.waitForElementVisible(base.id(TestIDScenario.CONNECT_CONFIGURE_IMPORT));
    base.waitForElementVisible(base.id(TestIDScenario.CONNECT_CONFIGURE_CANCEL));
    base.waitForElementVisible(base.id(TestIDScenario.CONNECT_CONFIGURE_CALCULATE));
  }

  /**
   * Opens file import dialog selects file and confirms import
   * @param fileName
   */
  public void importScenarioFile(final String fileName) {
    base.buttonClick(TestIDScenario.CONNECT_CONFIGURE_IMPORT);
    base.browserWaitForDrawAnimation();
    fileImport.importFile(fileName, ImportType.determineByFilename(fileName));
  }

  /**
   * Check if file is present in the list
   * @param fileName
   * @param expected
   */
  public void checkFileInList(final String fileName, final Boolean expected) {
    final String elementId = base.idPrefix(TestIDScenario.CALCULATION_DETAIL_TABLE_FILENAME);
    final String xpath = "//div[@id='" + elementId + "'][contains(.,'" + fileName + "')]";
    if (expected) {
      base.waitForElementVisible(base.xpath(xpath));
    } else {
      base.waitForElementInvisible(base.xpath(xpath));
    }
  }

  /**
   * Check if the file list is empty
   */
  public void checkFileListEmpty() {
    final String xpath = "//div[contains(@id,'" + TestIDScenario.CALCULATION_DETAIL_TABLE_FILENAME + "')]";
    final List<WebElement> fileList = base.getElementListByXPath(xpath);
    assertEquals("Files found while expecting none", 0, fileList.size());
  }

  /**
   * Check if the specified column is shown in the list
   * @param columnName
   */
  public void checkTableColumnShown(final String columnName) {
    final String xpath = "//div[contains(.,'" + columnName + "')]";
    assertNotNull("Column not shown: " + columnName, base.getElementByXPath(xpath));
  }

  /**
   * Check if the specified column is hidden
   * @param columnName
   */
  public void checkTableColumnHidden(final String columnName) {
    final String xpath = "//div[contains(.,'" + columnName + "')]";
    assertNull("Column not hidden: " + columnName, base.getElementByXPath(xpath));
  }

  /**
   * Delete the specified file from the imported scenario files
   * @param fileName
   */
  public void deleteScenarioFile(final String fileName) {
    // Locate and capture the table row containing filename
    final String xpathTableRow = "//div[contains(.,'" + fileName + "')]";
    final WebElement fileTableRow = base.buttonClick(base.waitForElementVisible(base.xpath(xpathTableRow)));
    base.browserWait(Duration.ofSeconds(1));

    // Target the delete button in this row and click it
    final String xpathButtonDelete = "//button[contains(@class,'buttonDeleteSmall')]";
    final WebElement buttonDelete = fileTableRow.findElement(By.xpath(xpathButtonDelete));

    assertNotNull("Button not present: " + xpathButtonDelete, buttonDelete);
    base.buttonClick(buttonDelete);
  }

  /**
   * Check whether the calculate button is enabled and fails if it is
   */
  public void checkCalculateDisabled() {
    final String xpath = "//button[@id='" + base.idPrefix(TestIDScenario.CONNECT_CONFIGURE_CALCULATE) + "' and @disabled]";
    base.waitForElementVisible(base.xpath(xpath));
  }

  public void setCalculationJobId(final String jobId) {
    openCalculationOptionsPanel();
    base.inputSetValue(base.id(TestIDScenario.CALCULATION_CONFIGURATION_NAME), jobId);
  }

  /**
   * Set the calculation options for optionName to optionValue
   * @param optionName
   * @param optionValue
   */
  public void setCalculationOption(final String optionName, final String optionValue) {
    // TODO: Fix the stepdefs/method when it is clear what options will be available.
    // Addded an if statement to not trigger the methode when called for by a removed calculation option.
    if (!optionName.contains("priority")) {
      // Expand the calculation options panel
      openCalculationOptionsPanel();
      // Locate optionElement and make sure it's not null
      final WebElement optionElement = getCalculationOptionWebElement(optionName);
      assertNotNull("Failed to locate option listbox: " + optionName, optionElement);

      // Set the option only if its not already set
      final WebElement activeOption = base.getElementByXPath("//div[@title='" + optionValue + "']");
      if (activeOption != null) {
        LOG.info(String.format("Option '%1s' already set to '%2s'", optionName, optionValue));
      } else {
        base.buttonClick(optionElement);
        base.browserWait(Duration.ofSeconds(1));

        final WebElement optionItem = base.getElementByXPath("//div[@class = 'popupContent']/div/div[contains(.,'" + optionValue + "')]");
        assertNotNull("Listbox option not present: " + optionName, optionItem);

        base.buttonClick(optionItem);
      }
    }
  }

  /**
   * Enable the check box for option. Fails if not found or already checked
   * @param optionName
   * TODO: Obsolete calculation option method for @AER-1718.
   */
  public void setCalculationOptionEnabled(final String optionName) {
    openCalculationOptionsPanel();
    final WebElement optionElement = getCalculationOptionWebElement(optionName);
    assertNotNull("Calculation option check box not found: " + optionName, optionElement);
    if (optionElement.isSelected()) {
      fail("Calculation option check box already ticked: " + optionName);
    }
    optionElement.click();
  }

  /**
   * Looks for @WebElement with optionName and returns null if not present
   * @param optionName
   * @return @WebElement
   */
  private WebElement getCalculationOptionWebElement(final String optionName) {
    WebElement optionElement = null;

    switch (optionName) {
    case "form":
      optionElement = base.waitForElementVisible(base.id(TestIDScenario.CALCULATION_CONFIGURATION_FORM));
      break;
    case "type":
      optionElement = base.waitForElementVisible(base.id(TestIDScenario.CALCULATION_CONFIGURATION_TYPE));
      break;
    case "year":
      optionElement = base.waitForElementVisible(base.id(TestIDScenario.CALCULATION_CONFIGURATION_YEAR));
      break;
    case "priority permit":
      optionElement = base.waitForElementVisible(base.id(TestIDScenario.CALCULATION_CONFIGURATION_PRIORITY_PERMIT));
      break;
    case "priority permit radius":
      optionElement = base.waitForElementVisible(base.id(TestIDScenario.CALCULATION_CONFIGURATION_PRIORITY_PERMIT_RADIUS));
      break;
    case "temporary project":
      optionElement = base.waitForElementVisible(base.id(TestIDScenario.CALCULATION_CONFIGURATION_TEMPORARY_PROJECT));
      break;
    case "temporary project years":
      optionElement = base.waitForElementVisible(base.id(TestIDScenario.CALCULATION_CONFIGURATION_TEMPORARY_PROJECT_YEARS));
      break;
    default:
      fail("Unrecognized option: " + optionName);
      break;
    }
    return optionElement;
  }

  /**
   * Check if the specified optionName is set to optionValue
   * @param optionName
   * @param optionValue
   * @throws InterruptedException
   */
  public void checkCalculationOption(final String optionName, final String optionValue) {
    openCalculationOptionsPanel();
    final WebElement activeOption = base.getElementByXPath("//div[@title='" + optionValue + "']");
    assertNotNull(String.format("Option '%1s' not set to '%2s'", optionName, optionValue), activeOption);
  }

  /**
   * Mark imported scenario file as research area by checking the check box
   * @param rowNumber
   */
  public void setResearchArea(final String rowNumber, final boolean checked) {
    // Target the row
    final String xpathRow = "//div[contains(@id,'gwt-debug-calculation-detail-table-" + rowNumber + "')]";
    final WebElement row = base.getElementByXPath(xpathRow);
    assertNotNull("Row not found: " + rowNumber, rowNumber);

    // In this row target researchArea check box label to send a click event
    final String xpathCheckBox = ".//span[contains(@class, 'gwt-CheckBox') and contains(@class, 'researchArea')]/label";
    final WebElement checkBox = row.findElement(ByXPath.xpath(xpathCheckBox));
    assertNotNull("Checkbox not found: " + xpathCheckBox, checkBox);

    if (checked == checkBox.isSelected()) {
      fail("Checkbox already in the desired state, check your logic...");
    } else {
      LOG.info("Toggling checkbox state...");
      checkBox.click();
    }
    base.browserWait(Duration.ofSeconds(1));
  }

  /**
   * Opens the Calculation options panel if it is closed
   */
  public void openCalculationOptionsPanel() {
    if (!isCalculationOptionsPanelOpen()) {
      clickCalculationOptionsPanel();
    }
  }

  public void clickCalculationOptionsPanel() {
    base.buttonClick(getCalculationOptionsPanel());
    base.browserWait(Duration.ofSeconds(1)); 
  }

  public WebElement getCalculationOptionsPanel() {
    return base.getElementByXPath("//div[contains(@id, 'Rekenopties')]");
  }

  /**
   * A check that returns a boolean, true when the Calculation options panel is op
   * false when the panel is closed
   */
  public boolean isCalculationOptionsPanelOpen() {
    boolean calculationOptionsVisible = getCalculationOptionsPanel().getAttribute("checked") != null;
    return calculationOptionsVisible;
  }

  /**
   * A check to assert if the Calculation option panel is open when the page is loaded
   */
  public void checkIfCalculationOptionsPanelIsOpen () {
    assertTrue("Expected Calculation options panel to be open, but was closed", isCalculationOptionsPanelOpen());
  }

  public void checkTemporaryStartYear(final String startYear) {
    openCalculationOptionsPanel();
    final String xpath = "//div[.='" + startYear + "']";
    base.waitForElementVisible(base.xpath(xpath), Duration.ofSeconds(3));
    final List<WebElement> startYearElements = base.getElementListByXPath(xpath);
    assertNotNull("Temporary project start year not found: " + startYear, startYearElements);
    // There should only be three elements found at this point, fail otherwise
    final int countElements = startYearElements.size();
    assertTrue("Expected three elements but found: " + countElements, countElements == 3);
    // The third element should contain the expected startYear, fail otherwise
    assertEquals("Temporary project start year differs from calculation year", startYear, startYearElements.get(2).getText());
  }

  public void checkOverviewFilterStatus(final String state) {
    final String xpath = "//span[@id='gwt-debug-connect-overview-toggle-status']/span";
    final String expectedLabel = "enabled".equals(state) ? "aan" : "uit";
    Assert.assertTrue(expectedLabel.equals(base.waitForElementVisible(base.xpath(xpath)).getText()));
  }

  public void checkOverviewContainsActiveJobsOnly() {
    final String xpath = "//div[@id='gwt-debug-connect-overview-table-info']";
    // Check each element for absence of completed jobs using text 'Inlezen' 
    for (final WebElement element : base.waitForElementList(base.xpath(xpath))) {
      Assert.assertFalse(element.getText().contains("Inlezen"));
    }
  }

  public void checkOverviewContainsAllJobs() {
    final String xpath = "//div[@id='gwt-debug-connect-overview-table-info']";
    // Assert success by comparing element counts before and after filter is enabled 
    final int countBefore = base.waitForElementList(base.xpath(xpath)).size();
    base.buttonClick(TestIDScenario.CONNECT_OVERVIEW_TOGGLE_STATUS + "-label");
    base.browserWait(Duration.ofSeconds(1));
    final int countAfter = base.waitForElementList(base.xpath(xpath)).size();
    Assert.assertNotEquals(countAfter, countBefore);
  }

}
