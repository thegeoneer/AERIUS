/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.stepdef;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDAdmin;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;

public class UserManagementStepDef {

  private final FunctionalTestBase base;

  public UserManagementStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
  }

  @When("^the add new user form is opened$")
  public void add_new_user_form_is_opened() {
    base.buttonClick(TestIDAdmin.USER_MANAGEMENT_NEW_USER);
    base.browserWaitForDrawAnimation();
  }

  @When("^the new user form field '(.+)' is set to '(.+)'$")
  public void user_field_is_set_to(final String fieldName, final String fieldValue) {
    // Build a proper element ID including dynamic part from the cucumber script
    String elementId = TestIDAdmin.USER_MANAGEMENT_ADD_EDITOR + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT + "_" + fieldName;
    final WebElement element = base.waitForElementVisible(base.id(elementId));
    // Some select box items for user roles requires a specific approach
    if ("roles".equals(fieldName)) {
      base.buttonClick(element);
      base.browserWaitForDrawAnimation();
      String roleId = elementId + "-" + fieldValue;
      base.buttonClick(roleId);
      base.buttonClick(element);
      base.browserWaitForDrawAnimation();
    } else if ("organisation".equals(fieldName)) {
      base.buttonClick(element);
      base.inputSelectListItemByText(base.id(elementId), fieldValue);
      base.buttonClick(element);
    } else {
      base.inputSetValue(element, fieldValue, true, false);
    }
  }

  @When("^the new user is '(.+)'$")
  public void user_is_enabled(final String activeStatus) {
    final String elementId = TestIDAdmin.USER_MANAGEMENT_ADD_EDITOR + "-" + TestIDAdmin.USER_MANAGEMENT_EDIT_ENABLED + "-input";
    final WebElement checkbox = base.getDriver().findElement(base.id(elementId));
    
    if ((checkbox.isSelected() && "disabled".equals(activeStatus)) || (!checkbox.isSelected() && "enabled".equals(activeStatus))) {
      // Clicks are blocked by the label element so a space is send here to toggle the check box  
      checkbox.sendKeys(Keys.SPACE);
    }
  }

  @When("^the new user form is submitted$")
  public void new_user_form_is_submitted() {
    base.buttonClick(TestID.BUTTON_DIALOG_CONFIRM);
    base.waitForElementInvisible(base.xpath("//div[contains(@class, 'gwt-PopupPanelGlass')]"));
  }

  @When("^the user '(.+)' is deleted$")
  public void the_user_is_deleted(final String username) {
    // XPath string to find a specific element in the user management div table
    final String xFinder = "(//*[contains(text(), '" + username + "')] | //*[@value='" + username + "'])";
    base.browserWait(Duration.ofSeconds(1));
    base.getElementByXPath(xFinder).click();
    base.buttonClick(TestIDAdmin.USER_MANAGEMENT_EDIT_DELETE);
    base.browserWait(Duration.ofSeconds(1));
    base.buttonClick(TestID.BUTTON_DIALOG_CONFIRM);
    base.browserWait(Duration.ofSeconds(1));
  }

  @When("^the user profile is opened")
  public void the_user_profile_is_opened() {
    base.buttonClick(TestIDRegister.BUTTON_PROFILE);
  }

  @Then("^the user profile contains '(.+)'")
  public void the_user_profile_contains(final String expectedText) {
    final String xpath = "//div[text()='" + expectedText.trim() + "']";
    final String actualText = base.waitForElementVisible(base.xpath(xpath)).getText();
    Assert.assertEquals(expectedText, actualText);
  }

  @Then("^the user profile is closed$")
  public void the_user_profile_is_closed() {
    base.buttonClick(TestIDRegister.BUTTON_PANEL_OK);
  }

}
