/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.scenario.stepdef;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation.UtilType;
import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.ImportAction;


public class ScenarioUtilsStepDef {

  private final FunctionalTestBase base;
  private final ImportAction fileImport;


  public ScenarioUtilsStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    fileImport = new ImportAction(base);
  }

  // Available options conform UtilType enum (VALIDATE, CONVERT, DELTA_VALUE, HIGHEST_VALUE, TOTAL_VALUE) 
  @When("^the utils option '(.+)' is selected$")
  public void when_select_utils_option(final UtilType utilType) {
    base.buttonClick(TestIDScenario.CONNECT_UTIL_SELECT + utilType);
  }

  @When("^the user continues to next step$")
  public void when_next_step() {
    base.buttonClick(TestIDScenario.CONNECT_UTIL_NEXT);
  }

  @When("^the user returns to previous step$")
  public void when_previous_step() {
    base.buttonClick(TestIDScenario.CONNECT_UTIL_CANCEL);
  }

  @When("^the user deletes file '(.+)' from the overview$")
  public void when_the_user_deletes_a_file(final String fileName) {
    base.mouseMoveToElement(base.buttonClick(base.xpath("//div[contains(text(),'" + fileName + "')]")));
    base.buttonClick(base.xpath("//button[contains(@class,'deleteButton') and @style='visibility: visible;']"));
  }

  @When("^the user toggles validate as PP$")
  public void when_toggle_validate_pp() {
    base.buttonClick(TestIDScenario.CONNECT_UTIL_VALIDATE_AS_PP_CHECKBOX);
  }

  @When("^the user toggles only increases$")
  public void when_toggle_only_increases() {
    base.buttonClick(TestIDScenario.CONNECT_UTIL_ONLYINCREASE_CHECKBOX);
  }

  @When("^the file import window is opened$")
  public void when_file_import_window_opened() {
    base.buttonClick(TestIDScenario.CONNECT_UTIL_IMPORT);
    base.browserWaitForDrawAnimation();
  }

  @When("^the file '(.+)' is selected$")
  public void when_import_file_selected(final String fileName) {
    fileImport.importSelectFile(fileName, ImportType.determineByFilename(fileName));
  }

  @When("^the file '(.+)' is imported$")
  public void when_import_file_imported(final String fileName) {
    when_file_import_window_opened();
    when_import_file_selected(fileName);
    when_import_confirmed();
  }

  @When("^the file import is confirmed$")
  public void when_import_confirmed() {
    fileImport.importConfirm(true);
    base.browserWaitForDrawAnimation();
  }

  @When("^the file import is cancelled$")
  public void when_import_cancelled() {
    fileImport.importCancel(true);
    base.browserWaitForDrawAnimation();
  }

  @Then("^the file '(.+)' is shown in the overview$")
  public void then_file_shown_in_overview(final String expectedFileName) {
    for (final WebElement e : base.waitForElementList(base.id(TestIDScenario.CALCULATION_DETAIL_TABLE_FILENAME))) {
      if (expectedFileName.equals(e.getText())) {
        return;
      }
    }
    Assert.fail("Expected file '" + expectedFileName + "' not found in overview!");
  }

  @Then("^the success label contains the message '(.+)'$")
  public void success_label_contains_message(final String expectedMessage) {
    base.browserWaitForPageLoaded(SeleniumDriverWrapper.getInstance().getSettingSyncTimeout());
    final String actualMessage = base.waitForElementVisible(base.id(TestIDScenario.CONNECT_UTIL_SUCCESS_LABEL)).getText();
    Assert.assertEquals(expectedMessage, actualMessage);
  }

  @Then("^the warning label contains the message '(.+)'$")
  public void warning_label_contains_message(final String expectedMessage) {
    final String actualMessage = base.waitForElementVisible(base.id(TestIDScenario.CONNECT_UTIL_WARNING_LABEL)).getText();
    Assert.assertEquals(expectedMessage.replace("\"", "''"), actualMessage);
  }

  @Then("^the error label contains the message '(.+)'$")
  public void error_label_contains_message(final String expectedMessage) {
    final String actualMessage = base.waitForElementVisible(base.id(TestIDScenario.CONNECT_UTIL_ERROR_LABEL)).getText();
    // assertEquals inviable here due to certain error messages containing uniquely generated references
    Assert.assertTrue(actualMessage.contains(expectedMessage.replace("\"", "''")));
  }

  @Then("^the util message label contains the message '(.+)'$")
  public void util_message_label_contains_message(final String expectedMessage) {
    final String actualMessage = base.waitForElementVisible(base.id(TestIDScenario.CONNECT_UTIL_VALIDATE_LABEL)).getText();
    Assert.assertEquals(expectedMessage, actualMessage);
  }

  @Then("^the success label is empty$")
  public void success_label_empty() {
    Assert.assertTrue(base.waitForElementVisible(base.id(TestIDScenario.CONNECT_UTIL_SUCCESS_LABEL)).getText().isEmpty());
  }

  @Then("^the warning label is empty$")
  public void warning_label_empty() {
    Assert.assertTrue(base.waitForElementVisible(base.id(TestIDScenario.CONNECT_UTIL_WARNING_LABEL)).getText().isEmpty());
  }

  @Then("^the error label is empty$")
  public void error_label_empty() {
    Assert.assertTrue(base.waitForElementVisible(base.id(TestIDScenario.CONNECT_UTIL_ERROR_LABEL)).getText().isEmpty());
  }

  @Then("^the util message label is empty$")
  public void util_message_label_empty() {
    Assert.assertTrue(base.waitForElementVisible(base.id(TestIDScenario.CONNECT_UTIL_VALIDATE_LABEL)).getText().isEmpty());
  }

  @Then("^the user validates file '(.+)'$")
  public void user_validates_file(final String fileName) {
    when_select_utils_option(UtilType.VALIDATE);
    when_next_step();
    when_import_file_imported(fileName);
    then_file_shown_in_overview(fileName);
  }

  @Then("^the user converts file '(.+)'$")
  public void user_converts_file(final String fileName) {
    when_select_utils_option(UtilType.CONVERT);
    when_next_step();
    when_import_file_imported(fileName);
    then_file_shown_in_overview(fileName);
  }


}
