/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.register.stepdef;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.register.action.ApplicationAction.ApplicationStatus;
import nl.overheid.aerius.wui.cucumber.shared.action.NotificationAction;

public class DossierInformationStepDef {
  private static final Logger LOG = LoggerFactory.getLogger(DossierInformationStepDef.class);

  private final FunctionalTestBase base;
  private final NotificationAction notifications;

  public DossierInformationStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    notifications = new NotificationAction(base);
  }

  @When("^the application is set to edit mode$")
  public void when_application_set_to_edit() {
    base.buttonClick(TestIDRegister.BUTTON_EDIT_APPLICATION);
  }

  @When("^the application dossier number is set to '(.+)'$")
  public void when_application_dossierid_set(final String dossierId) {
    base.inputSetValue(base.idPrefix(TestIDRegister.PERMIT_DOSSIER_ID_EDITOR), dossierId);
  }

  @When("^the application handler is set to username '(.+)'$")
  public void when_application_handler_set(final String handlerUserId) {
    base.inputSelectListItemByText(base.id(TestIDRegister.PERMIT_HANDLER_EDITOR), handlerUserId);
  }

  @When("^the application remarks is set to '(.+)'$")
  public void when_application_remarks_set(final String remarks) {
    base.inputSetValue(base.idPrefix(TestIDRegister.PERMIT_REMARKS_EDITOR), remarks);
  }

  @When("^the application is saved$")
  public void when_application_saved() {
    base.buttonClick(TestIDRegister.BUTTON_SAVE_APPLICATION);
    notifications.checkNotificationForErrors();
  }

  @When("^the application edit is canceled$")
  public void when_application_cancel_edit() {
    base.buttonClick(TestIDRegister.BUTTON_CANCEL_EDIT_APPLICATION);
    notifications.checkNotificationForErrors();
  }

  @When("^the application with id '(.+)' is removed$")
  public void when_application_remove(final String dossierId) {
    base.checkAndHandleUnexpectedNotifications();
    base.buttonClick(TestIDRegister.BUTTON_REMOVE_APPLICATION);
    base.browserAlertAccept();
    base.waitForElementInvisible(base.id(TestIDRegister.BUTTON_REMOVE_APPLICATION), base.getSettingPageLoadTimeout());
    // Check notification panel messages to determine success or failure
    notifications.checkForNotification("De aanvraag met zaaknummer '" + dossierId + "' is succesvol verwijderd", true);
    notifications.checkForNotification("is niet gevonden in de database.", false);
    // Checking for errors here saves clutter in the feature file
    notifications.checkNotificationForErrors();
  }

  @Then("^the application dossier number has the value '(.+)'$")
  public void when_application_dossierid_checked(final String dossierId) {
    base.inputCheckValue(base.idPrefix(TestIDRegister.APPLICATION_DOSSIER_ID_VIEWER), dossierId);
  }

  @Then("^the application dossier handler has the value '(.+)'$")
  public void when_application_handler_checked(final String handlerUserId) {
    base.inputCheckValue(base.idPrefix(TestIDRegister.APPLICATION_HANDLER_VIEWER), handlerUserId);
  }

  @Then("^the application dossier remarks has the value '(.+)'$")
  public void when_application_remarks_checked(final String remarks) {
    base.inputCheckValue(base.idPrefix(TestIDRegister.APPLICATION_REMARKS_VIEWER), remarks);
  }

  @Then("^the application corporation has the value '(.+)'$")
  public void when_application_corporation_checked(final String corporation) {
    base.inputCheckValue(base.idPrefix(TestIDRegister.APPLICATION_CORPORATION_VIEWER), corporation);
  }

  @Then("^the application project has the value '(.+)'$")
  public void when_application_project_checked(final String project) {
    base.inputCheckValue(base.idPrefix(TestIDRegister.APPLICATION_PROJECT_VIEWER), project);
  }

  @Then("^the application emission has the value '(.+)'$")
  public void when_application_emission_checked(final String emission) {
    base.inputCheckValue(base.idPrefix(TestIDRegister.APPLICATION_EMISSION_VALUE_VIEWER), emission
        + SeleniumDriverWrapper.getInstance().getRunParameterAsString(SeleniumDriverWrapper.UNITS_SOURCE_EMISSION));
  }

  @Then("^the application date has the value '(.+)'$")
  public void when_application_date_checked(final String startDate) {
    base.inputCheckValue(base.idPrefix(TestIDRegister.APPLICATION_DATE_VIEWER), startDate);
  }

  @Then("^the application enddate has the value '(.+)'$")
  public void when_application_enddate_checked(final String endDate) {
    base.inputCheckValue(base.idPrefix(TestIDRegister.APPLICATION_END_DATE_VIEWER), endDate);
  }

  @Then("^the application dossier status is '(.+)'$")
  public void then_application_dossier_status(final String dossierStatus) {
    base.buttonTextContains(TestIDRegister.APPLICATION_STATUS_BUTTON, dossierStatus);
  }

  @Then("^wait until the application dossier status is '(.+)'$")
  public void wait_until_the_application_status_is(final String dossierStatus) {
    final String buttonId = TestIDRegister.APPLICATION_STATUS_BUTTON;
    final int pollSeconds = 10;
    final int maxWaitMinutes = 5;
    final long maxWaitSeconds = TimeUnit.MINUTES.toSeconds(maxWaitMinutes);
    int totalWaitSeconds = 0;
    LOG.info("Checking for application status '{}' every {} seconds for {} minutes tops!", dossierStatus, pollSeconds, maxWaitMinutes);
    // Poll application status button until desired status or timeout
    while (!base.buttonTextContains(buttonId, dossierStatus)) {
      base.getDriver().navigate().refresh();
      base.browserWait(Duration.ofSeconds(pollSeconds));
      totalWaitSeconds += pollSeconds;
      if (totalWaitSeconds > maxWaitSeconds) {
        Assert.assertTrue("Timeout while waiting for application status change", totalWaitSeconds <= maxWaitSeconds);
      }
      LOG.debug("Status not available after waiting for {} seconds", totalWaitSeconds);

      // Making sure this step fails if status button is not found within 5 seconds
      Assert.assertTrue("Timeout while syncing to status button",
          base.waitForElementVisible(base.id(TestIDRegister.APPLICATION_STATUS_BUTTON), Duration.ofSeconds(5)).isDisplayed());
    }
  }

  @Then("^the application dossier status is set to '(.+)'$")
  public void then_application_dossier_status_is_set_to(final String dossierStatus) {
    // Open status selection dialog with JS click to prevent button being blocked by search suggestion or notification pop up
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.APPLICATION_STATUS_BUTTON)));

    // Determine debug id for the desired status button and grab a reference to the element
    final String elementId = TestIDRegister.STATUS_CHANGE_ITEM_PREFIX + ApplicationStatus.getUnsafe(dossierStatus);
    final WebElement webElement = base.waitForElementVisible(base.id(elementId));
    base.browserWaitForDrawAnimation();

    // Try/catch approach here in case of exceptions on the first click attempt
    // retry in case of stale element references
    try {
      base.buttonClick(webElement);
    } catch (StaleElementReferenceException e) {
      LOG.debug("Retrying after StaleElement on dossier status change: ({})", e);
      base.buttonClick(elementId);
    } catch (Exception e) {
      LOG.debug("Retrying after exception on dossier status change: ({})", e);
      base.buttonClick(elementId);
    }

    // Confirm new status and accept any alert for status rejected final (implicit deletion)
    base.buttonClick(TestIDRegister.STATUS_CHANGE_OK_BUTTON);
    base.browserAlertAccept();

    // Check notification panel messages to determine success or failure
    final Duration timeOutDuration = base.getSettingPageLoadTimeout().multipliedBy(3);
    if ("REJECTED_FINAL".equals(dossierStatus)) {
      // Changing status to REJECTED FINAL is confirmed with a deletion message
      notifications.waitForNotificationDone("is succesvol verwijderd", timeOutDuration.getSeconds());
    } else {
      // All other status changes are confirmed by a regular status change message
      notifications.waitForNotificationDone("Dit kan enkele minuten duren", timeOutDuration.getSeconds());
      notifications.waitForNotificationDone("is succesvol aangepast", timeOutDuration.getSeconds());
    }
  }

  @Then("^the application dossier status is changed to '(.+)'$")
  public void then_application_dossier_status_is_changed_to(final String dossierStatus) {
    base.buttonClick(TestIDRegister.APPLICATION_STATUS_BUTTON);
    base.browserWaitForDrawAnimation();
    base.buttonClick(TestIDRegister.STATUS_CHANGE_ITEM_PREFIX + ApplicationStatus.getUnsafe(dossierStatus));
  }

  @Then("^the application dossier status change is confirmed'$")
  public void then_application_dossier_status_change_is_confirmed() {
    base.buttonClick(TestIDRegister.STATUS_CHANGE_OK_BUTTON);
    base.browserAlertAccept();
  }

  @Then("^the application dossier status edit window is opened$")
  public void then_application_dossier_status_window_is_opened() {
    base.buttonClick(TestIDRegister.APPLICATION_STATUS_BUTTON);
  }

  @Then("^the application dossier status edit window is cancelled$")
  public void then_application_dossier_status_window_is_cancelled() {
    base.buttonClick(TestIDRegister.STATUS_CHANGE_CANCEL_BUTTON);
    base.browserAlertAccept();
  }

}
