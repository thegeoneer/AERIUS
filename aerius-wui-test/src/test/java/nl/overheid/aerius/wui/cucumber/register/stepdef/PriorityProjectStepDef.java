/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.register.stepdef;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.register.action.PriorityProjectAction;
import nl.overheid.aerius.wui.cucumber.shared.action.ImportAction;
import nl.overheid.aerius.wui.cucumber.shared.action.NotificationAction;

public class PriorityProjectStepDef {

  private final FunctionalTestBase base;
  private final PriorityProjectAction project;
  private final NotificationAction notification;

  public PriorityProjectStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    new ImportAction(base);
    notification = new NotificationAction(base);
    project = new PriorityProjectAction(base);
  }

  @Then("^the user can import '(.+)' with dossierID '(.+)' as a new priority project$")
  public void the_user_can_import(final String fileName, final String dossierId) {
    project.clickNewPriorityProjectButton();
    base.browserWaitForDrawAnimation();
    project.importPriorityProject(fileName, dossierId);
  }

  @Then("^the user can import '(.+)' with dossierID '(.+)' as a new priority project with errors$")
  public void the_priority_project_file_is_imported_with_errors(final String fileName, final String dossierID) {
    project.clickNewPriorityProjectButton();
    project.importProjectWithErrors(fileName, dossierID);
  }

  @Then("^the partial project file '(.+)' is imported$")
  public void the_partial_project_file_is_imported(final String fileName) {
    project.clickNewRequestButton();
    project.importPartialProject(fileName, "");
  }

  @Then("^the partial project file '(.+)' is imported with description '(.+)'$")
  public void the_partial_project_file_is_imported_with_description(final String fileName, final String description) {
    project.clickNewRequestButton();
    project.importPartialProject(fileName, description);
  }

  @Then("^the actualization project file '(.+)' is imported$")
  public void the_actualization_project_file_is_imported(final String fileName) {
    project.clickAddActualisationButton();
    project.importActualization(fileName);
    base.waitForElementInvisible(base.id(TestIDRegister.PRIORITYPROJECT_ACTUALIZATION_ADD_BUTTON));
  }

  @Then("^the user can import '(.+)' with dossierID '(.+)' as a new actualization project with errors$")
  public void the_actualization_project_file_with_errors_is_imported(final String fileName, final String dossierID) {
    project.clickAddActualizationProjectButton();
    project.importProjectWithErrors(fileName, dossierID);
  }

  @Then("^the actualization project file is replaced by '(.+)'$")
  public void the_actualization_project_file_is_replaced(final String fileName) {
    project.clickReplaceActualizationButton();
    project.importActualization(fileName);
    base.waitForElementInvisible(base.id(TestIDRegister.PRIORITYPROJECT_ACTUALIZATION_ADD_BUTTON));
  }

  @Then("^the actualization is deleted$")
  public void the_actualization_is_deleted() {
    project.clickDeleteActualizationButton();
  }

  @Then("^the history contains entry: '(.+)'$")
  public void the_history_contains_entry(final String expectedEntry) {
    project.checkProjectHistory(expectedEntry);
  }

  @Then("^the history contains '(.+)' identical entries with text: '(.+)'$")
  public void the_history_contains_identical_entries(final int expectedAmount, final String expectedEntry) {
    project.checkProjectHistoryOccurences(expectedEntry, expectedAmount);
  }

  @Then("^the priority project status button is '(.+)'$")
  public void the_status_button_is(final String expectedState) {
    project.buttonStatusStateis(expectedState);
  }

  @Then("^the priority project status is set to '(.+)'$")
  public void the_project_status_set_to(final String projectStatus) {
    project.buttonStatusSet(projectStatus);
  }

  @When("^the user opens priority project with dossierID '(.+)'$")
  public void the_user_opens_project_with_id(String projectId) {
    project.openProjectWithId(projectId);
  }

  @When("^the user opens priority sub project with dossierID '(.+)'$")
  public void the_user_opens_subproject_with_id(String projectId) {
    project.openSubProjectWithId(projectId);
  }

  @When("^the priority sub project can be opened by clicking the '(.+)' button$")
  public void the_priority_subproject_opened_by_button(final String navButton) {
    project.switchToTab(navButton);
  }

  @Then("^the user switches to the '(.+)' details page$")
  public void the_user_switches_to_detail_page(final String page) {
    project.switchToPage(page);
  }

  @Then("^the status change is confirmed by notification: '(.+)'$")
  public void the_user_waits_for_status_changed(final String message) {
    // Replace double for single quotes
    final String expectedMessage = message.replace("\"", "'");
    notification.waitForNotificationDone(expectedMessage, base.getSettingPageLoadTimeout().getSeconds());
  }

  @Then("^the user switches to the '(.+)' tab of the priority project$")
  public void the_user_switches_to_priority_tab(final String tab) {
    project.switchToTab(tab);
  }

  @Then("^the priority project navigation button '(.+)' is '(.+)'$")
  public void the_priority_project_button_disabled(final String navButton, final String expectedState) {
    project.buttonStateIs(navButton, expectedState);
  }

  @Then("^the field '(.+)' has value '(.+)'$")
  public void the_field_has_value(final String label, final String value) {
    project.checkFieldValue(label, value);
  }

  @Then("^the field '(.+)' is not present$")
  public void the_field_not_present(final String label) {
    project.checkFieldNotPresent(label);
  }

  @Then("^the priority project filter panel is toggled$")
  public void the_priority_filter_options_toggled() {
    project.togglePriorityFilterPanel();
  }

  @Then("^the priority project filter '(.+)' contains options$")
  public void the_priority_filter_option_contains(final String filter, final List<String> options) {
    project.priorityFilterOptionContains(filter, options);
  }

  @Then("^the fact sheet file '(.+)' is imported$")
  public void the_fact_sheet_is_imported(final String fileName) {
    project.clickAddFactSheetButton();
    project.importFactSheet(fileName);
    base.waitForElementVisible(base.xpath("//a[text()='" + fileName + "']"));
  }

  @Then("^the fact sheet file is replaced by '(.+)'$")
  public void the_fact_sheet_is_replaced(final String fileName) {
    project.clickReplaceFactSheetButton();
    project.importFactSheet(fileName);
    base.waitForElementVisible(base.xpath("//a[text()='" + fileName + "']"));
  }

  @Then("^the user is confronted with error message: '(.+)'$")
  public void the_user_confronted_with_error(final String expectedMessage) {
    final String actualMessage = base.waitForElementVisible(base.id(TestID.DIV_IMPORT_ERRORS)).getText();
    Assert.assertEquals(expectedMessage.replace("\"", "''"), actualMessage);
  }

  @Then("^the user cancels the file import$")
  public void the_user_cancels_import() {
    project.importCancel();
  }

  @Then("^the assessment explanation is toggled$")
  public void toggle_assessment_explanation() {
    final String xpath = "//div[@id='gwt-debug-" + TestID.BUTTON_COLLAPSEPANEL_PLUSMINUS + "-Toelichting symbolen']";
    base.buttonClick(By.xpath(xpath));
  }

  @Then("^the user switches to the Assessment tab of the priority sub project$")
  public void the_user_switches_to_priority_sub_tab() {
    project.switchToAssessmentTab();
  }

  @And("^contains Nature area '(.+)' with '(.+)' and '(.+)'$")
  public void contains_Nature_area_with_deposition_room(final String natureArea, final String devIcon, final String devRoomMessage) {
    project.checkNatureAreaValue(natureArea, devIcon, devRoomMessage);
  }
  
  @Then("^the priority project progress bar shows '(.+)' assigned$")
  public void project_progress_bar_percent_assigned(final String expectedPercentage) {
    project.checkProgressBarPercentage(expectedPercentage);
  }

  @Then("^the priority project progress bar title is '(.+)'$")
  public void project_progress_bar_title_is(final String expectedTitle) {
    project.checkProgressBarTitle(expectedTitle);
  }

  @Then("^the priority project indicative hexagon is '(.+)'$")
  public void project_indicative_hexagon_is(final String expectedIndication) {
    project.checkIndicativeHexagon(expectedIndication);
  }

  @Then("^the priority project '(.+)' progress indicator is '(.+)'$")
  public void project_progress_icon_is(final String projectName, final String expectedIndicator) {
    project.checkProgressIndicator(projectName, expectedIndicator);
  }

}
