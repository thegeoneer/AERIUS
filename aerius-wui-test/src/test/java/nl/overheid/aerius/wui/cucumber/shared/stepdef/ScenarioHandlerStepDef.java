/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.ScenarioHandler;
import nl.overheid.aerius.wui.cucumber.shared.ScenarioScreenshotEmbedder;
import nl.overheid.aerius.wui.cucumber.shared.ScenarioStopwatch;
import nl.overheid.aerius.wui.cucumber.shared.ScenarioVideoRecorder;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class ScenarioHandlerStepDef {

  private final FunctionalTestBase base;
  private final List<ScenarioHandler> handlers = new ArrayList<>();

  public ScenarioHandlerStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());

    // Add the handlers to use
    handlers.add(new ScenarioStopwatch());
    handlers.add(new ScenarioScreenshotEmbedder(base));
    handlers.add(new ScenarioVideoRecorder());
  }

  @Before
  public void beforeScenario(final Scenario result) {
    for (final ScenarioHandler handler : handlers) {
      handler.beforeScenario(result);
    }
  }

  @After
  public void afterScenario(final Scenario result) {
    for (final ScenarioHandler handler : handlers) {
      handler.afterScenario(result);
    }
  }

}
