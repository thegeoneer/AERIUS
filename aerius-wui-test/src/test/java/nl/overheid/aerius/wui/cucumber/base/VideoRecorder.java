/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.base;

import org.monte.media.FormatKeys;
import org.monte.media.VideoFormatKeys;
import org.monte.media.AudioFormatKeys;

import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.io.File;

/**
 * This class enables the capturing of video of selenium test execution
 * 
 * The screen recorder was originally authored by http://www.randelshofer.ch/monte/
 * and re-packed / mavenized by the kind folks over at http://pojosontheweb.com/
 * AVI output requires the TSCC codec from http://www.techsmith.com/download.html
 * 
 * Known Issue: Capturing audio is not working (yet) but we don't need it anyway 
 */
public final class VideoRecorder {

  private static final Logger LOG = LoggerFactory.getLogger(VideoRecorder.class);
  private static final int KEY_FRAME_MULTIPLIER = 60;

  private static VideoRecorder instance; 

  private final SeleniumDriverWrapper sdw = SeleniumDriverWrapper.getInstance(); 
  private final ScreenRecorder screenRecorder = getScreenRecorder();

  private VideoRecorder() {
    // Preventing rogue instantiation
  }

  public static VideoRecorder getInstance() {
    if (instance == null) {
      try {
        LOG.info("Creating new VideoRecorder instance");
        return new VideoRecorder();
      } catch (Exception e) {
        LOG.error("Exception occurred while creating new recorder instance: ", e);
      }
    }
    return instance;
  }

  /**
   * Start recording a new video
   */
  public void startRecording() {
    try {
      screenRecorder.start();
      LOG.info("VideoRecorder started recording.");
    } catch (IOException e) {
      LOG.error("Exception occurred on video recorder start: ", e);
    }
  }

  /**
   * Stop recording video and save the result file (default: ~/Videos/)
   * Print the result file path in the console on success  
   */
  public void stopRecording() {
    try {
      screenRecorder.stop();
      LOG.info("VideoRecorder stopped recording: {}", screenRecorder.getCreatedMovieFiles().toString());
    } catch (IOException e) {
      LOG.error("Exception occurred on video recorder stop: ", e);
    }
  }

  private GraphicsConfiguration getGraphicsDefaultConfig() {
    // log details for each available graphics device we could potentially use
    // in a docker container, or any other device running the selenium node
    for (GraphicsDevice gd : GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()) {
      LOG.debug("Found GraphicsDevice with ID: {}, type: {}, mode: {}, memory: {}, fullscreen support: {}",
          gd.getIDstring(),
          gd.getType(),
          gd.getDisplayMode(),
          gd.getAvailableAcceleratedMemory(),
          gd.isFullScreenSupported());
    }
    return GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
  }

  private ScreenRecorder getScreenRecorder() {
    LOG.info("Initializing ScreenRecorder with output location: {}", getVideoOutputPath());
    try {
      return new ScreenRecorder(
          getGraphicsDefaultConfig(),
          // Capture Area (null for full screen)
          null,
          // Output file MIME type Format
          getFormatMimeType(),
          // Screen Capture Format
          getFormatScreenCapture(),
          // Mouse Format (null for no mouse pointer)
          getFormatMouseCapture(),
          // Audio Format (null to capture without audio),
          getFormatAudioCapture(),
          // Path to write video files to
          getVideoOutputPath()
          );
    } catch (Exception e) {
       LOG.error("Exception caught while initializing VideoRecorder: ", e);
    }
    return null;
  }

  /**
   * @return preferred @Format for audio capture settings (null for no audio)
   */
  private Format getFormatAudioCapture() {
    if (sdw.getSettingAudioCapture()) {
      return new Format(
          FormatKeys.MediaTypeKey, MediaType.AUDIO,
          FormatKeys.EncodingKey, AudioFormatKeys.ENCODING_PCM_SIGNED,
          AudioFormatKeys.SampleRateKey, new Rational(sdw.getSettingAudioFrequency(), 1),
          AudioFormatKeys.SampleSizeInBitsKey, sdw.getSettingAudioBitDepth(),
          AudioFormatKeys.ChannelsKey, 1
          );
    } else {
      return null;
    }
  }

  /**
   * @return preferred @Format for mouse capture settings (null for no mouse)
   */
  private Format getFormatMouseCapture() {
    if (sdw.getSettingVideoMouse()) {
      return new Format(
          FormatKeys.MediaTypeKey, MediaType.VIDEO,
          FormatKeys.EncodingKey, ScreenRecorder.ENCODING_BLACK_CURSOR,
          FormatKeys.FrameRateKey, new Rational(sdw.getSettingVideoFrameRate(), 1)
          );
    } else {
      return null;
    }
  }

  /**
   * Set screen capture bit depth, frame rate and encoding
   * @return preferred @Format for screen capture settings
   */
  private Format getFormatScreenCapture() {
    return new Format(
        FormatKeys.MediaTypeKey, MediaType.VIDEO,
        FormatKeys.EncodingKey, VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
        VideoFormatKeys.CompressorNameKey, VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
        VideoFormatKeys.DepthKey, sdw.getSettingVideoBitDepth(),
        FormatKeys.FrameRateKey, Rational.valueOf(sdw.getSettingVideoFrameRate(), 1),
        VideoFormatKeys.QualityKey, 1.0f,
        FormatKeys.KeyFrameIntervalKey, sdw.getSettingVideoFrameRate() * KEY_FRAME_MULTIPLIER
        );
  }

  /**
   * Set video output MIME type for recording AVI files
   * @return preferred @Format for the video output file
   */
  private Format getFormatMimeType() {
    return new Format(
        FormatKeys.MediaTypeKey, MediaType.FILE,
        FormatKeys.MimeTypeKey, FormatKeys.MIME_AVI);
  }

  private File getVideoOutputPath() {
    return new File(sdw.getSettingVideoOutputPath());
  }

  public String getVideoFilePath() {
    return screenRecorder.getCreatedMovieFiles().toString();
  }
}

