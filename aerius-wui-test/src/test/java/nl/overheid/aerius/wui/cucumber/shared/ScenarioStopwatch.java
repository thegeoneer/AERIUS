/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared;

import org.apache.commons.lang3.time.StopWatch;

import cucumber.api.Scenario;

public class ScenarioStopwatch implements ScenarioHandler {

  final StopWatch stopwatch = new StopWatch();

  @Override
  public void beforeScenario(final Scenario result) {
    stopwatch.reset();
    stopwatch.start();
  }

  @Override
  public void afterScenario(final Scenario result) {
    stopwatch.stop();
    result.write("Scenario Duration: " + stopwatch.toString());
  }

}
