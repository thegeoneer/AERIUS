/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.CoordinateAttribute;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.LocationAction;

import cucumber.api.Transform;
import cucumber.api.Transformer;
import cucumber.api.java.en.When;

public class LocationStepDef {

  private final FunctionalTestBase base;
  private final LocationAction locationPage;

  public LocationStepDef() throws IOException {

    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    locationPage = new LocationAction(base);
  }

  @When("^the source location is set to a point with coordinates X (.+) and Y (.+)$")
  public void when_source_coordinates_are_set(final String xCoordinate, final String yCoordinate) {
    locationPage.setCoordinatesXY(xCoordinate, yCoordinate);
    locationPage.clickButtonNext();
  }

  @When("^the source location coordinate X is set to (.+)")
  public void when_source_coordinate_x_is_set(final String xCoordinate) {
    locationPage.setCoordinateX(xCoordinate);
  }

  @When("^the source location coordinate Y is set to (.+)")
  public void when_source_coordinate_y_is_set(final String yCoordinate) {
    locationPage.setCoordinateY(yCoordinate);
  }

  @When("^the source location is set to a point with random coordinates$")
  public void when_source_coordinates_are_set_random() {
    locationPage.setCoordinatesXY(base.getRandomCoordinate(CoordinateAttribute.x), base.getRandomCoordinate(CoordinateAttribute.y));
    locationPage.clickButtonNext();
  }

  @When("^the calculationpoint location is set to coordinates X (.+) and Y (.+)$")
  public void when_calcpoint_coordinates_are_set(final String xCoordinate, final String yCoordinate) {
    locationPage.setCoordinatesXY(xCoordinate, yCoordinate);
  }

  @When("^the source location is set to a '(.+)' with coordinates (.+)$")
  public void the_source_location_is_set_to_coordinates(@Transform(GeoTransformer.class) final TYPE geo, final String coordinates) {
    if (geo == TYPE.LINE) {
      locationPage.setLineSourceByCoordinates(coordinates);
      locationPage.clickButtonNext();
    }

    if (geo == TYPE.POLYGON) {
      locationPage.setPolygonSourceByCoordinates(coordinates);
      locationPage.clickButtonNext();
    }
  }

  @When("^the source location is entered as a '(.+)' with coordinates (.+)$")
  public void when_the_source_location_is_entered_as_geo_with_coordinates(@Transform(GeoTransformer.class) final TYPE geo, final String coordinates)
      throws Throwable {
    if (geo == TYPE.LINE) {
      locationPage.setLineSourceByCoordinates(coordinates);
    }

    if (geo == TYPE.POLYGON) {
      locationPage.setPolygonSourceByCoordinates(coordinates);
    }
  }

  @When("^the source location is saved$")
  public void when_the_source_location_is_saved() throws Throwable {
      locationPage.clickButtonNext();
  }

  @When("^the source location is set to edit mode$")
  public void when_the_source_location_editmode() throws Throwable {
      base.buttonClick(TestID.BUTTON_EDITSHAPESOURCE);
  }

  public static class GeoTransformer extends Transformer<WKTGeometry.TYPE> {
    @Override
    public TYPE transform(final String wkt) {
      return WKTGeometry.TYPE.getType(wkt.toUpperCase());
    }
  }

}
