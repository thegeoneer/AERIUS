/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.action;

import java.time.Duration;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for generating an export using the export dialog. 
 */
public class FilterAction {
  private static final Logger LOG = LoggerFactory.getLogger(FilterAction.class);

  private final FunctionalTestBase base;
  private final String xpathPopUp = "//div[contains(@class, 'popupContent')]";

  public FilterAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Toggles the animated filter panel between open and shut states. Times out after 5 seconds if no filter is found.
   */
  public void toggleFilterPanelCollapse() {
    final WebElement filterPanel = base.waitForElementVisible(base.id(TestIDRegister.DIV_FILTER_PANEL));
    final WebElement toggleCollapseButton = filterPanel.findElement(By.xpath(".//div[contains(@id, '"
        + TestID.BUTTON_COLLAPSEPANEL_PLUSMINUS + "')]"));
    base.buttonClick(toggleCollapseButton);
  }

  /**
   * Sets filter for application status
   * @param applicationStatus String application status option
   */
  public void setFilterApplicationStatus(final String applicationStatus) {
    base.buttonClick(TestIDRegister.INPUT_FILTER_STATUS);
    LOG.debug("Setting filter for APPLICATION STATUS");
    selectFilterOption(applicationStatus);
    base.buttonClick(TestIDRegister.INPUT_FILTER_STATUS);
    base.waitForElementInvisible(base.xpath(xpathPopUp));
  }

  /**
   * Show list of application status filter options to capture screenshots for the manual
   */
  public void showStatusFilterOptionsApplicationStatus() {
    base.buttonClick(TestIDRegister.INPUT_FILTER_STATUS);
    LOG.debug("Showing filter options for APPLICATION STATUS");
  }

  /**
   * Show list of application status filter options to capture screenshots for the manual
   */
  public void showStatusFilterOptionsApplicationSector() {
    base.buttonHover(TestID.DIV_FILTER_PANEL);
    final WebElement sector = base.getElementByXPath("//div[2][contains(text(),'Alle sectoren')]");
    base.buttonClick(sector); // INPUT_FILTER_STATUS);
    LOG.debug("Showing filter options for APPLICATION SECTOR");
  }

  /**
   * Sets filter for application location
   * @param applicationLocation String application location option
   */
  public void setFilterApplicationLocation(final String applicationLocation) {
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.INPUT_FILTER_LOCATION)));
    LOG.debug("Setting filter for APPLICATION LOCATION");
    selectFilterOption(applicationLocation);
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.INPUT_FILTER_LOCATION)));
    base.waitForElementInvisible(base.xpath(xpathPopUp));
  }

  /**
   * Sets filter for application authority
   * @param applicationAuthority String application authority option
   */
  public void setFilterApplicationAuthority(final String applicationAuthority) {
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.INPUT_FILTER_AUTHORITY)));
    LOG.debug("Setting filter for APPLICATION AUTHORITY");
    selectFilterOption(applicationAuthority);
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.INPUT_FILTER_AUTHORITY)));
    base.waitForElementInvisible(base.xpath(xpathPopUp));
  }

  /**
   * Sets filter for application area
   * @param applicationArea String application area option
   */
  public void setFilterApplicationArea(final String applicationArea) {
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.INPUT_FILTER_AREA)));
    LOG.debug("Setting filter for APPLICATION AREA");
    selectFilterOption(applicationArea);
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.INPUT_FILTER_AREA)));
    base.waitForElementInvisible(base.xpath(xpathPopUp));
  }

  /**
   * Sets filter for application sector
   * @param applicationSector String application sector option
   */
  public void setFilterApplicationSector(final String applicationSector) {
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.INPUT_FILTER_SECTOR)));
    LOG.debug("Setting filter for APPLICATION SECTOR");
    selectFilterOption(applicationSector);
    base.buttonClickJs(base.waitForElementVisible(base.id(TestIDRegister.INPUT_FILTER_SECTOR)));
    base.waitForElementInvisible(base.xpath(xpathPopUp));
  }

  /**
   * Selects an option from the filter options pop up
   * @param filterOption String option to select (visible text)
   */
  private void selectFilterOption(final String filterOption) {
    final String popupContent = "//div[contains(@class, 'popupContent')]";
    boolean optionSelected = false;
    base.waitForElementVisible(base.xpath(popupContent), Duration.ofSeconds(5));
    final WebElement filterPopUp = base.getElementByXPath(popupContent);
    final List<WebElement> filterOptions = filterPopUp.findElements(By.xpath(".//div"));
    for (final WebElement option : filterOptions) {
      LOG.info("Found filter option: '{}'", option.getText().replace("\n", ", "));
      if (filterOption.equals(option.getText().trim())) {
        base.buttonClick(option);
        optionSelected = true;
        LOG.info("Filter option selected:'" + filterOption + "'");
        break;
      }
    }
    if (!optionSelected) {
      Assert.fail("Filter option not found: " + filterOption);
    }
  }

  public void setFilterApplicationFromDate(final String applicationFromDate) {
    base.inputSetValue(TestIDRegister.INPUT_FILTER_FROM, applicationFromDate);
    base.browserWaitForDrawAnimation();
  }

  public void setFilterApplicationTillDate(String applicationTillDate) {
    base.inputSetValue(TestIDRegister.INPUT_FILTER_TILL, applicationTillDate);
    base.browserWaitForDrawAnimation();
  }


}
