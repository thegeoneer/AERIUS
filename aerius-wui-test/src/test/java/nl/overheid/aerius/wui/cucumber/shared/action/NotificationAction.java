/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;

/**
 * This class contains all the methods that apply for handling notifications in the notification-center.
 */
public class NotificationAction {
  private static final Logger LOG = LoggerFactory.getLogger(NotificationAction.class);

  private final FunctionalTestBase base;
  private final SeleniumDriverWrapper sdw;

  public NotificationAction(final FunctionalTestBase testRun) {
    base = testRun;
    sdw = SeleniumDriverWrapper.getInstance();
  }

  /**
   * Check for common errors in the notification panel and fails test if found 
   */
  public void checkNotificationForErrors() {
    checkNotificationsForText("fout;Fout;Exception;exception;error;Error;storing;niet succesvol", false, true);
  }

  /**
   * Checks the notification list on given string.
   * @param notificationText string to check for
   */
  public void checkForNotification(final String notificationText) {
    checkNotificationsForText(notificationText, true, true);
  }

  /**
   * Checks the notification list on given string.
   * @param notificationText string to check for
   * @param expectedResult true if text is expected to be found, false if not.
   */
  public void checkForNotification(final String notificationText, final boolean expectedResult) {
    checkNotificationsForText(notificationText, expectedResult, true);
  }

  /**
   * Checks the notifications for the given text, returns true if found.
   * @param text string to search for. use ";" to enter and separate multiple search strings
   * @param expected true if text is expected to be found, false if not.
   * @return true when text is found within the notifications
   */
  public boolean checkNotificationsForText(final String text, final boolean expected, final boolean failOnError) {
    boolean textFound = false;
    final String[] searchStringCollection = text.split(";");
    LOG.info("Checking notifications for text: '{}'", text);

    // store all found notifications to log in case of failure
    final StringBuilder foundNotifications = new StringBuilder("These are all the notifications found so far:\n");

    // check each notification found for the specified text
    for (final String notification : getAllNotifications()) {
      foundNotifications.append(" -> " + notification.replace("\n", " ") + "\n");
      for (final String searchString : searchStringCollection) {
        if (notification.contains(searchString)) {
          LOG.info("Notification match found for: '{}' in: '{}'", searchString, notification.replace("\n", " "));
          textFound = true;
          break;
        }
      }
    }

    if (failOnError && textFound != expected) {
      Assert.fail("No notification match found for text: '" + text + "' " + foundNotifications.toString());
    } else if (!failOnError && textFound == expected) {
      LOG.info("Expected error notification was found. {}", foundNotifications.toString());
    }
    return textFound;
  }

  /**
   * Gets all notifications from the notificationsList and returns an array.
   * @return a List<String> of notifications from notification list
   */
  private List<String> getAllNotifications() {
    open();
    if (getNotificationItems().size() > 0) {
      LOG.trace("Notification panel is open containing one or more items");
    } else {
      LOG.trace("Notification panel is open and empty");
    }

    // Compose an array of all messages found to log 
    final List<String> returnItems = new ArrayList<>();

    try {
      for (final WebElement notification : getNotificationItems()) {
        returnItems.add(notification.getText());
      }
    } catch (final Exception e) {
      LOG.trace("Exception caught while retrieving notifications: ", e);
    }
    return returnItems;
  }

  /**
   * Open the notification panel if it's not shown already. The notification panel can also open/close on it's
   * own and thus interfere with our intended flow of events. Extra steps in this method are there to ensure 
   * the notification panel is actually open to prevent occasional failures
   */
  public void open() {
    // If present close and reopen the panel
    if (isNotificationPanelPresent()) {
      LOG.trace("Notifications popup panel already opened. Closing and reopening to keep it open!");
      buttonClickNotificationToggle();
      buttonClickNotificationToggle();
      // If panel is not present at this point the panel opened/closed autonomously and we need another toggle
      if (!isNotificationPanelPresent()) {
      LOG.debug("Notification popup panel still absent due to interference. Reopening for good measure");
        buttonClickNotificationToggle();
      }
    } else {
      // If not present open the panel
      buttonClickNotificationToggle();
      // If panel is not present at this point the panel opened/closed autonomously and we need another toggle
      if (!isNotificationPanelPresent()) {
        buttonClickNotificationToggle();
      }
    }
  }

  /**
   * Close the notification panel if it's shown
   */
  public void close() {
    // If present close the panel
    if (isNotificationPanelPresent()) {
      LOG.debug("Closing notification popup panel");
      buttonClickNotificationToggle();
    } else {
      LOG.debug("Notification popup panel already closed");
    }
  }

  /**
   * Deletes all notifications from list by ensuring the notification
   * panel is opened and clicking the "delete all"-button.
   */
  public void deleteAllNotifications() {
    LOG.debug("Deleting all notifications");
    open();
    base.buttonClick(TestID.BUTTON_DELETE_ALL_NOTIFICATIONS);
  }

  /**
   * Waits for specific notification text to indicate that an action has
   * completed within the allowed timeout period. Test fails when a timeout
   * occurs and text was not found or when an error message is detected.
   * 
   * @param notifyText String expected notification text
   * @param timeOutSeconds int seconds
   *
   * Precondition: Action has been started.
   * Postcondition: Action has ended without errors.
   */
  public void waitForNotificationDone(final String notifyText, final long timeOutSeconds) {
    // TODO: replace this function with something more self explanatory and maintainable
    final int pollSeconds = (int) sdw.getSettingSyncTimeout().getSeconds();
    // Pause a second to allow for initial messages to appear.
    base.browserWait(Duration.ofSeconds(1));
    if (!checkNotificationsForText("De berekening wordt niet gestart voor project", true, false)) {
      LOG.info("Waiting {} seconds for notification text: '{}'. Checking once every {} seconds", timeOutSeconds, notifyText, pollSeconds);
      final long amountOfLoopsNeeded = timeOutSeconds / pollSeconds;
      for (int i = 0; i <= amountOfLoopsNeeded; i++) {
        checkNotificationsForText("fout;Fout;error;Error;storing", false, true);
        // Make sure we fail for the right reason
        if (i == amountOfLoopsNeeded) {
          Assert.fail("Expected notification failed to appear in " + timeOutSeconds + " seconds.");
        }
        if (checkNotificationsForText(notifyText, true, false)) {
          LOG.info("Found notification text: '{}'.", notifyText);
          break;
        } else {
          LOG.info("Notification text not found on search attempt {} of {}.", i, amountOfLoopsNeeded);
        }
        base.browserWait(Duration.ofSeconds(1));
      }
      base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
    }
  }

  public void waitForNotificationMessage(final String expectedMessage) {
    final int pollSeconds = (int) sdw.getSettingSyncTimeout().getSeconds();
    final int timeOutSeconds = sdw.getSettingNotificationTimeout();
    final int amountOfLoopsNeeded = timeOutSeconds / pollSeconds;
    LOG.info("Waiting {} seconds for notification text: '{}'.\nChecking once every {} seconds", timeOutSeconds, expectedMessage, pollSeconds);
    for (int i = 0; i <= amountOfLoopsNeeded; i++) {
      if (i == amountOfLoopsNeeded) {
        Assert.fail("Expected notification failed to appear in " + timeOutSeconds + " seconds.");
      }
      if (checkNotificationsForText(expectedMessage, true, false)) {
        LOG.info("Found notification text on attempt {} of {}.", i, amountOfLoopsNeeded);
        break;
      } else {
        LOG.info("Notification text not found on search attempt {} of {}.", i, amountOfLoopsNeeded);
      }
      base.browserWait(Duration.ofSeconds(pollSeconds));
    }
  }

  public void checkForValidationErrors() {
    base.getWaitDefault().until(new Function<WebDriver, Boolean>() {
      public Boolean apply(WebDriver driver) { 
        return base.waitForElementList(base.id(TestID.DIV_VALIDATION_POPUP), false).size() == 0;
      }
    });
  }

  public WebElement getNotificationToggleButton() {
    // old xpath - for safekeeping:  "//div[not(contains(@style,'display: none;'))]/child::div[@id='gwt-debug-notificationButton']"
    final List<WebElement> elements = base.waitForElementList(base.xpath("//div[@id='gwt-debug-notificationButton']"));
    return elements.stream()
        .filter(WebElement::isDisplayed)
        .findFirst()
        .get();
  }

  public boolean isNotificationPanelPresent() {
    return base.waitForElementList(base.id(TestID.BUTTON_DELETE_ALL_NOTIFICATIONS)).size() > 0;
  }

  public void buttonClickNotificationToggle() {
    base.buttonClick(getNotificationToggleButton());
    base.browserWaitForDrawAnimation();
  }

  public List<WebElement> getNotificationItems() {
    final String xpath = "//div[@id = 'gwt-debug-" + TestID.DIV_NOTIFICATIONITEM + "']";
    return base.waitForElementList(base.xpath(xpath));
  }

  public WebElement getPopupPanel() {
    return base.waitForElementVisible(base.id(TestID.DIV_NOTIFICATIONPOPUP));
  }

}
