/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.Duration;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for handling the task bar.
 */
public class TaskBarAction {
  private static final Logger LOG = LoggerFactory.getLogger(TaskBarAction.class);

  private final FunctionalTestBase base;

  public TaskBarAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Enumeration of all valid panels in the task bar.
   */
  public enum TaskBarPanel {
    LEGEND, INFO, LAYERS, OPTIONS;
  }

  /**
   * Enumeration of all valid result types.
   */
  public enum ResultType {
    DEPOSTION, CONCENTRATION, DAYS;
  }

  /**
   * Sets the calculation-year from the tool bar.
   * @param year the year to select from the year-selector
   */
  public void setYear(final String year) {
    final String yearOptionId = TestID.LABEL_YEARSELECTION + "-" + year;
    base.buttonClick(TestID.BUTTON_YEARSELECTION);
    base.browserWaitForDrawAnimation();
    base.buttonClick(yearOptionId);
    base.switchFocus();
    base.waitForElementInvisible(base.id(yearOptionId));
  }

  /**
   * Checks the calculation-year in the task bar.
   * @param year the year to look for in the year-selector
   */
  public void getYear(final String year) {
    base.buttonCheckText(TestID.BUTTON_YEARSELECTION, year);
  }

  /**
   * Opens the year list from the tool bar
   */
  public void openYearList() {
    base.buttonClick(TestID.BUTTON_YEARSELECTION);
  }

  /**
   * Sets the substance from the task bar.
   * @param substance the substance to select from the substance-selector
   */
  public void setSubstance(final String substance) {
    base.buttonClick(TestID.BUTTON_SUBSTANCESELECTION);
    base.buttonClickJs(base.waitForElementVisible(base.id(TestID.LABEL_SUBSTANCESELECTION + "-" + substance)));
  }

  /**
   * Check which substance is set in the task bar.
   * @param substance the substance to look for in the substance-selector button
   */
  public void getSubstance(final String substance) {
    base.buttonCheckText(TestID.BUTTON_SUBSTANCESELECTION, substance);
  }

  /**
   * Opens the substance list from the tool bar.
   */
  public void openSubstanceList() {
    base.buttonClick(TestID.BUTTON_SUBSTANCESELECTION);
  }

  /**
   * Selects deposition or concentration from the substance tool bar.
   * @param substance the substance to select from the substance-selector
   */
  public void setResultType(final ResultType resultType) {
    base.buttonClick(TestID.BUTTON_RESULTTYPESELECTION);
    base.browserWaitForDrawAnimation();

    switch (resultType) {
    case DEPOSTION:
      base.buttonClick(TestID.BUTTON_RESULTTYPE_DEPOSITION);
      break;
    case CONCENTRATION:
      base.buttonClick(TestID.BUTTON_RESULTTYPE_CONCENTRATION);
      break;
    case DAYS:
      base.buttonClick(TestID.BUTTON_RESULTTYPE_NUM_OF_DAYS);
      break;
    default:
      base.buttonClick(TestID.BUTTON_RESULTTYPE_DEPOSITION);
      break;
    }
    base.browserWaitForDrawAnimation();
  }

  /**
   * Opens the given panel.
   * @param panel {@link TaskBarPanel} to open
   */
  public void openPanel(final TaskBarPanel panel) {
    switch (panel) {
    case OPTIONS:
      base.buttonClick(TestID.BUTTON_OPTIONSPANEL);
      break;
    case LEGEND:
      base.buttonClick(TestID.BUTTON_LEGENDPANEL);
      break;
    case INFO:
      base.buttonClick(TestID.BUTTON_INFOPANEL);
      break;
    case LAYERS:
      base.buttonClick(TestID.BUTTON_LAYERPANEL);
      break;
    default:
      base.browserWait(Duration.ofSeconds(1));
      break;
    }
    base.browserWaitForDrawAnimation();
  }

  /**
   * Closes an active panel by pushing the close-button.
   */
  public void closePanel() {
    base.buttonClick(TestID.BUTTON_DIALOG_CONFIRM);
  }

  /**
   * Toggles the given map layer collapse panel.
   * Precondition: layer panel is open.
   * Postcondition: given layer toggled on or off.
   * @param mapLayerName visible text (name) of the map layer
   */
  public void layerToggleCollapse(final String mapLayerName) {
    final WebElement mapLayer = pLayerPanelGetMapLayer(mapLayerName);
    mapLayer.findElement(By.xpath(".//div[contains(@id, 'buttonPlusMinus')]")).click();
  }

  /**
   * Check if the layer is present
   * Precondition: layer panel is opened
   * @param mapLayerName visible text of the map layer to check
   */
  public boolean isLayerPresent(final String mapLayerName) {
    try {
      return pLayerPanelGetMapLayer(mapLayerName).isDisplayed();
    } catch(Exception e) {
      return false;
    }
  }

  /**
   * Toggles the given map layer on or off, in the map layer panel.
   * Precondition: layer panel is open.
   * Postcondition: given layer toggled on or off.
   * @param mapLayerName visible text (name) of the maplayer to select
   */
  public void layerToggleOnOff(final String mapLayerName) {
    final WebElement mapLayer = pLayerPanelGetMapLayer(mapLayerName);
    base.objectHighlight(mapLayer);
    final WebElement switchOnOff = mapLayer.findElement(By.xpath(".//label[contains(@id, 'checkbox')]"));
    switchOnOff.click();
    LOG.info("Maplayer '" + mapLayerName + "': visible toggled");
  }

  /**
   * Set the opacity of the given map layer, in the map layer panel.
   * Precondition: layer panel is open.
   * Postcondition: opacity is set.
   * @param mapLayerName visible text (name) of the map layer
   * @param opacity the new value for the opacity (number)
   */
  public void layerSetOpacity(final String mapLayerName, final String opacity) {
    // Find opacity slider knob starting from the map layer element and focus by clicking
    final WebElement mapLayer = pLayerPanelGetMapLayer(mapLayerName);
    final WebElement opacitySlider = mapLayer.findElement(By.xpath(".//div[contains(@class,'knob')]"));
    base.buttonClick(opacitySlider);

    // Determine current and target value
    final int currentOpacity = Integer.valueOf(opacitySlider.getText());
    final int targetOpacity = Integer.valueOf(opacity);

    // Send left or right keyboard input events as much as needed to reach target value
    if (currentOpacity < targetOpacity) {
      for (int i = 0; i < targetOpacity - currentOpacity; i++) {
        new Actions(base.getDriver()).sendKeys(Keys.RIGHT).perform();
        LOG.debug("Increasing opacity of map layer: '{}'", mapLayerName);
      }
    } else if (currentOpacity > targetOpacity) {
      for (int i = 0; i < currentOpacity - targetOpacity; i++) {
        new Actions(base.getDriver()).sendKeys(Keys.LEFT).perform();
        LOG.debug("Decreasing opacity of map layer: '{}'", mapLayerName);
      }
    }

    final String newOpacity = opacitySlider.getText();

    if (newOpacity.equals(opacity)) {
      LOG.info("Maplayer '{}' opacity succesfully changed from '{}' to '{}'", mapLayerName, currentOpacity, newOpacity);
    } else {
      Assert.fail("Maplayer '" + mapLayerName + "' opacity NOT set to: '" + newOpacity + "'");
    }

  }

  /**
   * Gets a map layer-element in the layer panel by the given text.
   * @param mapLayerName visible text (name) of the map layer to select
   * @return {@link WebElement} map layer in layer panel
   */
  private WebElement pLayerPanelGetMapLayer(final String mapLayerName) {
    final String layerId = base.idPrefix(TestID.DIV_LAYERPANELITEM + "-" + mapLayerName);
    return base.waitForElementClickable(base.waitForElementVisible(By.id(layerId)), true);
  }

  /**
   * Selects a habitat from the habitat filter by code
   * Precondition: layer panel is open.
   * Postcondition: habitat is selected
   * @param mapLayerName visible text (name) of the map layer
   */
  public void selectHabitatFromFilter(final String habitatCode) {
    base.inputSelectListItemByText(base.id(TestID.INPUT_LAYERPANEL_LISTBOX + "-" + "Habitattypen"), habitatCode);
  }

  /**
   * Opens the habitat filter list by clicking it
   */
  public void openHabitatFilterList() {
    base.mouseClickOnElement(base.waitForElementVisible(base.id(TestID.INPUT_LAYERPANEL_LISTBOX + "-" + "Habitattypen")));
  }

  /**
   * Selects a layer to display from list box "Total deposition per sector"
   * @param depositionLayer name of the layer to select
   */
  public void selectLayerFromTotalDepositionPerSector(final String depositionLayer) {
    base.inputSelectListItemByText(base.id(TestID.INPUT_LAYERPANEL_LISTBOX + "-" + "Totale depositie per sector"), depositionLayer);
  }

  /**
   * Opens the total deposition layer list by clicking it
   */
  public void openTotalDepositionLayerList() {
    base.mouseClickOnElement(base.waitForElementVisible(base.id(TestID.INPUT_LAYERPANEL_LISTBOX + "-" + "Totale depositie per sector")));
  }

  /**
   * Toggles expand/collapse in info panel of Habitats
   */
  public void infoToggleHabitats() {
    base.buttonClick(TestID.BUTTON_COLLAPSEPANEL_PLUSMINUS + "-" + "Habitattypen");
  }

  /**
   * Toggles expand/collapse in info panel of Deposition
   */
  public void infoToggleDeposition() {
    base.buttonClick(TestID.BUTTON_COLLAPSEPANEL_PLUSMINUS + "-" + "Depositie");
  }

  /**
   * Toggles expand/collapse in info panel of area
   */
  public void infoToggleAreaName(final String areaName) {
    base.buttonClick(TestID.BUTTON_COLLAPSEPANEL_PLUSMINUS + "-" + areaName);
  }

  /**
   * Toggles the habitat type in the info panel, by code or name
   * @param habitatCode the code of the habitatType to search for and click
   *
   */
  public void infoToggleHabitatTypeByCode(final String habitatCode) {
    final String xpathPupUp = "//div[contains(@class, 'popupContent')]";
    final String xpathHabitat = ".//div[contains(@id, '" + TestID.COMPACT_HABITAT_TYPE_TABLE + "')]";
    final String xpathLabel = ".//div[contains(@class, 'Label')]";
    String habitatTypeFound = "N/A";

    final WebElement popUpPanel = base.waitForElementVisible(base.xpath(xpathPupUp));

    for (final WebElement habitatTypeRow : popUpPanel.findElements(By.xpath(xpathHabitat))) {
      final String habitatTypeLabel = habitatTypeRow.findElement(By.xpath(xpathLabel)).getText().trim();
      if (habitatTypeLabel.startsWith(habitatCode)) {
        habitatTypeFound = habitatCode;
        habitatTypeRow.click();
        LOG.debug("Info panel toggled habitatType : '{}'", habitatTypeLabel);
        break;
      } else {
        LOG.debug("Info panel disregarded habitatType: '{}'", habitatTypeLabel);
      }
    }
    Assert.assertEquals("Info panel habitatType not found!", habitatCode, habitatTypeFound);
  }

  /**
   * Checks if the correct backgrounddeposition is shown
   * @param backgroundDeposition the amount the backgrounddeposition should be
   */
  public void backgroundDepositionCheck(final String backgroundDeposition) {
    final String xpathBackground = "//span[.='" + backgroundDeposition + " mol/ha/j']";
    base.waitForElementVisible(By.xpath(xpathBackground));
  }

  /*
   * Checks if the correct cursor pointer is visible
   */
  public void checkCursorPointer(final String cursorType, final String cursorImage, final String xPath) {
    base.waitForElementVisible(By.xpath(xPath + "//img[contains(@src, '" + cursorImage + "')]"));
  }

  /*
   * Checks the total amount of cursor pointers in the info panel,
   * this should be exactly 2.
   */
  public void checkCursorPointerAmount() {
    final List<WebElement> ellist = base.waitForElementList(base.xpath("//div[contains(@class, 'contentContainer')]//tbody[1]/tr"));
    assertEquals("Incorrect amount of cursor pointers", 2, ellist.size());
  }
}
