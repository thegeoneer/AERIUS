/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.IOException;

import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.SituationTabs;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.SituationAction;

import cucumber.api.java.en.When;

public class SituationStepDef {

  private final FunctionalTestBase base;
  private final SituationAction situation;

  public SituationStepDef() throws IOException, InterruptedException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    situation = new SituationAction(base);
  }

  @When("^the alternate situation is created$")
  public void when_situation_alternate_is_created() {
    situation.createAlternateSituation();
  }

  @When("^situation 1 is selected$")
  public void when_situation_one_is_selected() {
    situation.switchToSituation(SituationTabs.TAB_1);
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  @When("^situation 1 is renamed to '(.+)'$")
  public void when_situation_one_is_renamed(final String name) {
    situation.renameSituation(SituationTabs.TAB_1, name);
  }

  @When("^situation 1 is deleted$")
  public void when_situation_one_is_deleted() {
    situation.deleteSituation(SituationTabs.TAB_1);
  }

  @When("^situation 2 is selected$")
  public void when_situation_two_is_selected() {
    situation.switchToSituation(SituationTabs.TAB_2);
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  @When("^situation 2 is renamed to '(.+)'$")
  public void when_situation_two_is_renamed(final String name) {
    situation.renameSituation(SituationTabs.TAB_2, name);
  }

  @When("^situation 2 is deleted$")
  public void when_situation_two_is_deleted() {
    situation.deleteSituation(SituationTabs.TAB_2);
  }

  @When("^the comparison tab is selected$")
  public void when_situation_compare_is_selected() {
    situation.switchToSituation(SituationTabs.COMPARE);
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  @When("^the situations are switched$")
  public void when_situations_are_switched() {
    situation.switchSituations();
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  @When("^the tabs Situation 1, 2 and Comparison are visible$")
  public void tab_item_visible() {
	situation.checkIfAllTabItemsVisible(); 
  }
}
