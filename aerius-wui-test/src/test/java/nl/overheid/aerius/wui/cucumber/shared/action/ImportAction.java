/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains methods that apply for importing files.
 */
public class ImportAction {
  private static final Logger LOG = LoggerFactory.getLogger(ImportAction.class);

  private final FunctionalTestBase base;

  public ImportAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Imports a file using the generic import dialog
   * Precondition: import dialog is visible
   * Postcondition: import dialog is confirmed
   * @param fileName of the file to import
   * @param importType an instance of {@link ImportType}
   */
  public void importFile(final String fileName, final ImportType importType) {
    base.waitForElementVisible(base.id(TestID.INPUT_IMPORTFILE));
    importSelectFile(fileName, importType);
    importConfirm(true);
    base.browserWaitForDrawAnimation();
  }

  /*
   * Imports a file with an invalid calculation option
   * Precondition: import dialog is visible
   * Postcondition: import dialog is confirmed
   * @param fileName of the file to import
   * @param importType an instance of {@link ImportType}
   */
  public void importFileWrong(final String fileName, final ImportType importType) {
    base.waitForElementVisible(base.id(TestID.INPUT_IMPORTFILE));
    importSelectFile(fileName, importType);
    importConfirm();
  }

  /**
   * Select a file in the import dialog from a path determined by file type and
   * current application. Overrides Melding to use the Calculator folder 
   * @param fileName of the file to import
   * @param importType an instance of {@link ImportType}
   */
  public void importSelectFile(final String fileName, final ImportType importType) {
    ProductType productType = base.getProductType() == ProductType.MELDING ? ProductType.CALCULATOR : base.getProductType();
    ImportType.determineByFilename(fileName);
    final String importFile = getImportFilePath(fileName, importType, productType);
    LOG.info("Importing file: {}", importFile);
    base.waitForElementVisible(base.id(TestID.INPUT_IMPORTFILE)).sendKeys(importFile);
  }

  private String getImportFilePath(final String fileName, final ImportType format, ProductType product) {
    final String filePath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath()
        + product.name().toLowerCase() + "/" + format.getExtension() + "/" + fileName;
    return FunctionalTestBase.isWindows() ? filePath.replaceFirst("/", "").replace("/", "\\") : filePath;
  }

  /**
   * Set the application ID in the application import dialog (REGISTER)
   */
  public void importSetApplicationId(final String dossierId) {
    base.inputSetValue(base.waitForElementVisible(base.id(TestIDRegister.INPUT_DOSSIER_ID)), dossierId, true, false);
  }

  /**
   * Set the custom description in the partial project import dialog (REGISTER)
   */
  public void importSetDescription(final String description) {
    final By locator = base.id(TestIDRegister.INPUT_IMPORT_ADDITIONAL_FIELD + "-label");
    base.inputSetValue(base.waitForElementVisible(locator), description, true, false);
  }

  /**
   * Set the application date stamp in the application import dialog (REGISTER)
   */
  public void importSetApplicationDate(final String date) {
    base.inputSetValue(base.waitForElementVisible(
        base.id(TestIDRegister.INPUT_IMPORT_ADDITIONAL_FIELD + "-receivedDate")), date, true, false);
  }

  /**
   * Confirm the open import dialog and make sure it gets closed
   */
  public void importConfirm(final Boolean waitForDialogClosed) {
    base.buttonClick(TestIDRegister.BUTTON_DIALOG_CONFIRM);
    if (waitForDialogClosed) {
      base.browserWaitForPopUpPanel();
    }
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  public void importConfirm() {
    importConfirm(false);
  }

  /**
   * Cancel the open import dialog and make sure it gets closed
   */
  public void importCancel(final Boolean waitForDialogClosed) {
    base.buttonClick(TestIDRegister.BUTTON_DIALOG_CANCEL);
    if (waitForDialogClosed) {
      base.waitForElementInvisible(base.id(TestIDRegister.BUTTON_DIALOG_CANCEL));
    }
  }

  public void importCancel() {
    importCancel(false);
  }

  /**
   * Check file import window for expected error messages.
   */
  public void windowContainsError(final String expectedError) {
     base.getWaitDefault().until(ExpectedConditions.textToBe(base.id(TestID.DIV_IMPORT_ERRORS), expectedError));
  }

}
