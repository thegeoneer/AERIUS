/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for handling validation messages.
 */
public class ValidationAction {
  private static final Logger LOG = LoggerFactory.getLogger(ValidationAction.class);

  private final FunctionalTestBase base;

  public ValidationAction(final FunctionalTestBase testRun) {
    this.base = testRun;
  }

  /**
   * Checks for validation errors.
   * @param validationErrorExpected boolean set whether an error is expected or not
   */
  public void checkValidationMessageIsDisplayed(final boolean validationErrorExpected) {

    boolean validationErrorIsDisplayed = false;

    try {
      validationErrorIsDisplayed = base.buttonHover(TestID.DIV_VALIDATION_POPUP).isDisplayed();
    } catch (final Exception e) {
      // ignore exception
    }

    if (!validationErrorExpected && validationErrorIsDisplayed) {
      Assert.fail("Unexpected validation-error popped up: " + base.waitForElementVisible(base.id(TestID.DIV_VALIDATION_POPUP)).getText());
    }

    if (validationErrorExpected && !validationErrorIsDisplayed) {
      Assert.fail("Expected validation-error not displayed");
    }

    if (validationErrorExpected && validationErrorIsDisplayed) {
      LOG.debug("Expected validation-error: " + base.waitForElementVisible(base.id(TestID.DIV_VALIDATION_POPUP)).getText());
    }

    if (!validationErrorExpected && !validationErrorIsDisplayed) {
      LOG.debug("No validation errors");
    }

  }
}
