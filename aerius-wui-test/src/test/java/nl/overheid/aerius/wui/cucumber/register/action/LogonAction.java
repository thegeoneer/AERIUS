/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Duration;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods required to login to register
 */
public class LogonAction {

  private static final Logger LOG = LoggerFactory.getLogger(LogonAction.class);

  private final FunctionalTestBase base;

  public LogonAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * The user name and passwords should be defined in a properties file as system variable: register.users = [file]
   * Use following properties convention: <code>viewer.username = [name], viewer.password = [pass]</code> etc
   */
  public enum UserRoles {
    VIEWER, EDITOR, SUPERUSER, ADMIN;
  }

  /*
   * Get appropriate user credentials for provided role and attempt to login,
   * making sure the page is loaded completely before continuing.
   */
  public void loginUserAsRole(final UserRoles role) {
    final String username = getUserNameForRole(role);
    final String password = getUserPassForRole(role);
    if (username == null || password == null) {
      throw new IllegalArgumentException("Cannot login as role '" + role + "' as the username or password isn't set.");
    }
    LOG.debug("User logging in with role: '" + role + "'");
    logUserIn(username, password);

    // switchFocus to wait for the application header to show up, only then it
    // makes sense to check for any active loading widgets
    base.switchFocus();
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

 /*
  * Login with provided credentials after determining authorization type (LDAP or default)
  */
  private void logUserIn(final String username, final String password) {
    // Extra check to determine if we're trapped on the access denied page
    if (base.getDriver().getTitle().contains("beveiligde toegang")) {
      Assert.fail("LDAP says no. Access denied!");
    }

    // Check if we're still logged on and need to log off before we can attempt another logon
    if (base.isElementDisplayed(TestIDRegister.BUTTON_LOGOUT)) {
      LOG.debug("User already logged in. Logging out to start a fresh session!");
      base.buttonClick(TestIDRegister.BUTTON_LOGOUT);
      base.browserWait(Duration.ofSeconds(1));
      base.browserAlertAccept();
      base.getDriver().navigate().refresh();
    }

    // Check login form elements to determine if it's an LDAP login or not
    final boolean shiroLogin = base.isElementDisplayed("userName") || base.isElementDisplayed("password") || base.isElementDisplayed("Logon"); 
    if (shiroLogin) {
      logonDefault(username, password);
    } else {
      logonLdap(username, password);
    }
  }

  /*
   * Login register user with LDAP credentials
   */
  private void logonLdap(final String username, final String password) {
    LOG.info("LDAP login as user: '{}'", username);
    base.inputSetValue(By.id("IDPUsername"), username);
    base.inputSetValue(By.id("IDPPassword"), password).sendKeys(Keys.RETURN);
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  /*
   * Login register user with default credentials 
   */
  private void logonDefault(final String username, final String password) {
    LOG.info("Non-LDAP login as user: '{}'", username);
    base.inputSetValue("userName", username);
    base.inputSetValue("password", password).sendKeys(Keys.RETURN);
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  /*
   * Returns the user account properties from the specified properties file
   */
  private Properties getRegisterUsers() {
    final Properties props = new Properties();
    try (BufferedReader reader = Files.newBufferedReader(new File(System.getProperty("register.users")).toPath(), StandardCharsets.UTF_8)) {
      props.load(reader);
    } catch (final IOException e) {
      Assert.fail("Failed to retrieve AERIUS user properties! " + e.getMessage());
    }
    return props;
  }

  /*
   * Check whether the users full name is correctly represented
   */
  public void userFullNameShownAs(final String fullName) {
    base.waitForElementVisible(base.xpath("//span[contains(text(),'" + fullName + "')]"));
  }

  public void userLogOff() {
    base.buttonClick(TestIDRegister.BUTTON_LOGOUT);
  }

  private String getUserNameForRole(final UserRoles role) {
    return getRegisterUsers().getProperty(role.name().toLowerCase() + ".username");
  }

  private String getUserPassForRole(final UserRoles role) {
    return getRegisterUsers().getProperty(role.name().toLowerCase() + ".password");
  }

}
