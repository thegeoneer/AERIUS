/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionValuesEditorMaritimeMooring;
import nl.overheid.aerius.wui.cucumber.shared.action.MapAction;

/**
 * Set Definition class for Maritime Mooring detail screen
 */
public class MaritimeMooringStepDef {

  private static final Logger LOG = LoggerFactory.getLogger(MaritimeMooringStepDef.class);

  private final FunctionalTestBase base;
  private final EmissionValuesEditorMaritimeMooring mSources;
  private final MapAction map;

  public MaritimeMooringStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    mSources = new EmissionValuesEditorMaritimeMooring(base);
    map = new MapAction(base);
  }

  @When("^the maritime mooring ship is a '(.+)' of category (\\d+) with (\\d+) ships staying (\\d+) hours$")
  public void when_the_maritime_mooring_ship_is_a_of_category_with_ships_staying_hours(final String name, final int categoryId, final int amount,
      final int residenceTime) {
    mSources.setSubItemShippingData(name, String.valueOf(categoryId), String.valueOf(amount), String.valueOf(residenceTime));
  }

  @When("^the maritime mooring inland route is drawn with coordinates (.+)$")
  public void when_the_maritime_mooring_inland_route_with_coordinates(final String coordinates) {
    mSources.setSubItemNewInlandRoute();
    map.clickLineOnMap(coordinates);
  }

  @When("^the maritime mooring sea route at row (\\d+) is drawn with coordinates (.+)$")
  public void when_the_maritime_mooring_sea_route_at_row_i_at_coordinates(final int row, final String coordinates) {
    mSources.setSubItemNewMaritimeRoute(row);
    map.clickLineOnMap(coordinates);
    base.browserWaitForDrawAnimation();
  }

  @When("^the maritime mooring sea route at row (\\d+) is set to (\\d+) ships$")
  public void when_the_maritime_mooring_sea_route_at_row_i_set_to_x_ships(final int row, final int ships) {
    mSources.setMaritimeMovements(row, String.valueOf(ships));
  }

  @Then("^there must be a maritime mooring ship '(.+)' of category (\\d+) with (\\d+) ships staying (\\d+) hours$")
  public void then_there_must_be_a_maritime_mooring_ship_a_of_category_with_ships_staying_hours(final String name, final int category_id, 
      final int amount, final int residenceTime) {
    Assert.assertEquals("Check ship name", name, mSources.getSubItemName());
    Assert.assertEquals("Check ship amount", String.valueOf(amount), mSources.getSubItemAmount());
    Assert.assertEquals("Check ship residence time", String.valueOf(residenceTime), mSources.getSubItemResidenceTime());
  }

  @Then("^the maritime mooring sea route at row (.+) must be empty$")
  public void then_the_maritime_mooring_sea_route_at_row_i_must_be_empty(final String row) {
    Assert.assertEquals("Check sea route number", "Kies...", mSources.getMaritimeMooringRouteNumberAtRow(row));
  }

  @Then("^the maritime mooring sea route at row (\\d+) must be (\\d+) ships$")
  public void then_the_maritime_mooring_sea_route_at_row_i_must_be_x_ships(final int row, final int ships) {
    Assert.assertEquals("Check ships at row " + row, String.valueOf(ships), mSources.getShipMovementsAtRow(row));
  }

  @Then("^the maritime mooring sea route row (\\d+) must be '(.+)'$")
  public void then_the_maritime_mooring_sea_route_row_i_must_be_visible(final int row, final String visiblity) {
    final boolean visible = "visible".equals(visiblity);
    assert !visible && "invisible".equals(visiblity) : "feature field should be 'visible' or 'invisible'";
    Assert.assertEquals("Check sea route row visibility", visible, mSources.isMaritimeMooringRowVisible(row));
  }

  @Then("^an error message for not matching maritime mooring visits must be given$")
  public void an_error_message_for_not_matching_maritime_mooring_visits_must_be_given() {
    Assert.assertTrue("Check error message is given when sea route numbers don't match", mSources.checkErrorMessageNoMatchingNumbers());
    // Click on the routes button to ensure the message is removed before continuing (or risk non-clickable element exceptions) 
    base.buttonClick(base.id(TestID.BUTTON_SHIPPING_ROUTES));
    base.switchFocus();
    base.browserWaitForDrawAnimation();
  }

  @Then("^the maritime mooring sea route at (.+) must be '(.+)'$")
  public void the_maritime_mooring_sea_route_at_row_x_must_be_y(final String row, final String seaRouteNumber) {
    Assert.assertEquals("Check sea route number", seaRouteNumber, mSources.getMaritimeMooringRouteNumberAtRow(row));
  }

  @Then("^the total emission of the source with label '(.+)' should be (.+)$")
  public void the_total_emission_of_the_source_with_label_should_be(final String sourceLabel, final String expectedEmission) {
    final String xpathLabel = "//div[@id='gwt-debug-label' and text()='" + sourceLabel + "']";
    final String xpathEmission = "//div[@id='gwt-debug-tableSourcesSectorViewer-0']/div[2]";
    base.buttonHover(base.waitForElementVisible(base.xpath(xpathLabel)));
    base.browserWaitForDrawAnimation();
    final String actualEmission = base.waitForElementVisible(base.xpath(xpathEmission)).getText();
    Assert.assertEquals(expectedEmission, actualEmission);
  }

}
