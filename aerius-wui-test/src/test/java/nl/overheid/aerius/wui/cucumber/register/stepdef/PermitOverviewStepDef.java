/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.register.stepdef;

import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.register.action.PermitOverviewAction;

public class PermitOverviewStepDef {

  private final FunctionalTestBase base;
  private final PermitOverviewAction permitOverview;

  public PermitOverviewStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    permitOverview = new PermitOverviewAction(base);
  }

  @When("^the application with description '(.+)' is opened$")
  public void when_application_open_from_overview(final String permitDescription) {
    permitOverview.eSelectPermitFromOverview(permitDescription);
  }

  @When("^the application with description '(.+)' is deleted$")
  public void when_application_deleted(final String permitDescription) {
    when_application_open_from_overview(permitDescription);
    base.buttonClick(TestIDRegister.BUTTON_REMOVE_APPLICATION);
    base.browserAlertAccept();
    base.waitForElementInvisible(base.id(TestIDRegister.BUTTON_REMOVE_APPLICATION), base.getSettingPageLoadTimeout().multipliedBy(3));
  }

  @When("^the application is deleted$")
  public void when_application_deleted() {
    base.checkAndHandleUnexpectedNotifications();
    base.buttonClick(TestIDRegister.BUTTON_REMOVE_APPLICATION);
    base.browserAlertAccept();
    base.waitForElementInvisible(base.id(TestIDRegister.BUTTON_REMOVE_APPLICATION), base.getSettingPageLoadTimeout().multipliedBy(3));
  }

  @When("^the priority sub project is deleted$")
  public void when_sub_project_deleted() {
    base.checkAndHandleUnexpectedNotifications();
    base.buttonClick(TestIDRegister.BUTTON_REMOVE_APPLICATION);
    base.browserAlertAccept();
    base.waitForElementInvisible(base.id(TestIDRegister.BUTTON_REMOVE_APPLICATION), base.getSettingPageLoadTimeout().multipliedBy(3));
  }

  @When("^the priority project is deleted$")
  public void when_priority_project_deleted() {
    base.checkAndHandleUnexpectedNotifications();
    base.buttonClick(TestIDRegister.PRIORITYPROJECT_DELETE_BUTTON);
    base.browserAlertAccept();
    base.waitForElementInvisible(base.id(TestIDRegister.PRIORITYPROJECT_DELETE_BUTTON), base.getSettingPageLoadTimeout().multipliedBy(3));
  }

}
