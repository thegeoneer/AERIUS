/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import cucumber.api.java.en.When;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.MapAction;

public class MapStepDef {

  private final SeleniumDriverWrapper sdw;
  private final FunctionalTestBase base;
  private final MapAction map;

  public MapStepDef() {
    sdw = SeleniumDriverWrapper.getInstance();
    base = new FunctionalTestBase(sdw);
    map = new MapAction(base);
  }

  @When("^the map is zoomed in$")
  public void when_map_zoom_in() {
    map.clickZoomIn();
    base.browserWaitForDrawAnimation();
  }

  @When("^the map is zoomed out$")
  public void when_map_zoom_out() {
    map.clickZoomOut();
    base.browserWaitForDrawAnimation();
  }

  @When("^the map is zoomed out (\\d+) times$")
  public void when_map_zoom_out(final int numerOfTimes) {
    for (int x = 0; x < numerOfTimes; x++) {
      map.clickZoomOut();
    }
  }

  @When("^the map is zoomed in (\\d+) times$")
  public void when_map_zoom_in_times(final int numerOfTimes) {
    for (int x = 0; x < numerOfTimes; x++) {
      map.clickZoomIn();
    }
  }

  @When("^the map is clicked in the center$")
  public void when_map_click_center() {
    map.clickOnMapCenter();
  }

  @When("^the map is clicked in the center to select a point$")
  public void when_map_click_center_select_point() {
    map.selectPointMapCenter();
  }

  @When("^the map is panned up$")
  public void when_map_pan_up() {
    map.panUp();
  }

  @When("^the map is panned down$")
  public void when_map_pan_down() {
    map.panDown();
  }

  @When("^the map is panned left$")
  public void when_map_pan_left() {
    map.panLeft();
  }

  @When("^the map is panned right$")
  public void when_map_pan_right() {
    map.panRight();
  }

}
