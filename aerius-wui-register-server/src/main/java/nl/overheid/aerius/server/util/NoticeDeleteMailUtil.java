/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Locale;

import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.MailTo;
import nl.overheid.aerius.mail.MessageTaskClient;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.register.RegisterMailUtil;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Util to send mails informing authorities about the deletion of a notice.
 */
public final class NoticeDeleteMailUtil {

  private NoticeDeleteMailUtil() {
    //util class.
  }

  /**
   * Send a mail informing the authority about the deletion of a notice.
   */
  public static void sendMailToAuthority(final Connection con, final TaskManagerClient client, final Notice notice, final UserProfile deletedBy)
      throws SQLException, IOException {
    final Locale locale = LocaleUtils.getDefaultLocale();
    final Authority authority = RegisterMailUtil.getAuthorityForRequest(con, notice.getReference());

    final MailTo mailTo = new MailTo(authority.getEmailAddress());
    mailTo.addBcc(RegisterMailUtil.getAuthorityCentralMail(con));

    final MailMessageData mailMessageData = new MailMessageData(
        MessagesEnum.MELDING_DELETE_AUTHORITY_MAIL_SUBJECT, MessagesEnum.MELDING_DELETE_AUTHORITY_MAIL_CONTENT, locale, mailTo);

    setMailTokens(notice, authority, mailMessageData, deletedBy, locale);

    MessageTaskClient.startMessageTask(client, mailMessageData);
  }

  private static void setMailTokens(final Notice notice, final Authority authority, final MailMessageData mailMessageData,
      final UserProfile deletedBy, final Locale locale) {
    RegisterMailUtil.setAuthorityReplacementTokens(authority, mailMessageData);

    mailMessageData.setReplacement(ReplacementToken.PROJECT_NAME, notice.getScenarioMetaData().getCorporation());

    mailMessageData.setReplacement(ReplacementToken.AERIUS_REFERENCE, notice.getReference());

    mailMessageData.setReplacement(ReplacementToken.MELDING_CREATE_DATE,
        MessageTaskClient.getDefaultDateTimeFormatted(notice.getReceivedDate(), locale));
    mailMessageData.setReplacement(ReplacementToken.MELDING_REMOVE_DATE,
        MessageTaskClient.getDefaultDateTimeFormatted(new Date(), locale));
    mailMessageData.setReplacement(ReplacementToken.MELDING_REMOVED_BY,
        deletedBy.getName() + " " + deletedBy.getAuthority().getDescription());
  }

}
