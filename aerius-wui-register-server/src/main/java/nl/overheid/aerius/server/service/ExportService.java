/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.export.ExportStatus;
import nl.overheid.aerius.shared.domain.export.ExportStatus.Status;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.TaskResultCallback;

/**
 * Abstract export service that can be used to send a task to workers and poll till the result is in. Once the result is in, this result can be used
 * to return a download link (either to the location the worker placed the file, or to the servlet that will transfer the received byte array to the
 * user).
 */
abstract class ExportService<T> {

  private static final Logger LOG = LoggerFactory.getLogger(ExportService.class);

  private static final String POSTFIX_STATUS = "_status";
  protected final AeriusSession session;

  public ExportService(final AeriusSession session) {
    this.session = session;
  }

  protected String createSessionKey() {
    final String resultSessionKey = ExportStatus.KEY_PREFIX + UUID.randomUUID().toString();
    session.getSession().setAttribute(getStatusKey(resultSessionKey), ExportStatus.Status.RUNNING);
    return resultSessionKey;
  }

  protected ExportTaskResultCallback createResultCallback(final String resultSessionKey) {
    LOG.info("Export callback created with session key: {}", resultSessionKey);
    return new ExportTaskResultCallback(resultSessionKey, session.getSession());
  }

  protected ExportStatus.Status getCurrentStatus(final String sessionKey) throws AeriusException {
    final String statusKey = getStatusKey(sessionKey);
    if (!statusKey.startsWith(ExportStatus.KEY_PREFIX) || session.getSession().getAttribute(statusKey) == null) {
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    return (ExportStatus.Status) session.getSession().getAttribute(statusKey);
  }

  public T getExportResult(final String sessionKey) throws AeriusException {
    final ExportStatus.Status status = getCurrentStatus(sessionKey);
    T result = null;
    if (status == Status.FINISHED) {
      result = getExportResult(sessionKey, (ExportedData) session.getSession().getAttribute(sessionKey));
    }

    return result;
  }

  protected abstract T getExportResult(String sessionKey, ExportedData exportedData);

  private static String getStatusKey(final String resultSessionKey) {
    return resultSessionKey + POSTFIX_STATUS;
  }

  private static class ExportTaskResultCallback implements TaskResultCallback {

    private final String resultSessionKey;
    private final HttpSession session;

    ExportTaskResultCallback(final String resultSessionKey, final HttpSession session) {
      this.resultSessionKey = resultSessionKey;
      this.session = session;
    }

    @Override
    public void onSuccess(final Object value, final String correlationId, final String messageId) {
      if (value instanceof ExportedData) {
        setSessionData(resultSessionKey, (ExportedData) value);
      }
    }

    @Override
    public void onFailure(final Exception e, final String correlationId, final String messageId) {
      setSessionData(resultSessionKey, (ExportedData) null);
    }

    private void setSessionData(final String resultSessionKey, final ExportedData exportResult) {
      try {
        session.setAttribute(getStatusKey(resultSessionKey), exportResult == null ? ExportStatus.Status.ERROR : ExportStatus.Status.FINISHED);
        session.setAttribute(resultSessionKey, exportResult);
      } catch (final IllegalStateException e) {
        // Eat error, but warn
        LOG.info("Export error eaten.", e);
      }
    }
  }
}
