/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.register.NoticeRepository;
import nl.overheid.aerius.db.register.PermitRepository;
import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.util.ExportRequestsDownloadUtil;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFilter;
import nl.overheid.aerius.util.OSUtils;
import nl.overheid.aerius.util.ZipFileMaker;

/**
 * Allows the user to download an export of requests.
 */
@WebServlet("/" + ExportRequestsDownloadUtil.DOWNLOAD_LOCATION)
public class RequestExportDownloadServlet extends AbstractDownloadServlet {

  private static final long serialVersionUID = -4254975891565386478L;

  private static final Logger LOG = LoggerFactory.getLogger(RequestExportDownloadServlet.class);

  private static final String FILENAME_DELIMITER = "_";
  private static final String DATEFORMAT = "yyyy-MM-dd_HHmm";

  private static final String BASE_DOWNLOAD_NAME = "registerExport";

  private static final String ZIP_ENTRY_ERROR_FILE = "error.txt";

  private enum ExportEntity {
    NOTICE, PERMIT;

    private static ExportEntity determineEntity(final RequestFilter<?> filter) {
      ExportEntity entity = null;
      if (filter instanceof PermitFilter) {
        entity = ExportEntity.PERMIT;
      } else if (filter instanceof NoticeFilter) {
        entity = ExportEntity.NOTICE;
      }
      return entity;
    }
  }

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
    final String id = request.getParameter(ExportRequestsDownloadUtil.PARAMETER_ID);

    // Validate the parameters.
    if (StringUtils.isEmpty(id)) {
      missingRequiredParameter(response);
    } else {
      handleExport(request, response, id);
    }
  }

  private void handleExport(final HttpServletRequest request, final HttpServletResponse response, final String id) {
    final RequestFilter<?> filter = getFilter(request, id);
    if (filter == null) {
      writeMessageToUser(
          response,
          "Invalid ID or export already downloaded.",
          "ID of export non-existent. Invalid ID or export already downloaded.");
    } else {
      try (final Connection con = ServerPMF.getInstance().getConnection()) {
        final ExportEntity entity = ExportEntity.determineEntity(filter);
        final List<ExportEntry> exportEntries = getExportEntries(con, filter);
        if (isValidExport(response, exportEntries, entity, filter)) {
          exportAsZip(response, con, exportEntries, entity);
        }
      } catch (final SQLException e) {
        LOG.error("SQL error occured.", e);
        internalError(response);
      }
    }
  }

  private RequestFilter<?> getFilter(final HttpServletRequest request, final String id) {
    final RequestFilter<?> filter = ExportRequestsDownloadUtil.getFilterForUuid(request.getSession(true), id);
    ExportRequestsDownloadUtil.removeFilterForUuid(request.getSession(true), id);
    return filter;
  }

  private List<ExportEntry> getExportEntries(final Connection con, final RequestFilter<?> filter) throws SQLException {
    final List<ExportEntry> exportEntries = new ArrayList<>();

    // Fetch the requests matching the filter and make an inventory of all the files that it contains.
    if (filter instanceof PermitFilter) {
      for (final Permit permit : PermitRepository.getPermits(con, Integer.MAX_VALUE, 0, (PermitFilter) filter)) {
        addRequestFiles(con, exportEntries, permit);
      }
    } else if (filter instanceof NoticeFilter) {
      for (final Notice notice : NoticeRepository.getNotices(con, Integer.MAX_VALUE, 0, (NoticeFilter) filter)) {
        addRequestFiles(con, exportEntries, notice);
      }
    }
    return exportEntries;
  }

  private void addRequestFiles(final Connection con, final List<ExportEntry> exportEntries, final Request request) throws SQLException {
    final ArrayList<RequestFile> requestFiles = RequestRepository.getExistingRequestFiles(con, request.getReference());
    for (final RequestFile requestFile : requestFiles) {
      exportEntries.add(new ExportEntry(request.getReference(), requestFile));
    }
  }

  private boolean isValidExport(final HttpServletResponse response, final List<ExportEntry> exportEntries, final ExportEntity entity,
      final RequestFilter<?> filter) {
    boolean valid = false;
    if (entity == null) {
      LOG.error("Unimplemented filter type received: {}", filter);
      internalError(response);
    } else if (exportEntries.isEmpty()) {
      writeMessageToUser(
          response,
          "The filter of the export resulted in no matching files. Exported items have probably been deleted in the meantime.",
          "The filter of the export resulted in no matching files. Exported items have probably been deleted in the meantime: {}", filter);
    } else {
      valid = true;
    }
    return valid;
  }

  private void exportAsZip(final HttpServletResponse response, final Connection con, final List<ExportEntry> exportEntries, final ExportEntity entity)
      throws SQLException {
    final List<String> errors = new ArrayList<>();
    response.setContentType(MIMETYPE_ZIP);
    response.setHeader("Content-Disposition", "attachment; filename=\"" + getZipFileName(entity) + "\"");

    try (final ZipFileMaker zipMaker = new ZipFileMaker(response.getOutputStream())) {
      for (final ExportEntry exportEntry : exportEntries) {
        if (!addRequestFile(zipMaker, con, exportEntry, entity, errors)) {
          break;
        }
      }

      addErrorFile(zipMaker, errors);
    } catch (final IOException ioe) {
      LOG.warn("I/O Exception while creating ZIP", ioe);
      // To late to warn user, content already set, do nothing
    }
  }

  private boolean addRequestFile(final ZipFileMaker zipMaker, final Connection con, final ExportEntry exportEntry, final ExportEntity entity,
      final List<String> errors) throws SQLException {
    final byte[] fileContent = RequestRepository.getRequestFileContent(con, exportEntry.getReference(),
        exportEntry.getRequestFile().getRequestFileType());

    if (fileContent != null) {
      try {
        zipMaker.add(getFileName(exportEntry, entity), fileContent);
      } catch (final IOException e) {
        LOG.warn("Error while sending file content.", e);
        return false;
      }
    } else {
      LOG.error("Skipping empty file while exporting requests for reference '{}' and filetype '{}'",
          exportEntry.getReference(), exportEntry.getRequestFile().getRequestFileType());
      errors.add(entity + " with reference " + exportEntry.getReference()
          + " [" + exportEntry.getRequestFile().getRequestFileType().name().toLowerCase() + "]"
          + " does not have a valid file associated with it and has been skipped.");
    }
    return true;
  }

  private String getZipFileName(final ExportEntity entity) {
    final StringBuilder sb = new StringBuilder();
    sb.append(BASE_DOWNLOAD_NAME);
    sb.append(FILENAME_DELIMITER);
    sb.append(entity.name().toLowerCase());
    sb.append(FILENAME_DELIMITER);
    sb.append(new SimpleDateFormat(DATEFORMAT).format(new Date()));
    sb.append('.');
    sb.append(FileFormat.ZIP.getExtension());
    return sb.toString();
  }

  private String getFileName(final ExportEntry exportEntry, final ExportEntity entity) {
    final StringBuilder sb = new StringBuilder();
    sb.append(entity.name().toLowerCase());
    sb.append(FILENAME_DELIMITER);
    sb.append(exportEntry.getReference());
    sb.append(FILENAME_DELIMITER);
    sb.append(exportEntry.getRequestFile().getRequestFileType().name().toLowerCase());
    sb.append('.');
    sb.append(exportEntry.getRequestFile().getFileFormat().getExtension());
    return sb.toString();
  }

  private void addErrorFile(final ZipFileMaker zipMaker, final List<String> errors) throws IOException {
    if (!errors.isEmpty()) {
      final StringBuilder errBuilder = new StringBuilder("The export finished with one or more errors:").append(OSUtils.WNL);
      for (final String errorString : errors) {
        errBuilder.append("- ").append(errorString).append(OSUtils.WNL);
      }
      zipMaker.add(ZIP_ENTRY_ERROR_FILE, errBuilder.toString());
    }
  }

  @Override
  protected Logger getLogger() {
    return LOG;
  }

  private class ExportEntry {
    private final String reference;
    private final RequestFile requestFile;

    ExportEntry(final String reference, final RequestFile requestFile) {
      this.reference = reference;
      this.requestFile = requestFile;
    }

    private String getReference() {
      return reference;
    }

    private RequestFile getRequestFile() {
      return requestFile;
    }
  }

}
