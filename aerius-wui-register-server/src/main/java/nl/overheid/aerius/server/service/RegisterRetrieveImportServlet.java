/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportService;

@WebServlet("/aerius/aerius-retrieve-register-import")
public class RegisterRetrieveImportServlet extends RemoteServiceServlet implements RegisterRetrieveImportService {

  private static final long serialVersionUID = 3292736319349892794L;

  @Override
  public PermitKey getPermitResult(final String key) throws AeriusException {
    return (PermitKey) getResult(UUID_PREFIX_PERMIT, key);
  }

  @Override
  public PrioritySubProjectKey getPrioritySubProjectResult(final String key) throws AeriusException {
    return (PrioritySubProjectKey) getResult(UUID_PREFIX_PP_SUBPROJECT, key);
  }

  @Override
  public PriorityProjectKey getPriorityProjectFactsheetResult(final String key) throws AeriusException {
    return (PriorityProjectKey) getResult(UUID_PREFIX_PP_FACTSHEET, key);
  }

  @Override
  public PriorityProjectKey getPriorityProjectActualisationResult(final String key) throws AeriusException {
    return (PriorityProjectKey) getResult(UUID_PREFIX_PP_ACTUALISATION, key);
  }

  @Override
  public PriorityProjectKey getPriorityProjectResult(final String key) throws AeriusException {
    return (PriorityProjectKey) getResult(UUID_PREFIX_PP, key);
  }

  private Object getResult(final String keyPrefix, final String key) throws AeriusException {
    final HttpSession session = getThreadLocalRequest().getSession(true);
    final String fullKey = keyPrefix + key;

    final Object object = session.getAttribute(fullKey);
    if (object != null) {
      session.removeAttribute(fullKey);
    }

    if (object instanceof AeriusException) {
      throw (AeriusException) object;
    }
    return object;
  }

}
