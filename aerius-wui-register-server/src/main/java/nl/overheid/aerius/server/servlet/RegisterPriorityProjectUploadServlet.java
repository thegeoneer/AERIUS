/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.EnumSet;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.PriorityProjectModifyRepository;
import nl.overheid.aerius.db.register.PriorityProjectRepository;
import nl.overheid.aerius.register.RegisterImportUtil;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.service.ImportService.UploadOptions;
import nl.overheid.aerius.server.service.RequestInserter;
import nl.overheid.aerius.server.service.RequestUploadService;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.exception.ImportDuplicateEntryException;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportService;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Handles Priority Project uploads.
 */
@MultipartConfig
@SuppressWarnings("serial")
@WebServlet("/aerius/" + SharedConstants.IMPORT_PRIORITYPROJECT_SERVLET)
public class RegisterPriorityProjectUploadServlet extends HttpServlet {

  private static final Logger LOG = LoggerFactory.getLogger(RegisterPriorityProjectUploadServlet.class);

  private static final EnumSet<RequestFileType> VALID_TYPES =
      EnumSet.of(RequestFileType.PRIORITY_PROJECT_RESERVATION, RequestFileType.PRIORITY_PROJECT_ACTUALISATION);

  private static final RequestInserter<PriorityProject> INSERT_HELPER = new RequestInserter<PriorityProject>() {

    @Override
    public PriorityProject insertRequest(final Connection connection, final PriorityProject pp, final UserProfile editedBy,
        final InsertRequestFile insertRequestFile) throws SQLException, AeriusException {
      return PriorityProjectModifyRepository.insert(connection, pp, editedBy, insertRequestFile);
    }

    @Override
    public void doPersistResultsPostOperations(final Connection connection, final PriorityProject pp, final UserProfile editedBy)
        throws AeriusException {
      super.doPersistResultsPostOperations(connection, pp, editedBy);

      // reserve the development space needed
      PriorityProjectModifyRepository.insertDevelopmentSpaces(connection, pp.getKey(), editedBy, pp.getLastModified());
    }
  };

  /**
   * Processes the uploaded file and stores it in the database.
   */
  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
    final UploadOptions uploadOptions = new UploadOptions();
    uploadOptions.setSupportedFormats(SupportedUploadFormat.PRIORITY_PROJECT);
    uploadOptions.setPersistResults(true);

    final RequestFileType requestFileType = RequestFileType.safeValueOf(request.getParameter(SharedConstants.FORM_POST_TYPE));
    final RequestUploadService<PriorityProjectKey> requestUploadService = new RequestUploadService<PriorityProjectKey>(
        getPMF(),
        Internationalizer.getLocaleFromCookie(request),
        RegisterRetrieveImportService.UUID_PREFIX_PP,
        RequestFileType.PRIORITY_PROJECT_RESERVATION,
        uploadOptions) {

      @Override
      public PriorityProjectKey handleProcessedUpload(final ImportOutput output, final String dossierId, final UserProfile uploadedBy,
          final InsertRequestFile insertRequestFile) throws AeriusException {
        return insertProject(requestFileType, output, dossierId, uploadedBy, insertRequestFile);
      }

      @Override
      protected TaskManagerClient getTaskManagerClient() throws IOException {
        return RegisterPriorityProjectUploadServlet.this.getTaskManagerClient();
      }
    };

    requestUploadService.processRequest(request, response);
  }

  private PriorityProjectKey insertProject(final RequestFileType requestFileType, final ImportOutput output, final String dossierId,
      final UserProfile uploadedBy, final InsertRequestFile insertRequestFile) throws AeriusException {
    if (requestFileType == null || !VALID_TYPES.contains(requestFileType)) {
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }

    final boolean actualisation = RequestFileType.PRIORITY_PROJECT_ACTUALISATION == requestFileType;
    final boolean hasSources = output.getCalculatedScenario().getScenario().hasSources();
    final boolean hasResults = output.getCalculatedScenario().hasCalculations();

    // do permission check based on type
    UserProfileUtil.checkPermission(
        uploadedBy, actualisation ? RegisterPermission.ADD_NEW_PRIORITY_PROJECT : RegisterPermission.ADD_NEW_PRIORITY_PROJECT_RESERVATION);

    doFileChecks(actualisation, hasSources, hasResults);

    final String reference = output.getCalculatedScenario().getMetaData().getReference();

    try (final Connection connection = getPMF().getConnection()) {
      final PriorityProject priorityProject = RegisterImportUtil.convertToPriorityProject(output);
      priorityProject.getDossierMetaData().setDossierId(dossierId);
      priorityProject.setAuthority(uploadedBy.getAuthority());

      final PriorityProjectKey inDbByReference = PriorityProjectRepository.getPriorityProjectKey(connection, reference);
      final PriorityProjectKey inDbByKey =
          PriorityProjectRepository.getPriorityProjectByKey(connection, priorityProject.getKey()) == null ? null : priorityProject.getKey();
      if (inDbByReference == null && inDbByKey == null) {
        final PriorityProject insertedPriorityProject =
            INSERT_HELPER.insertInTransaction(connection, output, uploadedBy, priorityProject, insertRequestFile, !actualisation);

        // Also upload file as actualisation if it is one.
        if (actualisation) {
          insertRequestFile.setRequestFileType(RequestFileType.PRIORITY_PROJECT_ACTUALISATION);
          PriorityProjectModifyRepository.updateActualisation(connection, priorityProject.getKey(), insertRequestFile, uploadedBy);
        }

        return PriorityProjectRepository.getPriorityProjectKey(connection, insertedPriorityProject.getReference());
      } else if (inDbByReference != null) {
        throw new ImportDuplicateEntryException(inDbByReference);
      } else {
        throw new ImportDuplicateEntryException(inDbByKey);
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "SQLException while handling");
    }
  }

  protected PMF getPMF() {
    return ServerPMF.getInstance();
  }

  protected TaskManagerClient getTaskManagerClient() throws IOException {
    return TaskClientFactory.getInstance();
  }

  private void doFileChecks(final boolean actualisation, final boolean hasSources, final boolean hasResults) throws AeriusException {
    // PP actualisation should have sources
    if (actualisation && !hasSources) {
      throw new AeriusException(Reason.IMPORT_NO_SOURCES_PRESENT);
    // PP reservation should have results
    } else if (!actualisation && !hasResults) {
      throw new AeriusException(Reason.IMPORT_NO_RESULTS_PRESENT);
    }
  }

}
