/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import static org.mockito.Mockito.mock;

import java.sql.SQLException;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.server.ExampleUtil;
import nl.overheid.aerius.shared.domain.user.UserRole;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link RegisterContextServiceImpl}.
 */
public class RegisterContextServiceImplTest extends BaseDBTest {

  private RegisterContextServiceImpl context;

  @Before
  public void before() throws SQLException, AeriusException {
    final RegisterSession session = mock(RegisterSession.class);
    context = new RegisterContextServiceImpl(getRegPMF(), session);
    ExampleUtil.mockSubject(getRegConnection(), new HashSet<UserRole>());
  }

  @Test
  public void testGetUserContext() throws AeriusException {
    context.getUserContext();
  }
}
