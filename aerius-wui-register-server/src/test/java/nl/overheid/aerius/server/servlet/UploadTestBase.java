/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.ArrayUtils;

import nl.overheid.aerius.db.register.RequestTestBase;
import nl.overheid.aerius.server.ExampleUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.user.UserRole;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Base class for upload servlet tests.
 */
class UploadTestBase extends RequestTestBase {
  protected Map<String, Object> attributes;
  protected HttpServletRequest request;
  protected HttpServletResponse response;
  protected TaskManagerClient taskManagerClient;
  private final StringWriter writer = new StringWriter();

  public void before(final HashSet<UserRole> roles) throws SQLException, IOException, AeriusException {
    ExampleUtil.mockSubject(getRegConnection(), roles);
    final HttpSession session = mock(HttpSession.class);
    request = ExampleUtil.mockHttpServletRequest(session);
    attributes = ExampleUtil.mockAttributes(session);
    response = ExampleUtil.mockHttpServletResponse(writer);
    taskManagerClient = ExampleUtil.mockTaskManagerClient();
  }

  /**
   * assert if returned result to be send to client is ok.
   */
  protected void assertResults() {
    final String result = writer.toString();
    assertTrue("Result should be ok:" + ArrayUtils.toString(attributes.values()), result.startsWith(SharedConstants.IMPORT_SUCCESS_PREFIX));
  }
}
