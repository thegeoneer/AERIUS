/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.priorityproject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.developmentspace.PrioritySubProjectReviewInfo;
import nl.overheid.aerius.wui.main.widget.table.IsDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class PrioritySubProjectReviewTable extends Composite implements IsDataTable<PrioritySubProjectReviewInfo> {

  interface PrioritySubProjectReviewTableUiBinder extends UiBinder<Widget, PrioritySubProjectReviewTable> {
  }

  private static final PrioritySubProjectReviewTableUiBinder uiBinder = GWT.create(PrioritySubProjectReviewTableUiBinder.class);

  @UiField SimpleDivTable<PrioritySubProjectReviewInfo> table;

  @UiField(provided = true) TextColumn<PrioritySubProjectReviewInfo> natureAreaColumn;
  @UiField(provided = true) SimpleWidgetFactory<PrioritySubProjectReviewInfo> permitRuleImage;

  public PrioritySubProjectReviewTable() {
    natureAreaColumn = new TextColumn<PrioritySubProjectReviewInfo>() {
      @Override
      public String getValue(final PrioritySubProjectReviewInfo object) {
        return object.getAssessmentArea().getName();
      }
    };
    permitRuleImage = new SimpleWidgetFactory<PrioritySubProjectReviewInfo>() {
      @Override
      public Widget createWidget(final PrioritySubProjectReviewInfo object) {
        return new PrioritySubProjectReviewInfoIndicatorsPanel(object);
      }
    };

    initWidget(uiBinder.createAndBindUi(this));
  }

  @Override
  public SimpleDivTable<PrioritySubProjectReviewInfo> asDataTable() {
    return table;
  }
}
