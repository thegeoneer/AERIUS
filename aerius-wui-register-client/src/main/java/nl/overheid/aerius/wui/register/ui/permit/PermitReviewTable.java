/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.permit;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.developmentspace.PermitReviewInfo;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.IsDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class PermitReviewTable extends Composite implements IsDataTable<PermitReviewInfo> {

  interface PermitReviewTableUiBinder extends UiBinder<Widget, PermitReviewTable> {
  }

  private static final PermitReviewTableUiBinder UI_BINDER = GWT.create(PermitReviewTableUiBinder.class);

  @UiField SimpleDivTable<PermitReviewInfo> table;

  @UiField(provided = true) TextColumn<PermitReviewInfo> natureAreaColumn;
  @UiField(provided = true) TextColumn<PermitReviewInfo> devSpaceAvailableColumn;
  @UiField(provided = true) TextColumn<PermitReviewInfo> devSpaceRequestedColumn;
  @UiField(provided = true) SimpleWidgetFactory<PermitReviewInfo> permitRuleImage;

  private final RegisterUserContext userContext;

  @Inject
  public PermitReviewTable(final RegisterUserContext userContext) {
    this.userContext = userContext;
    natureAreaColumn = new TextColumn<PermitReviewInfo>() {
      @Override
      public String getValue(final PermitReviewInfo object) {
        return object.getAssessmentArea().getName();
      }
    };
    devSpaceAvailableColumn = new TextColumn<PermitReviewInfo>(true) {
      @Override
      public String getValue(final PermitReviewInfo object) {
        return getSpaceString(object.getAvailableExclusive());
      }
    };
    devSpaceRequestedColumn = new TextColumn<PermitReviewInfo>(true) {
      @Override
      public String getValue(final PermitReviewInfo object) {
        return getSpaceString(object.getRequiredSpaceForPermit());
      }
    };
    permitRuleImage = new SimpleWidgetFactory<PermitReviewInfo>() {
      @Override
      public Widget createWidget(final PermitReviewInfo object) {
        return new PermitReviewInfoIndicatorsPanel(object);
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public SimpleDivTable<PermitReviewInfo> asDataTable() {
    return table;
  }

  private String getSpaceString(final double space) {
    return FormatUtil.formatDeposition(space, userContext.getEmissionResultValueDisplaySettings(), 0);
  }
}
