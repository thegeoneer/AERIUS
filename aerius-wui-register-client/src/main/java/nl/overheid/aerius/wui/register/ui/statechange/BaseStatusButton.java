/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.statechange;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.test.TestIDRegister;

public class BaseStatusButton extends Composite {

  public interface BaseStatusHandler {
    boolean onChangeState(RequestState newState);

    void deletePermit();
  }

  interface BaseStatusButtonEventBinder extends EventBinder<BaseStatusButton> {
  }

  private static final BaseStatusButtonEventBinder EVENT_BINDER = GWT.create(BaseStatusButtonEventBinder.class);

  interface BaseStatusButtonUiBinder extends UiBinder<Widget, BaseStatusButton> {
  }

  private static final BaseStatusButtonUiBinder UI_BINDER = GWT.create(BaseStatusButtonUiBinder.class);

  @UiField Button stateButton;
  @UiField FlowPanel additionalButtons;
  @UiField Button deleteButton;

  private final BaseStateChangePanel baseStateChangePanel;
  private final ClickHandler okClickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      if (baseStatusHandler != null && baseStatusHandler.onChangeState(baseStateChangePanel.getSelected())) {
        baseStateChangePanel.hide();
        setValue(baseStateChangePanel.getSelected());
      }
    }
  };
  private BaseStatusHandler baseStatusHandler;

  private final StateChangeFlow stateChangeFlowUtil;

  public BaseStatusButton(final EventBus eventBus, final RegisterUserContext userContext, final StateChangeFlow stateChangeFlowUtil) {
    this.baseStateChangePanel = new BaseStateChangePanel(userContext.getUserProfile(), stateChangeFlowUtil);
    this.stateChangeFlowUtil = stateChangeFlowUtil;
    baseStateChangePanel.setOkClickHandler(okClickHandler);

    initWidget(UI_BINDER.createAndBindUi(this));

    stateButton.ensureDebugId(TestIDRegister.APPLICATION_STATUS_BUTTON);
    deleteButton.ensureDebugId(TestIDRegister.APPLICATION_DELETE_BUTTON);

    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  /**
   * Enable the status button.
   *
   * @param enabled True to enable, false to disable.
   */
  public void enableStatus(final boolean enabled) {
    stateButton.setEnabled(enabled);
  }

  /**
   * Enable the delete button.
   *
   * @param enabled True to enable, false to disable.
   */
  public void enableDelete(final boolean enabled) {
    deleteButton.setEnabled(enabled);
  }

  /**
   * @param baseStatusHandler the baseStatusHandler to set
   */
  public void setBaseStatusHandler(final BaseStatusHandler baseStatusHandler) {
    this.baseStatusHandler = baseStatusHandler;
  }

  @UiHandler("stateButton")
  void onStatusButtonClick(final ClickEvent event) {
    baseStateChangePanel.center();
  }

  @UiHandler("deleteButton")
  void onDeleteButtonClick(final ClickEvent event) {
    if (baseStatusHandler != null) {
      baseStatusHandler.deletePermit();
    }
  }

  public void setValue(final RequestState value) {
    stateButton.setText(stateChangeFlowUtil.getLabel(value));
    baseStateChangePanel.setValue(value);
  }

  public Button addAdditionalButton(final String style, final String debugId, final ClickHandler handler) {
    final Button button = new Button();
    button.addStyleName(style);
    button.ensureDebugId(debugId);
    button.addClickHandler(handler);

    additionalButtons.add(button);

    return button;
  }

  public BaseStateChangePanel getBaseStateChangePanel() {
    return this.baseStateChangePanel;
  }
}
