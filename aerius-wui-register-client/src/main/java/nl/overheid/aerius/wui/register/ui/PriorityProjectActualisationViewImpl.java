/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;

public class PriorityProjectActualisationViewImpl extends Composite implements PriorityProjectActualisationView {
  interface PriorityProjectActualisationViewImplUiBinder extends UiBinder<Widget, PriorityProjectActualisationViewImpl> {
  }

  private static final PriorityProjectActualisationViewImplUiBinder UI_BINDER = GWT.create(PriorityProjectActualisationViewImplUiBinder.class);

  private static final int FACTSHEET_SWITCH_PANEL_FILE_PRESENT = 0;
  private static final int FACTSHEET_SWITCH_PANEL_NO_FILE_PRESENT = 1;

  private static final int ACTUALISATION_SWITCH_PANEL_FILE_PRESENT = 0;
  private static final int ACTUALISATION_SWITCH_PANEL_NO_FILE_PRESENT = 1;

  private final RegisterUserContext userContext;
  private Presenter presenter;
  private PriorityProject priorityProject;

  @UiField Anchor factsheet;
  @UiField SwitchPanel factsheetSwitchPanel;
  @UiField Anchor actualisation;
  @UiField SwitchPanel actualisationSwitchPanel;
  @UiField Button addFactsheetButton;
  @UiField Button replaceFactsheetButton;
  @UiField Button addActualisationButton;
  @UiField Button replaceActualisationButton;
  @UiField Button removeActualisationButton;

  @Inject
  public PriorityProjectActualisationViewImpl(final RegisterUserContext userContext, final EventBus eventBus) {
    this.userContext = userContext;

    initWidget(UI_BINDER.createAndBindUi(this));

    addActualisationButton.ensureDebugId(TestIDRegister.PRIORITYPROJECT_ACTUALIZATION_ADD_BUTTON);
    replaceActualisationButton.ensureDebugId(TestIDRegister.PRIORITYPROJECT_ACTUALIZATION_REPLACE_BUTTON);
    removeActualisationButton.ensureDebugId(TestIDRegister.PRIORITYPROJECT_ACTUALIZATION_DELETE_BUTTON);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public String getTitleText() {
    return M.messages().registerMenuPriorityProjectsTitle();
  }

  @Override
  public void setData(final PriorityProject priorityProject) {
    this.priorityProject = priorityProject;

    final boolean noResults = priorityProject.getRequestState() == RequestState.INITIAL;

    if (priorityProject.hasRequestFile(RequestFileType.PRIORITY_PROJECT_FACTSHEET)) {
      factsheetSwitchPanel.showWidget(FACTSHEET_SWITCH_PANEL_FILE_PRESENT);
      factsheet.setText(getRequestFileName(RequestFileType.PRIORITY_PROJECT_FACTSHEET));
    } else {
      factsheetSwitchPanel.showWidget(FACTSHEET_SWITCH_PANEL_NO_FILE_PRESENT);
    }

    if (priorityProject.hasRequestFile(RequestFileType.PRIORITY_PROJECT_ACTUALISATION)) {
      actualisationSwitchPanel.showWidget(ACTUALISATION_SWITCH_PANEL_FILE_PRESENT);
      actualisation.setText(getRequestFileName(RequestFileType.PRIORITY_PROJECT_ACTUALISATION));
    } else {
      actualisationSwitchPanel.showWidget(ACTUALISATION_SWITCH_PANEL_NO_FILE_PRESENT);
    }

    final boolean canActualise =
        UserProfileUtil.hasPermissionForAuthority(userContext.getUserProfile(),
            priorityProject.getAuthority(), RegisterPermission.ADD_NEW_PRIORITY_PROJECT)
        && !priorityProject.isAssignCompleted();

    addActualisationButton.setEnabled(canActualise);
    replaceActualisationButton.setEnabled(canActualise);
    removeActualisationButton.setEnabled(canActualise && !noResults);

    final boolean canDoFactsheetStuff = canActualise;

    addFactsheetButton.setEnabled(canDoFactsheetStuff);
    replaceFactsheetButton.setEnabled(canDoFactsheetStuff);
  }

  @UiHandler({"addFactsheetButton", "replaceFactsheetButton"})
  public void onAddFactsheetButtonClick(final ClickEvent c) {
    presenter.openFactsheetPriorityProject();
  }

  @UiHandler({"addActualisationButton", "replaceActualisationButton"})
  public void onAddActualisationButtonClick(final ClickEvent c) {
    presenter.openActualisePriorityProject();
  }

  @UiHandler("removeActualisationButton")
  public void onRemoveActualisationClick(final ClickEvent c) {
    presenter.removeActualisation();
  }

  @UiHandler({"actualisationDownloadImage", "actualisation"})
  public void onActualisationDownload(final ClickEvent event) {
    presenter.openRequestFile(priorityProject, RequestFileType.PRIORITY_PROJECT_ACTUALISATION);
  }

  @UiHandler({"factsheetDownloadImage", "factsheet"})
  public void onFactsheetDownload(final ClickEvent event) {
    presenter.openRequestFile(priorityProject, RequestFileType.PRIORITY_PROJECT_FACTSHEET);
  }

  private String getRequestFileName(final RequestFileType requestFileType) {
    final String fileName = priorityProject.getFiles().get(requestFileType).getFileName();
    return fileName == null ? priorityProject.getReference() : fileName;
  }

}
