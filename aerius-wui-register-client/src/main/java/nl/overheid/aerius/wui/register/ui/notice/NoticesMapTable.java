/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.SortableDirection;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeSortableAttribute;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeader;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeader.SortActionHandler;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeaderBinderUtil;
import nl.overheid.aerius.wui.main.widget.table.DateColumn;
import nl.overheid.aerius.wui.main.widget.table.DivTableKeyProviders;
import nl.overheid.aerius.wui.main.widget.table.ImageColumn;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveDivTableRow;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;
import nl.overheid.aerius.wui.register.event.NoticeSelectedEvent;
import nl.overheid.aerius.wui.register.geo.NoticeMarker.NoticeMarkerType;


public class NoticesMapTable extends Composite implements IsInteractiveDataTable<Notice>, SortActionHandler<NoticeSortableAttribute> {
  interface NoticesMapTableUiBinder extends UiBinder<Widget, NoticesMapTable> {}

  private static NoticesMapTableUiBinder UI_BINDER = GWT.create(NoticesMapTableUiBinder.class);

  @UiField(provided = true) SimpleInteractiveClickDivTable<Notice> table;

  @UiField ColumnSortHeader<NoticeSortableAttribute> iconHeader;
  @UiField ColumnSortHeader<NoticeSortableAttribute> dateHeader;
  @UiField ColumnSortHeader<NoticeSortableAttribute> referenceHeader;
  @UiField ColumnSortHeader<NoticeSortableAttribute> corporationHeader;

  @UiField(provided = true) ImageColumn<Notice> iconColumn;
  @UiField(provided = true) DateColumn<Notice> dateColumn;
  @UiField(provided = true) TextColumn<Notice> referenceColumn;
  @UiField(provided = true) TextColumn<Notice> corporationColumn;

  private SortActionHandler<NoticeSortableAttribute> handler;

  public NoticesMapTable(final EventBus eventBus) {
    table = new SimpleInteractiveClickDivTable<Notice>() {
      @Override
      public void applyRowOptions(final SimpleInteractiveDivTableRow row, final Notice item) {
        super.applyRowOptions(row, item);
        row.addDomHandler(new MouseOverHandler() {
          @Override
          public void onMouseOver(final MouseOverEvent event) {
            eventBus.fireEvent(new NoticeSelectedEvent(item, NoticeMarkerType.HIGHLIGHT));
          }
        }, MouseOverEvent.getType());
      }
    };

    iconColumn = new ImageColumn<Notice>() {
      @Override
      public ImageResource getValue(final Notice notice) {
        return SectorImageUtil.getSectorImageResource(notice.getSectorIcon());
      }
      @Override
      public void applyRowOptions(final Widget row, final com.google.gwt.user.client.ui.Image item) {
        super.applyRowOptions(row, item);

      }
    };

    dateColumn = new DateColumn<Notice>() {
      @Override
      public Date getValue(final Notice notice) {
        return notice.getReceivedDate();
      }
    };

    referenceColumn = new TextColumn<Notice>() {
      @Override
      public String getValue(final Notice notice) {
        return notice.getReference();
      }
    };

    corporationColumn = new TextColumn<Notice>() {
      @Override
      public String getValue(final Notice notice) {
        return notice.getScenarioMetaData().getCorporation();
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    iconHeader.setHandler(this);
    dateHeader.setHandler(this);
    referenceHeader.setHandler(this);
    corporationHeader.setHandler(this);

    ColumnSortHeaderBinderUtil.connect(iconHeader, dateHeader, referenceHeader, corporationHeader);

    DivTableKeyProviders.autoConfigure(Notice.class, table);

    table.setSelectionModel(new SingleSelectionModel<Notice>());
    table.setLoadingByDefault(true);
  }

  @Override
  public SimpleInteractiveClickDivTable<Notice> asDataTable() {
    return table;
  }

  @Override
  public void setSorting(final NoticeSortableAttribute attribute, final SortableDirection dir) {
    if (handler != null) {
      handler.setSorting(attribute, dir);
    }
  }

  public void setSortHandler(final SortActionHandler<NoticeSortableAttribute> handler) {
    this.handler = handler;
  }
}
