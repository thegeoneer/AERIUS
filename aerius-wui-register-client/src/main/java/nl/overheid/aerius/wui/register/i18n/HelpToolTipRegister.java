/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.i18n;

import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.i18n.HelpManualUrl;
import nl.overheid.aerius.shared.i18n.HelpToolTipInfo;

/**
 * HelpTool tips for Register with id's to manual.
 */
public interface HelpToolTipRegister extends HelpToolTipInfo {
  int DASHBOARD_PERMIT_HEADING = 2336;
  int DASHBOARD_NOTICE_HEADING = 2367;

  @Override
  @HelpManualUrl(DASHBOARD_PERMIT_HEADING)
  HelpInfo ttRegisterDashboardPermitHeading();

  @Override
  @HelpManualUrl(DASHBOARD_NOTICE_HEADING)
  HelpInfo ttRegisterDashboardNoticeHeading();


}
