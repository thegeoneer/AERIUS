/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.wui.register.place.PermitPlace;
import nl.overheid.aerius.wui.register.ui.permit.PermitStatusButton.PermitStatusHandler;

/**
 * Base interface for the Permit detail page.
 */
public interface PermitDetailView extends IsWidget, HasTitle {
  /**
   * Presenter for the {@link PermitDetailView}.
   */
  interface Presenter extends PermitStatusHandler {
    void openRequestFile(Request request);

    void showPostCalculatorDialog();
  }

  /**
   * Set the presenter for the view.
   *
   * @param presenter Presenter to use
   */
  void setPresenter(Presenter presenter);

  /**
   * Whether to enable the status dropdown.
   * @param enabled enable on true, disable on false
   */
  void enableStatus(boolean enabled);

  /**
   * Whether to enable the delete button.
   * @param enabled enable on true, disable on false
   */
  void enableDelete(boolean enabled);

  /**
   * Whether to enable the mark button.
   * @param enabled enable on true, disable on false
   */
  void enableMark(boolean enabled);

  /**
   * Lock the interactive user interface elements until the operation finished.
   * @param lock if true lock
   */
  void setLocked(boolean lock);

  /**
   * Set the permit object.
   * @param permit permit
   */
  void setPermit(Permit permit);

  /**
   * Set the content panel.
   * @param contentPanel content panel.
   */
  void setContentPanel(IsWidget contentPanel);

  /**
   * Set the current place.
   * @param place place
   */
  void setPlace(PermitPlace place);
}
