/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PermitRequestPlace;
import nl.overheid.aerius.wui.register.ui.PermitDetailRequestViewImpl.PermitDetailRequestViewDriver;
import nl.overheid.aerius.wui.register.ui.calculator.CalculatorExportController;

/**
 *
 */
public class PermitDetailRequestActivity extends PermitDetailBaseActivity {

  private final PermitDetailRequestViewDriver driver = GWT.create(PermitDetailRequestViewDriver.class);
  private CalculatorExportController exportController;
  private Permit permit;

  @Inject
  public PermitDetailRequestActivity(final RegisterAppContext appContext, final PermitDetailView view, final PermitDetailRequestViewImpl contentPanel,
      @Assisted final PermitRequestPlace place) {
    super(appContext, view, contentPanel, place);

    contentPanel.setPresenter(this);

    driver.initialize(contentPanel);
  }

  @Override
  protected void onStart() {
    exportController = new CalculatorExportController(appContext.getContext(), eventBus) {

      @Override
      protected void doServiceCall(final AsyncCallback<ScenarioGMLs> callback) {
        appContext.getPermitService().getGML(permit.getPermitKey(), callback);
      }
    };

    super.onStart();
  }

  @Override
  protected void onPermitLoaded(final Permit permit) {
    this.permit = permit;

    driver.edit(permit);
  }

  @Override
  public void showPostCalculatorDialog() {
    exportController.showPostCalculatorDialog();
  }

}
