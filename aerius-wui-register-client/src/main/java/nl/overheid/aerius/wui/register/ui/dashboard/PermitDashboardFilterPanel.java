/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.dashboard;

import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.dashboard.PermitDashboardFilter;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

public class PermitDashboardFilterPanel extends DefaultDashboardFilterPanel<PermitDashboardFilter> {

  public PermitDashboardFilterPanel(final RegisterContext context, final RegisterUserContext userContext, final HelpPopupController hpC) {
    super(context, userContext);
    hpC.addWidget(provincesList, hpC.tt().ttRegisterDashboardFilterProvince());
    provincesList.ensureDebugId(TestIDRegister.DASHBOARD_PERMIT_PROVINCE_FILTER);
  }

  @Override
  protected PermitDashboardFilter createFilter() {
    return new PermitDashboardFilter();
  }

}
