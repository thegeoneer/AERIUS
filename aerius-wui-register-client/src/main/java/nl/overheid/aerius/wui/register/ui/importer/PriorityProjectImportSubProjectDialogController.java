/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.importer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.exception.ImportDuplicateEntryException;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectDossierDetailPlace;

public abstract class PriorityProjectImportSubProjectDialogController extends RegisterImportDialogController<PrioritySubProjectKey> {

  private static class PrioritySubProjectImportPollingAgent extends RequestImportPollingAgent<PrioritySubProjectKey> {

    PrioritySubProjectImportPollingAgent(final RegisterRetrieveImportServiceAsync service) {
      super(service);
    }

    @Override
    protected void callService(final String key, final AsyncCallback<PrioritySubProjectKey> resultCallback) {
      service.getPrioritySubProjectResult(key, resultCallback);
    }
  }

  private static final String UPLOAD_ACTION = GWT.getModuleBaseURL() + SharedConstants.IMPORT_PP_SUBPROJECT_SERVLET;

  public PriorityProjectImportSubProjectDialogController(final PlaceController placeController, final RegisterRetrieveImportServiceAsync service,
      final RegisterImportDialogPanel<PrioritySubProjectKey> content, final String reference) {
    super(placeController, content, UPLOAD_ACTION, new PrioritySubProjectImportPollingAgent(service));

    content.setId(reference);
  }

  @Override
  public void handleDuplicate(final ImportDuplicateEntryException duplicateException) {
    final PrioritySubProjectKey duplicateKey = (PrioritySubProjectKey) duplicateException.getKey();

    setDuplicateInfo(M.messages().registerImportOpenDuplicateLabelSubProject(duplicateKey.getReference()), getPlace(duplicateKey));
  }

  protected PriorityProjectProjectDossierDetailPlace getPlace(final PrioritySubProjectKey key) {
    return new PriorityProjectProjectDossierDetailPlace(key);
  }
}
