/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;
import java.util.Arrays;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.developmentspace.PrioritySubProjectReviewInfo;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectDossierReviewPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectPlace;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierBaseView.PriorityProjectDossierDetailViewType;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectExportDialogController;

public class PriorityProjectProjectDossierReviewActivity extends PriorityProjectProjectDossierBaseActivity<PriorityProjectProjectDossierReviewPlace>
implements PriorityProjectProjectDossierReviewView.Presenter {

  private final PriorityProjectProjectDossierReviewView view;

  @Inject
  public PriorityProjectProjectDossierReviewActivity(@Assisted final PriorityProjectProjectDossierReviewPlace place,
      final RegisterAppContext appContext, final PriorityProjectBaseView baseView, final PriorityProjectProjectDossierBaseView subBaseView,
      final PriorityProjectProjectDossierReviewView view, final PriorityProjectExportDialogController exportDialogController) {
    super(place, appContext, baseView, subBaseView, PriorityProjectDossierDetailViewType.REVIEW, exportDialogController);
    this.view = view;
  }

  @Override
  protected void startSubChild(final AcceptsOneWidget panel) {
    view.setPresenter(this);
    panel.setWidget(view);
  }

  @Override
  protected void onPriorityProjectLoaded(final PriorityProject priorityProject) {
    final PrioritySubProject subProject = determinePrioritySubProject(priorityProject);
    onPrioritySubProjectLoaded(subProject);
    if (Arrays.asList(RequestState.PRIORITY_SUBPROJECT_REVIEW_VALUES).contains(subProject.getRequestState())) {
      appContext.getPriorityProjectService().getPrioritySubProjectReviewInfo(currentPlace.getSubKey(), reviewCallback);
    } else {
      final String text;
      switch (subProject.getRequestState()) {
      case INITIAL:
        text = M.messages().registerPriorityProjectProjectDossierReviewWaitingForCalculation();
        break;
      default:
        text = M.messages().registerPriorityProjectProjectDossierReviewUnavailable(subProject.getRequestState());
        break;
      }
      view.hideReviewInfo(text);
    }
  }

  private final AsyncCallback<ArrayList<PrioritySubProjectReviewInfo>> reviewCallback = new AppAsyncCallback<ArrayList<PrioritySubProjectReviewInfo>>() {
    @Override
    public void onSuccess(final ArrayList<PrioritySubProjectReviewInfo> result) {
      view.setReviewInfo(result);
    }

    @Override
    public void onFailure(final Throwable caught) {
      super.onFailure(caught);
      if (caught instanceof AeriusException) {
        final AeriusException ae = (AeriusException) caught;
        if (Reason.PRIORITY_SUBPROJECT_UNKNOWN.equals(ae.getReason())) {
          // Return to the parent priority project
          goTo(new PriorityProjectProjectPlace(currentPlace.getKey()));
        }
      }
    }
  };

}
