/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.importer;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.exception.ImportDuplicateEntryException;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBox;
import nl.overheid.aerius.wui.main.ui.importer.ImportDialogPanel;
import nl.overheid.aerius.wui.main.ui.importer.ImportProperties;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.widget.LabelledWidget;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;

// Just an Editor because the ConfirmCancelDialog needs us to be, but don't create a driver/use @Ignore and such. Be fake. Be proud.
public class RegisterImportDialogPanel<K> extends Composite implements ImportDialogPanel<ImportProperties>, Editor<ImportProperties> {

  interface RegisterImportDialogPanelUiBinder extends UiBinder<Widget, RegisterImportDialogPanel<?>> {
  }

  private static RegisterImportDialogPanelUiBinder UI_BINDER = GWT.create(RegisterImportDialogPanelUiBinder.class);

  interface RequestHandler<K> {
    void handleResult(K key);
    void handleDuplicate(ImportDuplicateEntryException duplicateException);
    void openDuplicate();
  }

  public interface CustomStyle extends CssResource {
    String explanationText();
  }

  @UiField CustomStyle style;

  @UiField LabelledWidget dossierPanel;

  @UiField Label subTitleDiv;
  @UiField TextValueBox idField;
  @UiField FileUpload fileField;
  @UiField Label errorField;

  @UiField FlowPanel additionalFields;

  @UiField FlowPanel duplicateErrorDiv;
  @UiField Label duplicateErrorText;
  @UiField Button openDuplicate;

  private RequestHandler<K> requestHandler;

  public RegisterImportDialogPanel(final SupportedUploadFormat supportedFormats) {
    initWidget(UI_BINDER.createAndBindUi(this));

    final ArrayList<String> supportedFormatList = new ArrayList<>();
    for (final ImportType supportedFormat : supportedFormats.getSupported()) {
      supportedFormatList.add(supportedFormat.getFileFormat().name()); // we need it uppercase, so let's use the name directly
    }

    subTitleDiv.setText(M.messages().registerImportSubTitle(supportedFormatList));
    fileField.setName(SharedConstants.IMPORT_FILE_FIELD_NAME);
    fileField.ensureDebugId(TestID.INPUT_IMPORTFILE);
    errorField.ensureDebugId(TestID.DIV_IMPORT_ERRORS);
    idField.ensureDebugId(TestIDRegister.INPUT_DOSSIER_ID);

    duplicateErrorText.ensureDebugId(TestIDRegister.DIV_IMPORT_DUPLICATE_ERRORS);
  }

  private void addValidator(final ValidatedValueBox<String> field, final LabelledWidget parent, final String validationText) {
    field.addValueChangeHandler(new ValueChangeHandler<String>() {
      @Override
      public void onValueChange(final ValueChangeEvent<String> event) {
        ErrorPopupController.clearError(field);
      }
    });
    field.addValidator(new Validator<String>() {
      @Override
      public String validate(final String value) {
        return parent.isVisible() && field.getText().isEmpty() && value == null ? validationText : null;
      }
    });
  }

  public void showError(final String error) {
    errorField.setText(error);
  }

  public void addFileUploadChangedHandler(final ChangeHandler changeHandler) {
    fileField.addChangeHandler(changeHandler);
  }

  public void showIdField(final boolean showIdField, final String idLabelString, final String idFieldValidationText) {
    dossierPanel.setLabel(idLabelString);
    dossierPanel.setVisible(showIdField);

    if (showIdField) {
      addValidator(idField, dossierPanel, idFieldValidationText);
    }
  }

  private LabelledWidget addAdditionalField(final boolean showField, final String name, final String labelString, final String fieldValidationText,
      final Widget inputWidget) {
    final LabelledWidget widget = new LabelledWidget();
    widget.setLabel(labelString);
    widget.setVisible(showField);
    widget.addStyleName(R.css().calculatorRow());
    widget.add(inputWidget);
    inputWidget.ensureDebugId(TestID.concat(TestIDRegister.INPUT_IMPORT_ADDITIONAL_FIELD, name));

    if (fieldValidationText != null && inputWidget instanceof ValidatedValueBox<?>) {
      addValidator((ValidatedValueBox<String>) inputWidget, widget, fieldValidationText);
    }
    additionalFields.add(widget);

    return widget;
  }

  public TextValueBox addAdditionalTextField(final boolean showField, final String name, final String labelString, final String fieldValidationText,
      final String value) {
    final TextValueBox valueBox = new TextValueBox(name);
    valueBox.setName(name);
    valueBox.setValue(value);
    addAdditionalField(showField, name, labelString, fieldValidationText, valueBox);

    return valueBox;
  }

  public DateBox addAdditionalDateField(final boolean showField, final String name, final String labelString) {
    final DateBox dateBox = new DateBox();
    dateBox.addStyleName(R.css().sourceDetailCol2Var());
    dateBox.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(M.messages().dateTimeFormat())));
    dateBox.getDatePicker().setYearArrowsVisible(true);
    dateBox.getPopup().getElement().getStyle().setZIndex(R.css().zIndexGeneric());
    dateBox.getTextBox().setName(name);
    addAdditionalField(showField, name, labelString, null, dateBox);

    return dateBox;
  }

  public void addAdditionalExplanationText(final String explanation) {
    final Label explanationLabel = new Label(explanation);
    explanationLabel.addStyleName(R.css().calculatorRow());
    explanationLabel.addStyleName(style.explanationText());
    additionalFields.add(explanationLabel);
  }

  public String getDossierId() {
    return idField.getValue();
  }

  public String getFileUploadFilename() {
    return fileField.getFilename();
  }

  public boolean isFileUploadPresent() {
    return fileField.getFilename() != null && !fileField.getFilename().isEmpty();
  }

  @UiHandler("openDuplicate")
  void onOpenDuplicateClick(final ClickEvent e) {
    if (requestHandler != null) {
      requestHandler.openDuplicate();
    }
  }

  public void setRequestHandler(final RequestHandler<K> requestHandler) {
    this.requestHandler = requestHandler;
  }

  /**
   * Set the sub title of the panel.
   *
   * @param subTitle
   *          sub title text
   */
  public void setSubTitle(final String subTitle) {
    subTitleDiv.setText(subTitle);
  }

  public void showDuplicate(final String error, final boolean allowOpenDuplicate) {
    subTitleDiv.setVisible(error == null);
    duplicateErrorDiv.setVisible(error != null);
    openDuplicate.setVisible(error != null && allowOpenDuplicate);

    if (error != null) {
      showError(null);
      duplicateErrorText.setText(error);
    }
  }

  public void showIdMissing(final AeriusException error) {
    idField.asEditor().validate();
  }

  public void setId(final String id) {
    idField.setValue(id);
  }

}
