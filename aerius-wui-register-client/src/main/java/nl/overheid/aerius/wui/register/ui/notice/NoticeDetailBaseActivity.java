/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeKey;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.wui.admin.ui.AdminAbstractActivity;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.ContentLoadingWidget;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.NoticeDossierPlace;
import nl.overheid.aerius.wui.register.place.NoticeMapPlace;
import nl.overheid.aerius.wui.register.place.NoticeOverviewPlace;
import nl.overheid.aerius.wui.register.place.NoticePlace;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDeleteDialogController.DeleteHandler;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDetailView.NoticeViewType;


/**
 * Base Activity for detail of melding.
 */
public abstract class NoticeDetailBaseActivity extends AdminAbstractActivity<RegisterAppContext>
    implements NoticeDetailView.Presenter, AsyncCallback<Notice> {

  private final NoticeDetailView view;
  private final NoticeDeleteDialogController deleteDialogController;
  private final IsWidget contentPanel;
  private final NoticePlace currentPlace;

  protected Notice notice;

  private AcceptsOneWidget panel;

  private final AsyncCallback<Void> deleteDossierCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      broadcastNotificationMessage(M.messages().registerNotificationNoticeDeleted(notice.getReference()));
      goTo(new NoticeOverviewPlace());
      setLocked(false);

    }

    @Override
    public void onFailure(final Throwable caught) {
      NoticeDetailBaseActivity.this.onFailure(caught);
      super.onFailure(caught);
      setLocked(false);
    }
  };

  @Inject
  public NoticeDetailBaseActivity(final RegisterAppContext appContext, final NoticeDetailView view, final IsWidget contentPanel,
      final NoticePlace noticePlace, final NoticeDeleteDialogController deleteDialogController) {
    super(appContext, RegisterPermission.VIEW_PRONOUNCEMENTS);

    this.view = view;
    this.deleteDialogController = deleteDialogController;
    this.contentPanel = contentPanel;
    this.currentPlace = noticePlace;

    deleteDialogController.setDeleteHandler(new DeleteHandler() {
      @Override
      public void deleteNotice() {
        setLocked(true);
        appContext.getNoticeService().deleteNotice(notice.getId(), deleteDossierCallback);
      }
    });
  }

  @Override
  public void start(final AcceptsOneWidget panel) {
    this.panel = panel;

    view.setPresenter(this);
    view.setContentPanel(contentPanel);
    view.setPlace(currentPlace);
    panel.setWidget(view);
    onStart();
    refresh();
  }

  protected void onStart() {
    // no-op
  }

  protected void refresh() {
    panel.setWidget(new ContentLoadingWidget());
    appContext.getNotice(currentPlace.getKey(), this);

  }

  @Override
  public void onSuccess(final Notice notice) {
    this.notice = notice;
    panel.setWidget(view);
    view.setNotice(notice);
    onNoticeLoaded(notice);
    deleteDialogController.setReference(notice.getReference());
    setLocked(false);
  }

  /**
   * Called when the notice is loaded.
   * @param notice notice
   */
  protected abstract void onNoticeLoaded(final Notice notice);

  @Override
  public void onFailure(final Throwable caught) {
    setLocked(false);
    refresh();
  }

  protected void setLocked(final boolean lock) {
    view.setLocked(lock);
    updatePermissionStates(lock);
  }

  private void updatePermissionStates(final boolean lock) {
    final boolean permissionDelete = UserProfileUtil.hasPermission(appContext.getUserContext().getUserProfile(),
        RegisterPermission.DELETE_PRONOUNCEMENT);
    view.enableDelete(!lock && permissionDelete);
  }

  @Override
  public void deleteNotice() {
    deleteDialogController.show(currentPlace);
  }

  @Override
  public void switchView(final NoticeViewType value) {
    goTo(getPlace(value));
  }

  private Place getPlace(final NoticeViewType value) {
    if (value == NoticeViewType.MAP) {
      return new NoticeMapPlace(new NoticeKey(notice.getReference()));
    } else {
      return new NoticeDossierPlace(new NoticeKey(notice.getReference()));
    }
  }

  @Override
  public void onNoticeSelection(final Notice notice) {
    goTo(new NoticeDossierPlace(new NoticeKey(notice.getReference())));
  }

}
