/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitSortableAttribute;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeader.SortActionHandler;
import nl.overheid.aerius.wui.main.widget.table.DataTableFetcher;
import nl.overheid.aerius.wui.main.widget.table.GridScrollVisitor;
import nl.overheid.aerius.wui.register.widget.ExportRequests.ExportRequestsHandler;


/**
 *
 */
public interface PermitOverviewView extends IsWidget, HasTitle {

  interface Presenter extends DataTableFetcher, SortActionHandler<PermitSortableAttribute>, ExportRequestsHandler {

    /**
     * Enter a new application
     */
    void onNewApplication();

    /**
     * Adapt the filter used to display applications.
     */
    void onAdaptFilter();

    void onPermitSelection(Permit permit);

    void onRefresh();
  }

  void setPresenter(Presenter presenter);

  void setDataVisitor(GridScrollVisitor gridScrollVisitor);

  void updateData(ArrayList<Permit> result);

  void setData(ArrayList<Permit> result);

  /**
   * Set loading state.
   * @param loading loading state
   * @param hard clear all the data.
   */
  void setLoading(boolean loading, boolean hard);

  void updateExportResult(RequestExportResult requestExportResult);

}
