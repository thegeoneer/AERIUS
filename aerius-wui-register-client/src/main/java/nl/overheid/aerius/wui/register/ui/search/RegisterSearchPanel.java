/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.search;

import java.util.ArrayList;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestionType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.search.SearchPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

@Singleton
public class RegisterSearchPanel extends SearchPanel<RegisterSearchSuggestionType, RegisterSearchSuggestion, RegisterSearchAction> {

  @Inject
  public RegisterSearchPanel(final EventBus eventBus, final HelpPopupController hpC, final RegisterUserContext userContext,
      final RegisterSearchTable searchTable, final RegisterSearchAction searchAction) {
    super(eventBus, hpC, searchTable, searchAction);

    setPlaceHolder(M.messages().searchWidgetRegisterPlaceHolder(getSearchTypesBasedOnPermission(userContext.getUserProfile())));
  }

  private ArrayList<String> getSearchTypesBasedOnPermission(final UserProfile userProfile) {
    final ArrayList<String> searchTypes = new ArrayList<>();
    for (final RegisterSearchSuggestionType type : RegisterSearchSuggestionType.values()) {
      if (UserProfileUtil.hasPermission(userProfile, type.getPermission())) {
        searchTypes.add(M.messages().searchSuggestionType(type.name()));
      }
    }

    return searchTypes;
  }
}
