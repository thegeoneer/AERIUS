/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.importer;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.exception.ImportDuplicateEntryException;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierPlace;

public abstract class PriorityProjectImportProjectDialogController extends RegisterImportDialogController<PriorityProjectKey> {

  private static class PriorityProjectImportPollingAgent extends RequestImportPollingAgent<PriorityProjectKey> {

    PriorityProjectImportPollingAgent(final RegisterRetrieveImportServiceAsync service) {
      super(service);
    }

    @Override
    protected void callService(final String key, final AsyncCallback<PriorityProjectKey> resultCallback) {
      service.getPriorityProjectResult(key, resultCallback);
    }
  }

  private static final String UPLOAD_ACTION = GWT.getModuleBaseURL() + SharedConstants.IMPORT_PRIORITYPROJECT_SERVLET;

  public PriorityProjectImportProjectDialogController(final RequestFileType requestFileType, final PlaceController placeController,
      final RegisterRetrieveImportServiceAsync service, final RegisterImportDialogPanel<PriorityProjectKey> content) {
    super(placeController, content, UPLOAD_ACTION, new PriorityProjectImportPollingAgent(service), getAdditionalFormParams(requestFileType));

    content.showIdField(true,
        M.messages().registerPriorityProjectDossierId(), M.messages().ivRegisterPriorityProjectDossierIdRequired());
  }

  @Override
  public void handleDuplicate(final ImportDuplicateEntryException duplicateException) {
    final PriorityProjectKey duplicateKey = (PriorityProjectKey) duplicateException.getKey();

    setDuplicateInfo(M.messages().registerImportOpenDuplicateLabelPriorityProject(
        duplicateKey.getDossierId()),
        new PriorityProjectDossierPlace(duplicateKey));
  }

  protected PriorityProjectDossierPlace getPlace(final PriorityProjectKey key) {
    return new PriorityProjectDossierPlace(key);
  }

  private static HashMap<String, String> getAdditionalFormParams(final RequestFileType requestFileType) {
    final HashMap<String, String> additionalFormParams = new HashMap<>();

    additionalFormParams.put(SharedConstants.FORM_POST_TYPE, requestFileType.name());

    return additionalFormParams;
  }

}
