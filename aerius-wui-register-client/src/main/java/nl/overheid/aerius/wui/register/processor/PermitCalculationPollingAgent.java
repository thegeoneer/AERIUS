/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.processor;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.service.PermitServiceAsync;
import nl.overheid.aerius.wui.main.retrievers.PollingAgentImpl;

/**
 * Poll for permit calculation status updates.
 */
@Singleton
public class PermitCalculationPollingAgent extends PollingAgentImpl<PermitKey, PermitKey>  {
  private static final int POLL_WAIT_TIME = 30000;
  private final PermitServiceAsync service;

  @Inject
  public PermitCalculationPollingAgent(final PermitServiceAsync service) {
    super(POLL_WAIT_TIME);
    this.service = service;
  }

  @Override
  protected void callService(final PermitKey key, final AsyncCallback<PermitKey> resultCallback) {
    service.isCalculationFinished(key, resultCallback);
  }
}
