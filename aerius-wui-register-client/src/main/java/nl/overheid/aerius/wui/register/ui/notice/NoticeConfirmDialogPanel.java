/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 *
 */
public class NoticeConfirmDialogPanel extends Composite implements IsWidget {

  interface NoticeConfirmDialogPanelUiBinder extends UiBinder<Widget, NoticeConfirmDialogPanel> {}

  private static NoticeConfirmDialogPanelUiBinder uiBinder = GWT.create(NoticeConfirmDialogPanelUiBinder.class);

  @UiField Label explanation;
  @UiField Label amountToConfirm;

  public NoticeConfirmDialogPanel() {
    initWidget(uiBinder.createAndBindUi(this));
    explanation.setText(M.messages().registerNoticeConfirmPanelBatchExplanation(
        FormatUtil.getDefaultDateFormatter().format(new Date())));
    setAmountToConfirm(0);
  }

  public void setAmountToConfirm(final int amountToConfirm) {
    this.amountToConfirm.setText(M.messages().registerNoticeConfirmPanelBatchAmount(amountToConfirm));
  }

}
