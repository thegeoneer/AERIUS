/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.developmentspace.PrioritySubProjectReviewInfo;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.wui.main.domain.DevelopmentSpaceUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.register.ui.priorityproject.PrioritySubProjectReviewTable;

public class PriorityProjectProjectDossierReviewViewImpl extends Composite implements PriorityProjectProjectDossierReviewView {

  interface PriorityProjectProjectDossierReviewViewImplUiBinder extends UiBinder<Widget, PriorityProjectProjectDossierReviewViewImpl> {
  }

  private static final PriorityProjectProjectDossierReviewViewImplUiBinder uiBinder = GWT
      .create(PriorityProjectProjectDossierReviewViewImplUiBinder.class);

  @SuppressWarnings("unused") private Presenter presenter;
  @SuppressWarnings("unused") private PrioritySubProject prioritySubProject;

  @UiField Label noReviewLabel;
  @UiField FlowPanel reviewPanel;

  @UiField PrioritySubProjectReviewTable reviewTable;

  @UiField Image iconInsufficientOR;
  @UiField Label labelInsufficientOR;
  @UiField Image iconSufficientOR;
  @UiField Label labelSufficientOR;

  @Inject
  public PriorityProjectProjectDossierReviewViewImpl(final RegisterUserContext userContext) {
    initWidget(uiBinder.createAndBindUi(this));

    iconInsufficientOR.setResource(DevelopmentSpaceUtil.getDevelopmentRuleIcon(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK));
    iconSufficientOR.setResource(DevelopmentSpaceUtil.getDevelopmentRuleIcon(DevelopmentRule.EXCEEDING_SPACE_CHECK));

    labelInsufficientOR.setText(M.messages().registerReviewLegendDevelopmentRule(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK));
    labelSufficientOR.setText(M.messages().registerReviewLegendDevelopmentRule(DevelopmentRule.EXCEEDING_SPACE_CHECK));
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setReviewInfo(final ArrayList<PrioritySubProjectReviewInfo> reviewInfo) {
    reviewTable.asDataTable().setRowData(reviewInfo);
    setReviewInfoVisible(true);
  }

  @Override
  public void hideReviewInfo(final String text) {
    noReviewLabel.setText(text);
    setReviewInfoVisible(false);
  }

  private void setReviewInfoVisible(final boolean visible) {
    noReviewLabel.setVisible(!visible);
    reviewPanel.setVisible(visible);
  }

}
