/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.filter;

import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.register.PriorityProjectFilter;
import nl.overheid.aerius.shared.domain.register.PriorityProjectProgress;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.filter.BaseFilterPanel;
import nl.overheid.aerius.wui.main.widget.MultiSelectListBox;
import nl.overheid.aerius.wui.main.widget.SelectImageOptionFactory;
import nl.overheid.aerius.wui.main.widget.SelectOption;
import nl.overheid.aerius.wui.main.widget.SelectScalableImageOptionFactory;
import nl.overheid.aerius.wui.main.widget.SelectTextOptionFactory;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;
import nl.overheid.aerius.wui.register.util.PriorityProjectProgressUtil;

public class PriorityProjectFilterPanel extends BaseFilterPanel<PriorityProjectFilter> {
  interface PriorityProjectFilterPanelUiBinder extends UiBinder<Widget, PriorityProjectFilterPanel> {}

  private static final PriorityProjectFilterPanelUiBinder UI_BINDER = GWT.create(PriorityProjectFilterPanelUiBinder.class);

  private static final int PROGRESS_COLUMN_IMAGE_WIDTH = 31;

  @UiField MultiSelectListBox<PriorityProjectProgress> progressListBox;
  @UiField(provided = true) WidgetFactory<PriorityProjectProgress, SelectOption<PriorityProjectProgress>> progressOptionTemplate =
      new SelectScalableImageOptionFactory<PriorityProjectProgress>(PROGRESS_COLUMN_IMAGE_WIDTH) {
    @Override
    public String getItemText(final PriorityProjectProgress value) {
      return M.messages().registerPriorityProjectProgressState(value);
    }

    @Override
    public DataResource getDataResource(final PriorityProjectProgress state) {
      return PriorityProjectProgressUtil.getPriorityProjectProgressIcon(state);
    }
  };

  @UiField MultiSelectListBox<Authority> authorityListBox;
  @UiField(provided = true) WidgetFactory<Authority, SelectOption<Authority>> authorityOptionTemplate = new SelectTextOptionFactory<Authority>() {
    @Override
    public String getItemText(final Authority value) {
      return value.getDescription();
    }
  };

  @UiField MultiSelectListBox<AssessmentArea> natureAreaListBox;
  @UiField(provided = true) WidgetFactory<AssessmentArea, SelectOption<AssessmentArea>> natureAreaOptionTemplate =
      new SelectTextOptionFactory<AssessmentArea>() {
    @Override
    public String getItemText(final AssessmentArea value) {
      return value.getName();
    }
  };

  @UiField MultiSelectListBox<Sector> sectorListBox;
  @UiField(provided = true) WidgetFactory<Sector, SelectOption<Sector>> sectorOptionTemplate = new SelectImageOptionFactory<Sector>() {
    @Override
    public String getItemText(final Sector value) {
      return value.getName();
    }

    @Override
    public ImageResource getImageResource(final Sector state) {
      return SectorImageUtil.getSectorImageResource(state.getProperties().getIcon());
    }
  };

  @Inject
  public PriorityProjectFilterPanel(final RegisterContext context, final RegisterUserContext userContext) {
    initWidget(UI_BINDER.createAndBindUi(this));

    progressListBox.setSingleSelectionMode(true);

    progressListBox.setValues(PriorityProjectProgress.values());
    authorityListBox.setValues(context.getAuthorities());
    sectorListBox.setValues(context.getCategories().getSectors());
    natureAreaListBox.setValues(context.getNatura2kAreas());
  }

  @Override
  protected String getTitleLabel() {
    final String progress = progressListBox.getHeaderText(progressListBox.getSelectedValues());
    final String authority = authorityListBox.getHeaderText(authorityListBox.getSelectedValues());
    final String sector = sectorListBox.getHeaderText(sectorListBox.getSelectedValues());
    final String areas = natureAreaListBox.getHeaderText(natureAreaListBox.getSelectedValues());

    return M.messages().registerPriorityProjectFilterCollapseTitle(progress, authority, areas, sector);
  }

  @Override
  public void setValue(final PriorityProjectFilter filter) {
    final PriorityProjectProgress progressState = filter.getProgressState();
    progressListBox.setSelectedValue(progressState, true);
    authorityListBox.setValue(filter.getAuthorities());
    sectorListBox.setValue(filter.getSectors());
    natureAreaListBox.setValue(filter.getAssessmentAreas());

    super.setValue(filter);
  }

  @Override
  protected void updateFilter() {
    final HashSet<PriorityProjectProgress> selectedValues = progressListBox.getSelectedValues();
    value.setProgressState(selectedValues.isEmpty() ? null : selectedValues.iterator().next());

    value.setAuthorities(authorityListBox.getValue());
    value.setSectors(sectorListBox.getValue());
    value.setAssessmentAreas(natureAreaListBox.getValue());
  }

}
