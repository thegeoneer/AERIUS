/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectDossierEditor;

public class PriorityProjectDossierEditViewImpl extends Composite implements PriorityProjectDossierEditView {
  interface PriorityProjectDossierEditViewImplUiBinder extends UiBinder<Widget, PriorityProjectDossierEditViewImpl> {}

  private static final PriorityProjectDossierEditViewImplUiBinder UI_BINDER = GWT.create(PriorityProjectDossierEditViewImplUiBinder.class);

  public interface PriorityProjectDossierEditViewDriver extends SimpleBeanEditorDriver<PriorityProject, PriorityProjectDossierEditor> {}

  @UiField PriorityProjectDossierEditor dossierEditor;

  private Presenter presenter;

  @Inject
  public PriorityProjectDossierEditViewImpl(final RegisterUserContext userContext) {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public String getTitleText() {
    return M.messages().registerMenuPriorityProjectsTitle();
  }

  @UiHandler("saveButton")
  public void onEditClick(final ClickEvent e) {
    presenter.save();
  }

  @UiHandler("cancelButton")
  public void onCancelClick(final ClickEvent e) {
    presenter.cancel();
  }

  @Override
  public PriorityProjectDossierEditor getDossierEditor() {
    return dossierEditor;
  }
}
