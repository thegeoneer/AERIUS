/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.statechange;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.widget.StackButton;
import nl.overheid.aerius.wui.main.widget.dialog.AeriusDialogBox;

/**
 * Dialog box with the export view inside it.
 */
class BaseStateChangePanel  {
  interface BaseStateChangePanelUiBinder extends UiBinder<Widget, BaseStateChangePanel> {}

  private static final BaseStateChangePanelUiBinder UI_BINDER = GWT.create(BaseStateChangePanelUiBinder.class);

  private static final int STATE_IMAGE_WIDTH = 31;

  @UiField AeriusDialogBox dialog;

  @UiField Panel buttonContainerPanel;
  @UiField Button cancelButton;
  @UiField Button okButton;

  private final HashMap<RequestState, StackButton<RequestState>> selectButtons = new HashMap<>();
  private RequestState selected;
  private RequestState initiallySelected;
  private UserProfile userProfile;

  private final StateChangeFlow stateChangeFlowUtil;

  /**
   * Dialog box with the export view inside it params get injected.
   * @param stateChangeFlowUtil
   */
  public BaseStateChangePanel(final UserProfile userProfile, final StateChangeFlow stateChangeFlowUtil) {
    this.userProfile = userProfile;
    this.stateChangeFlowUtil = stateChangeFlowUtil;

    UI_BINDER.createAndBindUi(this);

    cancelButton.ensureDebugId(TestID.STATUS_CHANGE_CANCEL_BUTTON);
    okButton.ensureDebugId(TestID.STATUS_CHANGE_OK_BUTTON);
  }

  public void setValue(final RequestState newState) {
    initiallySelected = newState;
    initButtons();
    onItemClick(newState);
  }

  public void center() {
    dialog.center();
  }

  @UiHandler("cancelButton")
  void onCancelClick(final ClickEvent e) {
    setValue(initiallySelected);
    dialog.hide();
  }

  public void hide() {
    dialog.hide();
  }

  public HandlerRegistration setOkClickHandler(final ClickHandler handler) {
    return okButton.addClickHandler(handler);
  }

  public RequestState getSelected() {
    return selected;
  }

  private void initButtons() {
    buttonContainerPanel.clear();
    selectButtons.clear();

    final List<RequestState> visibleStates = stateChangeFlowUtil.getIncludedStates(initiallySelected, userProfile);
    final Set<RequestState> allowedStates = stateChangeFlowUtil.getValidStates(initiallySelected, userProfile);

    for (final RequestState state : visibleStates) {
      final StackButton<RequestState> button = new StackButton<>(state,
          stateChangeFlowUtil.getStateIcon(state),
          stateChangeFlowUtil.getStateIconInactive(state),
          STATE_IMAGE_WIDTH);

      button.setText(stateChangeFlowUtil.getLabel(state));
      button.ensureDebugId(TestID.STATUS_CHANGE_ITEM_PREFIX + state.name());

      button.addSelectionHandler(new SelectionHandler<RequestState>() {
        @Override
        public void onSelection(final SelectionEvent<RequestState> event) {
          onItemClick(event.getSelectedItem());
        }
      });

      buttonContainerPanel.add(button);
      selectButtons.put(state, button);

      // Activate and/or disable button
      activateItem(state, state == initiallySelected);
      enableItem(state, state == initiallySelected || allowedStates.contains(state));
    }
  }

  private void onItemClick(final RequestState state) {
    if (selected != null) {
      // Deactivate the previously selected button.
      activateItem(selected, false);
    }

    // Store the selected type.
    selected = state;
    activateItem(selected, true);

    okButton.setEnabled(selected != initiallySelected);
  }

  private void activateItem(final RequestState state, final boolean active) {
    if (selectButtons.containsKey(state)) {
      selectButtons.get(state).setActive(active);
    }
  }

  private void enableItem(final RequestState state, final boolean enable) {
    if (selectButtons.containsKey(state)) {
      selectButtons.get(state).setEnabled(enable);
    }
  }

  public void setUserProfile(final UserProfile userProfile) {
    this.userProfile = userProfile;

  }

}
