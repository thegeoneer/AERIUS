/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.priorityproject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.ScalableImageColumn;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;
import nl.overheid.aerius.wui.register.event.PriorityProjectSubProjectsEvent;
import nl.overheid.aerius.wui.register.util.PriorityProjectStateUtil;

/**
 * Table to display Priority Project - Deposition space subprojects Data Table.
 */
public class PriorityProjectSubProjectsDataTable extends Composite implements IsInteractiveDataTable<PrioritySubProject> {

  public interface PPSubProjectTableEventHandler {
    void onDownloadFile(PrioritySubProject subProject, RequestFileType type);
  }

  interface PriorityProjectSubProjectsDataTableBinder extends UiBinder<Widget, PriorityProjectSubProjectsDataTable> {}

  private static final PriorityProjectSubProjectsDataTableBinder UI_BINDER = GWT.create(PriorityProjectSubProjectsDataTableBinder.class);

  private static final DateTimeFormat DATEFORMAT = DateTimeFormat.getFormat("dd-MM-yyyy");
  private static final int STATUS_COLUMN_IMAGE_WIDTH = 31;

  @UiField SimpleInteractiveClickDivTable<PrioritySubProject> table;

  @UiField(provided = true) ScalableImageColumn<PrioritySubProject> statusColumn;
  @UiField(provided = true) TextColumn<PrioritySubProject> dateColumn;
  @UiField(provided = true) TextColumn<PrioritySubProject> nameColumn;
  @UiField(provided = true) SimpleWidgetFactory<PrioritySubProject> documentColumn;

  public PriorityProjectSubProjectsDataTable(final EventBus eventBus, final PPSubProjectTableEventHandler eventHandler) {
    statusColumn = new ScalableImageColumn<PrioritySubProject>(STATUS_COLUMN_IMAGE_WIDTH) {
      @Override
      public DataResource getValue(final PrioritySubProject object) {
        return PriorityProjectStateUtil.getPriorityProjectStateIcon(object.getRequestState());
      }
    };

    dateColumn = new TextColumn<PrioritySubProject>() {
      @Override
      public String getValue(final PrioritySubProject value) {
        return DATEFORMAT.format(value.getReceivedDate());
      }
    };

    nameColumn = new TextColumn<PrioritySubProject>() {
      @Override
      public String getValue(final PrioritySubProject value) {
        return value.getLabel() == null ? value.getScenarioMetaData().getProjectName() : value.getLabel();
      }
    };

    documentColumn = new SimpleWidgetFactory<PrioritySubProject>() {

      @Override
      public Widget createWidget(final PrioritySubProject subProject) {
        final FlowPanel panel = new FlowPanel();
        panel.addStyleName(R.css().flex());

        for (final RequestFileType requestFileType : subProject.getFiles().keySet()) {
          final Button button = new Button();
          final String iconFileName;
          switch (requestFileType) {
          case DECREE:
            iconFileName = R.css().buttonDownload();
            break;
          case DETAIL_DECREE:
            iconFileName = R.css().buttonDownloadMaps();
            break;
          default:
            continue;
          }
          button.addStyleName(iconFileName);
          button.addStyleName(R.css().noBoxShadow());
          button.addStyleName(R.css().cursorPointer());

          button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
              event.stopPropagation();
              eventHandler.onDownloadFile(subProject, requestFileType);
            }
          });

          panel.add(button);
        }

        return panel;
      }
    };


    initWidget(UI_BINDER.createAndBindUi(this));

    table.setSelectionModel(new SingleSelectionModel<PrioritySubProject>());
    table.addSelectionChangeHandler(new Handler() {

      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        final PrioritySubProject selectedObject = ((SingleSelectionModel<PrioritySubProject>) table.getSelectionModel()).getSelectedObject();
        eventBus.fireEvent(new PriorityProjectSubProjectsEvent(selectedObject.getReference()));

      }
    });

    table.setLoadingByDefault(true);
  }

  @Override
  public SimpleInteractiveClickDivTable<PrioritySubProject> asDataTable() {
    return table;
  }
}
