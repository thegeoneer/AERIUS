/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.place;

import com.google.gwt.place.shared.WithTokenizers;

import nl.overheid.aerius.wui.admin.place.AdminPlaceHistoryMapper;
import nl.overheid.aerius.wui.admin.place.RoleManagementPlace;
import nl.overheid.aerius.wui.admin.place.UserManagementPlace;

/**
 * PlaceHistoryMapper interface is used to attach all places which the PlaceHistoryHandler should be aware of. This is done via the @WithTokenizers
 * annotation or by extending PlaceHistoryMapperWithFactory and creating a separate TokenizerFactory.
 */
@WithTokenizers({DashboardPlace.Tokenizer.class,
  PermitOverviewPlace.Tokenizer.class,
  PermitDossierPlace.Tokenizer.class,
  PermitDossierEditPlace.Tokenizer.class,
  PermitRequestPlace.Tokenizer.class,
  PermitReviewPlace.Tokenizer.class,
  PermitDevelopmentSpacePlace.Tokenizer.class,
  NoticeOverviewPlace.Tokenizer.class,
  NoticeDossierPlace.Tokenizer.class,
  NoticeMapPlace.Tokenizer.class,
  UserManagementPlace.Tokenizer.class,
  RoleManagementPlace.Tokenizer.class,
  PriorityProjectOverviewPlace.Tokenizer.class,
  PriorityProjectDossierPlace.Tokenizer.class,
  PriorityProjectDossierEditPlace.Tokenizer.class,
  PriorityProjectProjectPlace.Tokenizer.class,
  PriorityProjectActualisationPlace.Tokenizer.class,
  PriorityProjectProjectDossierReviewPlace.Tokenizer.class,
  PriorityProjectProjectDossierDetailPlace.Tokenizer.class
})
public interface RegisterPlaceHistoryMapper extends AdminPlaceHistoryMapper {
  // empty interface, used by GWT to generate code.
}
