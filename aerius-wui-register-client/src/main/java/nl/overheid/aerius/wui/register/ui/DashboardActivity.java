/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.impl.SchedulerImpl;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.Filter;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.shared.domain.register.dashboard.NoticeDashboardFilter;
import nl.overheid.aerius.shared.domain.register.dashboard.PermitDashboardFilter;
import nl.overheid.aerius.wui.admin.ui.AdminAbstractActivity;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil.ConcurrentAsyncServiceCall;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;

/**
 * Activity for Register Dashboard.
 */
public class DashboardActivity extends AdminAbstractActivity<RegisterAppContext> implements DashboardView.Presenter {
  protected static final String PERMIT_ROW_THREAD = "permitService.permits";
  protected static final String NOTICE_ROW_THREAD = "noticeService.notices";

  private final DashboardView view;

  @Inject
  public DashboardActivity(final RegisterAppContext appContext, final DashboardView view) {
    super(appContext, RegisterPermission.VIEW_DASHBOARD);

    this.view = view;
  }

  @Override
  public void start(final AcceptsOneWidget panel) {
    view.setPresenter(this);
    panel.setWidget(view);

    //get the filters from usercontext and use it to retrieve data/set view selections.
    useFilter(appContext.getUserContext().getFilter(NoticeDashboardFilter.class));
    useFilter(appContext.getUserContext().getFilter(PermitDashboardFilter.class));
  }

  @Override
  public void onFilterChange(final Filter filter) {
    SchedulerImpl.get().scheduleDeferred(new Command() {
      @Override
      public void execute() {
        if (filter instanceof NoticeDashboardFilter) {
          view.prepareLoadNoticeData();

          ConcurrentAsyncServiceUtil.register(NOTICE_ROW_THREAD, appContext.getConcurrencyMap(),
              new AppAsyncCallback<ArrayList<DefaultDashboardRow>>() {
            @Override
            public void onSuccess(final ArrayList<DefaultDashboardRow> result) {
              view.setNoticeData(result);
              appContext.getUserContext().useFilter(filter);
            }
          },
              new ConcurrentAsyncServiceCall<ArrayList<DefaultDashboardRow>>() {
            @Override
            public void execute(final AsyncCallback<ArrayList<DefaultDashboardRow>> callback) {
              appContext.getNoticeService().getDashboardNoticeRows((NoticeDashboardFilter) filter, callback);
            }
          });
        } else if (filter instanceof PermitDashboardFilter) {
          view.prepareLoadPermitData();

          ConcurrentAsyncServiceUtil.register(PERMIT_ROW_THREAD, appContext.getConcurrencyMap(),
              new AppAsyncCallback<ArrayList<DefaultDashboardRow>>() {
            @Override
            public void onSuccess(final ArrayList<DefaultDashboardRow> result) {
              view.setPermitData(result);
              appContext.getUserContext().useFilter(filter);
            }
          },
              new ConcurrentAsyncServiceCall<ArrayList<DefaultDashboardRow>>() {
            @Override
            public void execute(final AsyncCallback<ArrayList<DefaultDashboardRow>> callback) {
              appContext.getPermitService().getPermitDashboardRows((PermitDashboardFilter) filter, callback);
            }
          });
        }
      }
    });
  }

  private void useFilter(final Filter filter) {
    view.setFilter(filter);
    onFilterChange(filter);
  }

}
