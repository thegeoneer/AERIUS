/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.priorityproject;

import com.google.gwt.user.client.ui.FlowPanel;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.developmentspace.PrioritySubProjectReviewInfo;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.register.widget.ReviewInfoIndicatorWidget;

public class PrioritySubProjectReviewInfoIndicatorsPanel extends FlowPanel {

  public PrioritySubProjectReviewInfoIndicatorsPanel(final PrioritySubProjectReviewInfo object) {

    if (object.getNumReceptorsShortageInclusive() > 0) {
      add(DevelopmentRule.EXCEEDING_SPACE_CHECK,
          M.messages().registerPriorityProjectProjectDossierReviewShortage(
              FormatUtil.toWholeThreshold(object.getShortageInclusive()),
              FormatUtil.toWhole(object.getNumReceptorsShortageInclusive())));
    } else {
      add(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK, "");
    }
  }

  private void add(final DevelopmentRule rule, final String txt) {
    this.add(new ReviewInfoIndicatorWidget(rule, txt));
  }
}
