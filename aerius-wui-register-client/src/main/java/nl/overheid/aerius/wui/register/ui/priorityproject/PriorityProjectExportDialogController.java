/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.priorityproject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.shared.service.RequestExportServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.register.place.PriorityProjectPlace;
import nl.overheid.aerius.wui.register.processor.PriorityProjectExportPollingAgent;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectExportDialogPanel.PriorityProjectExportDialogDriver;
import nl.overheid.aerius.wui.register.widget.ExportRequests;

/**
 * Controller for starting the Priority Project Export dialog and communicating with the server.
 */
@Singleton
public class PriorityProjectExportDialogController implements PriorityProjectExportDialogPanel.Presenter {

  private final SimpleBeanEditorDriver<PriorityProjectExportData, PriorityProjectExportDialogPanel> driver =
      GWT.create(PriorityProjectExportDialogDriver.class);
  private final ConfirmCancelDialog<PriorityProjectExportData, PriorityProjectExportDialogPanel> dialog;

  private final ConfirmHandler<PriorityProjectExportData> confirmHandler = new ConfirmHandler<PriorityProjectExportData>() {
    @Override
    public void onConfirm(final ConfirmEvent<PriorityProjectExportData> event) {
      exportRequests.onExportButtonClick();
      dialog.hide();
    }
  };

  private final AppAsyncCallback<String> startExportCallBack = new AppAsyncCallback<String>() {
    @Override
    public void onSuccess(final String result) {
      // Start polling agent to check periodically if the export is ready
      agent.start(result, isExportReadyCallBack);
    }
  };

  private final AppAsyncCallback<RequestExportResult> isExportReadyCallBack = new AppAsyncCallback<RequestExportResult>() {
    @Override
    public void onSuccess(final RequestExportResult result) {
      agent.stop();
      exportResult = result;
      exportRequests.updateExportResult(exportResult);
    }

    @Override
    public void onFailure(final Throwable caught) {
      agent.stop();
      super.onFailure(caught);
    }
  };

  private final RequestExportServiceAsync requestExportService;
  private final PriorityProjectExportPollingAgent agent;
  private final PriorityProjectExportDialogPanel contentPanel;
  private final ExportRequests exportRequests;
  private RequestExportResult exportResult;

  @Inject
  public PriorityProjectExportDialogController(final HelpPopupController hpC, final RequestExportServiceAsync requestExportService,
      final PriorityProjectExportPollingAgent agent) {
    this.requestExportService = requestExportService;
    this.agent = agent;

    contentPanel = new PriorityProjectExportDialogPanel(hpC);
    contentPanel.setPresenter(this);

    exportRequests = new ExportRequests(false);
    exportRequests.setHandler(this);

    dialog = new ConfirmCancelDialog<PriorityProjectExportData, PriorityProjectExportDialogPanel>(
        driver, M.messages().exportGo(), M.messages().cancelButton());
    dialog.setText(M.messages().registerPriorityProjectExportTitle());

    // TODO Internet Explorer, as expected, won't handle the animation correctly.
    dialog.setAnimationEnabled(false);

    dialog.addConfirmHandler(confirmHandler);
    dialog.setWidget(contentPanel);
  }

  public void show(final PriorityProjectPlace place) {
    final PriorityProjectExportData exportData = new PriorityProjectExportData();
    exportData.setPriorityProjectKey(place.getKey());
    driver.initialize(contentPanel);
    driver.edit(exportData);
    dialog.center();
  }

  @Override
  public void prepareExport() {
    requestExportService.startPriorityExport(driver.flush(), startExportCallBack);
  }

  @Override
  public void downloadExport() {
    Window.open(exportResult.getDownloadUrl(), "_blank", "");
  }

  @Override
  public void enableConfirmButton(final boolean enable) {
    dialog.enableConfirmButton(enable);
  }
}
