/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.util;

import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.shared.domain.register.PriorityProjectProgress;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Util class for {@link PriorityProjectProgress}.
 */
public class PriorityProjectProgressUtil {

  /**
   * Returns the image related to the {@link PriorityProjectProgress}.
   * @param state to get the image from
   * @return image related to the state
   */
  public static DataResource getPriorityProjectProgressIcon(final PriorityProjectProgress state) {
    final DataResource resource;

    switch (state == null ? PriorityProjectProgress.NONE : state) {
    case NONE:
      resource = R.images().registerProgressZeroPct();
      break;
    case UNDER_HALF:
      resource = R.images().registerProgressLessThan50Pct();
      break;
    case OVER_HALF:
      resource = R.images().registerProgressMoreThan50Pct();
      break;
    case FULL:
      resource = R.images().registerProgressHundredPct();
      break;
    case DONE:
      resource = R.images().registerProgressDone();
      break;
    case NO_RESERVATION_PRESENT:
    default:
      resource = R.images().applicationstateNoReservationPresent();
    }

    return resource;
  }
}
