/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.wui.register.place.NoticePlace;
import nl.overheid.aerius.wui.register.ui.notice.NoticeGroupButton.NoticeGroupButtonHandler;

/**
 *
 */
public interface NoticeDetailView extends IsWidget {
  enum NoticeViewType {
    DOSSIER, MAP;
  }

  interface Presenter extends NoticeGroupButtonHandler {
    void switchView(NoticeViewType value);

    void onNoticeSelection(Notice notice);

  }

  void setPresenter(Presenter presenter);

  void setPlace(NoticePlace noticePlace);

  void setNotice(Notice notice);

  void setLocked(boolean lock);

  void setContentPanel(IsWidget contentPanel);

  void enableDelete(boolean b);

}
