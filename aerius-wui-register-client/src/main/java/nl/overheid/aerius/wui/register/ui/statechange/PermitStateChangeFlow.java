/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.statechange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.register.util.PermitStateUtil;

/**
 * Util to return valid state changes given a specific state.
 */
public final class PermitStateChangeFlow implements StateChangeFlow {

  /**
   * Contains the valid state changes given a specific state.
   */
  private static final Map<RequestState, Set<RequestState>> VALID_STATE_CHANGES = new HashMap<>();
  /**
   * Contains state changes that are excluded given a specific state.
   */
  private static final Map<RequestState, Set<RequestState>> EXCLUDED_STATE_CHANGES = new HashMap<>();

  static {

    // The state changes (from and to) that are allowed for an user with all rights.
    for (final RequestState state : RequestState.values()) {
      VALID_STATE_CHANGES.put(state, new HashSet<RequestState>());
    }

    VALID_STATE_CHANGES.get(RequestState.QUEUED).add(RequestState.PENDING_WITH_SPACE);
    VALID_STATE_CHANGES.get(RequestState.QUEUED).add(RequestState.PENDING_WITHOUT_SPACE);

    VALID_STATE_CHANGES.get(RequestState.PENDING_WITH_SPACE).add(RequestState.QUEUED);
    VALID_STATE_CHANGES.get(RequestState.PENDING_WITH_SPACE).add(RequestState.ASSIGNED);

    VALID_STATE_CHANGES.get(RequestState.PENDING_WITHOUT_SPACE).add(RequestState.QUEUED);
    VALID_STATE_CHANGES.get(RequestState.PENDING_WITHOUT_SPACE).add(RequestState.REJECTED_WITHOUT_SPACE);

    VALID_STATE_CHANGES.get(RequestState.ASSIGNED).add(RequestState.ASSIGNED_FINAL);
    VALID_STATE_CHANGES.get(RequestState.ASSIGNED).add(RequestState.REJECTED_FINAL);

    VALID_STATE_CHANGES.get(RequestState.REJECTED_WITHOUT_SPACE).add(RequestState.QUEUED);
    VALID_STATE_CHANGES.get(RequestState.REJECTED_WITHOUT_SPACE).add(RequestState.REJECTED_FINAL);

    for (final RequestState state : RequestState.values()) {
      EXCLUDED_STATE_CHANGES.put(state, new HashSet<>(Arrays.asList(RequestState.INITIAL)));
    }

    EXCLUDED_STATE_CHANGES.get(RequestState.PENDING_WITH_SPACE).add(RequestState.PENDING_WITHOUT_SPACE);
    EXCLUDED_STATE_CHANGES.get(RequestState.PENDING_WITH_SPACE).add(RequestState.REJECTED_WITHOUT_SPACE);

    EXCLUDED_STATE_CHANGES.get(RequestState.PENDING_WITHOUT_SPACE).add(RequestState.PENDING_WITH_SPACE);
    EXCLUDED_STATE_CHANGES.get(RequestState.PENDING_WITHOUT_SPACE).add(RequestState.ASSIGNED);
    EXCLUDED_STATE_CHANGES.get(RequestState.PENDING_WITHOUT_SPACE).add(RequestState.ASSIGNED_FINAL);

    EXCLUDED_STATE_CHANGES.get(RequestState.ASSIGNED).add(RequestState.PENDING_WITHOUT_SPACE);
    EXCLUDED_STATE_CHANGES.get(RequestState.ASSIGNED).add(RequestState.REJECTED_WITHOUT_SPACE);

    EXCLUDED_STATE_CHANGES.get(RequestState.ASSIGNED_FINAL).add(RequestState.PENDING_WITHOUT_SPACE);
    EXCLUDED_STATE_CHANGES.get(RequestState.ASSIGNED_FINAL).add(RequestState.REJECTED_WITHOUT_SPACE);
    EXCLUDED_STATE_CHANGES.get(RequestState.ASSIGNED_FINAL).add(RequestState.REJECTED_FINAL);

    EXCLUDED_STATE_CHANGES.get(RequestState.REJECTED_WITHOUT_SPACE).add(RequestState.PENDING_WITH_SPACE);
    EXCLUDED_STATE_CHANGES.get(RequestState.REJECTED_WITHOUT_SPACE).add(RequestState.ASSIGNED);
    EXCLUDED_STATE_CHANGES.get(RequestState.REJECTED_WITHOUT_SPACE).add(RequestState.ASSIGNED_FINAL);
  }

  public PermitStateChangeFlow() {
  }

  /**
   * Considering a request with a certain state and user permissions, returns the possible states that
   * the request can change to.
   */
  @Override
  public Set<RequestState> getValidStates(final RequestState currentState, final UserProfile userProfile) {
    final Set<RequestState> allowedStates = new HashSet<>(VALID_STATE_CHANGES.get(currentState));

    if (currentState == RequestState.QUEUED
        && !userProfile.hasPermission(RegisterPermission.UPDATE_PERMIT_STATE_DEQUEUE)) {
      allowedStates.remove(RequestState.PENDING_WITH_SPACE);
      allowedStates.remove(RequestState.PENDING_WITHOUT_SPACE);
    }
    if ((currentState == RequestState.PENDING_WITH_SPACE || currentState == RequestState.PENDING_WITHOUT_SPACE)
        && !userProfile.hasPermission(RegisterPermission.UPDATE_PERMIT_STATE_ENQUEUE)) {
      allowedStates.remove(RequestState.QUEUED);
    }
    if (currentState == RequestState.REJECTED_WITHOUT_SPACE
        && !userProfile.hasPermission(RegisterPermission.UPDATE_PERMIT_STATE_ENQUEUE_REJECTED_WITHOUT_SPACE)) {
      allowedStates.remove(RequestState.QUEUED);
    }
    if (!userProfile.hasPermission(RegisterPermission.UPDATE_PERMIT_STATE_REJECT_FINAL)) {
      allowedStates.remove(RequestState.REJECTED_FINAL);
    }

    return allowedStates;
  }

  /**
   * Considering a request with a certain state and user permissions, returns the possible states that
   * may be shown as buttons in the dialog.
   */
  @Override
  public List<RequestState> getIncludedStates(final RequestState currentState, final UserProfile userProfile) {
    final ArrayList<RequestState> visibleStates = new ArrayList<>(Arrays.asList(RequestState.values()));
    final Set<RequestState> hiddenStates = new HashSet<>(EXCLUDED_STATE_CHANGES.get(currentState));
    visibleStates.removeAll(hiddenStates);
    return visibleStates;
  }

  @Override
  public String getLabel(final RequestState state) {
    return M.messages().registerRequestState(state);
  }

  @Override
  public DataResource getStateIcon(final RequestState state) {
    return PermitStateUtil.getPermitStateIcon(state);
  }

  @Override
  public DataResource getStateIconInactive(final RequestState state) {
    return PermitStateUtil.getPermitStateIconInactive(state);
  }

}
