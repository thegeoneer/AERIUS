/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.NoticeDossierPlace;
import nl.overheid.aerius.wui.register.ui.calculator.CalculatorExportController;

/**
 * Activity for detail view of melding.
 */
public class NoticeDetailDossierActivity extends NoticeDetailBaseActivity implements NoticeDetailDossierView.Presenter {

  private final NoticeDetailDossierView view;
  private CalculatorExportController exportController;
  private Notice notice;

  @Inject
  public NoticeDetailDossierActivity(final RegisterAppContext appContext, final NoticeDetailView baseView, final NoticeDetailDossierView view,
      @Assisted final NoticeDossierPlace place, final NoticeDeleteDialogController deleteDialogController) {
    super(appContext, baseView, view, place, deleteDialogController);
    this.view = view;
  }

  @Override
  protected void onStart() {
    super.onStart();
    view.setPresenter(this);

    exportController = new CalculatorExportController(appContext.getContext(), eventBus) {

      @Override
      protected void doServiceCall(final AsyncCallback<ScenarioGMLs> callback) {
        appContext.getNoticeService().getGML(notice.getKey(), callback);
      }
    };
  }

  @Override
  protected void onNoticeLoaded(final Notice notice) {
    this.notice = notice;
    view.setData(notice);
  }

  @Override
  public void openRequestFile(final Notice notice) {
    Window.open(notice.getUrl(RequestFileType.APPLICATION), "_blank", "");
  }

  @Override
  public void openInCalculator(final Notice notice) {
    exportController.showPostCalculatorDialog();
  }

}
