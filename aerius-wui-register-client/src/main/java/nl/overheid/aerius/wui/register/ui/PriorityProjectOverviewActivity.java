/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.SortableDirection;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectFilter;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PriorityProjectSortableAttribute;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportServiceAsync;
import nl.overheid.aerius.wui.admin.ui.AdminAbstractActivity;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.table.GridScrollVisitor;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierPlace;
import nl.overheid.aerius.wui.register.ui.importer.PriorityProjectImportProjectDialogController;
import nl.overheid.aerius.wui.register.ui.importer.RegisterImportDialogPanel;

public class PriorityProjectOverviewActivity extends AdminAbstractActivity<RegisterAppContext> implements PriorityProjectOverviewView.Presenter {
  private static final int MAX_RESULTS = 30;

  private final class SetDataAppAsyncCallback extends AppAsyncCallback<ArrayList<PriorityProject>> {
    @Override
    public void onSuccess(final ArrayList<PriorityProject> result) {
      if (activeCallback == this) {
        view.setData(result);
      }
    }
  }

  private final class UpdateAppAsyncCallback extends AppAsyncCallback<ArrayList<PriorityProject>> {
    @Override
    public void onSuccess(final ArrayList<PriorityProject> result) {
      if (activeCallback == this) {
        view.updateData(result);
      }
    }
  }

  private AsyncCallback<ArrayList<PriorityProject>> activeCallback;

  private final PriorityProjectOverviewView view;

  @Inject
  public PriorityProjectOverviewActivity(final PriorityProjectOverviewView view, final RegisterAppContext appContext) {
    super(appContext, RegisterPermission.VIEW_PRIORITY_PROJECTS);

    this.view = view;
  }

  @Override
  public void start(final AcceptsOneWidget panel) {
    panel.setWidget(view);

    view.setPresenter(this);
    view.setDataVisitor(new GridScrollVisitor(this));

    onRefresh();
  }

  @Override
  public void onProjectSelection(final PriorityProject project) {
    if (project != null) {
      goTo(new PriorityProjectDossierPlace(project.getKey()));
    }
  }

  @Override
  public void onRefresh() {
    onAdaptFilter();
  }

  @Override
  public void onNewPriorityProject() {
    final RegisterImportDialogPanel<PriorityProjectKey> dialog = new RegisterImportDialogPanel<>(SupportedUploadFormat.PRIORITY_PROJECT);

    final PriorityProjectImportProjectDialogController newPriorityProjectController = new PriorityProjectImportProjectDialogController(
        RequestFileType.PRIORITY_PROJECT_RESERVATION, appContext.getPlaceController(),
        RegisterRetrieveImportServiceAsync.Util.getInstance(), dialog) {
      @Override
      public final void handleResult(final PriorityProjectKey key) {
        hide();
        goTo(getPlace(key));
      }

    };
    newPriorityProjectController.setTitle(M.messages().registerNewPriorityProjectTitle());
    newPriorityProjectController.show();
  }

  @Override
  public void onNewActualisation() {
    final RegisterImportDialogPanel<PriorityProjectKey> dialog = new RegisterImportDialogPanel<>(SupportedUploadFormat.PRIORITY_PROJECT);

    final PriorityProjectImportProjectDialogController newActualisationController = new PriorityProjectImportProjectDialogController(
        RequestFileType.PRIORITY_PROJECT_ACTUALISATION, appContext.getPlaceController(),
        RegisterRetrieveImportServiceAsync.Util.getInstance(), dialog) {
      @Override
      public final void handleResult(final PriorityProjectKey key) {
        hide();
        goTo(getPlace(key));
      }

    };
    newActualisationController.setTitle(M.messages().registerNewActualisationTitle());
    newActualisationController.show();
  }

  @Override
  public void setSorting(final PriorityProjectSortableAttribute attribute, final SortableDirection dir) {
    final PriorityProjectFilter filter = appContext.getUserContext().getFilter(PriorityProjectFilter.class);

    filter.setSortAttribute(attribute);
    filter.setSortDirection(dir);

    onAdaptFilter();
  }

  @Override
  public void onAdaptFilter() {
    final PriorityProjectFilter filter = appContext.getUserContext().getFilter(PriorityProjectFilter.class);

    activeCallback = new SetDataAppAsyncCallback();
    appContext.getPriorityProjectService().getPriorityProjects(0, MAX_RESULTS, filter, activeCallback);
  }

  @Override
  public void loadData(final int offset) {
    final PriorityProjectFilter filter = appContext.getUserContext().getFilter(PriorityProjectFilter.class);

    activeCallback = new UpdateAppAsyncCallback();
    appContext.getPriorityProjectService().getPriorityProjects(offset, MAX_RESULTS, filter, activeCallback);
  }

}
