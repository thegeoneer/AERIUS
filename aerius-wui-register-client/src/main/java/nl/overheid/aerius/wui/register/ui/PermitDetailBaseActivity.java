/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.wui.admin.ui.AdminAbstractActivity;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.event.PermitCalculationFinishedEvent;
import nl.overheid.aerius.wui.register.event.PermitRetrievedEvent;
import nl.overheid.aerius.wui.register.place.PermitOverviewPlace;
import nl.overheid.aerius.wui.register.place.PermitPlace;
import nl.overheid.aerius.wui.register.ui.permit.PermitMarkDialogPanel;

/**
 *
 */
abstract class PermitDetailBaseActivity extends AdminAbstractActivity<RegisterAppContext>
    implements PermitDetailView.Presenter, AsyncCallback<Permit> {

  interface PermitDetailBaseEventBinder extends EventBinder<PermitDetailBaseActivity> {
  }

  private final PermitDetailBaseEventBinder eventBinder = GWT.create(PermitDetailBaseEventBinder.class);

  private final PermitDetailView view;
  private final IsWidget contentPanel;
  private final PermitPlace permitPlace;
  private boolean permitUpdated;

  private final AsyncCallback<Void> deleteDossierCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      setLocked(false);
      broadcastNotificationMessage(M.messages().registerNotificationApplicationDeleted(permit.getDossierMetaData().getDossierId()));
      appContext.invalidatePermit();
      goTo(new PermitOverviewPlace());
    }

    @Override
    public void onFailure(final Throwable caught) {
      PermitDetailBaseActivity.this.onFailure(caught);
      super.onFailure(caught);
    }
  };

  private final AsyncCallback<Void> savePermitStateCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      setLocked(false);
      broadcastNotificationMessage(M.messages().registerNotificationApplicationStateChangeDone(permit.getDossierMetaData().getDossierId()));
      // TODO Use another notification/flag
      permitUpdated = true;
      reloadCurrentPermit();
    }

    @Override
    public void onFailure(final Throwable caught) {
      PermitDetailBaseActivity.this.onFailure(caught);
      super.onFailure(caught);
    }
  };

  private final AsyncCallback<Void> reloadPermitCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      setLocked(false);
      reloadCurrentPermit();
    }

    @Override
    public void onFailure(final Throwable caught) {
      PermitDetailBaseActivity.this.onFailure(caught);
      super.onFailure(caught);
    }
  };

  private Permit permit;

  public PermitDetailBaseActivity(final RegisterAppContext appContext, final PermitDetailView view, final IsWidget contentPanel,
      final PermitPlace permitPlace) {
    super(appContext, RegisterPermission.VIEW_PERMITS);

    this.appContext = appContext;
    this.view = view;
    this.contentPanel = contentPanel;
    this.permitPlace = permitPlace;
  }

  @Override
  public final void start(final AcceptsOneWidget panel) {
    eventBinder.bindEventHandlers(this, eventBus);
    setLocked(true);
    view.setPresenter(this);
    view.setContentPanel(contentPanel);
    panel.setWidget(view);
    view.setPlace(permitPlace);
    appContext.getPermit(permitPlace.getKey(), this);
    onStart();
  }

  protected void onStart() {
    // no-op
  }

  @Override
  public final boolean onChangeState(final RequestState newState) {
    boolean changed;
    if (newState == RequestState.REJECTED_FINAL && !Window.confirm(M.messages().registerApplicationDeleteTitle())) {
      changed = false;
    } else {
      changed = true;
      if (newState == RequestState.REJECTED_FINAL) {
        rejectPermit();
      } else {
        saveApplicationState(newState);
      }
    }
    return changed;
  }

  @Override
  public final void onSuccess(final Permit permit) {
    this.permit = permit;
    // Fire an event notifying an application has been retrieved (and interpreted) when all is set and done
    fireEvent(new PermitRetrievedEvent(permit));
    view.setPermit(permit);
    onPermitLoaded(permit);
    // Notifying here if the application update flag is set
    if (permitUpdated) {
      broadcastNotificationMessage(M.messages().registerNotificationApplicationUpdated(permit.getDossierMetaData().getDossierId()));
      permitUpdated = false;
    }
    setLocked(false);
  }

  /**
   * Called when the permit is loaded.
   * @param permit permit
   */
  protected abstract void onPermitLoaded(final Permit permit);

  @Override
  public void onFailure(final Throwable caught) {
    setLocked(false);
    if (caught instanceof AeriusException && Reason.PERMIT_UNKNOWN.equals(((AeriusException) caught).getReason())) {
      goTo(new PermitOverviewPlace());
    } else {
      // Revert to the last version on failure. I assume that's the best course of action when the
      // deletion fails...
      reloadCurrentPermit();
    }
  }

  @Override
  public void deletePermit() {
    if (!Window.confirm(M.messages().registerApplicationDeleteTitle())) {
      return;
    }
    setLocked(true);
    appContext.getPermitService().delete(permitPlace.getKey(), deleteDossierCallback);
  }

  @Override
  public void markRequest() {
    final ConfirmCancelDialog<Object, ?> dialog = new ConfirmCancelDialog<>(M.messages().genericOK(), M.messages().genericCancel());
    dialog.setText(permit.isMarked()
        ? M.messages().registerApplicationUnmarkDialogPanelTitle()
            : M.messages().registerApplicationMarkDialogPanelTitle());

    dialog.setWidget(new PermitMarkDialogPanel(permit.isMarked()));

    dialog.addConfirmHandler(new ConfirmHandler<Object>() {
      @Override
      public void onConfirm(final ConfirmEvent<Object> event) {
        updateMark(!permit.isMarked());
        dialog.hide();
      }
    });
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        dialog.center();
      }
    });
  }

  @EventHandler
  void onCalculationFinished(final PermitCalculationFinishedEvent event) {
    if (event.getValue().equals(permit.getPermitKey())) {
      reloadCurrentPermit();
    }
  }

  private void rejectPermit() {
    setLocked(true);
    broadcastNotificationMessage(M.messages().registerNotificationApplicationStateBeingChanged(permit.getDossierMetaData().getDossierId()));
    appContext.getPermitService().reject(permit.getPermitKey(), permit.getLastModified(), deleteDossierCallback);
  }

  private void updateMark(final boolean mark) {
    appContext.getPermitService().updateMark(permit.getPermitKey(), mark, reloadPermitCallback);
  }

  private void saveApplicationState(final RequestState requestState) {
    setLocked(true);
    broadcastNotificationMessage(M.messages().registerNotificationApplicationStateBeingChanged(permit.getDossierMetaData().getDossierId()));
    appContext.getPermitService().changeState(permit.getPermitKey(), requestState, permit.getLastModified(), savePermitStateCallback);
  }

  protected void reloadCurrentPermit() {
    appContext.invalidatePermit();
    if (permit == null) {
      goTo(new PermitOverviewPlace());
    } else {
      setLocked(true);
      appContext.getPermitForceReload(permit.getPermitKey(), this);
    }
  }

  protected void setLocked(final boolean lock) {
    view.setLocked(lock);
    updatePermissionStates(lock);
  }

  private void updatePermissionStates(final boolean lock) {
    final boolean isInitial = permit != null && permit.getRequestState() == RequestState.INITIAL;
    final boolean permissionDelete = hasPermission(RegisterPermission.DELETE_PERMIT_ALL)
        || isInitial && hasPermission(RegisterPermission.DELETE_PERMIT_INACTIVE_INITIAL);

    view.enableDelete(!lock && permissionDelete);
    view.enableStatus(!lock && hasPermission(RegisterPermission.UPDATE_PERMIT_STATE));
    view.enableMark(!lock && hasPermission(RegisterPermission.UPDATE_PERMIT_MARK));
  }

  private boolean hasPermission(final RegisterPermission permission) {
    return UserProfileUtil.hasPermissionForAuthority(
        appContext.getUserContext().getUserProfile(), permit == null ? null : permit.getAuthority(), permission);
  }

  @Override
  public void openRequestFile(final Request request) {
    Window.open(request.getUrl(RequestFileType.APPLICATION), "_blank", "");
  }

  @Override
  public void showPostCalculatorDialog() {
    // by default no-op
  }
}
