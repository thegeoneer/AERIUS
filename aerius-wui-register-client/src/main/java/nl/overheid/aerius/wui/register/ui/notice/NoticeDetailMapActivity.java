/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeMapData;
import nl.overheid.aerius.wui.geo.LayerConfigPanel;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.event.NoticeMapDataRetrievedEvent;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.event.NoticeSelectedEvent;
import nl.overheid.aerius.wui.register.geo.NoticeMarker.NoticeMarkerType;
import nl.overheid.aerius.wui.register.place.NoticeMapPlace;

/**
 * Activity for map detail view of a notice.
 */
public class NoticeDetailMapActivity extends NoticeDetailBaseActivity implements NoticeDetailMapView.Presenter {

  private static final int LOCATION_CHANGE_RIB_WIDTH = 20000;

  private final NoticeDetailMapView view;
  private final LayerConfigPanel layerConfigPanel;

  private final AsyncCallback<NoticeMapData> mapDataCallBack = new AppAsyncCallback<NoticeMapData>() {

    @Override
    public void onSuccess(final NoticeMapData result) {
      Scheduler.get().scheduleDeferred(new ScheduledCommand() {
        @Override
        public void execute() {
          view.updateListData(result.getNotices());
          result.getNotices().add(notice); // add current to highlight in chart
          fireEvent(new NoticeMapDataRetrievedEvent(result));
          updateSelectedNotice();

        }
      });
    }
  };

  @Inject
  public NoticeDetailMapActivity(final RegisterAppContext appContext, final NoticeDetailView baseView, final NoticeDetailMapView view,
      @Assisted final NoticeMapPlace place, final NoticeDeleteDialogController deleteDialogController, final LayerConfigPanel layerConfigPanel) {
    super(appContext, baseView, view, place, deleteDialogController);
    this.view = view;
    this.layerConfigPanel = layerConfigPanel;
  }

  @Override
  protected void onStart() {
    view.setPresenter(this);
    view.startupMap();
  }

  @Override
  public void onStop() {
    CloseEvent.fire(layerConfigPanel, true);
    view.cleanup();
  }

  private void updateSelectedNotice() {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        fireEvent(new NoticeSelectedEvent(notice, NoticeMarkerType.SELECTED));
        fireEvent(new LocationChangeEvent(new BBox(notice.getPoint().getX(), notice.getPoint().getY(), LOCATION_CHANGE_RIB_WIDTH)));
      }
    });
  }

  @Override
  protected void onNoticeLoaded(final Notice notice) {
    this.notice = notice;
    view.setData(notice);
    appContext.getNoticeService().getNoticesMapData(notice.getId(), mapDataCallBack);
  }

}
