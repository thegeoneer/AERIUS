/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.dashboard;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.Option;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.register.dashboard.NoticeDashboardFilter;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.register.widget.FilterPanel;

public class NoticeDashboardFilterPanel extends FilterPanel<NoticeDashboardFilter> {

  private static final NoticeDashboardFilterPanelUiBinder uiBinder = GWT.create(NoticeDashboardFilterPanelUiBinder.class);

  interface NoticeDashboardFilterPanelUiBinder extends UiBinder<Widget, NoticeDashboardFilterPanel> {
  }

  @UiField ListBox provincesList;
  @UiField ListBox minimalDepositionUsedList;

  public NoticeDashboardFilterPanel(final RegisterContext context, final RegisterUserContext userContext, final HelpPopupController hpC) {
    initWidget(uiBinder.createAndBindUi(this));

    provincesList.addItem(M.messages().registerDashboardProvinces(), "0");
    for (final Province province : context.getProvinces()) {
      provincesList.addItem(province.getName(), String.valueOf(province.getId()));

      if (userContext.getUserProfile().getAuthority().getId() == province.getProvinceId()) {
        provincesList.setSelectedIndex(provincesList.getItemCount());
      }
    }

    for (final Option<Double> option : context.getMinimalDepositionSpaceOptions()) {
      minimalDepositionUsedList.addItem(option.getLabel(), String.valueOf(option.getValue()));
      if (option.isDefaultValue()) {
        minimalDepositionUsedList.setSelectedIndex(minimalDepositionUsedList.getItemCount() - 1);
      }
    }

    hpC.addWidget(provincesList, hpC.tt().ttRegisterDashboardFilterProvince());

    provincesList.ensureDebugId(TestIDRegister.LIST_PROVINCES_NOTICES);
    minimalDepositionUsedList.ensureDebugId(TestIDRegister.LIST_MIN_DEP);
  }

  @UiHandler("provincesList")
  void onProvinceChange(final ChangeEvent event) {
    onFilterChange();
  }

  @UiHandler("minimalDepositionUsedList")
  void onMinFractionUsedChange(final ChangeEvent event) {
    onFilterChange();
  }

  @Override
  public void setFilter(final NoticeDashboardFilter filter) {
    if (filter.getProvinceId() != null) {
      for (int i = 0; i < provincesList.getItemCount(); i++) {
        if (String.valueOf(filter.getProvinceId()).equals(provincesList.getValue(i))) {
          provincesList.setSelectedIndex(i);
        }
      }
    }
    for (int i = 0; i < minimalDepositionUsedList.getItemCount(); i++) {
      if (String.valueOf(filter.getMinFractionUsed()).equals(minimalDepositionUsedList.getValue(i))) {
        minimalDepositionUsedList.setSelectedIndex(i);
      }
    }
  }

  private void onFilterChange() {
    final NoticeDashboardFilter filter = new NoticeDashboardFilter();
    filter.setProvinceId(Integer.parseInt(provincesList.getValue(provincesList.getSelectedIndex())));
    filter.setMinFractionUsed(Double.parseDouble(minimalDepositionUsedList.getValue(minimalDepositionUsedList.getSelectedIndex())));
    onFilterChange(filter);
  }

}
