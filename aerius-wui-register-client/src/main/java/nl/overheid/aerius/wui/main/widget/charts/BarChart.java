/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.charts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.SimplePanel;

import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * Parameterized {@link BarChart}.
 *
 * Displays a stylable horizontal element containing {@link Bar}s.
 *
 * @param <E> the type of the data.
 */
public abstract class BarChart<E> extends Composite implements HasSelectionHandlers<Integer> {
  private static final int PERCENT = 100;

  /**
   * Represents a single {@link Bar} in this {@link BarChart}.
   */
  public static class Bar {
    private final String color;
    private final String name;
    private final double value;

    /**
     * Create a {@link Bar} for a {@link BarChart}.
     *
     * @param color the color for this bar
     * @param name the name for this bar
     * @param value the value that represents the weight on this bar
     */
    public Bar(final String color, final String name, final double value) {
      this.color = color;
      this.name = name;
      this.value = value;
    }
  }

  private final ArrayList<Bar> bars = new ArrayList<Bar>();
  private final HashMap<Bar, Panel> barPanels = new HashMap<Bar, Panel>();
  private double totalWeight;

  protected final FlowPanel panel;

  /**
   * Constructor for a {@link BarChart}.
   */
  public BarChart() {
    panel = new FlowPanel();

    initWidget(panel);
  }

  /**
   * Move the bar at the given index to the front.
   *
   * @param idx index of the bar to move.
   */
  public void moveToFront(final int idx) {
    move(idx, 0);
  }

  /**
   * Clear the bars.
   */
  public void clear() {
    bars.clear();
    totalWeight = 0;
  }

  /**
   * Move the given bar to the given index.
   *
   * @param from index to start
   * @param to indext to end
   */
  public void move(final int from, final int to) {
    panel.insert(panel.getWidget(from), to);
  }

  /**
   * Add a single bar to the bar chart.
   *
   * @param val Element out of which to create a bar
   */
  public void addBar(final E val) {
    final Bar bar = createBar(val);
    totalWeight += bar.value;
    bars.add(bar);
  }

  /**
   * Set the data for this bar chart.
   *
   * @param list a list containing elements out of which {@link Bar}s will be created.
   */
  public void addAllBars(final List<E> list) {
    for (final E val : list) {
      addBar(val);
    }

    draw();
  }

  @Override
  public final HandlerRegistration addSelectionHandler(final SelectionHandler<Integer> handler) {
    return addHandler(handler, SelectionEvent.getType());
  }

  /**
   * Create a {@link Bar} using the given value.
   *
   * @param val the typed value
   *
   * @return a {@link Bar}
   */
  public abstract Bar createBar(final E val);

  /**
   * Draw the chart.
   */
  public void draw() {
    panel.clear();

    for (final Bar b : bars) {
      final SimplePanel barPanel = new SimplePanel();
      barPanel.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
      barPanel.getElement().getStyle().setBackgroundColor(ColorUtil.webColor(b.color));
      barPanel.getElement().getStyle().setWidth(b.value / totalWeight * PERCENT, Unit.PCT);
      barPanel.getElement().getStyle().setHeight(PERCENT, Unit.PCT);

      // TODO i18n?
      if (b.name != null && !b.name.isEmpty()) {
        barPanel.setTitle(b.name + ": " + FormatUtil.toFixed(b.value, 1));
      }

      panel.add(barPanel);
      barPanels.put(b, barPanel);
    }
  }

  /**
   * @param bar The bar to get the panel for.
   * @return The panel for the bar.
   */
  public Panel getPanel(final Bar bar) {
    return barPanels.get(bar);
  }
}
