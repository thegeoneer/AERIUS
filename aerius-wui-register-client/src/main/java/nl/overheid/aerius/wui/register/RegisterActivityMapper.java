/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.wui.admin.AdminActivityFactory;
import nl.overheid.aerius.wui.admin.AdminActivityMapper;
import nl.overheid.aerius.wui.register.RegisterActivityMapper.ActivityFactory;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.DashboardPlace;
import nl.overheid.aerius.wui.register.place.NoticeDossierPlace;
import nl.overheid.aerius.wui.register.place.NoticeMapPlace;
import nl.overheid.aerius.wui.register.place.NoticeOverviewPlace;
import nl.overheid.aerius.wui.register.place.PermitDevelopmentSpacePlace;
import nl.overheid.aerius.wui.register.place.PermitDossierEditPlace;
import nl.overheid.aerius.wui.register.place.PermitDossierPlace;
import nl.overheid.aerius.wui.register.place.PermitOverviewPlace;
import nl.overheid.aerius.wui.register.place.PermitRequestPlace;
import nl.overheid.aerius.wui.register.place.PermitReviewPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectActualisationPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierEditPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectOverviewPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectDossierDetailPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectDossierReviewPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectPlace;
import nl.overheid.aerius.wui.register.ui.DashboardActivity;
import nl.overheid.aerius.wui.register.ui.NoticeActivity;
import nl.overheid.aerius.wui.register.ui.PermitDetailDevelopmentSpaceActivity;
import nl.overheid.aerius.wui.register.ui.PermitDetailDossierEditActivity;
import nl.overheid.aerius.wui.register.ui.PermitDetailDossierViewActivity;
import nl.overheid.aerius.wui.register.ui.PermitDetailRequestActivity;
import nl.overheid.aerius.wui.register.ui.PermitDetailReviewActivity;
import nl.overheid.aerius.wui.register.ui.PermitOverviewActivity;
import nl.overheid.aerius.wui.register.ui.PriorityProjectActualisationActivity;
import nl.overheid.aerius.wui.register.ui.PriorityProjectDossierActivity;
import nl.overheid.aerius.wui.register.ui.PriorityProjectDossierEditActivity;
import nl.overheid.aerius.wui.register.ui.PriorityProjectOverviewActivity;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectActivity;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierDetailActivity;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierReviewActivity;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDetailDossierActivity;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDetailMapActivity;

/**
 * Activity Mapper for the Register.
 */
class RegisterActivityMapper extends AdminActivityMapper<ActivityFactory, RegisterAppContext, RegisterContext, RegisterUserContext> {
  private final PlaceController placeController;

  /**
   * CaclulatorActivityMapper associates each Place with its corresponding {@link Activity}.
   *
   * @param factory
   *          Generated factory to add params to constructors
   * @param contextProvider
   *          user context object
   */
  @Inject
  public RegisterActivityMapper(final PlaceController placeController, final ActivityFactory factory, final RegisterAppContext context) {
    super(factory, context);
    this.placeController = placeController;
  }

  @Override
  public Activity getActivity(final Place place) {
    Activity presenter = null;

    if (place instanceof DashboardPlace) {
      presenter = factory.createDashboardActivity((DashboardPlace) place);
    } else if (place instanceof PermitOverviewPlace) {
      presenter = factory.createPermitOverviewActivity((PermitOverviewPlace) place);
    } else if (place instanceof PermitDossierPlace) {
      presenter = factory.createPermitDetailDossierViewActivity((PermitDossierPlace) place);
    } else if (place instanceof PermitDossierEditPlace) {
      presenter = factory.createPermitDetailDossierEditActivity((PermitDossierEditPlace) place);
    } else if (place instanceof PermitRequestPlace) {
      presenter = factory.createPermitDetailApplicationActivity((PermitRequestPlace) place);
    } else if (place instanceof PermitReviewPlace) {
      presenter = factory.createPermitDetailValidationActivity((PermitReviewPlace) place);
    } else if (place instanceof PermitDevelopmentSpacePlace) {
      presenter = factory.createPermitDetailDevelopmentSpaceActivity((PermitDevelopmentSpacePlace) place);
    } else if (place instanceof NoticeOverviewPlace) {
      presenter = factory.createNoticeActivity((NoticeOverviewPlace) place);
    } else if (place instanceof NoticeDossierPlace) {
      presenter = factory.createNoticeDossierActivity((NoticeDossierPlace) place);
    } else if (place instanceof NoticeMapPlace) {
      presenter = factory.createNoticeMapActivity((NoticeMapPlace) place);
    } else if (place instanceof PriorityProjectOverviewPlace) {
      presenter = factory.createPriorityProjectOverviewActivity((PriorityProjectOverviewPlace) place);
    } else if (place instanceof PriorityProjectDossierPlace) {
      presenter = factory.createPriorityProjectDossierActivity((PriorityProjectDossierPlace) place);
    } else if (place instanceof PriorityProjectDossierEditPlace) {
      presenter = factory.createPriorityProjectDossierEditActivity((PriorityProjectDossierEditPlace) place);
    } else if (place instanceof PriorityProjectProjectPlace) {
      presenter = factory.createPriorityProjectProjectActivity((PriorityProjectProjectPlace) place);
    } else if (place instanceof PriorityProjectActualisationPlace) {
      presenter = factory.createPriorityProjectActualisationActivity((PriorityProjectActualisationPlace) place);
    } else if (place instanceof PriorityProjectProjectDossierDetailPlace) {
      presenter = factory.createPriorityProjectProjectDossierDetailActivity((PriorityProjectProjectDossierDetailPlace) place);
    } else if (place instanceof PriorityProjectProjectDossierReviewPlace) {
      presenter = factory.createPriorityProjectProjectDossierReviewActivity((PriorityProjectProjectDossierReviewPlace) place);
    } else {
      presenter = super.getActivity(place);
    }

    if (presenter != null) {
      // normal place just go to this place
      return presenter;
    }

    Scheduler.get().scheduleFinally(new ScheduledCommand() {
      @Override
      public void execute() {
        placeController.goTo(new DashboardPlace());
      }
    });

    return null;

  }

  /**
   * Methods capable of creating presenters given the place that is passed in.
   */
  public interface ActivityFactory extends AdminActivityFactory {
    /**
     * Create Activity with Place as parameter.
     *
     * @param place
     *          current place
     * @param subActivity
     *          activity within the main activity
     * @return new ACtivity instance
     */
    DashboardActivity createDashboardActivity(DashboardPlace place);

    PriorityProjectDossierEditActivity createPriorityProjectDossierEditActivity(PriorityProjectDossierEditPlace place);

    PriorityProjectDossierActivity createPriorityProjectDossierActivity(PriorityProjectDossierPlace place);

    PriorityProjectOverviewActivity createPriorityProjectOverviewActivity(PriorityProjectOverviewPlace place);

    PriorityProjectProjectActivity createPriorityProjectProjectActivity(PriorityProjectProjectPlace place);

    PriorityProjectActualisationActivity createPriorityProjectActualisationActivity(PriorityProjectActualisationPlace place);

    PriorityProjectProjectDossierDetailActivity createPriorityProjectProjectDossierDetailActivity(PriorityProjectProjectDossierDetailPlace place);

    PriorityProjectProjectDossierReviewActivity createPriorityProjectProjectDossierReviewActivity(PriorityProjectProjectDossierReviewPlace place);

    PermitDetailDossierViewActivity createPermitDetailDossierViewActivity(PermitDossierPlace place);

    PermitDetailDossierEditActivity createPermitDetailDossierEditActivity(PermitDossierEditPlace place);

    PermitDetailRequestActivity createPermitDetailApplicationActivity(PermitRequestPlace place);

    PermitDetailReviewActivity createPermitDetailValidationActivity(PermitReviewPlace place);

    PermitDetailDevelopmentSpaceActivity createPermitDetailDevelopmentSpaceActivity(PermitDevelopmentSpacePlace place);

    PermitOverviewActivity createPermitOverviewActivity(PermitOverviewPlace place);

    NoticeActivity createNoticeActivity(NoticeOverviewPlace place);

    NoticeDetailDossierActivity createNoticeDossierActivity(NoticeDossierPlace place);

    NoticeDetailMapActivity createNoticeMapActivity(NoticeMapPlace place);
  }
}
