/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.Filter;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.shared.domain.register.dashboard.NoticeDashboardFilter;
import nl.overheid.aerius.shared.domain.register.dashboard.PermitDashboardFilter;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.HeadingWidget;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.register.ui.dashboard.DefaultDashboardTable;
import nl.overheid.aerius.wui.register.ui.dashboard.NoticeDashboardFilterPanel;
import nl.overheid.aerius.wui.register.ui.dashboard.NoticeDashboardTable;
import nl.overheid.aerius.wui.register.ui.dashboard.PermitDashboardFilterPanel;

/**
 * TODO Not at all happy with the resize handler in here. Theoretically a css rule 'flex-wrap: wrap;' does the exact same thing,
 * but this appears to be buggy because heights are not recalculated to their container as you would expect if elements on the
 * horizontal axis are wrapped. Instead, height will be content-height, which in this case will be much too high a value.
 */
public class DashboardViewImpl extends Composite implements DashboardView {
  private static DashboardViewImplUiBinder uiBinder = GWT.create(DashboardViewImplUiBinder.class);

  interface DashboardViewImplUiBinder extends UiBinder<Widget, DashboardViewImpl> {
  }

  private static final int COLUMN_THRESHOLD = 1200;

  @UiField FlowPanel permitHeader;
  @UiField FlowPanel meldingHeader;
  @UiField HeadingWidget permitHeading;
  @UiField HeadingWidget noticeHeading;

  @UiField(provided = true) NoticeDashboardFilterPanel noticeDashboardFilterPanel;
  @UiField(provided = true) NoticeDashboardTable noticeDashboardTable;

  @UiField(provided = true) PermitDashboardFilterPanel permitDashboardFilterPanel;
  @UiField DefaultDashboardTable permitDashboardTable;

  @UiField FlowPanel tableContainer;

  private final HandlerRegistration handle;
  private boolean layoutScheduled;

  private final ScheduledCommand layoutCmd = new ScheduledCommand() {
    @Override
    public void execute() {
      layoutScheduled = false;
      forceLayout();
    }
  };

  @Inject
  public DashboardViewImpl(final RegisterContext context, final RegisterUserContext userContext, final HelpPopupController hpC) {
    noticeDashboardFilterPanel = new NoticeDashboardFilterPanel(context, userContext, hpC);
    permitDashboardFilterPanel = new PermitDashboardFilterPanel(context, userContext, hpC);

    noticeDashboardTable = new NoticeDashboardTable(context);

    initWidget(uiBinder.createAndBindUi(this));

    handle = Window.addResizeHandler(new ResizeHandler() {
      @Override
      public void onResize(final ResizeEvent event) {
        scheduleLayout();
      }
    });

    hpC.addWidget(permitHeading, hpC.tt().ttRegisterDashboardPermitHeading());
    hpC.addWidget(noticeHeading, hpC.tt().ttRegisterDashboardNoticeHeading());
  }

  @Override
  protected void onUnload() {
    handle.removeHandler();
  }

  @Override
  protected void onLoad() {
    scheduleLayout();
  }

  @Override
  public void setPresenter(final DashboardView.Presenter presenter) {
    noticeDashboardFilterPanel.setPresenter(presenter);
    permitDashboardFilterPanel.setPresenter(presenter);
  }

  @Override
  public void setNoticeData(final ArrayList<DefaultDashboardRow> rows) {
    noticeDashboardTable.asDataTable().setRowData(rows);
  }

  @Override
  public void setPermitData(final ArrayList<DefaultDashboardRow> rows) {
    permitDashboardTable.asDataTable().setRowData(rows);
  }

  @Override
  public void setFilter(final Filter filter) {
    if (filter instanceof NoticeDashboardFilter) {
      noticeDashboardFilterPanel.setFilter((NoticeDashboardFilter) filter);
    } else if (filter instanceof PermitDashboardFilter) {
      permitDashboardFilterPanel.setFilter((PermitDashboardFilter) filter);
    }
  }

  @Override
  public void prepareLoadNoticeData() {
    noticeDashboardTable.asDataTable().clear();
  }

  @Override
  public void prepareLoadPermitData() {
    permitDashboardTable.asDataTable().clear();
  }

  @Override
  public String getTitleText() {
    return M.messages().registerDashboardTitle();
  }

  private void scheduleLayout() {
    if (isAttached() && !layoutScheduled) {
      layoutScheduled = true;
      Scheduler.get().scheduleDeferred(layoutCmd);
    }
  }

  private void forceLayout() {
    final boolean rows = getOffsetWidth() > COLUMN_THRESHOLD;

    tableContainer.setStyleName(R.css().rows(), rows);
    tableContainer.setStyleName(R.css().columnsClean(), !rows);

    meldingHeader.setStyleName(R.css().flex(), !rows);
    meldingHeader.setStyleName(R.css().spaceBetween(), !rows);
    meldingHeader.setStyleName(R.css().calculatorRow(), rows);

    permitHeader.setStyleName(R.css().flex(), !rows);
    permitHeader.setStyleName(R.css().spaceBetween(), !rows);
    permitHeader.setStyleName(R.css().calculatorRow(), rows);
  }
}
