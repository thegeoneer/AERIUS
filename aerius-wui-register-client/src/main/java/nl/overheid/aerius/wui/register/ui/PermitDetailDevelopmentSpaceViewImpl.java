/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitSummary;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.geo.LayerPanel;
import nl.overheid.aerius.wui.main.domain.DevelopmentSpaceUtil;
import nl.overheid.aerius.wui.main.event.DevelopmentRulesEvent;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.depositionspace.DepositionSpacePanel;
import nl.overheid.aerius.wui.main.ui.depositionspace.DepositionSpacePanel.DepositionSpaceDriver;
import nl.overheid.aerius.wui.main.widget.AttachedPopupButton;
import nl.overheid.aerius.wui.main.widget.AttachedPopupButtonGroup;
import nl.overheid.aerius.wui.register.geo.RegisterCoreMapLayoutPanel;
import nl.overheid.aerius.wui.register.geo.RegisterDevelopmentRuleLayerWrapper;
import nl.overheid.aerius.wui.register.geo.RegisterProjectContributionLayerWrapper;

public class PermitDetailDevelopmentSpaceViewImpl extends Composite implements PermitDetailSummaryView {

  interface PermitDevelopmentSpaceViewImplUiBinder extends UiBinder<Widget, PermitDetailDevelopmentSpaceViewImpl> {}

  private static final PermitDevelopmentSpaceViewImplUiBinder UI_BINDER = GWT.create(PermitDevelopmentSpaceViewImplUiBinder.class);

  interface PermitDevelopmentSpaceViewImplEventBinder extends EventBinder<PermitDetailDevelopmentSpaceViewImpl> {}

  private static final PermitDevelopmentSpaceViewImplEventBinder EVENT_BINDER = GWT.create(PermitDevelopmentSpaceViewImplEventBinder.class);

  private static final int LOCATION_CHANGE_RIB_WIDTH = 2000;

  @UiField AttachedPopupButtonGroup buttonGroup;
  @UiField AttachedPopupButton layerPanelButton;

  @UiField(provided = true) DepositionSpacePanel depositionSpacePanel;
  @UiField Image iconInsufficientOR;
  @UiField Image iconSufficientOR;
  @UiField Label labelInsufficientOR;
  @UiField Label labelSufficientOR;
  @UiField Label labelLegenda;
  @UiField(provided = true) RegisterCoreMapLayoutPanel mapPanel;
  @UiField(provided = true) LayerPanel layerPanel;
  @UiField Label summaryNotAvailable;
  @UiField FlowPanel summaryAvailable;

  private final DepositionSpaceDriver driverDeposition = GWT.create(DepositionSpaceDriver.class);

  private final RegisterProjectContributionLayerWrapper projectSpaceLayer;
  private final RegisterDevelopmentRuleLayerWrapper developmentRuleLayer;

  private final EventBus eventBus;
  private HandlerRegistration mapHandlers;

  @Inject
  public PermitDetailDevelopmentSpaceViewImpl(final DepositionSpacePanel depositionSpacePanel, final LayerPanel layerPanel,
      final RegisterCoreMapLayoutPanel mapPanel, final RegisterContext context, final EventBus eventBus,
      final RegisterProjectContributionLayerWrapper projectSpaceLayer, final RegisterDevelopmentRuleLayerWrapper developmentRuleLayer) {
    this.depositionSpacePanel = depositionSpacePanel;
    this.mapPanel = mapPanel;
    this.layerPanel = layerPanel;
    this.eventBus = eventBus;
    this.projectSpaceLayer = projectSpaceLayer;
    this.developmentRuleLayer = developmentRuleLayer;

    initWidget(UI_BINDER.createAndBindUi(this));

    iconInsufficientOR.setResource(DevelopmentSpaceUtil.getDevelopmentRuleIcon(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK));
    iconSufficientOR.setResource(DevelopmentSpaceUtil.getDevelopmentRuleIcon(DevelopmentRule.EXCEEDING_SPACE_CHECK));

    labelInsufficientOR.setText(M.messages().registerReviewLegendDevelopmentRule(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK));
    labelSufficientOR.setText(M.messages().registerReviewLegendDevelopmentRule(DevelopmentRule.EXCEEDING_SPACE_CHECK));

    labelLegenda.setText(M.messages().registerPermitDevelopmentSpaceLegendaRemarks());

    driverDeposition.initialize(depositionSpacePanel);

    layerPanelButton.ensureDebugId(TestIDRegister.BUTTON_LAYERPANEL);
    depositionSpacePanel.ensureDebugId(TestIDRegister.DIV_TABLE_RES_DEPOSITION);

    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  @Override
  public void setSummary(final PermitSummary permitSummary) {
    setData(permitSummary.getDevelopmentRuleResults());

    if (permitSummary.getEmissionSources() != null) {
      mapPanel.getMarkersLayer().setMarkers(permitSummary.getEmissionSources(), null);
    }
  }

  @Override
  protected void onLoad() {
    mapHandlers = mapPanel.attachEventBinders();
    mapPanel.nudge();
  }

  public void setPermit(final Permit permit) {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        eventBus.fireEvent(new LocationChangeEvent(permit.getPoint(), LOCATION_CHANGE_RIB_WIDTH));
      }
    });

    projectSpaceLayer.updateProjectContributionAndDraw(permit.getId());
    developmentRuleLayer.clearMarkers();
  }

  private void setData(final List<CalculationAreaSummaryResult> developmentRuleResults) {
    driverDeposition.edit(developmentRuleResults);
  }

  @EventHandler
  void onDevelopmentRuleEvent(final DevelopmentRulesEvent e) {
    developmentRuleLayer.updateDevelopmentRulesAndDraw(e);
    mapPanel.refreshLayers();
  }

  @Override
  protected void onUnload() {
    if (mapHandlers != null) {
      mapHandlers.removeHandler();
    }

    buttonGroup.deactivate();

    projectSpaceLayer.detach();
    developmentRuleLayer.detach();

    mapPanel.getMarkersLayer().clear();
  }

  @Override
  public void setSummaryAvailable(final boolean available) {
    summaryNotAvailable.setVisible(!available);
    summaryAvailable.setVisible(available);
  }
}
