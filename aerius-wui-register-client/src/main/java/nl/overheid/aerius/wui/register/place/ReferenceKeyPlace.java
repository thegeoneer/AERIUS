/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.place;

import java.util.Map;

import nl.overheid.aerius.shared.domain.register.ReferenceKey;
import nl.overheid.aerius.wui.main.place.CompositeTokenizer;

/**
 * Base class for Reference Key places.
 */
public abstract class ReferenceKeyPlace<K extends ReferenceKey> extends RegisterPlace {
  private static final String REFERENCE = "ref";

  public static abstract class Tokenizer<K extends ReferenceKey, P extends ReferenceKeyPlace<K>> extends CompositeTokenizer<P> {
    @Override
    protected void updatePlace(final Map<String, String> tokens, final P place) {
      place.reference = tokens.get(REFERENCE);
    }

    @Override
    protected void setTokenMap(final P place, final Map<String, String> tokens) {
      put(tokens, REFERENCE, place.reference);
    }
  }

  protected String reference;

  protected ReferenceKeyPlace() {
    this(null);
  }

  protected ReferenceKeyPlace(final K key) {
    this.reference = key == null ? null : key.getReference();
  }

  public abstract K getKey();

  @Override
  public String toString() {
    return "ReferenceKeyPlace [reference=" + reference + "]";
  }
}
