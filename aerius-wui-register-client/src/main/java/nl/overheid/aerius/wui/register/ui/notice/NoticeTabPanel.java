/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.widget.SimpleFocusLabel;
import nl.overheid.aerius.wui.main.widget.SimpleTabPanel;
import nl.overheid.aerius.wui.register.place.NoticeMapPlace;
import nl.overheid.aerius.wui.register.place.NoticePlace;

/**
 * A simple UI wrapper for the register tab panel.
 */
@Singleton
public class NoticeTabPanel extends Composite {
  interface NoticeTabPanelUiBinder extends UiBinder<Widget, NoticeTabPanel> {
  }

  private static final NoticeTabPanelUiBinder UI_BINDER = GWT.create(NoticeTabPanelUiBinder.class);

  @UiField SimpleTabPanel tabPanel;
  @UiField SimpleFocusLabel tabNoticeDossier;
  @UiField SimpleFocusLabel tabNoticeMap;
  /**
   * Default constructor for the simple {@link SimpleTabPanel} register UI wrapper.
   * @param eventBus EventBus
   */
  @Inject
  public NoticeTabPanel(final EventBus eventBus) {
    initWidget(UI_BINDER.createAndBindUi(this));
    eventBus.addHandler(PlaceChangeEvent.TYPE, new PlaceChangeEvent.Handler() {
      @Override
      public void onPlaceChange(final PlaceChangeEvent event) {
        if (event.getNewPlace() instanceof NoticePlace) {
          setTabFocus((NoticePlace) event.getNewPlace());
        }
      }
    });

  }

  /**
   * Lock the tab panel, causing it to never fire a selection event for lockable
   * tabs and change styling on preset tabs.
   *
   * @param locked True to lock, false to unlock.
   */
  public void setLocked(final boolean locked) {
    tabPanel.setLocked(locked);
  }

  public void setTabFocus(final NoticePlace place) {
    if (place instanceof NoticeMapPlace) {
      tabPanel.setFocus(tabNoticeMap, false);
    } else {
      tabPanel.setFocus(tabNoticeDossier, false);
    }
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    tabNoticeDossier.ensureDebugId(TestIDRegister.NOTICE_TAB_DOSSIER);
    tabNoticeMap.ensureDebugId(TestIDRegister.NOTICE_TAB_CHART);
  }

  public HandlerRegistration addSelectionHandler(final SelectionHandler<Integer> handler) {
    return tabPanel.addHandler(handler, SelectionEvent.getType());
  }
}
