/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import com.google.gwt.editor.client.LeafValueEditor;

import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;

/**
 * Contains export requests logic/dialog to be used in all the request tabs.
 */
public class ExportRequests {

  public interface ExportRequestsHandler {
    void prepareExport();
    void downloadExport();
  }

  private final ConfirmHandler<Void> onExportConfirmHandler = new ConfirmHandler<Void>() {
    @Override
    public void onConfirm(final ConfirmEvent<Void> event) {
      confirmExportDialog.hide();
      handler.downloadExport();
    }
  };

  private final ConfirmCancelDialog<Void, LeafValueEditor<Void>> confirmExportDialog =
      new ConfirmCancelDialog<Void, LeafValueEditor<Void>>(M.messages().okButton(), M.messages().cancelButton());
  private final ExportConfirmDialogPanel confirmExportDialogContent;
  private ExportRequestsHandler handler;

  /**
   * Construct.
   * @param usesFilterToExport Whether the export functionality uses filters to filter the export that should be generated.
   *  At this time this only changes the i18n.
   */
  public ExportRequests(final boolean usesFilterToExport) {
    confirmExportDialogContent = new ExportConfirmDialogPanel(usesFilterToExport);
    confirmExportDialog.setHTML(M.messages().registerExportConfirmPanelTitle());
    confirmExportDialog.setWidget(confirmExportDialogContent);
    confirmExportDialog.addConfirmHandler(onExportConfirmHandler);
  }

  public void updateExportResult(final RequestExportResult requestExportResult) {
    if (requestExportResult == null) {
      confirmExportDialogContent.setAmountToConfirm(0, 0);
      confirmExportDialog.enableConfirmButton(false);
    } else {
      confirmExportDialogContent.setAmountToConfirm(requestExportResult.getAmount(), requestExportResult.getTotalSize());
      confirmExportDialog.enableConfirmButton(requestExportResult.getAmount() > 0);
    }
    confirmExportDialog.showProgressImage(false);
  }

  public void onExportButtonClick() {
    confirmExportDialogContent.setLoading();
    confirmExportDialog.showProgressImage(true);
    confirmExportDialog.enableConfirmButton(false);
    confirmExportDialog.center();
    handler.prepareExport();
  }

  public void setHandler(final ExportRequestsHandler handler) {
    this.handler = handler;
  }

}
