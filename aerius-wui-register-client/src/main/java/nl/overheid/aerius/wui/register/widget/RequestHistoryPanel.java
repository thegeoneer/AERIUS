/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.register.AuditTrailChange;
import nl.overheid.aerius.shared.domain.register.AuditTrailItem;
import nl.overheid.aerius.shared.domain.register.AuditTrailRequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.InlineHTMLColumn;
import nl.overheid.aerius.wui.main.widget.table.IsDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class RequestHistoryPanel extends Composite implements IsDataTable<AuditTrailItem> {
  interface RequestHistoryPanelUiBinder extends UiBinder<Widget, RequestHistoryPanel> {
  }

  private static final RequestHistoryPanelUiBinder UI_BINDER = GWT.create(RequestHistoryPanelUiBinder.class);

  protected static final String DEFAULT_CHANGE_TEXT = "-";

  @UiField SimpleDivTable<AuditTrailItem> divTable;

  @UiField(provided = true) TextColumn<AuditTrailItem> dateColumn;
  @UiField(provided = true) InlineHTMLColumn<AuditTrailItem> actionColumn;

  public RequestHistoryPanel() {
    dateColumn = new TextColumn<AuditTrailItem>() {
      @Override
      public String getValue(final AuditTrailItem object) {
        return FormatUtil.getDefaultDateTimeFormatter().format(object.getDate());
      }
    };

    actionColumn = new InlineHTMLColumn<AuditTrailItem>() {
      @Override
      public SafeHtml getValue(final AuditTrailItem value) {
        final SafeHtml result;

        if (value.getItems().isEmpty()) {
          result = SafeHtmlUtils.fromString(DEFAULT_CHANGE_TEXT);
        } else {
          result = getAuditTrailText(value);
        }

        return result;
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public SimpleDivTable<AuditTrailItem> asDataTable() {
    return divTable;
  }

  protected SafeHtml getAuditTrailText(final AuditTrailItem item) {
    final SafeHtmlBuilder changeTextBuilder = new SafeHtmlBuilder();

    for (int i = 0; i < item.getItems().size(); ++i) {
      final AuditTrailChange change = item.getItems().get(i);

      if (i > 0) {
        changeTextBuilder.appendEscapedLines("\n");
      }

      changeTextBuilder.appendEscaped(getChangeItemText(item, change));
    }

    return changeTextBuilder.toSafeHtml();
  }

  protected String getChangeItemText(final AuditTrailItem item, final AuditTrailChange change) {
    String changeItemText;

    switch (change.getType()) {
    case ASSIGN_COMPLETED:
      changeItemText =
        Boolean.valueOf(change.getNewValue())
        ? M.messages().registerApplicationTabHistoryAssignCompleted()
            : M.messages().registerApplicationTabHistoryAssignInProgress();
      break;
    case STATE:
      final AuditTrailRequestState state = AuditTrailRequestState.safeValueOf(change.getNewValue());
      changeItemText = M.messages().registerApplicationTabHistoryStateFull(
          M.messages().registerApplicationTabHistoryState(item.getSegment(), state));
      if (AuditTrailRequestState.REMOVED_STATES.contains(state)) {
        changeItemText += " " + M.messages().registerApplicationTabHistoryStateRemovedState();
      }
      break;
    case SEGMENT:
      changeItemText = M.messages().registerApplicationTabHistorySegment(
          M.messages().registerApplicationSegment(SegmentType.safeValueOf(change.getNewValue())));
      break;
    default:
      changeItemText =  M.messages().registerApplicationTabHistoryChange(change.getType(), change.getOldValue(), change.getNewValue());
      break;
    }

    return changeItemText;
  }
}
