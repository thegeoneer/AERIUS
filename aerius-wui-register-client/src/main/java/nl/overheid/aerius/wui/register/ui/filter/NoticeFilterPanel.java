/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.filter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 * Filter Panel for Notices.
 */
@Singleton
public class NoticeFilterPanel extends BaseRegisterFilterPanel<NoticeFilter> {
  private static final NoticeFilterPanelUiBinder UI_BINDER = GWT.create(NoticeFilterPanelUiBinder.class);

  interface NoticeFilterPanelUiBinder extends UiBinder<Widget, NoticeFilterPanel> {}

  @Inject
  public NoticeFilterPanel(final RegisterContext context) {
    super(context);
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  protected String getTitleLabel(final String province, final String from, final String till, final boolean fromIsDefault, final Boolean marked) {
    String title;
    final String sector = sectorListBox.getHeaderText(sectorListBox.getSelectedValues());
    if (fromIsDefault) {
      title = M.messages().registerNoticeFilterCollapseTitleNoDate(province, sector);
    } else {
      title = M.messages().registerNoticeFilterCollapseTitle(province, from, till, sector);
    }
    return title;
  }
}
