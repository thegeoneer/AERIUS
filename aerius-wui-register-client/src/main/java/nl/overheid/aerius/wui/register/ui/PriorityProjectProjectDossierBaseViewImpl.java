/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectProjectDossierTabPanel;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectStatusButton;


public class PriorityProjectProjectDossierBaseViewImpl extends Composite implements PriorityProjectProjectDossierBaseView {
  interface PriorityProjectProjectDossierBaseViewImplUiBinder extends UiBinder<Widget, PriorityProjectProjectDossierBaseViewImpl> {}

  private static final PriorityProjectProjectDossierBaseViewImplUiBinder UI_BINDER = GWT.create(PriorityProjectProjectDossierBaseViewImplUiBinder.class);

  interface CustomStyle extends CssResource {
    String lock();
  }

  @UiField CustomStyle style;

  @UiField(provided = true) PriorityProjectProjectDossierTabPanel tabPanel;
  @UiField(provided = true) PriorityProjectStatusButton statusButton;
  @UiField SimplePanel child;

  private Presenter presenter;
  private PrioritySubProject prioritySubProject;

  private HandlerRegistration disregardInputRegistration;

  private final ClickHandler disregardInputHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      event.stopPropagation();
      event.preventDefault();
    }
  };

  @Inject
  public PriorityProjectProjectDossierBaseViewImpl(final RegisterUserContext userContext,
      final PriorityProjectStatusButton statusButton, final PriorityProjectProjectDossierTabPanel tabPanel) {
    this.tabPanel = tabPanel;
    this.statusButton = statusButton;
    initWidget(UI_BINDER.createAndBindUi(this));
    tabPanel.ensureDebugId(TestIDRegister.APPLICATION_TAB_PANEL);
  }

  @Override
  public void setWidget(final IsWidget w) {
    child.setWidget(w);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
    statusButton.setBaseStatusHandler(presenter);
  }

  @Override
  public void setViewType(final PriorityProjectDossierDetailViewType viewType) {
  }

  @Override
  public void setPrioritySubProject(final PrioritySubProject prioritySubProject) {
    this.prioritySubProject = prioritySubProject;
    statusButton.setValue(prioritySubProject.getRequestState());
  }

  @Override
  public void enableStatus(final boolean enabled) {
    statusButton.enableStatus(enabled);
  }

  @Override
  public void enableDelete(final boolean enabled) {
    statusButton.enableDelete(enabled);
  }

  @Override
  public void setLocked(final boolean lock) {
    child.setStyleName(style.lock(), lock);

    if (disregardInputRegistration != null) {
      disregardInputRegistration.removeHandler();
    }

    if (lock) {
      disregardInputRegistration = child.addDomHandler(disregardInputHandler, ClickEvent.getType());
    }

    tabPanel.setLocked(lock);
  }

}
