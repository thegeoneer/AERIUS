/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;

/**
 * Test class for {@link OptionUtil}. A  fair part is already tested via the service implementations, but not all.
 */
public class OptionUtilTest extends BaseDBTest {

  @Test
  public void testGetExportProperties() {
    final CalculationOptions options = new CalculationOptions();
    options.setYear(2019);
    final ScenarioMetaData metadata = new ScenarioMetaData();
    
    final ExportProperties exportProperties1 = OptionUtil.getExportProperties(options, ExportType.GML_WITH_RESULTS, "aerius@example.com", metadata);
    Assert.assertTrue("Expected EMAIL_USER by default", exportProperties1.getAdditionalOptions().contains(ExportAdditionalOptions.EMAIL_USER));
    
    options.setSendEmail(false);
    final ExportProperties exportProperties2 = OptionUtil.getExportProperties(options, ExportType.GML_WITH_RESULTS, "aerius@example.com", metadata);
    Assert.assertFalse("Expected no EMAIL_USER if sendEmail = false", exportProperties2.getAdditionalOptions().contains(ExportAdditionalOptions.EMAIL_USER));
  }
  
}
