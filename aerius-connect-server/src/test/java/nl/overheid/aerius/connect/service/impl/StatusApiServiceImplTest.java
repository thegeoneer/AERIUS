/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.StatusJobResponse;
import nl.overheid.aerius.db.scenario.ScenarioUserRepository;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Test class for {@link StatusApiServiceImpl}.
 */
public class StatusApiServiceImplTest extends TestBaseService {

  private static final String BAD_API_KEY = "badKey";
  private static final String BAD_JOB_KEY = "badJobKey";
  private static final String TEST_EMAIL = "aerius@aerius.nl";

  private StatusApiServiceImpl statusService;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    statusService = new StatusApiServiceImpl(new ConnectServiceContext(getCalcPMF(), null));
  }

  /**
   * @throws AeriusException
   * @throws SQLException
   */
  private ScenarioUser createScenarioUser() throws AeriusException, SQLException {
    final GenerateAPIKeyApiServiceImpl userService =
        new GenerateAPIKeyApiServiceImpl(new ConnectServiceContext(getCalcPMF(), new TaskManagerClient(factory)));
    userService.generateAPIKey(TEST_EMAIL);
    return ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL);
  }

  @Test(expected = AeriusException.class)
  public void testBadAPIKey() throws IOException, AeriusException {
    try {
      statusService.jobs(BAD_API_KEY);
    } catch (final AeriusException e) {
      assertEquals("Expected USER_INVALID_API_KEY as reason", Reason.USER_INVALID_API_KEY, e.getReason());
      throw e;
    }
  }

  @Test
  public void testCorrectAPIKey() throws IOException, AeriusException, SQLException {
    final ScenarioUser scenarioUser = createScenarioUser();
    final StatusJobResponse result = statusService.jobs(scenarioUser.getApiKey());
    assertNotNull("Progress result should not be null", result);
    assertTrue("Progress list should be empty", result.getEntries().isEmpty());
  }
  
  @Test(expected = AeriusException.class)
  public void testBadAPIKeyAndBadJobKey() throws IOException, AeriusException {
    try {
      statusService.jobById(BAD_API_KEY, BAD_JOB_KEY);
    } catch (final AeriusException e) {
      assertEquals("Expected USER_INVALID_API_KEY as reason", Reason.USER_INVALID_API_KEY, e.getReason());
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testBadJobKeyOnly() throws IOException, AeriusException, SQLException {
    final ScenarioUser scenarioUser = createScenarioUser();
    try {
      statusService.jobById(scenarioUser.getApiKey(), BAD_JOB_KEY);
    } catch (final AeriusException e) {
      assertEquals("Expected CONNECT_USER_JOBKEY_DOES_NOT_EXIST as reason", Reason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, e.getReason());
      throw e;
    }
  }
}
