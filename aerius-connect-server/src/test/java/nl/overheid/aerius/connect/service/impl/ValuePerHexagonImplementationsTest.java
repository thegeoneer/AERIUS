/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.stream.DoubleStream;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

public class ValuePerHexagonImplementationsTest {

  private static final double EPSILON = 1E-5;

  final double[] NOX_VALUES = new double[] {1.2, 1.4, 1.3};
  final double[] NH3_VALUES = new double[] {2.5, 2.2, 2.6};
  final double[] NOX_MISSING_VALUES = new double[] {1.2, 0, 1.3};
  final double[] NH3_MISSING_VALUES = new double[] {0, 1.1, 0};
  final double[] NOX_VALUES_DELTA = new double[] {1.2, 1.4};
  final double[] NH3_VALUES_DELTA = new double[] {2.5, 2.2};

  @Test
  public void highestValue() {
    final AeriusResultPoint[] results = createResultPoints(NOX_VALUES, NH3_VALUES);

    final AeriusResultPoint processed = new AeriusResultPoint();
    HighestValuePerHexagonService.mergeResults(results[0], processed);
    HighestValuePerHexagonService.mergeResults(results[1], processed);

    // expected first result as NOX + NH3 of the first result is higher than the second
    assertEquals("Result should match first NOX value", NOX_VALUES[0], processed.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), EPSILON);
    assertEquals("Result should match first NH3 value", NH3_VALUES[0], processed.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), EPSILON);

    HighestValuePerHexagonService.mergeResults(results[2], processed);
    // expected third result as NOX + NH3 of the third result is higher than the previous result
    assertEquals("Result should match third NOX value", NOX_VALUES[2], processed.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), EPSILON);
    assertEquals("Result should match third NH3 value", NH3_VALUES[2], processed.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), EPSILON);
  }

  @Test
  public void highestValueMissingValues() {
    final AeriusResultPoint[] results = createResultPoints(NOX_MISSING_VALUES, NH3_MISSING_VALUES);
    addResults(results, EmissionResultKey.NO2_CONCENTRATION, new double[] {2, 2, 2});

    final AeriusResultPoint processed = new AeriusResultPoint();
    HighestValuePerHexagonService.mergeResults(results[0], processed);
    HighestValuePerHexagonService.mergeResults(results[1], processed);

    // expected first result as NOX + NH3 of the first result is higher than the second
    assertEquals("Result should match first NOX value",
        NOX_MISSING_VALUES[0], processed.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), EPSILON);
    assertEquals("Result should match first NH3 value",
        NH3_MISSING_VALUES[0], processed.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), EPSILON);
    assertEquals("No CO2 concentration should be present", 0, processed.getEmissionResult(EmissionResultKey.NO2_CONCENTRATION), EPSILON);

    HighestValuePerHexagonService.mergeResults(results[2], processed);
    // expected third result as NOX + NH3 of the third result is higher than the previous result
    assertEquals("Result should match third NOX value",
        NOX_MISSING_VALUES[2], processed.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), EPSILON);
    assertEquals("Result should match third NH3 value",
        NH3_MISSING_VALUES[2], processed.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), EPSILON);
    assertEquals("No CO2 concentration should be present", 0, processed.getEmissionResult(EmissionResultKey.NO2_CONCENTRATION), EPSILON);
  }

  @Test
  public void totalValue() {
    final AeriusResultPoint[] results = createResultPoints(NOX_VALUES, NH3_VALUES);

    final AeriusResultPoint processed = new AeriusResultPoint();
    TotalValuePerHexagonService.mergeResults(results[0], processed);
    TotalValuePerHexagonService.mergeResults(results[1], processed);

    assertEquals("Result should be the sum of the first 2 NOX values",
        DoubleStream.of(Arrays.copyOfRange(NOX_VALUES, 0, 2)).sum(), processed.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), EPSILON);
    assertEquals("Result should be the sum of the first 2 NH3 values",
        DoubleStream.of(Arrays.copyOfRange(NH3_VALUES, 0, 2)).sum(), processed.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), EPSILON);

    TotalValuePerHexagonService.mergeResults(results[2], processed);
    assertEquals("Result should be the sum all the NOX values",
        DoubleStream.of(NOX_VALUES).sum(), processed.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), EPSILON);
    assertEquals("Result should be the sum all the NH3 values",
        DoubleStream.of(NH3_VALUES).sum(), processed.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), EPSILON);
  }

  @Test
  public void deltaValue() {
    final AeriusResultPoint[] results = createResultPoints(NOX_VALUES_DELTA, NH3_VALUES_DELTA);

    final AeriusResultPoint processed = new AeriusResultPoint();
    // proposed first - that's why the index used is 1 instead of 0
    DeltaValuePerHexagonService.mergeResults(1, results[1], processed, false);
    DeltaValuePerHexagonService.mergeResults(2, results[0], processed, false);

    assertEquals("Result should be the second NOX value subtracted from the first NOX value",
        NOX_VALUES_DELTA[1] - NOX_VALUES_DELTA[0], processed.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), EPSILON);
    assertEquals("Result should be the second NH3 value subtracted from the first NH3 value",
        NH3_VALUES_DELTA[1] - NH3_VALUES_DELTA[0], processed.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), EPSILON);
  }

  @Test
  public void deltaValueNoIncreasement() {
    final AeriusResultPoint[] results = createResultPoints(NOX_VALUES_DELTA, NH3_VALUES_DELTA);

    final AeriusResultPoint processed = new AeriusResultPoint();
    // proposed first - that's why the index used is 1 instead of 0
    DeltaValuePerHexagonService.mergeResults(1, results[1], processed, true);
    DeltaValuePerHexagonService.mergeResults(2, results[0], processed, true);

    assertEquals("Only increasement NOX check",
        Math.max(0, NOX_VALUES_DELTA[1] - NOX_VALUES_DELTA[0]), processed.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), EPSILON);
    assertEquals("Only increasement NH3 check",
        Math.max(0, NH3_VALUES_DELTA[1] - NH3_VALUES_DELTA[0]), processed.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), EPSILON);
  }

  private AeriusResultPoint[] createResultPoints(final double[] noxValues, final double[] nh3Values) {
    assertEquals("value arrays should have the same size", noxValues.length, nh3Values.length);

    final AeriusResultPoint[] results = new AeriusResultPoint[noxValues.length];
    for (int i = 0; i < results.length; ++i) {
      results[i] = new AeriusResultPoint();
    }

    addResults(results, EmissionResultKey.NOX_DEPOSITION, noxValues);
    addResults(results, EmissionResultKey.NH3_DEPOSITION, nh3Values);

    return results;
  }

  private void addResults(final AeriusResultPoint[] results, final EmissionResultKey key, final double[] values) {
    assertEquals("amount of results should match the amount of values", results.length, values.length);
    for (int i = 0; i < results.length; i++) {
      if (values[i] != 0) {
        results[i].setEmissionResult(key, values[i]);
      }
    }
  }
}
