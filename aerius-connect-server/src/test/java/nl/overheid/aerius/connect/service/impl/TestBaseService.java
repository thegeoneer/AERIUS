/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.io.IOException;

import org.junit.Before;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.MockChannel;
import nl.overheid.aerius.taskmanager.client.MockConnection;

/**
 * Base class to setup generic part for ServiceImpl classes.
 */
public class TestBaseService extends BaseDBTest {

  protected BrokerConnectionFactory factory;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    final MockChannel mockChannel = new MockChannel() {
      @Override
      public void basicPublish(final String exchange, final String routingKey, final boolean mandatory, final boolean immediate,
          final BasicProperties props, final byte[] body) throws IOException {
      }
    };
    factory = new BrokerConnectionFactory(EXECUTOR) {
      @Override
      protected Connection createNewConnection() throws IOException {
        return new MockConnection() {
          @Override
          public Channel createChannel() throws IOException {
            return mockChannel;
          }
        };
      }
    };
  }
}
