/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.impl.util.Base64;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ValidateApiServiceImpl}.
 */
public class ValidateApiServiceImplTest extends BaseDBTest {
  private static final String VALIDATION_FAILED = "Validation failed.";

  private ValidateApiServiceImpl validator;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    validator = new ValidateApiServiceImpl(new ConnectServiceContext(getCalcPMF(), null));
  }

  @Test
  public void testGmlValidationOk() throws IOException, AeriusException {
    final String content = FileUtil.readFile("beach_race_track.gml");
    final ValidateResponse result = validator.validate(DataType.GML, ContentType.TEXT, content, false, false, null);

    assertTrue(VALIDATION_FAILED, result.getSuccessful());
  }

  @Test
  public void testGmlValidationOkBase64() throws IOException, AeriusException {
    final String content = FileUtil.readFile("beach_race_track.gml");
    final String base64Content = new String(Base64.encode(content.getBytes(StandardCharsets.UTF_8)));
    final ValidateResponse result = validator.validate(DataType.GML, ContentType.BASE64, base64Content, false, false, null);

    assertTrue(VALIDATION_FAILED, result.getSuccessful());
  }

  @Test
  public void testGmlValidationOkZip() throws IOException, AeriusException {
    final String content = new String(Base64.encode(IOUtils.toByteArray(FileUtil.class.getResourceAsStream("beach_race_track.zip"))));
    final ValidateResponse result = validator.validate(DataType.ZIP, ContentType.BASE64, content, false, false, null);

    assertTrue(VALIDATION_FAILED, result.getSuccessful());
  }

  @Test
  public void testGmlValidationNotOk() throws IOException, AeriusException {
    final String content = FileUtil.readFile("farm_case_1.gml");
    final ValidateResponse result = validator.validate(DataType.GML, ContentType.TEXT, content, false, false, null);

    assertFalse("Validation gml succesful, while expected otherwise.", result.getSuccessful());
  }

  @Test(expected = AeriusException.class)
  public void testCsvValidationNotOk() throws IOException, AeriusException {
    final String content = FileUtil.readFile("srm2_roads_invalid.csv");
    final ValidateResponse result = validator.validate(DataType.CSV, ContentType.TEXT, content, false, false, null);

    assertFalse("Validation csv succesful, while expected otherwise.", result.getSuccessful());
  }
}
