/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.UUID;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.db.scenario.ScenarioUserRepository;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link UserUtil}. A  fair part is already tested via the service implementations, but not all.
 */
public class UserUtilTest extends BaseDBTest {

  private static final String TEST_EMAIL = "aerius@example.com";

  @Test
  public void testConcurrentJobsLimit() throws AeriusException, SQLException {
    UserUtil.generateAPIKey(getCalcConnection(), TEST_EMAIL);
    final ScenarioUser user = ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL);

    for (int i = 0; i < user.getMaxConcurrentJobs(); ++i) {
      // Just make sure we don't get an USER_MAX_CONCURRENT_JOB_LIMIT_REACHED before the limit is reached.
      UserUtil.getUserWhileValidatingMaximumConcurrentJobs(getCalcConnection(), user.getApiKey());

      JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, UUID.randomUUID().toString());
    }

    // Should pass as the check isn't done.
    UserUtil.getUserWithoutValidatingMaximumConcurrentJobs(getCalcConnection(), user.getApiKey());

    boolean foundProperException = false;
    try {
      UserUtil.getUserWhileValidatingMaximumConcurrentJobs(getCalcConnection(), user.getApiKey());
    } catch (final AeriusException e) {
      assertSame("Should throw USER_MAX_CONCURRENT_JOB_LIMIT_REACHED", e.getReason(), Reason.USER_MAX_CONCURRENT_JOB_LIMIT_REACHED);
      foundProperException = true;
    }

    if (!foundProperException) {
      fail("Should have triggered an USER_MAX_CONCURRENT_JOB_LIMIT_REACHED.");
    }
  }

}
