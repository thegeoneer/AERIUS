/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.impl.util.Base64;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.ConvertResponse;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.ValidationMessage;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link ValuePerHexagonApiServiceImpl}.
 */
public class ValuePerHexagonApiServiceImplTest extends BaseDBTest {
  private static final String CONTENT_TYPE_NOT_EXPECTED = "ContentType not {0}";
  private static final String DATATYPE_NOT_EXPECTED = "DataType not {0}";
  private static final String MISSING_FILE_DATA = "Missing file data.";
  private static final String MERGING_FAILED = "Merging failed.";

  private ConnectServiceContext context;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    context = new ConnectServiceContext(getCalcPMF(), null);
  }

  @Test
  public void testGmlMergeOk() throws IOException, AeriusException {
    final ArrayList<DataObject> dataObjects = new ArrayList<DataObject>();
    dataObjects.add(createDataObject("mergetest1_1.gml", ContentType.TEXT));
    dataObjects.add(createDataObject("mergetest1_2.gml", ContentType.TEXT));

    assertAndRunTests(dataObjects, ContentType.TEXT, DataType.GML);
  }

  @Test
  public void testGmlMergeOkBase64() throws IOException, AeriusException {
    final ArrayList<DataObject> dataObjects = new ArrayList<DataObject>();
    dataObjects.add(createDataObject("mergetest1_1.gml", ContentType.BASE64));
    dataObjects.add(createDataObject("mergetest1_2.gml", ContentType.BASE64));

    assertAndRunTests(dataObjects, ContentType.TEXT, DataType.GML);
  }

  @Test
  public void testZipMergeOk() throws IOException, AeriusException {
    final ArrayList<DataObject> dataObjects = new ArrayList<DataObject>();
    dataObjects.add(createDataObject("mergetest1_1.zip", ContentType.BASE64));
    dataObjects.add(createDataObject("mergetest1_2.gml", ContentType.TEXT));

    assertAndRunTests(dataObjects, ContentType.BASE64, DataType.ZIP);
  }

  @Test
  public void testGmlMergeInvalidFile() throws IOException, AeriusException {
    final ArrayList<DataObject> dataObjects = new ArrayList<DataObject>();
    dataObjects.add(createDataObject("mergetest1_1.gml", ContentType.TEXT));
    dataObjects.add(createDataObject("mergetest2.gml", ContentType.TEXT));

    assertInvalidGMLResult(HighestValuePerHexagonService.highestValuePerHexagon(context, dataObjects));
    assertInvalidGMLResult(TotalValuePerHexagonService.totalValuePerHexagon(context, dataObjects));
    assertInvalidGMLResult(DeltaValuePerHexagonService.deltaValuePerHexagon(context, dataObjects.get(0), dataObjects.get(1), true));
  }

  private DataObject createDataObject(final String filename, final ContentType contentType) throws IOException {
    final DataType dataType = DataType.valueOf(FilenameUtils.getExtension(filename).toUpperCase());

    assertNotNull("determining dataType based on filename failed", dataType);

    final DataObject obj = new DataObject()
        .contentType(contentType)
        .dataType(dataType)
        .data(
          contentType == ContentType.TEXT
          ? FileUtil.readFile(filename)
          : new String(Base64.encode(IOUtils.toByteArray(FileUtil.class.getResourceAsStream(filename)))));

    return obj;
  }

  private void assertAndRunTests(final ArrayList<DataObject> dataObjects, final ContentType expectedContentType, final DataType expectedDataType)
      throws AeriusException {
    assertSuccessfulResult(HighestValuePerHexagonService.highestValuePerHexagon(context, dataObjects), expectedContentType, expectedDataType);
    assertSuccessfulResult(TotalValuePerHexagonService.totalValuePerHexagon(context, dataObjects), expectedContentType, expectedDataType);
    // Only if the test case contains 2 data objects a delta run can be done.
    if (dataObjects.size() == 2) {
      assertSuccessfulResult(
          DeltaValuePerHexagonService.deltaValuePerHexagon(context, dataObjects.get(0), dataObjects.get(1), true),
          expectedContentType, expectedDataType);

    }
  }

  private void assertSuccessfulResult(final ConvertResponse result, final ContentType expectedContentType, final DataType expectedDataType) {
    assertTrue(MERGING_FAILED, result.getSuccessful());
    assertNotNull(MISSING_FILE_DATA, result.getDataObject().getData());
    assertSame(MessageFormat.format(CONTENT_TYPE_NOT_EXPECTED, expectedContentType), expectedContentType, result.getDataObject().getContentType());
    assertSame(MessageFormat.format(DATATYPE_NOT_EXPECTED, expectedDataType), expectedDataType, result.getDataObject().getDataType());
  }

  private void assertInvalidGMLResult(final ConvertResponse result) {
    final List<ValidationMessage> errors = result.getErrors();
    assertFalse("Should have errors", errors.isEmpty());
    assertEquals("Should have exception: GML_VALIDATION_FAILED", Reason.GML_VALIDATION_FAILED.getErrorCode(), errors.get(0).getCode().intValue());
  }
}
