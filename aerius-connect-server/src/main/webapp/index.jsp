<%@page import="nl.overheid.aerius.enums.ConstantsEnum"%>
<%@page import="nl.overheid.aerius.db.common.ConstantRepository"%>
<%@page import="nl.overheid.aerius.server.ServerPMF"%>
<%
    String redirectURL = ConstantRepository.getString(ServerPMF.getInstance(), ConstantsEnum.CONNECT_ROOT_REDIRECT_URL);
    response.sendRedirect(redirectURL);
%>
