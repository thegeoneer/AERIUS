/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.connect.conversion.ScenarioInputReader;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.ScenarioInput;
import nl.overheid.aerius.connect.domain.ValidationMessage;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * @author AERIUS
 *
 */
public final class ConnectSituationUtil {

  /**
   * Class should never be instantiated.
   */
  private ConnectSituationUtil() {
    super();
  }

  /**
   * Collects all the input data.
   * @param scenario
   * @param situation1
   * @param situation2
   * @param customPoints
   * @param validateSchema if true the file will be validated against the XSD (only for IMAER)
   * @param context
   * @return
   * @throws AeriusException
   * @throws IOException
   * @throws SQLException
   */
  public static List<ValidationMessage> collectInput(final CalculatedScenario scenario, final DataObject[] situation1, final DataObject[] situation2,
      final boolean customPoints, final boolean validateMetaData, final boolean validateSchema, final ConnectServiceContext context)
          throws AeriusException, SQLException, IOException {
    if (situation1.length == 0) {
      throw new AeriusException(Reason.CONNECT_NO_SOURCES);
    }

    final List<ValidationMessage> errors = new ArrayList<>();

    if (scenario instanceof CalculatedSingle) {
      final CalculatedSingle single = (CalculatedSingle) scenario;
      errors.addAll(setSituation(situation1, single.getSources(), scenario,
          new ImportParameters(customPoints, validateMetaData, true, validateSchema), context));
      if (errors.isEmpty()) {
        // get and set sources because setSources does some side-effects...
        single.setSources(single.getSources());
      }
    } else if (scenario instanceof CalculatedComparison) {
      final CalculatedComparison comparison = (CalculatedComparison) scenario;
      errors.addAll(setSituation(situation1, comparison.getSourcesOne(), scenario,
          new ImportParameters(customPoints, validateMetaData, true, validateSchema), context));
      errors.addAll(setSituation(situation2, comparison.getSourcesTwo(), scenario,
          new ImportParameters(customPoints, validateMetaData, true, validateSchema), context));
      if (errors.isEmpty()) {
        // get and set sources because setSources does some side-effects...
        comparison.setSourcesOne(comparison.getSourcesOne());
        comparison.setSourcesTwo(comparison.getSourcesTwo());
      }
    }
    return errors;
  }

  /**
   * Handles import of the list of data objects.
   * @param data input data objects.
   * @param esl emission source list to add sources.
   * @param scenario scenario to set meta data and custom points.
   * @param customPoints if true reads the custom points and sets them in the scenario
   * @param validateMetaData if true if meta data is read it validates the meta data
   * @param readMetaData if true reads the meta data from the first data object
   * @param validateSchema if true the file will be validated against the XSD (only for IMAER)
   * @param context
   * @return
   * @throws AeriusException
   * @throws IOException
   * @throws SQLException
   */
  public static List<ValidationMessage> setSituation(final DataObject[] data, final EmissionSourceList esl, final CalculatedScenario scenario,
    final ImportParameters situationControl, final ConnectServiceContext context) throws AeriusException, SQLException, IOException {
    if (data.length == 0) {
      throw new AeriusException(Reason.CONNECT_NO_SOURCES);
    }
    final List<ValidationMessage> errors = new ArrayList<>();
    boolean first = situationControl.isReadMetaData();
    for (final DataObject dataObject : data) {
      // only validate meta data on first data object if validation is needed at all.
      final boolean doValidateMetaData = situationControl.isValidateMetaData() && first;
      final ScenarioInputReader inputReader = new ScenarioInputReader(context) {
        @Override
        protected Importer getImporter() throws SQLException, AeriusException {
          final Importer importer = super.getImporter();
          importer.setUseValidMetaData(doValidateMetaData);
          return importer;
        }
      };
      final ScenarioInput input = inputReader.run(dataObject.getDataType(), dataObject.getContentType(), dataObject.getData(), true, false, false,
          false, null,
          situationControl.isValidateSchema());
      first = processScenarioInput(esl, scenario, situationControl.isCustomPoints(), errors, first, input);
      if(!errors.isEmpty()){
        break;
      }
    }
    return errors;
  }

  /**
   * @param esl
   * @param scenario
   * @param customPoints
   * @param errors
   * @param first
   * @param input
   * @return
   */
  private static boolean processScenarioInput(final EmissionSourceList esl, final CalculatedScenario scenario, final boolean customPoints,
      final List<ValidationMessage> errors, boolean first, final ScenarioInput input) {
    if (Boolean.TRUE.equals(input.getSuccessful())) {
      if (first) {
        scenario.setMetaData(input.getImportResult().getMetaData());
        first = false;
      }
      esl.addAll(input.getImportResult().getSourceLists().get(0));
      if (StringUtils.isEmpty(esl.getName())) {
        esl.setName(input.getImportResult().getSourceLists().get(0).getName());
      }
      if (customPoints) {
        scenario.getCalculationPoints().addAll(input.getImportResult().getCalculationPoints());
      }
    } else {
      errors.addAll(input.getErrors());
     }
    return first;
  }

  private static class ImportParameters{

    private final boolean customPoints;
    private final boolean validateMetaData;
    private final boolean readMetaData;
    private final boolean validateSchema;

    public ImportParameters(final boolean customPoints, final boolean validateMetaData, final boolean readMetaData, final boolean validateSchema) {
      super();
      this.customPoints = customPoints;
      this.validateMetaData = validateMetaData;
      this.readMetaData = readMetaData;
      this.validateSchema = validateSchema;
    }

    public boolean isCustomPoints() {
      return customPoints;
    }

    public boolean isValidateMetaData() {
      return validateMetaData;
    }

    public boolean isReadMetaData() {
      return readMetaData;
    }

    public boolean isValidateSchema() {
      return validateSchema;
    }
  }

}
