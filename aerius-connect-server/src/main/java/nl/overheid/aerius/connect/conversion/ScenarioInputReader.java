/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.conversion;

import java.io.IOException;
import java.sql.SQLException;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.ScenarioInput;
import nl.overheid.aerius.server.util.AfterPASErrorsHandler;
import nl.overheid.aerius.server.util.AfterPASHandler;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;

public class ScenarioInputReader extends Converter<ScenarioInput> {

  public ScenarioInputReader(final ConnectServiceContext context) {
    super(new ConverterSpecifics<ScenarioInput>() {

      private final AfterPASHandler afterPASHandler = new AfterPASErrorsHandler(context.getLocale(), AfterPASHandler.AFTERPAS_2B_DETERMINED);

      @Override
      public void onSuccess(final ConnectServiceContext context, final ScenarioInput si, final ImportResult ir,
          final DataType dataType, final ContentType contentType)
          throws AeriusException, SQLException, IOException {
        si.dataObject(new DataObject().contentType(contentType).dataType(dataType));
        si.setImportResult(ir);
      }

      @Override
      public ScenarioInput create() {
        return new ScenarioInput();
      }

      @Override
      public AfterPASHandler getAfterPASHandler() {
        return afterPASHandler;
      }
    }, context);
  }

}
