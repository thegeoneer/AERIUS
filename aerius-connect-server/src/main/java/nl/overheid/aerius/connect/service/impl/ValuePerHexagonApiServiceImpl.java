/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.DeltaValuePerHexagonRequest;
import nl.overheid.aerius.connect.domain.HighestValuePerHexagonRequest;
import nl.overheid.aerius.connect.domain.TotalValuePerHexagonRequest;
import nl.overheid.aerius.connect.service.NotFoundException;
import nl.overheid.aerius.connect.service.ValuePerHexagonApiService;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Implementation of {@link ValuePerHexagonApiService}.
 */
public class ValuePerHexagonApiServiceImpl extends ValuePerHexagonApiService {

  private final ConnectServiceContext context;

  public ValuePerHexagonApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  ValuePerHexagonApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
  }

  @Override
  public Response postHighestValuePerHexagon(final HighestValuePerHexagonRequest request, final SecurityContext securityContext)
      throws NotFoundException {
    try {
      return Response.ok(HighestValuePerHexagonService.highestValuePerHexagon(context, request.getDataObjects())).build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

  @Override
  public Response postDeltaValuePerHexagon(final DeltaValuePerHexagonRequest request, final SecurityContext securityContext)
      throws NotFoundException {
    try {
      return Response.ok(
          DeltaValuePerHexagonService.deltaValuePerHexagon(
              context, request.getCurrent(), request.getProposed(), request.getOnlyIncreasement())).build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

  @Override
  public Response postTotalValuePerHexagon(final TotalValuePerHexagonRequest request, final SecurityContext securityContext)
      throws NotFoundException {
    try {
      return Response.ok(TotalValuePerHexagonService.totalValuePerHexagon(context, request.getDataObjects())).build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

}
