/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.domain;

import nl.overheid.aerius.shared.domain.importer.ImportResult;

public class ScenarioInput extends ConvertResponse {

  private ImportResult importResult;

  public ImportResult getImportResult() {
    return importResult;
  }

  public void setImportResult(final ImportResult importResult) {
    this.importResult = importResult;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    final int result = super.hashCode();
    return prime * result + ((importResult == null) ? 0 : importResult.hashCode());
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null
        && this.getClass() == obj.getClass()
        && super.equals(obj) // not switching these
        && ((importResult == null && ((ScenarioInput) obj).importResult == null)
            || (importResult != null && importResult.equals(((ScenarioInput) obj).importResult)));
  }

}
