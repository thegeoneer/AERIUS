/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.conversion;

import java.io.IOException;
import java.sql.SQLException;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.server.util.AfterPASHandler;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;

public interface ConverterSpecifics<V extends ValidateResponse> {

  /**
   * @return Returns an Object initialized to handle AfterPAS errors for the specific API-call.
   */
  AfterPASHandler getAfterPASHandler();

  /**
   * Called when input was correctly processed.
   * @param vr object of validation result
   * @param ir ImportResult
   * @param dataType data type of input
   * @param contentType content type of input
   * @throws IOException
   * @throws SQLException
   * @throws AeriusException
   */
  void onSuccess(ConnectServiceContext context, V vr, ImportResult ir, DataType dataType, ContentType contentType)
      throws AeriusException, SQLException, IOException;

  /**
   * Creates the return object which is a specific instance of ValidateResult.
   * Allows one to create a class with extra members which can be filled when
   *  {@link #onSuccess(ConnectServiceContext, V, ImportResult, DataType, ContentType)} is called.
   * @return new instance
   */
  V create();
}
