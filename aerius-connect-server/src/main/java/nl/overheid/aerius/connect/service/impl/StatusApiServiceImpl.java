/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.JobState;
import nl.overheid.aerius.connect.domain.JobType;
import nl.overheid.aerius.connect.domain.StatusJobProgress;
import nl.overheid.aerius.connect.domain.StatusJobResponse;
import nl.overheid.aerius.connect.service.NotFoundException;
import nl.overheid.aerius.connect.service.StatusApiService;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.connect.service.util.ValidationUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.shared.domain.scenario.JobProgress;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Implementation of {@link StatusApiService}.
 */
public class StatusApiServiceImpl extends StatusApiService {
  private static final Logger LOG = LoggerFactory.getLogger(StatusApiServiceImpl.class);

  private final ConnectServiceContext context;

  public StatusApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  StatusApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
  }

  @Override
  public Response getStatusJobs(final String apiKey, final SecurityContext securityContext) throws NotFoundException {
    try {
      return Response.ok(jobs(apiKey)).build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

  @Override
  public Response getStatusJobById(final String apiKey, final String jobKey, final SecurityContext securityContext) throws NotFoundException {
    try {
      return Response.ok(jobById(apiKey, jobKey)).build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

  StatusJobResponse jobs(final String apiKey) throws AeriusException {
    final StatusJobResponse result = new StatusJobResponse();
    try {
      final PMF pmf = context.getPMF();
      try (final Connection con = pmf.getConnection()) {
        final ScenarioUser user = UserUtil.getUserWithoutValidatingMaximumConcurrentJobs(con, apiKey);
        result.entries(convert(JobRepository.getProgressForUser(con, user)));
      }
      // catching exception is allowed here
    } catch (final Exception e) {
      LOG.error("Fetching the status of jobs failed:", e);
      throw AeriusExceptionConversionUtil.convert(e, context);
    }

    return result;
  }

  StatusJobResponse jobById(final String apiKey, final String jobKey) throws AeriusException {
    final StatusJobResponse result = new StatusJobResponse();
    try {
      final PMF pmf = context.getPMF();
      try (final Connection con = pmf.getConnection()) {
        final ScenarioUser user = UserUtil.getUserWithoutValidatingMaximumConcurrentJobs(con, apiKey);
        ValidationUtil.validateJobKey(con, user, jobKey);
        result.addEntriesItem(convert(JobRepository.getProgressForUserAndKey(con, user, jobKey)));
      }
      // catching exception is allowed here
    } catch (final Exception e) {
      LOG.error("Fetching the status of job with id:{} failed: {}", jobKey, e);
      throw AeriusExceptionConversionUtil.convert(e, context);
    }

    return result;
  }

  private List<StatusJobProgress> convert(final List<JobProgress> collection) {
    final List<StatusJobProgress> progresses = new ArrayList<>();

    for (final JobProgress item : collection) {
      progresses.add(convert(item));
    }

    return progresses;
  }

  private StatusJobProgress convert(final JobProgress value) {
    return new StatusJobProgress()
        .name(value.getName())
        .startDateTime(value.getStartDateTime())
        .endDateTime(value.getEndDateTime())
        .hectareCalculated(value.getHexagonCount())
        .state(getState(value))
        .type(JobType.valueOf(value.getType().name()))
        .key(value.getKey())
        .resultUrl(value.getResultUrl());
  }

  private JobState getState(final JobProgress value) {
    JobState state = JobState.QUEUED;

    if (value.getState() != null) {
      switch (value.getState()) {
      case RUNNING:
        state = JobState.RUNNING;
        break;
      case CANCELLED:
      case ERROR:
        state = JobState.CANCELLED;
        break;
      case COMPLETED:
        state = JobState.COMPLETED;
        break;
      case UNDEFINED:
      case INITIALIZED:
      default:
        break;
      }
    }

    return state;
  }

}
