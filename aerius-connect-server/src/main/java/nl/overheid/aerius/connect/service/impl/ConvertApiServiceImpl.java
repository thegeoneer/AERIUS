/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.io.IOException;
import java.sql.SQLException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.conversion.Converter;
import nl.overheid.aerius.connect.conversion.ConverterSpecifics;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.ConvertRequest;
import nl.overheid.aerius.connect.domain.ConvertResponse;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.Substance;
import nl.overheid.aerius.connect.service.ConvertApiService;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.DataObjectUtil;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.server.util.AfterPASHandler;
import nl.overheid.aerius.server.util.AfterPASWarningsHandler;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Server side implementation of {@link ConvertApiService}.
 */
public class ConvertApiServiceImpl extends ConvertApiService {

  private static final Logger LOG = LoggerFactory.getLogger(ConvertApiServiceImpl.class);

  private final ConnectServiceContext context;
  private final Converter<ConvertResponse> converter;

  private final AfterPASHandler afterPASHandler;

  private final ConverterSpecifics<ConvertResponse> implementation = new ConverterSpecifics<ConvertResponse>() {

    @Override
    public void onSuccess(final ConnectServiceContext context, final ConvertResponse cr, final ImportResult ir, final DataType dataType,
        final ContentType contentType) throws AeriusException, SQLException, IOException {
      cr.setDataObject(DataObjectUtil.build(context.getPMF(), ir, dataType));
    }

    @Override
    public ConvertResponse create() {
      return new ConvertResponse();
    }

    @Override
    public AfterPASHandler getAfterPASHandler() {
      return afterPASHandler;
    }
  };

  public ConvertApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  ConvertApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
    this.converter = new Converter<>(implementation, context);
    afterPASHandler = new AfterPASWarningsHandler(context.getLocale(), AfterPASHandler.AFTERPAS_CONVERT_INVALID_INPUT_HASBEENREMOVED);
  }

  @Override
  public Response postConvert(final ConvertRequest request, final SecurityContext securityContext) {
    try {
      final DataObject dataObject = request.getDataObject();
      return Response.ok(
          convert(dataObject.getDataType(), dataObject.getContentType(), dataObject.getData(), dataObject.getSubstance())).build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

  ConvertResponse convert(final DataType dataType, final ContentType contentType, final String data, final Substance substance)
      throws AeriusException {
    ConvertResponse result = null;

    try {
      result = converter.run(dataType, contentType, data, true, true, false, false, substance, true);
    } catch (final Exception e) {
      LOG.error("Conversion failed:", e);
      throw AeriusExceptionConversionUtil.convert(e, converter.getLocale());
    }
    return result;
  }

}
