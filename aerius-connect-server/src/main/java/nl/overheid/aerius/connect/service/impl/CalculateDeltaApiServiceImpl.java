/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.domain.CalculateDeltaRequest;
import nl.overheid.aerius.connect.domain.CalculateResponse;
import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.domain.CalculationOptions.CalculationTypeEnum;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.SituationDataObject;
import nl.overheid.aerius.connect.domain.ValidationMessage;
import nl.overheid.aerius.connect.service.CalculateDeltaApiService;
import nl.overheid.aerius.connect.service.NotFoundException;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.ConnectAfterPASRestrictionsUtil;
import nl.overheid.aerius.connect.service.util.OptionUtil;
import nl.overheid.aerius.connect.service.util.SituationDataObjectProcessor;
import nl.overheid.aerius.connect.service.util.ConnectSituationUtil;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.export.ExportTaskClient;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.NoopCallback;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Server side implementation of the {@link CalculateDeltaApiService}.
 */
public class CalculateDeltaApiServiceImpl extends CalculateDeltaApiService {

  private static final Logger LOG = LoggerFactory.getLogger(CalculateDeltaApiServiceImpl.class);

  private final ConnectServiceContext context;

  public CalculateDeltaApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  CalculateDeltaApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
  }

  @Override
  public Response postCalculateDelta(final CalculateDeltaRequest request, final SecurityContext securityContext) throws NotFoundException {
    final CalculateResponse response;

    try {
      response = processCalculateDelta(request.getOptions(), request.getSituationDataObjects(), request.getApiKey());
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }

    return Response.ok(response).build();
  }

  CalculateResponse processCalculateDelta(final CalculationOptions options, final List<SituationDataObject> situationDataObjects,
      final String apiKey) throws AeriusException {
    try {
      checkAfterPASLegal(options);

      final SituationDataObjectProcessor processor = new SituationDataObjectProcessor();
      processor.process(situationDataObjects);
      if (processor.isProposedEmpty()) {
        throw new AeriusException(Reason.CONNECT_SITUATION_NO_PROPOSED);
      }

      // Later: refactor report to allow current and proposed instead of 'situations'. This is fugly.
      if (processor.isCurrentEmpty()) {
        return calculateDelta(options, processor.getProposed(), null, apiKey);
      } else {
        return calculateDelta(options, processor.getCurrent(), processor.getProposed(), apiKey);
      }
    } catch (final Exception e) {
      LOG.error("Starting calculateDelta failed", e);
      throw AeriusExceptionConversionUtil.convert(e, context);
    }

  }

  /**
   * @param options
   * @throws AeriusException
   */
  private void checkAfterPASLegal(final CalculationOptions options) throws AeriusException {
    ConnectAfterPASRestrictionsUtil.errorIfTemporaryProject(options, context.getLocale(), "APPLICATION_TEMPPROJECTYEARS_UNSUPPORTED",
        "APPLICATION_CONNECT_NOREP");
  }

  CalculateResponse calculateDelta(final CalculationOptions options, final DataObject[] situation1, final DataObject[] situation2,
      final String apiKey) throws AeriusException, SQLException, IOException {
    final PMF pmf = context.getPMF();

    final ScenarioUser user;
    try (final Connection con = pmf.getConnection()) {
      user = UserUtil.getUserWhileValidatingMaximumConcurrentJobs(con, apiKey);
    }

    final CalculatedScenario scenario = createCalculatedScenario(situation2);
    final boolean isCustomPoints = options.getCalculationType() == CalculationTypeEnum.CUSTOM_POINTS;
    final boolean validateMetadata = false;
    final List<ValidationMessage> errors = ConnectSituationUtil.collectInput(scenario, situation1, situation2,
        isCustomPoints, validateMetadata, isCustomPoints && Boolean.TRUE.equals(options.getValidate()), context);
    final ExportProperties exportProperties = OptionUtil.getExportProperties(options,  ExportType.GML_WITH_RESULTS, user.getEmailAddress(),
        scenario.getScenario().getMetaData());
    final CalculateResponse job = new CalculateResponse();

    job.successful(Boolean.FALSE);
    if (errors.isEmpty()) {
      OptionUtil.setCalculationOptions(scenario, options);

      final String correlationId = ExportTaskClient.startPAAExport(context.getClientFactory(), WorkerType.CONNECT, QueueEnum.CONNECT_PAA_EXPORT,
          exportProperties, scenario, new NoopCallback());

      job.setKey(correlationId);
      try (final Connection con = context.getPMF().getConnection()) {
        JobRepository.createJob(con, user, JobType.CALCULATE_DELTA, correlationId, options.getName());
      }

      job.successful(Boolean.TRUE);
    } else {
      job.errors(errors);
    }

    return job;
  }

  /**
   * Creates the {@link CalculatedScenario} object.
   * @param situation2 if situation 2 is null or empty it's a single calculation else a comparison calculation.
   * @return
   */
  private CalculatedScenario createCalculatedScenario(final DataObject... situation2) {
    return situation2 == null || situation2.length == 0 ? new CalculatedSingle() : new CalculatedComparison();
  }


}
