/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ConvertResponse;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.service.ValuePerHexagonApiService;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.valueperhexagon.ValuePerHexagonHelper;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Implementation of {@link ValuePerHexagonApiService}, specifically the delta part.
 */
public final class DeltaValuePerHexagonService {
  private static final Logger LOG = LoggerFactory.getLogger(DeltaValuePerHexagonService.class);

  private DeltaValuePerHexagonService() {
    // not to be constructed
  }

  static ConvertResponse deltaValuePerHexagon(final ConnectServiceContext context, final DataObject current, final DataObject proposed,
      final boolean onlyIncreasement) throws AeriusException {
    LOG.debug("Received files to determine delta values per hexagon for");

    try {
      final ValuePerHexagonHelper helper =
          new ValuePerHexagonHelper((fileIndex, point, mergedResultPoint) -> mergeResults(fileIndex, point, mergedResultPoint, onlyIncreasement));

      final List<DataObject> dataObjects = new ArrayList<>();
      dataObjects.add(proposed);
      dataObjects.add(current);
      return helper.valuePerHexagon(context, dataObjects);
    } catch (final Exception e) {
      LOG.error("Determining delta value failed unexpectedly:", e);
      throw AeriusExceptionConversionUtil.convert(e, LocaleUtils.getDefaultLocale());
    }
  }

  static void mergeResults(final int fileIndex, final AeriusResultPoint point, final AeriusResultPoint mergedResultPoint,
      final boolean onlyIncreasement) {
    // should add value if first file (proposed), should subtract if second file (current)
    final boolean subtract = fileIndex == 1 ? false : true;

    // multiplying something you are going to add by -1 is the same as subtracting and multiplying by 1 will still add.
    // i.e. adding 2 + (1 x 6) = 2 + 6 = 8.. adding 2 + (-1 x 6) = 2 + -6 = -4.
    final int multiplier = subtract ? -1 : 1;

    addValueToSubstance(mergedResultPoint,
        EmissionResultKey.NOX_DEPOSITION, multiplier * point.getEmissionResult(EmissionResultKey.NOX_DEPOSITION), onlyIncreasement);
    addValueToSubstance(mergedResultPoint,
        EmissionResultKey.NH3_DEPOSITION, multiplier * point.getEmissionResult(EmissionResultKey.NH3_DEPOSITION), onlyIncreasement);
  }

  private static void addValueToSubstance(final AeriusResultPoint point, final EmissionResultKey key, final double value,
      final boolean onlyIncreasement) {
    // use BigDecimal to make sure the result doesn't contain any weird fractions
    final double result = BigDecimal.valueOf(point.getEmissionResult(key)).add(BigDecimal.valueOf(value)).doubleValue();

    point.setEmissionResult(key, onlyIncreasement && result < 0 ? 0 : result);
  }
}
