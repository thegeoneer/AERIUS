/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.importer;

import java.util.ArrayList;

import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.service.RetrieveImportServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.ui.importer.ImportConflictAction;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioImportDialogController;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioImportDialogPanel;
import nl.overheid.aerius.wui.scenario.base.place.ResultGraphicsPlace;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.processor.ImportOutputPollingAgent;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.geo.ScenarioMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.processor.ScenarioResultProcessor;

/**
 * Import controller for Scenario.
 */
public class ScenarioImportController extends ScenarioBaseImportController {

  private final ScenarioResultProcessor processor;
  private final ScenarioAppContext context;

  @Inject
  public ScenarioImportController(final ScenarioMapLayoutPanel map, final ScenarioAppContext context, final RetrieveImportServiceAsync service,
      final HelpPopupController hpC, final ScenarioResultProcessor processor) {
    super(map, context, service, hpC, true, true, true, true);
    this.context = context;
    this.processor = processor;
  }

  @Override
  protected final ScenarioBasePlace getImportCompletePlace(final int sid1, final int sid2, final ImportResult result, final Situation viewSituation,
      final String uuid) {
    service.getCalculations(uuid, new AppAsyncCallback<ArrayList<Calculation>>() {
      @Override
      public void onSuccess(final ArrayList<Calculation> result) {
        if (result.isEmpty()) {
          NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioConnectNoCalculationStored());
          return;
        }

        processor.initStart(result);
      }
    });
    final ResultGraphicsPlace rgp = new ResultGraphicsPlace(context.getPlaceController().getWhere());
    rgp.setsId1(sid1);
    rgp.setsId2(sid2);
    rgp.setSituation(viewSituation);
    // load up as priority project utilisation
    if (idController.isUtilisation() && sid2 != 0) {
      rgp.setJobType(JobType.PRIORITY_PROJECT_UTILISATION);
    } else if (idController.isUtilisation()) {
      rgp.setJobType(null);
    }
    return rgp;
  }

  @Override
  protected boolean cleanImport() {
    return true;
  }

  @Override
  protected ScenarioImportDialogController getImportDialogController(final Scenario scenario,
      final ScenarioBasePlace place, final ScenarioImportDialogPanel scenarioImportDialogPanel) {
    return new ScenarioImportDialogController(scenario.getSources(place.getActiveSituationId()),
        scenario.getCalculationPoints(), new ImportOutputPollingAgent(service), scenarioImportDialogPanel) {

      @Override
      public void handleImportResult(final ImportResult result, final ImportConflictAction conflictAction, final String uuid) {
        ScenarioImportController.this.handleImportResult(result, conflictAction, place, uuid);
      }

      @Override
      protected boolean shouldImportRightAway(final ImportResult result) {
        //for scenario, clear existing sources (and calculation points just to be sure) on a new import.
        scenario.getSourceLists().clear();
        scenario.getCalculationPoints().clear();
        return true;
      }

    };
  }
}
