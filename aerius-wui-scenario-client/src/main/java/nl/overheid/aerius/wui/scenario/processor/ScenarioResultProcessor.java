/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.processor;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationInitResult;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.service.CalculateServiceAsync;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.processor.CalculationHighByDistancePollingAgent;
import nl.overheid.aerius.wui.scenario.base.processor.CalculationInitPollingAgent;
import nl.overheid.aerius.wui.scenario.base.processor.CalculationPollingAgent;
import nl.overheid.aerius.wui.scenario.base.processor.ScenarioBaseCalculationProcessor;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;

/**
 * Fake calculation processor. The calculations for scenario are made on import, this processor just ensures the 'calculation-way' stays intact by
 * ensuring the calculation is tracked on the server (so it can be deleted at some point) and that the proper events are fired.
 *
 * Only reason for this to extend the ScenarioBaseCalculationProcessor is to avoid scenario somehow starting calculations.
 */
@Singleton
public class ScenarioResultProcessor extends ScenarioBaseCalculationProcessor {

  private final ScenarioAppContext appContext;
  private final EventBus eventBus;

  private String lastCalculationKey;

  @Inject
  public ScenarioResultProcessor(final ScenarioAppContext appContext, final CalculateServiceAsync service, final CalculationPollingAgent resultAgent,
      final CalculationInitPollingAgent initAgent, final CalculationHighByDistancePollingAgent progressAgent, final Context context,
      final EventBus eventBus) {
    super(service, resultAgent, initAgent, progressAgent, context, eventBus);

    this.appContext = appContext;
    this.eventBus = eventBus;
  }

  public void init(final ArrayList<Calculation> calculations) {
    updateUserContext(calculations);
  }

  public void start(final ArrayList<Calculation> calculations) {
    trackCalculationsOnServer(switchCalculationsIfPriorityProjectUtilisation(calculations));

    fireEventsAboutCalculations(calculations);
  }

  public void initStart(final ArrayList<Calculation> calculations) {
    init(calculations);
    start(calculations);
  }

  private ArrayList<Calculation> switchCalculationsIfPriorityProjectUtilisation(final ArrayList<Calculation> calculations) {
    final ArrayList<Calculation> result;

    final ScenarioBasePlace place = (ScenarioBasePlace) appContext.getPlaceController().getWhere();
    if (JobType.PRIORITY_PROJECT_UTILISATION == place.getJobType()) {
      result = new ArrayList<>();
      for (final Calculation calculation : calculations) {
        result.add(0, calculation);
      }
    } else {
      result = calculations;
    }

    return result;
  }

  private void updateUserContext(final ArrayList<Calculation> calculations) {
    final Scenario scenario = appContext.getUserContext().getScenario();
    scenario.getSourceLists().clear();
    for (final Calculation calculation : calculations) {
      EmissionSourceList sources = calculation.getSources();
      if (sources == null) {
        sources = new EmissionSourceList();
      }

      scenario.addSources(sources);
    }

    final int situationId2 = scenario.getSourceLists().size() == 2 ? 1 : -1;
    final CalculatedScenario calculatedScenario = CalculatedScenarioUtil.toCalculatedScenario(scenario, 0, situationId2);
    calculatedScenario.getCalculations().clear();
    calculatedScenario.getCalculations().addAll(calculations);
    appContext.getUserContext().setCalculatedScenario(calculatedScenario);
  }

  private void trackCalculationsOnServer(final ArrayList<Calculation> calculations) {
    // track the calculation on the server to ensure 'old' calculations will be removed when we're done with them.
    service.startCalculation(lastCalculationKey, new AppAsyncCallback<String>() {
      @Override
      public void onSuccess(final String calculationKey) {
        lastCalculationKey = calculationKey;
        service.trackCalculation(calculationKey, calculations, new AsyncCallback<Void>() {

          @Override
          public void onSuccess(final Void result) {
            // nothing to do.
          }

          @Override
          public void onFailure(final Throwable caught) {
            // not very important at this point.
          }
        });
      }

      @Override
      public void onFailure(final Throwable caught) {
        // not very important at this point.
      }
    });
  }

  private void fireEventsAboutCalculations(final ArrayList<Calculation> calculations) {
    // ensure result layer knows about the calculations.
    eventBus.fireEvent(new CalculationInitEvent(new CalculationInitResult(calculations.get(0).getOptions(), calculations)));

    // ensure summary information is retrieved.
    eventBus.fireEvent(new CalculationFinishEvent(true));
  }

  @Override
  protected void doStartCalculationServiceCall(final CalculatedScenario scenario, final String key, final AsyncCallback<String> callback) {
    // NO-OP, scenario isn't actually starting calculations...
  }

  @Override
  protected boolean isCalculationActual() {
    return true;
  }

}
