/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario;

import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.domain.context.ScenarioUserContext;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.wui.geo.LegendTitleFactory;
import nl.overheid.aerius.wui.geo.search.MapSearchAction;
import nl.overheid.aerius.wui.main.AeriusContextProvider;
import nl.overheid.aerius.wui.main.ApplicationInitializer;
import nl.overheid.aerius.wui.main.RootPanelFactory;
import nl.overheid.aerius.wui.main.RootPanelFactory.RootLayoutPanelFactoryImpl;
import nl.overheid.aerius.wui.main.context.AppContext;
import nl.overheid.aerius.wui.main.place.DefaultPlace;
import nl.overheid.aerius.wui.main.ui.ApplicationDisplay;
import nl.overheid.aerius.wui.main.ui.ApplicationRootMenuViewImpl;
import nl.overheid.aerius.wui.main.ui.ApplicationRootView;
import nl.overheid.aerius.wui.main.ui.menu.MenuItemFactory;
import nl.overheid.aerius.wui.main.util.development.EmbeddedDevPanel;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger.SuperNitroTurboJuggernaut;
import nl.overheid.aerius.wui.main.widget.NotificationButton;
import nl.overheid.aerius.wui.main.widget.NotificationButtonCircular;
import nl.overheid.aerius.wui.scenario.ScenarioActivityMapper.ScenarioActivityFactory;
import nl.overheid.aerius.wui.scenario.base.ScenarioBaseClientModule;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.geo.CalculationResultLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.geo.CalculatorLegendTitleFactory;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.geo.WmsCalculationResultLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;
import nl.overheid.aerius.wui.scenario.base.place.StartUpPlace;
import nl.overheid.aerius.wui.scenario.base.processor.ScenarioBaseCalculationProcessor;
import nl.overheid.aerius.wui.scenario.base.ui.CalculationController;
import nl.overheid.aerius.wui.scenario.base.ui.CalculationControllerImpl;
import nl.overheid.aerius.wui.scenario.base.ui.CalculatorButtonGroup;
import nl.overheid.aerius.wui.scenario.base.ui.EmissionSourcesView;
import nl.overheid.aerius.wui.scenario.base.ui.EmissionSourcesViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.NetworkDetailView;
import nl.overheid.aerius.wui.scenario.base.ui.NetworkDetailViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ResultFilterView;
import nl.overheid.aerius.wui.scenario.base.ui.ResultFilterViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ResultGraphicsView;
import nl.overheid.aerius.wui.scenario.base.ui.ResultGraphicsViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ResultTableView;
import nl.overheid.aerius.wui.scenario.base.ui.ResultTableViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ResultView;
import nl.overheid.aerius.wui.scenario.base.ui.ResultViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseTaskBar;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseView;
import nl.overheid.aerius.wui.scenario.base.ui.SituationView;
import nl.overheid.aerius.wui.scenario.base.ui.SituationViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.overview.SourcesPanel;
import nl.overheid.aerius.wui.scenario.base.ui.overview.ViewableSourcesPanel;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.geo.ScenarioMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.importer.ScenarioImportController;
import nl.overheid.aerius.wui.scenario.menu.ScenarioMenuItemFactory;
import nl.overheid.aerius.wui.scenario.place.ScenarioPlaceHistoryMapper;
import nl.overheid.aerius.wui.scenario.popups.MetaDataPanelView;
import nl.overheid.aerius.wui.scenario.popups.MetaDataPanelViewImpl;
import nl.overheid.aerius.wui.scenario.processor.ScenarioResultProcessor;
import nl.overheid.aerius.wui.scenario.search.ScenarioMapSearchAction;
import nl.overheid.aerius.wui.scenario.service.APIServiceAsync;
import nl.overheid.aerius.wui.scenario.service.APIServiceAsyncImpl;
import nl.overheid.aerius.wui.scenario.ui.ConnectConfigureView;
import nl.overheid.aerius.wui.scenario.ui.ConnectConfigureViewImpl;
import nl.overheid.aerius.wui.scenario.ui.ConnectLoginView;
import nl.overheid.aerius.wui.scenario.ui.ConnectLoginViewImpl;
import nl.overheid.aerius.wui.scenario.ui.ConnectOverviewView;
import nl.overheid.aerius.wui.scenario.ui.ConnectOverviewViewImpl;
import nl.overheid.aerius.wui.scenario.ui.ConnectUtilsView;
import nl.overheid.aerius.wui.scenario.ui.ConnectUtilsViewImpl;
import nl.overheid.aerius.wui.scenario.ui.IdleCalculatorButtonGroup;
import nl.overheid.aerius.wui.scenario.ui.ScenarioTaskBarImpl;
import nl.overheid.aerius.wui.scenario.ui.ScenarioViewImpl;
import nl.overheid.aerius.wui.scenario.ui.StartUpView;
import nl.overheid.aerius.wui.scenario.ui.StartUpViewImpl;
import nl.overheid.aerius.wui.scenario.util.development.ScenarioDevPanel;
import nl.overheid.aerius.wui.scenario.util.development.SuperNitroTurboScenarioLogger;
import nl.overheid.aerius.wui.scenario.util.development.SuperNitroTurboScenarioLogger.SuperNitroTurboScenarioJuggernaut;

/**
 * Gin bindings for the Calculator application.
 */
class ScenarioClientModule extends ScenarioBaseClientModule {
  @Override
  protected void configure() {
    // Essentials
    bind(PlaceController.class).to(AeriusPlaceController.class).in(Singleton.class);
    bind(ActivityMapper.class).to(ScenarioActivityMapper.class).in(Singleton.class);
    bind(PlaceHistoryMapper.class).to(ScenarioPlaceHistoryMapper.class).in(Singleton.class);
    bind(new TypeLiteral<AeriusContextProvider<?, ?>>() {}).to(ScenarioContextProvider.class);
    bind(new TypeLiteral<ScenarioBaseAppContext<?, ?>>() {}).to(ScenarioAppContext.class);
    bind(new TypeLiteral<AppContext<?, ?>>() {}).to(ScenarioAppContext.class);
    bind(UserContext.class).to(ScenarioUserContext.class);
    bind(Context.class).to(ScenarioContext.class);
    bind(ApplicationRootView.class).to(ApplicationRootMenuViewImpl.class).in(Singleton.class);
    bind(RootPanelFactory.class).to(RootLayoutPanelFactoryImpl.class);
    bind(MenuItemFactory.class).to(ScenarioMenuItemFactory.class);
    bind(LegendTitleFactory.class).to(CalculatorLegendTitleFactory.class).in(Singleton.class);
    bind(ApplicationInitializer.class).to(ScenarioApplicationInitializer.class).in(Singleton.class);
    bind(Place.class).annotatedWith(DefaultPlace.class).to(StartUpPlace.class);
    bind(ScenarioBaseImportController.class).to(ScenarioImportController.class);
    bind(CalculationController.class).to(CalculationControllerImpl.class);
    bind(CalculatorButtonGroup.class).to(IdleCalculatorButtonGroup.class);
    bind(ScenarioBaseCalculationProcessor.class).to(ScenarioResultProcessor.class);
    bind(CalculationResultLayerWrapper.class).to(WmsCalculationResultLayerWrapper.class);
    bind(NetworkDetailView.class).to(NetworkDetailViewImpl.class);

    // Application specific
    bind(NotificationButton.class).to(NotificationButtonCircular.class);
    bind(MapSearchAction.class).to(ScenarioMapSearchAction.class);
    bind(SourcesPanel.class).to(ViewableSourcesPanel.class);

    // Views
    bind(ScenarioBaseView.class).to(ScenarioViewImpl.class);
    bind(ApplicationDisplay.class).to(ScenarioViewImpl.class);
    bind(ScenarioBaseTaskBar.class).to(ScenarioTaskBarImpl.class);
    bind(MapLayoutPanel.class).to(ScenarioMapLayoutPanel.class);
    bind(ScenarioBaseMapLayoutPanel.class).to(ScenarioMapLayoutPanel.class);
    bind(StartUpView.class).to(StartUpViewImpl.class);
    bind(SituationView.class).to(SituationViewImpl.class);
    bind(ResultView.class).to(ResultViewImpl.class);
    bind(ResultGraphicsView.class).to(ResultGraphicsViewImpl.class);
    bind(ResultTableView.class).to(ResultTableViewImpl.class);
    bind(ResultFilterView.class).to(ResultFilterViewImpl.class);
    bind(MetaDataPanelView.class).to(MetaDataPanelViewImpl.class);
    bind(EmissionSourcesView.class).to(EmissionSourcesViewImpl.class);

    // Connect views
    bind(ConnectLoginView.class).to(ConnectLoginViewImpl.class);
    bind(ConnectOverviewView.class).to(ConnectOverviewViewImpl.class);
    bind(ConnectConfigureView.class).to(ConnectConfigureViewImpl.class);
    bind(ConnectUtilsView.class).to(ConnectUtilsViewImpl.class);

    // Connect api service
    bind(APIServiceAsync.class).to(APIServiceAsyncImpl.class);

    // Development / etc.
    bind(SuperNitroTurboLogger.class).to(SuperNitroTurboScenarioLogger.class);
    bind(SuperNitroTurboJuggernaut.class).to(SuperNitroTurboScenarioJuggernaut.class);
    bind(EmbeddedDevPanel.class).to(ScenarioDevPanel.class);

    // Place to Activity assisted injection
    install(new GinFactoryModuleBuilder().build(ScenarioActivityFactory.class));
  }

  @Provides
  public ScenarioContext getScenarioContext(final ScenarioContextProvider provider) {
    return provider.getContext();
  }

  @Provides
  public ScenarioUserContext getScenarioUserContext(final ScenarioContextProvider provider) {
    return provider.getUserContext();
  }
}
