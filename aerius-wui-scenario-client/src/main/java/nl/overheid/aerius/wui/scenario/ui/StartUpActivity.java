/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;
import nl.overheid.aerius.wui.scenario.importer.ScenarioImportController;

public class StartUpActivity extends AbstractActivity implements StartUpView.Presenter {
  private final StartUpView view;
  private final ScenarioBaseImportController importController;

  @Inject
  public StartUpActivity(final StartUpView view, final ScenarioImportController importController) {
    this.view = view;
    this.importController = importController;
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    view.setPresenter(this);

    panel.setWidget(view);
  }

  @Override
  public void showImportDialog() {
    importController.showImportDialog();
  }
}
