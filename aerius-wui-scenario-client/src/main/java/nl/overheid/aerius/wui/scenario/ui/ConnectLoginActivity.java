/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.ScenarioUserContext;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.place.ConnectOverviewPlace;
import nl.overheid.aerius.wui.scenario.service.APIServiceAsync;
import nl.overheid.aerius.wui.scenario.ui.ConnectLoginViewImpl.CreateEmailDriver;
import nl.overheid.aerius.wui.scenario.ui.ConnectLoginViewImpl.LoginAPIDriver;

public class ConnectLoginActivity extends AbstractActivity implements ConnectLoginView.Presenter {
  private final LoginAPIDriver loginAPIKeyDriver = GWT.create(LoginAPIDriver.class);

  private final CreateEmailDriver createKeyDriver = GWT.create(CreateEmailDriver.class);

  private final PlaceController placeController;
  private final APIServiceAsync service;
  private final ConnectLoginView view;

  private EventBus eventBus;

  private final ScenarioUserContext userContext;

  @Inject
  public ConnectLoginActivity(final ConnectLoginView view, final PlaceController placeController, final APIServiceAsync service,
      final ScenarioAppContext appContext) {
    this.view = view;
    this.placeController = placeController;
    this.service = service;
    this.userContext = appContext.getUserContext();
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.eventBus = eventBus;

    view.setPresenter(this);

    panel.setWidget(view);

    loginAPIKeyDriver.initialize(view.getLoginEditor());
    loginAPIKeyDriver.edit("");

    createKeyDriver.initialize(view.getCreationEditor());
    createKeyDriver.edit("");
  }

  @Override
  public void login(final String apiKey) {
    loginAPIKeyDriver.flush();

    if (loginAPIKeyDriver.hasErrors()) {
      ErrorPopupController.addErrors(loginAPIKeyDriver.getErrors());
    } else {
      loginApiKey(apiKey);
    }
  }

  @Override
  public void create(final String email) {
    createKeyDriver.flush();

    if (createKeyDriver.hasErrors()) {
      ErrorPopupController.addErrors(createKeyDriver.getErrors());
    } else {
      createApiKey(email);
    }
  }

  private void loginApiKey(final String apiKey) {
    service.requestStatus(apiKey, new AppAsyncCallback<JSONValue>() {
      @Override
      public void onSuccess(final JSONValue result) {
        userContext.setAPIKey(apiKey);
        placeController.goTo(new ConnectOverviewPlace(placeController.getWhere()));
      }
    });
  }

  private void createApiKey(final String email) {
    service.requestAPIKey(email, new AppAsyncCallback<JSONValue>() {
      @Override
      public void onSuccess(final JSONValue result) {
        NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioConnectGeneratingKey(email));
      }
    });
  }
}
