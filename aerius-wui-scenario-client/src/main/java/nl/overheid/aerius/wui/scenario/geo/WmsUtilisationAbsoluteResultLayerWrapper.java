/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.geo;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.LabeledMultiMapLayer;
import nl.overheid.aerius.geo.LayerPreparationUtil;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.shared.LabeledMultiLayerProps;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.shared.domain.WMSLayerType;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationResultsRetrievedEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

@Singleton
public class WmsUtilisationAbsoluteResultLayerWrapper {

  private static final String COMPARISON_SITUATION_1_PARAM = "calculation_a_id";
  private static final String COMPARISON_SITUATION_2_PARAM = "calculation_b_id";
  private static final String CALCULATION_SUBSTANCE_PARAM = "calculation_substance";

  interface WmsCalculationAbsoluteResultsLayerWrapperEventBinder extends EventBinder<WmsUtilisationAbsoluteResultLayerWrapper> {}

  private final WmsCalculationAbsoluteResultsLayerWrapperEventBinder eventBinder = 
      GWT.create(WmsCalculationAbsoluteResultsLayerWrapperEventBinder.class);

  private final FetchGeometryServiceAsync service;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private final LabeledMultiLayerProps utilisationProps;

  private MapLayoutPanel map;
  private LabeledMultiLayerProps selectedLayerProps;
  private LabeledMultiMapLayer layer;

  @Inject
  public WmsUtilisationAbsoluteResultLayerWrapper(final ScenarioBaseAppContext<?, ?> appContext, final FetchGeometryServiceAsync service) {
    this.service = service;
    this.appContext = appContext;
    this.utilisationProps = new LabeledMultiLayerProps();
    final ArrayList<LayerProps> layers = new ArrayList<LayerProps>();
    layers.add(appContext.getContext().getLayer(WMSLayerType.WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION));
    layers.add(appContext.getContext().getLayer(WMSLayerType.WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION_LABEL));
    utilisationProps.setLayers(layers);
    LayerPreparationUtil.prepareLayer(service, utilisationProps);
    eventBinder.bindEventHandlers(this, appContext.getEventBus());
  }

  @EventHandler
  void onCalculationInit(final CalculationInitEvent event) {
    updateLayer();
  }

  @EventHandler
  void onCalculationResultsRetrieved(final CalculationResultsRetrievedEvent event) {
    updateLayer();
  }

  @EventHandler
  void onEmissionResultKeyChanged(final EmissionResultKeyChangeEvent event) {
    updateLayer();
  }

  @EventHandler
  void onPlaceChange(final PlaceChangeEvent event) {
    updateLayer();
  }

  /**
   * Updates the layer
   */
  private void updateLayer() {
    final LabeledMultiLayerProps layerProps = selectLayer();
    if (layerProps == null && layer != null) {
      detach();
    } else if (layerProps != null && layer != null && layerProps.equals(selectedLayerProps)) {
      layer.redraw();
    } else if (layerProps != null) {
      LayerPreparationUtil.prepareLayer(service, layerProps, new AppAsyncCallback<LayerProps>() {
        @Override
        public void onSuccess(final LayerProps result) {
          detach();
          layerProps.setEnabled(true);
          layer = (LabeledMultiMapLayer) map.addLayer(layerProps);
          selectedLayerProps = layerProps;
        }
      });
    }
  }

  private LabeledMultiLayerProps selectLayer() {
    final Place where = appContext.getPlaceController().getWhere();
    if (!(where instanceof ScenarioBasePlace)) {
      return null;
    }

    final ScenarioBasePlace place = (ScenarioBasePlace) where;
    LabeledMultiLayerProps selected = null;
    final boolean hasScenario = appContext.getUserContext().getCalculatedScenario() != null;
    if (hasScenario && place.getSituation() == Situation.COMPARISON && place.getJobType() == JobType.PRIORITY_PROJECT_UTILISATION) {
      updateUtilisationProps(place.getSid1(), place.getSid2());
      selected = utilisationProps;
    }
    return selected;
  }

  private void updateUtilisationProps(final int currentSituationId, final int proposedSituationId) {
    for (final LayerProps l : utilisationProps.getLayers()) {
      updateSubstanceParam((LayerWMSProps) l);
      ((LayerWMSProps) l).setParamFilter(COMPARISON_SITUATION_1_PARAM,
        appContext.getUserContext().getCalculatedScenario().getCalculationId(currentSituationId));
      ((LayerWMSProps) l).setParamFilter(COMPARISON_SITUATION_2_PARAM,
        appContext.getUserContext().getCalculatedScenario().getCalculationId(proposedSituationId));
    }
  }

  private void updateSubstanceParam(final LayerWMSProps layerProps) {
    layerProps.setParamFilter(CALCULATION_SUBSTANCE_PARAM, appContext.getUserContext().getEmissionValueKey().getSubstance().toDatabaseString());
  }

  public void detach() {
    if (layer != null) {
      map.removeLayer(layer);
      layer = null;
      selectedLayerProps = null;
    }
  }

  public void setMap(final MapLayoutPanel map) {
    this.map = map;
  }

  public void setVisible(final boolean visible) {
    if (layer == null) {
      return;
    }

    map.setVisible(layer, visible);
  }
}
