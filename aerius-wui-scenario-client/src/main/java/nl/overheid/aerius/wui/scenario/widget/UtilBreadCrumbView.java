/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor.Ignore;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.wui.scenario.ui.ConnectUtilsActivity.ViewState;

public class UtilBreadCrumbView extends Composite {
  private static final UtilBreadCrumbViewImplUiBinder UI_BINDER = GWT.create(UtilBreadCrumbViewImplUiBinder.class);

  interface UtilBreadCrumbViewImplUiBinder extends UiBinder<Widget, UtilBreadCrumbView> {
  }

  interface CustomStyle extends CssResource {
    String selected();
  }

  @UiField CustomStyle style;
  @Ignore @UiField Label utilCrumbSelectLabel;
  @Ignore @UiField Label utilCrumbUploadLabel;
  @Ignore @UiField Label utilCrumbResultLabel;

  @Inject
  public UtilBreadCrumbView() {
    initWidget(UI_BINDER.createAndBindUi(this));

  }

  public void setCrumbSelected(final ViewState state) {
    utilCrumbSelectLabel.removeStyleName(style.selected());
    utilCrumbUploadLabel.removeStyleName(style.selected());
    utilCrumbResultLabel.removeStyleName(style.selected());
    switch (state) {
    case SELECT:
      utilCrumbSelectLabel.addStyleName(style.selected());
      break;
    case UPLOAD:
      utilCrumbUploadLabel.addStyleName(style.selected());
      break;
    case RESULT:
      utilCrumbResultLabel.addStyleName(style.selected());
      break;
    default:
      break;
    }
  }
}
