/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.service;

import org.apache.http.HttpStatus;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestBuilder.Method;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.exception.APIException;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * This class implements a REST service call implementation to Connect API Service.
 */
public class APIServiceAsyncImpl implements APIServiceAsync {
  private static final class APIServiceCallback implements RequestCallback {
    private final AsyncCallback<JSONValue> callback;

    private APIServiceCallback(final AsyncCallback<JSONValue> callback) {
      this.callback = callback;
    }

    @Override
    public void onResponseReceived(final Request request, final Response response) {
      try {
        if (response.getStatusCode() == HttpStatus.SC_OK) {
          callback.onSuccess(JSONParser.parseStrict(response.getText()));
        } else if (response.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
          final JSONObject obj = JSONParser.parseStrict(response.getText()).isObject();
          throw new APIException(Reason.fromErrorCode((int) ((JSONNumber) obj.get("code")).doubleValue()),
              ((JSONString) obj.get("message")).stringValue());
        } else {
          throw new AeriusException(Reason.FAULTY_REQUEST);
        }
      } catch (final Exception e) {
        callback.onFailure(e);
      }
    }

    @Override
    public void onError(final Request request, final Throwable exception) {
      callback.onFailure(exception);
    }
  }

  private static final String GENERATE_API_KEY = "generateAPIKey";
  private static final String STATUS_JOBS = "status/jobs";
  private static final String EXPORT_JOB = "export/priorityProjectUtilisation";
  private static final String CANCELJOB = "cancelJob";

  private static final int CLIENT_TIMEOUT = 3000;

  private final String endPoint;

  @Inject
  public APIServiceAsyncImpl(final ScenarioContext context) {
    this.endPoint = context.getSetting(SharedConstantsEnum.CONNECT_API_URL).toString();
  }

  /**
   *
   * @param email
   *          parameter email.
   * @param callback
   *          AsyncCallback.
   * @throws AeriusException
   */
  @Override
  public void requestAPIKey(final String email, final AsyncCallback<JSONValue> callback) {
    final String url = endPoint + GENERATE_API_KEY;
    final String requestData = "{\"email\":\"" + email + "\"}";
    requestAPI(url, RequestBuilder.POST, requestData, callback);
  }

  /**
   *
   * @param apiKey
   *          String apikey.
   * @param callback
   *          AsyncCallback.
   * @throws AeriusException
   */
  @Override
  public void requestStatus(final String apiKey, final AsyncCallback<JSONValue> callback) {
    final String url = endPoint + STATUS_JOBS + "?apiKey=" + apiKey;
    final String requestData = null;
    requestAPI(url, RequestBuilder.GET, requestData, callback);
  }

  /**
   *
   * @param url
   *          String endpoint API Url.
   * @param method
   *          RequestBuilder.mehtod.
   * @param requestData
   *          Request dataobject.
   * @param callback
   * @throws AeriusException
   */
  private static void requestAPI(final String url, final Method method, final String requestData, final AsyncCallback<JSONValue> callback) {
    final RequestBuilder builder = new RequestBuilder(method, URL.encode(url));
    builder.setHeader("Content-Type", "application/json");
    builder.setHeader("Accept", "application/json");

    builder.setTimeoutMillis(CLIENT_TIMEOUT);
    try {
      builder.sendRequest(requestData, new APIServiceCallback(callback));
    } catch (final RequestException requestException) {
      callback.onFailure(requestException);
    }
  }

  @Override
  public void requestExport(final String apiKey, final String projectKey, final AsyncCallback<JSONValue> callback) {
    final String url = endPoint + EXPORT_JOB;
    final String requestData = "{\"apiKey\":\"" + apiKey + "\", \"projectKey\":\"" + projectKey + "\"}";
    requestAPI(url, RequestBuilder.POST, requestData, callback);
  }

  @Override
  public void cancelJob(final String apiKey, final String jobKey, final AsyncCallback<JSONValue> callback) {
    final String url = endPoint + CANCELJOB;
    final String requestData = "{\"apiKey\":\"" + apiKey + "\", \"jobKey\":\"" + jobKey + "\"}}";
    requestAPI(url, RequestBuilder.POST, requestData, callback);
  }
}
