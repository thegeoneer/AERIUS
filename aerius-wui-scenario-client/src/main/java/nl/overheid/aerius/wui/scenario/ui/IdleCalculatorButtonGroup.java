/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import com.google.gwt.user.client.ui.SimplePanel;

import nl.overheid.aerius.wui.scenario.base.ui.CalculationController;
import nl.overheid.aerius.wui.scenario.base.ui.CalculatorButtonGroup;

public class IdleCalculatorButtonGroup extends SimplePanel implements CalculatorButtonGroup {
  @Override
  public void setPresenter(final CalculationController presenter) {}

  @Override
  public void setButtonState(final boolean calculationRunning, final boolean visibleCalculate) {}

}
