/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.CalculationType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.ExportType;
import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.SelectListBox;
import nl.overheid.aerius.wui.main.widget.SelectOption;
import nl.overheid.aerius.wui.main.widget.SelectTextOptionFactory;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public class CalculationConfigurationView extends Composite implements Editor<ConnectCalculationInformation> {
  private static final ProjectInformationViewImplUiBinder UI_BINDER = GWT.create(ProjectInformationViewImplUiBinder.class);

  interface ProjectInformationViewImplUiBinder extends UiBinder<Widget, CalculationConfigurationView> {}

  public interface ConfigurationContentHandler {
    void updateExportType(ExportType exportType);
  }

  @Path("exportType") @UiField SelectListBox<ExportType> calculationFormListBox;
  @UiField(provided = true) WidgetFactory<ExportType, SelectOption<ExportType>> calculationFormTemplate =
      new SelectTextOptionFactory<ExportType>() {
    @Override
    public String getItemText(final ExportType value) {
      return M.messages().scenarioConnectExportType(value);
    }
  };
  @Path("calculationType") @UiField SelectListBox<CalculationType> calculationTypeListBox;
  @UiField(provided = true) WidgetFactory<CalculationType, SelectOption<CalculationType>> calculationTypeTemplate =
      new SelectTextOptionFactory<CalculationType>() {
    @Override
    public String getItemText(final CalculationType value) {
      return M.messages().scenarioConnectCalculationType(value);
    }
  };
  @Path("year") @UiField SelectListBox<Integer> calculationYearListBox;
  @UiField(provided = true) WidgetFactory<Integer, SelectOption<Integer>> calculationYearTemplate =
      new SelectTextOptionFactory<Integer>() {
    @Override
    public String getItemText(final Integer value) {
      return String.valueOf(value);
    }
  };

  @Path("name") @UiField TextValueBox nameValueBox;

  private ConfigurationContentHandler handler;

  @Inject
  public CalculationConfigurationView(final ScenarioContext context) {
    initWidget(UI_BINDER.createAndBindUi(this));
    final int currentYear = SharedConstants.getCurrentYear();

    calculationFormListBox.setValues(ExportType.values());
    calculationFormListBox.addValueChangeHandler(new ValueChangeHandler<ConnectCalculationInformation.ExportType>() {
      @Override
      public void onValueChange(final ValueChangeEvent<ExportType> event) {
        handler.updateExportType(event.getValue());
      }
    });
    calculationTypeListBox.setValues(CalculationType.values());

    final int firstYear = context.getSetting(SharedConstantsEnum.MIN_YEAR);
    final int lastYear = context.getSetting(SharedConstantsEnum.MAX_YEAR);
    for (int i = firstYear; i <= lastYear; i++) {
      calculationYearListBox.addValue(i);
    }
    calculationYearListBox.addValueChangeHandler(new ValueChangeHandler<Integer>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Integer> event) {
        final Integer selectedValue = calculationYearListBox.getValue();
        if (selectedValue == null) {
          //reset to default year
          calculationYearListBox.setSelectedValue(currentYear, true);
        }
      }
    });

    addPlaceholders();
    ensureDebugIds();
  }

  private void ensureDebugIds() {
    nameValueBox.ensureDebugId(TestIDScenario.CALCULATION_CONFIGURATION_NAME);
    calculationFormListBox.ensureDebugId(TestIDScenario.CALCULATION_CONFIGURATION_FORM);
    calculationTypeListBox.ensureDebugId(TestIDScenario.CALCULATION_CONFIGURATION_TYPE);
    calculationYearListBox.ensureDebugId(TestIDScenario.CALCULATION_CONFIGURATION_YEAR);
  }

  private void addPlaceholders() {
    StyleUtil.I.setPlaceHolder(nameValueBox, M.messages().scenarioConnectCalculationCalculationOptionsNameLabel());
  }

  public void setContentHandler(final ConfigurationContentHandler handler) {
    this.handler = handler;
  }
}
