/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.widget;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.dom.client.DragEnterHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation.UtilType;
import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.scenario.importer.ConnectImportController;
import nl.overheid.aerius.wui.scenario.ui.ConnectUtilsActivity.ViewState;
import nl.overheid.aerius.wui.scenario.widget.CalculationDetailTable.FileHandler;

public class UtilUploadView extends Composite implements Editor<ConnectUtilInformation>, DragEnterHandler, FileHandler {

  private static final UtilUploadViewImplUiBinder UI_BINDER = GWT.create(UtilUploadViewImplUiBinder.class);

  interface UtilUploadViewImplUiBinder extends UiBinder<Widget, UtilUploadView> {
  }

  @Ignore @UiField Label selectedDescriptionLabel;
  @UiField Button importButton;
  @Path("files") @UiField(provided = true) CalculationDetailTable calculationDetailTable;
  @Ignore @UiField FlowPanel scenarioConnectValidateOptionalPanel;
  @UiField @Ignore Label scenarioConnectValidateOptionsTitleLabel;
  @Ignore @UiField FlowPanel scenarioConnectValidateAsPPPanel;
  @Path("validateAsPriorityProject") @UiField CheckBox validateAsPPCheckBox;
  @UiField @Ignore Label scenarioConnectValidateAsPPTitleLabel;
  @Ignore @UiField FlowPanel scenarioConnectOnlyIncreasePanel;
  @Path("onlyIncreasement") @UiField CheckBox onlyIncreaseCheckBox;
  @UiField @Ignore Label scenarioConnectOnlyIncreaseTitleLabel;
  @Ignore @UiField Label validateLabel;

  private final ConnectImportController importController;
  private ViewState currentViewState;
  private UtilUploadHandler handler;

  @Inject
  public UtilUploadView(final CalculationDetailTable detailTable, final ConnectImportController importController) {
    this.calculationDetailTable = detailTable;
    this.importController = importController;
    initWidget(UI_BINDER.createAndBindUi(this));

    addDomHandler(this, DragEnterEvent.getType());
    calculationDetailTable.setFileHandler(this);
    validateLabel.ensureDebugId(TestIDScenario.CONNECT_UTIL_VALIDATE_LABEL);
    importButton.ensureDebugId(TestIDScenario.CONNECT_UTIL_IMPORT);
    scenarioConnectValidateOptionalPanel.ensureDebugId(TestIDScenario.CONNECT_UTIL_OPTION_PANEL);
    scenarioConnectValidateOptionsTitleLabel.ensureDebugId(TestIDScenario.CONNECT_UTIL_OPTION_PANEL_LABEL);
    validateAsPPCheckBox.ensureDebugId(TestIDScenario.CONNECT_UTIL_VALIDATE_AS_PP_CHECKBOX);
    onlyIncreaseCheckBox.ensureDebugId(TestIDScenario.CONNECT_UTIL_ONLYINCREASE_CHECKBOX);
  }

  @Override
  public void onDragEnter(final DragEnterEvent event) {
    event.preventDefault();
    event.stopPropagation();
    if (currentViewState == ViewState.UPLOAD) {
      importController.showImportDialog(true);
    }
  }

  @Override
  public void removeFile(final String uid) {
    handler.removeFile(uid);
  }

  @UiHandler("scenarioConnectValidateAsPPTitleLabel")
  public void onScenarioConnectValidatePPTitleLabelClick(final ClickEvent e) {
    validateAsPPCheckBox.setValue(!validateAsPPCheckBox.getValue(), true);
  }

  @UiHandler("scenarioConnectOnlyIncreaseTitleLabel")
  public void onscenarioConnectOnlyIncreaseTitleLabelClick(final ClickEvent e) {
    onlyIncreaseCheckBox.setValue(!onlyIncreaseCheckBox.getValue(), true);
  }

  @UiHandler("importButton")
  public void onImportClick(final ClickEvent e) {
    importController.showImportDialog();
  }

  public void clearLabel() {
    validateLabel.setText("");
  }

  public void setContentHandler(final UtilUploadHandler handler) {
    this.handler = handler;
    importController.setContentController(handler);
  }

  public void setFilesWarningText(final String filesWarningText) {
    validateLabel.setText(filesWarningText);
  }

  public void setData(final ArrayList<ConnectCalculationFile> files, final UtilType utilType) {
    calculationDetailTable.determineColumnVisibility(null, files.size(), utilType == UtilType.DELTA_VALUE);
    calculationDetailTable.asDataTable().setRowData(files);
  }

  public void updateSelectedUtilState(final UtilType ut) {
    scenarioConnectValidateOptionalPanel.setVisible(ut == UtilType.VALIDATE || ut == UtilType.DELTA_VALUE);
    scenarioConnectValidateAsPPPanel.setVisible(ut == UtilType.VALIDATE);
    scenarioConnectOnlyIncreasePanel.setVisible(ut == UtilType.DELTA_VALUE);
    selectedDescriptionLabel.setText(M.messages().scenarioConnectSelectedUtilDescription(ut));
  }

  public void updateViewState(final ViewState newState) {
    currentViewState = newState;
  }

}
