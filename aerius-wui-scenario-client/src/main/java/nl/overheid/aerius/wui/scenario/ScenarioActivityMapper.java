/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario;

import java.util.ArrayList;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.domain.context.ScenarioUserContext;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.wui.scenario.ScenarioActivityMapper.ScenarioActivityFactory;
import nl.overheid.aerius.wui.scenario.base.ScenarioBaseActivityFactory;
import nl.overheid.aerius.wui.scenario.base.ScenarioBaseActivityMapper;
import nl.overheid.aerius.wui.scenario.base.place.NetworkDetailPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultFilterPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultGraphicsPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultTablePlace;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;
import nl.overheid.aerius.wui.scenario.base.place.StartUpPlace;
import nl.overheid.aerius.wui.scenario.base.ui.NetworkDetailPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.ResultFilterPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.ResultGraphicsPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.ResultTablePresenter;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.place.ConnectConfigurePlace;
import nl.overheid.aerius.wui.scenario.place.ConnectLoginPlace;
import nl.overheid.aerius.wui.scenario.place.ConnectOverviewPlace;
import nl.overheid.aerius.wui.scenario.place.ConnectPlace;
import nl.overheid.aerius.wui.scenario.place.ConnectUtilsPlace;
import nl.overheid.aerius.wui.scenario.ui.ConnectConfigureActivity;
import nl.overheid.aerius.wui.scenario.ui.ConnectLoginActivity;
import nl.overheid.aerius.wui.scenario.ui.ConnectOverviewActivity;
import nl.overheid.aerius.wui.scenario.ui.ConnectUtilsActivity;
import nl.overheid.aerius.wui.scenario.ui.ScenarioEmissionSourcesPresenter;
import nl.overheid.aerius.wui.scenario.ui.StartUpActivity;

/**
 * Activity Mapper for the Scenario.
 */
class ScenarioActivityMapper extends ScenarioBaseActivityMapper<ScenarioActivityFactory, ScenarioAppContext, ScenarioContext, ScenarioUserContext> {
  private final ConnectJobTracker tracker;

  /**
   * CaclulatorActivityMapper associates each Place with its corresponding {@link Activity}.
   *
   * @param factory Generated factory to add params to constructors
   * @param contextProvider user context object
   */
  @Inject
  public ScenarioActivityMapper(final ScenarioActivityFactory factory, final PlaceController placeController, final ScenarioAppContext appContext,
      final ConnectJobTracker tracker) {
    super(factory, appContext, placeController);
    this.tracker = tracker;
  }

  @Override
  public Activity getActivity(final Place place) {
    initSingleton();

    Activity presenter = null;

    if (place instanceof ConnectPlace) {
      if (place instanceof ConnectUtilsPlace) {
        presenter = factory.createConnectUtilPresenter((ConnectUtilsPlace) place);
      } else if (place instanceof ConnectLoginPlace) {
        presenter = factory.createConnectLoginPresenter((ConnectLoginPlace) place);
      } else
      // If there is no API key, redirect to the login place immediately
      if (!context.getUserContext().hasAPIKey()) {
        redirectPlace(new ConnectLoginPlace(place));
        return null;
      } else if (place instanceof ConnectOverviewPlace) {
        presenter = factory.createConnectOverviewPresenter((ConnectOverviewPlace) place);
      } else if (place instanceof ConnectConfigurePlace) {
        presenter = factory.createConnectConfigurePresenter((ConnectConfigurePlace) place);
      }
    } else if (place instanceof ScenarioBasePlace && ((ScenarioBasePlace) place).getJobKey() != null && !context.getUserContext().isFollowingJob()) {
      // Fetch job information, but only go there when it is done fetching (this code is here so that history tokens may start off this behavior)
      fetchJobCalculations((ScenarioBasePlace) place);
    } else if (context.getUserContext().getScenario().getSourceLists().isEmpty()) {
      // no source list means no import done and we're at startup. Even a GML without sources will cause a entry in the sourcelists.
      // let it redirect to the startup (unless that's where we are).
      if (place instanceof StartUpPlace) {
        presenter = factory.createStartUpPresenter((StartUpPlace) place);
      }
    } else if (place instanceof StartUpPlace) {
      presenter = factory.createStartUpPresenter((StartUpPlace) place);
    } else if (place instanceof ResultGraphicsPlace) {
      presenter = factory.createResultGraphicsPresenter((ResultGraphicsPlace) place);
    } else if (place instanceof ResultTablePlace) {
      presenter = factory.createResultTablePresenter((ResultTablePlace) place);
    } else if (place instanceof ResultFilterPlace) {
      presenter = factory.createResultFilterPresenter((ResultFilterPlace) place);
    } else if (place instanceof SourcesPlace) {
      presenter = factory.createSourcesOverviewPresenter((SourcesPlace) place);
    } else if (place instanceof NetworkDetailPlace) {
      presenter = factory.createNetworkDetailPresenter((NetworkDetailPlace) place, ((NetworkDetailPlace) place).getEmissionSource());
    }

    if (presenter == null) {
      // No presenter could be generated out of the given place for some reason.
      // Defer a goto to the default place (StartupPlace).
      redirectPlace(new StartUpPlace());
      return null;
    } else {
      if (place instanceof ScenarioBasePlace) {
        factory.createPreActivity((ScenarioBasePlace) place);
      }

      // normal place just go to this place
      return presenter;
    }
  }

  private void fetchJobCalculations(final ScenarioBasePlace place) {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        tracker.trackJobProgress(place);
      }
    });
  }

  /**
   * Methods capable of creating presenters given the place that is passed in.
   */
  public interface ScenarioActivityFactory extends ScenarioBaseActivityFactory {

    StartUpActivity createStartUpPresenter(StartUpPlace place);

    ConnectLoginActivity createConnectLoginPresenter(ConnectLoginPlace place);

    ConnectOverviewActivity createConnectOverviewPresenter(ConnectOverviewPlace place);

    ConnectUtilsActivity createConnectUtilPresenter(ConnectUtilsPlace place);

    ConnectConfigureActivity createConnectConfigurePresenter(ConnectConfigurePlace place);

    ScenarioEmissionSourcesPresenter createSourcesOverviewPresenter(SourcesPlace place);

    ResultGraphicsPresenter createResultGraphicsPresenter(ResultGraphicsPlace place);

    ResultFilterPresenter createResultFilterPresenter(ResultFilterPlace place);

    ResultTablePresenter createResultTablePresenter(ResultTablePlace place);

    NetworkDetailPresenter createNetworkDetailPresenter(NetworkDetailPlace place, ArrayList<EmissionSource> emissionSource);

  }
}
