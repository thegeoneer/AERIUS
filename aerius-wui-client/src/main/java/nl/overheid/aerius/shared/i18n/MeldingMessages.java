/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.melding.NoticeStep;

public interface MeldingMessages {
  String meldingImportantMessage();
  String meldingProgressStep(@Select NoticeStep step);
  String meldingLeaveUnfinished();
  String meldingConfirmIgnoreChanges();
  String meldingFileAllreadyExists();
  String meldingFileNotUploaded();

  String meldingBenefactorTitle();
  String meldingBeneficiaryTitle();
  String meldingExecutorTitle();

  String meldingBenefactorSubtitle();
  String meldingBeneficiarySubtitle();
  String meldingExecutorSubtitle();

  String meldingBeneficiaryCorrespondanceAddress();
  String meldingCorrespondenceAddressSubtitle();
  String meldingCorrespondenceAddressSame();
  String meldingCorrespondenceAddressCustom();
  String meldingInitiativeAddress();

  String meldingBeneficiaryEmailConfirmation();

  String placeHolderOrganisationName();
  String placeHolderOrganisationContactPerson();
  String placeHolderOrganisationAddress();
  String placeHolderOrganisationPostcode();
  String placeHolderOrganisationCity();
  String placeHolderOrganisationEmail();

  String meldingOrganisationSame();
  String meldingOrganisationSameAsPrevious();
  String meldingOrganisationCustom();

  String meldingEmailAlsoConfirmation();
  String meldingAddAuthorizationFiles();
  String meldingFilesRequired();
  String meldingSubstantiationFilesRequired();
  String meldingDeleteFile();

  String meldingCoreInformationTitle();

  String meldingCoreInformationCalculation();
  String meldingCoreInformationReference();
  String meldingCoreInformationEmissionNOX();
  String meldingCoreInformationEmissionNH3();
  String meldingCoreInformationHighestDeposition();

  String meldingPreviousNotices();

  String meldingRadioButtonNo();
  String meldingRadioButtonYes();

  String meldingExistingPermitQuestion();
  String meldingExistingPermitReference();
  String meldingPrevMeldingQuestion();
  String meldingPrevMeldingReference();
  String meldingFoundationDescription();
  String meldingInformationFilledInCorrectly();
  String meldingNonNitrogenWarning();

  String meldingExistingSourcePermitQuestion();
  String meldingSubstantiationFiles();
  String meldingSubstantiationFileUpload();

  String meldingSuccessSending();
  String meldingSuccessSendingText();
  String meldingSuccessfullySent();
  String meldingSuccessfullySentDescription();
  String meldingSuccessSendFailure();
  String meldingSuccessSentFailureText();

  String validatorOrganisationName();
  String validatorOrganisationContactPerson();
  String validatorOrganisationAddress();
  String validatorOrganisationPostcode();
  String validatorOrganisationCity();
  String validatorOrganisationEmail();
  String validatorExistingPermitReference();
  String validatorPrevMeldingReference();
  String validatorPrevMeldingReferenceIncorrect();

  String meldingPreviousStep();
  String meldingNextStep(@Select NoticeStep noticeStep);
  String meldingBusyUploading(String label);
  String meldingBusyUploadingConfirmIgnore(String label);
}
