/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import com.google.gwt.i18n.client.Messages.Select;


/**
 * Text for Register application.
 */
public interface AdminMessages {
  String adminMenuUserManagement();

  String adminMenuUserManagementTitle();

  String adminUsersUserEditorTitle();

  String adminUsersUserControllerTitle();

  String adminUsersNewUser();

  String adminUsersRemoveConfirm();

  String adminUsersNewUserTitle();

  String adminUsersSearchUser();

  String adminUsersOrganisations();

  String adminUsersRoles();

  String adminUsersAllOrganisations();

  String adminUsersAllRoles();

  String adminUsersNoRoles();

  String adminUsersUsername();

  String adminUsersInitials();

  String adminUsersName();

  String adminUsersFilterText(String enabled, String username, String organisations, String roles);

  String adminUsersFilterTextNoName(String enabled, String organisations, String roles);

  String adminUsersFirstName();

  String adminUsersLastName();

  String adminUsersAuthority();

  String adminUsersEmailAddress();

  String adminUsersEnabled();

  String adminUsersEnabledText(@Select boolean bool);

  String adminUsersPassword();

  String adminUsersResetPassword();

  String adminUsersResetPasswordConfirmation();

  String adminUsersRole();

  String adminUsersUsernameValidator();

  String adminUsersInitialsValidator();

  String adminUsersFirstNameValidator();

  String adminUsersLastNameValidator();

  String adminUsersEmailAddressValidator();

  String adminNotificationUserRemovalComplete();

  String adminNotificationUserUpdateComplete();

  String adminNotificationUserCreationComplete(String name);

  String adminMenuRoleManagement();

  String adminMenuRoleManagementTitle();

  String adminPermission(@Select String permission);

  String adminRoleName();

  String adminRoleColor();

  String adminRolesNew();
}
