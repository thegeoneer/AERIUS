/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

/**
 * Tool tip data class for message and id to manual.
 */
public class HelpInfo {
  private final String message;
  private final int key;

  /**
   * Creates a help tool tip info when the given message.
   *
   * <b>You should not use this constructor directly but always use the {@link nl.overheid.aerius.wui.main.widget.HelpPopupController}</b>
   *
   * @param message message of help tool tip
   */
  public HelpInfo(final String message) {
    this(message, HelpToolTipInfo.NO_LINK);
  }

  /**
   * Creates a help tool tip info when the given message and id to the user manual.
   *
   * <b>You should not use this constructor directly but always use the {@link nl.overheid.aerius.wui.main.widget.HelpPopupController}</b>
   *
   * @param message message of help tool tip
   * @param key key referring to the id of a manual page
   */
  public HelpInfo(final String message, final int key) {
    this.message = message;
    this.key = key;
  }

  /**
   * Creates a help tool tip info when the given id to the user manual.
   *
   * <b>You should not use this constructor directly but always use the {@link nl.overheid.aerius.wui.main.widget.HelpPopupController}</b>
   *
   * @param key key referring to the id of a manual page
   */
  public HelpInfo(final int key) {
    this(HelpToolTipInfo.NO_TEXT, key);
  }

  public int getKey() {
    return key;
  }

  public String getMessage() {
    return message;
  }
}
