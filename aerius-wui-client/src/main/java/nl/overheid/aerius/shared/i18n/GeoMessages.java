/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

/**
 * Interface for Messages in the geo package.
 */
public interface GeoMessages {

  /**
   * Displays message if the current zoom level exceeds the level at which data
   * is available for the layer.
   *
   * @param name Name of the layer
   * @return message text
   */
  String layerZoomExceeded(String name);

  /**
   * Displays message if the current zoom level inceeds the level at which data
   * is available for the layer.
   *
   * @param name Name of the layer
   * @return message text
   */
  String layerZoomInceeded(String name);

  /**
   * Displays message if no data for the layer is available for the year to be shown.
   *
   * @param name Name of the layer
   * @param year Year requested
   * @return message text
   */
  String layerNoDataForYear(String name, String year);
}
