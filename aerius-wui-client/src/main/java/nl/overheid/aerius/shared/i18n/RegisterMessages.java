/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import java.util.List;

import com.google.gwt.i18n.client.Messages.Optional;
import com.google.gwt.i18n.client.Messages.PluralCount;
import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData.PriorityProjectExportType;
import nl.overheid.aerius.shared.domain.register.AuditTrailRequestState;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.PriorityProjectProgress;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;

/**
 * Text for Register application.
 */
public interface RegisterMessages {

  String registerTitle();

  String registerMenuDashboard();

  String registerMenuApplications();

  String registerMenuMessages();

  String registerMenuPrioritaryProjects();

  String registerAddNewApplicationButton();

  String registerAddNewPriorityProjectButton();

  String registerAddNewActualisationButton();

  String registerDashboardTitle();

  String registerDashboardLocationFilterLabel();

  String registerDashboardPriorityProjectsTitle();

  String registerDashboardProvinces();

  String registerDashboardNoticesTitle();

  String registerDashboardMinimalDepositionBefore();

  String registerDashboardTableArea();

  String registerDashboardTableStatus();

  String registerPriorityProjectAssignComplete();

  String registerPriorityProjectAssignInProgress();

  String registerProirityProjectAssignCompleteDialogPanelTitle();

  String registerProirityProjectAssignCompleteDialogPanelText();

  String registerProirityProjectAssignInProgressDialogPanelTitle();

  String registerProirityProjectAssignInProgressDialogPanelText();

  String registerNoticeDashboardTableArea();

  String registerNoticeDashboardTableStatus();

  String registerNoticeDashboardStatusUnconfirmed();

  String registerNoticeDashboardStatusConfirmed();

  String registerNoticeDashboardStatusPending();

  String registerNoticeDashboardStatusRemaining();

  String registerDashboardPermitsTitle();

  String registerNoticeContentReceivedDate();

  String registerNoticeContentProjectName();

  String registerNoticeContentReference();

  String registerNoticeContentAuthority();

  String registerPermitDashboardStatusConfirmed();

  String registerPrioritySubProjectExportStart();

  String registerPermitSpaceShortage(double threshold, int count);

  String registerPermitSpaceNearShortage(int percentage, int count);

  String registerPermitSpaceNoShortage(int percentage);

  String registerPermitDevelopmentSpaceLegendaRemarks();

  String registerMenuApplicationsTitle();

  String registerMenuPriorityProjectsTitle();

  String registerAdaptFiltersButton();

  String registerUpdateApplicationTitle();

  String registerUpdateApplicationStateTitle();

  String registerNewApplicationTitle();

  String registerNewPriorityProjectTitle();

  String registerNewActualisationTitle();

  String registerImportSubTitle(@PluralCount List<String> supportedFileExtensions);

  String registerImportErrorOptionText();

  String registerImportOpenDuplicateButton();

  String registerImportOpenDuplicateLabelPermit(String dossierId);

  String registerImportOpenDuplicateLabelPriorityProject(String dossierId);

  String registerImportOpenDuplicateLabelSubProject(String reference);

  String registerTableSector();

  String registerTableState();

  String registerTableDate();

  String registerTableApplicant();

  String registerTableDossierId();

  String registerTableAuthority();

  String registerTableMarked();

  String registerTableProgress();

  String registerTableProjectName();

  String registerTableProjectId();

  String registerApplicationDeleteTitle();

  String registerapplicationDeletePriorityProjectTitle();

  String registerapplicationDeletePrioritySubProjectTitle();

  String registerapplicationDeleteActualisationTitle();

  String registerApplicationTabDossier();

  String registerApplicationTabApplication();

  String registerApplicationTabEconomicGrowth();

  String registerApplicationSegment(@Select SegmentType segment);

  String registerOpenInCalculator();

  String registerOpenInCalculatorPanelTitle();

  String registerOpenInCalculatorPanelText();

  String registerOpenInCalculatorForwardText();

  String registerOpenInCalculatorForwardButton();

  String registerOpenInCalculatorPrepError();

  String registerApplicationNew();

  String registerApplicationTabDossierInformationTitle();

  String registerApplicationTabDossierDecreeTitle();

  String registerApplicationTabDossierDetailDecreeTitle();

  String registerApplicationTabNoDate();

  String registerApplicationTabAppAppendixField();

  String registerApplicationTabErrorNoDossierNumber();

  String registerApplicationTabErrorNoReceivedDate();

  String registerApplicationTabErrorNoValidReceivedDate(String dateString);

  String registerApplicationTabNoRemarks();

  String registerApplicationTabApplicationInformationTitle();

  String registerApplicationTabApplicationInformationDescription();

  String registerApplicationTabAppExitWhileInEdit();

  String registerApplicationTabAppCorporation();

  String registerApplicationTabAppDate();

  String registerApplicationTabAppProject();

  String registerApplicationTabAppSituation();

  String registerApplicationTabAppSituationNew();

  String registerApplicationTabAppSituationExpansion();

  String registerApplicationTabAppTotalEmission();

  String registerApplicationTabAppCalculation();

  String registerApplicationTabAppDuration();

  String registerApplicationTabAppDurationValue(int duration);

  String registerApplicationTabAppDurationNA();

  String registerApplicationTabAppTreator();

  String registerApplicationTabAppDossierNumber();

  String registerApplicationTabAppRemarks();

  String registerApplicationTabSummaryUnavailable();

  String registerApplicationTabReview();

  String registerPermitDetailReviewViewType(@Select String name);

  String registerApplicationTabReviewSpaceExceeding(String inclusive, String inclusiveReceptors);

  String registerApplicationTabReviewSymbolShortage();

  String registerApplicationTabReviewLegendShortage();

  String registerApplicationTabEconomicGrowthRules();

  String registerApplicationTabBeingCalculated();

  String registerApplicationTabDownloadDecreeInfo();

  String registerApplicationTabDownloadDecreeInfoNotReady();

  String registerPriorityProjectProjectDossierReviewShortage(String shortage, String shortageCount);

  String registerUser(String lastName, String initials);

  String registerFullUser(String firstName, String initials, String lastName);

  String registerApplicationDelete();

  String registerApplicationEdit();

  String registerApplicationCancel();

  String registerApplicationSave();

  String registerApplicationMarkDialogPanelTitle();

  String registerApplicationMarkDialogPanelText();

  String registerApplicationUnmarkDialogPanelTitle();

  String registerApplicationUnmarkDialogPanelText();

  String registerApplicationDownloadDecree();

  String registerApplicationTabHistoryTitle();

  String registerApplicationTabHistoryChangeTitle();

  String registerApplicationTabHistoryChange(@Select AuditTrailType type, @Optional String oldValue, @Optional String newValue);

  String registerApplicationTabHistoryStateFull(String state);

  String registerApplicationTabHistoryState(@Select SegmentType segment, @Select AuditTrailRequestState state);

  String registerApplicationTabHistoryStateRemovedState();

  String registerApplicationTabHistorySave();

  String registerApplicationTabHistoryAssignCompleted();

  String registerApplicationTabHistoryAssignInProgress();

  String registerApplicationTabHistorySegment(String segmentType);

  String registerNoticesTitle();

  String registerNoticeConfirmButton();

  String registerNoticeTableSector();

  String registerNoticeTableReference();

  String registerNoticeTableDate();

  String registerNoticeTableCorporation();

  String registerNoticeTableDeleteConfirmation();

  String registerNoticeTableOpen();

  String registerNoticeConfirmPanelTitle();

  String registerNoticeConfirmPanelButton();

  String registerNoticeConfirmPanelBatchExplanation(String date);

  String registerNoticeConfirmPanelBatchAmount(int amount);

  String registerNoticeTabDossier();

  String registerNoticeTabMap();

  String registerExportConfirmPanelTitle();

  String registerExportConfirmPanelButton();

  String registerExportConfirmPanelExplanation(int amount, long size);

  String registerExportConfirmPanelExplanationNoItems();

  String registerExportConfirmPanelNoFilterExplanation(int amount, long size);

  String registerExportConfirmPanelNoFilterExplanationNoItems();

  String registerExportConfirmPanelLoading();

  String registerNotificationApplicationUpdated(String dossier);

  String registerNotificationApplicationDeleted(String dossier);

  String registerNotificationApplicationStateBeingChanged(String dossier);

  String registerNotificationApplicationStateChangeDone(String dossier);

  String registerNotificationApplicationStateChangeFailed();

  String registerNotificationApplicationCalculationStarted();

  String registerNotificationApplicationCalculationComplete();

  String registerNotificationApplicationNoDecree();

  String registerNotificationNoticeDeleted(String reference);

  String registerNotificationPriorityProjectUpdated(String dossier);

  String registeraNotificationApplicationPriorityProjectDeleted(String dossier);

  String registeraNotificationApplicationPrioritySubProjectDeleted(String dossier);

  String registerPermitFilterCollapseTitle(String status, String province, String from, String till, String authority, String sector);

  String registerPermitFilterCollapseTitleMarkedAddendum(String marked);

  String registerPermitFilterCollapseTitleNoDate(String status, String province, String authority, String sector);

  String registerNoticeFilterCollapseTitle(String province, String from, String till, String sector);

  String registerNoticeFilterCollapseTitleNoDate(String province, String sector);

  String registerPriorityProjectFilterCollapseTitle(String progress, String authority, String areas, String sector);

  String registerPriorityProjectProjectInformationTitle();

  String registerPriorityProjectProjectNoResultsSoNoEditText();

  String registerPriorityProjectProjectProjectName();

  String registerPriorityProjectProjectReservation();

  String registerPriorityProjectProjectApplicationVersion();

  String registerPriorityProjectProjectDatabaseVersion();

  String registerPriorityProjectProjectPercentageAssigned(double percentage);

  String registerPriorityProjectProjectAssigned();

  String registerPriorityProjectProjectRemaining();

  String registerPriorityProjectProjectIndicativeHexagon();

  String registerPriorityProjectProjectIndicativeHexagonEmptyValue();

  String registerPriorityProjectProjectIndicativeHexagonValue(double x, double y, float percentageIssued, String receptorId);

  String registerPriorityProjectProjectFactsheetLabel();

  String registerPriorityProjectProjectAddFactsheetButton();

  String registerPriorityProjectProjectReplaceFactsheetButton();

  String registerPriorityProjectProjectFactsheetTitle();

  String registerPriorityProjectProjectActualisationLabel();

  String registerPriorityProjectProjectAddActualisationButton();

  String registerPriorityProjectProjectReplaceActualisationButton();

  String registerPriorityProjectProjectActualiseTitle();

  String registerPriorityProjectProjectSubProjectNew();

  String registerPriorityProjectProjectSubProjectTableTitle();

  String registerPriorityProjectProjectSubProjectTableDate();

  String registerPriorityProjectProjectSubProjectTableStatus();

  String registerPriorityProjectProjectSubProjectTableName();

  String registerPriorityProjectProjectSubProjectTableDocuments();

  String registerPriorityProjectProjectSubProjectTableNoRows();

  String registerPriorityProjectActualisationInformationTitle();

  String registerPriorityProjectExportTitle();

  String registerPriorityProjectExportText();

  String registerPriorityProjectExportTypes(@Select PriorityProjectExportType type);

  String registerNoticeDeleteTitle();

  String registerNoticeDeleteText();

  String registerNoticeDossierInformationTitle();

  String registerNoticeDossierInformationInitiator();

  String registerNoticeDossierInformationAuthority();

  String registerNoticeDossierInformationLocation();

  String registerNoticeDossierInformationSourceDescription();

  String registerNoticeDossierInformationCalculation();

  String registerNoticeDossierInformationDescription();

  String registerNoticeDossierInformationDate();

  String registerNoticeDossierInformationEndDate();

  String registerNoticeDossierMapTitle(String distance);

  String registerNoticeDeleteLabelCancel();

  String registerNoticeDeleteLabelExecute();

  String registerFilterProgressLabel();

  String registerFilterStatusLabel();

  String registerFilterSectorLabel();

  String registerFilterAreaLabel();

  String registerFilterLocationLabel();

  String registerFilterAuthorityLabel();

  String registerFilterMarkedLabel();

  String registerFilterDateFromLabel();

  String registerFilterDateTillLabel();

  String registerFilterApplyButton();

  String registerFilterResetButton();

  String registerFilterNoticeDefault();

  String registerFilterSectorDefault();

  String registerFilterAreaDefault();

  String registerFilterOwnerDefault();

  String registerFilterAllDefault();

  String registerRequestState(@Select RequestState state);

  String registerRequestMarked(@Select boolean marked);

  String registerPriorityProjectProgressState(@Select PriorityProjectProgress value);

  String registerRequestPriorityState(@Select RequestState state);

  String registerReviewHeaderAssessmentArea();

  String registerReviewHeaderDepositionSpace();

  String registerReviewHeaderDepositionSpaceSufficient();

  String registerReviewHeaderAvailable();

  String registerReviewHeaderRequested();

  String registerReviewHeaderRemaining();

  String registerReviewHeaderReserved();

  String registerReviewHeaderAssigned();

  String registerReviewLegendTitle();

  String registerReviewLegendAvailable();

  String registerReviewLegendRequested();

  String registerReviewLegendReserved();

  String registerReviewLegendAssigned();

  String registerReviewLegendDevelopmentRule(@Select DevelopmentRule rule);

  String registerPermitDevelopmentSpaceTitle();

  String registerPriorityProjectHistoryPanelReference();

  String registerPriorityProjectViewType(@Select String value);

  String registerPriorityProjectDossierInformationTitle();

  String registerPriorityProjectDossierName();

  String registerPriorityProjectDossierId();

  String registerValidatorPriorityProjectDossierId();

  String registerPriorityProjectDossierAuthority();

  String registerPriorityProjectDossierRemarks();

  String registerPriorityProjectDossierDevelopmentSpace();

  String registerPriorityProjectProjectDossierInformationTabTitle();

  String registerPriorityProjectProjectDossierInformationTitle();

  String registerPriorityProjectProjectDossierInformationSubProjectLabel();

  String registerPriorityProjectProjectDossierInformationSubProjectLabelExplanation();

  String registerPriorityProjectProjectDossierInformationSubProject();

  String registerPriorityProjectProjectDossierInformationReference();

  String registerPriorityProjectProjectDossierInformationInitiator();

  String registerPriorityProjectProjectDossierInformationTemporayProject();

  String registerPriorityProjectProjectDossierInformationTemporayProjectNA();

  String registerPriorityProjectProjectDossierInformationTotalEmission();

  String registerPriorityProjectProjectDossierInformationContractor();

  String registerPriorityProjectProjectDossierInformationCalculation();

  String registerPriorityProjectProjectDossierInformationDescription();

  String registerPriorityProjectProjectDossierReviewTabTitle();

  String registerPriorityProjectProjectDossierReviewWaitingForCalculation();

  String registerPriorityProjectProjectDossierReviewUnavailable(@Select RequestState requestState);

  String searchWidgetRegisterPlaceHolder(@PluralCount List<String> permissionsToSearchFor);

  String userProfilePanelTitle();

  String registerUserProfileName();

  String registerUserProfileEmail();

  String registerUserProfileAuthority();

  String registerUserProfileRoles();
}
