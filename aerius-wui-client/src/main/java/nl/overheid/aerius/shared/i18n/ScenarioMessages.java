/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile.Situation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.CalculationType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.ExportType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation.UtilType;

/**
 * Messages interface for Scenario related UI elements.
 */
public interface ScenarioMessages {

  String scenario();

  String scenarioStartUpTitle();

  String scenarioStartUpExplain();

  String scenarioMetaDataPanelTitle();

  String scenarioMetaDataReference();

  String scenarioMetaDataProjectName();

  String scenarioMetaDataDescription();

  String scenarioMetaDataCorporation();

  String scenarioMetaDataCity();

  String scenarioMetaDataPostcode();

  String scenarioMetaDataStreetAddress();

  String scenarioConnectExplainInsertApiKey();

  String scenarioConnectExplainCreateApiKey();

  String scenarioConnectApiKeyError();

  String scenarioConnectProjectKeyError();

  String scenarioConnectEmailError();

  String scenarioConnectApiKeyPlaceholder();

  String scenarioConnectEmailPlaceholder();

  String scenarioConnectProjectKeyPlaceholder();

  String scenarioConnectGeneratingKey(String email);

  String scenarioConnectNewJobButton();

  String scenarioConnectExportJobStarted(String projectKey);

  String scenarioConnectExportJobButton();

  String scenarioConnectJobStatusToggleButton();

  String scenarioConnectJobStatusTableScenario();

  String scenarioConnectJobStatusTableStatus();

  String scenarioConnectLoadJobButton(@Select JobType jobType);

  String scenarioConnectNoCalculationStored();

  String scenarioConnectPurgeExistingScenario();

  String scenarioConnectCalculationProjectInformationTitle();

  String scenarioConnectCalculatorProjectInformationDefaultText();

  String scenarioConnectCalculationProjectInformationContactName();

  String scenarioConnectCalculationProjectInformationProjectName();

  String scenarioConnectCalculationProjectInformationLocation();

  String scenarioConnectCalculationProjectInformationDescription();

  String scenarioConnectCalculationCalculationOptionsTitle();

  String scenarioConnectCalculationCalculationOptionsForm();

  String scenarioConnectExportType(@Select ExportType value);

  String scenarioConnectCalculationCalculationOptionsCalculationType();

  String scenarioConnectCalculationType(@Select CalculationType value);

  String scenarioConnectCalculationCalculationOptionsYear();

  String scenarioConnectCalculationCalculationOptionsName();

  String scenarioConnectCalculationCalculationOptionsNameLabel();

  String scenarioConnectCalculationDocumentTableTitleEmpty();

  String scenarioConnectCalculationDocumentTableTitle(String number);

  String scenarioConnectCalculationDocumentTableAddFilePlaceHolder();

  String scenarioConnectCalculationDocumentTableColumnFile();

  String scenarioConnectCalculationDocumentTableColumnSituation();

  String scenarioConnectCalculationDocumentTableColumnSituationText(@Select Situation value);

  String scenarioConnectCalculationLeaveConfirm();

  String scenarioConnectCalculationConfirmStarted();

  String scenarioConnectCalculationWarnings(String warnings);

  String scenarioConnectCalculationErrors(String errors);

  String scenarioConnectCalculationValidationMessage(int code, String message);

  String scenarioConnectJobInfoRunning(long hexagonCount);

  String scenarioConnectJobInfoCompleted(@Select JobType object);

  String scenarioConnectJobInfo(@Select JobState object);

  String scenarioConnectJobCancel();

  String scenarioConnectJobCancelSuccessful();

  String scenarioConnectUtilTitle();

  String scenarioConnectUtilBreadcrumbSeperator();

  String scenarioConnectUtilBreadcrumb1();

  String scenarioConnectUtilBreadcrumb2();

  String scenarioConnectUtilBreadcrumb3();

  String scenarioConnectExplainUtil();

  String scenarioConnectUtilMaxFileValidation(int count);

  String scenarioConnectUtilMinFileValidation(int count);

  String scenarioConnectUtilCorrectSituationValidation();

  String scenarioConnectSelectedUtilTitle(@Select UtilType select);

  String scenarioConnectSelectedUtilDescription(@Select UtilType select);

  String scenarioConnectValidateOptionsTitle();

  String scenarioConnectValidateAsPriorityProjectCheckbox();

  String scenarioConnectOnlyIncreaseCheckBox();

  String scenarioContentProgressInfoMessage();

  String scenarioContentProgressWarningMessage();

  String scenarioConnectImportNoFileSelected();

  String scenarioConnectUtilConfirmStarted();

  String scenarioConnectUtilWarningsTitle();

  String scenarioConnectUtilWarnings(String warnings);

  String scenarioConnectUtilErrorsTitle();

  String scenarioConnectUtilErrors(String error);

  String scenarioConnectUtilSuccessfull();

  String scenarioConnectUtilExcecuted();

  String scenarioConnectUtilExcecutedWithWarning();

}
