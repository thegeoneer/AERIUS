/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;


/**
 * Interface for adding help tool tip messages to widgets. Bind a tool tip message to a widget with the
 * {@link nl.overheid.aerius.wui.main.widget.HelpPopupController}. This should be done in the constructor of the class where the widget is created:
 * <pre>hPC.addWidget([widget], hPC.tt().tt[widget tool tip message]());</pre>
 * <p>To create tool tips with a url to a specific manual page, extends this interface with a new interface. For each message that has an url to point
 * to override the method and add the annotation {@link HelpManualUrl}. For example:
 * <pre>
 * {@literal @}Override
 * {@literal @}HelpManualUrl(200)
 * HelpInfo ttOverviewButtonCalculate();
 * </pre>
 * This will generate a tool tip which points to manual page with id 200.
 * <p>To get the extended interface working in GWT needs to be aware of the extended interface. This needs to done via a gin binding. Add the
 * following line to the gin binding configuration method in the class extending AbstractGinModule:
 * <pre>
 * bind(HelpToolTipInfo.class).to([custom interface].class);
 * </pre>
 */
public interface HelpToolTipInfo extends HelpToolTipMessages<HelpInfo> { //NOPMD - it's an interface because the way it's used.
  int NO_LINK = -1;
  String NO_TEXT = null;
  @Override
  HelpInfo ttLayerPanelSlider();
}
