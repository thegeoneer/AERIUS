/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main;

import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;

import nl.overheid.aerius.wui.main.resources.R;

public interface RootPanelFactory {
  class RootPanelFactoryImpl implements RootPanelFactory {
    @Override
    public Panel getPanel() {
      return RootPanel.get();
    }
  }

  class RootLayoutPanelFactoryImpl implements RootPanelFactory {
    @Override
    public Panel getPanel() {
      RootLayoutPanel.get().setStyleName(R.css().rootLayoutPanel());
      return RootLayoutPanel.get();
    }
  }

  Panel getPanel();
}
