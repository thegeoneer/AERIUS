/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Event when an action is confirmed.
 *
 * @param <T> the type of data confirmed
 */
public class ConfirmEvent<T> extends GwtEvent<ConfirmHandler<T>> {

  private final T value;

  /**
   * @param value confirm value of type T
   */
  public ConfirmEvent(final T value) {
    this.value = value;
  }

  /**
   * A singleton instance
   */
  private static final Type<ConfirmHandler<?>> TYPE = new Type<ConfirmHandler<?>>();

  public static Type<ConfirmHandler<?>> getType() {
    return TYPE;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Type<ConfirmHandler<T>> getAssociatedType() {
    return (Type) getType();
  }

  public T getValue() {
    return value;
  }

  @Override
  protected void dispatch(final ConfirmHandler<T> handler) {
    handler.onConfirm(this);
  }
}
