/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.retrievers;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Interface for Polling agent.
 */
public interface PollingAgent<K, R> {

  /**
   * Start a polling process.
   * @param key key to identify recurring polls on the server
   * @param callback callback that is passed to the service
   */
  void start(final K key, AsyncCallback<R> callback);

  /**
   * Stops the polling process. Any calls that return from the server after {@link #stop()} is called will be ignored.
   */
  void stop();
}
