/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import java.util.HashSet;

import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;

public class SelectionModelConnector<T> {
  private final HashSet<IsInteractiveDataTable<T>> tables = new HashSet<>();

  private final HashSet<IsInteractiveDataTable<T>> ignore = new HashSet<>();

  public void connect(final IsInteractiveDataTable<T> model) {
    tables.add(model);

    model.asDataTable().addSelectionChangeHandler(new Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        if (ignore.contains(model)) {
          ignore.remove(model);
        } else {
          setSelection(model);
        }
      }
    });
  }

  private void setSelection(final IsInteractiveDataTable<T> source) {
    final T selection = ((SingleSelectionModel<T>) source.asDataTable().getSelectionModel()).getSelectedObject();

    for (final IsInteractiveDataTable<T> model : tables) {
      if (model.equals(source)) {
        continue;
      }

      ignore.add(model);
      model.asDataTable().setSelectedItem(selection);
    }
  }
}
