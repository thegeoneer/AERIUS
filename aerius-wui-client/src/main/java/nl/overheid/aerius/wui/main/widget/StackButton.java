/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Styled and selectable button that represents a typed option.
 */
public class StackButton<T> extends Composite implements HasSelectionHandlers<T> {
  interface StackButtonUiBinder extends UiBinder<Widget, StackButton<?>> {}

  private static final StackButtonUiBinder UI_BINDER = GWT.create(StackButtonUiBinder.class);

  private static final int SWITCHPANEL_ACTIVE = 0;
  private static final int SWITCHPANEL_INACTIVE = 1;

  interface CustomStyle extends CssResource {
    String active();
    String disabled();
  }

  @UiField CustomStyle style;

  @UiField SwitchPanel switchPanel;
  @UiField Label label;
  @UiField Image image;
  @UiField Image imageInactive;

  // Enabled by default
  private boolean enabled = true;
  private boolean active;

  private final T object;

  private final ClickHandler clickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      if (active || !enabled) {
        return;
      }

      setActive(true);

      SelectionEvent.fire(StackButton.this, object);
    }
  };

  private StackButton(final T object) {
    this.object = object;

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  public StackButton(final T object, final ImageResource activeFace, final ImageResource inactiveFace) {
    this(object);

    image.setResource(activeFace);
    imageInactive.setResource(inactiveFace);

    init();
  }

  public StackButton(final T object, final DataResource activeFace, final DataResource inactiveFace, final int width) {
    this(object);

    image.setUrl(activeFace.getSafeUri());
    imageInactive.setUrl(inactiveFace.getSafeUri());

    image.getElement().getStyle().setWidth(width, Unit.PX);
    imageInactive.getElement().getStyle().setWidth(width, Unit.PX);

    init();
  }

  private void init() {
    addDomHandler(clickHandler, ClickEvent.getType());

    setImageFace();
  }

  public void setText(final String text) {
    label.setText(text);
  }

  public void setActive(final boolean active) {
    this.active = active;
    setImageFace();
    setStyleName(style.active(), active);
  }

  private void setImageFace() {
    switchPanel.showWidget(active && enabled ? SWITCHPANEL_ACTIVE : SWITCHPANEL_INACTIVE);
  }

  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;

    setStyleName(style.disabled(), !enabled);

    setImageFace();
  }

  @Override
  public HandlerRegistration addSelectionHandler(final SelectionHandler<T> handler) {
    return addHandler(handler, SelectionEvent.getType());
  }
}
