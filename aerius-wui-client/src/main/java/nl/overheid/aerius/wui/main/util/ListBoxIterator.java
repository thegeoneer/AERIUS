/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util;

import java.util.Iterator;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.ListBox;

/**
 * Simple abstract iterator for the ListBox widget.
 */
public abstract class ListBoxIterator implements Iterator<String> {
  /**
   * Iterator for the values of a listBox.
   */
  private static final class ListBoxValueIterator extends ListBoxIterator {
    private ListBoxValueIterator(final ListBox listBox, final boolean selectedOnly) {
      super(listBox, selectedOnly);
    }

    @Override
    public String getValue(final int idx) {
      return listBox.getValue(idx);
    }
  }

  /**
   * Iterator for the item texts of a listbox.
   */
  private static final class ListBoxItemIterator extends ListBoxIterator {
    private ListBoxItemIterator(final ListBox listBox, final boolean selectedOnly) {
      super(listBox, selectedOnly);
    }

    @Override
    public String getValue(final int idx) {
      return listBox.getItemText(idx);
    }
  }

  protected final ListBox listBox;

  private int idx;
  private final boolean selectedOnly;

  private ListBoxIterator(final ListBox listBox, final boolean selectedOnly) {
    this.listBox = listBox;
    this.selectedOnly = selectedOnly;
  }

  @Override
  public boolean hasNext() {
    if (!selectedOnly) {
      return idx < listBox.getItemCount();
    }

    for (int i = idx + 1; i < listBox.getItemCount(); i++) {
      if (listBox.isItemSelected(i)) {
        return true;
      }
    }

    return false;
  }

  @Override
  public String next() {
    if (!selectedOnly) {
      return listBox.getValue(idx++);
    }

    for (int i = idx + 1; i < listBox.getItemCount(); i++) {
      if (listBox.isItemSelected(i)) {
        idx = i;
        return getValue(idx);
      }
    }

    // Shouldn't happen, log anyway
    GWT.log("ListBoxIterator: Next() called while hasNext is (/should be) false!");
    return null;
  }

  /**
   * Get the value for the given listbox index.
   * 
   * @param idx ListBox index.
   * 
   * @return A value.
   */
  public abstract String getValue(int idx);

  @Override
  public void remove() {
    // Not supported
  }

  public static final ListBoxIterator forValues(final ListBox listBox) {
    return forValues(listBox, true);
  }

  public static final ListBoxIterator forValues(final ListBox listBox, final boolean selectedOnly) {
    return new ListBoxValueIterator(listBox, selectedOnly);
  }

  public static final ListBoxIterator forItems(final ListBox listBox) {
    return forItems(listBox, true);
  }

  public static final ListBoxIterator forItems(final ListBox listBox, final boolean selectedOnly) {
    return new ListBoxItemIterator(listBox, selectedOnly);
  }
}
