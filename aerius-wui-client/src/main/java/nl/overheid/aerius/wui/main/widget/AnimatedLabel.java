/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.editor.client.IsEditor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.ui.client.adapters.HasTextEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.util.ComputedStyle;

public class AnimatedLabel extends Composite implements HasText, IsEditor<LeafValueEditor<String>>, HasClickHandlers {
  interface AnimatedLabelUiBinder extends UiBinder<Widget, AnimatedLabel> {}

  private static final AnimatedLabelUiBinder UI_BINDER = GWT.create(AnimatedLabelUiBinder.class);

  private static final int CLEAN_UP_DELAY = 155;

  public interface CustomStyle extends CssResource {
    String noAnimation();
  }

  @UiField CustomStyle style;

  @UiField FlowPanel container;
  @UiField SimplePanel field;

  private String value;

  private LeafValueEditor<String> editor;

  private final Timer cleanupTimer = new Timer() {
    @Override
    public void run() {
      // Make sure we're not animating
      container.addStyleName(style.noAnimation());

      // Remove all but the last widget
      while (container.getWidgetCount() > 1) {
        container.remove(previousAnimationUp ? 0 : 1);
      }

      // Reset the 'top' attribute to 0, and enforce the DOM change
      container.getElement().getStyle().setTop(0, Unit.PX);
      field.getElement().getStyle().clearHeight();
      ComputedStyle.forceStyleRender(container);

      // Re-enable animation
      container.removeStyleName(style.noAnimation());
    }
  };

  private boolean previousAnimationUp;

  public AnimatedLabel() {
    this(null);
  }

  public AnimatedLabel(final String text) {
    initWidget(UI_BINDER.createAndBindUi(this));

    setText(text);
  }

  @Override
  public LeafValueEditor<String> asEditor() {
    if (editor == null) {
      editor = HasTextEditor.of(this);
    }

    return editor;
  }

  @Override
  public String getText() {
    return value;
  }

  @Override
  public void setText(final String value) {
    setText(value, true);
  }

  public void setText(final String value, final boolean up) {
    // Bug out if the text to display is the same as the current text
    if (this.value != null && this.value.equals(value)) {
      return;
    }

    final HTML lbl = new HTML(value);

    final int offsetHeight = up ? animateUp(lbl) : animateDown(lbl);
    if (offsetHeight != 0) {
      field.getElement().getStyle().setHeight(offsetHeight, Unit.PX);
    }

    this.previousAnimationUp = up;
    this.value = value;

    cleanupTimer.cancel();
    cleanupTimer.schedule(CLEAN_UP_DELAY);
  }

  private int animateUp(final Widget widg) {
    container.add(widg);
    final int offsetHeight = widg.getOffsetHeight();

    container.getElement().getStyle().setTop(-container.getOffsetHeight() + offsetHeight, Unit.PX);

    return offsetHeight;
  }

  private int animateDown(final Widget widg) {
    container.insert(widg, 0);
    final int offsetHeight = widg.getOffsetHeight();

    container.addStyleName(style.noAnimation());

    container.getElement().getStyle().setTop(-offsetHeight, Unit.PX);
    ComputedStyle.forceStyleRender(container);

    container.removeStyleName(style.noAnimation());

    container.getElement().getStyle().setTop(0, Unit.PX);

    return offsetHeight;
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return addDomHandler(handler, ClickEvent.getType());
  }
}
