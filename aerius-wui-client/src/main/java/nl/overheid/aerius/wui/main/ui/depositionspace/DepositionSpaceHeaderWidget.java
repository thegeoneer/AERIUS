/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.depositionspace;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResult;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.wui.main.domain.DevelopmentSpaceUtil;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;

class DepositionSpaceHeaderWidget extends Composite implements ValueChangeHandler<Boolean> {
  interface DepositionSpaceHeaderWidgetUiBinder extends UiBinder<Widget, DepositionSpaceHeaderWidget> {}

  private static final DepositionSpaceHeaderWidgetUiBinder UI_BINDER = GWT.create(DepositionSpaceHeaderWidgetUiBinder.class);

  // not using a actual deckpanel because it set styles that don't work here...
  @UiField FlowPanel deckPanel;
  @UiField FlowPanel resultContainer;

  private final EventBus eventBus;
  private final AssessmentArea area;

  public DepositionSpaceHeaderWidget(final EventBus eventBus, final AssessmentArea area, final ArrayList<DevelopmentRuleResult> results) {
    this.eventBus = eventBus;
    this.area = area;

    initWidget(UI_BINDER.createAndBindUi(this));

    final boolean containsOnlyNotExceedingResults = containsOnlyNotExceedingResults(results);
    for (final DevelopmentRuleResult result : results) {
      if (!result.isEmpty()) {
        if (containsOnlyNotExceedingResults || (result.getRule() != DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK)) {
          final Image image = new Image(DevelopmentSpaceUtil.getDevelopmentRuleIcon(result.getRule(), false));
          image.setTitle(M.messages().calculatorDevelopmentRuleType(result.getRule()));
          resultContainer.add(image);
        }
      }
    }
    showHeaders(false);
  }

  private boolean containsOnlyNotExceedingResults(final ArrayList<DevelopmentRuleResult> results) {
    boolean containsOnly = true;

    for (final DevelopmentRuleResult result : results) {
      if (((result.getRule() == DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK) && result.isEmpty())
          || ((result.getRule() != DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK) && !result.isEmpty())) {
        containsOnly = false;
        break;
      }
    }

    return containsOnly;
  }

  @Override
  public void onValueChange(final ValueChangeEvent<Boolean> event) {
    showHeaders(event.getValue());
  }

  @UiHandler("zoomButton")
  public void onZoomClick(final ClickEvent e) {
    e.stopPropagation();

    eventBus.fireEvent(new LocationChangeEvent(area.getBounds()));
  }

  private void showHeaders(final boolean showZoom) {
    deckPanel.getWidget(0).setVisible(showZoom); // zoom
    deckPanel.getWidget(1).setVisible(!showZoom); // result
  }
}
