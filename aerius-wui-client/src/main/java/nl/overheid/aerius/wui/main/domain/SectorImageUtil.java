/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.domain;

import com.google.gwt.resources.client.ImageResource;

import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.resources.SectorImageResources;

/**
 * Util class for sector related helper methods.
 */
public final class SectorImageUtil {

  private SectorImageUtil() {
  }

  /**
   * Returns the {@link ImageResource} for the {@link EmissionSource} object.
   * For Farm Lodging this means the specific animal image, for the other sources
   * it means the {@link nl.overheid.aerius.shared.domain.sector.Sector} image.
   *
   * @param object the emission source.
   * @return the image resource.
   */
  public static ImageResource getEmissionSourceImageResource(final FarmEmissionSource object) {
    return FarmUtil.getFarmImageResource(object);
  }

  /**
   * Returns the {@link ImageResource} for the {@link EmissionSource} object.
   * For Farm Lodging this means the specific animal image, for the other sources
   * it means the {@link nl.overheid.aerius.shared.domain.sector.Sector} image.
   *
   * @param object the emission source.
   * @return the image resource.
   */
  public static ImageResource getEmissionSourceImageResource(final EmissionSource object) {
    return object == null ? null : getSectorImageResource(object.getSector().getProperties().getIcon());
  }

  /**
   * Returns the {@link ImageResource} for the {@link SectorIcon}. If no image
   * available the image OTHER is returned.
   *
   * @param icon sector icon
   * @return ImageResource or Other in case image available
   */
  public static ImageResource getSectorImageResource(final SectorIcon icon) {
    final SectorImageResources sir = R.images();
    final ImageResource ir;

    switch (icon) {
    case ABROAD:
      ir = sir.iconSectorGroupAbroad();
      break;
    case AVIATION:
      ir = sir.iconSectorAviation();
      break;
    case AVIATION_AERODROME:
      ir = sir.iconSectorAviationAerodome();
      break;
    case AVIATION_TAKE_OFF:
      ir = sir.iconSectorAviationTakeOff();
      break;
    case AVIATION_TAXI:
      ir = sir.iconSectorAviationTaxi();
      break;
    case AVIATION_TOUCH_DOWN:
      ir = sir.iconSectorAviationTouchDown();
      break;
    case BACKGROUND_DEPOSITION:
      ir = sir.iconSectorGroupBackgroundDeposition();
      break;
    case CONSTRUCTION:
      ir = sir.iconSectorConstruction();
      break;
    case CONSUMERS:
      ir = sir.iconSectorConsumers();
      break;
    case ENERGY:
      ir = sir.iconSectorEnergy();
      break;
    case ENERGY_POWER_PLANT:
      ir = sir.iconSectorEnergyPowerPlant();
      break;
    case ENERGY_PRODUCTION_DISTRIBUTION:
      ir = sir.iconSectorEnergyProductionDistribution();
      break;
    case AGRICULTURE:
      ir = sir.iconSectorGroupAgriculture();
      break;
    case FARM_FERTILIZER:
      ir = sir.iconSectorFarmFertilizer();
      break;
    case FARM_LODGE:
      ir = sir.iconSectorFarmLodge();
      break;
    case FARM_MANURE_STORAGE:
      ir = sir.iconSectorFarmManureStorage();
      break;
    case FARM_OTHER:
      ir = sir.iconSectorFarmOther();
      break;
    case FARM_PASTURE:
      ir = sir.iconSectorFarmPasture();
      break;
    case GREENHOUSE:
      ir = sir.iconSectorGreenhouse();
      break;
    case HDO:
      ir = sir.iconSectorHdo();
      break;
    case INDUSTRY:
      ir = sir.iconSectorIndustry();
      break;
    case INDUSTRY_BASE_MATERIAL:
      ir = sir.iconSectorIndustryBaseMaterial();
      break;
    case INDUSTRY_BUILDING_MATERIALS:
      ir = sir.iconSectorIndustryBuildingMaterials();
      break;
    case INDUSTRY_CHEMICAL_INDUSTRY:
      ir = sir.iconSectorIndustryChemicalIndustry();
      break;
    case INDUSTRY_FOOD_INDUSTRY:
      ir = sir.iconSectorIndustryFoodIndustry();
      break;
    case INDUSTRY_METAL_INDUSTRY:
      ir = sir.iconSectorIndustryMetalIndustry();
      break;
    case MEASUREMENT_CORRECTION:
      ir = sir.iconSectorGroupMeasurementCorrection();
      break;
    case MINING:
      ir = sir.iconSectorMining();
      break;
    case OFFICES_SHOPS:
      ir = sir.iconSectorOfficesShops();
      break;
    case OTHER:
      ir = sir.iconSectorOther();
      break;
    case PLAN:
      ir = sir.iconSectorPlan();
      break;
    case RAIL_EMPLACEMENT:
      ir = sir.iconSectorRailEmplacement();
      break;
    case RAIL_TRANSPORT:
      ir = sir.iconSectorRailTransport();
      break;
    case RECREATION:
      ir = sir.iconSectorRecreation();
      break;
    case ROAD_CONSTRUCTION_INDUSTRY:
      ir = sir.iconSectorRoadConstructionIndustry();
      break;
    case ROAD_FARM:
      ir = sir.iconSectorRoadFarm();
      break;
    case ROAD_FREEWAY:
      ir = sir.iconSectorRoadFreeway();
      break;
    case ROAD_OTHER:
      ir = sir.iconSectorRoadOther();
      break;
    case ROAD_CONSUMER:
      ir = sir.iconSectorRoadConsumer();
      break;
    case ROAD_NON_URBAN:
      ir = sir.iconSectorRoadNonUrban();
      break;
    case ROAD_URBAN:
      ir = sir.iconSectorRoadUrban();
      break;
    case ROAD_UNDERLYING_NETWORK:
      ir = sir.iconSectorRoadUnderlyingNetwork();
      break;
    case SHIPPING:
      ir = sir.iconSectorGroupShipping();
      break;
    case SHIPPING_DOCK:
      ir = sir.iconSectorShippingDock();
      break;
    case SHIPPING_FISHING:
      ir = sir.iconSectorShippingFishing();
      break;
    case SHIPPING_INLAND:
      ir = sir.iconSectorShippingInland();
      break;
    case SHIPPING_INLAND_DOCK:
      ir = sir.iconSectorShippingInlandDocked();
      break;
    case SHIPPING_MARITIME_MOORING:
      ir = sir.iconSectorShippingMaritimeMooring();
      break;
    case SHIPPING_MARITIME_NCP:
      ir = sir.iconSectorShippingMaritimeNCP();
      break;
    case SHIPPING_RECREATIONAL:
      ir = sir.iconSectorShippingRecreational();
      break;
    case TRAFFIC_AND_TRANSPORT:
      ir = sir.iconSectorGroupTrafficAndTransport();
      break;
    case WASTE_MANAGEMENT:
      ir = sir.iconSectorWasteManagement();
      break;
    case RETURNED_DEPOSITION_SPACE:
      ir = sir.iconSectorGroupReturnedDeposition();
      break;
    case RETURNED_DEPOSITION_SPACE_LIMBURG:
      ir = sir.iconSectorGroupReturnedLimburg();
      break;
    case DEPOSITION_SPACE_ADDITION:
      ir = sir.iconSectorGroupRaised();
      break;
    default:
      ir = sir.iconSectorOther();
      break;
    }
    return ir;
  }
}
