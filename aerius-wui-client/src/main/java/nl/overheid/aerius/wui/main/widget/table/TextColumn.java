/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.user.client.ui.Label;

import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public abstract class TextColumn<L> extends WidgetFactory<L, Label> {

  private final boolean displayTitle;

  public TextColumn() {
    this(false);
  }

  public TextColumn(final boolean displayTitle) {
    this.displayTitle = displayTitle;
  }

  @Override
  public Label createWidget(final L object) {
    final Label label = new Label();

    final String value = getValue(object);

    if (value != null) {
      label.setText(value);
    }

    if (displayTitle) {
      final String titleText = getTitleText(object);
      label.setTitle(titleText == null ? value : titleText);
    }

    return label;
  }

  public String getTitleText(final L object) {
    return null;
  }

  public abstract String getValue(final L object);
}
