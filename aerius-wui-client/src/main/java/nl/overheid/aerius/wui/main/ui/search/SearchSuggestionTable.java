/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.search;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;

import nl.overheid.aerius.shared.domain.search.SearchSuggestion;
import nl.overheid.aerius.shared.domain.search.SearchSuggestionType;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.LazyTableColumn;
import nl.overheid.aerius.wui.main.widget.table.SimpleVariableCollapsibleDivTable;
import nl.overheid.aerius.wui.main.widget.table.TableColumn;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public abstract class SearchSuggestionTable<T extends SearchSuggestionType, E extends SearchSuggestion<T, E>> extends Composite implements IsInteractiveDataTable<E> {
  public interface CustomStyle extends CssResource {
    String uncollapsibleTitleText();
  }

  @UiField(provided = true) public SimpleVariableCollapsibleDivTable<E> divTable;

  @UiField(provided = true) public TextColumn<E> nameLabel;
  @UiField(provided = true) public TableColumn<E, E, SearchSuggestionTable<T, E>> contentColumn;

  @UiField public CustomStyle style;

  public SearchSuggestionTable(final HelpPopupController hpC, final SearchAction<T, E> searchAction) {
    divTable = new SimpleVariableCollapsibleDivTable<E>(true) {
      @Override
      protected boolean hasCollapsibleContent(final E object) {
        return SearchSuggestionTable.this.hasCollapsibleContent(object);
      }
    };
    divTable.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        final E selection = divTable.getSelectionModel().getSelectedObject();

        // Don't select deselected suggestions.
        if (selection != null && !hasCollapsibleContent(selection)) {
          searchAction.selectSuggestion(selection);
        }
      }
    });

    nameLabel = new TextColumn<E>() {
      @Override
      public String getValue(final E object) {
        return getName(object);
      }

      @Override
      public void applyCellOptions(final Widget cell, final E object) {
        super.applyCellOptions(cell, object);

        cell.addDomHandler(new ClickHandler() {
          @Override
          public void onClick(final ClickEvent event) {
            searchAction.selectSuggestion(object);
          }

        }, ClickEvent.getType());

        if (!hasCollapsibleContent(object)) {
          cell.addStyleName(style.uncollapsibleTitleText());
        }
      }
    };

    contentColumn = new LazyTableColumn<ArrayList<E>, E, E, SearchSuggestionTable<T, E>>() {
      @Override
      public ArrayList<E> getRowData(final E object) {
        return object.getSubItems();
      }

      @Override
      public SearchSuggestionTable<T, E> createDataTable(final E object) {
        return createSuggestionTable(hpC, searchAction);
      }

      @Override
      protected void doLazyCall(final E origin, final AsyncCallback<ArrayList<E>> callback) {
        searchAction.getSubSuggestions(origin, callback);
      }

      @Override
      protected boolean isLazyCall(final E object) {
        return object.hasLazySubItems();
      }
    };
  }

  protected abstract SearchSuggestionTable<T, E> createSuggestionTable(HelpPopupController hpC, SearchAction<T, E> searchAction);

  protected abstract String getName(E object);

  private boolean hasCollapsibleContent(final E suggestion) {
    return suggestion.hasLazySubItems() || !suggestion.getSubItems().isEmpty();
  }

  @Override
  public SimpleVariableCollapsibleDivTable<E> asDataTable() {
    return divTable;
  }
}
