/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.widget.WidgetFactory;

/**
 * Simple widget factory which can generate a {@link Widget} out of some object type.
 * 
 * Exists because it doesn't care about explicit widget typing.
 * 
 * Creates a panel and nothing else by default. You should override.
 * 
 * @param <L> Type of object to generate a widget out of.
 */
public abstract class SimpleWidgetFactory<L> extends WidgetFactory<L, Widget> {
  @Override
  public Widget createWidget(final L object) {
    return new SimplePanel();
  }
}
