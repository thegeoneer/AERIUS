/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.animation;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.user.client.ui.UIObject;

/**
 * Fade In or Fade Out animation.
 *
 * Creates a fading effect by setting the Opacity of the widget. If
 * {@value Fade#OUT} is used the opacity is set from 1.0 to 0.0, else the
 * opacity is set from 0.0 to 1.0.
 */
public class FadeAnimation extends Animation {

  public enum Fade {
    IN, OUT
  }

  private final UIObject uIObject;
  private final Fade fade;

  /**
   * Initializes the Animation for given object and sets Fade type. If Fade
   * is {@link Fade#IN} the opacity of the widget is set to 0.0.
   *
   * @param uIObject
   * @param fade
   */
  public FadeAnimation(final UIObject uIObject, final Fade fade) {
    this.uIObject = uIObject;
    this.fade = fade;
    if (fade == Fade.IN) {
      uIObject.getElement().getStyle().setOpacity(0);
    }
  }

  /**
   * Sets the opacity value using the progress parameter.
   *
   */
  @Override
  protected void onUpdate(final double progress) {
    uIObject.getElement().getStyle().setOpacity(fade == Fade.OUT ? 1.0 - progress : progress);
  }
}
