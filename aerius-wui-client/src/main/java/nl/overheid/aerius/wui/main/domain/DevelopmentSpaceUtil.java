/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.domain;

import com.google.gwt.resources.client.ImageResource;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Util class for Development space related data.
 */
public final class DevelopmentSpaceUtil {

  private DevelopmentSpaceUtil() {
  }

  /**
   * @param rule The rule to get the proper icon for.
   * @param inactive Indicates if the image should be inactive or not.
   * @return The proper icon.
   */
  public static ImageResource getDevelopmentRuleIcon(final DevelopmentRule rule, final boolean inactive) {
    return inactive ? getDevelopmentRuleInactiveIcon(rule) : getDevelopmentRuleIcon(rule);
  }

  /**
   * Returns an ImageResource based on the {@link DevelopmentRule}.
   *
   * @param rule The rule to get the proper icon for.
   * @return Returns ImageResource
   */
  public static ImageResource getDevelopmentRuleIcon(final DevelopmentRule rule) {
    final ImageResource ir;
    switch (rule) {
    case NOT_EXCEEDING_SPACE_CHECK:
      ir = R.images().depositionSpaceAvailable();
      break;
    case EXCEEDING_SPACE_CHECK:
      ir = R.images().depositionSpaceShortage();
      break;
    default:
      ir = null;
      break;
    }
    return ir;
  }

  /**
   * Returns an inactive ImageResource base on the {@link DevelopmentRule}.
   *
   * @param rule The rule to get the proper icon for.
   * @return Returns ImageResource
   */
  public static ImageResource getDevelopmentRuleInactiveIcon(final DevelopmentRule rule) {
    final ImageResource ir;
    switch (rule) {
    case NOT_EXCEEDING_SPACE_CHECK:
      ir = R.images().depositionSpaceAvailableInactive();
      break;
    case EXCEEDING_SPACE_CHECK:
      ir = R.images().depositionSpaceShortageInactive();
      break;
    default:
      ir = null;
      break;
    }
    return ir;
  }

}
