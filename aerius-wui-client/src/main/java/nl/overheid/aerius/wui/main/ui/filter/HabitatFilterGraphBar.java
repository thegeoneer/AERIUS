/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.filter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.Range;
import nl.overheid.aerius.shared.domain.deposition.HabitatFilterRangeContent;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.util.scaling.ScalingUtil;

/**
 * @param <E> The type of the (Range) object that this GraphBar is using.
 */
public class HabitatFilterGraphBar<E extends Range> extends Composite {
  interface HabitatFilterGraphBarUiBinder extends UiBinder<Widget, HabitatFilterGraphBar<?>> {
  }

  private static HabitatFilterGraphBarUiBinder uiBinder = GWT.create(HabitatFilterGraphBarUiBinder.class);

  interface CustomStyle extends CssResource {
    String inactive();
  }

  @UiField CustomStyle style;
  @UiField SimplePanel barContainer;
  @UiField Widget bar;

  private final ScalingUtil scaler;
  private final HabitatFilterRangeContent content;

  /**
   * @param scaler The scaler to use.
   * @param content The content for this particular bar.
   */
  public HabitatFilterGraphBar(final ScalingUtil scaler, final HabitatFilterRangeContent content) {
    this.scaler = scaler;
    this.content = content;

    initWidget(uiBinder.createAndBindUi(this));
  }

  /**
   * @param max The maximum value of a bar.
   */
  public void updateWidth(final double max) {
    final double width = scaler.scale(content.getTotalValue(), max);

    bar.getElement().getStyle().setWidth(width * 100, Unit.PCT);
  }

  /**
   * @param active Set this bar to be active (in current filtered range).
   */
  public void setActive(final boolean active) {
    barContainer.setStyleName(style.inactive(), !active);
  }

  /**
   * @param color The color to use for the bar.
   */
  public void setColor(final String color) {
    bar.getElement().getStyle().setBackgroundColor(ColorUtil.webColor(color));
  }

  public HabitatFilterRangeContent getContent() {
    return content;
  }

  /**
   * Set the tooltip of the bar.
   * @param title Text of the tooltip
   */
  public void setBarTitle(final String title) {
    barContainer.setTitle(title);
  }

}
