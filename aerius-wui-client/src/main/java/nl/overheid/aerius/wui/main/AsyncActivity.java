/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.main.widget.ContentLoadingWidget;

public class AsyncActivity extends AbstractActivity {
  private final AsyncActivityFactory factory;
  private AcceptsOneWidget panel;
  private EventBus eventBus;

  @Inject
  public AsyncActivity(@Assisted final AsyncActivityFactory factory) {
    this.factory = factory;

    factory.setActivity(this);
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.panel = panel;
    this.eventBus = eventBus;
    panel.setWidget(new ContentLoadingWidget());

    factory.doRunAsync();
  }

  public void doDeferredStart(final Activity createActivity) {
    createActivity.start(panel, eventBus);
  }
}
