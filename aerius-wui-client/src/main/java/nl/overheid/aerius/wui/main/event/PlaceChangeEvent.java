/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.event;

import com.google.gwt.place.shared.Place;

/**
 * Wrapper for the {@link com.google.gwt.place.shared.PlaceChangeEvent} to make
 * it possible to use place changes events in combination with the gwt event library.
 * The application will fire this event when an actual place event is received.
 */
public class PlaceChangeEvent extends SimpleGenericEvent<Place> {

  public PlaceChangeEvent(final Place obj) {
    super(obj);
  }
}
