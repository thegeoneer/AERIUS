/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public abstract class HoverButtonColumn<T> extends WidgetFactory<T, Button> {
  @Override
  public Button createWidget(final T object) {
    final Button button = new Button();
    button.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        event.stopPropagation();
        HoverButtonColumn.this.onClick(button, object);
      }
    });

    return button;
  }

  /**
   * Creates a widget.
   *
   * @param rowContainer
   *          Row container the cell will be stuck in.
   * @param object
   *          Value this widget will be generated with.
   *
   * @return A widget.
   */
  @Override
  public final Button createWidget(final Widget rowContainer, final T object) {
    final Button widget = createWidget(object);

    if (widget != null) {
      applyRowOptions(rowContainer, widget);
    }

    return widget;
  }

  protected abstract void onClick(Button button, final T object);

  @Override
  public void applyRowOptions(final Widget rowContainer, final Button button) {
    button.getElement().getStyle().setVisibility(Visibility.HIDDEN);

    rowContainer.addDomHandler(new MouseOverHandler() {
      @Override
      public void onMouseOver(final MouseOverEvent event) {
        button.getElement().getStyle().setVisibility(Visibility.VISIBLE);
      }
    }, MouseOverEvent.getType());
    rowContainer.addDomHandler(new MouseOutHandler() {
      @Override
      public void onMouseOut(final MouseOutEvent event) {
        button.getElement().getStyle().setVisibility(Visibility.HIDDEN);
      }
    }, MouseOutEvent.getType());
  }
}
