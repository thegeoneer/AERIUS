/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.Notification;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.DefaultPlace;
import nl.overheid.aerius.wui.main.ui.ApplicationRootView;
import nl.overheid.aerius.wui.main.ui.systeminfo.SystemInfoPollingAgent;
import nl.overheid.aerius.wui.main.util.CompatibilityUtil;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.util.ParamUtil;
import nl.overheid.aerius.wui.main.util.SecurityUtil;
import nl.overheid.aerius.wui.main.util.development.DevPanel;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger;

/**
 * Root of the application logic.
 */
public class ApplicationRoot {

  @Inject private PlaceController placeController;

  @Inject private EventBus eventBus;

  @Inject private ActivityMapper actvityMapper;

  @Inject private PlaceHistoryMapper placeHistoryMapper;

  @Inject @DefaultPlace Place defaultPlace;

  @Inject private RootPanelFactory rootPanelFactory;

  @Inject
  private SystemInfoPollingAgent systemMessagePollingAgent;

  @Inject private SuperNitroTurboLogger superTurboLogger;

  @Inject private DevPanel devPanel;

  @Inject private ApplicationInitializer initializer;

  private ApplicationRootView appDisplay;

  /**
   * Starts the application.
   */
  public void startUp() {
    M.setDebug(ParamUtil.inDebug());
    AeriusGinjector.INSTANCE.inject(this);
    final ActivityManager appActivityManager = new ActivityManager(actvityMapper, eventBus);
    final PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(placeHistoryMapper);
    appDisplay = AeriusGinjector.INSTANCE.getApplicationRootView();
    appActivityManager.setDisplay(appDisplay);
    appDisplay.asWidget().setVisible(false);

    initializer.init();

    rootPanelFactory.getPanel().add(appDisplay);

    historyHandler.register(placeController, eventBus, defaultPlace);

    // Wrap GWT's PlaceChangeEvent with our own
    eventBus.addHandler(PlaceChangeEvent.TYPE, new PlaceChangeEvent.Handler() {
      @Override
      public void onPlaceChange(final PlaceChangeEvent event) {
        eventBus.fireEvent(new nl.overheid.aerius.wui.main.event.PlaceChangeEvent(event.getNewPlace()));
      }
    });
    // Go to the place represented by the current URL or if no place the default place:
    historyHandler.handleCurrentHistory();
    setUncaughtExceptionHandler();

    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        CompatibilityUtil.doVersionCompatibilityChecks(eventBus);
      }
    });

    systemMessagePollingAgent.start();
  }

  /**
   * Hides the main application display, if attached.
   */
  public void hideDisplay() {
    if (appDisplay != null && appDisplay.asWidget().isAttached()) {
      //destroy panel. unrecoverable error
      appDisplay.asWidget().removeFromParent();
    }
  }

  /**
   * Shows the main display.
   */
  public void showDisplay() {
    appDisplay.asWidget().setVisible(true);
  }

  private void setUncaughtExceptionHandler() {
    GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
      @Override
      public void onUncaughtException(final Throwable e) {
        final Throwable cause = e == null ? null : e.getCause();

        SecurityUtil.reloadOnAuthenticationExpired(e);
        
        if ((e instanceof IncompatibleRemoteServiceException) || ((cause instanceof IncompatibleRemoteServiceException)
            && Window.confirm(M.messages().errorInternalApplicationOutdated()))) {
          Window.Location.reload();
        }

        final Notification notification = new Notification(e);
        //Only log errors on the server if they are not one of the following exceptions:
        if (!(isKnownException(e) || isKnownException(cause))) {
          final String message = e == null ? "[null]" : e.getMessage();
          Logger.getLogger("UncaughtExceptionHandler").log(Level.SEVERE, message + ";reference=" + notification.getReference(), e);
        }

        NotificationUtil.broadcast(eventBus, notification);
      }
    });
  }

  private boolean isKnownException(final Throwable e) {
    return e instanceof IncompatibleRemoteServiceException
        || e instanceof InvocationException
        || e instanceof StatusCodeException
        || e instanceof AeriusException;
  }
}
