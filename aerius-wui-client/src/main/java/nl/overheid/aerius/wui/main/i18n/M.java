/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.i18n;

import java.util.MissingResourceException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.StatusCodeException;

import nl.overheid.aerius.shared.exception.APIException;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.i18n.AeriusExceptionMessages;
import nl.overheid.aerius.shared.i18n.AeriusMessages;

/**
 * Global class to messages interfaces.
 */
public final class M {
  private static final Logger LOG = Logger.getLogger("M");
  private static final AeriusMessages AERIUS_MESSAGES = GWT.create(AeriusMessages.class);
  private static final AeriusExceptionMessages AERIUS_EXCEPTIONS = GWT.create(AeriusExceptionMessages.class);

  private static final int STATUSCODE_SERVICE_UNAVAILABLE = 503;

  private static boolean debug;

  private M() {}

  /**
   * {@link AeriusMessages} instance.
   *
   * @return instance of messages interface.
   */
  public static AeriusMessages messages() {
    return AERIUS_MESSAGES;
  }

  /**
   * Returns a user friendly readable message of the exception.
   *
   * @param e
   *          Exception to format
   * @return user friendly version of the message
   */
  public static String getErrorMessage(final Throwable e) {
    String message = "";
    final Throwable cause = e == null ? null : e.getCause();
    if (e == null) {
      // do nothing, just return empty string.
    } else if (debug) {
      message = e.getMessage();
    } else if (e instanceof IncompatibleRemoteServiceException || cause instanceof IncompatibleRemoteServiceException) {
      message = M.messages().errorInternalApplicationOutdated();
    } else if (e instanceof StatusCodeException || cause instanceof StatusCodeException) {
      final StatusCodeException scException = e instanceof StatusCodeException ? (StatusCodeException) e : (StatusCodeException) cause;

      message = scException.getStatusCode() == STATUSCODE_SERVICE_UNAVAILABLE ? M.messages().serviceUnavailable() : M.messages().errorConnection();
    } else if (e instanceof InvocationException || cause instanceof InvocationException) {
      message = M.messages().errorConnection();
    } else if (e instanceof APIException) {
      message = getAPIException((APIException) e);
    } else if (cause instanceof APIException) {
      message = getAPIException((APIException) cause);
    } else if (e instanceof AeriusException) {
      message = getServerExceptionMessage((AeriusException) e);
    } else if (cause instanceof AeriusException) {
      message = getServerExceptionMessage((AeriusException) cause);
    } else {
      message = M.messages().errorInternalFatal();
    }
    return message;
  }

  private static String getAPIException(final APIException e) {
    return M.messages().errorAPIError(e.getReason().getErrorCode(), e.getMessage());
  }

  /**
   * Set messages in debug mode.
   *
   * @param debug
   *          the debug to set
   */
  public static void setDebug(final boolean debug) {
    M.debug = debug;
  }

  /**
   * Returns the formatted message based on the error code and optional additional arguments in the ServerExeption.
   *
   * @param se
   *          ServerException server exception to decode
   * @return Human readable error message based on the error code
   */
  private static String getServerExceptionMessage(final AeriusException se) {
    String messageToDisplay = null;

    try {
      final String format = AERIUS_EXCEPTIONS.getString(se.getReason().getErrorCodeKey());
      final String[] args = se.getArgs() == null ? null : (String[]) se.getArgs();

      if (args == null) {
        messageToDisplay = format;

      } else {
        messageToDisplay = format(format, args);
      }
    } catch (final MissingResourceException e) {
      LOG.log(Level.SEVERE, "[ServerErrorUtils - Client] Missing resource for error code: " + se.getReason());
      messageToDisplay = M.messages().errorInternalFatal();
    }
    return messageToDisplay;
  }

  /**
   * Constructs a String based on a format with arguments. The arguments are code as <code>{0}</code>, where <code>0</code> indicates the index in the
   * variable list of arguments.
   *
   * @param format
   * @param args
   * @return
   */
  static String format(final String format, final String... args) {
    final StringBuilder sb = new StringBuilder();
    final int len = format.length();
    int cur = 0;

    while (cur < len) {
      final int fi = format.indexOf('{', cur);

      if (fi == -1) {
        sb.append(format.substring(cur, len));
        break;
      } else {
        sb.append(format.substring(cur, fi));
        final int si = format.indexOf('}', fi);

        if (si == -1) {
          sb.append(format.substring(fi));
          break;
        } else {
          final String nStr = format.substring(fi + 1, si);
          final int i = Integer.parseInt(nStr);

          sb.append(args[i] == null ? "" : SafeHtmlUtils.htmlEscapeAllowEntities(args[i]));
          cur = si + 1;
        }
      }
    }
    return sb.toString();
  }

}
