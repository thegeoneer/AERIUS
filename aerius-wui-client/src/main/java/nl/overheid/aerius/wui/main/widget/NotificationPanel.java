/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.PopupPanel.AnimationType;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.Notification;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.NotificationEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;

public class NotificationPanel extends Composite {
  private static final int POPUP_WIDTH = 315;
  private static final int LIST_SHOW_TIME = 5000;
  private static final int MAX_ITEMS = 9;

  interface NotificationEventBinder extends EventBinder<NotificationPanel> {}

  private final NotificationEventBinder eventBinder = GWT.create(NotificationEventBinder.class);

  @UiField DivElement numNotificationText;
  @UiField FocusPanel notificationButton;
  private final AttachedPopupPanel attachedPopupPanel = new AttachedPopupPanel();
  private final NotificationPopup notificationList;
  private final Timer timer;

  private boolean stayActive;
  private Widget alignmentHandle;

  @Inject
  public NotificationPanel(final EventBus eventBus, final HelpPopupController hpC, final NotificationButton notificationButton) {
    initWidget(notificationButton.createAndBindUi(this));
    this.notificationList = new NotificationPopup(this);

    numNotificationText.getStyle().setDisplay(Display.NONE);
    attachedPopupPanel.setAnimationType(AnimationType.ROLL_DOWN);
    attachedPopupPanel.addStyleName(R.css().notificationListPanel());
    attachedPopupPanel.setArrowColor(R.css().colorBorder(), R.css().colorNotificationItemReadBackground());
    attachedPopupPanel.setWidget(notificationList);
    attachedPopupPanel.attachToWidget(getWidget());

    timer = new Timer() {
      @Override
      public void run() {
        if (!stayActive) {
          attachedPopupPanel.hide();
        }
      }
    };

    Window.addResizeHandler(new ResizeHandler() {
      @Override
      public void onResize(final ResizeEvent event) {
        setPopupPosition();
      }
    });

    eventBinder.bindEventHandlers(this, eventBus);

    hpC.addWidget(this, hpC.tt().ttNotificationCenter());

    notificationList.ensureDebugId(TestID.DIV_NOTIFICATIONLIST);
    getWidget().ensureDebugId(TestID.BUTTON_NOTIFICATION);
    attachedPopupPanel.ensureDebugId(TestID.DIV_NOTIFICATIONPOPUP);
  }

  @EventHandler
  void onNotification(final NotificationEvent event) {
    updateNotifications(event.getValue());
  }

  @UiHandler("notificationButton")
  void onButtonClick(final ClickEvent e) {
    if (attachedPopupPanel.isShowing()) {
      notificationList.readAll();
      attachedPopupPanel.hide();
      updateNotificationCounter();
    } else {
      showListPanel();
    }
  }

  public void updateVisiblity() {
    updateNotificationCounter();
  }

  public void setStayActive(final boolean stayActive) {
    this.stayActive = stayActive;
    if (!stayActive) {
      setTimer();
    }
  }

  public void hide() {
    timer.cancel();
    attachedPopupPanel.hide();
  }

  public void setAlignmentHandle(final Widget elem) {
    this.alignmentHandle = elem;
  }

  private void showListPanel() {
    stayActive = false;
    setPopupPosition();
    attachedPopupPanel.show();
  }

  private void setPopupPosition() {
    final Widget handle = alignmentHandle == null ? notificationButton : alignmentHandle;

    attachedPopupPanel.setPopupPosition2(handle.getAbsoluteLeft() + handle.getOffsetWidth() - POPUP_WIDTH,
        handle.getAbsoluteTop() + handle.getOffsetHeight());
  }

  private void updateNotifications(final Notification notification) {
    notificationList.add(notification);
    updateNotificationCounter();

    if (!attachedPopupPanel.isShowing()) {
      // Peek into the list
      showListPanel();
      setTimer();
    }
  }

  private void updateNotificationCounter() {
    final int num = notificationList.getNumUnread();

    numNotificationText.getStyle().setDisplay(num == 0 ? Display.NONE : Display.BLOCK);
    numNotificationText.setInnerText(String.valueOf(num > MAX_ITEMS ? M.messages().notificationsMoreThanMax() : num));
  }

  private void setTimer() {
    // Remove it after a set interval
    timer.cancel();
    timer.schedule(LIST_SHOW_TIME);
  }
}
