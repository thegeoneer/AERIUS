/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main;

import com.google.gwt.user.client.DOM;

/**
 * Don't start the application for unsupported browser, but instead display the
 * download other browsers panel.
 */
public class ApplicationNotSupportedBrowser extends Application {

  private static final String SPLASH_BROWSER_NOT_SUPPORTED = "browserNotSupported";

  @Override
  public void onModuleLoad() {
    DOM.getElementById(LOADING_IMG_ID).removeFromParent();
    DOM.getElementById(SPLASH_SCREEN_ID).addClassName(SPLASH_BROWSER_NOT_SUPPORTED);
  }
}
