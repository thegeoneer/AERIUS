/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.logical.shared.AttachEvent.Handler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PopupPanel.AnimationType;
import com.google.gwt.user.client.ui.PopupPanel.PositionCallback;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.util.ParamUtil;

public class ContextDivTable<DT extends DivTable<T, R>, T, R extends DivTableRow> implements IsDataTable<T>, DivTableRowDecorator<T, R> {

  private static final int PROD_DELAY = 400;
  private static final int DEBUG_DELAY = 40000;
  private static final int HIDE_DELAY = ParamUtil.inDebug() ? DEBUG_DELAY : PROD_DELAY;

  private static final int SHOW_DELAY = 25;

  private static final int LEFT_MARGIN = 10;

  private class ShowTimer extends Timer {
    private Widget row;
    private T item;

    @Override
    public void run() {
      if (popupContent.view(item)) {
        popup.setPopupPositionAndShow(new PositionCallback() {
          @Override
          public void setPosition(final int offsetWidth, final int offsetHeight) {
            popup.setPopupPosition(row.getAbsoluteLeft() + row.getOffsetWidth() + LEFT_MARGIN,
                Math.min(row.getAbsoluteTop(), Window.getClientHeight() - offsetHeight));
            popupContent.setArrowTop(row.getOffsetHeight() / 2);
          }
        });
      } else {
        popup.hide();
      }
    }

    public void reset(final Widget row, final T item) {
      this.row = row;
      this.item = item;

      cancel();
      schedule(SHOW_DELAY);
    }
  }

  private final DT divTable;

  private final PopupPanel popup = new PopupPanel();
  private final ContextualContent<T> popupContent;

  private final Timer timer = new Timer() {
    @Override
    public void run() {
      popup.hide();
    }
  };
  private final ShowTimer showTimer = new ShowTimer();

  public ContextDivTable(final DT divTable, final ContextualContent<T> popupContent) {
    this.divTable = divTable;
    this.popupContent = popupContent;

    popup.setAnimationEnabled(true);
    popup.setAnimationType(AnimationType.ROLL_DOWN);
    popup.setWidget(popupContent);

    popupContent.asWidget().addDomHandler(new MouseOverHandler() {
      @Override
      public void onMouseOver(final MouseOverEvent event) {
        timer.cancel();
      }
    }, MouseOverEvent.getType());

    popupContent.asWidget().addDomHandler(new MouseOutHandler() {
      @Override
      public void onMouseOut(final MouseOutEvent event) {
        hide();
      }
    }, MouseOutEvent.getType());

    divTable.addAttachHandler(new Handler() {
      @Override
      public void onAttachOrDetach(final AttachEvent event) {
        if (!event.isAttached()) {
          hide();
        }
      }
    });

    divTable.addRowDecorator(this);
  }

  @Override
  public void applyRowOptions(final R row, final T item) {
    row.asWidget().addDomHandler(new MouseOverHandler() {
      @Override
      public void onMouseOver(final MouseOverEvent event) {
        show(row.asWidget(), item);
      }
    }, MouseOverEvent.getType());
    row.asWidget().addDomHandler(new MouseOutHandler() {
      @Override
      public void onMouseOut(final MouseOutEvent event) {
        hide();
      }
    }, MouseOutEvent.getType());
  }

  private void hide() {
    showTimer.cancel();
    timer.schedule(HIDE_DELAY);
  }

  private void show(final Widget row, final T item) {
    showTimer.reset(row, item);
    timer.cancel();
  }

  @Override
  public DT asDataTable() {
    return divTable;
  }

  @Override
  public Widget asWidget() {
    return divTable;
  }

  /**
   * Wrap the given DivTable with a contextually aware popup which will display context for the selected row.
   *
   * @param divTable Table to wrap.
   * @param popupContent Content to show context for.
   *
   * @return The ContextDivTable
   */
  public static <DT extends DivTable<T, R>, T, R extends DivTableRow> ContextDivTable<DT, T, R> wrap(final DT divTable, final ContextualContent<T> popupContent) {
    return new ContextDivTable<DT, T, R>(divTable, popupContent);
  }
}
