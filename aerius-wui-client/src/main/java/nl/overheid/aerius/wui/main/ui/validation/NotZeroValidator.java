/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.validation;

/**
 * Validator to check numbers for value may not be null or 0.
 *
 * @param <N> Number class to check
 */
public abstract class NotZeroValidator<N extends Number> implements Validator<N> {

  @Override
  public String validate(final N value) {
    return value == null || value.intValue() == 0 ? getMessage(value) : null;
  }

  /**
   * Returns the localized error message for this object when null or empty.
   *
   * @param value current value
   * @return localized message
   */
  protected abstract String getMessage(N value);
}
