/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.validation;

import com.google.gwt.user.client.ui.Widget;

/**
 * Widgets that can have error validation and need to show a popup but where the widget itself doesn't contain the actual
 * input widget containing the value. A input widget is a widget that will get the focus, hence the {@link #getFocusWidget()} method.
 */
public interface IsWidgetErrorStyle {

  /**
   * Returns the widget that should get the focus when an error is shown for this widget.
   * @return input widget
   */
  Widget getFocusWidget();
}
