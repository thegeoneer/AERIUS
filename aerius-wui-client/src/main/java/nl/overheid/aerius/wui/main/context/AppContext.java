/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.context;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.UserContext;

/**
 * Data object containing Application Context objects.
 */
public class AppContext<C extends Context, UC extends UserContext> {
  protected final EventBus eventBus;
  protected final PlaceController placeController;
  protected final C context;
  protected final UC userContext;

  /**
   *
   * @param eventBus
   * @param placeController
   * @param userContext
   * @param context
   */
  public AppContext(final EventBus eventBus, final PlaceController placeController, final C context, final UC userContext) {
    this.eventBus = eventBus;
    this.placeController = placeController;
    this.context = context;
    this.userContext = userContext;
  }

  /**
   * Returns the Context object.
   * @return Context object
   */
  public C getContext() {
    return context;
  }

  /**
   * Returns the UserContext object.
   * @return UserContext object
   */
  public UC getUserContext() {
    return userContext;
  }

  /**
   * @return the eventBus
   */
  public EventBus getEventBus() {
    return eventBus;
  }

  /**
   * @return the placeController
   */
  public PlaceController getPlaceController() {
    return placeController;
  }

  public void goToDefaultPlace() {
    placeController.goTo(Place.NOWHERE);
  }
}
