/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ProvidesResize;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.Arrow.Orientation;

/**
 * Popup that can be 'attached' to another widget. So it will be shown relative
 * to that widget. Part of this popup is to display a triangle (arrow) shape
 * that is positioned between the popup and the widget the popup is attached to.
 */
public class AttachedPopupPanel extends PopupPanel implements ProvidesResize, ResizeHandler {
  private static final int MIN_LEFT_OFFSET = 12;
  private final Arrow arr;
  private Widget widget;
  private boolean hideAttachment;

  /**
   * Default constructor.
   */
  public AttachedPopupPanel() {
    this(false);
  }

  /**
   * @param autoClose auto-close boolean
   */
  public AttachedPopupPanel(final boolean autoClose) {
    super(autoClose);

    setAnimationEnabled(true);
    setAnimationType(AnimationType.ROLL_DOWN);

    // TODO Make dynamic
    arr = new Arrow(Orientation.TOP, 1);
    arr.setHeight(R.css().arrowHeight());
    arr.setWidth(R.css().arrowWidth());
    arr.setColor(R.css().colorPopupTitleBackground(), R.css().colorBorder());
    getContainerElement().appendChild(arr.getElement());

    addStyleName(R.css().attachedPopupPanel());
  }

  /**
   * Attaches this popup to the given widget and repositions the arrow to be
   * placed in the middle of the given widget. This doesn't position the
   * popup itself, that should be done prior to calling this method, because the
   * arrow is repositioned relative to the position of the popup.
   *
   * @param w Widget to attach to
   */
  public void attachToWidget(final Widget w) {
    this.widget = w;
    updateArrowPosition();
  }

  @Override
  public void show() {
    clearResize();
    super.show();
  }

  @Override
  public void onLoad() {
    updateArrowPosition();
  }

  /**
   * Set the border and inner color on the arrow. Because of the complex CSS
   * required to create the arrow it's more convenient to set it programmatically
   * than it is to set it in CSS.
   *
   * @param borderColor Color of the border of the arrow
   * @param backgroundColor Color of the background of the arrow
   */
  public void setArrowColor(final String borderColor, final String backgroundColor) {
    arr.setBorderColor(backgroundColor);
    arr.setBackgroundColor(backgroundColor);
  }

  /**
   * Set the popup position in such a way it will always try to be on-screen. Align the popup south of the widget
   * by default, if there is no space there, align it north. If there's no space there; tough luck.
   * 
   * @param widget Widget to align to.
   */
  public void setSmartPopupPosition(final Widget w) {
    setPopupPosition2(w);

    if (Window.getClientHeight() < getAbsoluteTop() + getOffsetHeight()) {
      final int wd = (w.getOffsetWidth() - getOffsetWidth()) / 2;

      super.setPopupPosition(Math.max(MIN_LEFT_OFFSET, w.getAbsoluteLeft() + wd), w.getAbsoluteTop() - getOffsetHeight() - arr.getHeight());

      // TODO Arrow should be stuck onto the bottom, which is a hassle and a relatively major change while other things have priority right now for something so trivial; hide it instead.
      hideAttachment = true;
    }
  }

  public void setPopupPosition2(final Widget w) {
    hideAttachment = false;
    final int wd = (w.getOffsetWidth() - getOffsetWidth()) / 2;

    super.setPopupPosition(
        Math.max(MIN_LEFT_OFFSET, w.getAbsoluteLeft() + wd), w.getAbsoluteTop() + w.getOffsetHeight() + (arr == null ? 0 : arr.getHeight()));
  }

  public void setPopupPosition2(final int left, final int top) {
    hideAttachment = false;
    super.setPopupPosition(left, top + (arr == null ? 0 : arr.getHeight()));
  }

  @Override
  public void onResize(final ResizeEvent event) {
    updateArrowPosition();
    if (getWidget() instanceof RequiresResize) {
      ((RequiresResize) getWidget()).onResize();
    }
  }

  protected Arrow getArrow() {
    return arr;
  }

  private void updateArrowPosition() {
    if (widget != null && widget.isAttached()) {
      arr.setVisible(!hideAttachment);
      arr.setPosition(widget.getAbsoluteLeft() - getAbsoluteLeft() + widget.getOffsetWidth() / 2 - R.css().arrowWidth());
    }
  }

  public void hideAttachment() {
    arr.setVisible(false);
  }

  public void reattach() {
    attachToWidget(widget);
  }

  public void setResizeHeight(final double newMaxHeight) {
    getElement().getStyle().setProperty("maxHeight", Math.max(0, newMaxHeight), Unit.PX);
  }

  public void clearResize() {
    getElement().getStyle().clearProperty("maxHeight");
  }
}
