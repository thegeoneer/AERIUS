/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.text.ParseException;

import com.google.gwt.dom.client.Document;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.text.shared.Parser;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.uibinder.client.UiConstructor;

/**
 * {@link NumberValueBox} for {@link Double} values. Unlike IntegerValueBox this
 * value box only accepts numeric values without dots or comma's. Additional
 * this value box doesn't even accept invalid character input.
 */
public class IntValueBox extends NumberValueBox<Integer> {

  private static final Renderer<Integer> INT_RENDERER = new AbstractRenderer<Integer>() {
    @Override
    public String render(final Integer object) {
      return object == null ? "" : object.toString();
    }
  };

  private static final Parser<Integer> INT_PARSER = new Parser<Integer>() {
    @Override
    public Integer parse(final CharSequence object) throws ParseException {
      if (object == null || object.toString().isEmpty()) {
        return 0;
      }
      try {
        return Integer.valueOf(object.toString());
      } catch (final NumberFormatException e) {
        throw new ParseException((String) object, 0);
      }
    }
  };

  @UiConstructor
  public IntValueBox(final String label) {
    super(Document.get().createTextInputElement(), INT_RENDERER, INT_PARSER, label);
    setFraction(false);
  }

  @Override
  public Integer getValue() {
    final Integer value = super.getValue();

    return value == null ? 0 : value;
  }

  @Override
  public Integer getValueOrThrow() throws ParseException {
    final Integer value = super.getValueOrThrow();

    return value == null ? 0 : value;
  }

  @Override
  protected Integer getValueOnInvalidInput() {
    return 0;
  }
}
