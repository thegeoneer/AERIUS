/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.CheckBox;

import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public abstract class CheckBoxColumn<O> extends WidgetFactory<O, CheckBox> {
  @Override
  public CheckBox createWidget(final O object) {
    final CheckBox checkBox = new CheckBox();

    checkBox.setValue(getValue(object));
    checkBox.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Boolean> event) {
        setValue(object, event.getValue());
      }
    });

    return checkBox;
  }

  @Override
  public void ensureDebugId(final String debugId) {
    // Turn this into a no-op -- setting a double ID on CheckBox destroys it, so pre-emptively disable it.
  }

  protected abstract Boolean getValue(O outer);

  protected abstract void setValue(O object, Boolean value);
}
