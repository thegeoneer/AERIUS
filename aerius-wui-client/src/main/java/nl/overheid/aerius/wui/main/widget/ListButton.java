/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;

/**
 * Button Widget with a drop down list. *
 * It is encouraged but not required to use the SelectionChange event for this
 * Widget. By default, when an added custom widget is clicked, a SelectionEvent
 * containing the object that has been associated with it when it was added will
 * be fired.
 */
public class ListButton extends FocusWidget implements HasHTML, HasSelectionHandlers<Integer> {
  private static class SpanComplexPanel extends ComplexPanel {
    public SpanComplexPanel() {
      setElement(DOM.createSpan());
    }

    @Override
    public void add(final Widget child) {
      add(child, getElement());
    }
  }

  private final InlineHTML buttonContent;
  private final FlowPanel list = new FlowPanel() {
    final Map<Widget, HandlerRegistration> handlers = new HashMap<Widget, HandlerRegistration>();

    @Override
    protected void add(final Widget child, final Element e) {
      super.add(child, e);
      child.addStyleName(R.css().listButtonItem());
      handlers.put(child, child.addDomHandler(clickHandler, ClickEvent.getType()));
    }
    @Override
    protected void insert(final Widget child, final Element container, final int beforeIndex, final boolean domInsert) {
      super.insert(child, container, beforeIndex, domInsert);
      child.addStyleName(R.css().listButtonItem());
      handlers.put(child, child.addDomHandler(clickHandler, ClickEvent.getType()));
    }

    @Override
    public boolean remove(final Widget w) {
      if (handlers.containsKey(w)) {
        handlers.get(w).removeHandler();
      }
      return super.remove(w);
    }
  };
  private final PopupPanel pp = new PopupPanel(true);

  private final ClickHandler clickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      final Widget widget = (Widget) event.getSource();
      final int widgetIndex = list.getWidgetIndex((Widget) event.getSource());
      setButtonText(widget);
      pp.hide();
      SelectionEvent.fire(ListButton.this, widgetIndex);
    }
  };
  private final SpanComplexPanel button;

  public ListButton() {
    this("");
  }

  public ListButton(final String html) {
    buttonContent = new InlineHTML(html);

    button = new SpanComplexPanel();
    button.add(buttonContent);
    setElement(button.getElement());
    sinkEvents(Event.KEYEVENTS);
    setStyleName(R.css().listButton());
    pp.setAnimationEnabled(true);
    pp.setAnimationType(PopupPanel.AnimationType.ROLL_DOWN);
    pp.setStyleName(R.css().listButtonPopup());
    pp.add(list);
    pp.addAutoHidePartner(button.getElement());
    addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        toggle();
      }
    });
  }

  /**
   * @return the buttonContent
   */
  public InlineHTML getButtonContent() {
    return buttonContent;
  }

  @Override
  public HandlerRegistration addSelectionHandler(final SelectionHandler<Integer> handler) {
    return addHandler(handler, SelectionEvent.getType());
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    if (!enabled) {
      hide();
    }
    button.setStyleName(R.css().listButtonDisabled(), !enabled);
  }

  public FlowPanel getListPanel() {
    return list;
  }

  @Override
  public void onBrowserEvent(final Event event) {
    if (DOM.eventGetType(event) == Event.ONKEYUP && (DOM.eventGetKeyCode(event) == KeyCodes.KEY_DOWN
        || DOM.eventGetKeyCode(event) == KeyCodes.KEY_ENTER || DOM.eventGetKeyCode(event) == ' ')) {
      toggle();
      if (list.getWidget(0) instanceof Focusable) {
        ((Focusable) list.getWidget(0)).setFocus(true);
      }
    }
    super.onBrowserEvent(event);
  }

  public void addPopupStyle(final String style) {
    pp.addStyleName(style);
  }

  public void show() {
    pp.showRelativeTo(ListButton.this);
  }

  /**
   * Hides the list.
   */
  public void hide() {
    pp.hide();
  }

  public void toggle() {
    if (pp.isShowing() || !ListButton.this.isEnabled()) {
      hide();
    } else {
      show();
    }
  }

  public PopupPanel getPopupPanel() {
    return pp;
  }

  @Override
  public String getText() {
    return buttonContent.getText();
  }

  @Override
  public void setText(final String text) {
    buttonContent.setText(text);
  }

  @Override
  public String getHTML() {
    return buttonContent.getHTML();
  }

  @Override
  public void setHTML(final String html) {
    buttonContent.setHTML(html);
  }

  /**
   * Sets the text of the button representing the current selected item.
   * @param selectedWidget widget selected
   */
  protected void setButtonText(final Widget selectedWidget) {
    if (selectedWidget instanceof HasText) {
      setText(((HasText) selectedWidget).getText());
    }
  }
}
