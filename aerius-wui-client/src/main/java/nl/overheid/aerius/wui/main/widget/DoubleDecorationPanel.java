/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * <p>Comparable to a normal SimplePanel, except with an added 2 pixel padding
 * on the bottom that easily allows for a double underline.</p>
 * 
 * <p>It's advised not to change the padding-bottom style property of this widget.</p>
 */
public class DoubleDecorationPanel extends SimplePanel {
  /**
   * Padding used between double decoration.
   */
  public static final int UNDERLINE_PADDING = 2;

  /**
   * Default constructor.
   */
  public DoubleDecorationPanel() {
    getElement().getStyle().setPaddingBottom(UNDERLINE_PADDING, Unit.PX);
  }

  /**
   * Set the text decoration of the top (inner) element.
   * 
   * @param style the Style to apply
   */
  public void addTopStyle(final String style) {
    getWidget().addStyleName(style);
  }

  /**
   * Remove the text decoration of the top (inner) element.
   * 
   * @param style the Style to remove
   */
  public void removeTopStyle(final String style) {
    getWidget().removeStyleName(style);
  }

  /**
   * Set the text decoration of the bottom (outer) element.
   * 
   * @param style the Style to apply
   */
  public void addBottomStyle(final String style) {
    addStyleName(style);
  }

  /**
   * Remove the text decoration of the bottom (outer) element.
   * 
   * @param style the Style to remove
   */
  public void removeBottomStyle(final String style) {
    removeStyleName(style);
  }
}
