/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @param <T>
 */
public class SimpleProgressStep<T> extends Composite {
  interface SimpleProgressStepUiBinder extends UiBinder<Widget, SimpleProgressStep> {}

  private static final SimpleProgressStepUiBinder UI_BINDER = GWT.create(SimpleProgressStepUiBinder.class);

  public interface CustomStyle extends CssResource {
    String active();

    String complete();
  }

  @UiField SimplePanel icon;

  @UiField Label titleLabel;

  @UiField CustomStyle style;

  private final T object;

  public SimpleProgressStep(final T object, final String title) {
    this.object = object;
    initWidget(UI_BINDER.createAndBindUi(this));

    titleLabel.setText(title);
  }

  public boolean isActive(final T object) {
    return this.object.equals(object);
  }

  /**
   * Current active step.
   */
  public void setActive() {
    setState(true, false);
  }

  /**
   * Step has been done.
   */
  public void setStepDone() {
    setState(false, true);
  }

  /**
   * Step is not yet done.
   */
  public void setStepNotYetDone() {
    setState(false, false);
  }

  private void setState(final boolean active, final boolean done) {
    titleLabel.setStyleName(style.active(), active);
    icon.setStyleName(style.active(), active);
    icon.setStyleName(style.complete(), done);
  }
}
