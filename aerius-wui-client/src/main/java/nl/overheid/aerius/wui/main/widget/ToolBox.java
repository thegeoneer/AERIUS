/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.Messages;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.widget.event.AddEvent;
import nl.overheid.aerius.wui.main.widget.event.CopyEvent;
import nl.overheid.aerius.wui.main.widget.event.DeleteEvent;
import nl.overheid.aerius.wui.main.widget.event.EditEvent;

/**
 * Styled toolbox containing tools to edit, delete, copy and add 'things'.
 *
 * A given handler will define a more concrete implementation.
 */
public class ToolBox<T> extends Composite {
  private static ToolBoxUiBinder uiBinder = GWT.create(ToolBoxUiBinder.class);

  public interface ToolBoxHandler<S> {
    S getSelectedObject();

    Class<S> getObjectClass();

    void handleCustomButton();
  }

  public interface ToolBoxMessages extends Messages {
    String add();

    HelpInfo editHelp();

    HelpInfo copyHelp();

    HelpInfo deleteHelp();

    HelpInfo addHelp();

    String customButtonText();

    HelpInfo customButtonHelp();
  }

  interface ToolBoxUiBinder extends UiBinder<Widget, ToolBox<?>> {}

  @UiField Button copy;
  @UiField Button delete;
  @UiField Button edit;
  @UiField Button customButton;
  @UiField Button add;

  /**
   * Boolean to keep track if copy button should be enabled/disabled at all times.
   * Default is true
   */
  private EventBus eventBus;
  private final ToolBoxHandler<T> handler;

  public ToolBox(final ToolBoxHandler<T> activeValue, final HelpPopupController hpC, final ToolBoxMessages messages) {
    this.handler = activeValue;
    initWidget(uiBinder.createAndBindUi(this));

    // Localize
    add.setText(messages.add());

    hpC.addWidget(edit, messages.editHelp());
    hpC.addWidget(delete, messages.deleteHelp());
    hpC.addWidget(copy, messages.copyHelp());
    hpC.addWidget(customButton, messages.customButtonHelp());
    hpC.addWidget(add, messages.addHelp());

    if (messages.customButtonText() == null) {
      customButton.removeFromParent();
    } else {
      customButton.setText(messages.customButtonText());
    }
  }

  /**
   * Enables or disables the edit/delete/copy buttons.
   *
   * @param enable true to enable, false to disable
   */
  public void setButtonsEnabled(final boolean enable) {
    edit.setEnabled(enable);
    delete.setEnabled(enable);
    copy.setEnabled(enable);
  }

  /**
   * Enables or disables the button for addition.
   *
   * @param enable true to enable, false to disable
   */
  public void setAdditionEnabled(final boolean enable) {
    add.setEnabled(enable);
  }

  public void setEventBus(final EventBus eventBus) {
    this.eventBus = eventBus;
  }

  /**
   * Enables or disables the button for copy.
   *
   * @param enable true to enable, false to disable
   */
  public void setCopyEnabled(final boolean enable) {
    copy.setEnabled(enable);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    copy.ensureDebugId(baseID + "-" + TestID.BUTTON_COPY);
    delete.ensureDebugId(baseID + "-" + TestID.BUTTON_DELETE);
    edit.ensureDebugId(baseID + "-" + TestID.BUTTON_EDIT);
    add.ensureDebugId(baseID + "-" + TestID.BUTTON_ADD);
    customButton.ensureDebugId(baseID + "-" + TestID.BUTTON_TOOLBOX_CUSTOM);
  }

  @UiHandler("edit")
  void onEditClick(final ClickEvent ev) {
    if (eventBus != null) {
      eventBus.fireEvent(new EditEvent<T>(handler.getSelectedObject()));
    }
  }

  @UiHandler("copy")
  void onCopyClick(final ClickEvent ev) {
    if (eventBus != null) {
      eventBus.fireEvent(new CopyEvent<T>(handler.getSelectedObject()));
    }
  }

  @UiHandler("delete")
  void onDeleteClick(final ClickEvent ev) {
    if (eventBus != null) {
      eventBus.fireEvent(new DeleteEvent<T>(handler.getSelectedObject()));
    }
  }

  @UiHandler("add")
  void onAddClick(final ClickEvent ev) {
    if (eventBus != null) {
      eventBus.fireEvent(new AddEvent<T>(handler.getObjectClass()));
    }
  }

  @UiHandler("customButton")
  void onCustomButtonClick(final ClickEvent ev) {
    handler.handleCustomButton();
  }
}
