/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.StyleUtil;

public class ColorPicker extends SelectListBox<String> {
  private static final String[] COLORS = new String[] { "AC725E", "D06B64", "F83A22", "FA573C", "FF7537", "FFAD46", "42D692", "16A765", "7BD148"
      , "B3DC6C", "FBE983", "FAD165", "92E1C0", "9FE1E7", "9FC6E7", "4986E7", "9A9CFF", "B99AFF", "8F8F8F", "CABDBF"
      , "CCA6AC", "F691B2", "CD74E6", "A47AE2" };

  private static final String DEFAULT_COLOR = COLORS[0];

  interface ColorPickerUiBinder extends UiBinder<Widget, ColorPicker> {}

  private static final ColorPickerUiBinder UI_BINDER = GWT.create(ColorPickerUiBinder.class);

  public ColorPicker() {
    super(new WidgetFactory<String, SelectOption<String>>() {
      @Override
      public SelectOption<String> createWidget(final String color) {
        return new SelectColorOption(color);
      }
    });

    setPopupStyle(StyleUtil.joinStyles(R.css().flex(), R.css().wrap(), R.css().colorPicker(), R.css().spaceBetween()));

    setValues(COLORS);
  }

  @Override
  protected void init() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  protected void updateHeaderText() {
    final String headerText = getHeaderText(getValue());

    labelField.setTitle(headerText);
    labelField.getElement().getStyle().setBackgroundColor(ColorUtil.webColor(headerText));
  }

  @Override
  public String getValue() {
    final String value = super.getValue();

    if (value == null) {
      return DEFAULT_COLOR;
    }

    return value;
  }
}
