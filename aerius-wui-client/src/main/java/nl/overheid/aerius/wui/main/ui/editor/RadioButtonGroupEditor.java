/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.util.ArrayList;

import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.RadioButton;

/**
 * Class to manage a group of radio buttons.
 * The class keeps track of the different buttons
 * @param <T> data type for the radio buttons
 */
public abstract class RadioButtonGroupEditor<T> implements LeafValueEditor<T> {

  private final String groupName;

  private final ArrayList<RadioButton> radioButtons = new ArrayList<RadioButton>();

  /**
   * Convenience class for grouping radiobuttons.
   * @param groupName The name the radiobuttons should use for the group.
   */
  public RadioButtonGroupEditor(final String groupName) {
    this.groupName = groupName;
  }

  /**
   * Adds a existing radio button to the group for the value given.
   * @param radioButton radio button
   * @param value value for this radio button
   * @return the radio button
   */
  public RadioButton addRadioButton(final RadioButton radioButton, final T value) {
    assert groupName.equals(radioButton.getName()) : "Name of the radiobutton should be same as class is initialzed with";
    radioButton.setFormValue(value2String(value));
    radioButtons.add(radioButton);
    return radioButton;
  }

  /**
   * Don't use radioButton.setName for the button as it will mess up the group.
   * @param label The label to use for the radio button.
   * @param value The value that should be returned when the radiobutton is selected.
   * @return The radiobutton that is created.
   */
  public RadioButton createRadioButton(final String label, final T value) {
    final RadioButton radioButton = new RadioButton(groupName, label);
    radioButton.setFormValue(value2String(value));
    radioButtons.add(radioButton);
    return radioButton;
  }

  /**
   * Ensure all radiobuttons in the group are unselected (not checked).
   */
  public void resetSelection() {
    for (final RadioButton radioButton : radioButtons) {
      radioButton.setValue(false);
    }
  }

  /**
   * Ensure all radiobuttons in the group are enabled or not.
   * @param enabled set all buttons enabled.
   */
  public void setEnabled(final boolean enabled) {
    for (final RadioButton radioButton : radioButtons) {
      radioButton.setEnabled(enabled);
    }
  }

  /**
   * @return The value that is currently selected, null if nothing is selected within the group.
   */
  @Override
  public T getValue() {
    String selectedValue = null;
    for (final RadioButton radioButton : radioButtons) {
      //should only be one selected value for the group.
      if (radioButton.getValue()) {
        selectedValue = radioButton.getFormValue();
        break;
      }
    }
    return string2Value(selectedValue);
  }

  /**
   * Converts the form value, which is a String, to the data type of the group.
   * @param value value to convert
   * @return value as data type
   */
  protected abstract T string2Value(String value);

  /**
   * Select the radio button which form value matches the given value.
   * @param value form value of the radio button to set
   */
  @Override
  public void setValue(final T value) {
    final String stringValue = value2String(value);
    for (final RadioButton radioButton : radioButtons) {
      if (radioButton.getFormValue().equals(stringValue)) {
        radioButton.setValue(true);
        break;
      }
    }
  }

  /**
   * Converts a data value to a string form value.
   * @param value data value to convert
   * @return String as used in form value.
   */
  protected abstract String value2String(final T value);

  /**
   * Add a valuechangehandler to all current created radiobuttons.
   * Won't be added to new created ones.
   * @param changeHandler The changehandler to add for all radiobuttons.
   */
  public void addValueChangeHandler(final ValueChangeHandler<Boolean> changeHandler) {
    for (final RadioButton button : radioButtons) {
      button.addValueChangeHandler(changeHandler);
    }
  }
}
