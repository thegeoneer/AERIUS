/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.systeminfo;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.service.SystemInfoServiceAsync;
import nl.overheid.aerius.wui.main.event.SystemInfoMessageEvent;
import nl.overheid.aerius.wui.main.retrievers.PollingAgentImpl;

/**
 * Scenario Polling Agent.
 */
@Singleton
public class SystemInfoPollingAgent extends PollingAgentImpl<String, String> {
  private static final int POLL_WAIT_TIME = 2 * 60 * 1000;

  private final EventBus eventBus;
  private final SystemInfoServiceAsync service;

  private String lastResult;

  @Inject
  public SystemInfoPollingAgent(final EventBus eventBus, final SystemInfoServiceAsync service, final Context context) {
    super(Math.max((int) context.getSetting(SharedConstantsEnum.SYSTEM_INFO_POLLING_TIME), POLL_WAIT_TIME));
    this.service = service;
    this.eventBus = eventBus;
  }

  @Override
  protected void callService(final String key, final AsyncCallback<String> resultCallback) {
    service.getSystemInfoMessage(resultCallback);
  }

  public void start() {
    start(null, new AsyncCallback<String>() {
      @Override
      public void onSuccess(final String result) {
        if (result == null || result.equals(lastResult)) {
          return;
        }

        eventBus.fireEvent(new SystemInfoMessageEvent(result));
        lastResult = result;
      }

      @Override
      public void onFailure(final Throwable caught) {
        new Timer() {
          @Override
          public void run() {
            // Restart.
            start();
          }
        }.schedule(POLL_WAIT_TIME);
      }
    });
  }
}
