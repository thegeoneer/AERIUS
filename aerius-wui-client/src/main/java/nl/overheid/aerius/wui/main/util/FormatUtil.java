/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;

import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.deposition.PasValueUtil;
import nl.overheid.aerius.shared.domain.result.CriticalLevels;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 * Utility class with helpers for formatting messages.
 */
public final class FormatUtil {
  private static final int MINIMUM_THRESHOLD = 100;
  private static final int METERS_IN_KILOMETER = 1000;
  private static final int KILO_IN_TON = 1000;
  private static final int M2_TO_HA = 100 * 100;
  private static final double BYTES_IN_KILOBYTE = 1024; // Double because this will be used for division and needs the precision
  private static final int MAX_DECIMALS = 5;
  private static final int CONCENTRATION_VALUE_FIXED_LENGTH = 1;
  private static final int DEPOSITION_VALUE_FIXED_LENGTH = 1;
  private static final int AVERAGE_DEVELOPMENTSPACE_THRESHOLD = 1;

  /**
   * Value threshold below which values will be rounded more precisely.
   */
  private static final int DEPOSITION_PRECISE_ROUNDING_LENGTH = 2;
  private static final int EMISSION_VALUE_FIXED_LENGTH = 1;
  private static final int EMISSION_VALUE_ONE_PRECISE = 1;
  private static final int PERCENTAGE_VALUE_FIXED_LENGTH = 1;
  private static final int DISTANCE_VALUE_FIXED_LENGTH = 1;
  private static final int SURFACE_VALUE_FIXED_LENGTH = 1;
  private static final int SURFACE_VALUE_PRECISE_LENGTH = 1;
  private static final int DESIRE_VALUE_FIXED_LENGTH = 2;
  private static final String UNSUPPORTED_EMISSION_RESULT_TYPE = "Unsupported EmissionResultType";

  private static final DateTimeFormat DEFAULT_DATE_TIME_FORMATTER = DateTimeFormat.getFormat(M.messages().notificationDateTimeFormat());
  private static final DateTimeFormat DEFAULT_DATE_FORMATTER = DateTimeFormat.getFormat(M.messages().notificationDateFormat());
  private static final DateTimeFormat DEFAULT_COMPACT_DATE_FORMATTER = DateTimeFormat.getFormat(M.messages().dateFormat());
  private static final DateTimeFormat DEFAULT_COMPACT_DATETIME_FORMATTER = DateTimeFormat.getFormat(M.messages().dateTimeFormat());

  /**
   * This value should only be used for monitor.
   */
  private static final EmissionResultValueDisplaySettings DEFAULT_DISPLAY_SETTINGS = new EmissionResultValueDisplaySettings(
      DepositionValueDisplayType.MOLAR_UNITS, 1, DEPOSITION_VALUE_FIXED_LENGTH, DEPOSITION_PRECISE_ROUNDING_LENGTH);

  private FormatUtil() {}

  /* Decimal numbers */

  /**
   * Returns the value formatted as a string with the correct localized decimal separator and a fixed number of decimal places.<br>
   * Used for presenting doubles in an aligned fashion, e.g. in tables.
   *
   * @param value The value to format using a fixed number of decimal places
   * @param length The exact amount of decimal places used
   * @return The value formatted as string
   */
  public static String toFixed(final double value, final int length) {
    return M.messages().decimalNumberFixed(PasValueUtil.pasProoftoFixed(value, length), length);
  }

  /**
   * Format a decimal value as an integer number. Same as calling {@link #toFixed} with length 0.
   *
   * @param value The double value to format as integer
   * @return String where the decimal value is rounded to an integer
   */
  public static String toWhole(final double value) {
    return toFixed(value, 0);
  }

  /**
   * Format a decimal value as an integer number. But show < 1 below Threshold.
   *
   * @param value The double value to format as integer
   * @return String where the decimal value is rounded to an integer
   */
  public static String toWholeThreshold(final double value) {
    return value < AVERAGE_DEVELOPMENTSPACE_THRESHOLD ? M.messages().prioritySubProjectDevelopmentSpaceMinimum() : toWhole(value);
  }

  /**
   * Returns the value formatted as a string with the correct localized decimal separator and a maximum number of decimal places.<br>
   * There will be at most the given number of decimal places, but trailing zeros are removed.<br>
   * <br>
   * E.g. 3.100 => 3.1, 3.0 => 3, also 3.10001 with length=2 => 3.1<br>
   * <br>
   * Used for presenting doubles in a stand-alone fashion (no need to align), e.g. tooltips.
   *
   * @param value The value to format using a maximum number of decimal places
   * @param length The maximum amount of decimal places used
   * @return The value formatted as string
   */
  public static String formatDecimalCapped(final double value, final int length) {
    return M.messages().decimalNumberCapped(value, length);
  }

  /**
   * Returns the value formatted as a string with the correct localized decimal separator and a maximum number of {@value #MAX_DECIMALS} decimal
   * places.<br>
   * There will be at most {@value #MAX_DECIMALS} decimal places, but trailing zeros are removed.
   *
   * @see #formatDecimalCapped(double, int)
   */
  public static String formatDecimalCapped(final double value) {
    return M.messages().decimalNumberCapped(value, MAX_DECIMALS);
  }

  /* Emission */

  public static String formatEmission(final double em, final int precision) {
    return toFixed(em, precision);
  }

  public static String formatEmission(final double em) {
    return formatEmission(em, EMISSION_VALUE_FIXED_LENGTH);
  }

  public static String formatEmissionOnePrecise(final double em) { // NO_UCD (use private)
    return formatEmission(em, EMISSION_VALUE_ONE_PRECISE);
  }

  public static String formatEmissionWithUnit(final double em) {
    return M.messages().unitKgY(formatEmission(em));
  }

  public static String formatEmissionWithUnit(final double em, final int precision) {
    return M.messages().unitKgY(formatEmission(em, precision));
  }

  public static String formatEmissionWithUnitSmart(final double em) {
    final String withUnit;
    if ((Math.abs(em) < MINIMUM_THRESHOLD) && (em != 0)) {
      if (em < 0) {
        withUnit = M.messages().unitMinimumNegativeMTY();
      } else {
        withUnit = M.messages().unitMinimumMTY();
      }
    } else {
      withUnit = M.messages().unitMTY(formatEmissionToTon(em));
    }
    return withUnit;
  }

  public static String formatDoubleEmissionSmart(final double nh3Total, final double noxTotal) {
    return FormatUtil.formatEmissionWithUnitSmart(nh3Total) + " + " + FormatUtil.formatEmissionWithUnitSmart(noxTotal);
  }

  private static String formatEmissionToTon(final double em) {
    return formatEmissionOnePrecise(em / KILO_IN_TON);
  }

  /* Concentration */

  public static String formatConcentration(final double concentration) {
    return formatConcentration(concentration, CONCENTRATION_VALUE_FIXED_LENGTH);
  }

  public static String formatConcentration(final double concentration, final int precision) {
    return toFixed(concentration, precision);
  }

  public static String formatConcentrationWithUnit(final double concentration) {
    return formatConcentrationWithUnit(concentration, CONCENTRATION_VALUE_FIXED_LENGTH);
  }

  public static String formatConcentrationWithUnit(final double concentration, final int precision) {
    return M.messages().unitUgM3(formatConcentration(concentration, precision));
  }

  /* Deposition */

  public static String formatDeposition(final double deposition, final EmissionResultValueDisplaySettings settings) {
    return formatDeposition(deposition, settings, settings.getDepositionValueRoundingLength());
  }

  public static String formatDeposition(final double deposition, final EmissionResultValueDisplaySettings settings, final int precision) {
    return toFixed(convertDepositionValue(deposition, settings), precision);
  }

  /**
   * format deposition to 2 digits.
   *
   * @param deposition to be formatted.
   * @return String with formatted values
   */
  public static String formatDepositionPASProof(final double deposition, final EmissionResultValueDisplaySettings settings) {
    // The pasProofValue need not be converted because the PasValueUtil expects molar units, which the value in the deposition parameter is.
    return PasValueUtil.pasProofValue(deposition) + formatDeposition(deposition, settings, settings.getDepositionValuePreciseRoundingLength());
  }

  public static String formatDepositionPASProofWithUnit(final double deposition, final EmissionResultValueDisplaySettings settings) {
    return formatDepositionValueDisplayUnit(formatDepositionPASProof(deposition, settings), settings);
  }

  public static String formatDepositionWithUnit(final double deposition, final EmissionResultValueDisplaySettings settings) {
    return formatDepositionWithUnit(deposition, settings, settings.getDepositionValueRoundingLength());
  }

  public static String formatDepositionWithUnit(final double deposition, final EmissionResultValueDisplaySettings settings, final int precision) {
    return formatDepositionValueDisplayUnit(formatDeposition(deposition, settings, precision), settings);
  }

  private static double convertDepositionValue(final double depositionValue, final EmissionResultValueDisplaySettings settings) {
    return depositionValue * settings.getConversionFactor();
  }

  private static String formatDepositionValueDisplayUnit(final String depositionValue, final EmissionResultValueDisplaySettings settings) {
    return M.messages().unitDeposition(settings.getDisplayType(), depositionValue);
  }

  /* Results */

  public static double convertEmissionResult(final EmissionResultType emissionResultType, final double emissionResult,
      final EmissionResultValueDisplaySettings settings) {
    switch (emissionResultType) {
    case DEPOSITION:
      return convertDepositionValue(emissionResult, settings);
    case CONCENTRATION:
    case NUM_EXCESS_DAY:
    default:
      return emissionResult;
    }
  }

  public static String formatEmissionResult(final EmissionResultType emissionResultType, final double emissionResult,
      final EmissionResultValueDisplaySettings settings) {
    switch (emissionResultType) {
    case CONCENTRATION:
      return formatConcentration(emissionResult);
    case DEPOSITION:
      return formatDepositionPASProof(emissionResult, settings);
    case NUM_EXCESS_DAY:
      return formatNumDays(emissionResult);
    default:
      throw new IllegalArgumentException(UNSUPPORTED_EMISSION_RESULT_TYPE);
    }
  }

  public static String formatEmissionResultWithUnit(final EmissionResultType emissionResultType, final double emissionResult,
      final EmissionResultValueDisplaySettings settings) {
    switch (emissionResultType) {
    case CONCENTRATION:
      return formatConcentrationWithUnit(emissionResult);
    case DEPOSITION:
      return formatDepositionPASProofWithUnit(emissionResult, settings);
    case NUM_EXCESS_DAY:
      return formatNumDaysWithUnit(emissionResult);
    default:
      throw new IllegalArgumentException(UNSUPPORTED_EMISSION_RESULT_TYPE);
    }
  }

  public static String formatEmissionResult(final EmissionResultKey emissionResultKey, final CriticalLevels criticalLevels,
      final EmissionResultValueDisplaySettings displaySettings) {
    final double level = criticalLevels.get(emissionResultKey);

    return level == 0 ? M.messages().unitNoCriticalLevel() : formatEmissionResult(emissionResultKey.getEmissionResultType(), level, displaySettings);
  }

  public static String formatEmissionResultWithUnit(final EmissionResultKey emissionResultKey, final CriticalLevels criticalLevels,
      final EmissionResultValueDisplaySettings displaySettings) {
    final double level = criticalLevels.get(emissionResultKey);

    return level == 0 ? M.messages().unitNoCriticalLevel()
        : formatEmissionResultWithUnit(emissionResultKey.getEmissionResultType(), level, displaySettings);
  }

  public static String formatEmissionResult(final EmissionResultKey emissionResultKey, final EmissionResults criticalLevels,
      final EmissionResultValueDisplaySettings displaySettings) {
    return formatEmissionResult(emissionResultKey.getEmissionResultType(), criticalLevels.get(emissionResultKey), displaySettings);
  }

  public static String formatEmissionResultWithUnit(final EmissionResultKey emissionResultKey, final EmissionResults criticalLevels,
      final EmissionResultValueDisplaySettings displaySettings) {
    return formatEmissionResultWithUnit(emissionResultKey.getEmissionResultType(), criticalLevels.get(emissionResultKey), displaySettings);
  }

  /* Number of days */

  private static String formatNumDays(final double numDays) {
    return toFixed(MathUtil.round(numDays), 0);
  }

  private static String formatNumDaysWithUnit(final double numDays) {
    return M.messages().unitDays(MathUtil.round(numDays));
  }

  /**
   * Speed in km/h
   */
  public static String formatSpeedWithUnit(final int ms) {
    return M.messages().unitKmH(ms);
  }

  /**
   * Format a given distance in meters to kilometers.
   *
   * @param distance Distance in meters.
   * @return String containing distance
   */
  public static String formatDistance(final double distance) {
    return toFixed(distance / METERS_IN_KILOMETER, DISTANCE_VALUE_FIXED_LENGTH);
  }

  /**
   * Format a given distance in meters if < 1000 meter, else to kilometers.
   *
   * @param distance Distance in meters.
   * @return String containing distance
   */
  public static String formatDistanceWithUnit(final double distance) {
    return distance < METERS_IN_KILOMETER ? formatDistanceMetersWithUnit(distance) : M.messages().unitKm(formatDistance(distance));
  }

  /**
   * Format a given distance in meters if < 1000 meter, else to kilometers in whole numbers.
   *
   * @param distance Distance in meters.
   * @return String containing distance
   */
  public static String formatDistanceToWholeWithUnit(final double distance) {
    return distance < METERS_IN_KILOMETER ? formatDistanceMetersWithUnit(distance) : M.messages().unitKm(toWhole(distance / METERS_IN_KILOMETER));
  }

  /**
   * Format a given distance in meters.
   *
   * @param distance Distance in meters.
   * @return String containing distance
   */
  public static String formatDistanceMetersWithUnit(final double distance) {
    return M.messages().unitM(toWhole(distance));
  }

  /* Percentage */

  /**
   * Format the given value as a percentage.
   *
   * @param percentage The percentage
   * @return
   */
  public static String formatPercentage(final double percentage) {
    return toFixed(percentage, PERCENTAGE_VALUE_FIXED_LENGTH);
  }

  private static String formatFactorAsPercentage(final double factor) {
    return toFixed(factor * 100, PERCENTAGE_VALUE_FIXED_LENGTH);
  }

  /**
   * Format the given value as a percentage.
   *
   * @param percentage The percentage
   * @return
   */
  public static String formatPercentageWithUnit(final double percentage) {
    return M.messages().unitPercentage(formatPercentage(percentage));
  }

  public static String formatFactorAsPercentageWithUnit(final double factor) {
    return M.messages().unitPercentage(formatFactorAsPercentage(factor));
  }

  /* Surface */

  /**
   * Format a given surface in m2 to ha without appending unit.
   *
   * @param surface Surface in m2.
   * @return String containing surface in ha
   */
  public static String formatSurface(final double surface) {
    return toFixed(surface / M2_TO_HA, SURFACE_VALUE_FIXED_LENGTH);
  }

  /**
   * Format a given surface in m2 to ha without appending unit.
   *
   * @param surface Surface in m2.
   * @return String containing surface in ha
   */
  public static String formatSurfacePrecise(final double surface) {
    return toFixed(surface / M2_TO_HA, SURFACE_VALUE_PRECISE_LENGTH);
  }

  /**
   * Format a given surface in m2 to ha and appends unit.
   *
   * @param surface Surface in m2.
   * @return String containing surface in ha
   */
  public static String formatSurfaceWithUnit(final double surface) {
    return M.messages().unitSurface(formatSurface(surface));
  }

  /**
   * Format a given surface in m2 to ha and appends unit.
   *
   * @param surface Surface in m2.
   * @return String containing surface in ha
   */
  public static String formatSurfacePreciseWithUnit(final double surface) {
    return M.messages().unitSurface(formatSurfacePrecise(surface));
  }

  /* Byte size */

  /**
   * Format the given number of bytes into an interpretable value.
   *
   * - Anything 0-1024 will be formatted as [# kb]
   * - Anything over or equal to 1024 will be formatted as [0.## mb]
   *
   * @param bytes the amount of bytes
   * @return
   */
  public static String formatByteSizeWithUnit(final long bytes) {
    if (bytes < BYTES_IN_KILOBYTE) {
      return M.messages().unitKB(NumberFormat.getFormat("0.##").format(bytes / BYTES_IN_KILOBYTE));
    } else {
      return M.messages().unitMB(NumberFormat.getFormat("0.##").format(bytes / (BYTES_IN_KILOBYTE * BYTES_IN_KILOBYTE)));
    }
  }

  /* Development space and desire */

  public static String formatDevelopmentSpace(final double totalDesire) {
    return toWhole(totalDesire);
  }

  public static String formatDevelopmentSpacePrecise(final double totalDesire) {
    return toFixed(totalDesire, DESIRE_VALUE_FIXED_LENGTH);
  }

  /* Dates */

  public static DateTimeFormat getDefaultDateTimeFormatter() {
    return DEFAULT_DATE_TIME_FORMATTER;
  }

  public static DateTimeFormat getDefaultDateFormatter() {
    return DEFAULT_DATE_FORMATTER;
  }

  public static DateTimeFormat getDefaultCompactDateFormatter() {
    return DEFAULT_COMPACT_DATE_FORMATTER;
  }

  public static DateTimeFormat getDefaultCompactDateTimeFormatter() {
    return DEFAULT_COMPACT_DATETIME_FORMATTER;
  }

  public static String formatDate(final Date date) {
    return date == null ? M.messages().nullDateDefault() : DEFAULT_DATE_FORMATTER.format(date);
  }

  /**
   * Format the deposition at the default precision with an appropriate unit.
   *
   * @deprecated used for legacy code only. [monitor]
   */
  @Deprecated
  public static String formatDepositionWithUnit(final double deposition) {
    return formatDepositionWithUnit(deposition, DEFAULT_DISPLAY_SETTINGS);
  }

  /**
   * Format the deposition at the given precision with an appropriate unit.
   *
   * @deprecated used for legacy code only. [monitor]
   */
  @Deprecated
  public static String formatDepositionWithUnit(final double deposition, final int precision) {
    return formatDepositionWithUnit(deposition, DEFAULT_DISPLAY_SETTINGS, precision);
  }

  /**
   * Format the deposition at the given precision without a unit.
   *
   * @deprecated used for legacy code only. [monitor]
   */
  @Deprecated
  public static String formatDeposition(final double deposition, final int precision) {
    return formatDeposition(deposition, DEFAULT_DISPLAY_SETTINGS, precision);
  }
}
