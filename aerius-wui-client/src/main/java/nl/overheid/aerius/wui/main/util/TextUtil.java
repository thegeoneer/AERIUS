/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util;

import java.util.Iterator;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;

public final class TextUtil {
  private TextUtil() {}

  public interface TextFactory<T> {
    String getText(T item);
  }

  /* Text utils */

  /**
   * Capitalises the first letter in the given string.
   *
   * @param nocaps The string to capitalise.
   * @return the given argument if the argument is null or empty, or a string with the first character uppercased.
   */
  public static String capitalise(final String nocaps) {
    if ((nocaps == null) || nocaps.isEmpty()) {
      return nocaps;
    }

    return new StringBuilder(nocaps.substring(0, 1).toUpperCase()).append(nocaps.substring(1)).toString();
  }

  public static <T> String joinItems(final Iterator<T> iterator, final TextFactory<T> textMachine, final String seperator) {
    final JsArrayString jsa = JavaScriptObject.createArray().cast();

    while (iterator.hasNext()) {
      jsa.push(textMachine.getText(iterator.next()));
    }

    return jsa.join(seperator);
  }

  /**
   * Join array of strings separated by a space
   * @param strings array of strings to join
   * @return joined string
   */
  public static String joinStrings(final String... strings) {
    return joinStrings(strings, " ");
  }

  /**
   * Join array of strings separated by the separator
   * @param strings array of strings to join
   * @param seperator string to use as separator
   * @return joined string
   */
  public static String joinStrings(final String[] strings, final String seperator) {
    final JsArrayString jsa = JavaScriptObject.createArray().cast();

    for (int i = 0; i < strings.length; i++) {
      jsa.push(strings[i]);
    }

    return jsa.join(seperator);
  }
}
