/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.search;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PopupPanel.AnimationType;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.search.SearchSuggestion;
import nl.overheid.aerius.shared.domain.search.SearchSuggestionType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.GWTAtomicInteger;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.AutoSizePopupPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * The searchPanel.
 */
public class SearchPanel<T extends SearchSuggestionType, S extends SearchSuggestion<T, S>, A extends SearchAction<T, S>> extends Composite {

  interface SearchPanelUiBinder extends UiBinder<Widget, SearchPanel<?, ?, ?>> {}

  private static final SearchPanelUiBinder UI_BINDER = GWT.create(SearchPanelUiBinder.class);

  public interface CustomStyle extends CssResource {
    String searchBoxFocus();
  }

  /** Time (in ms) before autocomplete is called when typing in a letter.
   *  (When quickly typing in a search term we don't need to call the searchservice every time.) */
  private static final int SEARCH_BOX_AUTOCOMPLETE_DELAY = 200;

  // Stop-gap way of animating horizontally. Ugly but it works fine.
  private static final int ANIMATION_DELAY = 5;
  private static final int ANIMATION_STEPS = 50;

  protected final A searchAction;

  private S topSuggestion;
  private boolean searchBoxFocused;
  private final Timer searchDelayTimer;

  @UiField TextBox searchBox;
  @UiField Image searchImage;

  @UiField CustomStyle style;

  final AutoSizePopupPanel suggestionPopup;
  final SearchTable<T, S> searchTable;

  private String placeHolder;

  public SearchPanel(final EventBus eventBus, final HelpPopupController hpC, final SearchTable<T, S> searchTable, final A searchAction) {
    this.searchTable = searchTable;
    this.searchAction = searchAction;
    initWidget(UI_BINDER.createAndBindUi(this));

    suggestionPopup = new AutoSizePopupPanel(true);
    suggestionPopup.setAnimationType(AnimationType.ROLL_DOWN);
    suggestionPopup.setAnimationEnabled(true);
    suggestionPopup.addAutoHidePartner(searchBox.getElement());
    suggestionPopup.setWidget(searchTable);
    suggestionPopup.addCloseHandler(new CloseHandler<PopupPanel>() {
      @Override
      public void onClose(final CloseEvent<PopupPanel> event) {
        searchBox.removeStyleName(style.searchBoxFocus());

        // Another stop-gap way of animating.
        final GWTAtomicInteger at = new GWTAtomicInteger();
        Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
          @Override
          public boolean execute() {
            suggestionPopup.setPopupPosition(getAbsoluteLeft(), getAbsoluteTop() + getOffsetHeight());

            return at.incrementAndGet() < ANIMATION_STEPS;
          }
        }, ANIMATION_DELAY);
      }
    });
    searchDelayTimer = new Timer() {
      @Override
      public void run() {
        startSearch();
      }
    };

    Window.addResizeHandler(new ResizeHandler() {
      @Override
      public void onResize(final ResizeEvent event) {
        if (suggestionPopup.isShowing()) {
          suggestionPopup.showRelativeTo(SearchPanel.this);
        }
      }
    });

    hpC.addWidget(searchBox, hpC.tt().ttSearchBox());
    searchBox.ensureDebugId(TestID.INPUT_SEARCHBOX);
    suggestionPopup.ensureDebugId(TestID.DIV_SEARCHSUGGESTION);
    searchImage.ensureDebugId(TestID.BUTTON_SEARCHIMAGE);
  }

  @UiHandler("searchBox")
  public void onSearchBoxBlur(final BlurEvent event) {
    searchBoxFocused = false;
    updateSearchImage(false);
  }

  @UiHandler("searchBox")
  public void onSearchBoxFocus(final FocusEvent event) {
    searchBoxFocused = true;
    updateSearchImage(false);
    startSearch();
  }

  @UiHandler("searchBox")
  public void onSearchBoxKeyUp(final KeyUpEvent event) {
    if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
      if (topSuggestion == null) {
        startSearch();
      } else {
        searchAction.selectSuggestion(topSuggestion);
      }
    } else {
      searchDelayed();
    }
  }

  public void setPlaceHolder(final String placeHolder) {
    this.placeHolder = placeHolder;

    StyleUtil.I.setPlaceHolder(searchBox, placeHolder);
  }

  @UiHandler("searchImage")
  public void onSearchImageClick(final ClickEvent event) {
    searchBox.setFocus(true);
  }

  private void updateSearchImage(final boolean searching) {
    if (searching) {
      searchImage.setResource(R.images().buttonSearchAnimate());
    } else if (searchBoxFocused) {
      searchImage.setResource(R.images().buttonSearchFocus());
    } else {
      searchImage.setResource(R.images().buttonSearchNormal());
    }
  }

  /**
   * Initiates the search after a short interval. Each call will reset the timer, so if called again
   * within the interval period, any previous calls are essentially dismissed.
   */
  private void searchDelayed() {
    searchDelayTimer.cancel();
    searchDelayTimer.schedule(SEARCH_BOX_AUTOCOMPLETE_DELAY);
  }

  private void hideSuggestionPopup() {
    suggestionPopup.hide();
  }

  private void showSuggestionPopup() {
    searchBox.addStyleName(style.searchBoxFocus());
    suggestionPopup.showRelativeTo(SearchPanel.this);

    final GWTAtomicInteger at = new GWTAtomicInteger();
    Scheduler.get().scheduleFixedDelay(new RepeatingCommand() {
      @Override
      public boolean execute() {
        suggestionPopup.showRelativeTo(SearchPanel.this);

        return at.incrementAndGet() < ANIMATION_STEPS;
      }
    }, ANIMATION_DELAY);
  }

  private void startSearch() {
    final String searchText = searchBox.getText().trim();

    // Make sure to do nothing and that no results are visible so long as nothing meaningful is entered
    if (!searchAction.isSearchable(searchText)) {
      updateSearchImage(false);
      hideSuggestionPopup();
      searchTable.clear();
      topSuggestion = null;
      return;
    }

    // For IE, where placeholder is put as text: If content matches placeholder
    // text just return, and don't start search.
    if (placeHolder != null && placeHolder.equals(searchText)) {
      return;
    }

    // Display suggestion popup
    showSuggestionPopup();

    // Otherwise, begin a new search
    topSuggestion = null;
    updateSearchImage(true);

    searchAction.search(searchText, new AppAsyncCallback<ArrayList<S>>() {
      @Override
      public void onSuccess(final ArrayList<S> suggestions) {
        updateSearchImage(false);

        if (!suggestions.isEmpty()) {
          topSuggestion = suggestions.get(0);
        }

        searchDelayTimer.cancel();

        searchTable.clear();
        searchTable.setSuggestions(suggestions);
      }

      @Override
      public void onFailure(final Throwable caught) {
        updateSearchImage(false);

        super.onFailure(caught);
      }
    });
  }
}
