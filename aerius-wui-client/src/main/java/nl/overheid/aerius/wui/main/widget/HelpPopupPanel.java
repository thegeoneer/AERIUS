/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;

import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.i18n.HelpToolTipInfo;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * The PopupPanel of the Help tool tip.
 */
public class HelpPopupPanel extends AttachedPopupPanel {
  private static final String LINK_TARGET = "aerius_manual";
  private final SimplePanel linkContainer = new SimplePanel();
  private final HTML content = new HTML();
  private final Anchor link = new Anchor();

  private final String linkTemplate;

  public HelpPopupPanel(final String linkTemplate) {
    this.linkTemplate = linkTemplate;

    setAnimationType(AnimationType.ROLL_DOWN);

    link.setText(M.messages().helpPopupLinkText());
    link.setTarget(LINK_TARGET);

    linkContainer.setWidget(link);

    getArrow().setColor(R.css().colorHelpContent(), R.css().colorHelpBorder());
    setStyleName(R.css().helpPopupPanel());
    final FlowPanel container = new FlowPanel();
    container.add(content);
    container.add(linkContainer);
    container.setStyleName(R.css().helpPopupPanelContent());
    setWidget(container);
  }

  /**
   * Sets this popup's link, or null if no link is required.
   *
   * @param href the link
   */
  public void setLink(final String href) {
    if (href == null) {
      linkContainer.setWidget(null);
      link.setHref((String) null);
    } else {
      linkContainer.setWidget(link);
      link.setHref(href);
    }
  }

  public void setHelpInfo(final HelpInfo info) {
    content.setHTML(info.getMessage());
    content.setVisible(info.getMessage() != null);
    linkContainer.setStyleName(R.css().link(), info.getMessage() != null);

    if (info.getKey() != HelpToolTipInfo.NO_LINK) {
      setLink(linkTemplate + info.getKey());
    } else {
      setLink(null);
    }
  }
}
