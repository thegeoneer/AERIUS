/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.text.ParseException;
import java.util.Collection;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.HasBlurHandlers;
import com.google.gwt.event.dom.client.HasFocusHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.text.shared.Parser;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PopupPanel.AnimationType;
import com.google.gwt.user.client.ui.SuggestOracle.Callback;
import com.google.gwt.user.client.ui.SuggestOracle.Request;
import com.google.gwt.user.client.ui.SuggestOracle.Response;
import com.google.gwt.user.client.ui.ValueBox;
import com.google.gwt.user.client.ui.ValueBoxBase;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.HasName;

/**
 * List box with suggestions dynamically loaded and/or displayed based on the user input.
 * @param <E> Data object returned from the suggestion
 */
public class SuggestBox<E> extends Composite implements HasValue<E>, HasFocusHandlers, HasBlurHandlers, LeafValueEditor<E>, ValueAwareEditor<E> {

  /**
   * Separates data object from suggestion specific methods.
   * @param <E> Data object
   */
  public interface TextMachine<E> {
    /**
     * Returns the text representation of the suggestion data object.
     * @param val data object
     * @return text value shown in input box
     */
    String asText(E val);

    /**
     * Returns the error message when suggestion is invalid.
     * @param input value entered by the user
     * @return error message
     */
    String asError(String input);
  }

  private class ValueBoxRenderer extends AbstractRenderer<E> implements Parser<E> {
    @Override
    public String render(final E object) {
      return textMachine.asText(object);
    }

    @Override
    public E parse(final CharSequence text) throws ParseException {
      return getValue();
    }
  }

  /**
   * {@link TextMachine} implementing {@link HasName}. The {@link #asText(HasName)} is implemented as {@link HasName#getName()}.
   * @param <T> implements HasName
   */
  public abstract static class NameTextMachine<T extends HasName> implements TextMachine<T> {
    @Override
    public String asText(final T val) {
      return val == null ? "" : val.getName();
    }
  }

  @SuppressWarnings("rawtypes")
  interface SuggestBoxUiBinder extends UiBinder<Widget, SuggestBox> { }

  private static SuggestBoxUiBinder uiBinder = GWT.create(SuggestBoxUiBinder.class);

  @UiField(provided = true) ValueBox<E> valueBox;

  @UiField Button listButton;

  private E value;

  private final ParameterizedSuggestOracle<E> oracle;
  private final TextMachine<E> textMachine;

  private final PopupPanel popupPanel;
  private final SuggestionPopup<E> suggestionPopup;

  private final BlurHandler blurHandler = new BlurHandler() {
    @Override
    public void onBlur(final BlurEvent event) {
      final ParameterizedSuggestion<E> selection = suggestionPopup.getSelection();
      if (selection != null) {
        //only set selection when not null, otherwise current selection would be overridden.
        selectSuggestion(selection);
      }
    }
  };

  private final KeyUpHandler keyUpHandler = new KeyUpHandler() {
    @Override
    public void onKeyUp(final KeyUpEvent event) {
      switch (event.getNativeKeyCode()) {
      case KeyCodes.KEY_PAGEDOWN:
      case KeyCodes.KEY_PAGEUP:
      case KeyCodes.KEY_DOWN:
      case KeyCodes.KEY_UP:
      case KeyCodes.KEY_ENTER:
        event.preventDefault();
        event.stopPropagation();
        break;
      default:
        oracle.requestSuggestions(valueBox.getText(), suggestionCallback);
        break;
      }
    }
  };

  private final KeyDownHandler keyDownHandler = new KeyDownHandler() {
    @Override
    public void onKeyDown(final KeyDownEvent event) {
      switch (event.getNativeKeyCode()) {
      case KeyCodes.KEY_PAGEDOWN:
        suggestionPopup.moveDown(10);
        event.preventDefault();
        event.stopPropagation();
        break;
      case KeyCodes.KEY_PAGEUP:
        suggestionPopup.moveUp(10);
        event.preventDefault();
        event.stopPropagation();
        break;
      case KeyCodes.KEY_DOWN:
        suggestionPopup.moveDown();
        event.preventDefault();
        event.stopPropagation();
        break;
      case KeyCodes.KEY_UP:
        suggestionPopup.moveUp();
        event.preventDefault();
        event.stopPropagation();
        break;
      case KeyCodes.KEY_ENTER:
        selectSuggestion(suggestionPopup.getSelection());
        break;
      default:
        return;
      }
    }
  };

  private final CloseHandler<PopupPanel> closeHandler = new CloseHandler<PopupPanel>() {

    @Override
    public void onClose(final CloseEvent<PopupPanel> event) {
      if (!event.isAutoClosed()) {
        return;
      }

      selectSuggestion(selectedSuggestion);
    }
  };

  private final Callback suggestionCallback = new Callback() {
    @Override
    @SuppressWarnings("unchecked")
    public void onSuggestionsReady(final Request request, final Response response) {
      final Collection<ParameterizedSuggestion<E>> suggestions = (Collection<ParameterizedSuggestion<E>>) response.getSuggestions();

      clearSuggestions();

      if (suggestions.isEmpty()) {
        preselectSuggestion(null);
        showSuggestions(false);
      } else {
        preselectSuggestion(suggestions.iterator().next());
        addSuggestions(suggestions);
        showSuggestions(true);
      }
    }
  };

  private final SelectionHandler<ParameterizedSuggestion<E>> suggestionSelectionHandler = new SelectionHandler<ParameterizedSuggestion<E>>() {
    @Override
    public void onSelection(final SelectionEvent<ParameterizedSuggestion<E>> event) {
      selectSuggestion(event.getSelectedItem());
    }
  };

  private ParameterizedSuggestion<E> selectedSuggestion;

  private EditorDelegate<E> delegate;

  public SuggestBox(final ParameterizedSuggestOracle<E> oracle, final TextMachine<E> textMachine) {
    this.oracle = oracle;
    this.textMachine = textMachine;

    suggestionPopup = new SuggestionPopup<E>();
    suggestionPopup.addSelectionHandler(suggestionSelectionHandler);
    popupPanel = new PopupPanel(true);
    popupPanel.setWidget(suggestionPopup);
    popupPanel.addCloseHandler(closeHandler);
    popupPanel.setAnimationType(AnimationType.ROLL_DOWN);

    final ValueBoxRenderer renderer = new ValueBoxRenderer();
    valueBox = new ValueBox<E>(Document.get().createTextInputElement(), renderer, renderer) { };
    valueBox.addKeyUpHandler(keyUpHandler);
    valueBox.addKeyDownHandler(keyDownHandler);
    valueBox.addBlurHandler(blurHandler);

    initWidget(uiBinder.createAndBindUi(this));

    listButton.setHTML(SharedConstants.ARROW_DOWN_SMALL);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<E> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public E getValue() {
    return value;
  }

  @Override
  public void setValue(final E value) {
    clearSuggestions(); // reset current selections
    setValue(value, false);
  }

  @Override
  public void setValue(final E value, final boolean fireEvents) {
    setValue(value, fireEvents, false);
  }

  @Override
  public HandlerRegistration addBlurHandler(final BlurHandler handler) {
    return valueBox.addBlurHandler(handler);
  }

  @Override
  public HandlerRegistration addFocusHandler(final FocusHandler handler) {
    return valueBox.addFocusHandler(handler);
  }

  @Override
  public void setDelegate(final EditorDelegate<E> delegate) {
    this.delegate = delegate;
  }

  /**
   * Enable the list button for this {@link SuggestBox}, allowing it to also be used as a drop-down.
   *
   * @param enable true to enable, false to disable
   */
  public void setListButtonVisible(final boolean enable) {
    listButton.setVisible(enable);
  }

  public ValueBoxBase<E> getValueBox() {
    return valueBox;
  }

  @UiHandler("listButton")
  void onListButtonClick(final ClickEvent e) {
    if (popupPanel.isShowing()) {
      showSuggestions(false);
    } else {
      oracle.requestAllSuggestions(suggestionCallback);
    }
  }

  /**
   * Sets the soft value, meant to let implementing classes know the value has changed, without impeding
   * user interaction by setting input fields. Such as when a user is typing. Meant as an internal call.
   *
   * @param value The value that is being set.
   */
  protected void setSoftValue(final E value, final boolean fireEvents) {
    setValue(value, fireEvents, true);
  }

  protected void setValue(final E value, final boolean fireEvents, final boolean soft) {
    if (fireEvents) {
      // Due to soft events, the old value may be equal to the current, but an event hasn't been fired yet.
      // Therefore, force fire the event, even if the value is equal.
      ValueChangeEvent.fire(this, value);
    }

    this.value = value;

    if (!soft) {
      valueBox.setText(textMachine.asText(value));
    }
  }

  private void preselectSuggestion(final ParameterizedSuggestion<E> suggestion) {
    selectSuggestion(suggestion, false);
  }

  private void selectSuggestion(final ParameterizedSuggestion<E> suggestion) {
    selectSuggestion(suggestion, true);
  }

  private void selectSuggestion(final ParameterizedSuggestion<E> suggestion, final boolean finalSelection) {
    this.selectedSuggestion = suggestion;
    setValue(suggestion == null ? null : suggestion.getObject(), finalSelection, !finalSelection);

    if (finalSelection) {
      showSuggestions(false);
    }
  }

  private void showSuggestions(final boolean b) {
    if (b) {
      popupPanel.showRelativeTo(this);
    } else {
      popupPanel.hide();
    }
  }

  private void clearSuggestions() {
    suggestionPopup.clear();
  }

  private void addSuggestions(final Collection<ParameterizedSuggestion<E>> suggestions) {
    suggestionPopup.addAll(suggestions);
  }

  @Override
  public void flush() {
    if (delegate != null && value == null) {
      final String message = textMachine.asError(valueBox.getText());

      delegate.recordError(message, null, valueBox);
    }
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

}
