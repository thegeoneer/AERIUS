/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.importer;

import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.Hidden;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.CancelEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;

/**
 * Controller for import dialog.
 */
public abstract class ImportDialogController<I extends ImportProperties, E extends ImportDialogPanel<I>, R>
    implements CancelEvent.Handler, SubmitHandler, SubmitCompleteHandler, AsyncCallback<R> {
  protected static final String DEFAULT_UPLOAD_ACTION = GWT.getModuleBaseURL() + SharedConstants.IMPORT_SAVE_SERVLET;

  private final ConfirmCancelDialog<I, E> dialog;
  private final ConfirmHandler<I> submitButtonConfirmHandler = new ConfirmHandler<I>() {
    @Override
    public void onConfirm(final ConfirmEvent<I> event) {
      submitClick();
    }
  };
  private boolean inProgress;

  protected E content;

  public ImportDialogController(final E content) {
    this(content, DEFAULT_UPLOAD_ACTION);
  }

  public ImportDialogController(final E content, final String uploadAction) {
    this(content, uploadAction, null);
  }

  public ImportDialogController(final E content, final String uploadAction, final HashMap<String, String> additionalFormParams) {
    this.content = content;

    dialog = new ConfirmCancelDialog<>(null, M.messages().importPanelOk(), M.messages().cancelButton(), true);
    dialog.setHTML(M.messages().importPanelTitle());
    dialog.setWidget(content);
    dialog.addConfirmHandler(submitButtonConfirmHandler);
    dialog.addCancelHandler(this);

    // The content itself may do a file upload on its own accord (using file-drop for example)
    // In which case it will fire a SubmitEvent and SubmitCompleteEvent, notifying the controller
    // of this fact. We will handle the import from there as usual.
    content.asWidget().addHandler(this, SubmitEvent.getType());
    content.asWidget().addHandler(this, SubmitCompleteEvent.getType());
    content.asWidget().addHandler(new ChangeHandler() {
      @Override
      public void onChange(final ChangeEvent event) {
        dialog.center();
      }
    }, ChangeEvent.getType());

    final FormPanel fileForm = dialog.getFormPanel();
    fileForm.addSubmitHandler(this);
    fileForm.addSubmitCompleteHandler(this);
    fileForm.setAction(uploadAction);
    fileForm.setEncoding(FormPanel.ENCODING_MULTIPART);
    fileForm.setMethod(FormPanel.METHOD_POST);

    // add additional form parameters if given
    if (additionalFormParams != null) {
      final ComplexPanel formContentPanel = (ComplexPanel) fileForm.getWidget();

      for (final Entry<String, String> formParam : additionalFormParams.entrySet()) {
        formContentPanel.add(new Hidden(formParam.getKey(), formParam.getValue()));
      }
    }
  }

  @Override
  public void onSubmit(final SubmitEvent event) {
    if (isInProgress()) {
      return;
    }

    onFailure(null);
    setInProgress(true);
  }

  protected boolean isInProgress() {
    return inProgress;
  }

  protected void setConfirmMode(final boolean b) {
    dialog.setConfirmButtonForFormPanel(b);
  }

  /**
   * Handle the string returned from the server on submit complete. The result
   * will indicate if it was success full and contains a uuid to find the results
   * back on the server.
   * @param event SubmitCompleteEvent
   */
  @Override
  public void onSubmitComplete(final SubmitCompleteEvent event) {
    final String[] parts = event.getResults().split(SharedConstants.IMPORT_ARGUMENT_SEPERATOR);
    getImport(parts[1]);
  }

  public abstract void getImport(final String uuid);

  /**
   * Show the dialog.
   */
  public void show() {
    dialog.center();
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        dialog.center();
      }
    });
  }

  protected void hide() {
    dialog.hide();
  }

  @Override
  public void onCancel(final CancelEvent event) {
    dialog.showProgressImage(false);
  }

  public void setSubmitEnabled(final boolean submit) {
    dialog.enableConfirmButton(submit);
  }

  protected void setInProgress(final boolean b) {
    inProgress = b;
    dialog.showProgressImage(b);
    dialog.enableConfirmButton(!b);
  }

  public void setTitle(final String title) {
    dialog.setHTML(title);
  }

  @Override
  public abstract void onFailure(Throwable caught);

  /**
   * Will be called when the user clicks the submit button, and the submit button has been turned off for the form.
   */
  protected abstract void submitClick();
}
