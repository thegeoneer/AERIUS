/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main;

import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.UserContext;

@SuppressWarnings("unchecked")
public abstract class AeriusContextProvider<C extends Context, UC extends UserContext> {

  private C context;
  private UC userContext;

  public C getContext() {
    return context;
  }

  public UC getUserContext() {
    return userContext;
  }

  public void setContext(final Context context) {
    this.context = (C) context;
  }

  public void setUserContext(final UserContext userContext) {
    this.userContext = (UC) userContext;
  }
}
