/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import java.util.Iterator;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;

public class TypedFlowPanel<R extends DivTableRow> implements IsWidget {
  private final FlowPanel panel = new FlowPanel();

  public void add(final R child) {
    panel.add(child);
  }

  public void insert(final R w, final int beforeIndex) {
    panel.insert(w, beforeIndex);
  }

  @SuppressWarnings("unchecked")
  public R getWidget(final int index) {
    return (R) panel.getWidget(index);
  }

  public int size() {
    return panel.getWidgetCount();
  }

  @SuppressWarnings("unchecked")
  public Iterator<R> iterator() {
    return (Iterator<R>) (Iterator<?>) panel.iterator();
  }

  @Override
  public FlowPanel asWidget() {
    return panel;
  }

  public void clear() {
    panel.clear();
  }

  public boolean isEmpty() {
    return panel.getWidgetCount() == 0;
  }

  public boolean remove(final int i) {
    return panel.remove(i);
  }
}
