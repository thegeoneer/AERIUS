/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main;

import com.google.gwt.activity.shared.ActivityMapper;

import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.wui.main.AeriusActivityMapper.ActivityFactory;
import nl.overheid.aerius.wui.main.context.AppContext;

/**
 * @param <F> Type of factory.
 * @param <UC> UserContext type.
 * @param <C> Context type.
 */
public abstract class AeriusActivityMapper<F extends ActivityFactory, A extends AppContext<C, UC>, C extends Context, UC extends UserContext> implements ActivityMapper {
  protected final F factory;
  protected final A context;

  public AeriusActivityMapper(final F factory, final A context) {
    this.factory = factory;
    this.context = context;
  }

  /**
   * The activity mapper used to produce activities.
   */
  public interface ActivityFactory {}
}
