/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.filter;

import java.util.Map;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.Range;
import nl.overheid.aerius.shared.domain.deposition.HabitatFilterRangeContent;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.util.scaling.LinearScalingUtil;

/**
 * Abstract implementation of a habitat filter, typed to a ranging scheme.
 *
 * @param <E> Ranging scheme this filter is typed to.
 */
public abstract class AbstractHabitatFilter<E extends Range> implements IsWidget, HabitatFilter<E> {

  private final HabitatFilterGraph<E> graph;

  private EmissionResultKey emissionResultKey;

  private double upperLimit = Integer.MAX_VALUE;
  private double lowerLimit = Integer.MIN_VALUE;

  private boolean useReceptors;

  /**
   * Constructor, taking the result layer need to filter and the substance to filter for.
   *
   * @param emissionResultKey Selected emissionValueKey
   */
  public AbstractHabitatFilter(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
    this.graph = new HabitatFilterGraph<E>(this, new LinearScalingUtil());
  }

  @Override
  public Widget asWidget() {
    return graph;
  }

  public EmissionResultKey getEmissionResultKey() {
    return emissionResultKey;
  }

  public void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
  }

  protected void setUpperLimit(final double upper) {
    upperLimit = upper;
  }

  /**
   * @param label The label to set for the vertical axes.
   */
  public void setVerticalLabel(final String label) {
    graph.setVerticalLabel(label);
  }

  protected void setLowerLimit(final double lower) {
    lowerLimit = lower;
  }

  public double getUpperLimit() {
    return upperLimit;
  }

  public double getLowerLimit() {
    return lowerLimit;
  }

  protected abstract Map<E, HabitatFilterRangeContent> getRanges();

  protected abstract int getUpperCacheIdx();

  protected abstract int getLowerCacheIdx();

  protected void constructGraph() {
    final Map<E, HabitatFilterRangeContent> ranges = getRanges();

    if (ranges == null || ranges.isEmpty()) {
      graph.setVisible(false);
      return;
    }

    for (final HabitatFilterRangeContent content : ranges.values()) {
      useReceptors = content.isUseNumberReceptors();
      break;
    }

    graph.setHorizontalBottomLabel(useReceptors ? M.messages().habitatFilterNumReceptors() :  M.messages().habitatFilterSurface());

    graph.draw(ranges, getLowerCacheIdx(), getUpperCacheIdx());
  }


  @Override
  public String formatSum(final double sum) {
    return useReceptors ? FormatUtil.toFixed(sum, 0) : FormatUtil.formatSurface(sum);
  }

}
