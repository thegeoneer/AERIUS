/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.event;

import nl.overheid.aerius.wui.main.event.SimpleGenericEvent;

/**
 * Event to add a object of type.
 * @param <T> type of the object to add
 */
public class AddEvent<T> extends SimpleGenericEvent<Class<T>> {

  /**
   * Event to add object.
   * @param clazz Class of the object to add, use to determine what object should be added
   */
  public AddEvent(final Class<T> clazz) {
    super(clazz);
  }
}
