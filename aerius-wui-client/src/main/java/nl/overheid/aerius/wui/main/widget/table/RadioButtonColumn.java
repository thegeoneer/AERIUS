/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import java.util.Arrays;
import java.util.List;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public abstract class RadioButtonColumn<O, T> extends WidgetFactory<O, RadioButtonEditor<T>> {
  private List<T> values;

  @Override
  public RadioButtonEditor<T> createWidget(final O object) {
    final RadioButtonEditor<T> panel = new RadioButtonEditor<T>(values, getObject(object)) {
      @Override
      protected String getText(final T value) {
        return RadioButtonColumn.this.getText(value);
      }
    };

    panel.addValueChangeHandler(new ValueChangeHandler<T>() {
      @Override
      public void onValueChange(final ValueChangeEvent<T> event) {
        setObject(object, event.getValue());
      }
    });

    return panel;
  }

  protected abstract T getObject(O outer);

  protected abstract void setObject(O outer, T object);

  protected abstract String getText(final T value);

  public void setValues(final T[] values) {
    this.values = Arrays.asList(values);
  }

  public void setValues(final List<T> values) {
    this.values = values;
  }
}
