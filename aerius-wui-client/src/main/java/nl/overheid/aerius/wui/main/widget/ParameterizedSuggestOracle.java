/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.SuggestOracle;

/**
 * Parameterized method of using a suggestion oracle.
 *
 * @param <E> The object type
 */
public abstract class ParameterizedSuggestOracle<E> extends SuggestOracle {

  protected final List<E> objects;

  /**
   * Constructor taking a list with objects of the given type.
   *
   * @param objects list of typed objects
   */
  public ParameterizedSuggestOracle(final List<E> objects) {
    this.objects = objects;
  }

  public void requestSuggestions(final String query, final Callback callback) {
    requestSuggestions(query, callback, false);
  }

  public void requestSuggestions(final String query, final Callback callback, final boolean allowEmpty) {
    requestSuggestions(new Request(query), callback, allowEmpty);
  }

  public void requestAllSuggestions(final Callback callback) {
    requestSuggestions(new Request("", objects.size()), callback, true);
  }

  @Override
  public void requestSuggestions(final Request request, final Callback callback) {
    requestSuggestions(request, callback, false);
  }

  protected void requestSuggestions(final Request request, final Callback callback, final boolean allowEmpty) {
    final List<Suggestion> suggestions = new ArrayList<Suggestion>();
    final String query = request.getQuery() == null ? "" : request.getQuery().toLowerCase();
    final boolean isEmpty = query.isEmpty();

    for (final E obj : objects) {
      final Suggestion sug = allowEmpty && isEmpty ? getSuggestion(obj) : getSuggestion(query, obj);
      if (sug != null) {
        suggestions.add(sug);
      }
    }

    callback.onSuggestionsReady(request, new Response(suggestions));
  }

  /**
   * Get an appropriate suggestion for the given query and type
   */
  protected abstract Suggestion getSuggestion(final String query, final E obj);

  /**
   * Get an appropriate suggestion for the given type
   */
  protected abstract Suggestion getSuggestion(final E obj);
}
