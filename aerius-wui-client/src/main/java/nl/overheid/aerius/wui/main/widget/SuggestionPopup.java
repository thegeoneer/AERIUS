/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.Collection;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;

public class SuggestionPopup<E> extends Composite implements HasSelectionHandlers<ParameterizedSuggestion<E>> {
  public interface Style extends CssResource {
    String hover();
  }

  private class LabelWithSuggestion extends Label implements TakesValue<ParameterizedSuggestion<E>> {
    private ParameterizedSuggestion<E> value;

    public LabelWithSuggestion(final ParameterizedSuggestion<E> value) {
      super(value.getDisplayString());
      setTitle(value.getDisplayString());
      this.value = value;
    }

    @Override
    public ParameterizedSuggestion<E> getValue() {
      return value;
    }

    @Override
    public void setValue(final ParameterizedSuggestion<E> value) {
      this.value = value;
    }
  }

  @SuppressWarnings("rawtypes")
  interface SuggestionPopupUiBinder extends UiBinder<Widget, SuggestionPopup> { }

  private static SuggestionPopupUiBinder uiBinder = GWT.create(SuggestionPopupUiBinder.class);

  @UiField Style style;
  @UiField ScrollPanel scrollPanel;
  @UiField FlowPanel suggestions;

  private int hoverSelectionIndex;

  private final ClickHandler mouseClickHandler = new ClickHandler() {
    @SuppressWarnings("unchecked")
    @Override
    public void onClick(final ClickEvent event) {
      if (event.getSource() instanceof SuggestionPopup.LabelWithSuggestion) {
        select((SuggestionPopup<E>.LabelWithSuggestion) event.getSource());
      }
    }
  };

  private final MouseOverHandler mouseOverHandler = new MouseOverHandler() {
    @Override
    public void onMouseOver(final MouseOverEvent event) {
      setHoverStyle(suggestions.getWidgetIndex((Widget) event.getSource()));
    }
  };

  public SuggestionPopup() {
    initWidget(uiBinder.createAndBindUi(this));
  }

  public void clear() {
    if (suggestions.getWidgetCount() > 0) {
      setHoverStyle(hoverSelectionIndex, false);
    }

    hoverSelectionIndex = 0;
    suggestions.clear();
  }

  public void addAll(final Collection<ParameterizedSuggestion<E>> suggestions) {
    for (final ParameterizedSuggestion<E> suggestion : suggestions) {
      add(suggestion);
    }

    if (!suggestions.isEmpty()) {
      setHoverStyle(0);
    }
  }


  public void add(final ParameterizedSuggestion<E> suggestion) {
    final Label label = new LabelWithSuggestion(suggestion);
    label.addClickHandler(mouseClickHandler);
    label.addMouseOverHandler(mouseOverHandler);
    final String styleName = suggestion.getStyleName();
    if (styleName != null && !styleName.isEmpty()) {
      label.addStyleName(styleName);
    }
    suggestions.add(label);
  }

  @Override
  public HandlerRegistration addSelectionHandler(final SelectionHandler<ParameterizedSuggestion<E>> handler) {
    return addHandler(handler, SelectionEvent.getType());
  }

  public String determineStyleName(final ParameterizedSuggestion<E> suggestion) {
    return R.css().suggestionWarning();
  }

  private void select(final LabelWithSuggestion source) {
    SelectionEvent.fire(this, source.getValue());
  }

  private void setHoverStyle(final int idx) {
    setHoverStyle(idx, true);
  }

  private void setHoverStyle(final int idx, final boolean add) {
    suggestions.getWidget(hoverSelectionIndex).setStyleName(style.hover(), !add);
    if (add) {
      hoverSelectionIndex = idx;
      final Widget widg = suggestions.getWidget(hoverSelectionIndex);
      widg.setStyleName(style.hover(), add);
      widg.getElement().scrollIntoView();
    }
  }

  public void moveDown() {
    moveDown(1);
  }

  public void moveDown(final int amount) {
    final int move = Math.min(hoverSelectionIndex + amount, suggestions.getWidgetCount() - 1);
    if (suggestions.getWidgetCount() == 0 || move == suggestions.getWidgetCount() - 1 && hoverSelectionIndex == move) {
      return;
    }
    setHoverStyle(move);
  }

  public void moveUp() {
    moveUp(1);
  }

  public void moveUp(final int amount) {
    final int move = Math.max(hoverSelectionIndex - amount, 0);

    if (move == 0 && hoverSelectionIndex == 0) {
      return;
    }
    setHoverStyle(move);
  }

  /**
   * Get the current selection.
   */
  @SuppressWarnings("unchecked")
  public ParameterizedSuggestion<E> getSelection() {
    // Safe due to class contract.
    return suggestions.getWidgetCount() != 0 && suggestions.getWidgetCount() >= hoverSelectionIndex
        ? ((LabelWithSuggestion) suggestions.getWidget(hoverSelectionIndex)).getValue() : null;
  }

}
