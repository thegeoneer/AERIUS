/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.event;

import java.util.Set;

import nl.overheid.aerius.shared.domain.HabitatHoverType;

/**
 * Event which is fired when a hover event occurs.
 */
public class HabitatTypeHoverEvent extends HabitatTypeEvent {
  public HabitatTypeHoverEvent(final HabitatDisplayType type) {
    super(type, null, 0, null);
  }

  public HabitatTypeHoverEvent(final HabitatHoverType hoverType, final int hoverTypeId, final int habitatTypeId) {
    super(hoverType, hoverTypeId, habitatTypeId);
  }

  public HabitatTypeHoverEvent(final HabitatHoverType hoverType, final int hoverTypeId, final Set<Integer> habitatTypeIds) {
    super(hoverType, hoverTypeId, habitatTypeIds);
  }
}
