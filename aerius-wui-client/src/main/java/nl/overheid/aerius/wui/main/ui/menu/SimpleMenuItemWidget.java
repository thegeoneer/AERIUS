/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.menu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.impl.HyperlinkImpl;

/**
 * Private class for the menu items.
 */
public class SimpleMenuItemWidget extends Composite implements IsMenuItemWidget {
  private static final HyperlinkImpl IMPL = GWT.create(HyperlinkImpl.class);
  private final Anchor anchor;
  private final SimpleMenuItem item;

  public SimpleMenuItemWidget(final PlaceController placeController, final SimpleMenuItem item, final String styleName) {
    this.item = item;
    anchor = new Anchor(item.getTitle(), "#");
    anchor.setStyleName(styleName);
    initWidget(anchor);

    anchor.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        if (IMPL.handleAsClick((Event) event.getNativeEvent())) {
          if (item.isEnabled()) {
            placeController.goTo(item.getPlace(placeController.getWhere()));
          }
          event.preventDefault();
        }
      }
    });
  }

  @Override
  public SimpleMenuItem getItem() {
    return item;
  }

  @Override
  public void update(final Place place) {
    // No-op
  }
}
