/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.text.ParseException;

import com.google.gwt.dom.client.Document;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.text.client.DoubleParser;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.text.shared.Parser;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.uibinder.client.UiConstructor;

import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.util.NumberUtil;

/**
 * {@link NumberValueBox} for {@link Double} values.
 * Also supports setting a fixed number of decimals.
 */
public class DoubleValueBox extends NumberValueBox<Double> {

  private static final String DEFAULT_SEPARATOR = "."; // This separator is always supported as input
  private static final String LOCAL_SEPARATOR = LocaleInfo.getCurrentLocale().getNumberConstants().decimalSeparator();

  /**
   * ValueBox parser which is in essence DoubleParser, but also allows the
   * use of a point as decimal separator even when the local separator is different.
   * E.g. in locale=nl it supports both "." and "," while in locale=en only "." is supported.
   */
  private static final Parser<Double> FLEXABLE_DOUBLE_PARSER = new Parser<Double>() {
    @Override
    public Double parse(final CharSequence object) throws ParseException {
      if (object == null || object.toString().isEmpty()) {
        return 0d;
      }

      return DoubleParser.instance().parse(object.toString().replace(DEFAULT_SEPARATOR, LOCAL_SEPARATOR));
    }
  };

  /**
   * ValueBox renderer that uses a maximum number of decimals and no grouping
   */
  private static final Renderer<Double> CAPPED_DOUBLE_RENDERER = new AbstractRenderer<Double>() {
    @Override
    public String render(final Double object) {
      return object == null ? "" : FormatUtil.formatDecimalCapped(object);
    }
  };

  /**
   * ValueBox parser for which the resulting double will have a fixed (maximum) number of decimals
   */
  private static class FixedDoubleParser implements Parser<Double> {
    private final int length;

    public FixedDoubleParser(final int length) {
      this.length = length;
    }

    @Override
    public Double parse(final CharSequence object) throws ParseException {
      return NumberUtil.round(FLEXABLE_DOUBLE_PARSER.parse(object), length);
    }
  }

  /**
   * ValueBox renderer that uses the given fixed number of decimals and no grouping
   */
  private static class FixedDoubleRenderer extends AbstractRenderer<Double> {
    private final int length;

    public FixedDoubleRenderer(final int length) {
      this.length = length;
    }

    @Override
    public String render(final Double object) {
      return object == null ? "" : FormatUtil.toFixed(object, length);
    }
  }

  /**
   * Construct a value box for decimal numbers that shows the actual value of the double.
   * The number of decimals is capped at {@value nl.overheid.aerius.wui.main.util.FormatUtil#MAX_DECIMALS}.
   * @param label
   */
  @UiConstructor
  public DoubleValueBox(final String label) {
    // Use default renderer and custom parser
    super(Document.get().createTextInputElement(), CAPPED_DOUBLE_RENDERER, FLEXABLE_DOUBLE_PARSER, label);
    setFraction(true);
  }

  /**
   * Construct a value box for decimal numbers that shows the value of the double with a fixed number of decimals.
   * @param label
   * @param length Number of decimals (padded with zeroes)
   */
  public DoubleValueBox(final String label, final int length) {
    // Use custom fixed renderer and custom fixed parser
    super(Document.get().createTextInputElement(), new FixedDoubleRenderer(length), new FixedDoubleParser(length), label);
    setFraction(true);
  }

  @Override
  public Double getValue() {
    final Double value = super.getValue();

    return value == null ? 0 : value;
  }

  @Override
  public Double getValueOrThrow() throws ParseException {
    final Double value = super.getValueOrThrow();

    return value == null ? 0 : value;
  }

  @Override
  protected Double getValueOnInvalidInput() {
    return 0d;
  }
}
