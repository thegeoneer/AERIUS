/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util;

import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * ConcurrentService with concurrency in mind; only calls callback methods if this is the most recent call for its respective concurrencyThread.
 *
 * @param <T> Object to call for
 */
public final class ConcurrentAsyncServiceUtil<T> {
  private ConcurrentAsyncServiceUtil() {}

  public interface ConcurrentAsyncServiceCall<T> {
    void execute(AsyncCallback<T> callback);
  }

  public static class ConcurrentAsyncServiceHandle<T> {
    private final ConcurrentAsyncServiceCall<T> serviceCall;
    private final AsyncProxyCallback<T> proxiedCallback;

    public ConcurrentAsyncServiceHandle(final String concurrencyThread, final HashMap<String, ConcurrentAsyncServiceHandle<?>> concurrencyMap,
        final AsyncCallback<T> callback, final ConcurrentAsyncServiceCall<T> serviceCall) {
      this.serviceCall = serviceCall;
      this.proxiedCallback = new AsyncProxyCallback<T>(callback) {
        @Override
        boolean proceedWithCallback() {
          final ConcurrentAsyncServiceHandle<?> mostRecentOracle = concurrencyMap.get(concurrencyThread);

          final boolean isMostRecentCall = mostRecentOracle == ConcurrentAsyncServiceHandle.this;

          if (isMostRecentCall) {
            concurrencyMap.remove(concurrencyThread);
          } else {
            mostRecentOracle.execute();
          }

          return isMostRecentCall;
        }
      };

      final boolean initialExecution = !concurrencyMap.containsKey(concurrencyThread);

      // Mark current call as most recent.
      concurrencyMap.put(concurrencyThread, this);

      if (initialExecution) {
        execute();
      }
    }

    /**
     * If there is no current call for this thread, go ahead and make the call
     */
    private void execute() {
      serviceCall.execute(proxiedCallback);
    }
  }

  public static <T> void register(final String concurrencyThread, final HashMap<String, ConcurrentAsyncServiceHandle<?>> concurrencyMap,
      final AsyncCallback<T> callback, final ConcurrentAsyncServiceCall<T> serviceCall) {
    new ConcurrentAsyncServiceHandle<T>(concurrencyThread, concurrencyMap, callback, serviceCall);
  }
}
