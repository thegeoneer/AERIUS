/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.user.client.ui.Button;

/**
 * Simple button that contains a reference to some popup content.
 */
public class AttachedPopupButton extends Button {
  private String activeFaceCss;
  private AttachedPopupContent popupContent;

  public void setActiveFaceCss(final String activeFaceCss) {
    this.activeFaceCss = activeFaceCss;
  }

  public String getActiveFaceCss() {
    return activeFaceCss;
  }

  /**
   * Set the popup for this button.
   * 
   * @param popupContent {@link AttachedPopupContent} that represents the popup content.
   */
  @UiChild(limit = 1, tagname = "popup")
  public void addPopup(final AttachedPopupContent popupContent) {
    this.popupContent = popupContent;
  }

  public AttachedPopupContent getPopupContent() {
    return popupContent;
  }
}
