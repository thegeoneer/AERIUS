/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.Iterator;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.ComputedStyle;

/**
 * Panel with a title and a content panel that can be collapsed.
 */
public class CollapsiblePanel extends Composite implements HasWidgets.ForIsWidget, HasCloseHandlers<CollapsiblePanel>, CanCollapse {
  private static final int ANIMATION_TIME = 125;
  private final SimplePanel contentContainer = new SimplePanel();
  private final FlowPanel contentPanel = new FlowPanel();
  private final SimplePanel titleContainer = new SimplePanel();
  private final ButtonPlusMinus buttonPlusMin = new ButtonPlusMinus();

  private final ClickHandler toggleClickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      final EventTarget target = event.getNativeEvent().getEventTarget();
      if (Element.is(target) && buttonPlusMin.getElement().isOrHasChild(Element.as(target))) {
        return;
      }

      if (!titleInteractive && !event.getSource().equals(buttonPlusMin)) {
        buttonPlusMin.toggle();
      }
    }
  };

  /**
   * Indicates whether or not collapse behavior should be limited
   * to the plus/minus button or the entire title.
   */
  private boolean titleInteractive;
  private final FlowPanel titlePanel;
  private String titleText;

  public CollapsiblePanel() {
    final FlowPanel panel = new FlowPanel();
    panel.setStyleName(R.css().collapsiblePanel());
    titlePanel = new FlowPanel();
    titlePanel.setStyleName(R.css().collapsiblePanelHeader());
    titlePanel.add(buttonPlusMin);
    titlePanel.add(titleContainer);
    titlePanel.addStyleName(R.css().flex());
    titlePanel.addStyleName(R.css().noShrink());
    titleContainer.setStyleName(R.css().collapsiblePanelHeaderLabel());
    contentPanel.addStyleName(R.css().flex());
    contentPanel.addStyleName(R.css().columnsClean());
    contentPanel.addStyleName(R.css().grow());
    contentContainer.setWidget(contentPanel);
    contentContainer.setStyleName(R.css().collapsibleContent());

    // Set the initial style height to 0, force it
    contentContainer.removeStyleName(R.css().collapsibleContentAnimated());
    contentContainer.getElement().getStyle().setHeight(0, Unit.PX);
    contentContainer.getElement().getStyle().setOverflow(Overflow.HIDDEN);
    contentContainer.addStyleName(R.css().collapsibleContentAnimated());

    // Append random thing to Test ID
    buttonPlusMin.ensureDebugId(TestID.BUTTON_COLLAPSEPANEL_PLUSMINUS + DOM.createUniqueId());

    panel.add(titlePanel);
    panel.add(contentContainer);
    initWidget(panel);
    buttonPlusMin.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Boolean> event) {
        setValue(event.getValue());
      }
    });

    titlePanel.addDomHandler(toggleClickHandler, ClickEvent.getType());
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public void add(final IsWidget w) {
    add(w.asWidget());
  }

  @Override
  public void add(final Widget widget) {
    contentPanel.add(widget);
  }

  @Override
  public void clear() {
    contentPanel.clear();
  }

  @Override
  public Iterator<Widget> iterator() {
    return contentPanel.iterator();
  }

  @Override
  public boolean remove(final IsWidget w) {
    return remove(w.asWidget());
  }

  @Override
  public boolean remove(final Widget widget) {
    return contentPanel.remove(widget);
  }

  public String getTitleText() {
    return titleText;
  }

  public void setTitleText(final String txt) {
    this.titleText = txt;
    addTitle(new InlineHTML(txt), false);

    buttonPlusMin.ensureDebugId(TestID.BUTTON_COLLAPSEPANEL_PLUSMINUS + "-" + titleText);
  }

  @UiChild(limit = 1)
  public void addTitle(final Widget title, final boolean interactive) {
    setTitleInteractive(interactive);

    titleContainer.setWidget(title);
  }

  public Widget getTitleWidget() {
    return titleContainer.getWidget();
  }

  private void setTitleInteractive(final boolean interactive) {
    titleInteractive = interactive;
    titlePanel.setStyleName(R.css().cursorPointer(), !titleInteractive);
  }

  public void setContentStyleName(final String styleName) {
    contentPanel.setStyleName(styleName, true);
  }

  /**
   * True if the content is collapsed, false if it is not.
   *
   * @return true for yay, false for nay
   */
  @Override
  @Deprecated
  public final boolean isCollapsed() {
    return !getValue();
  }

  @Override
  public final Boolean getValue() {
    return buttonPlusMin.getValue();
  }

  @Override
  public final void setValue(final Boolean open) {
    setValue(open, true);
  }

  @Override
  public void setValue(final Boolean open, final boolean fireEvent) {
    buttonPlusMin.setValue(open, false);

    setCollapseAnimated(!open);

    if (fireEvent) {
      // Notify listeners
      ValueChangeEvent.fire(this, open);
    }

    // Fire an event to close listeners
    if (!open) {
      CloseEvent.fire(this, this, fireEvent);
    }
  }

  public void changeEvent(final Boolean open, final boolean fireEvent) {}

  private void setCollapseAnimated(final boolean collapse) {
    if (collapse) {
      contentContainer.removeStyleName(R.css().collapsibleContentAnimated());
      contentContainer.removeStyleName(R.css().flex());
      contentContainer.removeStyleName(R.css().columnsClean());

      contentContainer.getElement().getStyle().setHeight(contentPanel.getOffsetHeight(), Unit.PX);

      contentContainer.getElement().getStyle().setOverflow(Overflow.HIDDEN);

      // Force the style render through the DOM
      ComputedStyle.forceStyleRender(contentContainer);

      contentContainer.addStyleName(R.css().collapsibleContentAnimated());
      contentContainer.getElement().getStyle().setHeight(0, Unit.PX);
    } else {
      contentContainer.getElement().getStyle().setHeight(contentPanel.getOffsetHeight(), Unit.PX);
      new Timer() {
        @Override
        public void run() {
          contentContainer.getElement().getStyle().clearHeight();
          contentContainer.addStyleName(R.css().flex());
          contentContainer.addStyleName(R.css().columnsClean());
          contentContainer.getElement().getStyle().setOverflow(Overflow.VISIBLE);
        }
      }.schedule(ANIMATION_TIME);
    }
  }

  /**
   * Set the maximum height for the panel.
   * @param maxHeight The maximum height for the panel to set.
   */
  public void setMaxHeight(final int maxHeight) {
    //title should always be visible and isn't inside the content.
    //ensure the content scroller gets the right max height.
    final int contentMaxHeight = Math.max(0, maxHeight - titleContainer.getOffsetHeight());
    contentPanel.getElement().getStyle().setPropertyPx("maxHeight", contentMaxHeight);
    contentPanel.getElement().getStyle().setOverflowY(Overflow.AUTO);
  }

  @Override
  public HandlerRegistration addCloseHandler(final CloseHandler<CollapsiblePanel> handler) {
    return addHandler(handler, CloseEvent.getType());
  }

  /**
   * Set the title style for this collapsible panel.
   * @param styleName Stylename to set.
   */
  public void setTitleStyle(final String styleName) {
    titleContainer.setStyleName(styleName);
  }

}
