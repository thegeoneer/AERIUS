/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.OptGroupElement;
import com.google.gwt.dom.client.OptionElement;

/**
 * {@link OptGroupListBoxEditor} supports optgroups using addGroup() and addGroupedItem().
 * @param <V> data for the individual items on the list box.
 */
public class OptGroupListBoxEditor<V> extends ListBoxEditor<V> {

  private static final String GROUP_SEPERATOR = ":";

  /**
   * Private class to store nested opt group data.
   * @param <V>
   */
  private class OptGroupData {

    private final HashMap<String, ArrayList<V>> groups;

    @SuppressWarnings("serial")
    public OptGroupData() {
      groups = new LinkedHashMap<String, ArrayList<V>>() {
        @Override
        public ArrayList<V> get(final Object key) {
          if (!(key instanceof String)) {
            throw new IllegalArgumentException("Invalid key provided (wrong type).");
          }

          if (!containsKey(key)) {
            put((String) key, new ArrayList<V>());
          }

          return super.get(key);
        }
      };
    }

    public <I extends V> void setValue(final List<I> values) {
      for (final I value : values) {
        if (passedFilter(value)) {
          groups.get(getLabel(value).split(GROUP_SEPERATOR, 2)[0]).add(value);
        }
      }
    }

    protected boolean passedFilter(final V value) {
      return true;
    }
  }

  private OptGroupElement currentGroup;

  @Override
  public <I extends V> void addItems(final List<I> items) {
    final OptGroupData ogd = new OptGroupData();
    ogd.setValue(items);
    addItems(ogd);
  }

  /**
   *
   * @param data
   */
  private void addItems(final OptGroupData data) {
    for (final Map.Entry<String, ArrayList<V>> entry : data.groups.entrySet()) {
      if (entry.getValue().size() == 1) {
        final V v = entry.getValue().get(0);
        addItem(v);
      } else {
        addGroup(entry.getKey());
        for (final V value : entry.getValue()) {
          addGroupedItem(value);
        }
      }
    }
  }

  /**
   * Create a new opt group.
   *
   * @param name the name
   */
  public void addGroup(final String name) {
    final OptGroupElement elem = Document.get().createOptGroupElement();
    elem.setLabel(name);
    getElement().appendChild(elem);
    currentGroup = elem;
  }

  /**
   * Add an item to the last created group.
   *
   * If no group was created, returns and does nothing
   *
   * @param value item
   */
  public void addGroupedItem(final V value) {
    if (currentGroup == null) {
      return;
    }

    final OptionElement option = Document.get().createOptionElement();
    setOptionText(option, getOptName(value), null);
    option.setValue(addOptionItem(value));

    // IE8 doesn't understand setText
    option.setInnerText(getOptName(value));
    currentGroup.appendChild(option);
  }

  /**
   * Name to display in the list.
   * @param name full name
   * @return filtered name
   */
  private String getOptName(final V name) {
    final String[] split = getLabel(name).split(GROUP_SEPERATOR, 2);
    return split.length == 1 ? split[0] :  split[1];
  }

  /**
   * The super method does not remove opt groups.
   *
   * We're doing a full sweep of all child elements here.
   *
   * Also, super.clear() crashes in IE8 when optgroups are present.
   */
  @Override
  public void clear() {
    while (getElement().getChildCount() > 0) {
      getElement().removeChild(getElement().getFirstChild());
    }
    super.clear();
  }
}
