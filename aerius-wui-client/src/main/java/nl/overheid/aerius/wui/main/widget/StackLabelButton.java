/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Styled and selectable button that represents a typed option.
 */
public class StackLabelButton<T> extends Composite implements HasSelectionHandlers<T> {
  interface AreaTypeButtonUiBinder extends UiBinder<Widget, StackLabelButton<?>> {}

  private static final AreaTypeButtonUiBinder UI_BINDER = GWT.create(AreaTypeButtonUiBinder.class);

  interface CustomStyle extends CssResource {
    String active();
    String disabled();
  }

  @UiField CustomStyle style;

  @UiField Label label;

  // Enabled by default
  private boolean enabled = true;
  private boolean active;

  private final T object;

  private final ClickHandler clickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      if (active || !enabled) {
        return;
      }

      setActive(true);
      SelectionEvent.fire(StackLabelButton.this, object);
    }
  };

  public StackLabelButton(final T object) {
    this.object = object;

    initWidget(UI_BINDER.createAndBindUi(this));

    addDomHandler(clickHandler, ClickEvent.getType());

  }

  public void setText(final String text) {
    label.setText(text);
  }

  public void setActive(final boolean active) {
    this.active = active;
    setStyleName(style.active(), active);
  }


  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;

    setStyleName(style.disabled(), !enabled);

  }

  @Override
  public HandlerRegistration addSelectionHandler(final SelectionHandler<T> handler) {
    return addHandler(handler, SelectionEvent.getType());
  }
}
