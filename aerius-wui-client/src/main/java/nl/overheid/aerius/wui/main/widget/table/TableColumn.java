/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import java.util.Collection;

import nl.overheid.aerius.wui.main.widget.WidgetFactory;

/**
 * Defines a column that is an inner table.
 * 
 * @param <T> Inner content type
 * @param <E> Outer content (row) type
 * @param <W> Type of the table
 */
public abstract class TableColumn<T, E, W extends IsDataTable<T>> extends WidgetFactory<E, W> {

  @Override
  public W createWidget(final E object) {
    final W table = createDataTable(object);

    configureDataTable(table);

    if (table != null) {
      table.asDataTable().setRowData(getRowData(object));
    }

    return table;
  }

  public abstract Collection<T> getRowData(E object);

  public void configureDataTable(final W table) {
    // No-op by default
  }

  public abstract W createDataTable(E object);

}
