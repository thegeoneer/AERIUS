/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;

/**
 * Simple tab panel doing no content management whatsoever, fires
 * a SelectionEvent when a tab changes.
 */
public class SimpleTabPanel extends FlowPanel implements HasSelectionHandlers<Integer> {
  /**
   * Indicates an item can have focus.
   */
  public interface CanHaveFocus {
    /**
     * Set the focus.
     *
     * @param focus true to set it, false to unset it.
     */
    void setFocus(boolean focus);
  }

  private Widget focus;

  /**
   * Default constructor.
   */
  public SimpleTabPanel() {
    addStyleName(R.css().tabPanel());
    addStyleName(R.css().noShrink());
  }

  private final ClickHandler clickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      if (event.getSource() instanceof Widget) {
        if (locked || focus == event.getSource()) {
          return;
        }

        setFocus((Widget) event.getSource());
      }
    }
  };

  private boolean locked;

  /**
   * Lock or unlock the tabs. If locking lock everything except the focused tab, if unlocking unlock everything.
   * @param locked true if lock
   */
  public void setLocked(final boolean locked) {
    this.locked = locked;

    for (final Widget w : getChildren()) {
      //if locked set all tabs locked except the one with focus, else unlock everything
      w.setStyleName(R.css().tabPanelItemLocked(), locked && w != focus);
    }
  }

  public boolean isLocked() {
    return locked;
  }

  /**
   * Add the given item to the tab and sets focus if it's the first item.
   *
   * @param item Item to add.
   */
  @Override
  public void add(final Widget item) {
    add(item, getWidgetCount() == 0);
  }

  /**
   * Sets focus to the specified {@link Widget}.
   *
   * @param w the widget to set focus to
   */
  public void setFocus(final Widget w) {
    setFocus(w, true);
  }

  /**
   * Sets focus to the specified {@link Widget} and fires an event if needed.
   *
   * @param w the widget to set focus to
   * @param fireEvent true to fire an event
   */
  public void setFocus(final Widget w, final boolean fireEvent) {
    // Return if we do not know this widget
    if (getWidgetIndex(w) == -1) {
      return;
    }

    // If the previous focus element is the same as the new one, return
    if (focus == w) {
      return;
    }

    if (w instanceof CanHaveFocus) {
      // Remove focus for the previous element
      if (focus != null) {
        focus.removeStyleName(R.css().tabPanelItemFocus());
        if (focus instanceof CanHaveFocus) {
          ((CanHaveFocus) focus).setFocus(false);
        }
      }

      // Update focus widget
      focus = w;
      focus.addStyleName(R.css().tabPanelItemFocus());

      // If the widget to be focused is a CanHaveFocus widget, have it know it's being focused.
      final CanHaveFocus focusWidget = (CanHaveFocus) w;
      focusWidget.setFocus(true);
    }

    // Fire an event if requested
    if (fireEvent) {
      SelectionEvent.fire(SimpleTabPanel.this, getWidgetIndex(w));
    }
  }

  @Override
  public HandlerRegistration addSelectionHandler(final SelectionHandler<Integer> handler) {
    return addHandler(handler, SelectionEvent.getType());
  }

  /**
   * Adds the given item to the tab panel and sets focus if requested.
   *
   * @param item Item to add.
   * @param setFocus True to set focus
   */
  public void add(final Widget item, final boolean setFocus) {
    item.addDomHandler(clickHandler, ClickEvent.getType());
    item.addStyleName(R.css().tabPanelItem());
    item.addStyleName(R.css().textOverflowEllipsis());
    super.add(item);

    if (setFocus) {
      setFocus(item, false);
    }
  }
}
