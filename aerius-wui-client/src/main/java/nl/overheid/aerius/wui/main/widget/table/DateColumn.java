/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import java.util.Date;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.util.FormatUtil;

public abstract class DateColumn<L> extends SimpleWidgetFactory<L> {
  private static final String NOT_AVAILABLE_VALUE = "-";

  @Override
  public Widget createWidget(final L object) {
    return new Label(formatValue(getValue(object)));
  }

  private String formatValue(final Date value) {
    if (value == null) {
      return NOT_AVAILABLE_VALUE;
    }

    return FormatUtil.getDefaultCompactDateFormatter().format(value);
  }

  public abstract Date getValue(final L object);
}
