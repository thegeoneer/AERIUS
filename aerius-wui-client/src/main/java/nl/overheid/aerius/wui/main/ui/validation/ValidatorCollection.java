/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.validation;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Convenience class for combining multiple validators.
 *
 * @param <T> Object type the validators validate
 */
public class ValidatorCollection<T> implements Validator<T> {

  /*
   * validator is a list, not a set, as the order for the validators actually matters.
   */
  private final Set<Validator<T>> validators = new LinkedHashSet<Validator<T>>();

  /**
   * Add validator to the list of validators.
   * Each validator validates based on the point it was added (first in, first validate, fifv...)
   * If a validator is already in the list, it won't be added again but will stick to it's position.
   *
   * @param validator Validator to add
   */
  public void addValidator(final Validator<T> validator) {
    validators.add(validator);
  }

  /**
   * Remove validator to the list of validators.
   *
   * @param validator Validator to remove
   */
  public void removeValidator(final Validator<T> validator) {
    validators.remove(validator);
  }

  @Override
  public String validate(final T value) {
    String message = null;
    for (final Validator<T> validator : validators) {
      message = validator.validate(value);

      if (message != null) {
        break;
      }
    }
    return message;
  }
}
