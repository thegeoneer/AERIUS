/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.dom.client.Touch;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.TouchEndEvent;
import com.google.gwt.event.dom.client.TouchEndHandler;
import com.google.gwt.event.dom.client.TouchMoveEvent;
import com.google.gwt.event.dom.client.TouchMoveHandler;
import com.google.gwt.event.dom.client.TouchStartEvent;
import com.google.gwt.event.dom.client.TouchStartHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.TouchUtil;

/**
 * Base class for all popups attached to the calculator view (buttons on top).
 */
public class AttachedPopupBase extends Composite implements HasWidgets {
  private static final int RESIZE_MARGIN = 2;

  private static final int NEAR_FIELD = 50;
  private static final int OUTER_SPACE = 20;

  private static final int BUTTON_SIZE = 53;
  private static final int PANEL_LENGTHS = 54; // 2 * 11 (grips) + 32 (title)

  private static final AttachedPopupBaseUiBinder UI_BINDER = GWT.create(AttachedPopupBaseUiBinder.class);

  interface AttachedPopupBaseUiBinder extends UiBinder<Widget, AttachedPopupBase> {}

  private AttachedPopupPanel popup;

  private class DragMouseHandler implements MouseDownHandler, MouseUpHandler, MouseMoveHandler, TouchMoveHandler, TouchStartHandler, TouchEndHandler {
    @Override
    public void onMouseDown(final MouseDownEvent event) {
      haltEvent(event);
      beginDragging(event);
    }

    @Override
    public void onMouseMove(final MouseMoveEvent event) {
      haltEvent(event);
      continueDragging(event);
    }

    @Override
    public void onMouseUp(final MouseUpEvent event) {
      haltEvent(event);
      endDragging();
    }

    @Override
    public void onTouchMove(final TouchMoveEvent event) {
      haltEvent(event);
      continueDragging(event);
    }

    @Override
    public void onTouchEnd(final TouchEndEvent event) {
      haltEvent(event);
      endDragging();
    }

    @Override
    public void onTouchStart(final TouchStartEvent event) {
      haltEvent(event);
      beginDragging(event);
    }

    private void haltEvent(final DomEvent<?> event) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  private static enum ResizeType {
    NORTH, SOUTH;
  }

  private abstract class ResizeMouseHandler implements MouseDownHandler, MouseUpHandler, MouseMoveHandler,
  TouchMoveHandler, TouchStartHandler, TouchEndHandler {
    private final Widget grip;

    public ResizeMouseHandler(final Widget grip) {
      this.grip = grip;
    }

    @Override
    public void onMouseDown(final MouseDownEvent event) {
      haltEvent(event);
      beginResize(event, grip);
    }

    @Override
    public void onMouseUp(final MouseUpEvent event) {
      haltEvent(event);
      endResize(grip);
    }

    @Override
    public void onTouchStart(final TouchStartEvent event) {
      haltEvent(event);
      beginResize(event, grip);
    }

    @Override
    public void onTouchEnd(final TouchEndEvent event) {
      haltEvent(event);
      endResize(grip);
    }

    protected void haltEvent(final DomEvent<?> event) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  private class ResizeNorthMouseHandler extends ResizeMouseHandler {
    public ResizeNorthMouseHandler(final Widget grip) {
      super(grip);
    }

    @Override
    public void onMouseMove(final MouseMoveEvent event) {
      haltEvent(event);
      continueResize(event, ResizeType.NORTH);
    }

    @Override
    public void onTouchMove(final TouchMoveEvent event) {
      haltEvent(event);
      continueResize(event, ResizeType.NORTH);
    }
  }

  private class ResizeSouthMouseHandler extends ResizeMouseHandler {
    public ResizeSouthMouseHandler(final Widget grip) {
      super(grip);
    }

    @Override
    public void onMouseMove(final MouseMoveEvent event) {
      haltEvent(event);
      continueResize(event, ResizeType.SOUTH);
    }

    @Override
    public void onTouchMove(final TouchMoveEvent event) {
      haltEvent(event);
      continueResize(event, ResizeType.SOUTH);
    }
  }

  @UiField FlowPanel content;
  @UiField SimplePanel titleContainer;
  @UiField Button okButton;

  @UiField FocusPanel closeButton;
  @UiField SimplePanel closeButtonContainer;

  @UiField SimplePanel resizeGripNorth;
  @UiField SimplePanel resizeGripSouth;

  private boolean resizing;

  private boolean dragging;
  private int absoluteDragStartX = -1;
  private int absoluteDragStartY = -1;
  private int dragStartX;
  private int dragStartY;

  private double windowWidth;
  private final double clientLeft;
  private final double clientTop;
  private HandlerRegistration resizeHandlerRegistration;
  private double resizeStartY;
  private double resizeStartHeight;

  public AttachedPopupBase() {
    initWidget(UI_BINDER.createAndBindUi(this));

    final AttachedPopupBase interested = this;
    Window.addResizeHandler(new ResizeHandler() {
      @Override
      public void onResize(final ResizeEvent event) {
        interested.adjustHeight();
      }
    });

    okButton.ensureDebugId(TestID.BUTTON_PANEL_OK);

    windowWidth = Window.getClientWidth();
    clientLeft = Document.get().getBodyOffsetLeft();
    clientTop = Document.get().getBodyOffsetTop();

    final DragMouseHandler dragHandler = new DragMouseHandler();
    titleContainer.addDomHandler(dragHandler, MouseDownEvent.getType());
    titleContainer.addDomHandler(dragHandler, MouseUpEvent.getType());
    titleContainer.addDomHandler(dragHandler, MouseMoveEvent.getType());
    titleContainer.addDomHandler(dragHandler, TouchStartEvent.getType());
    titleContainer.addDomHandler(dragHandler, TouchEndEvent.getType());
    titleContainer.addDomHandler(dragHandler, TouchMoveEvent.getType());

    final ResizeMouseHandler resizeNorthHandler = new ResizeNorthMouseHandler(resizeGripNorth);
    resizeGripNorth.addDomHandler(resizeNorthHandler, MouseDownEvent.getType());
    resizeGripNorth.addDomHandler(resizeNorthHandler, MouseUpEvent.getType());
    resizeGripNorth.addDomHandler(resizeNorthHandler, MouseMoveEvent.getType());
    resizeGripNorth.addDomHandler(resizeNorthHandler, TouchStartEvent.getType());
    resizeGripNorth.addDomHandler(resizeNorthHandler, TouchEndEvent.getType());
    resizeGripNorth.addDomHandler(resizeNorthHandler, TouchMoveEvent.getType());

    final ResizeMouseHandler resizeSouthHandler = new ResizeSouthMouseHandler(resizeGripSouth);
    resizeGripSouth.addDomHandler(resizeSouthHandler, MouseDownEvent.getType());
    resizeGripSouth.addDomHandler(resizeSouthHandler, MouseUpEvent.getType());
    resizeGripSouth.addDomHandler(resizeSouthHandler, MouseMoveEvent.getType());
    resizeGripSouth.addDomHandler(resizeSouthHandler, TouchStartEvent.getType());
    resizeGripSouth.addDomHandler(resizeSouthHandler, TouchEndEvent.getType());
    resizeGripSouth.addDomHandler(resizeSouthHandler, TouchMoveEvent.getType());
  }

  @UiChild(limit = 1, tagname = "title")
  public void addTitle(final Widget title) {
    titleContainer.setWidget(title);
  }

  public void setTitleText(final String txt) {
    addTitle(new Label(txt));
  }

  @UiHandler("closeButton")
  public void onCloseButtonClick(final ClickEvent e) {
    hide(false);
  }

  public void setCloseText(final String txt) {
    okButton.setText(txt);
  }

  private void adjustHeight() {
    if (popup == null || !popup.isShowing()) {
      return;
    }
  }

  public void setCloseButton(final boolean b) {
    okButton.setVisible(b);
  }

  @UiHandler("okButton")
  void onOkButtonClik(final ClickEvent e) {
    hide();
  }

  private void hide(final boolean autoClosed) {
    if (popup != null) {
      popup.hide();
    }
  }

  private void hide() {
    hide(false);
  }

  @Override
  protected void onAttach() {
    if (resizeHandlerRegistration == null) {
      resizeHandlerRegistration = Window.addResizeHandler(new ResizeHandler() {
        @Override
        public void onResize(final ResizeEvent event) {
          windowWidth = event.getWidth();
        }
      });
    }

    final Visibility visibility = !(popup.getWidget() instanceof RequiresResize) ? Visibility.HIDDEN : Visibility.VISIBLE;
    resizeGripNorth.getElement().getStyle().setVisibility(visibility);
    resizeGripSouth.getElement().getStyle().setVisibility(visibility);

    setButtonVisible(true);

    super.onAttach();
  }

  @Override
  protected void onDetach() {
    super.onDetach();

    absoluteDragStartX = -1;
    absoluteDragStartY = -1;
    resizeHandlerRegistration.removeHandler();
  }

  public void setPopup(final AttachedPopupPanel pp) {
    this.popup = pp;

    pp.addStyleName(R.css().infoPopupPanel());
  }

  @Override
  public void add(final Widget w) {
    content.add(w);
  }

  @Override
  public void clear() {
    content.clear();
  }

  @Override
  public Iterator<Widget> iterator() {
    return content.iterator();
  }

  @Override
  public boolean remove(final Widget w) {
    return content.remove(w);
  }

  public AttachedPopupPanel getPopup() {
    return popup;
  }

  private void beginResize(final TouchStartEvent event, final Widget grip) {
    final Touch touch = TouchUtil.getFirstTouch(event);

    if (touch != null) {
      beginResize(touch.getClientY(), grip);
    }
  }

  private void beginResize(final MouseDownEvent event, final Widget grip) {
    beginResize(event.getClientY(), grip);
  }

  private void beginResize(final int y, final Widget grip) {
    if (popup.getWidget() instanceof RequiresResize && DOM.getCaptureElement() == null) {
      resizing = true;
      DOM.setCapture(grip.getElement());

      if (absoluteDragStartX == -1) {
        absoluteDragStartX = popup.getAbsoluteLeft();
      }
      if (absoluteDragStartY == -1) {
        absoluteDragStartY = popup.getAbsoluteTop();
      }

      resizeStartY = y;
      resizeStartHeight = popup.getElement().getScrollHeight();
    } // else if the popup does not support resizing, bug out.
  }

  private void continueResize(final TouchMoveEvent event, final ResizeType type) {
    if (!resizing) {
      return;
    }

    final Touch touch = TouchUtil.getFirstTouch(event);

    if (touch != null) {
      continueResize(touch.getClientY(), type);
    }
  }

  private void continueResize(final MouseMoveEvent event, final ResizeType type) {
    continueResize(event.getClientY(), type);
  }

  private void continueResize(final int y, final ResizeType type) {
    if (resizing) {
      final double absY = y - resizeStartY;

      double newMaxHeight = 0;
      if (type == ResizeType.NORTH) {
        // Limit to some (small) resize margin so popup doesn't end up at the element's edge.
        popup.setPopupPosition(popup.getAbsoluteLeft(), Math.max(RESIZE_MARGIN, y - RESIZE_MARGIN));
        newMaxHeight = resizeStartHeight - absY;
      } else {
        newMaxHeight = resizeStartHeight + absY;
      }

      popup.setResizeHeight(newMaxHeight);

      ((RequiresResize) popup.getWidget()).onResize();

      dockIfNear();
    }
  }

  private void endResize(final Widget grip) {
    resizing = false;
    DOM.releaseCapture(grip.getElement());
  }

  /**
   * Called on mouse down in the caption area, begins the dragging loop by
   * turning on event capture.
   *
   * @see DOM#setCapture
   * @see #continueDragging
   * @param event the mouse down event that triggered dragging
   */
  protected void beginDragging(final MouseDownEvent event) {
    final int x = event.getX();
    final int y = event.getY();

    beginDragging(x, y);
  }

  private void beginDragging(final TouchStartEvent event) {
    final Touch touch = event.getTouches().get(0);

    final int x = touch.getRelativeX(titleContainer.getElement());
    final int y = touch.getRelativeY(titleContainer.getElement());

    beginDragging(x, y);
  }

  private void beginDragging(final int x, final int y) {
    if (DOM.getCaptureElement() == null) {
      /*
       * Need to check to make sure that we aren't already capturing an element
       * otherwise events will not fire as expected. If this check isn't here,
       * any class which extends custom button will not fire its click event for
       * example.
       */
      dragging = true;
      popup.hideAttachment();
      DOM.setCapture(titleContainer.getElement());

      dragStartX = x;
      dragStartY = y;

      if (absoluteDragStartX == -1) {
        absoluteDragStartX = popup.getAbsoluteLeft();
      }
      if (absoluteDragStartY == -1) {
        absoluteDragStartY = popup.getAbsoluteTop();
      }
    }
  }

  protected void continueDragging(final TouchMoveEvent event) {
    if (dragging) {
      final Touch touch = event.getTouches().get(0);

      final int absX = touch.getRelativeX(titleContainer.getElement()) + popup.getAbsoluteLeft();
      final int absY = touch.getRelativeY(titleContainer.getElement()) + popup.getAbsoluteTop();

      continueDragging(absX, absY);
    }
  }

  /**
   * Called on mouse move in the caption area, continues dragging if it was
   * started by {@link #beginDragging}.
   *
   * @see #beginDragging
   * @see #endDragging
   * @param event the mouse move event that continues dragging
   */
  protected void continueDragging(final MouseMoveEvent event) {
    if (dragging) {
      final int absX = event.getX() + popup.getAbsoluteLeft();
      final int absY = event.getY() + popup.getAbsoluteTop();

      continueDragging(absX, absY);
    }
  }

  private void continueDragging(final int absX, final int absY) {
    // if the mouse is off the screen to the left, right, or top, don't
    // move the dialog box. This would let users lose dialog boxes, which
    // would be bad for modal popups.
    if (absX < clientLeft || absX >= windowWidth || absY < clientTop) {
      return;
    }

    resizePopup();

    popup.setPopupPosition(absX - dragStartX, absY - dragStartY);
    dockIfNear();
  }

  /**
   * Called on mouse up in the caption area, ends dragging by ending event
   * capture.
   *
   * @see DOM#releaseCapture
   * @see #beginDragging
   * @see #endDragging
   */
  protected void endDragging() {
    dragging = false;
    DOM.releaseCapture(titleContainer.getElement());

    dockIfNear();
  }

  private void resizePopup() {
    if (popup.getWidget() instanceof RequiresResize) {
      ((RequiresResize) popup.getWidget()).onResize();
    }
  }

  public void setButtonVisible(final boolean visible) {
    closeButtonContainer.setVisible(visible);

    popup.getElement().getStyle().setProperty("minHeight", getPanelSkeletonHeight(), Unit.PX);
  }

  /**
   * FIXME Doing a lot of double calls here (reattach/hide and button visibility) which we _may_ want to avoid.
   */
  private void dockIfNear() {
    if (popup == null) {
      return;
    }

    if (isNear(popup, absoluteDragStartX, absoluteDragStartY)) {
      popup.setPopupPosition(absoluteDragStartX, absoluteDragStartY);
      popup.reattach();
      setButtonVisible(true);
    } else {
      popup.hideAttachment();
      setButtonVisible(false);
    }
  }

  private boolean isNear(final Widget widget, final int x, final int y) {
    final int diffX = widget.getAbsoluteLeft() - x;
    final int diffY = widget.getAbsoluteTop() - y;

    // Test with pyth and return whether near or not
    return Math.sqrt(diffX * diffX + diffY * diffY) < NEAR_FIELD;
  }

  public int getPanelSkeletonHeight() {
    return PANEL_LENGTHS + (closeButtonContainer.isVisible() ? BUTTON_SIZE : 0);
  }

  public double calculateMaxHeightBasedOnScreen() {
    final String parentMaxHeight = getPopup().getElement().getStyle().getProperty("maxHeight");

    // Get the max height relative to the screen
    double newMaxHeight = Math.max(0, Window.getClientHeight()
        - getPopup().getAbsoluteTop()
        - getPanelSkeletonHeight()
        - AttachedPopupBase.OUTER_SPACE);

    // If the parent window has a max height, use that (unless the screen height is smaller)
    if (!parentMaxHeight.isEmpty()) {
      final double parsedMaxHeight = Double.parseDouble(parentMaxHeight.substring(0, parentMaxHeight.length() - 2));
      newMaxHeight = Math.max(0, Math.min(newMaxHeight, parsedMaxHeight - getPanelSkeletonHeight()));
    }

    return newMaxHeight;
  }
}
