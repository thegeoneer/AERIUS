/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.Notification;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;

public class NotificationPopup extends Composite implements MouseOverHandler, MouseOutHandler {
  private static final int OFFSET_FROM_TOP = 220;
  private static final int MIN_HEIGHT = 200;

  interface NotificationPopupUiBinder extends UiBinder<Widget, NotificationPopup> { }

  private static NotificationPopupUiBinder uiBinder = GWT.create(NotificationPopupUiBinder.class);

  private final ArrayList<NotificationItem> unreadNotifications = new ArrayList<NotificationItem>();

  @UiField HTMLPanel panel;
  @UiField ScrollPanel scroller;
  @UiField FlowPanel list;
  @UiField Button removeButton;

  private final SpanElement toggleText = Document.get().createSpanElement();

  private final NotificationPanel button;

  public NotificationPopup(final NotificationPanel button) {
    this.button = button;

    initWidget(uiBinder.createAndBindUi(this));

    toggleText.setInnerHTML(M.messages().helpOn());

    panel.addDomHandler(this, MouseOverEvent.getType());
    panel.addDomHandler(this, MouseOutEvent.getType());

    // Rather ugly resize implementation, we should have the panel be notified instead of listening to the global resize event.
    Window.addResizeHandler(new ResizeHandler() {
      @Override
      public void onResize(final ResizeEvent event) {
        setMaxHeight();
      }
    });

    setMaxHeight();
    removeButton.ensureDebugId(TestID.BUTTON_DELETE_ALL_NOTIFICATIONS);
  }

  @Override
  public void onMouseOver(final MouseOverEvent event) {
    button.setStayActive(true);
  }

  @Override
  public void onMouseOut(final MouseOutEvent event) {
    button.setStayActive(false);
  }

  @UiHandler("removeButton")
  void onRemoveAllClick(final ClickEvent e) {
    removeAll();
    button.hide();
  }

  public void add(final Notification notification) {
    final NotificationItem ni = new NotificationItem(this, notification);

    unreadNotifications.add(ni);
    list.insert(ni, 0);
  }

  public boolean remove(final Widget w) {
    unreadNotifications.remove(w);
    final boolean rem = list.remove(w);
    button.updateVisiblity();
    return rem;
  }

  private void removeAll() {
    unreadNotifications.clear();
    list.clear();
    button.updateVisiblity();
  }

  public void readAll() {
    for (final NotificationItem item : unreadNotifications) {
      item.setRead();
    }

    unreadNotifications.clear();
  }

  public int size() {
    return list.getWidgetCount();
  }

  public int getNumUnread() {
    return unreadNotifications.size();
  }

  public boolean hasUnread() {
    return getNumUnread() != 0;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public void setRead(final NotificationItem w) {
    w.setRead();
    unreadNotifications.remove(w);
    button.updateVisiblity();
  }

  public NotificationItem getWidget(final int i) {
    return (NotificationItem) list.getWidget(i);
  }

  private void setMaxHeight() {
    final int maxHeight = Math.max(MIN_HEIGHT, Window.getClientHeight() - OFFSET_FROM_TOP);
    scroller.getElement().getStyle().setPropertyPx("maxHeight", maxHeight);
  }
}
