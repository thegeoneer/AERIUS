/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.Range;
import nl.overheid.aerius.shared.domain.deposition.HabitatFilterRangeContent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.util.scaling.ScalingUtil;
import nl.overheid.aerius.wui.main.widget.DoubleVerticalSliderBar;
import nl.overheid.aerius.wui.main.widget.DoubleVerticalSliderBar.DoubleVerticalSliderBarResources;
import nl.overheid.aerius.wui.main.widget.DoubleVerticalSliderBar.ValueRange;

/**
 * View for a Habitat Filter. Creates a graph and slider for a set of potentially arbitrarily ranged values.
 *
 * @param <E> Ranging type, needed by controller
 */
public class HabitatFilterGraph<E extends Range> extends Composite implements ValueChangeHandler<ValueRange> {
  // One and a third *
  private static final double MAXIMUM_NUM_MARGIN = 1d + 1d / 3d;

  interface HabitatFilterGraphUiBinder extends UiBinder<Widget, HabitatFilterGraph<?>> { }

  private static final HabitatFilterGraphUiBinder UI_BINDER = GWT.create(HabitatFilterGraphUiBinder.class);

  interface CustomStyle extends DoubleVerticalSliderBarResources { }

  private final HabitatFilter<E> controller;
  private final ScalingUtil scaler;

  @UiField CustomStyle style;
  @UiField(provided = true) VerticalLabels verticalLabels;
  @UiField FlowPanel barContainer;

  @UiField(provided = true) DoubleVerticalSliderBar slider;

  @UiField FlowPanel sliderInfoContainer;

  @UiField Label horizontalTopLabel;
  @UiField Label horizontalBottomLabel;

  @UiField Label verticalLeftLabel;

  @UiField Label sliderInfoTop;
  @UiField Label sliderInfoMiddle;
  @UiField Label sliderInfoBottom;

  /**
   * Initialize the {@link HabitatFilterGraph}.
   *
   * @param controller Controller that determines what's the content.
   * @param scaler Method of scaling.
   */
  public HabitatFilterGraph(final HabitatFilter<E> controller, final ScalingUtil scaler) {
    this.controller = controller;
    this.scaler = scaler;
    verticalLabels = new VerticalLabels(scaler);

    slider = new DoubleVerticalSliderBar(0, controller.size());
    slider.addValueChangeHandler(this);

    initWidget(UI_BINDER.createAndBindUi(this));

    slider.setResources(style);
  }

  @Override
  protected void onDetach() {
    super.onDetach();
    controller.clear();
  }

  /**
   * Draw the graph.
   *
   * @param ranges Ranged data to draw (bars)
   * @param initUpperIdx initial upper slider value
   * @param initLowerIdx initial lower slider value
   */
  public void draw(final Map<E, HabitatFilterRangeContent> ranges, final int initUpperIdx, final int initLowerIdx) {
    barContainer.clear();

    // We must determine the max value in the list in order to properly scale it in the graph.
    double max = 0.0;
    final ArrayList<Double> rangeValues = new ArrayList<Double>();
    for (final Entry<E, HabitatFilterRangeContent> range : ranges.entrySet()) {
      max = Math.max(range.getValue().getTotalValue(), max);

      final HabitatFilterGraphBar<E> habitatFilterGraphBar = new HabitatFilterGraphBar<E>(scaler, range.getValue());
      final String barTitle = range.getValue().isUseNumberReceptors()
          ? M.messages().habitatFilterBarTitleHexagons(String.valueOf(range.getValue().getNumberOfReceptors()))
          : M.messages().habitatFilterBarTitleArea(FormatUtil.formatSurfaceWithUnit(range.getValue().getSurface()));
      habitatFilterGraphBar.setBarTitle(barTitle);
      habitatFilterGraphBar.setColor(range.getKey().getColor());
      rangeValues.add(range.getKey().getLowerValue());
      barContainer.add(habitatFilterGraphBar);
    }

    Collections.reverse(rangeValues);
    verticalLabels.updateLabels(rangeValues);
    //avoid having colored bars when there is no max.
    if (max == 0.0) {
      max = MAXIMUM_NUM_MARGIN;
    }

    // Increase the max by a margin
    max *= MAXIMUM_NUM_MARGIN;

    //
    for (int i = 0; i < barContainer.getWidgetCount(); i++) {
      final Widget item = barContainer.getWidget(i);
      if (item instanceof HabitatFilterGraphBar) {
        ((HabitatFilterGraphBar<?>) item).updateWidth(max);
      }
    }

    // The reason for this is lengthy and I won't go into it. Just don't change it and the world will live to see another day.
    // Actually, to elaborate, the graph height will be determined after the browser is done rendering. We need to set the slider
    // height to the graph height though, but we can't do that at this point. To solve this we'll be doing it after the browser
    // is done rendering, using scheduler's deferred command to do it.
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        slider.setMaxHeight(controller.size());
        slider.getElement().getStyle().setHeight(barContainer.getOffsetHeight() - 3, Unit.PX);
        sliderInfoContainer.getElement().getStyle().setHeight(barContainer.getOffsetHeight() - 3, Unit.PX);
        slider.setValue(new ValueRange(Math.max(0, initLowerIdx), Math.min(slider.getMaxValue(), initUpperIdx)));
      }
    });
  }

  @Override
  public void onValueChange(final ValueChangeEvent<ValueRange> event) {
    // Update graph visuals
    updateGraph(event.getValue());

    // Update slider visuals
    updateFilterInfo(event.getValue());

    // Redraw the layer, given new filter values
    redrawLayer(event.getValue());
  }

  /**
   * Set the label for the vertical axis.
   *
   * @param txt label to set.
   */
  public void setVerticalLabel(final String txt) {
    verticalLeftLabel.setText(txt);
  }

  /**
   * Set the label for the aggregated values next to the graph.
   *
   * @param txt label to set.
   */
  public void setHorizontalTopLabel(final String txt) {
    horizontalTopLabel.setText(txt);
  }

  /**
   * Set the label for the horizontal axis.
   *
   * @param txt label to set.
   */
  public void setHorizontalBottomLabel(final String txt) {
    horizontalBottomLabel.setText(txt);
  }

  /**
   * The slider shows information about the regions that are filtered and those that are not filtered (divided in top and bottom)
   *
   * @param value
   */
  private void updateFilterInfo(final ValueRange value) {
    double topSum = 0;
    double middleSum = 0;
    double bottomSum = 0;

    for (int i = 0; i < barContainer.getWidgetCount(); i++) {
      final Widget item = barContainer.getWidget(i);
      if (item instanceof HabitatFilterGraphBar) {
        final HabitatFilterRangeContent content = ((HabitatFilterGraphBar<?>) item).getContent();

        if (i < value.getLowerValue()) {
          topSum += content.getTotalValue();
        } else if (i < value.getUpperValue()) {
          middleSum += content.getTotalValue();
        } else {
          bottomSum += content.getTotalValue();
        }
      }
    }

    verticalLabels.setActiveRange(value);

    // Upper info
    if (value.getLowerValue() > 0) {
      sliderInfoTop.getElement().getStyle().setLineHeight(slider.getLowerKnob().getElement().getOffsetTop()
          + slider.getUpperKnob().getOffsetHeight() / 2, Unit.PX);
    }
    sliderInfoTop.setText(controller.formatSum(topSum));
    sliderInfoTop.setVisible(value.getLowerValue() > 0);

    // Middle info
    sliderInfoMiddle.setText(controller.formatSum(middleSum));
    sliderInfoMiddle.getElement().getStyle().setLineHeight(slider.getUpperKnob().getElement().getOffsetTop()
        - slider.getLowerKnob().getElement().getOffsetTop(), Unit.PX);

    // Bottom info
    if (value.getUpperValue() < controller.size()) {
      sliderInfoBottom.getElement().getStyle().setLineHeight(slider.getElement().getClientHeight()
          - (slider.getUpperKnob().getElement().getOffsetTop() + slider.getUpperKnob().getOffsetHeight() / 2), Unit.PX);
    }
    sliderInfoBottom.setText(controller.formatSum(bottomSum));
    sliderInfoBottom.setVisible(value.getUpperValue() < controller.size());
  }

  @SuppressWarnings("unchecked")
  private void updateGraph(final ValueRange value) {
    final int upperValue = value.getUpperValue() - 1;
    for (int i = 0; i < barContainer.getWidgetCount(); i++) {
      ((HabitatFilterGraphBar<E>) barContainer.getWidget(i)).setActive(value.getLowerValue() <= i && i <= upperValue);
    }
  }

  private void redrawLayer(final ValueRange value) {
    controller.setValueRange(value);
  }
}
