/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.dialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;

import nl.overheid.aerius.wui.main.resources.R;

public class AeriusDialogBox extends DialogBox {
  protected interface ADBTemplate extends SafeHtmlTemplates {
    /**
     * Text wrapped by a h2 tag.
     */
    @Template("<h2>{0}</h2>")
    SafeHtml h2(SafeHtml html);
  }

  private static final ADBTemplate TEMPLATE = GWT.create(ADBTemplate.class);

  protected final FlowPanel panel = new FlowPanel();

  public AeriusDialogBox() {
    setAnimationEnabled(true);
    setAnimationType(AnimationType.CENTER);

    setGlassEnabled(true);
    setStyleName(R.css().confirmCancelDialog());
  }

  @Override
  public void setHTML(final SafeHtml html) {
    super.setHTML(TEMPLATE.h2(html));
  }

  @Override
  public void setHTML(final String html) {
    super.setHTML(TEMPLATE.h2(SafeHtmlUtils.fromTrustedString(html)));
  }

  @Override
  public void setText(final String text) {
    super.setHTML(TEMPLATE.h2(SafeHtmlUtils.fromTrustedString(text)));
  }
}
