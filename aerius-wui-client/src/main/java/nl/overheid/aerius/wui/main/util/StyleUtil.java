/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.logical.shared.AttachEvent.Handler;
import com.google.gwt.user.client.ui.ValueBoxBase;

import nl.overheid.aerius.wui.main.resources.R;

/**
 * Util class to set style properties on widgets/elements.
 */
public class StyleUtil {
  public static final StyleUtil I = GWT.create(StyleUtil.class);

  /**
   * For IE, which doesn't support a placeholder attribute the placeholder text
   * is set as value in the input element when the element has no focus and no
   * content. This doesn't work perfectly for input field so the initial text
   * is set deferred via a scheduler.
   * Also it might cause problems for type specific input boxes that for example
   * expect int values. In case where the input value is read as to be the value
   * and the content is the placeholder. If not is done is basically means the
   * input field is obliged as it expects to have a valid value.
   */
  static class IEStyleUtil extends StyleUtil {
    @Override
    public void setPlaceHolder(final ValueBoxBase<?> vbb, final String text) {
      vbb.getElement().setAttribute("placeholder", text);
      final ScheduledCommand scheduledCommand = new ScheduledCommand() {
        @Override
        public void execute() {
          setPlaceHolderState(vbb, text, false);
        }
      };
      vbb.addAttachHandler(new Handler() {
        @Override
        public void onAttachOrDetach(final AttachEvent event) {
          if (event.isAttached()) {
            Scheduler.get().scheduleFinally(scheduledCommand);
          }
        }
      });
      vbb.addFocusHandler(new FocusHandler() {
        @Override
        public void onFocus(final FocusEvent event) {
          setPlaceHolderState(vbb, text, true);
        }
      });
      vbb.addBlurHandler(new BlurHandler() {
        @Override
        public void onBlur(final BlurEvent event) {
          setPlaceHolderState(vbb, text, false);
        }
      });
    }

    /**
     * Set the place holder test. If the {@link ValueBoxBase} get focus then
     * remove the content if the content matches the placeholder text. If it
     * doens't have focus or lost focus only set the placeholder text if the
     * context is null or empty.
     *
     * @param vbb Widget to show placeholder text on.
     * @param placeHolderText placeholder text to display
     * @param focus true if this was called when the item got focus
     */
    private void setPlaceHolderState(final ValueBoxBase<?> vbb, final String placeHolderText, final boolean focus) {
      final InputElement e = (InputElement) vbb.getElement().cast();
      final boolean isPlaceHolderText = (e.getValue() != null) && placeHolderText.equals(e.getValue());

      if (focus) {
        if (isPlaceHolderText) {
          e.setValue("");
          e.removeClassName(R.css().inputPlaceHolder());
        }
      } else {
        if ((e.getValue() == null) || e.getValue().isEmpty()) {
          e.setValue(placeHolderText);
          e.addClassName(R.css().inputPlaceHolder());
        }
      }
    }
  }

  private StyleUtil() {}

  public static String joinStyles(final String... styles) {
    return TextUtil.joinStrings(styles, " ");
  }

  /**
   * Set the text as placeholder property on a html element.
   *
   * @param vbb Widget to set placeholder
   * @param text Text to set as placeholder
   */
  public void setPlaceHolder(final ValueBoxBase<?> vbb, final String text) {
    vbb.getElement().setAttribute("placeholder", text);
  }
}
