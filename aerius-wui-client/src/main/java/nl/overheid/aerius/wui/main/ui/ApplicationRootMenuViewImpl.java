/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ProvidesResize;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.i18n.Language;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.event.RefreshMenuEvent;
import nl.overheid.aerius.wui.main.event.SystemInfoMessageEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.menu.IsMenuItemWidget;
import nl.overheid.aerius.wui.main.ui.menu.MenuItem;
import nl.overheid.aerius.wui.main.ui.menu.MenuItemFactory;
import nl.overheid.aerius.wui.main.ui.systeminfo.SystemInfoMessagePanel;
import nl.overheid.aerius.wui.main.util.development.DevModeUtil;
import nl.overheid.aerius.wui.main.util.development.DevPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Main Application Display implementation. Shows a menu on the left side and
 * a content panel on the right side. The content panel is the display panel
 * of the ActivityManager.
 */
@Singleton
public final class ApplicationRootMenuViewImpl extends Composite implements RequiresResize, ProvidesResize, ApplicationRootView {

  interface ApplicationRootViewEventBinder extends EventBinder<ApplicationRootMenuViewImpl> {}

  private final ApplicationRootViewEventBinder eventBinder = GWT.create(ApplicationRootViewEventBinder.class);

  private static ApplicationRootViewUiBinder uiBinder = GWT.create(ApplicationRootViewUiBinder.class);

  interface ApplicationRootViewUiBinder extends UiBinder<Widget, ApplicationRootMenuViewImpl> {}

  interface Style extends CssResource {
    String menuItem();

    String menuDisabled();

    String menuActive();

    String noHoverMenuItem();

    String subMenuItem();
  }

  @UiField Style style;
  @UiField(provided = true) Image logoImage;
  @UiField(provided = true) DockLayoutPanel dock;
  @UiField FlowPanel westPanel;
  @UiField ComplexPanel menuItems;
  @UiField CheckBox helpButton;
  @UiField Anchor manualAnchor;
  @UiField Label languageText;
  @UiField FlowPanel languageItem;
  @UiField(provided = true)
  SystemInfoMessagePanel systemInfoMessagePanel;
  private final SpanElement helpText = Document.get().createSpanElement();
  private final HelpPopupController hpC;
  private final PlaceController placeController;
  private final ApplicationDisplay display;

  @Inject(optional = true)
  public ApplicationRootMenuViewImpl(final HelpPopupController hpC, final MenuItemFactory menuItemFactory, final EventBus eventBus,
      final PlaceController placeController, final ApplicationDisplay display, final Context context,
      final SystemInfoMessagePanel systemInfoMessagePanel,
      final DevPanel devPanel) {
    this.hpC = hpC;
    this.placeController = placeController;
    this.display = display;
    this.systemInfoMessagePanel = systemInfoMessagePanel;

    logoImage = new Image(display.getLogo().getSafeUri());
    dock = new DockLayoutPanel(Unit.PX);

    // If, and only if, we're in developer mode, show the dev panel.
    if (devPanel != null && DevModeUtil.I.isDevMode()) {
      dock.addNorth(devPanel, DevPanel.HEIGHT);
    }

    initWidget(uiBinder.createAndBindUi(this));

    // Add overflow to the parent element. It must be !important because it's a Layout panel
    // which means the overflow is auto-set. Because the parent is not accessible directly it needs to
    // be done via the child panel. All this is necessary to show the box-shadow on the right side of the
    // westpanel.
    westPanel.getElement().getParentElement().setAttribute("style",
        westPanel.getElement().getParentElement().getAttribute("style") + ";overflow:visible !important");

    final String manualUrl = context.getSetting(SharedConstantsEnum.MANUAL_URL);
    manualAnchor.setHref(manualUrl);

    helpText.setInnerHTML(M.messages().helpOff());
    helpButton.getElement().appendChild(helpText);
    helpButton.ensureDebugId(TestID.HELP_BUTTON_TOGGLE);

    final Language lang = Language.fromToken(LocaleInfo.getCurrentLocale().getLocaleName()) == Language.EN ? Language.NL : Language.EN;
    languageText.setText(M.messages().applicationLanguage(lang));
    languageItem.addDomHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        final UrlBuilder bldr = Location.createUrlBuilder();
        bldr.setParameter(SharedConstants.LOCALE_KEY, lang.name().toLowerCase());

        Location.replace(bldr.buildString());
      }
    }, ClickEvent.getType());

    hpC.addWidget(helpButton, hpC.tt().ttHelpButton());

    dock.add(display);
    addMenus(menuItemFactory, display.getMenuItems());
    eventBinder.bindEventHandlers(this, eventBus);
    dock.setWidgetHidden(systemInfoMessagePanel.asWidget(), true);
  }

  @EventHandler
  void onInfoPanelChange(final SystemInfoMessageEvent event) {
    dock.setWidgetHidden(systemInfoMessagePanel.asWidget(), event.getValue().isEmpty());
    dock.onResize();
  }

  @Override
  public void onResize() {
    dock.onResize();
  }

  @UiHandler("helpButton")
  void onHelpClickEvent(final ClickEvent event) {
    helpText.setInnerHTML(helpButton.getValue() ? M.messages().helpOn() : M.messages().helpOff());
    hpC.setEnabled(helpButton.getValue());
  }

  @EventHandler
  void onRefreshMenu(final RefreshMenuEvent event) {
    refreshMenu(placeController.getWhere());
  }

  @EventHandler
  void onPlaceChange(final PlaceChangeEvent event) {
    refreshMenu(event.getValue());
  }

  private void refreshMenu(final Place place) {
    for (final Widget w : menuItems) {
      if (w instanceof IsMenuItemWidget) {
        final MenuItem item = ((IsMenuItemWidget) w).getItem();
        final boolean enabled = item.isEnabled(place);
        w.setStyleName(style.noHoverMenuItem(), !enabled);
        w.setStyleName(style.menuItem(), enabled);
        w.setStyleName(style.menuActive(), enabled && item.isActivePlace(place));
        w.setStyleName(style.menuDisabled(), !enabled);
        w.setStyleName(R.css().flex(), true);

        ((IsMenuItemWidget) w).update(place);
      }
    }
  }

  /**
   * Add menu items to the view, and adds submenu items.
   *
   * @param historyMapper
   * @param placeController
   * @param mainMenuItems
   */
  private void addMenus(final MenuItemFactory factory, final List<MenuItem> mainMenuItems) {
    addMenus(factory, mainMenuItems, style.menuItem());
  }

  private void addMenus(final MenuItemFactory factory, final List<MenuItem> mainMenuItems, final String styleName) {
    for (int i = 0; i < mainMenuItems.size(); ++i) {
      final MenuItem item = mainMenuItems.get(i);
      final IsWidget menuItemWidget = factory.createMenuItem(placeController, item, styleName);
      menuItems.add(menuItemWidget);

      if (item.getHelpInfo() != null) {
        hpC.addWidget(menuItemWidget, item.getHelpInfo());
      }

      ensureDebugId(menuItemWidget.asWidget().getElement(), TestID.MENU_ITEMS, item.getTestId());
      if (item.getChildren() != null) {
        addMenus(factory, item.getChildren(), style.subMenuItem());
      }
    }
  }

  @Override
  public void setWidget(final IsWidget w) {
    display.setWidget(w);
  }
}
