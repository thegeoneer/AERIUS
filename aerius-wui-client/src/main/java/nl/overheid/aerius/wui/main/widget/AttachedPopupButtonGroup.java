/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Group of buttons that have an attached popup. Can contain
 * {@link Widget}s that do not have a popup.
 */
public class AttachedPopupButtonGroup extends FlowPanel {
  public enum ALIGNMENT {
    LEFT, RIGHT;
  }

  private final HashMap<AttachedPopupButton, AttachedPopupPanel> popups = new HashMap<>();

  // TODO Make this configurable / use css (hard to do due to popup alignment)
  private static final int POPUP_WIDTH = 338;

  private final ClickHandler clickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      final AttachedPopupButton btn = (AttachedPopupButton) event.getSource();

      final boolean active = updatePopup(btn);

      activateButton(btn, active);
    }
  };

  private ALIGNMENT alignment = ALIGNMENT.LEFT;

  /**
   * Add a widget to the group of buttons. If the attached widget is of the type {@link AttachedPopupButton}
   * we'll add handlers to open up the associated popup.
   *
   * @param w Widget to add.
   */
  @Override
  public void add(final Widget w) {
    super.add(w);

    if (w instanceof AttachedPopupButton) {
      final AttachedPopupButton btn = (AttachedPopupButton) w;
      final AttachedPopupPanel pp = new AttachedPopupPanel();

      pp.addCloseHandler(new CloseHandler<PopupPanel>() {
        @Override
        public void onClose(final CloseEvent<PopupPanel> event) {
          activateButton(btn, false);
        }
      });

      btn.getPopupContent().setPopup(pp);
      btn.addClickHandler(clickHandler);

      popups.put(btn, pp);
    }
  }

  /**
   * Set the alignment of the button group. In string format to allow
   * for a uibinder setter.
   *
   * @param alignment string format (.name()) of a {@link ALIGNMENT} value, case insensitive
   */
  public void setAlignment(final String alignment) {
    setAlignment(ALIGNMENT.valueOf(alignment.toUpperCase()));
  }

  /**
   * Set the alignment of the button group.
   *
   * @param alignment {@link ALIGNMENT} to align popups, relative to the button group.
   */
  public void setAlignment(final ALIGNMENT alignment) {
    this.alignment = alignment;
  }

  /**
   * Deactive the button group (hides the popup and deactivates button if visible).
   */
  public void deactivate() {
    for(final Entry<AttachedPopupButton, AttachedPopupPanel> entry : popups.entrySet()) {
      final AttachedPopupPanel pp = entry.getValue();
      if (pp.isShowing()) {
        activateButton(entry.getKey(), false);
        pp.hide();
      }
    }
  }

  private void activateButton(final AttachedPopupButton btn, final boolean active) {
    btn.setStyleName(btn.getActiveFaceCss(), active);
  }

  private boolean updatePopup(final AttachedPopupButton button) {
    final AttachedPopupPanel pp = popups.get(button);

    if (pp.getWidget() != null && pp.getWidget() == button.getPopupContent() && pp.isShowing()) {
      pp.hide();
      return false;
    }
    pp.setWidget(button.getPopupContent());
    pp.attachToWidget(button);
    updatePopupPosition(pp);
    pp.show();
    return true;
  }

  private void updatePopupPosition(final AttachedPopupPanel pp) {
    pp.setPopupPosition2(alignment == ALIGNMENT.RIGHT
        ? getAbsoluteLeft() + getOffsetWidth() - POPUP_WIDTH
            : getAbsoluteLeft(),
            getAbsoluteTop() + getOffsetHeight() - 1);
  }
}
