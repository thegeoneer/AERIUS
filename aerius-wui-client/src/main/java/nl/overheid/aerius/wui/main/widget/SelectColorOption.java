/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.util.ColorUtil;

public class SelectColorOption extends Composite implements SelectOption<String>, ClickHandler {
  private boolean selected;

  interface MultiSelectColorOptionUiBinder extends UiBinder<Widget, SelectColorOption> {}

  private static final MultiSelectColorOptionUiBinder UI_BINDER = GWT.create(MultiSelectColorOptionUiBinder.class);

  public interface CustomStyle extends CssResource {
    String selected();
  }

  @UiField CustomStyle style;

  @UiField SimplePanel color;
  @UiField SimplePanel inner;

  public SelectColorOption(final String color) {
    initWidget(UI_BINDER.createAndBindUi(this));
    inner.getElement().getStyle().setBackgroundColor(ColorUtil.webColor(color));

    addDomHandler(this, ClickEvent.getType());
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public String getItemText(final String item) {
    return item;
  }

  @Override
  public void setSelected(final boolean select, final boolean fireEvents) {
    selected = select;

    color.setStyleName(style.selected(), selected);

    if (fireEvents) {
      ValueChangeEvent.fire(this, select);
    }
  }

  @Override
  public void onClick(final ClickEvent event) {
    setSelected(!selected, true);
  }
}
