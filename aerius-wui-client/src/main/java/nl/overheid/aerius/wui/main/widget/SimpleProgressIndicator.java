/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public abstract class SimpleProgressIndicator<T> extends Composite implements ProgressIndicator<T> {
  interface SimpleProgressIndicatorUiBinder extends UiBinder<Widget, SimpleProgressIndicator<?>> {}

  private static final SimpleProgressIndicatorUiBinder UI_BINDER = GWT.create(SimpleProgressIndicatorUiBinder.class);

  private final WidgetFactory<T, SimpleProgressStep<T>> factory;

  @UiField SimplePanel line;
  @UiField FlowPanel container;

  public SimpleProgressIndicator() {
    factory = new WidgetFactory<T, SimpleProgressStep<T>>() {
      @Override
      public SimpleProgressStep<T> createWidget(final T object) {
        final String text = getStepText(object);
        return text == null ? null : new SimpleProgressStep<T>(object, text);
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  public void setSteps(final T[] steps) {
    container.clear();
    for (final T step : steps) {
      final SimpleProgressStep<T> widget = factory.createWidget(step);

      if (widget != null) {
        container.add(widget);
      }
    }

    alignLine();
  }

  @Override
  protected void onLoad() {
    super.onLoad();

    // It doesn't make any sense that we have to do this, but we do..
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        alignLine();
      }
    });
  }

  private void alignLine() {
    final int offsetWidth = container.getElement().getOffsetWidth();

    final int lineWidth = offsetWidth - offsetWidth / container.getWidgetCount();

    line.getElement().getStyle().setWidth(lineWidth, Unit.PX);
    line.getElement().getStyle().setMarginLeft(-lineWidth / 2, Unit.PX);
  }

  @Override
  public void setStepState(final T obj) {
    boolean activeFound = false;

    for (int i = 0; i < container.getWidgetCount(); i++) {
      @SuppressWarnings("unchecked")
      final SimpleProgressStep<T> step = (SimpleProgressStep<T>) container.getWidget(i);

      if (step.isActive(obj)) {
        step.setActive();
        activeFound = true;
      } else if (activeFound) {
        step.setStepNotYetDone();
      } else {
        step.setStepDone();
      }
    }
  }

  protected abstract String getStepText(final T object);

}
