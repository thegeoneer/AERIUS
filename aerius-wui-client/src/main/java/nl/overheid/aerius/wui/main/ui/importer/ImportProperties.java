/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.importer;


/**
 * Data object for supporting Editor of {@link ImportDialogPanel}.
 */
public class ImportProperties {

  private int substanceId;
  private String fileName;

  public int getSubstanceId() {
    return substanceId;
  }
  public String getFileName() {
    return fileName;
  }
  public void setSubstanceId(final int substanceId) {
    this.substanceId = substanceId;
  }
  public void setFileName(final String fileName) {
    this.fileName = fileName;
  }
}
