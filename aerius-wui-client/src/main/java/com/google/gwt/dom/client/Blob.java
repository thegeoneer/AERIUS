/*
 * Copyright 2014 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.google.gwt.dom.client;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Class representing the Blob interface.
 *
 * <p>
 * <span style="color:red">Experimental API: This API is still under development
 * and is subject to change. </span>
 * </p>
 *
 * @see <a
 *      href="http://www.w3.org/TR/FileAPI/#blob">W3C
 *      HTML Specification</a>
 */
public class Blob extends JavaScriptObject {

  /**
   * Required constructor for GWT compiler to function.
   */
  protected Blob() {
  }

  /**
   * Returns the size of the Blob object in bytes. On getting, conforming user
   * agents return the total number of bytes that can be read by a FileReader
   * or FileReaderSync object, or 0 if the Blob has no bytes to be read. If
   * the Blob has been neutered with close called on it, then size returns 0.
   *
   * @return the size of the blob object
   * @see <a
   *      href="http://www.w3.org/TR/FileAPI/#dfn-size">W3C
   *      HTML Specification</a>
   */
  public final native int getSize() /*-{
    return this.size;
  }-*/;

  /**
   * The slice method returns a new Blob object with bytes ranging from the
   * optional start parameter upto but not including the optional end
   * parameter, and with a type attribute that is the value of the optional
   * contentType parameter.
   *
   * @param start
   * @param end
   * @return
   * @see <a
   *      href="http://www.w3.org/TR/FileAPI/#dfn-size">W3C
   *      HTML Specification</a>
   */
  public final native JavaScriptObject slice(final int start, final int end) /*-{
    return this.slice(start, end);
  }-*/;

}
