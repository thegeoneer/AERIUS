/*
 * Copyright 2014 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.google.gwt.dom.client;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Class representing FileList interface.
 *
 * <p>
 * <span style="color:red">Experimental API: This API is still under development
 * and is subject to change. </span>
 * </p>
 *
 * @see <a
 *      href="http://www.w3.org/TR/FileAPI/#dfn-filelist">W3C
 *      HTML Specification</a>
 */
public class FileList extends JavaScriptObject {

  /**
   * Required constructor for GWT compiler to function.
   */
  protected FileList() {
  }

  /**
   * Returns the number of files in the FileList object. If there are no files,
   * this attribute returns 0.
   *
   * @return the number of files
   * @see <a
   *      href="http://www.w3.org/TR/FileAPI/#dfn-length">W3C
   *      Specification</a>
   */
  public final native int length() /*-{
    return this.length;
  }-*/;

 /**
  * Returns the indexth File object in the FileList. If there is no indexth
  * File object in the FileList, then this method returns null.
  *
  * @param the index of the file to return
  * @return the file or null if no file
  * @see <a
  *      href="http://www.w3.org/TR/FileAPI/#dfn-item">W3C
  *      Specification</a>
  */
  public final native File getItem(int index) /*-{
    return this[index];
  }-*/;
}
