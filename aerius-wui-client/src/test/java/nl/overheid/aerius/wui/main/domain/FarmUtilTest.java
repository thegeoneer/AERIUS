/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gwtmockito.GwtMockitoTestRunner;

import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Unit test for {@link FarmUtil} class.
 */
@RunWith(GwtMockitoTestRunner.class)
public class FarmUtilTest {

  @Test
  public void testGetFarmImageResource() {
    final FarmEmissionSource source = new FarmEmissionSource();

    assertEquals("No lodgings", R.images().iconSectorAnimalMixed().getSafeUri(), FarmUtil.getFarmImageResource(source).getSafeUri());

    final FarmLodgingStandardEmissions lodging1 = new FarmLodgingStandardEmissions();
    lodging1.setCategory(new FarmLodgingCategory(1, "A1", "A1", "A1", 1.0, null, null, false));
    lodging1.setAmount(1);
    source.getEmissionSubSources().add(lodging1);
    assertEquals("Cow Lodging", R.images().iconSectorAnimalCow().getSafeUri(), FarmUtil.getFarmImageResource(source).getSafeUri());

    final FarmLodgingStandardEmissions lodging2 = new FarmLodgingStandardEmissions();
    lodging2.setCategory(new FarmLodgingCategory(1, "B1", "B1", "B1", 1.0, null, null, false));
    lodging2.setAmount(1);
    source.getEmissionSubSources().add(lodging2);
    assertEquals("Mixed Lodgings", R.images().iconSectorAnimalMixed().getSafeUri(), FarmUtil.getFarmImageResource(source).getSafeUri());
  }
}
