/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
//package nl.overheid.aerius.wui.main.ui.source;
//
//import static org.mockito.Mockito.verify;
//import nl.overheid.aerius.shared.domain.Substance;
//import nl.overheid.aerius.shared.domain.source.GenericEmissionsValues;
//import nl.overheid.aerius.wui.main.widget.HelpPopupController;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//
//import com.google.gwtmockito.GwtMockitoTestRunner;
//
///**
// * Test for class {@link EmissionValuesEditor}.
// */
//@RunWith(GwtMockitoTestRunner.class)
//public class EmissionValuesEditorTest {
//  @Mock HelpPopupController hpc;
//
//  private GenericEmissionsEditor gEditor;
//  private GenericEmissionsValues gValues;
//
//  @Before
//  public void setUp() {
//    gEditor = new GenericEmissionsEditor(hpc);
//    gValues = new GenericEmissionsValues();
//  }
//
//  @Test
//  public void genericEmissionTest() {
//    gEditor.setValue(gValues);
//
//    gValues.setEmission(Substance.NH3, 5);
//    verify(gEditor.emissionNH3).setText("5.00");
//  }
//}
