/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.google.gwt.i18n.client.Messages.DefaultMessage;

/**
 * Base class for testing if the messages interface is in sync with the properties file.
 */
@RunWith(Parameterized.class)
public abstract class MessagesTestBase {
  private static final String[] LANGUAGES = {"", "_en", };
  private static final Pattern PROPERTY_KEY = Pattern.compile("([^ \\[=]+)(.)");
  private Set<String> properties;
  private Set<String> methods;
  private final String langPropFilename;
  private final Class<?> clazz;
  private Set<String> duplicates;

  protected MessagesTestBase(final Class<?> clazz, final String languagePostFix) {
    this.clazz = clazz;
    this.langPropFilename = clazz.getSimpleName() + languagePostFix;
  }

  @Parameters(name = "Property file language: {0}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> list = new ArrayList<>();
    for (final String lang: LANGUAGES) {
      final String[] obj = new String[1];
      obj[0] = lang;
      list.add(obj);
    }
    return list;
  }

  @Before
  public void before() throws IOException {
    duplicates = new TreeSet<>();
    properties = getKeysFromProperties(langPropFilename);
    methods = getMethods(clazz);
  }

  private Set<String> getMethods(final Class<?> clazz) {
    final Set<String> methods = new TreeSet<>();
    addMethods(methods, clazz);
    for (final Class<?> clz : clazz.getInterfaces()) {
      addMethods(methods, clz);
    }
    return methods;
  }

  private void addMethods(final Set<String> methods, final Class<?> clazz) {
    for (final Method method : clazz.getDeclaredMethods()) {
      if (!method.isAnnotationPresent(DefaultMessage.class)) {
        methods.add(method.getName());
      }
    }
  }

  @Test
  public void testPropertiesFileInInterface() throws IOException {
    final List<String> missing = findMissing(properties, methods);
    assertTrue("Dead properties #" + missing.size() + ":" + String.join(", ", missing), missing.isEmpty());
  }

  @Test
  public void testInterfaceInPropertiesFile() throws IOException {
    final List<String> missing = findMissing(methods, properties);
    assertTrue("Missing properties #" + missing.size() + ":" + String.join(", ", missing), missing.isEmpty());
  }

  @Test
  public void testDuplicatesInPropertiesFile() throws IOException {
    assertTrue("Duplicate properties #" + duplicates.size() + ":" + String.join(", ", duplicates), duplicates.isEmpty());
  }

  private List<String> findMissing(final Set<String> input, final Set<String> checkSet) {
    final List<String> missing = new ArrayList<>();
    for (final String key : input) {
      if (!checkSet.contains(key)) {
        missing.add(key);
      }
    }
    return missing;
  }

  private Set<String> getKeysFromProperties(final String filenName) throws IOException {
    try (final InputStream pStream = getClass().getResourceAsStream(filenName + ".properties");
        final InputStreamReader is = new InputStreamReader(pStream);
        final LineNumberReader reader = new LineNumberReader(is)) {
      String line;
      final Set<String> keys = new TreeSet<>();
      while ((line = reader.readLine()) != null) {
        final Matcher matcher = PROPERTY_KEY.matcher(line);
        if (matcher.find()) {
          final String key = matcher.group(1);
          //Don't change the order, due to side effects. First check on [. Only if it succeeds the key is added to the list keys.
          if (!"[".equals(matcher.group(2)) && !keys.add(key)) {
            duplicates.add(key);
          }
        }
      }
      return keys;
    }
  }
}
