/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationInfoRepository;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationInitResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.calculation.ResearchAreaCalculationScenario;
import nl.overheid.aerius.shared.domain.calculation.ResultHighValuesByDistances;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;

/**
 * Test class for {@link CalculationWorker}.
 */
public class CalculationWorkerTest extends BaseDBWorkerTest<CalculationWorker> {

  private static final int MAX_RECEPTORS = 1000;

  private MockResultSender mockResultSender;

  @Override
  @Before
  public void setUp() throws Exception {
    //worker uses connections on different threads, which doesn't really work with a cached connection which is used for all those threads...
    getCalcPMF().setAutoCommit(true);
    super.setUp();
  }

  @Override
  @After
  public void tearDown() throws Exception {
    if (mockResultSender != null && mockResultSender.received.get(0) instanceof CalculationInitResult) {
      //clean up any calculations
      for (final Calculation calculation : ((CalculationInitResult) mockResultSender.received.get(0)).getCalculations()) {
        CalculationRepository.removeCalculation(getCalcConnection(), calculation.getCalculationId());
      }
    }
    super.tearDown();
  }

  @Override
  protected CalculationWorker createWorker() throws IOException, SQLException {
    initCalculator(getCalcPMF(), factory);
    return new CalculationWorker(calculatorBuildDirector, getCalcPMF());
  }

  @Test
  public void testRunSingle() throws Exception {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setScenario(getExampleSingleScenario());
    inputData.setQueueName("matters_not");
    inputData.getAdditionalOptions().add(ExportAdditionalOptions.RETURN_CALCULATION_SUMMARY);

    mockResultSender = new MockResultSender();

    final CalculationSummary summary = worker.run(inputData, mockResultSender, null);

    assertResults(summary, 1);
  }

  @Test
  public void testRunComparison() throws Exception {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setScenario(getExampleComparisonScenario());
    inputData.setQueueName("matters_not");
    inputData.getAdditionalOptions().add(ExportAdditionalOptions.RETURN_CALCULATION_SUMMARY);

    mockResultSender = new MockResultSender();

    final CalculationSummary summary = worker.run(inputData, mockResultSender, null);

    assertResults(summary, 2);
  }

  @Test
  public void testSectorResults() throws Exception {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setScenario(getExampleSingleScenario());
    inputData.setQueueName("matters_not");
    inputData.getAdditionalOptions().add(ExportAdditionalOptions.RETURN_CALCULATION_SUMMARY);
    inputData.setExportType(ExportType.GML_WITH_SECTORS_RESULTS);

    mockResultSender = new MockResultSender();

    worker.run(inputData, mockResultSender, null);

    // get the sector results for default sector
    final CalculationInitResult sectorResult = (CalculationInitResult) mockResultSender.received.get(0);
    final int id = sectorResult.getCalculations().get(0).getCalculationId();
    final ArrayList<AeriusPoint> receptorPoints = getReceptors(getCalcPMF(), id, inputData.getScenario(), Sector.SECTOR_DEFAULT);

    assertNotNull("Sector result retrieved", receptorPoints);
    assertFalse("Calculations should not be empty", receptorPoints.isEmpty());

    for (final AeriusPoint point : receptorPoints) {
      final AeriusResultPoint p = (AeriusResultPoint) point;
      assertFalse("Sector emission shoud not be empty", p.getEmissionResults().isEmpty());
    }
  }

  private static ArrayList<AeriusPoint> getReceptors(final PMF pmf, final Integer calculationId,
      final CalculatedScenario scenario, final Sector sector) throws SQLException {
    final ArrayList<AeriusPoint> receptors = new ArrayList<>();
    if (calculationId == null || calculationId == 0) {
      receptors.addAll(scenario.getCalculationPoints());
    } else {
      //no need for validating calculation, if not available no results will be found anyway.
      //(or should we take that as an exception?)
      try (final Connection connection = pmf.getConnection()) {
        final PartialCalculationResult result =
            CalculationInfoRepository.getCalculationSectorResults(connection, calculationId, sector.getSectorId());
        receptors.addAll(result.getResults());
      }
    }
    return receptors;
  }

  private void assertResults(final CalculationSummary summary, final int nrOfCalculationsExpected) {
    assertNotNull("Summary retrieved", summary);
    assertFalse("Intermediate should not be empty", mockResultSender.received.isEmpty());
    assertTrue("First result should be an init result", mockResultSender.received.get(0) instanceof CalculationInitResult);
    final CalculationInitResult initResult = (CalculationInitResult) mockResultSender.received.get(0);
    assertEquals("Size of calculations", nrOfCalculationsExpected, initResult.getCalculations().size());
    final Set<Integer> calculationIds = new HashSet<>();
    for (final Calculation calc : initResult.getCalculations()) {
      calculationIds.add(calc.getCalculationId());
    }
    int receptorsReceived = 0;
    int highValuesByDistancesReceived = 0;
    for (final Serializable result : mockResultSender.received.subList(1, mockResultSender.received.size())) {
      if (result instanceof ResultHighValuesByDistances) {
        highValuesByDistancesReceived++;
      } else if (result instanceof PartialCalculationResult) {
        final PartialCalculationResult partialResult = (PartialCalculationResult) result;
        assertTrue("Calculation ID should match", calculationIds.contains(partialResult.getCalculationId()));
        receptorsReceived += partialResult.getResults().size();
      } else {
        fail("Unexpected result received: " + result.getClass());
      }
    }
    assertNotEquals("Nr of receptors should not be empty", 0, receptorsReceived);
    assertNotEquals("Should have received some updates for the nice little graph", 0, highValuesByDistancesReceived);
  }

  private CalculatorOptions getExampleOptions() {
    final CalculatorOptions options = new CalculatorOptions();
    options.setMaxCalculationEngineUnits(100000);
    options.setMaxChunkDelay(1000);
    options.setMaxReceptors(MAX_RECEPTORS);
    options.setMinReceptors(1);
    options.setMaxConcurrentChunks(8);
    options.setMinReceptorsChunkDelay(200);
    return options;
  }

  private CalculatedScenario getExampleSingleScenario() {
    final CalculatedSingle scenario = new CalculatedSingle();
    scenario.setSources(getExampleSourceList(1.0));
    scenario.setYear(TestDomain.YEAR);
    scenario.setOptions(getExampleCalculationSetOptions());
    return scenario;
  }

  private CalculatedScenario getExampleComparisonScenario() {
    final CalculatedComparison scenario = new CalculatedComparison();
    scenario.setSourcesOne(getExampleSourceList(1.0));
    scenario.setSourcesTwo(getExampleSourceList(2.0));
    scenario.setYear(TestDomain.YEAR);
    scenario.setOptions(getExampleCalculationSetOptions());
    return scenario;
  }

  private CalculatedScenario getExampleResearchAreaScenario() {
    final ResearchAreaCalculationScenario scenario = new ResearchAreaCalculationScenario();
    scenario.setResearchAreaSources(getExampleSourceList(1.0));
    final EmissionSourceList actualCalculationSources = new EmissionSourceList();
    actualCalculationSources.add(getExampleSourceList(1.0).get(0));
    final WKTGeometry geometrySourceExtra = new WKTGeometry("POINT(" + (TestDomain.XCOORD_1 + 1000) + " " + (TestDomain.YCOORD_1 + 1000) + ")", 1);
    final GenericEmissionSource emissionSource = new GenericEmissionSource();
    emissionSource.setEmission(Substance.NH3, 20.0);
    emissionSource.setEmission(Substance.NOX, 20.0);
    actualCalculationSources.add(TestDomain.getSource(2, geometrySourceExtra, "SomeExtraSource", emissionSource));
    scenario.setSources(actualCalculationSources);
    scenario.setYear(TestDomain.YEAR);
    scenario.setOptions(getExampleCalculationSetOptions());
    return scenario;
  }

  private EmissionSourceList getExampleSourceList(final double emissionFactor) {
    final EmissionSourceList sourceList = new EmissionSourceList();
    final WKTGeometry geometrySource1 = new WKTGeometry("POINT(" + TestDomain.XCOORD_1 + " " + TestDomain.YCOORD_1 + ")", 1);
    final GenericEmissionSource emissionSource = new GenericEmissionSource();
    emissionSource.setEmission(Substance.NH3, 10.0 * emissionFactor);
    emissionSource.setEmission(Substance.NOX, 20.0 * emissionFactor);
    sourceList.add(TestDomain.getSource(1, geometrySource1, "SomeSource", emissionSource));
    return sourceList;
  }

  private CalculationSetOptions getExampleCalculationSetOptions() {
    final CalculationSetOptions options = new CalculationSetOptions();
    options.setCalculationType(CalculationType.PAS);
    options.setCalculateMaximumRange(3000);
    options.getSubstances().add(Substance.NOXNH3);
    options.getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
    return options;
  }

  private static class MockResultSender implements WorkerIntermediateResultSender {

    private final List<Serializable> received = new ArrayList<>();

    @Override
    public void sendIntermediateResult(final Serializable result) throws IOException {
      received.add(result);
    }

  }
}
