/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

/**
 *
 */
public class ScheduleUtilsTest {

  @Test
  public void testDetermineStartTimeMidnight() throws ParseException {
    final Date date = ScheduleUtils.determineStartTime("00:00");
    final Calendar expectedDate = Calendar.getInstance();
    expectedDate.set(Calendar.HOUR_OF_DAY, 0);
    expectedDate.set(Calendar.MINUTE, 0);
    clearUnusedFields(expectedDate);
    expectedDate.add(Calendar.DATE, 1);
    assertEquals("Expected start date should be tomorrow 00:00", expectedDate.getTime(), date);
  }

  @Test
  public void testDetermineStartTimeMinuteToMidnight() throws ParseException {
    final Date date = ScheduleUtils.determineStartTime("23:59");
    final Calendar expectedDate = Calendar.getInstance();
    expectedDate.set(Calendar.HOUR_OF_DAY, 23);
    expectedDate.set(Calendar.MINUTE, 59);
    clearUnusedFields(expectedDate);
    assertEquals("Expected start date should be today 23:59", expectedDate.getTime(), date);
  }

  @Test
  public void testDetermineStartTimeAroundNoon() throws ParseException {
    final Date date = ScheduleUtils.determineStartTime("12:30");
    final Calendar expectedDate = Calendar.getInstance();
    expectedDate.set(Calendar.HOUR_OF_DAY, 12);
    expectedDate.set(Calendar.MINUTE, 30);
    clearUnusedFields(expectedDate);
    if (Calendar.getInstance().after(expectedDate)) {
      expectedDate.add(Calendar.DATE, 1);
    }
    assertEquals("Expected start date should be today 12:30", expectedDate.getTime(), date);
  }

  @Test
  public void testDetermineStartTimeBeforeNow() throws ParseException {
    final Calendar expectedDate = Calendar.getInstance();
    expectedDate.add(Calendar.MINUTE, -1);
    expectedDate.add(Calendar.DATE, 1);
    final Date date = ScheduleUtils.determineStartTime(
        expectedDate.get(Calendar.HOUR_OF_DAY) + ":" + expectedDate.get(Calendar.MINUTE));
    clearUnusedFields(expectedDate);

    assertEquals("Expected start date if time before now", expectedDate.getTime(), date);
  }

  @Test
  public void testDetermineStartTimeAfterNow() throws ParseException {
    final Calendar expectedDate = Calendar.getInstance();
    expectedDate.add(Calendar.MINUTE, 1);
    final Date date = ScheduleUtils.determineStartTime(
        expectedDate.get(Calendar.HOUR_OF_DAY) + ":" + expectedDate.get(Calendar.MINUTE));
    clearUnusedFields(expectedDate);

    assertEquals("Expected start date if time after now", expectedDate.getTime(), date);
  }

  @Test(expected = ParseException.class)
  public void testDetermineStartTimeBogusTime() throws ParseException {
    ScheduleUtils.determineStartTime("23h01");
  }

  private void clearUnusedFields(final Calendar calendar) {
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
  }

}
