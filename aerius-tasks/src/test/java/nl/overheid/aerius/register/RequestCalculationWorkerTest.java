/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.OPSMockConnection;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.db.register.PermitRepository;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.OSUtils;
import nl.overheid.aerius.worker.PropertiesUtil;
import nl.overheid.aerius.worker.configuration.BrokerConfiguration;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * Test class for {@link RequestCalculationWorker}.
 */
public class RequestCalculationWorkerTest extends AbstractRegisterWorkerTest {

  private BrokerConnectionFactory factory;
  private RequestCalculationWorker worker;

  @Override
  @Before
  public void setUp() throws Exception {
    final Properties properties = OSUtils.isWindows() ? PropertiesUtil.getFromPropertyFile("worker.properties")
        : PropertiesUtil.getFromPropertyFile("worker_test_linux.properties");
    factory = new BrokerConnectionFactory(EXECUTOR, new BrokerConfiguration(properties)) {
      @Override
      protected com.rabbitmq.client.Connection createNewConnection() throws IOException {
        return new OPSMockConnection(1.0, 0.6);
      }
    };
    worker = new RequestCalculationWorker(new DBWorkerConfiguration(getProperties(), ProductType.REGISTER, WorkerType.REGISTER), factory,
        SegmentType.PROJECTS) {
      @Override
      protected CalculatorBuildDirector createCalculatorBuildDirector(final BrokerConnectionFactory factory) throws SQLException {
        return new CalculatorBuildDirector(registerPMF, factory, true) {
          @Override
          protected CalculatorOptions getCalculatorOptions(final Connection con, final boolean calculatorOptionsUI) throws SQLException {
            final CalculatorOptions options = super.getCalculatorOptions(con, calculatorOptionsUI);

            //normally we'd use big chunks as we'd expect to calculate quite some areas. That takes aaages however.
            //So now we're abusing the OPSMockConnection: By using small chunks, the results go down pretty fast (due to degradationFactor used).
            options.setMaxReceptors(100);
            options.setMaxConcurrentChunks(1);
            return options;
          }
        };
      }
    };
    //testPMF default returns connections with autocommit = false.
    //these test expect connections with autocommit = true however (using multiple connections), so...
    getRegPMF().setAutoCommit(true);
  }

  @Override
  @After
  public void tearDown() throws Exception {
    super.tearDown();
    factory.shutdown();
  }

  @Test(timeout = 60000)
  public void testRun() throws Exception {
    // Test if worker run method works, it should not find any requests, or if it does, it's no problem.
    // The shutdown should make sure the run stops after processing or not processing the request.
    EXECUTOR.execute(worker);
    Thread.sleep(TimeUnit.SECONDS.toMillis(1));
    assertFalse("Worker not shutdown correctly", worker.shutDown());
  }

  @Override
  protected void runActualTest(final Permit permit) throws Exception {
    worker.processRequest(permit);
  }

  /**
   * Test if permit actually calculated.
   * @param permit
   * @throws SQLException
   */
  @Override
  protected void assertPermit(final Permit permit) throws SQLException {
    try (Connection con = getRegConnection()) {
      assertSame("State should be queued", RequestState.QUEUED, PermitRepository.getSkinnedPermitByKey(con, permit.getPermitKey()).getRequestState());
    }
  }

}
