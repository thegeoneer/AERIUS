/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.lowagie.text.FontFactory;

import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.mock.MockCloseableHttpClientUtil;
import nl.overheid.aerius.paa.PAARunnerTest;
import nl.overheid.aerius.register.RequestDecreeGeneratorWorker.PermitDecreeGenerator;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SituationType;

/**
 *
 */
@RunWith(Parameterized.class)
public class PermitDecreeGeneratorWorkerTest extends AbstractRegisterWorkerTest {

  private PermitDecreeGenerator generator;

  private final RequestFileType fileType;

  public PermitDecreeGeneratorWorkerTest(final RequestFileType fileType) {
    this.fileType = fileType;
  }

  @Parameters(name = "{0}")
  public static List<Object[]> data() {
    final List<Object[]> data = new ArrayList<>();
    data.add(new Object[] {RequestFileType.DECREE});
    data.add(new Object[] {RequestFileType.DETAIL_DECREE});
    return data;
  }

  @Override
  @Before
  public void setUp() throws Exception {
    generator = new PermitDecreeGenerator(getRegPMF(), fileType) {

      @Override
      protected CloseableHttpClient createHttpClient() {
        return MockCloseableHttpClientUtil.create();
      }

    };
    //testPMF default returns connections with autocommit = false.
    //these test expect connections with autocommit = true however (using multiple connections), so...
    getRegPMF().setAutoCommit(true);
  }

  @BeforeClass
  public static void beforeClass() {
    // Load fonts to be used for PDF's // temporary for testing in main - is normally done when starting tasks
    FontFactory.registerDirectory(PAARunnerTest.class.getResource("/fonts/").getFile());
  }

  @Override
  protected void runActualTest(final Permit permit) throws Exception {
    //add calculations (doesn't matter much if we add 1 or 2).
    addCalculation(permit, 1.0, SituationType.CURRENT);
    addCalculation(permit, 1.2, SituationType.PROPOSED);
    permit.setRequestState(RequestState.ASSIGNED_FINAL);
    generator.process(permit);
  }

  @Override
  protected void assertPermit(final Permit permit) throws SQLException {
    try (Connection con = getRegConnection()) {
      assertNotNull("Expected file in DB", RequestRepository.getRequestFileContent(con, permit.getReference(), fileType));
      assertNull("Other possible file in DB", RequestRepository.getRequestFileContent(con, permit.getReference(),
          fileType == RequestFileType.DECREE ? RequestFileType.DETAIL_DECREE : RequestFileType.DECREE));
    }
  }

}
