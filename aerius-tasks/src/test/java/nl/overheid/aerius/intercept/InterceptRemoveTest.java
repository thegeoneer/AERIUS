/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

import java.io.IOException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.rabbitmq.client.AMQP.Queue;
import com.rabbitmq.client.AMQP.Queue.DeleteOk;
import com.rabbitmq.client.GetResponse;

import nl.overheid.aerius.intercept.InterceptRemove.InterceptRemoveCommand;

/**
 * Test class for {@link InterceptRemove}.
 */
public class InterceptRemoveTest extends InterceptTestBase {

  @Test
  public void testRemove() throws Exception {
    final String result = "result";
    final String inQueue = "inQueue";
    final InterceptRemove remove = new InterceptRemove(new InterceptRemoveCommand(new String[] {inQueue, getTmpPath()}));
    mockConsume(result, inQueue);
    final AtomicBoolean calledAck = mockAck(true);
    remove.remove(factory);
    assertTrue("Ack should be called in remove", calledAck.get());
  }

  @Test(expected = NullPointerException.class)
  public void testRemoveEmptyQueue() throws Exception {
    final String result = "result";
    final String inQueue = "intercept.inQueue";
    final InterceptRemove remove = new InterceptRemove(new InterceptRemoveCommand(new String[] {inQueue, getTmpPath()}));
    mockExceptionConsume(result, inQueue);
    final Semaphore lock = new Semaphore(0);
    Mockito.when(channel.queueDelete(anyString())).thenAnswer(new Answer<Queue.DeleteOk>() {
      @Override
      public DeleteOk answer(final InvocationOnMock invocation) throws Throwable {
        lock.release();
        return null;
      }
    });
    try {
      remove.remove(factory);
      fail("Should have a NullPointerException");
    } catch (final NullPointerException e) {
      assertTrue("Test intercept remove possible in deadlock, lock time elapsed", lock.tryAcquire(1, TimeUnit.SECONDS));
      throw e;
    }
  }

  private Semaphore mockExceptionConsume(final String result, final String inQueue) throws IOException {
    Mockito.when(channel.basicGet(eq(inQueue), anyBoolean())).then(new Answer<GetResponse>() {
      @Override
      public GetResponse answer(final InvocationOnMock invocation) throws Throwable {
        throw new NullPointerException();
      }
    });
    return null;
  }
}
