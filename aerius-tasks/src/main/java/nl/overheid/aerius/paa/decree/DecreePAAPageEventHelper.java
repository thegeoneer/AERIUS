/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.decree;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.paa.base.PAAFrontPage;
import nl.overheid.aerius.paa.base.PAAPageEventHelper;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAAFlowState;
import nl.overheid.aerius.shared.domain.register.RequestState;


/**
 *
 */
class DecreePAAPageEventHelper extends PAAPageEventHelper {

  private final RequestState requestState;

  DecreePAAPageEventHelper(final PAAFlowState flowState, final DecreePAAContext paaContext) {
    super(flowState, paaContext);
    this.requestState = paaContext.getDecreeInfo().getRequestState();
  }

  @Override
  protected PAAFrontPage getFrontPage(final PAAContext paaContext) {
    return new DecreePAAFrontPage((DecreePAAContext) paaContext);
  }

  @Override
  protected String getProductText() {
    return MessageFormat.format(super.getProductText(), paaContext.getText("requestState", requestState));
  }

  @Override
  protected String getRemainenFooterTextFirstLine() {
    final String projectName = paaContext.getMetaData().getProjectName();

    return StringUtils.isEmpty(projectName) ? "-" : projectName;
  }

  @Override
  protected String getRemainenFooterTextSecondLine() {
    return null;
  }

}
