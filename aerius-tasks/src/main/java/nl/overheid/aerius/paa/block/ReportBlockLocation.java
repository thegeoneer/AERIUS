/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.awt.image.BufferedImage;
import java.util.List;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAALayerUtil;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAASourcesUtil;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.PDFImageLayer;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * PAA Report Block: Location.
 */
public class ReportBlockLocation extends PAAReportBlock<PAAContext> {

  private static final int OVERVIEW_BBOX_WIDTH = 5000; // in meters (RDNEW)
  private static final int OVERVIEWIMAGE_HEIGHT = 320;
  private static final int OVERVIEWIMAGE_WIDTH = 420;

  private static final int PADDING_AFTER_SOURCE_LIMIT_TEXT = 10;

  private final EmissionSourceList sources;

  public ReportBlockLocation(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext, final EmissionSourceList sources) {
    super(pdfDocument, paaContext);

    this.sources = sources;
  }

  @Override
  public void generate() throws DocumentException, AeriusException {
    if (!sources.isEmpty()) {
      final PDFSimpleTable locationTable = PAAUtil.getDefaultBlockEmptyTable(false);
      PAAUtil.addBlockHeaderCellWithSubHeader(locationTable, paaContext.getText("location_title"), sources.getName());
      if (sources.size() > PAASharedConstants.CLUSTERED_SOURCES_IMAGES_MAX_SOURCES) {
        locationTable.add(paaContext.getText("clustered_sources_image_source_limit_reached"), PAASharedFonts.DEFAULT_TEXT);

        // add some extra padding at the bottom of the table to make it look decent
        locationTable.addEmptyCell();
        locationTable.addEmptyCell(PADDING_AFTER_SOURCE_LIMIT_TEXT);
      } else {
        final BBox bboxAllSources = PAASourcesUtil.getBBoxAllSources(sources);
        final BBox bboxOverview = GeometryUtil.ensureMinimumWidthHeight(bboxAllSources, OVERVIEW_BBOX_WIDTH, 0);

        final BufferedImage imageToAdd = getLocationImage(bboxOverview, OVERVIEWIMAGE_WIDTH, OVERVIEWIMAGE_HEIGHT);

        locationTable.addImage(imageToAdd);
      }

      pdfDocument.addElement(locationTable);
    }
  }

  private BufferedImage getLocationImage(final BBox bbox, final int width, final int height) throws AeriusException {
    final List<PDFImageLayer> layers = PAALayerUtil.getLayersForSources(bbox, width, height, sources, null, false,
        PAASharedConstants.MARKER_THRESHOLD);

    return PAALayerUtil.getFeaturedImage(paaContext, bbox, width, height, layers.toArray(new PDFImageLayer[layers.size()]));
  }

}
