/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;

import nl.overheid.aerius.pdf.table.PDFRelativeWidths;

/**
 * Contains various shared constants as used by PAA.
 */
public final class PAASharedConstants {
  /** The directory where all the PAA shared resources are stored. */
  public static final String RESOURCES_DIR = "/nl/overheid/aerius/paa/shared/";

  /** Resolution of the images that are generated are multiplied by this before it's added to the PDF (after it's scaled). */
  public static final float QUALITY_SETTING = 2.4f;

  /** Default leading. */
  public static final float DEFAULT_LEADING = 1.3f;

  /** The page size used by the reports. */
  public static final Rectangle PAGESIZE = PageSize.A4;

  /** Color used for almost all text. */
  public static final String DEFAULT_COLOR = "114273"; // Dark blue-ish

  /** Color used when the background is similar to the default color or the default color doesn't work well with the text. */
  public static final String ALTERNATIVE_COLOR = "FFFFFF"; // White

  /** Color used when we are bored with the default color and want to add more color so to the text and the design looks good. */
  public static final String ADDITIONAL_COLOR = "818284"; // Gray

  /** The text to use on empty cells. */
  public static final String DEFAULT_EMPTY_CELL_STRING = "-";

  /** Default cells get this padding top. */
  public static final float DEFAULT_CELL_PADDING_TOP = 7;
  /** Default cells get this padding left. */
  public static final float DEFAULT_CELL_PADDING_LEFT = 7;

  /** Default relative widths as used by two-cell empty blocks. */
  public static final PDFRelativeWidths DEFAULT_RELATIVE_WIDTH_TWOCELL_EMPTY_BLOCK = new PDFRelativeWidths(8, 35);

  /** Marker threshold. */
  public static final int MARKER_THRESHOLD = 12;

  /** Contains the limit after which we will stop showing clustered source images. */
  public static final int CLUSTERED_SOURCES_IMAGES_MAX_SOURCES = 250;

  private PAASharedConstants() {
    // Default constructor utility class.
  }

}
