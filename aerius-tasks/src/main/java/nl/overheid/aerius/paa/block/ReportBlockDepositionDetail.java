/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;

import nl.overheid.aerius.db.common.PAARepository;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAALayerUtil;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.PDFImageLayer;
import nl.overheid.aerius.pdf.font.PDFFonts;
import nl.overheid.aerius.pdf.table.PDFRelativeWidths;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.pdf.table.Padding;
import nl.overheid.aerius.shared.domain.Range;
import nl.overheid.aerius.shared.domain.deposition.ComparisonEnum;
import nl.overheid.aerius.shared.domain.result.EmissionResultColorRanges;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.worker.util.ImageUtils;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 *
 */
public class ReportBlockDepositionDetail extends PAAReportBlock<PAAContext> {

  private static final int INDEX_ICON_POSITION_X = 25;
  private static final int INDEX_ICON_POSITION_Y = 25;
  private static final double INDEX_ICON_SCALE = 0.3;

  private static final int OVERVIEW_IMAGE_WIDTH = 420;
  private static final int OVERVIEW_IMAGE_HEIGHT = 500;

  private static final int DETAIL_IMAGE_WIDTH = 500;
  private static final int DETAIL_IMAGE_HEIGHT = 650;

  private static final int DETAIL_IMAGE_LEFT_MARGIN = 20;

  private static final int DEFAULT_BOUNDING_BOX_WIDTH = 4455;
  private static final int DEFAULT_BOUNDING_BOX_HEIGHT = 5985;

  private static final int LEGEND_TITLE_PADDING = 7;
  private static final int LEGEND_ICON_HEIGHT = 10;

  private static final PDFRelativeWidths LEGEND_RELATIVE_WIDTHS = new PDFRelativeWidths(2, 10, 2, 10, 2, 10);

  private static final double BOUNDING_BOX_PADDING = 0.05;

  public ReportBlockDepositionDetail(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  public void generate() throws AeriusException, DocumentException, IOException, SQLException {
    //determine the bounding boxes to show
    List<BBox> boundingBoxes;
    try (final Connection con = getPmf().getConnection()) {
      boundingBoxes = PAARepository.getDetailPagesExtents(con,
          set2 == null ? set1.getCalculationId() : set2.getCalculationId(),
          set2 == null ? null : set1.getCalculationId(), DEFAULT_BOUNDING_BOX_WIDTH, DEFAULT_BOUNDING_BOX_HEIGHT);
    }
    if (!boundingBoxes.isEmpty()) {
      //first overview
      showOverview(boundingBoxes);

      //then each individual page.
      for (final BBox boundingBox : boundingBoxes) {
        showIndividual(boundingBox, boundingBoxes.indexOf(boundingBox) + 1);
      }
    }
  }

  private void showOverview(final List<BBox> boundingBoxes) throws AeriusException, IOException, DocumentException {
    final BBox totalBoundingBox = GeometryUtil.merge(boundingBoxes);

    final PDFImageLayer extentLayer = new PDFImageLayer(getExtentImage(totalBoundingBox, boundingBoxes, OVERVIEW_IMAGE_WIDTH, OVERVIEW_IMAGE_HEIGHT));

    final BufferedImage image = getImage(totalBoundingBox, OVERVIEW_IMAGE_WIDTH, OVERVIEW_IMAGE_HEIGHT, extentLayer);

    final PDFSimpleTable overviewTable = PAAUtil.getDefaultBlockEmptyTable(true);
    PAAUtil.addBlockHeaderCellWithSubHeader(overviewTable, paaContext.getText("detail_overview_title"),
        paaContext.getText("detail_overview_sub_title"));
    overviewTable.addImage(image);

    overviewTable.addEmptyCell();
    overviewTable.add(getDepositionLegendTable());

    pdfDocument.addElement(overviewTable);
    pdfDocument.newPage();
  }

  private void showIndividual(final BBox boundingBox, final int index) throws AeriusException, IOException, DocumentException {
    final PDFImageLayer depositionLayer = getDepositionLayer();
    final PDFImageLayer iconLayer = new PDFImageLayer(getIndexIconImage(Integer.toString(index)), INDEX_ICON_POSITION_X, INDEX_ICON_POSITION_Y);

    final BufferedImage image = getImage(boundingBox, DETAIL_IMAGE_WIDTH, DETAIL_IMAGE_HEIGHT, depositionLayer, iconLayer);

    final PDFSimpleTable detailTable = new PDFSimpleTable(1);
    detailTable.addImageWithPadding(image, new Padding(Padding.DEFAULT_PADDING, Padding.DEFAULT_PADDING, DETAIL_IMAGE_LEFT_MARGIN,
        Padding.DEFAULT_PADDING));

    pdfDocument.addElement(detailTable);
    pdfDocument.newPage();
  }

  private BufferedImage getExtentImage(final BBox totalBoundingBox, final List<BBox> boundingBoxes, final int imageWidth, final int imageHeight) {
    final int actualImageWidth = determineImageWidth(totalBoundingBox, imageWidth, imageHeight);
    final int actualImageHeight = determineImageHeight(totalBoundingBox, imageWidth, imageHeight);

    final BBox fixedBbox = GeometryUtil.getFixedBBox(GeometryUtil.addPadding(totalBoundingBox, BOUNDING_BOX_PADDING),
        actualImageWidth, actualImageHeight);

    return getExtentOverviewLayer(fixedBbox, actualImageWidth, actualImageHeight, boundingBoxes,
        PAASharedFonts.DETAIL_OVERVIEW_INDEXNUMBER, PAASharedConstants.QUALITY_SETTING);
  }

  private BufferedImage getExtentOverviewLayer(final BBox imageBbox, final int imageWidth, final int imageHeight,
      final List<BBox> detailedExtends, final PDFFonts font, final float qualitySetting) {
    final BufferedImage image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
    final Graphics2D graphic = image.createGraphics();

    try {
      final int fontSize = calculateFontSize(font, imageBbox, detailedExtends, qualitySetting);
      final Font awtFont = new Font(font.getFontName(), Font.BOLD, fontSize);

      graphic.setStroke(new BasicStroke(2 * qualitySetting));
      graphic.setFont(awtFont);

      for (final BBox bbox : detailedExtends) {
        ImageUtils.drawExtent(graphic, imageBbox, bbox, imageWidth, imageHeight, detailedExtends.indexOf(bbox) + 1, awtFont, font.getColor());
      }
    } finally {
      graphic.dispose();
    }

    return image;
  }

  private int calculateFontSize(final PDFFonts font, final BBox imageBbox, final List<BBox> detailedExtends, final float qualitySetting) {
    final boolean isPortrait = imageBbox.getWidth() < imageBbox.getHeight();
    final BBox firstBBox = detailedExtends.get(0);

    final double bboxRib = isPortrait ? firstBBox.getWidth() : firstBBox.getHeight();
    final double imageBBoxRib = isPortrait ? imageBbox.getWidth() : imageBbox.getHeight();

    return (int) Math.round(font.getSize() * qualitySetting * (bboxRib / imageBBoxRib));
  }

  private BufferedImage getImage(final BBox boundingBox, final int imageWidth, final int imageHeight, final PDFImageLayer... layers)
      throws AeriusException {
    final int actualImageWidth = determineImageWidth(boundingBox, imageWidth, imageHeight);
    final int actualImageHeight = determineImageHeight(boundingBox, imageWidth, imageHeight);

    final BBox fixedBbox = GeometryUtil.getFixedBBox(GeometryUtil.addPadding(boundingBox, BOUNDING_BOX_PADDING),
        actualImageWidth, actualImageHeight);

    return PAALayerUtil.generateMapImage(paaContext, fixedBbox, actualImageWidth, actualImageHeight, layers);
  }

  private int determineImageWidth(final BBox bbox, final int width, final int height) {
    final boolean isPortrait = bbox.getWidth() < bbox.getHeight();

    return (isPortrait ? width : height) * (int) PAASharedConstants.QUALITY_SETTING;
  }

  private int determineImageHeight(final BBox bbox, final int width, final int height) {
    final boolean isPortrait = bbox.getWidth() < bbox.getHeight();

    return (isPortrait ? height : width) * (int) PAASharedConstants.QUALITY_SETTING;
  }

  private BufferedImage getIndexIconImage(final String label) {
    final PDFFonts font = PAASharedFonts.DETAIL_PAGE_INDEXNUMBER;

    return PDFUtils.getIndexIconImage(label, PAASharedConstants.QUALITY_SETTING * INDEX_ICON_SCALE, font.getFontName());
  }

  private PDFSimpleTable getDepositionLegendTable() {
    final PDFSimpleTable table = new PDFSimpleTable(LEGEND_RELATIVE_WIDTHS);

    final List<Range> legendItems = new ArrayList<>();
    final String legendDescriptionKey;
    if (set2 == null) {
      //deposition for one situation
      legendDescriptionKey = "legend_deposition_single_description";
      for (final Range colorRange : EmissionResultColorRanges.DEPOSITION.getRanges()) {
        legendItems.add(colorRange);
      }

    } else {
      //difference in deposition between 2 situations
      legendDescriptionKey = "legend_deposition_comparison_description";
      for (final Range comparison : ComparisonEnum.values()) {
        legendItems.add(comparison);
      }
    }
    final PdfPCell headerCell = PDFSimpleTable.getNewCellWithDefaultSettings();
    headerCell.setPhrase(new Phrase(paaContext.getText(legendDescriptionKey), PAASharedFonts.DEFAULT_TEXT.get()));
    headerCell.setColspan(LEGEND_RELATIVE_WIDTHS.getNumberOfColumns());
    headerCell.setPadding(LEGEND_TITLE_PADDING);
    table.add(headerCell);
    final int itemsPerColumn = (2 * legendItems.size() / LEGEND_RELATIVE_WIDTHS.getNumberOfColumns())
        + (legendItems.size() % LEGEND_RELATIVE_WIDTHS.getNumberOfColumns() == 0 ? 0 : 1);
    for (int i = 0; i < itemsPerColumn; i++) {
      for (int j = 0; j < LEGEND_RELATIVE_WIDTHS.getNumberOfColumns() / 2; j++) {
        final int currentLegendIndex = i + j * itemsPerColumn;
        if (currentLegendIndex >= legendItems.size()) {
          //no value, so just add an empty cell
          table.addEmptyCell();
          table.addEmptyCell();
        } else {
          final Range range = legendItems.get(currentLegendIndex);
          //first the icon
          table.addImage(
              ImageUtils.getHexagonImage(Math.round(LEGEND_ICON_HEIGHT * PAASharedConstants.QUALITY_SETTING), range.getColor()),
              LEGEND_ICON_HEIGHT);
          //second the value
          table.add(Double.toString(range.getLowerValue()), PAASharedFonts.DEPOSITION_LEGEND);
        }
      }
    }

    return table;
  }

}
