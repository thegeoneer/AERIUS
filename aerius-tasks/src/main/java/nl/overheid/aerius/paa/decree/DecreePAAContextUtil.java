/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.decree;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.register.PriorityProjectPDFRepository;
import nl.overheid.aerius.paa.common.PAAContextUtil;
import nl.overheid.aerius.paa.common.PAAReportType;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public final class DecreePAAContextUtil {

  private DecreePAAContextUtil() {
    //util class
  }

  public static DecreePAAContext createContext(final PMF pmf, final CloseableHttpClient httpClient, final CalculationInputData data,
      final DecreeInformation decreeInfo, final PAAReportType reportType) throws SQLException, AeriusException {
    final Locale locale = PAAContextUtil.getLocale(data);
    final SectorCategories categories = PAAContextUtil.getSectorCategories(pmf, locale);
    final List<PDFExportAssessmentAreaInfo> exportInfo = getExportInfo(pmf, data.getScenario(), decreeInfo);
    final int srid = ReceptorGridSettingsRepository.getSrid(pmf);
    return new DecreePAAContext(pmf, httpClient, locale, PAAContextUtil.getEpsg(srid), reportType, categories, data.getScenario(),
        exportInfo, decreeInfo);
  }

  private static List<PDFExportAssessmentAreaInfo> getExportInfo(final PMF pmf, final CalculatedScenario scenario, final DecreeInformation decreeInfo)
      throws SQLException {
    final List<PDFExportAssessmentAreaInfo> exportInfo;
    final boolean includeDemand = includeDemand(decreeInfo.getRequestState());
    if (isPrioritySubProject(decreeInfo)) {
      exportInfo = PriorityProjectPDFRepository.determineExportInfo(pmf, (PrioritySubProjectKey) decreeInfo.getProjectKey(), includeDemand);
    } else {
      exportInfo = PAAContextUtil.getExportInfo(pmf, scenario, decreeInfo.getSegment(), includeDemand);
    }
    return exportInfo;
  }

  private static boolean includeDemand(final RequestState requestState) {
    final boolean withDemand;

    switch (requestState) {
    case REJECTED_WITHOUT_SPACE:
      withDemand = true;
      break;
    case ASSIGNED:
    case ASSIGNED_FINAL:
      withDemand = false;
      break;
    // All other states should never trigger this export.
    default:
      throw new IllegalArgumentException("DecreePAAExport triggered for permit in unusual state or implementation missing.");
    }

    return withDemand;
  }

  private static boolean isPrioritySubProject(final DecreeInformation decreeInfo) {
    return decreeInfo.getSegment() == SegmentType.PRIORITY_PROJECTS && decreeInfo.getProjectKey() instanceof PrioritySubProjectKey;
  }

}
