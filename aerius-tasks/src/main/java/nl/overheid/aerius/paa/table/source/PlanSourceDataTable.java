/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.util.Locale;

import com.lowagie.text.Image;

import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.table.ImagePDFColumn;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PDFColumnPropertiesImpl;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource.PlanEmission;
import nl.overheid.aerius.worker.util.SectorIconUtil;

/**
 * Data table as used in the PAA export containing emission data for a plan source.
 */
public class PlanSourceDataTable extends SubSourceDataTable<PlanEmission> {

  enum PlanColumn {
    SECTOR_ICON(SubSourceColumn.getHeaderKey("sector"), 3),
    CATEGORY_NAME(SubSourceColumn.getHeaderKey("category_name"), 8),
    NAME(SubSourceColumn.getHeaderKey("description"), 8),
    UNITS(SubSourceColumn.getHeaderKey("plan_units"), 6),
    SUBSTANCE(SubSourceColumn.SUBSTANCE.getHeaderI18nKey(), 3),
    EMISSION(SubSourceColumn.EMISSION.getHeaderI18nKey(), 6);

    private final PDFColumnProperties columnProperties;

    private PlanColumn(final String headerKey, final float relativeWidth) {
      this.columnProperties = new PDFColumnPropertiesImpl(headerKey, relativeWidth);
    }

    public PDFColumnProperties getColumnProperties() {
      return columnProperties;
    }
  }

  private static final int SCALE_ICONS_TO_PERCENT = 50;

  /**
   * Constructor to set header + widths.
   * @param year The year to use.
   * @param emissionSource The PlanEmissionSource to make table for.
   * @param locale The locale to use.
   */
  public PlanSourceDataTable(final int year, final PlanEmissionSource emissionSource, final Locale locale) {
    super(year, emissionSource, locale);
  }

  @Override
  protected void addDescriptionColumns(final Locale locale) {
    addColumn(new ImagePDFColumn<PlanEmission>(PlanColumn.SECTOR_ICON.getColumnProperties()) {

      @Override
      protected Image getImage(final PlanEmission data) {
        return getSectorIcon(data.getCategory().getSectorIcon());
      }
    });

    addColumn(new TextPDFColumn<PlanEmission>(locale, PlanColumn.CATEGORY_NAME.getColumnProperties()) {

      @Override
      protected String getText(final PlanEmission data) {
        return data.getCategory().getName();
      }
    });

    addColumn(new TextPDFColumn<PlanEmission>(locale, PlanColumn.NAME.getColumnProperties()) {

      @Override
      protected String getText(final PlanEmission data) {
        return data.getName();
      }
    });
  }

  @Override
  protected void addSpecificColumns(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<PlanEmission>(locale, PlanColumn.UNITS.getColumnProperties()) {

      @Override
      protected String getText(final PlanEmission data) {
        return formatDouble(data.getAmount()) + " "
            + PAABaseTable.getText("emission_datatable_plan_unit", data.getCategory().getCategoryUnit(), getLocale());
      }
    });
  }

  @Override
  protected PDFColumnProperties getSubstanceColumnProperties() {
    return PlanColumn.SUBSTANCE.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getEmissionColumnProperties() {
    return PlanColumn.EMISSION.getColumnProperties();
  }

  private Image getSectorIcon(final SectorIcon sectorIcon) {
    return getImage(SectorIconUtil.getIconFilename(sectorIcon), SCALE_ICONS_TO_PERCENT);
  }

}
