/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public final class PAASharedFontsAwt {
  private static final Logger LOG = LoggerFactory.getLogger(PAASharedFontsAwt.class);

  //  FONTS *
  private static java.awt.Font fontMarkerText;

  private PAASharedFontsAwt() {
    //default constructor utility class.
  }

  public static java.awt.Font getFontMarkers() throws AeriusException {
    synchronized (PAASharedFontsAwt.class) {
      if (fontMarkerText == null) {
        try {
          fontMarkerText = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT,
              Thread.currentThread().getContextClassLoader().getResourceAsStream("fonts/rijksoverheidsanslf-bold.ttf"));
          GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(fontMarkerText);
        } catch (FontFormatException | IOException e) {
          LOG.error("getFontMarkers", e);
          throw new AeriusException(Reason.INTERNAL_ERROR);
        }
      }
    return fontMarkerText;
    }
  }

}
