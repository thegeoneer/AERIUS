/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.melding;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAAReportType;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public class MeldingPAAContext extends PAAContext {

  private final MeldingInformation meldingInfo;

  public MeldingPAAContext(final PMF pmf, final CloseableHttpClient httpClient, final Locale locale, final EPSG epsg, final PAAReportType reportType,
      final SectorCategories categories, final CalculatedScenario calculatedSenario, final List<PDFExportAssessmentAreaInfo> exportInfo,
      final MeldingInformation meldingInfo) throws SQLException, AeriusException {
    super(pmf, httpClient, locale, epsg, reportType, categories, calculatedSenario, exportInfo);
    this.meldingInfo = meldingInfo;
  }

  public MeldingInformation getMeldingInfo() {
    return meldingInfo;
  }



}
