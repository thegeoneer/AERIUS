/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.calculation;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAAReportType;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

public class CalculationPAAContext extends PAAContext {

  private static final int SHORTLIST_SIZE = 10;

  public CalculationPAAContext(final PMF pmf, final CloseableHttpClient httpClient, final Locale locale, final EPSG epsg,
      final PAAReportType reportType, final SectorCategories categories, final CalculatedScenario calculatedSenario,
      final List<PDFExportAssessmentAreaInfo> exportInfo)
          throws SQLException, AeriusException {
    super(pmf, httpClient, locale, epsg, reportType, categories, calculatedSenario, exportInfo);
  }

  @Override
  public List<PDFExportAssessmentAreaInfo> getHabitatedAreaInfoPasOnly() {
    final List<PDFExportAssessmentAreaInfo> usedToBePASList = getAreaInfoPasOnly();
    return usedToBePASList.size() > SHORTLIST_SIZE ? usedToBePASList.subList(0, SHORTLIST_SIZE) : usedToBePASList;
  }

  @Override
  public String getDepositionHabitatsSubheaderText() {
    return getText("deposition_habitats_top10_subheader");
  }

}
