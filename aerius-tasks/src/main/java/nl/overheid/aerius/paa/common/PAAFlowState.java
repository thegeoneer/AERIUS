/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

/**
 * Keeps track of the state of certain information during the generation process.
 * Because not everything can be accessed all the time, due to the way a pdf is generated this class
 * keeps track of certain state information at a specific moment in the generation flow.
 */
public class PAAFlowState {

  private boolean addMiniLogoHeader;
  private boolean addProductTitle;

  public boolean isAddMiniLogoHeader() {
    return addMiniLogoHeader;
  }

  public void setAddMiniLogoHeader(final boolean addMiniLogoHeader) {
    this.addMiniLogoHeader = addMiniLogoHeader;
  }

  public boolean isAddProductTitle() {
    return addProductTitle;
  }

  public void setAddProductTitle(final boolean addProductTitle) {
    this.addProductTitle = addProductTitle;
  }
}
