/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;

/**
 * Data table as used in the PAA export containing emission data for a shipping source.
 */
public class InlandMooringRouteTable extends AbstractRouteTable<InlandMooringRouteTable.InlandRouteTableRowData> {

  enum Column implements PDFColumnProperties {
    ROUTE_LABEL("ship_inland_route", 6),
    SHIP_CODE("ship_code", 12),
    DIRECTION("ship_direction", 6),
    WATERWAY("waterway", 6),
    VISITS("ship_movements", 6),
    PERCENTAGE_LADEN("percentage_loaded", 6);

    private final String headerKey;
    private final float relativeWidth;

    private Column(final String headerKey, final float relativeWidth) {
      this.headerKey = "emission_datatable_" + headerKey;
      this.relativeWidth = relativeWidth;
    }

    @Override
    public String getHeaderI18nKey() {
      return headerKey;
    }

    @Override
    public float getRelativeWidth() {
      return relativeWidth;
    }
  }

  /**
   * Constructor to set header + widths.
   * @param emissionValues The EmissionValues to make table for.
   * @param locale The locale to use.
   */
  public InlandMooringRouteTable(final InlandMooringEmissionSource emissionValues, final Locale locale) {
    super(locale);

    setData(getRowData(emissionValues));

    addColumns(locale);
  }

  private void addColumns(final Locale locale) {
    addLabelColumn(locale);
    addCodeColumn(locale);
    addDirectionColumn(locale);
    addWaterwayColumn(locale);
    addVisitsColumn(locale);
    addPercentageLadenColumn(locale);
  }

  private void addLabelColumn(final Locale locale) {
    addColumn(new TextPDFColumn<InlandRouteTableRowData>(locale, Column.ROUTE_LABEL) {

      @Override
      protected String getText(final InlandRouteTableRowData data) {
        return data.getLabel();
      }
    });
  }

  private void addCodeColumn(final Locale locale) {
    addColumn(new TextPDFColumn<InlandRouteTableRowData>(locale, Column.SHIP_CODE) {

      @Override
      protected String getText(final InlandRouteTableRowData data) {
        return data.getCategory().getDescription();
      }
    });
  }

  private void addDirectionColumn(final Locale locale) {
    addColumn(new TextPDFColumn<InlandRouteTableRowData>(locale, Column.DIRECTION) {

      @Override
      protected String getText(final InlandRouteTableRowData data) {
        return getText("emission_datatable_ship_direction_" + data.getRoute().getDirection().name().toLowerCase());
      }
    });
  }

  private void addWaterwayColumn(final Locale locale) {
    addColumn(new TextPDFColumn<InlandRouteTableRowData>(locale, Column.WATERWAY) {

      @Override
      protected String getText(final InlandRouteTableRowData data) {
        final String waterwayLabel;
        final InlandWaterwayCategory waterwayCategory = data.getRoute().getRoute().getInlandWaterwayType().getWaterwayCategory();
        if (waterwayCategory == null) {
          waterwayLabel = "";
        } else {
          final String direction = waterwayCategory.isDirectionRelevant()
              ? (" (" + getText("emission_waterway_direction", data.getRoute().getRoute().getInlandWaterwayType().getWaterwayDirection()) + ")") : "";
          waterwayLabel = waterwayCategory.getName() + direction;
        }
        return waterwayLabel;
      }
    });
  }

  private void addVisitsColumn(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<InlandRouteTableRowData>(locale, Column.VISITS) {

      @Override
      protected String getText(final InlandRouteTableRowData data) {
        return formatInt(data.getRoute().getShipMovementsPerYear());
      }
    });
  }

  private void addPercentageLadenColumn(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<InlandRouteTableRowData>(locale, Column.PERCENTAGE_LADEN) {

      @Override
      protected String getText(final InlandRouteTableRowData data) {
        return formatInt(data.getRoute().getPercentageLaden());
      }
    });
  }

  private List<InlandRouteTableRowData> getRowData(final InlandMooringEmissionSource emissionValues) {
    //convert the vesselgroups to something we can work with for this situation.
    //in this case, group the data by routes.
    final Map<ShippingRoute, List<InlandRouteTableRowData>> rowDataMap = new HashMap<>();
    for (final InlandMooringVesselGroup vesselGroupEmissionValues : emissionValues.getEmissionSubSources()) {
      for (final InlandMooringRoute inlandMooringRoute : vesselGroupEmissionValues.getRoutes()) {
        if (rowDataMap.get(inlandMooringRoute.getRoute()) == null) {
          rowDataMap.put(inlandMooringRoute.getRoute(), new ArrayList<InlandRouteTableRowData>());
        }
        rowDataMap.get(inlandMooringRoute.getRoute()).add(
            new InlandRouteTableRowData(
                vesselGroupEmissionValues.getCategory(),
                inlandMooringRoute));
      }
    }
    return toOrderedTableRows(rowDataMap);
  }

  static class InlandRouteTableRowData extends AbstractRouteTable.RouteTableRowData<InlandShippingCategory> {

    private final InlandMooringRoute route;

    InlandRouteTableRowData(final InlandShippingCategory category, final InlandMooringRoute route) {
      super(category);
      this.route = route;
    }

    public InlandMooringRoute getRoute() {
      return route;
    }

    @Override
    public int compareTo(final RouteTableRowData<InlandShippingCategory> other) {
      int compared = super.compareTo(other);
      if (compared == 0 && other instanceof InlandRouteTableRowData) {
        compared = route != null && ((InlandRouteTableRowData) other).route != null
            ? route.getDirection().compareTo(((InlandRouteTableRowData) other).route.getDirection()) : 0;
      }
      return compared;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + super.hashCode();
      result = prime * result + ((route == null) ? 0 : route.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      boolean equal = false;
      if (obj != null && this.getClass() == obj.getClass()) {
        final InlandRouteTableRowData other = (InlandRouteTableRowData) obj;
        equal = super.equals(obj)
            && ((route != null && route.equals(other.getRoute())) || (route == null && other.route == null));
      }
      return equal;
    }

  }

}
