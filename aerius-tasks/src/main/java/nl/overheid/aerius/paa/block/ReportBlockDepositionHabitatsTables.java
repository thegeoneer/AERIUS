/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.io.IOException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.paa.layer.DepositionLegendTable;
import nl.overheid.aerius.paa.table.result.DepositionHabitatsDataTable;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * PAA Report Block: Deposition Habitats tables.
 */
public class ReportBlockDepositionHabitatsTables extends PAAReportBlock<PAAContext> {

  private final boolean pasOnly;
  private final boolean showForeignAreas;
  private final boolean showRulesColumn;

  public ReportBlockDepositionHabitatsTables(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext, final boolean pasOnly,
      final boolean showForeignAreas, final boolean showRulesColumn) {
    super(pdfDocument, paaContext);
    this.pasOnly = pasOnly;
    this.showForeignAreas = showForeignAreas;
    this.showRulesColumn = showRulesColumn;
  }

  @Override
  public void generate() throws DocumentException, SQLException, IOException, AeriusException {
    final List<PDFExportAssessmentAreaInfo> areas = pasOnly ? paaContext.getHabitatedAreaInfoPasOnly()
            : paaContext.getAreaInfoNonPasOnly(showForeignAreas);
    addDepositionTables(areas);
    pdfDocument.newPage();
  }

  private void addDepositionTables(final List<PDFExportAssessmentAreaInfo> areas) throws DocumentException, IOException {
    if (!areas.isEmpty()) {
      addDepositionHabitatsTables(areas);
      addDepositionTableLegend();
    }
  }

  private void addDepositionHabitatsTables(final List<PDFExportAssessmentAreaInfo> areas) throws DocumentException, IOException {
    boolean first = true;
    for (final PDFExportAssessmentAreaInfo info : areas) {
      if (!first) {
        pdfDocument.addPadding(PADDING_BETWEEN_BLOCKS);
      }

      final PDFSimpleTable wrappedTable = PAAUtil.getDefaultBlockEmptyTable(false);
      wrappedTable.setKeepTogether(true);
      wrappedTable.allowSplittingRows();

      if (first) {
        PAAUtil.addBlockHeaderCellWithSubHeader(
            wrappedTable,
            paaContext.getText("deposition_habitats_title"),
            MessageFormat.format(paaContext.getDepositionHabitatsSubheaderText(),
                paaContext.getText("subheader_unit_mol_ha_y")));
      } else {
        wrappedTable.addEmptyCell();
      }

      final PDFSimpleTable habitatCell = new PDFSimpleTable(1);
      habitatCell.setHeaderRows(1);
      final PdfPCell habitatTitleCell = PDFSimpleTable.getNewCellWithDefaultSettings();
      habitatTitleCell.setPhrase(new Phrase(info.getName(), PAASharedFonts.DEFAULT_TEXT_BIG.get()));
      habitatTitleCell.setPaddingBottom(PADDING_BETWEEN_TABLES);
      habitatCell.add(habitatTitleCell);

      habitatCell.add(new DepositionHabitatsDataTable(paaContext.getLocale(), paaContext, info, showRulesColumn));

      wrappedTable.add(habitatCell);

      pdfDocument.addElement(wrappedTable);
      first = false;
    }
  }

  private void addDepositionTableLegend() throws DocumentException, IOException {
    final PDFSimpleTable table = PAAUtil.getDefaultBlockEmptyTable(false);
    table.addEmptyCell();
    table.add(DepositionLegendTable.get(paaContext, showRulesColumn));

    pdfDocument.addPadding(PADDING_BETWEEN_TABLES);
    pdfDocument.addElement(table);
  }

}
