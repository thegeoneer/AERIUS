/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

import com.lowagie.text.Font;

import nl.overheid.aerius.pdf.font.F;
import nl.overheid.aerius.pdf.font.PDFFonts;

/**
 * Contains shared fonts for the reports.
 */
public final class PAASharedFonts {
  // Default header/text fonts.
//  static final PDFFonts DEFAULT_HEADER = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT_BOLD, PAAFonts.DEFAULT_COLOR, 12),
//  static final PDFFonts DEFAULT_SUBHEADER = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT_BOLD, GebiedssamenvattingConstants.ADDITIONAL_COLOR, 10),
  public static final PDFFonts DEFAULT_TEXT = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.DEFAULT_COLOR, 8);
  public static final PDFFonts DEFAULT_TEXT_UNDERLINE =
      PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.DEFAULT_COLOR, 8, Font.UNDERLINE);
  public static final PDFFonts DEFAULT_TEXT_BIG = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.DEFAULT_COLOR, 14);
  public static final PDFFonts DEFAULT_TEXT_BIG_UNDERLINE =
      PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.DEFAULT_COLOR, 14, Font.UNDERLINE);
  public static final PDFFonts TABLE_HEADER = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.ADDITIONAL_COLOR, 8);
  public static final PDFFonts TABLE_CELL = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.DEFAULT_COLOR, 10);
  public static final PDFFonts TABLE_CELL_STRIKETHRU =
      PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.DEFAULT_COLOR, 10, Font.STRIKETHRU);

  // Specific items fonts.
  public static final PDFFonts FRONTPAGE_FULL = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT_ITALIC, PAASharedConstants.ALTERNATIVE_COLOR, 14);
  public static final PDFFonts FRONTPAGE_EXTRA = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.ALTERNATIVE_COLOR, 10);
  public static final PDFFonts FRONTPAGE_INDEX = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.DEFAULT_COLOR, 10);
  public static final PDFFonts FOOTER_LEFTBLOCK = PDFFonts.create(F.RIJKSOVERHEID_SANS_HEADING, PAASharedConstants.ADDITIONAL_COLOR, 8);
  public static final PDFFonts FOOTER_CENTERBLOCK = PDFFonts.create(F.RIJKSOVERHEID_SANS_HEADING, PAASharedConstants.DEFAULT_COLOR, 8);
  public static final PDFFonts FOOTER_FRONTPAGE_PAGENUMBER = PDFFonts.create(F.RIJKSOVERHEID_SANS_HEADING, PAASharedConstants.ALTERNATIVE_COLOR, 8);
  public static final PDFFonts FOOTER_PAGENUMBER = PDFFonts.create(F.RIJKSOVERHEID_SANS_HEADING, PAASharedConstants.ADDITIONAL_COLOR, 8);
  public static final PDFFonts FRONTPAGE_FOOTER_REFERENCE = PDFFonts.create(F.RIJKSOVERHEID_SANS_HEADING, PAASharedConstants.ALTERNATIVE_COLOR, 8);
  public static final PDFFonts FOOTER_REFERENCE = PDFFonts.create(F.RIJKSOVERHEID_SANS_HEADING, PAASharedConstants.DEFAULT_COLOR, 8);
  public static final PDFFonts PRODUCT_NAME = PDFFonts.create(F.RIJKSOVERHEID_SANS_HEADING, PAASharedConstants.DEFAULT_COLOR, 14);
  public static final PDFFonts PAGES_EXPLANATION_HEADER = PDFFonts.create(F.RIJKSOVERHEID_SANS_HEADING, PAASharedConstants.ADDITIONAL_COLOR, 8);
  public static final PDFFonts BLOCK_HEADER = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.ADDITIONAL_COLOR, 14);
  public static final PDFFonts BLOCK_SUB_HEADER = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.ADDITIONAL_COLOR, 10);
  public static final PDFFonts EMISSION_KEY = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.ADDITIONAL_COLOR, 8);
  public static final PDFFonts DEPOSITION_LEGEND = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.ADDITIONAL_COLOR, 8);
  public static final PDFFonts DEPOSITION_LEGEND_SMALLER = PDFFonts.create(F.RIJKSOVERHEID_SANS_TEXT, PAASharedConstants.ADDITIONAL_COLOR, 7);
  public static final PDFFonts DETAIL_OVERVIEW_INDEXNUMBER = PDFFonts.create(F.RIJKSOVERHEID_SANS_LF, PAASharedConstants.ADDITIONAL_COLOR, 150);
  public static final PDFFonts DETAIL_PAGE_INDEXNUMBER = PDFFonts.create(F.RIJKSOVERHEID_SANS_LF, PAASharedConstants.ALTERNATIVE_COLOR, 92);

  // Not to be constructed.
  private PAASharedFonts() { }
}
