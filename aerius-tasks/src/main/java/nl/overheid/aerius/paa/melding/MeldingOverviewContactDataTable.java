/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.melding;

import java.text.MessageFormat;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.table.GenericThreeColumnDataTable;
import nl.overheid.aerius.pdf.PDFConstants;
import nl.overheid.aerius.shared.domain.melding.BeneficiaryInformation;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.domain.melding.OrganisationInformation;

/**
 * Datatable containing contact data.
 */
public class MeldingOverviewContactDataTable extends GenericThreeColumnDataTable {

  /**
   * Default constructor setting header and such.
   *
   * @param r
   *          The report.
   * @param paaContext
   *          The PAA context.
   */
  public MeldingOverviewContactDataTable(final MeldingPAAContext paaContext) {
    super(paaContext.getLocale(), getHeaders(paaContext.getLocale(), paaContext.getMeldingInfo()));

    addRow(getData(paaContext.getMeldingInfo()));
  }

  private GenericThreeColumnDataRow getData(final MeldingInformation info) {
    final BeneficiaryInformation beneficiaryInfo = info.getBeneficiaryInformation();
    final String beneficiaryOrg = orgInfoToText(beneficiaryInfo, null)
        + (beneficiaryInfo.isCorrespondenceCustom() ? correspondenceInfoToText(beneficiaryInfo) : "");

    String benefactorOrg;
    String executorOrg;

    if (info.isBenefactorCustom()) {
      benefactorOrg = orgInfoToText(info.getBenefactorInformation(), info.getOrganizationOin());
    } else {
      benefactorOrg = null;
    }

    if (info.isExecutorCustom()) {
      executorOrg = orgInfoToText(info.getExecutorInformation(), null);
    } else {
      executorOrg = null;
    }
    final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
    row.set(Column.COLUMN_1, beneficiaryOrg);
    row.set(Column.COLUMN_2, benefactorOrg);
    row.set(Column.COLUMN_3, executorOrg);
    return row;
  }

  private static GenericThreeColumnDataRow getHeaders(final Locale locale, final MeldingInformation info) {
    final GenericThreeColumnDataRow headerRow = new GenericThreeColumnDataRow();
    headerRow.set(Column.COLUMN_1, "base_contact_calculated_for");

    if (info.isBenefactorCustom()) {
      headerRow.set(Column.COLUMN_2, "base_contact_calculated_by");
    }

    if (info.isExecutorCustom()) {
      headerRow.set(Column.COLUMN_3, "base_contact_executor");
    }
    return headerRow;
  }

  private String correspondenceInfoToText(final BeneficiaryInformation info) {
    return PDFConstants.NEWLINE
        + getText("base_contact_other_location",  getLocale())
        + PDFConstants.NEWLINE
        + normalize(info.getCorrespondenceAddress())
        + PDFConstants.NEWLINE
        + (StringUtils.isEmpty(info.getCorrespondencePostcode()) && StringUtils.isEmpty(info.getCorrespondenceCity())
            ? PAASharedConstants.DEFAULT_EMPTY_CELL_STRING
                : normalize(info.getCorrespondencePostcode()) + " " + normalize(info.getCorrespondenceCity()));
  }

  private String orgInfoToText(final OrganisationInformation info, final String organitionOin) {
    return normalize(info.getOrganisationName())
        + PDFConstants.NEWLINE
        + normalize(info.getOrganisationContactPerson())
        + PDFConstants.NEWLINE
        + normalize(info.getOrganisationAddress())
        + PDFConstants.NEWLINE
        + (StringUtils.isEmpty(info.getOrganisationPostcode()) && StringUtils.isEmpty(info.getOrganisationCity())
            ? PAASharedConstants.DEFAULT_EMPTY_CELL_STRING
                : normalize(info.getOrganisationPostcode()) + " " + normalize(info.getOrganisationCity()))
                + PDFConstants.NEWLINE
                + normalize(info.getOrganisationEmail())
                + (StringUtils.isEmpty(organitionOin) ? "" : PDFConstants.NEWLINE
                    + MessageFormat.format(getText("base_contact_kvk", getLocale()), organitionOin));
  }

}
