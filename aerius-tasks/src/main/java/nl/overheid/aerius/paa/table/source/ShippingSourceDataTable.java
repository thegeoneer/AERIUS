/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.util.Locale;

import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.source.EmissionSourceCollection;
import nl.overheid.aerius.shared.domain.source.VesselGroupEmissionSubSource;

/**
 * Data table as used in the PAA export containing emission data for a shipping source.
 */
public abstract class ShippingSourceDataTable<V extends VesselGroupEmissionSubSource<?>> extends SubSourceDataTable<V> {

  enum ShippingSourceColumn {
    SHIP_CODE("ship_code"),
    DESCRIPTION("description");

    private final String headerKey;

    private ShippingSourceColumn(final String headerKey) {
      this.headerKey = SubSourceColumn.getHeaderKey(headerKey);
    }

    public String getHeaderI18nKey() {
      return headerKey;
    }


  }

  /**
   * @param year The year to use.
   * @param emissionSource The emissionSource to make table for.
   * @param locale The locale to use.
   */
  public ShippingSourceDataTable(final int year, final EmissionSourceCollection<V> emissionSource, final Locale locale) {
    super(year, emissionSource, locale);
  }

  @Override
  protected void addDescriptionColumns(final Locale locale) {
    addShipCodeColumn(locale);
    addDescriptionColumn(locale);
  }

  protected abstract PDFColumnProperties getShipCodeColumnProperties();

  protected abstract PDFColumnProperties getDescriptionColumnProperties();

  private void addShipCodeColumn(final Locale locale) {
    addColumn(new TextPDFColumn<V>(locale, getShipCodeColumnProperties()) {

      @Override
      protected String getText(final V data) {
        return data.getCategory().getName();
      }
    });
  }

  private void addDescriptionColumn(final Locale locale) {
    addColumn(new TextPDFColumn<V>(locale, getDescriptionColumnProperties()) {

      @Override
      protected String getText(final V data) {
        return data.getName();
      }
    });
  }

}
