/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;

/**
 * PAA Report SubBlock: Base - Explanation.
 */
public class ReportSubBlockBaseExplanation extends PAAReportBlock<PAAContext> {

  /**
   *
   * @param r The report.
   * @param pdfContext The PDF context.
   * @param paaContext The PAA context.
   */
  public ReportSubBlockBaseExplanation(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  public void generate() throws DocumentException {
    final PDFSimpleTable explanationTable = PAAUtil.getDefaultBlockEmptyTable(true);
    PAAUtil.addBlockHeaderCell(explanationTable, paaContext.getText("base_explanation_title"));
    explanationTable.add(paaContext.getMetaData().getDescription(), PAASharedFonts.DEFAULT_TEXT);
    pdfDocument.addElement(explanationTable);
  }

}
