/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.util.Locale;

import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PDFColumnPropertiesImpl;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;

/**
 * Data table as used in the PAA export containing emission data for a shipping source.
 */
public class InlandMooringSourceDataTable extends ShippingSourceDataTable<InlandMooringVesselGroup> {

  enum InlandMooringColumn {
    SHIP_CODE(ShippingSourceColumn.SHIP_CODE.getHeaderI18nKey(), 8),
    DESCRIPTION(ShippingSourceColumn.DESCRIPTION.getHeaderI18nKey(), 12),
    RESIDENCE_TIME(SubSourceColumn.getHeaderKey("ship_residence_time"), 6),
    SUBSTANCE(SubSourceColumn.SUBSTANCE.getHeaderI18nKey(), 3),
    EMISSION(SubSourceColumn.EMISSION.getHeaderI18nKey(), 6);

    private final PDFColumnProperties columnProperties;

    private InlandMooringColumn(final String headerKey, final float relativeWidth) {
      this.columnProperties = new PDFColumnPropertiesImpl(headerKey, relativeWidth);
    }

    public PDFColumnProperties getColumnProperties() {
      return columnProperties;
    }
  }

  /**
   * Constructor to set header + widths.
   * @param year The year to use.
   * @param emissionSource The InlandMooringEmissionValues to make table for.
   * @param locale The locale to use.
   */
  public InlandMooringSourceDataTable(final int year, final InlandMooringEmissionSource emissionSource, final Locale locale) {
    super(year, emissionSource, locale);
  }

  @Override
  protected void addSpecificColumns(final Locale locale) {
    addResidenceTimeColumn(locale);
  }

  private void addResidenceTimeColumn(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<InlandMooringVesselGroup>(locale, InlandMooringColumn.RESIDENCE_TIME.getColumnProperties()) {

      @Override
      protected String getText(final InlandMooringVesselGroup data) {
        return formatInt(data.getResidenceTime());
      }
    });
  }

  @Override
  protected PDFColumnProperties getShipCodeColumnProperties() {
    return InlandMooringColumn.SHIP_CODE.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getDescriptionColumnProperties() {
    return InlandMooringColumn.DESCRIPTION.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getSubstanceColumnProperties() {
    return InlandMooringColumn.SUBSTANCE.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getEmissionColumnProperties() {
    return InlandMooringColumn.EMISSION.getColumnProperties();
  }

}
