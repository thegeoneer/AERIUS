/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.base;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAAFlowState;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.WatermarkType;
import nl.overheid.aerius.pdf.PDFAlignment;
import nl.overheid.aerius.pdf.PDFClarifications;
import nl.overheid.aerius.pdf.PDFWriterUtil;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * Writes data to the pdf on Page End event. Data written is header and footer information,
 * but also the complete frontpage.
 */
public class PAAPageEventHelper extends PdfPageEventHelper {

  private static final int FOOTER_PADDING_FROM_RIGHT = 40;
  private static final int FOOTER_PADDING_FROM_BOTTOM = 30;
  private static final float FOOTER_RIGHT_POSX = PAASharedConstants.PAGESIZE.getWidth() - FOOTER_PADDING_FROM_RIGHT;
  private static final float FOOTER_CENTER_POSX = 130;
  private static final float FOOTER_POSY = FOOTER_PADDING_FROM_BOTTOM; // in PDF's the bottom of a page would be Y-position 0
  private static final String FOOTER_REFERENCE_FORMAT = "{0} ({1})"; // {0} = reference, {1} = date
  private static final String FOOTER_REFERENCE_DATE_FORMAT = "dd MMMM yyyy";

  private static final int FOOTER_PRODUCT_TEXT_HEIGHT = 40;
  private static final float FOOTER_PRODUCT_TEXT_URX = 110;
  private static final float FOOTER_PRODUCT_TEXT_URY = FOOTER_PADDING_FROM_BOTTOM + 10;
  private static final float FOOTER_PRODUCT_TEXT_LLX = 10;
  private static final float FOOTER_PRODUCT_TEXT_LLY = FOOTER_PRODUCT_TEXT_URY - FOOTER_PRODUCT_TEXT_HEIGHT;

  private static final double ADD_30_PERCENT = 1.3;

  private static final float ADD_50_PERCENT = 1.5f;

  private static final float HEADER_CLARIFICATION_TEXT_POSX = PAASharedConstants.PAGESIZE.getWidth() - FOOTER_PADDING_FROM_RIGHT;
  private static final float HEADER_CLARIFICATION_MAX_SIZE = 300;
  private static final float HEADER_CLARIFICATION_TEXT_POSY_OFFSET = 68;
  private static final float HEADER_CLARIFICATION_TEXT_POSY = PAASharedConstants.PAGESIZE.getHeight() - HEADER_CLARIFICATION_TEXT_POSY_OFFSET;

  private static final int HEADER_ADDITIONALPAGES_LOGO_HEIGHT = 150;
  private static final float HEADER_ADDITIONALPAGES_LOGO_POSX = 62;
  private static final float HEADER_ADDITIONALPAGES_LOGO_PADDING_FROM_TOP = 75;
  private static final float HEADER_ADDITIONALPAGES_LOGO_POSY = PAASharedConstants.PAGESIZE.getHeight()
      - HEADER_ADDITIONALPAGES_LOGO_PADDING_FROM_TOP;

  private static final int PRODUCT_TEXT_WIDTH = 160;
  private static final int PRODUCT_TEXT_HEIGHT = 60;
  private static final float PRODUCT_TEXT_URX = PAASharedConstants.PAGESIZE.getWidth() - FOOTER_PADDING_FROM_RIGHT;
  private static final float PRODUCT_TEXT_URY =
      PAASharedConstants.PAGESIZE.getHeight() - HEADER_ADDITIONALPAGES_LOGO_PADDING_FROM_TOP + PAASharedFonts.PRODUCT_NAME.getNextLinePadding();
  private static final float PRODUCT_TEXT_LLX = PRODUCT_TEXT_URX - PRODUCT_TEXT_WIDTH;
  private static final float PRODUCT_TEXT_LLY = PRODUCT_TEXT_URY - PRODUCT_TEXT_HEIGHT;

  private static final float WATERMARK_DIMENSION = 200;
  // Even index is the X position, uneven index the Y position.
  private static final float[] WATERMARK_POSITIONS = new float[] {
      PAASharedConstants.PAGESIZE.getWidth(), PAASharedConstants.PAGESIZE.getHeight() - WATERMARK_DIMENSION,
      0, 0,
  };

  private Image watermarkImage;
  private final PDFClarifications clarifications;
  private final PAAFrontPage frontPage;
  private final PAAFlowState flowState;
  protected final PAAContext paaContext;

  /**
   *
   */
  protected PAAPageEventHelper(final PAAFlowState flowState, final PAAContext paaContext) {
    this.flowState = flowState;
    this.paaContext = paaContext;
    this.clarifications = new PDFClarifications(paaContext.getText("header_clarification_mark"),
        paaContext.getText("header_clarification_separator"));
    frontPage = getFrontPage(paaContext);
  }

  protected PAAFrontPage getFrontPage(final PAAContext paaContext) {
    return new PAAFrontPage(paaContext);
  }

  public void setWaterMark(final WatermarkType watermark) {
    try {
      if (watermark == null) {
        watermarkImage = null;
      } else {
        watermarkImage = PDFUtils.getImage(paaContext.getResourcesDir() + watermark.getDefaultFilename(), paaContext.getLocale());
        watermarkImage.scaleToFit(WATERMARK_DIMENSION, WATERMARK_DIMENSION);
      }
    } catch (IllegalArgumentException | DocumentException | IOException e) {
      throw new ExceptionConverter(e);
    }
  }

  @Override
  public void onEndPage(final PdfWriter writer, final Document document) {
    final boolean isFrontPage = document.getPageNumber() == 1;
    final CalculatedScenario scenario = paaContext.getCalculatedScenario();

    try {
      addClarificationHeaders(writer);

      if (isFrontPage) {
        frontPage.addFrontPageContent(writer);
      }

      // The Footer part that we can add without problems.
      final String dateString = paaContext.getFormattedCreationData(FOOTER_REFERENCE_DATE_FORMAT);
      final String referenceString = MessageFormat.format(
          FOOTER_REFERENCE_FORMAT,
          scenario.getMetaData().getReference(),
          dateString);

      PDFWriterUtil.addText(writer, referenceString, isFrontPage ? PAASharedFonts.FRONTPAGE_FOOTER_REFERENCE : PAASharedFonts.FOOTER_REFERENCE,
          FOOTER_RIGHT_POSX, FOOTER_POSY, PDFAlignment.RIGHT);

      if (!isFrontPage) {
        addRemainingFooter(writer);
        miniLogoHeader(writer);
        productText(writer);
        addWaterMark(writer);
      }
    } catch (final DocumentException | IOException de) {
      throw new ExceptionConverter(de);
    }
  }

  private void addRemainingFooter(final PdfWriter writer) throws DocumentException {
    final ColumnText productText = new ColumnText(writer.getDirectContent());
    productText.addText(new Phrase(getProductText(), PAASharedFonts.FOOTER_LEFTBLOCK.get()));
    productText.setSimpleColumn(FOOTER_PRODUCT_TEXT_LLX, FOOTER_PRODUCT_TEXT_LLY, FOOTER_PRODUCT_TEXT_URX, FOOTER_PRODUCT_TEXT_URY);
    productText.setLeading(0, PAASharedConstants.DEFAULT_LEADING);
    productText.setAlignment(Element.ALIGN_RIGHT);
    productText.go();

    PDFWriterUtil.addText(writer, getRemainenFooterTextFirstLine(), PAASharedFonts.FOOTER_CENTERBLOCK, FOOTER_CENTER_POSX, FOOTER_POSY);

    final String footerSecondLine = getRemainenFooterTextSecondLine();
    if (!StringUtils.isEmpty(footerSecondLine)) {
      PDFWriterUtil.addText(writer, getRemainenFooterTextSecondLine(), PAASharedFonts.FOOTER_CENTERBLOCK, FOOTER_CENTER_POSX,
          FOOTER_POSY - PAASharedFonts.FOOTER_CENTERBLOCK.getSize() * ADD_50_PERCENT);
    }
  }

  protected String getRemainenFooterTextFirstLine() {
    final boolean isComparison = paaContext.getCalculatedScenario() instanceof CalculatedComparison;
    final Calculation set1 = isComparison ? ((CalculatedComparison) paaContext.getCalculatedScenario()).getCalculationOne()
        : ((CalculatedSingle) paaContext.getCalculatedScenario()).getCalculation();

    return set1.getSources().getName();
  }

  protected String getRemainenFooterTextSecondLine() {
    final boolean isComparison = paaContext.getCalculatedScenario() instanceof CalculatedComparison;
    final Calculation set2 = isComparison ? ((CalculatedComparison) paaContext.getCalculatedScenario()).getCalculationTwo() : null;

    return set2 == null ? null : set2.getSources().getName();
  }

  private void miniLogoHeader(final PdfWriter writer) throws DocumentException, IOException {
    if (flowState.isAddMiniLogoHeader()) {
      final Image headerImage = PDFUtils.getImage(paaContext.getHeaderLogoFileName(), paaContext.getLocale());
      headerImage.scaleToFit(HEADER_ADDITIONALPAGES_LOGO_HEIGHT, PAASharedConstants.PAGESIZE.getWidth());
      ColumnText.showTextAligned(
          writer.getDirectContent(),
          Element.ALIGN_LEFT,
          new Phrase(new Chunk(headerImage, 0, 0)),
          HEADER_ADDITIONALPAGES_LOGO_POSX, HEADER_ADDITIONALPAGES_LOGO_POSY, 0);
    }
  }

  private void productText(final PdfWriter writer) throws DocumentException {
    if (flowState.isAddProductTitle()) {
      final ColumnText productText = new ColumnText(writer.getDirectContent());
      productText.addText(new Phrase(getProductText(), PAASharedFonts.PRODUCT_NAME.get()));
      productText.setSimpleColumn(PRODUCT_TEXT_LLX, PRODUCT_TEXT_LLY, PRODUCT_TEXT_URX, PRODUCT_TEXT_URY);
      productText.setAlignment(Element.ALIGN_RIGHT);
      productText.go();
    }
  }

  protected String getProductText() {
    return paaContext.getText("product", paaContext.getReportType());
  }

  private void addWaterMark(final PdfWriter writer) {
    if (watermarkImage != null) {
      // Watermark.
      for (int i = 0; i < WATERMARK_POSITIONS.length; i += 2) {
        PDFWriterUtil.addPhrase(writer.getDirectContentUnder(), new Phrase(new Chunk(watermarkImage, 0, 0)),
            WATERMARK_POSITIONS[i], WATERMARK_POSITIONS[i + 1], PDFAlignment.CENTER);
      }
    }
  }

  private void addClarificationHeaders(final PdfWriter writer) {
    float width = 0;
    final List<Phrase> phrases = new ArrayList<>();
    for (final String clarification : clarifications.getClarificationTexts()) {
      final Phrase phrase = new Phrase(clarification, PAASharedFonts.PAGES_EXPLANATION_HEADER.get());
      width = Math.max(width, ColumnText.getWidth(phrase));
      phrases.add(phrase);
    }
    width = Math.min(width, HEADER_CLARIFICATION_MAX_SIZE);
    int markerNumber = 1;
    for (final Phrase phrase : phrases) {
      PDFWriterUtil.addPhrase(writer, phrase,
          HEADER_CLARIFICATION_TEXT_POSX - width,
          HEADER_CLARIFICATION_TEXT_POSY
              - (float) (PAASharedFonts.PAGES_EXPLANATION_HEADER.getSize() * (markerNumber++ - 1) * ADD_30_PERCENT));
    }
    clarifications.reset();
  }

}
