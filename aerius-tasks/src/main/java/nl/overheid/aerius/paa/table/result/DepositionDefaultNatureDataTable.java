/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.result;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;

/**
 * Data table as used in the PAA export containing deposition on nature areas.
 */
public class DepositionDefaultNatureDataTable extends DepositionNatureDataTable {

  public DepositionDefaultNatureDataTable(final PAAContext paaContext, final List<PDFExportAssessmentAreaInfo> areas,
      final boolean showRulesColumn) {
    super(paaContext, areas, getColumns(paaContext, showRulesColumn));
  }

  private static List<Column> getColumns(final PAAContext paaContext, final boolean showRules) {
    final List<Column> columns = new ArrayList<>();
    columns.add(Column.NAME);
    if (paaContext.isComparison()) {
      columns.add(Column.CURRENT_DEMAND_FOR_MAX_DELTA_DEMAND);
      columns.add(Column.PROPOSED_DEMAND_FOR_MAX_DELTA_DEMAND);
      columns.add(Column.MAX_DELTA_DEMAND);
    } else {
      columns.add(Column.MAX_PROPOSED_DEMAND);
    }
    columns.add(Column.MAX_DEMAND_ONLY_EXCEEDING);
    if (showRules) {
      columns.add(Column.RULES);
    }

    return columns;
  }

}
