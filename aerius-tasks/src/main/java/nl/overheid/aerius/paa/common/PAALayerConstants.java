/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;


/**
 *
 */
public final class PAALayerConstants {

  public static final String WMS_NATURE_AREAS_LAYER = "wms_nature_areas_view";
  public static final String WMS_NATURE_AREAS_NO_NAME_SLD = "wms_nature_areas_no_names_view";
  public static final float WMS_NATURE_AREAS_NO_NAME_OPACITY = 0.3f;

  public static final String WMS_NATURE_AREAS_NAMES_ONLY_SLD = "wms_nature_areas_names_only_view";

  public static final String WMS_EXCEEDING_CALC_DEPOSITION_LAYER = "wms_exceeding_calculation_deposition_results_view";
  public static final String WMS_CALC_DELTA_DEPOSITION_LAYER = "wms_calculations_deposition_results_difference_view";
  public static final float WMS_CALCULATION_LAYER_OPACITY = 0.8f;

  private PAALayerConstants() {
    //constants class
  }

}
