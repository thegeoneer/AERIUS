/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.text.MessageFormat;
import java.util.Locale;

import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PDFColumnPropertiesImpl;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;

/**
 * Data table as used in the PAA export containing emission data for a shipping source.
 */
public class InlandRouteSourceDataTable extends ShippingSourceDataTable<RouteInlandVesselGroup> {

  enum InlandRouteColumn {
    SHIP_CODE(ShippingSourceColumn.SHIP_CODE.getHeaderI18nKey(), 8),
    DESCRIPTION(ShippingSourceColumn.DESCRIPTION.getHeaderI18nKey(), 10),
    NUMBER_OF_SHIPS_ATOB(SubSourceColumn.getHeaderKey("shipmovement_atob"), 7),
    PERCENTAGE_LADEN_ATOB(SubSourceColumn.getHeaderKey("percentage_loaded"), 6),
    NUMBER_OF_SHIPS_BTOA(SubSourceColumn.getHeaderKey("shipmovement_btoa"), 7),
    PERCENTAGE_LADEN_BTOA(SubSourceColumn.getHeaderKey("percentage_loaded"), 6),
    SUBSTANCE(SubSourceColumn.SUBSTANCE.getHeaderI18nKey(), 4),
    EMISSION(SubSourceColumn.EMISSION.getHeaderI18nKey(), 6);

    private final PDFColumnProperties columnProperties;

    private InlandRouteColumn(final String headerKey, final float relativeWidth) {
      this.columnProperties = new PDFColumnPropertiesImpl(headerKey, relativeWidth);
    }

    public PDFColumnProperties getColumnProperties() {
      return columnProperties;
    }

  }

  private static final String PERCENTAGE_POST_TEXT = "%";

  /**
   * Constructor to set header + widths.
   * @param year The year to use.
   * @param emissionSource The InlandShipEmissionValues to make table for.
   * @param locale The locale to use.
   */
  public InlandRouteSourceDataTable(final int year, final InlandRouteEmissionSource emissionSource, final Locale locale) {
    super(year, emissionSource, locale);
  }

  @Override
  protected void addSpecificColumns(final Locale locale) {
    addAToBColumns(locale);
    addBToAColumns(locale);
  }

  private void addAToBColumns(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<RouteInlandVesselGroup>(locale, InlandRouteColumn.NUMBER_OF_SHIPS_ATOB.getColumnProperties()) {

      @Override
      protected String getText(final RouteInlandVesselGroup data) {
        return MessageFormat.format(getText("value_per_time_unit", data.getTimeUnitShipsAtoB()), formatInt(data.getNumberOfShipsAtoBperTimeUnit()));
      }
    });
    addColumnRightAligned(new TextPDFColumn<RouteInlandVesselGroup>(locale, InlandRouteColumn.PERCENTAGE_LADEN_ATOB.getColumnProperties()) {

      @Override
      protected String getText(final RouteInlandVesselGroup data) {
        return formatInt(data.getPercentageLadenAtoB()) + PERCENTAGE_POST_TEXT;
      }
    });
  }

  private void addBToAColumns(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<RouteInlandVesselGroup>(locale, InlandRouteColumn.NUMBER_OF_SHIPS_BTOA.getColumnProperties()) {

      @Override
      protected String getText(final RouteInlandVesselGroup data) {
        return MessageFormat.format(getText("value_per_time_unit", data.getTimeUnitShipsBtoA()), formatInt(data.getNumberOfShipsBtoAperTimeUnit()));

      }
    });
    addColumnRightAligned(new TextPDFColumn<RouteInlandVesselGroup>(locale, InlandRouteColumn.PERCENTAGE_LADEN_BTOA.getColumnProperties()) {

      @Override
      protected String getText(final RouteInlandVesselGroup data) {
        return formatInt(data.getPercentageLadenBtoA()) + PERCENTAGE_POST_TEXT;
      }
    });
  }

  @Override
  protected PDFColumnProperties getShipCodeColumnProperties() {
    return InlandRouteColumn.SHIP_CODE.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getDescriptionColumnProperties() {
    return InlandRouteColumn.DESCRIPTION.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getSubstanceColumnProperties() {
    return InlandRouteColumn.SUBSTANCE.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getEmissionColumnProperties() {
    return InlandRouteColumn.EMISSION.getColumnProperties();
  }

}
