/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.io.IOException;
import java.sql.SQLException;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAALayerConstants;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.PDFImageWMSLayer;
import nl.overheid.aerius.pdf.ReportBlock;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.util.WMSUtils.LayerParam;

/**
 * Contains the core needed for a report block.
 */
public abstract class PAAReportBlock<T extends PAAContext> extends ReportBlock<T> {

  protected static final int PADDING_BETWEEN_TEXT_AND_TABLE = 15;
  protected static final int PADDING_BETWEEN_BLOCKS = 15;
  protected static final int PADDING_BETWEEN_TABLES = 8;

  protected final T paaContext;
  protected final Calculation set1;
  protected final Calculation set2;

  private boolean newPageAfterBlock = true;

  protected PAAReportBlock(final PDFDocumentWrapper pdfDocument, final T paaContext) {
    super(pdfDocument, paaContext);
    this.paaContext = paaContext;
    this.set1 = paaContext.isComparison() ? ((CalculatedComparison) paaContext.getCalculatedScenario()).getCalculationOne()
        : ((CalculatedSingle) paaContext.getCalculatedScenario()).getCalculation();
    this.set2 = paaContext.isComparison() ? ((CalculatedComparison) paaContext.getCalculatedScenario()).getCalculationTwo() : null;
  }

  @Override
  public abstract void generate() throws AeriusException, DocumentException, IOException, SQLException;

  public boolean isNewPageAfterBlock() {
    return newPageAfterBlock;
  }

  public void setNewPageAfterBlock(final boolean newPageAfterBlock) {
    this.newPageAfterBlock = newPageAfterBlock;
  }

  protected PDFImageWMSLayer getDepositionLayer() {
    final PDFImageWMSLayer wmsCalculationLayer;
    if (set2 == null) {
      wmsCalculationLayer = getProductReportWMSLayer(PAALayerConstants.WMS_EXCEEDING_CALC_DEPOSITION_LAYER, null);
      wmsCalculationLayer.setLayerParameter(LayerParam.CALCULATION_ID, set1.getCalculationId());
    } else {
      wmsCalculationLayer = getProductReportWMSLayer(PAALayerConstants.WMS_CALC_DELTA_DEPOSITION_LAYER, null);
      wmsCalculationLayer.setLayerParameter(LayerParam.CALCULATION_A_ID, set1.getCalculationId());
      wmsCalculationLayer.setLayerParameter(LayerParam.CALCULATION_B_ID, set2.getCalculationId());
    }
    wmsCalculationLayer.setOpacity(PAALayerConstants.WMS_CALCULATION_LAYER_OPACITY);
    return wmsCalculationLayer;
  }

  protected PDFImageWMSLayer getNatureAreaLayer() {
    final PDFImageWMSLayer wmsNatureAreasLayerWithoutName = getProductWMSLayer(PAALayerConstants.WMS_NATURE_AREAS_LAYER,
        PAALayerConstants.WMS_NATURE_AREAS_NO_NAME_SLD);
    wmsNatureAreasLayerWithoutName.setOpacity(PAALayerConstants.WMS_NATURE_AREAS_NO_NAME_OPACITY);
    return wmsNatureAreasLayerWithoutName;
  }

  protected PDFImageWMSLayer getNatureAreaNamesOnlyLayer() {
    return getProductWMSLayer(PAALayerConstants.WMS_NATURE_AREAS_LAYER, PAALayerConstants.WMS_NATURE_AREAS_NAMES_ONLY_SLD);
  }

}
