/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PDFColumnPropertiesImpl;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.domain.source.TimeUnit;

/**
 * Data table as used in the PAA export containing emission data for a shipping source.
 */
public class MaritimeMooringRouteTable extends AbstractRouteTable<MaritimeMooringRouteTable.MaritimeRouteTableRowData> {

  enum Column {
    ROUTE_LABEL("ship_maritime_route", "ship_inland_route", 8),
    SHIP_CODE("ship_code", "ship_code", 12),
    VISITS("ship_movements", "ship_visits", 6);

    private final String maritimeHeaderKey;
    private final String inlandHeaderKey;
    private final float relativeWidth;

    private Column(final String maritimeHeaderKey, final String inlandHeaderKey, final float relativeWidth) {
      this.maritimeHeaderKey = "emission_datatable_" + maritimeHeaderKey;
      this.inlandHeaderKey = "emission_datatable_" + inlandHeaderKey;
      this.relativeWidth = relativeWidth;
    }

    public String getHeaderI18nKey(final boolean useMaritimeRoutes) {
      return useMaritimeRoutes ? maritimeHeaderKey : inlandHeaderKey;
    }

    public float getRelativeWidth() {
      return relativeWidth;
    }
  }

  private final boolean useMaritimeRoutes;

  /**
   * Constructor to set header + widths.
   * @param shipEmissionValues The ShipEmissionValues to make table for.
   * @param useMaritimeRoutes Should the table be for maritime routes (true) or for inland routes (false).
   * @param locale The locale to use.
   */
  public MaritimeMooringRouteTable(final MaritimeMooringEmissionSource shipEmissionValues, final boolean useMaritimeRoutes,
      final Locale locale) {
    super(locale);

    this.useMaritimeRoutes = useMaritimeRoutes;
    setData(getRowData(shipEmissionValues));

    addColumns(locale);
  }

  private void addColumns(final Locale locale) {
    addLabelColumn(locale);
    addCodeColumn(locale);
    addVisitsColumn(locale);
  }

  private void addLabelColumn(final Locale locale) {
    addColumn(new TextPDFColumn<MaritimeRouteTableRowData>(locale, getColumnProps(Column.ROUTE_LABEL)) {

      @Override
      protected String getText(final MaritimeRouteTableRowData data) {
        return data.getLabel();
      }
    });
  }

  private void addCodeColumn(final Locale locale) {
    addColumn(new TextPDFColumn<MaritimeRouteTableRowData>(locale, getColumnProps(Column.SHIP_CODE)) {

      @Override
      protected String getText(final MaritimeRouteTableRowData data) {
        return data.getCategory().getDescription();
      }
    });
  }

  private void addVisitsColumn(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<MaritimeRouteTableRowData>(locale, getColumnProps(Column.VISITS)) {

      @Override
      protected String getText(final MaritimeRouteTableRowData data) {
        return MessageFormat.format(getText("value_per_time_unit", data.getTimeUnit()), formatInt(data.getNumberOfShipsPerTimeUnit()));
      }
    });
  }

  private PDFColumnProperties getColumnProps(final Column column) {
    return new PDFColumnPropertiesImpl(column.getHeaderI18nKey(useMaritimeRoutes), column.getRelativeWidth());
  }

  private List<MaritimeRouteTableRowData> getRowData(final MaritimeMooringEmissionSource shipEmissionValues) {
    //convert the vesselgroups to something we can work with for this situation.
    final Map<ShippingRoute, List<MaritimeRouteTableRowData>> rowDataMap = new HashMap<>();
    for (final MooringMaritimeVesselGroup vesselGroupEmissionValues : shipEmissionValues.getEmissionSubSources()) {
      if (useMaritimeRoutes) {
        for (final MaritimeRoute maritimeRoute : vesselGroupEmissionValues.getMaritimeRoutes()) {
          if (rowDataMap.get(maritimeRoute.getRoute()) == null) {
            rowDataMap.put(maritimeRoute.getRoute(), new ArrayList<MaritimeRouteTableRowData>());
          }
          rowDataMap.get(maritimeRoute.getRoute()).add(
              new MaritimeRouteTableRowData(
                  vesselGroupEmissionValues.getCategory(),
                  maritimeRoute.getShipMovementsPerTimeUnit(),
                  maritimeRoute.getTimeUnit()));
        }
      } else {
        if (rowDataMap.get(vesselGroupEmissionValues.getInlandRoute()) == null) {
          rowDataMap.put(vesselGroupEmissionValues.getInlandRoute(), new ArrayList<MaritimeRouteTableRowData>());
        }
        rowDataMap.get(vesselGroupEmissionValues.getInlandRoute()).add(
            new MaritimeRouteTableRowData(
                vesselGroupEmissionValues.getCategory(),
                vesselGroupEmissionValues.getNumberOfShipsPerTimeUnit(),
                vesselGroupEmissionValues.getTimeUnit()));
      }
    }

    return toOrderedTableRows(rowDataMap);
  }

  static class MaritimeRouteTableRowData extends AbstractRouteTable.RouteTableRowData<MaritimeShippingCategory> {

    private final int numberOfShipsPerTimeUnit;
    private final TimeUnit timeUnit;

    MaritimeRouteTableRowData(final MaritimeShippingCategory category, final int numberOfShipsPerTimeUnit, final TimeUnit timeUnit) {
      super(category);
      this.numberOfShipsPerTimeUnit = numberOfShipsPerTimeUnit;
      this.timeUnit = timeUnit;
    }

    public int getNumberOfShipsPerTimeUnit() {
      return numberOfShipsPerTimeUnit;
    }

    public TimeUnit getTimeUnit() {
      return timeUnit;
    }

  }

}
