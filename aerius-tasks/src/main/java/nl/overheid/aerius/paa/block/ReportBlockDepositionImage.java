/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.db.common.CalculationInfoRepository;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.paa.common.PAAAnchors;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAALayerUtil;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAASourcesUtil;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.paa.layer.DepositionLocationLegendTable;
import nl.overheid.aerius.paa.layer.DepositionMarkerLayers;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.PDFImageLayer;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * PAA Report Block: Deposition Image.
 */
public class ReportBlockDepositionImage extends PAAReportBlock<PAAContext> {

  private static final int DEPOSITION_IMAGE_WIDTH = 420;
  private static final int DEPOSITION_IMAGE_HEIGHT = 320;

  private DepositionMarkerLayers depositionMarkerLayers;

  /**
   * @deprecated Emptied out for the current version of the PDF.
   *
   * @param pdfDocument
   * @param paaContext
   */
  @Deprecated
  public ReportBlockDepositionImage(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  private void initData() {
    getDepositionMarkerLayers();
  }

  @Override
  public void generate() throws DocumentException, SQLException, IOException, AeriusException {
//    initData();
//
//    addDepositionImageTable();
//
//    pdfDocument.newPage();
  }

  private void addDepositionImageTable() throws DocumentException, IOException, AeriusException, SQLException {
    final EmissionSourceList emissionSourceList1 = set1.getSources();
    final EmissionSourceList emissionSourceList2 = set2 == null ? null : set2.getSources();
    final BufferedImage depositionImage = getDepositionImage(emissionSourceList1, emissionSourceList2);

    final PDFSimpleTable imageTable = PAAUtil.getDefaultBlockEmptyTable(false);
    PAAUtil.addBlockHeaderCell(imageTable, paaContext.getText("deposition_title"), PAAAnchors.getAnchorReferenceToDepositionMapBlock());

    final int amountOfTotalSources = emissionSourceList1.size() + (emissionSourceList2 == null ? 0 : emissionSourceList2.size());
    if (amountOfTotalSources > PAASharedConstants.CLUSTERED_SOURCES_IMAGES_MAX_SOURCES) {
      imageTable.add(paaContext.getText("clustered_sources_image_source_limit_reached"), PAASharedFonts.DEFAULT_TEXT);
    } else {
      imageTable.setKeepTogether(true);
      imageTable.addImage(depositionImage);

      imageTable.addEmptyCell();
      final String areaName =
          getDepositionMarkerLayers().getMaxMarkerArea() == null ? null : getDepositionMarkerLayers().getMaxMarkerArea().getName();
      imageTable.add(DepositionLocationLegendTable.get(context, emissionSourceList2 != null, areaName, true));
    }

    pdfDocument.addElement(imageTable);
  }

  private BBox determineBoundingBox(final List<EmissionSource> emissionSourceList1, final List<EmissionSource> emissionSourceList2)
      throws AeriusException, SQLException {
    final ArrayList<EmissionSource> allSources = new ArrayList<>();
    for (final EmissionSource source : emissionSourceList1) {
      allSources.add(source);
    }
    if (emissionSourceList2 != null) {
      for (final EmissionSource source : emissionSourceList2) {
        allSources.add(source);
      }
    }

    final BBox bboxAllSources = PAASourcesUtil.getBBoxAllSources(allSources);
    final BBox depositionBBox;
    try (Connection con = getPmf().getConnection()) {
      final BBox calculatedDepositionBBox = CalculationInfoRepository.determineBoundingBox(con, set1.getCalculationId());
      // If there are no receptors found - it will be empty - fall back to the BBox of the sources.
      if (calculatedDepositionBBox == null || calculatedDepositionBBox.getWidth() == 0 || calculatedDepositionBBox.getHeight() == 0) {
        depositionBBox = bboxAllSources;
      } else if (allSources.isEmpty()) {
        depositionBBox = calculatedDepositionBBox;
      } else {
        depositionBBox = GeometryUtil.merge(calculatedDepositionBBox, bboxAllSources);
      }
    }
    return depositionBBox;
  }

  protected BufferedImage getDepositionImage(final EmissionSourceList emissionSourceList1,
      final EmissionSourceList emissionSourceList2) throws AeriusException, SQLException {
    final BBox depositionBBox = determineBoundingBox(emissionSourceList1, emissionSourceList2);

    //sources layer on top.
    final List<PDFImageLayer> imageLayers = PAALayerUtil.getLayersForSources(depositionBBox, DEPOSITION_IMAGE_WIDTH,
        DEPOSITION_IMAGE_HEIGHT, emissionSourceList1, emissionSourceList2, false, PAASharedConstants.MARKER_THRESHOLD);

    //calculation layer below it.
    imageLayers.add(0, getDepositionLayer());

    //Nature layer below that.
    imageLayers.add(0, getNatureAreaLayer());

    //names of the nature areas above
    imageLayers.add(getNatureAreaNamesOnlyLayer());

    imageLayers.add(getDepositionMarkerLayers());

    return PAALayerUtil.getFeaturedImage(paaContext, depositionBBox, DEPOSITION_IMAGE_WIDTH,
        DEPOSITION_IMAGE_HEIGHT, imageLayers.toArray(new PDFImageLayer[imageLayers.size()]));
  }

  private DepositionMarkerLayers getDepositionMarkerLayers() {
    if (depositionMarkerLayers == null) {
      depositionMarkerLayers = new DepositionMarkerLayers(paaContext);
    }
    return depositionMarkerLayers;
  }

}
