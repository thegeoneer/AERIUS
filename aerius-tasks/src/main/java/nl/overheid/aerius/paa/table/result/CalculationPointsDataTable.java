/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.result;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Image;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.paa.common.PAASharedFontsAwt;
import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.PDFAlignment;
import nl.overheid.aerius.pdf.format.PDFFormatUtil;
import nl.overheid.aerius.pdf.marker.Marker;
import nl.overheid.aerius.pdf.marker.MarkerImages;
import nl.overheid.aerius.pdf.marker.MarkerImagesFactory;
import nl.overheid.aerius.pdf.table.DepositionPDFColumn;
import nl.overheid.aerius.pdf.table.ImagePDFColumn;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.pdf.table.cell.AlignCellProcessor;
import nl.overheid.aerius.shared.domain.deposition.PAACalculationPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.util.FormatUtil;

/**
 * Data table as used in the PAA export containing calculation point data.
 */
public class CalculationPointsDataTable extends PAABaseTable<PAACalculationPoint> {

  private static final int MARKERIMAGE_HEIGHT = 18;
  private static final int MARKERIMAGE_WIDTH = 22;
  private static final String MARKERIMAGE_BACKGROUND_COLOR = "FFFE7E";

  enum Column implements PDFColumnProperties {
    CHAR("char", 5),
    NAME("name", 25),
    POSITION("position", 8),
    PROJECT_DEPOSITION("projectdeposition", 10),
    CURRENT_DEPOSITION("situation1", 10),
    PROPOSED_DEPOSITION("situation2", 10),
    COMPARISON("difference", 10),
    DISTANCE_TO_CLOSEST_SOURCE("distancetoclosestsource", 10);

    private final String headerKey;
    private final float relativeWidth;

    private Column(final String headerKey, final float relativeWidth) {
      this.headerKey = "calculationpoint_datatable_" + headerKey;
      this.relativeWidth = relativeWidth;
    }

    @Override
    public String getHeaderI18nKey() {
      return headerKey;
    }

    @Override
    public float getRelativeWidth() {
      return relativeWidth;
    }
  }

  /**
   * Constructor to fill headers and such.
   * @param locale The locale to use.
   * @param cPoints Calculation points to show.
   */
  public CalculationPointsDataTable(final Locale locale, final List<PAACalculationPoint> cPoints) {
    super(locale);

    setData(cPoints);

    addColumns(locale);
  }

  private void addColumns(final Locale locale) {
    addIconColumn();
    addNameColumn(locale);
    addPositionColumn(locale);
    addDepositionColumns(locale);
    addDistanceColumn(locale);
  }

  private void addIconColumn() {
    final ImagePDFColumn<PAACalculationPoint> iconColumn = new ImagePDFColumn<PAACalculationPoint>(Column.CHAR) {

      @Override
      protected Image getImage(final PAACalculationPoint data) {
        final List<Marker> markers = new ArrayList<>();
        markers.add(new Marker(new Point(0, 0), FormatUtil.formatAlphabetical(data.getId()), MARKERIMAGE_BACKGROUND_COLOR));
        try {
          final MarkerImages markerImages = MarkerImagesFactory.createSourceMarkerImage(markers, PAASharedFontsAwt.getFontMarkers(), 2);
          markerImages.setTextColor(Color.BLACK);
          final BufferedImage markerImage = markerImages.getMarkersImage();
          final Image cpImage = Image.getInstance(markerImage, null);
          cpImage.scaleAbsolute(MARKERIMAGE_WIDTH, MARKERIMAGE_HEIGHT);
          return cpImage;
        } catch (final AeriusException | BadElementException | IOException e) {
          throw new IllegalArgumentException("Error generating image for calculation point " + data.getId(), e);
        }
      }

    };
    iconColumn.addProcessor(new AlignCellProcessor(PDFAlignment.MIDDLE));
    addColumn(iconColumn);
  }

  private void addNameColumn(final Locale locale) {
    addColumn(new TextPDFColumn<PAACalculationPoint>(locale, Column.NAME) {

      @Override
      protected String getText(final PAACalculationPoint data) {
        return data.getLabel();
      }

    });
  }

  private void addPositionColumn(final Locale locale) {
    addColumn(new TextPDFColumn<PAACalculationPoint>(locale, Column.POSITION) {

      @Override
      protected String getText(final PAACalculationPoint data) {
        return (int) data.getX() + ", " + (int) data.getY();
      }

    });
  }

  protected void addDepositionColumns(final Locale locale) {
    addColumnRightAligned(new DepositionPDFColumn<PAACalculationPoint>(locale, Column.PROJECT_DEPOSITION) {

      @Override
      protected double getDeposition(final PAACalculationPoint data) {
        return data.getEmissionResult(EmissionResultKey.NOXNH3_DEPOSITION);
      }

    });
 }

  protected void addDistanceColumn(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<PAACalculationPoint>(locale, Column.DISTANCE_TO_CLOSEST_SOURCE) {

      @Override
      protected String getText(final PAACalculationPoint data) {
        return PDFFormatUtil.formatDistance(getLocale(), data.getDistanceToClosestSource());
      }

    });
  }

}
