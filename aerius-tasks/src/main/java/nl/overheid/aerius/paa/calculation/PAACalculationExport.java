/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.calculation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.paa.base.PAAExport;
import nl.overheid.aerius.paa.block.PAAReportBlock;
import nl.overheid.aerius.paa.block.ReportBlockCalculationPoints;
import nl.overheid.aerius.paa.common.PAAReportType;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * A 'Vergunningsbijlage - Eigen Berekening' export.
 */
public class PAACalculationExport extends PAAExport<CalculationPAAContext> {

  /**
   * @param pmf The PMF to use.
   * @param httpClient The HTTP client to use when using stuff like WMS images.
   * @param data The data that should be used when constructing this PAA.
   * @param gmlStrings The GML's to be used for metadata (import reasons).
   * @throws SQLException In case of database errors.
   * @throws AeriusException
   */
  public PAACalculationExport(final PMF pmf, final CloseableHttpClient httpClient, final CalculationInputData data,
      final List<StringDataSource> gmlStrings) throws SQLException, AeriusException {
    super(createContext(pmf, httpClient, data), gmlStrings);
  }

  private static CalculationPAAContext createContext(final PMF pmf, final CloseableHttpClient httpClient, final CalculationInputData data)
      throws SQLException, AeriusException {
    return CalculationPAAContextUtil.createContext(pmf, httpClient, data, SegmentType.PROJECTS, true, PAAReportType.CALCULATION);
  }

  @Override
  protected List<PAAReportBlock<?>> getReportBlocks() {
    final List<PAAReportBlock<?>> reportBlocks = new ArrayList<>();

    reportBlocks.add(new CalculationReportBlockOverview(pdfDocument, getContext()));

    addSourceRecapBlocks(reportBlocks);

    addDepositionBlocks(reportBlocks, true, true, true, false);

    if (CalculationType.CUSTOM_POINTS == getContext().getCalculatedScenario().getOptions().getCalculationType()) {
      reportBlocks.add(new ReportBlockCalculationPoints(pdfDocument, getContext()));
    }

    addSourceBlocks(reportBlocks);
    addDisclaimerBlocks(reportBlocks);
    return reportBlocks;
  }

}
