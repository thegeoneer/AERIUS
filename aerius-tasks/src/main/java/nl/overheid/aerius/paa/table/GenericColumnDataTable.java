/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.table.GenericColumnDataTable.GenericColumnDataRow;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PDFColumnPropertiesImpl;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.pdf.table.cell.ColspanCellProcessor;

/**
 * Datatable containing generic column data.
 */
public abstract class GenericColumnDataTable<E extends Enum<E>> extends PAABaseTable<GenericColumnDataRow<E>> {

  /**
   * Default constructor setting header and such.
   * @param locale The locale to use.
   * @param headerRow Row containing header values (should be strings).
   * If a value for a column is null, the column will be 'added' to the one before it via colspan.
   */
  public GenericColumnDataTable(final Locale locale, final GenericColumnDataRow<E> headerRow) {
    super(locale);

    addColumns(locale, headerRow);
  }

  private void addColumns(final Locale locale, final GenericColumnDataRow<E> headerRow) {
    final Map<E, Integer> colspans = determineColspans(headerRow);
    for (final E column : headerRow.getEnumClass().getEnumConstants()) {
      final String columnHeader = headerRow.getColumn(column) == null ? "" : headerRow.getColumn(column).toString();
      final PDFColumnProperties columnProperties  = new PDFColumnPropertiesImpl(columnHeader);
      final TextPDFColumn<GenericColumnDataRow<E>> tableColumn = new TextPDFColumn<GenericColumnDataRow<E>>(locale, columnProperties) {

        @Override
        protected String getText(final GenericColumnDataRow<E> data) {
          return data.getColumn(column);
        }
      };
      tableColumn.addProcessor(new ColspanCellProcessor(colspans.get(column)));
      addColumn(tableColumn);
    }
  }

  private Map<E, Integer> determineColspans(final GenericColumnDataRow<E> headerRow) {
    final Map<E, Integer> colspanMap = new HashMap<>();
    final List<E> columns = new ArrayList<>();
    for (final E column : headerRow.getEnumClass().getEnumConstants()) {
      columns.add(column);
    }
    //reverse the list, so each colspan gets added to the column before that if null
    Collections.reverse(columns);
    int colspan = 1;
    for (final E column : columns) {
      if (headerRow.getColumn(column) == null) {
        colspanMap.put(column, 0);
        colspan++;
      } else {
        colspanMap.put(column, colspan);
        colspan = 1;
      }
    }
    return colspanMap;
  }

  public void addRow(final GenericColumnDataRow<E> row) {
    getData().add(row);
  }

  protected static String normalize(final String str) {
    return StringUtils.isEmpty(str) ? PAASharedConstants.DEFAULT_EMPTY_CELL_STRING : str;
  }

  public static class GenericColumnDataRow<E extends Enum<E>> {

    private final EnumMap<E, String> columnData;
    private final Class<E> clazz;

    public GenericColumnDataRow(final Class<E> clazz) {
      this.clazz = clazz;
      this.columnData = new EnumMap<>(clazz);
    }

    public String getColumn(final E column) {
      String returnValue = "";
      if (columnData.containsKey(column)) {
        returnValue = columnData.get(column);
      }
      return returnValue;
    }

    public void set(final E column, final String value) {
      columnData.put(column, value);
    }

    public void set(final E column, final Number value) {
      columnData.put(column, value.toString());
    }

    Class<E> getEnumClass() {
      return clazz;
    }
  }

}
