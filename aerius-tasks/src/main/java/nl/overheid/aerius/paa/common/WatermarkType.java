/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

public enum WatermarkType {
  CONCEPT("concept_sticker.png", "frontpage_concept_sticker.png"),
  DEPRECATED("deprecated_sticker.png", "frontpage_deprecated_sticker.png");

  private String defaultFilename;
  private String frontpageFilename;

  private WatermarkType(final String defaultFilename, final String frontpageFilename) {
    this.defaultFilename = defaultFilename;
    this.frontpageFilename = frontpageFilename;
  }

  public String getDefaultFilename() {
    return defaultFilename;
  }

  public String getFrontpageFilename() {
    return frontpageFilename;
  }
}
