/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.paa.table.GenericThreeColumnDataTable;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.format.PDFFormatUtil;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportDepositionInfo;
import nl.overheid.aerius.worker.util.TextUtils;

/**
 * PAA Report SubBlock: Base - Project contribution.
 */
public class ReportSubBlockProjectContribution extends PAAReportBlock<PAAContext> {

  /**
   *
   * @param r The report.
   * @param pdfContext The PDF context.
   * @param paaContext The PAA context.
   */
  public ReportSubBlockProjectContribution(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  public void generate() throws DocumentException, SQLException, IOException {
    final PDFSimpleTable contributionTable = PAAUtil.getDefaultBlockEmptyTable(true);
    PAAUtil.addBlockHeaderCellWithSubHeader(contributionTable, paaContext.getText("base_depositiondemand_title"),
        paaContext.getText(paaContext.isComparison() ? "base_depositiondemand_title_comparison" : "base_depositiondemand_title_single"));

    String natureArea = null;
    Double projectContribution1 = null;
    Double projectContributionDelta = null;
    Double projectContributionExceeding = null;

    final PDFExportAssessmentAreaInfo areaInfo = paaContext.getAreaWithHighestProjectContribution();
    if (areaInfo != null) {
      final PDFExportDepositionInfo depositionInfo = areaInfo.getDepositionInfo();
      natureArea = areaInfo.getName();
      projectContribution1 = paaContext.isComparison() ? depositionInfo.getCurrentDemandMaxDelta() : depositionInfo.getMaxDeltaDemand();
      projectContributionDelta = paaContext.isComparison() ? depositionInfo.getMaxDeltaDemand() : null;
      projectContributionExceeding = depositionInfo.getMaxDeltaDemandOnlyExceeding();
      contributionTable.add(new ProjectContributionFirstRowDataTable(paaContext.getLocale(), natureArea,
          projectContribution1, projectContributionDelta, projectContributionExceeding));
    } else if (CalculationType.CUSTOM_POINTS == paaContext.getCalculatedScenario().getOptions().getCalculationType()) {
      contributionTable.add(new ProjectContributionCustomPointsNoValueTable(paaContext.getLocale()));
    } else {
      contributionTable.add(new ProjectContributionNaturaAreaNoValueTable(paaContext.getLocale()));
    }

    pdfDocument.addElement(contributionTable);
//    if (areaInfo == null) {
//      addFootNote();
//    }
  }

  /**
   * Renders an explanation of empty cells in the contributionTable.
   *
   * @throws DocumentException
   */
  private void addFootNote() throws DocumentException {
    final PDFSimpleTable extraInformationTable = PAAUtil.getDefaultBlockEmptyTable(true);
    extraInformationTable.addEmptyCell();
    extraInformationTable.add(paaContext.getText("base_depositiondemand_footnote"), PAASharedFonts.BLOCK_SUB_HEADER);
    pdfDocument.addElement(extraInformationTable);
  }

  class ProjectContributionFirstRowDataTable extends GenericThreeColumnDataTable {

    /**
     * Default constructor setting header and such.
     * @param locale The locale to use.
     * @param data The data to set.
     */
    public ProjectContributionFirstRowDataTable(final Locale locale, final String natureArea, final Double projectContribution1,
        final Double projectContributionDelta, final Double projectContributionExceeding) {
      super(locale, new GenericThreeColumnDataRow(
          "base_depositiondemand_assessment_area",
          null,
          projectContributionDelta == null ? "base_depositiondemand_contribution" : "base_depositiondemand_difference"));

      final boolean isDelta = projectContributionDelta != null;
      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
      row.set(Column.COLUMN_1, normalize(natureArea));
      row.set(Column.COLUMN_3, format(locale, isDelta ? projectContributionDelta : projectContribution1, isDelta));
      addRow(row);
    }

    /**
     * Outputs the appropriately formatted value or a message when no value is provided.
     * The exceeding value is not used at the moment.
     *
     * @param locale
     * @param value
     * @param delta
     * @return
     */
    private String format(final Locale locale, final Double value, final boolean delta) {
      String returnValue;
      if (value == null) {
         returnValue = null;
      } else {
          returnValue = delta ? PDFFormatUtil.formatDepositionDeltaToSignlessZero(locale, value) : PDFFormatUtil.formatDeposition(locale, value);
     }
      return returnValue;
    }

  }

  class ProjectContributionCustomPointsNoValueTable extends GenericThreeColumnDataTable {

    /**
     * Default constructor setting header and such.
     * Case when no naturearea is applicable for display for a custom points calculation.
     *
     * @param locale The locale to use.
     * @param data The data to set.
     */
    public ProjectContributionCustomPointsNoValueTable(final Locale locale) {
      super(locale, new GenericThreeColumnDataRow(
          "base_depositiondemand_assessment_area",
          null,
          paaContext.isComparison() ? "base_depositiondemand_difference" : "base_depositiondemand_contribution"));
      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();

      row.set(Column.COLUMN_1, TextUtils.getText("default_calculationpoints_no_result_string", locale));
      row.set(Column.COLUMN_3, TextUtils.getText("default_calculationpoints_no_result_string", locale));

      addRow(row);
    }

  }

  class ProjectContributionNaturaAreaNoValueTable extends GenericThreeColumnDataTable {

    /**
     * Default constructor setting header and such.
     * Case when no naturearea is applicable for display for a non-custom points calculation.
     *
     * @param locale The locale to use.
     * @param data The data to set.
     */
    public ProjectContributionNaturaAreaNoValueTable(final Locale locale) {
      super(locale, new GenericThreeColumnDataRow(
          "base_depositiondemand_assessment_area",
          null,
          null));
      final boolean isDelta = paaContext.isComparison();
      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();

      row.set(Column.COLUMN_1,
          TextUtils.getText(isDelta ? "default_natureareas_no_delta_string" : "default_natureareas_no_result_string", locale));
      addRow(row);
    }

  }

}
