/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Polygon;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.pdf.PDFImageLayer;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.worker.util.ImageUtils;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * Util class containing methods to get a complete source image for various shapes.
 */
public final class PAAImageLayerUtil {

  private static final String LAYER_POLYGON_BORDER_COLOR = "000000";
  private static final String LAYER_POLYGON_FILL_COLOR = "91596F";
  private static final int LAYER_POLYGON_FILL_OPACITY = 150;
  private static final BasicStroke LAYER_POLYGON_STROKE = new BasicStroke(1.3f * PAASharedConstants.QUALITY_SETTING);

  private static final String LAYER_LINE_COLOR = "FF0000";
  private static final int LAYER_LINE_OPACITY = 150;
  private static final BasicStroke LAYER_LINE_STROKE = new BasicStroke(1.3f * PAASharedConstants.QUALITY_SETTING);

  private static final String LAYER_TRANSFER_ROUTE_LINE_COLOR = "000000";
  private static final BasicStroke LAYER_TRANSFER_ROUTE_LINE_STROKE = new BasicStroke(1.1f * PAASharedConstants.QUALITY_SETTING,
      BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER,
      10.0f, new float[] {1.1f * PAASharedConstants.QUALITY_SETTING, 10f}, 0.5f);

  private PAAImageLayerUtil() {
    // Default constructor utility class.
  }

  /**
   * Get the layer for given polygon.
   * @param fixedBBox
   * @param width The width (without scale factor).
   * @param height The height (without scale factor).
   * @param poly
   * @return
   */
  static PDFImageLayer getLayerForPolygon(final BBox fixedBBox, final int width, final int height,
      final Polygon poly) {
    final BufferedImage image = new BufferedImage(
        Math.round(width * PAASharedConstants.QUALITY_SETTING),
        Math.round(height * PAASharedConstants.QUALITY_SETTING),
        BufferedImage.TYPE_INT_ARGB);
    final Graphics2D g = image.createGraphics();

    try {
      final LineString ring = poly.getExteriorRing();

      final Coordinate[] points = ring.getCoordinates();
      final int[] xPoints = new int[points.length];
      final int[] yPoints = new int[points.length];
      for (int j = 0; j < points.length; j++) {
        final Coordinate point = points[j];

        final Point pointInImage = PDFUtils.getPositionInMapImage(fixedBBox, new Point(point.x, point.y),
            width, height, PAASharedConstants.QUALITY_SETTING);
        xPoints[j] = (int) pointInImage.getX();
        yPoints[j] = (int) pointInImage.getY();
      }
      g.setColor(ImageUtils.getColorByHexValue(LAYER_POLYGON_FILL_COLOR, LAYER_POLYGON_FILL_OPACITY));
      g.fillPolygon(xPoints, yPoints, xPoints.length);
      g.setColor(ImageUtils.getColorByHexValue(LAYER_POLYGON_BORDER_COLOR));
      g.setStroke(LAYER_POLYGON_STROKE);
      g.drawPolygon(xPoints, yPoints, xPoints.length);
    } finally {
      g.dispose();
    }

    return new PDFImageLayer(image);
  }

  static PDFImageLayer getLayerForTransferRoute(final BBox fixedImageBBox, final int width, final int height,
      final ShippingRoute route) {
    return getLayerForLine(fixedImageBBox, width, height,
        (LineString) GeometryUtil.getGeometryUnsafe(route.getGeometry().getWKT()),
        LAYER_TRANSFER_ROUTE_LINE_COLOR, LAYER_TRANSFER_ROUTE_LINE_STROKE);
  }

  /**
   * Get the layer for given Linestring.
   * @param fixedImageBBox
   * @param width The width (without scale factor).
   * @param height The height (without scale factor).
   * @param line
   * @return
   */
  static PDFImageLayer getLayerForLine(final BBox fixedImageBBox, final int width, final int height,
      final LineString line) {
    return getLayerForLine(fixedImageBBox, width, height, line, LAYER_LINE_COLOR, LAYER_LINE_STROKE);
  }

  static PDFImageLayer getLayerForLine(final BBox fixedImageBBox, final int width, final int height,
      final LineString line,
      final String color, final BasicStroke basicStroke) {
    final BufferedImage image = new BufferedImage(
        Math.round(width * PAASharedConstants.QUALITY_SETTING),
        Math.round(height * PAASharedConstants.QUALITY_SETTING),
        BufferedImage.TYPE_INT_ARGB);
    final Graphics2D g = image.createGraphics();

    try {
      g.setColor(ImageUtils.getColorByHexValue(color, LAYER_LINE_OPACITY));
      g.setStroke(basicStroke);

      Point lastPointInImage = null;
      for (final Coordinate point : line.getCoordinates()) {

        final Point pointInImage = PDFUtils.getPositionInMapImage(fixedImageBBox, new Point(point.x, point.y),
            width, height, PAASharedConstants.QUALITY_SETTING);
        if (lastPointInImage != null) {
          g.drawLine((int) lastPointInImage.getX(), (int) lastPointInImage.getY(), (int) pointInImage.getX(),
              (int) pointInImage.getY());
        }

        lastPointInImage = pointInImage;
      }
    } finally {
      g.dispose();
    }

    return new PDFImageLayer(image);
  }

}
