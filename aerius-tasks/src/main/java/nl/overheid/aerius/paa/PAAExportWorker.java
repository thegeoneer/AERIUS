/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.profile.pas.PASUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.paa.base.PAAExport;
import nl.overheid.aerius.paa.calculation.PAACalculationExport;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.worker.ExportWorker;
import nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData;

/**
 * Permit Application Appendix Worker. Used to generate the PDF that can be used for a permit application.
 */
public class PAAExportWorker implements ExportWorker {

  private static final Logger LOG = LoggerFactory.getLogger(PAAExportWorker.class);

  private static final String FILENAME_PREFIX = "AERIUS_bijlage";
  private static final String FILENAME_EXTENSION = ".pdf";

  private final PMF pmf;
  private final GMLWriter builder;

  /**
   * @param pmf The Persistence Manager Factory to use
   * @throws SQLException
   * @throws IOException
   */
  public PAAExportWorker(final PMF pmf) throws SQLException {
    this.pmf = pmf;
    builder = new GMLWriter(ReceptorGridSettingsRepository.getReceptorGridSettings(pmf));
    //Reference is generated in this class so it can be used again for the PAA.
    builder.setOverrideReference(false);
  }

  @Override
  public void prepare(final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments)
      throws SQLException, AeriusException {
    final String reference = ReferenceUtil.generatePermitReference();
    inputData.getScenario().getMetaData().setReference(reference);
    prepare(getPMF(), inputData);
    // Generate metadata before calculating, because if this fails the calculations are useless.
    LOG.debug("Generating GML for project name: {}", inputData.getScenario().getMetaData().getProjectName());
    backupAttachments.addAll(generateGMLs(inputData));
    forcePAACalculationOptions(inputData);
  }

  private void prepare(final PMF pmf, final CalculationInputData inputData) throws SQLException {
    try (Connection con = pmf.getConnection()) {
      //Make sure the calculation id's are not present, because they are recalculated by worker
      for (final Calculation cs : inputData.getScenario().getCalculations()) {
        cs.setCalculationId(0);
      }
    }
    final CalculationPointList calculationPoints = inputData.getScenario().getCalculationPoints();
    if (isWnbExportType(inputData.getExportType()) && calculationPoints != null) {
      //ensure PAA does not include calculation points in the GML.
      calculationPoints.clear();
    }
  }

  @Override
  public void createFileContent(final File outputFile, final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments,
      final String downloadDirectory, final String correlationId) throws IOException, SQLException, AeriusException {
    LOG.info("Generating PDF for project, reference: {}", inputData.getScenario().getMetaData().getReference());
    try (final OutputStream os = new FileOutputStream(outputFile)) {
      os.write(generatePDF(inputData, backupAttachments));
    }
  }

  @Override
  public ImmediateExportData postProcess(final ImmediateExportData immediateExportData) {
    return immediateExportData;
  }

  public ArrayList<StringDataSource> generateGMLs(final CalculationInputData inputData)
      throws SQLException, AeriusException {
    return builder.writeToStrings(getPMF().getDatabaseVersion(), inputData.getScenario());
  }

  private byte[] generatePDF(final CalculationInputData inputData, final List<StringDataSource> gmls)
      throws SQLException, IOException, AeriusException {
    try (CloseableHttpClient client = createHttpClient()) {
      final PAAExport<?> export;
      switch (inputData.getExportType()) {
      case PAA_OWN_USE:
        export = new PAACalculationExport(getPMF(), client, inputData, gmls);
        break;
      //      case PAA_DEVELOPMENT_SPACES:
      //        export = new PAAPermitDevelopmentSpacesExport(getPMF(), client, inputData, gmls);
      //        break;
      //      case PAA_DEMAND:
      //        export = new PAAPermitDemandExport(getPMF(), client, inputData, gmls);
      //        break;
      default:
        throw new IllegalArgumentException("Unexpected export type encountered in PAAExportWorker: " + inputData.getExportType());
      }
      export.setWatermark(ConstantRepository.getEnum(getPMF(), ConstantsEnum.RELEASE, ReleaseType.class));
      return export.generatePDF();
    }
  }

  protected CloseableHttpClient createHttpClient() {
    return HttpClientManager.getRetryingHttpClient(-1);
  }

  /**
   * When calculating a PAA (NBwet permit) Force NBWet specific calculation options on the scenario such that we know for sure it calculated with the
   * correct values.
   * @param inputData input data
   */
  private void forcePAACalculationOptions(final CalculationInputData inputData) {
    if (isWnbExportType(inputData.getExportType())) {
      inputData.getScenario().setOptions(
          PASUtil.getCalculationSetOptions(inputData.getScenario().getOptions().getTemporaryProjectYears(),
              inputData.getScenario().getOptions().getPermitCalculationRadiusType()));
    }
  }

  @Override
  public MessagesEnum getMailSubject(final CalculationInputData inputData) {
    final MessagesEnum result;

    switch (inputData.getExportType()) {
    //    case PAA_DEVELOPMENT_SPACES:
    //      result = MessagesEnum.PAA_DEVELOPMENT_SPACES_MAIL_SUBJECT;
    //      break;
    //    case PAA_DEMAND:
    //      result = MessagesEnum.PAA_DEMAND_MAIL_SUBJECT;
    //      break;
    case PAA_OWN_USE:
      result = MessagesEnum.PAA_OWN_USE_MAIL_SUBJECT;
      break;
    default:
      throw new IllegalArgumentException("Unexpected export type encountered in PAAExportWorker: " + inputData.getExportType());
    }

    return result;
  }

  @Override
  public void setReplacementTokens(final CalculationInputData inputData, final MailMessageData mailMessageData) {
    mailMessageData.setReplacement(ReplacementToken.PROJECT_NAME,
        inputData.getScenario().getMetaData().getProjectName() == null ? "" : inputData.getScenario().getMetaData().getProjectName());
  }

  @Override
  public MessagesEnum getMailContent(final CalculationInputData inputData) {
    return isWnbExportType(inputData.getExportType()) ? MessagesEnum.PAA_MAIL_CONTENT : MessagesEnum.PAA_OWN_USE_MAIL_CONTENT;
  }

  @Override
  public String getFileName(final CalculationInputData inputData) {
    return FileUtil.getFileName(FILENAME_PREFIX, FILENAME_EXTENSION, inputData.getScenario().getMetaData().getReference(),
        inputData.getCreationDate());
  }

  private boolean isWnbExportType(final ExportType exportType) {
    return Arrays.asList(/* ExportType.PAA_DEVELOPMENT_SPACES, ExportType.PAA_DEMAND */).contains(exportType);
  }

  private PMF getPMF() {
    return pmf;
  }
}
