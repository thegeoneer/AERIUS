/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;

/**
 *
 */
class MeldingInputData extends ExportData {

  private static final long serialVersionUID = -3836207462744491377L;

  private String proposedGML;
  private String currentGML;
  private MeldingInformation meldingInformation;
  private byte[] zipAttachment;

  private String reference;

  public String getProposedGML() {
    return proposedGML;
  }

  public void setProposedGML(final String proposedGML) {
    this.proposedGML = proposedGML;
  }

  public String getCurrentGML() {
    return currentGML;
  }

  public void setCurrentGML(final String currentGML) {
    this.currentGML = currentGML;
  }

  public MeldingInformation getMeldingInformation() {
    return meldingInformation;
  }

  public void setMeldingInformation(final MeldingInformation meldingInformation) {
    this.meldingInformation = meldingInformation;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(final String reference) {
    this.reference = reference;
  }

  public byte[] getZipAttachment() {
    return zipAttachment;
  }

  public void setZipAttachment(final byte[] zipAttachment) {
    this.zipAttachment = zipAttachment;
  }

}
