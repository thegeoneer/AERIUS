/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.register.export.RequestResultFileExporter;
import nl.overheid.aerius.shared.domain.export.PrioritySubProjectExportData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.worker.base.AbstractExportWorker;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 *
 */
public class PrioritySubProjectExportWorker extends AbstractExportWorker<PrioritySubProjectExportData> {

  private static final String FILENAME_PREFIX = "AERIUS_toetsing";
  private static final String FILENAME_EXTENSION = ".zip";

  public PrioritySubProjectExportWorker(final PMF pmf, final DBWorkerConfiguration config, final BrokerConnectionFactory factory) {
    super(pmf, config.getGMLDownloadDirectory(), factory);
  }

  @Override
  protected ImmediateExportData handleExport(final PrioritySubProjectExportData inputData, final ArrayList<StringDataSource> backupAttachments,
      final String correlationId) throws Exception {
    return generateZip(inputData);
  }

  protected ImmediateExportData generateZip(final PrioritySubProjectExportData inputData) throws SQLException, AeriusException, IOException {
    try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final Connection con = getConnection()) {
      final RequestResultFileExporter exporter = new RequestResultFileExporter(getPMF());
      final int filesInZip = exporter.generateSubPriorityAssignOverview(outputStream, con, inputData.getPrioritySubProjectKey());
      final ImmediateExportData immediateExportData = new ImmediateExportData(outputStream.toByteArray());
      immediateExportData.setFilesInZip(filesInZip);
      return immediateExportData;
    }
  }

  @Override
  protected String getFileName(final PrioritySubProjectExportData inputData) {
    return FileUtil.getFileName(FILENAME_PREFIX, FILENAME_EXTENSION, inputData.getPrioritySubProjectKey().getReference(),
        inputData.getCreationDate());
  }

  @Override
  protected void setReplacementTokens(final PrioritySubProjectExportData inputData, final MailMessageData mailMessageData) {
    // NO-OP (not using mails)
  }

}
