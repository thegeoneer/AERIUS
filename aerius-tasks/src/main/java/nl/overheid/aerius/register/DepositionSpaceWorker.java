/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.SyncRepository;
import nl.overheid.aerius.db.common.SyncRepository.DevelopmentSpacesEntry;
import nl.overheid.aerius.db.common.SyncRepository.InitialAvailableDevelopmentSpacesEntry;
import nl.overheid.aerius.db.common.SyncRepository.ReservedDevelopmentSpacesEntry;
import nl.overheid.aerius.db.register.PermitModifyRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.ScheduleUtils;
import nl.overheid.aerius.worker.util.WorkerPMFFactory;

/**
 * Deposition Space Worker. Updates deposition space related stuff daily at a set time, like dequeueing permits.
 */
final class DepositionSpaceWorker extends AbstractRegisterWorker implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(DepositionSpaceWorker.class);

  private final PMF calculatorPMF;

  private final Date startTime;
  private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

  /**
   * @param configuration The configuration containing DB config and start time for this worker.
   * @throws AeriusException
   */
  public DepositionSpaceWorker(final DBWorkerConfiguration registerConfiguration, final DBWorkerConfiguration calculatorConfiguration)
      throws AeriusException {
    super(registerConfiguration);
    try {
      startTime = ScheduleUtils.determineStartTime(registerConfiguration.getDepositionSpaceStartTime());
    } catch (final ParseException e) {
      LOG.error("Error while parsing start time for dequeueing permits.", e);
      throw new RuntimeException(e);
    }
    calculatorPMF = WorkerPMFFactory.createPMF(calculatorConfiguration);
  }

  @Override
  public void run() {
    LOG.info("Scheduling deposition space worker at {}", startTime);
    //start at configured time and rerun each day.
    scheduler.scheduleAtFixedRate(new ActualScheduledTask(), startTime.getTime() - System.currentTimeMillis(), SharedConstants.MILLISECONDS_IN_DAY,
        TimeUnit.MILLISECONDS);
  }

  private class ActualScheduledTask implements Runnable {

    @Override
    public void run() {
      dequeuePermits();

      updateSyncTimestamp();
      // Could combine all 3 sync actions in one transaction but that would decrease performance a bit and the data isn't tied
      // together much (reserved should stay the same throughout, permit threshold should only change on a few occasions)
      syncDevelopmentSpaces();
      syncReservedAndInitialAvailableDevelopmentSpaces();
      syncPermitThresholdValues();
    }

    private void dequeuePermits() {
      LOG.info("Dequeueing permits.");
      try (final Connection con = registerPMF.getConnection()) {
        final UserProfile userProfile = getUserProfile(con);
        PermitModifyRepository.dequeuePermits(con, userProfile);
      } catch (final SQLException | AeriusException e) {
        LOG.error("Error while dequeueing permits.", e);
      }
      LOG.info("Done dequeueing permits.");
    }

    private void syncDevelopmentSpaces() {
      final Synchronizer synchronizer = new Synchronizer("development spaces") {

        @Override
        protected void actualSync(final Connection registerConnection, final Connection calculatorConnection) throws SQLException {
          final List<DevelopmentSpacesEntry> records = SyncRepository.readDevelopmentSpaces(registerConnection);
          SyncRepository.writeDevelopmentSpaces(calculatorConnection, records);
        }

      };
      synchronizer.sync();
    }

    private void syncReservedAndInitialAvailableDevelopmentSpaces() {
      final Synchronizer synchronizer = new Synchronizer("reserved development spaces + initial available development spaces") {

        @Override
        protected void actualSync(final Connection registerConnection, final Connection calculatorConnection) throws SQLException {
          final List<ReservedDevelopmentSpacesEntry> recordsReserved = SyncRepository.readReservedDevelopmentSpaces(registerConnection);
          final List<InitialAvailableDevelopmentSpacesEntry> recordsInitialAvailable =
              SyncRepository.readInitialAvailableDevelopmentSpaces(registerConnection);
          SyncRepository.writeReservedAndInitialAvailableDevelopmentSpaces(calculatorConnection, recordsReserved, recordsInitialAvailable);
        }

      };
      synchronizer.sync();
    }

    private void syncPermitThresholdValues() {
      final Synchronizer synchronizer = new Synchronizer("permit threshold values") {

        @Override
        protected void actualSync(final Connection registerConnection, final Connection calculatorConnection) throws SQLException {
          final Map<Integer, Double> values = SyncRepository.readPermitThresholdValues(registerConnection);
          SyncRepository.writePermitThresholdValues(calculatorConnection, values);
        }

      };
      synchronizer.sync();
    }

    private void updateSyncTimestamp() {
      try (final Connection con = calculatorPMF.getConnection()) {
        ConstantRepository.setString(con, ConstantsEnum.DEVELOPMENT_SPACE_SYNC_TIMESTAMP, String.valueOf(new Date().getTime()));
      } catch (final SQLException e) {
        LOG.error("Error while setting constant for 'development space sync-timestamp'.", e);
      }
    }
  }

  private abstract class Synchronizer {

    private final String dataToSync;

    Synchronizer(final String dataToSync) {
      this.dataToSync = dataToSync;
    }

    private void sync() {
      LOG.info("Synchronizing {}.", dataToSync);
      try (final Connection registerConnection = registerPMF.getConnection();
          final Connection calculatorConnection = calculatorPMF.getConnection()) {
        try {
          //only transaction needed on the calculator connection (target for writing the sync data'
          calculatorConnection.setAutoCommit(false);
          actualSync(registerConnection, calculatorConnection);
          calculatorConnection.commit();
        } catch (final SQLException e) {
          calculatorConnection.rollback();
          throw e;
        }
      } catch (final SQLException e) {
        LOG.error("Error while synchronizing {}.", dataToSync, e);
      }
      LOG.info("Done synchronizing {}.", dataToSync);
    }

    protected abstract void actualSync(final Connection registerConnection, final Connection calculatorConnection) throws SQLException;
  }
}
