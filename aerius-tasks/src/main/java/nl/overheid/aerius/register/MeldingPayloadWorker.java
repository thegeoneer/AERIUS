/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.DownloadInfo;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.base.AbstractDBWorker;
import nl.overheid.aerius.worker.base.AbstractExportWorker;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * Melding Payload worker. Used to handle 'meldingen', does this by wrapping all content in a zip document.
 */
public class MeldingPayloadWorker extends AbstractDBWorker<MeldingPayloadData, DownloadInfo> {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractExportWorker.class);

  private final CalculatorBuildDirector calculatorBuildDirector;
  private final DBWorkerConfiguration configuration;
  private final BrokerConnectionFactory factory;

  /**
   * @param registerPMF The persistence manager factory to use.
   * @param configuration WorkerConfiguration.
   * @param factory to manager connection communication, RabbitMQ.
   */
  public MeldingPayloadWorker(final CalculatorBuildDirector calculatorBuildDirector, final PMF registerPMF, final DBWorkerConfiguration configuration,
      final BrokerConnectionFactory factory) {
    super(registerPMF);
    this.calculatorBuildDirector = calculatorBuildDirector;
    this.configuration = configuration;
    this.factory = factory;
  }

  @Override
  public DownloadInfo run(final MeldingPayloadData payload, final WorkerIntermediateResultSender resultSender, final String correlationId)
      throws Exception {
    final MeldingInputData inputData = new MeldingInputData();

    try (final ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(payload.getPayload()))) {
      ZipEntry zipEntry = null;
      while ((zipEntry = zipStream.getNextEntry()) != null) {
        switch (zipEntry.getName()) {
        case MeldingPayloadData.MELDING_JSON:
          inputData.setMeldingInformation(convertMeldingInformationStream2Result(zip2String(zipStream)));
          break;
        case MeldingPayloadData.CURRENT_GML:
          inputData.setCurrentGML(zip2String(zipStream));
          break;
        case MeldingPayloadData.PROPOSED_GML:
          inputData.setProposedGML(zip2String(zipStream));
          break;
        case MeldingPayloadData.ATTACHMENTS:
          inputData.setZipAttachment(IOUtils.toByteArray(zipStream));
          break;
        default:
          break;
        }
      }

      inputData.setEmailAddress(inputData.getMeldingInformation().getBeneficiaryInformation().getOrganisationEmail());
      inputData.setLocale(LocaleUtils.getLocale(inputData.getMeldingInformation().getLocale()).toString());

      final MeldingWorker meldingworker = new MeldingWorker(calculatorBuildDirector, getPMF(), configuration, factory);
      return meldingworker.run(inputData, resultSender, null).getDownloadInfo();
    }
  }

  private String zip2String(final ZipInputStream zipStream) throws IOException {
    return new String(IOUtils.toByteArray(zipStream), StandardCharsets.UTF_8.name());
  }

  private MeldingInformation convertMeldingInformationStream2Result(final String meldingJson) throws Exception {
    final Gson gson = new Gson();
    try {
      return gson.fromJson(meldingJson, MeldingInformation.class);

    } catch (final JsonSyntaxException e) {
      LOG.error("MeldingInformation object can not be constructed from json. Content was: {}", meldingJson);
      throw e;
    }
  }

}
