/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 *
 */
abstract class AbstractContinuousRegisterWorker extends AbstractRegisterWorker implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractContinuousRegisterWorker.class);

  private final AtomicBoolean running = new AtomicBoolean(false);
  private final int waitTimeInMinutes;

  protected AbstractContinuousRegisterWorker(final DBWorkerConfiguration configuration, final int waitTimeInMinutes) throws AeriusException {
    super(configuration);
    this.waitTimeInMinutes = waitTimeInMinutes;
  }

  @Override
  public final void run() {
    running.set(true);
    LOG.info("Starting continuous worker {}", getWorkerName());
    while (running.get()) {
      try {
        if (!process()) {
          Thread.sleep(TimeUnit.MINUTES.toMillis(waitTimeInMinutes));
        }
      } catch (final InterruptedException e) {
        LOG.error("{} interrupted while processing", getWorkerName(), e);
      }
    }
  }

  protected abstract String getWorkerName();

  public abstract boolean process();

  /**
   * Finished the current process, if busy and then shuts down.
   * @return running state, should always be false. Process could still be running because it's a lazy shutdown.
   * @throws InterruptedException interrupted
   */
  public boolean shutDown() throws InterruptedException {
    running.set(false);
    return running.get();
  }

}
