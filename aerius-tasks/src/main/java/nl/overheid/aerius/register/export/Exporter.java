/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register.export;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.ZipFileMaker;

/**
 * Base class for exporting generated files in a ZIP.
 */
class Exporter {

  private static final Logger LOG = LoggerFactory.getLogger(Exporter.class);

  protected final Date exportDate = new Date();

  void generateZip(final OutputStream out, final List<FileWrapper> fileWrappers) throws IOException {
    generateZip(out, fileWrappers, true);
  }

  /**
   * @param out The OutputStream to write the zipfile to.
   * @param fileWrappers The files to generate a zip for.
   * @param cleanup boolean to indicate if we want to delete the file(s) that we added to the zip.
   * @throws IOException In case of I/O errors.
   */
  void generateZip(final OutputStream out, final List<FileWrapper> fileWrappers, final boolean cleanup)
      throws IOException {
    try (final ZipFileMaker zipFileMaker = new ZipFileMaker(out)) {
      for (final FileWrapper wrapper : fileWrappers) {
        zipFileMaker.add(wrapper.getExportName(), wrapper.getFile());
      }
    /* Clean up files, even if it failed generating the zip. */
    } finally {
      if (cleanup) {
        for (final FileWrapper wrapper : fileWrappers) {
          if (!wrapper.getFile().delete()) {
            LOG.warn("Cleaning up generated file failed: {}", wrapper.getFile().getAbsolutePath());
          }
        }
      }
    }
  }

  String getFileName(final String name, final String optionalPart, final String extension) {
    return FileUtil.getFileName(name, optionalPart, exportDate) + "." + extension;
  }

}
