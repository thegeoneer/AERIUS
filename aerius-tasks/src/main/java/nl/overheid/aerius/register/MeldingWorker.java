/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.profile.pas.PASUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.db.register.NoticeModifyRepository;
import nl.overheid.aerius.db.register.NoticeRepository;
import nl.overheid.aerius.db.register.RequestModifyRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.MailTo;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.paa.melding.PAAMeldingExport;
import nl.overheid.aerius.shared.domain.DownloadInfo;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.NBWetCalculationUtil;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.base.AbstractExportWorker;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * Melding worker. Used to handle 'meldingen', doing stuff in the register DB and mailing the user about it.
 */
class MeldingWorker extends AbstractExportWorker<MeldingInputData> {

  private static final Logger LOG = LoggerFactory.getLogger(MeldingWorker.class);

  private static final String FILENAME_PREFIX = "AERIUS_melding";
  private static final String FILENAME_EXTENSION = ".pdf";
  private static final String MIMETYPE_ZIP = "application/zip";

  private static final String MELDING_WORKER_USERNAME = "melding_worker";
  private static final EnumSet<Reason> MELDING_NOT_REGISTERED = EnumSet.of(Reason.MELDING_DOES_NOT_FIT, Reason.MELDING_ABOVE_PERMIT_THRESHOLD);

  private final CalculatorBuildDirector calculatorBuildDirector;

  /**
   * @param calculator
   * @param pmf The PMF to use.
   * @param config The config for the worker.
   * @param factory Broker connection factory, used to setup a taskmanager client.
   * @throws SQLException
   */
  public MeldingWorker(final CalculatorBuildDirector calculatorBuildDirector, final PMF pmf, final DBWorkerConfiguration config,
      final BrokerConnectionFactory factory) {
    super(pmf, config.getPAADownloadDirectory(), factory);
    this.calculatorBuildDirector = calculatorBuildDirector;
  }

  @Override
  protected String getFileName(final MeldingInputData inputData) {
    return FileUtil.getFileName(FILENAME_PREFIX, FILENAME_EXTENSION, getReference(inputData), null);
  }

  @Override
  protected ImmediateExportData handleExport(final MeldingInputData inputData, final ArrayList<StringDataSource> backupAttachments,
      final String correlationId) throws Exception {
    // ensure GMLs are available to mail as backup in case something goes wrong.
    addBackups(backupAttachments, inputData);

    final byte[] pdf = doMelding(inputData, backupAttachments);

    final String downloadURL = saveFile(inputData, pdf);

    informAuthority(inputData, downloadURL);

    // Send attachments to authority if they are present
    if (inputData.getZipAttachment() != null && inputData.getZipAttachment().length > 0) {
      sendDocumentsToAuthority(inputData, inputData.getReference());
    }

    return new ImmediateExportData(downloadURL);
  }

  private String getReference(final MeldingInputData inputData) {
    if (StringUtils.isBlank(inputData.getReference()) || !ReferenceUtil.validateMeldingReference(inputData.getReference())) {
      inputData.setReference(ReferenceUtil.generateMeldingReference());
    }
    return inputData.getReference();
  }

  private void addBackups(final ArrayList<StringDataSource> backupAttachments, final MeldingInputData inputData) throws IOException {
    LOG.info("Adding backups for project(name={})", inputData.getMeldingInformation().getBeneficiaryInformation().getOrganisationName());
    if (!StringUtils.isEmpty(inputData.getCurrentGML())) {
      backupAttachments.add(new StringDataSource(inputData.getCurrentGML(), GMLWriter.CURRENT_GML, GMLWriter.GML_MIMETYPE));
    }
    if (!StringUtils.isEmpty(inputData.getProposedGML())) {
      backupAttachments.add(new StringDataSource(inputData.getProposedGML(), GMLWriter.PROPOSED_GML, GMLWriter.GML_MIMETYPE));
    }
  }

  private byte[] doMelding(final MeldingInputData inputData, final ArrayList<StringDataSource> backupAttachments) throws Exception {
    final InsertRequestFile requestFile;
    ImportOutput importOutput = null;
    Notice notice = null;
    try {
      // Convert the GML(s) to a scenario. This will set the proper reference.
      importOutput = getImportOutput(inputData);
      // Now the reference should be extracted from the input GML, so use it for the rest.
      final String reference = getReference(inputData);
      LOG.info("Reference for project(name={}) is: {}", inputData.getMeldingInformation().getBeneficiaryInformation().getOrganisationName(),
          reference);

      // calculate
      calculateScenario(importOutput.getCalculatedScenario(), reference);

      // generate PDF
      requestFile = generatePDF(importOutput.getCalculatedScenario(), inputData, reference, backupAttachments);

      // do the melding (and insert into database).
      notice = RegisterImportUtil.convertToNotice(importOutput);
      // insert the melding as initialized -> queued.
      insertMelding(notice, requestFile, reference, importOutput.getCalculatedScenario());
      // Now do the actual melding -> assigned.
      assignMelding(notice, reference);
    } catch (final Exception e) {
      cleanupOnError(notice, importOutput);
      throw e;
    }
    return requestFile.getFileContent();
  }

  private void cleanupOnError(final Notice notice, final ImportOutput importOutput) {
    try (final Connection con = getConnection()) {
      if (notice != null && notice.getId() > 0) {
        NoticeModifyRepository.deleteMelding(con, notice.getId(), getUserProfile(con));
      }
      if (importOutput != null) {
        // deleting notice should already clean calculations, but the exception could occurs before the notice is added
        // or before the notice is linked to the calculations, so ensure we're removing any leftovers.
        for (final Calculation calculation : importOutput.getCalculatedScenario().getCalculations()) {
          CalculationRepository.removeCalculation(con, calculation.getCalculationId());
        }
      }
    } catch (final Exception e) {
      // no-op at this point, just log.
      LOG.info("Exception while cleaning up", e);
    }
  }

  private ImportOutput getImportOutput(final MeldingInputData inputData) throws SQLException, IOException, AeriusException {
    LOG.info("Converting melding to scenario for project(name={})",
        inputData.getMeldingInformation().getBeneficiaryInformation().getOrganisationName());
    final Importer importer = new Importer(getPMF(), LocaleUtils.getLocale(inputData.getLocale()));
    // do not import calculation points.
    importer.setIncludeCalculationPoints(false);
    final CalculatedScenario scenario;
    if (!StringUtils.isEmpty(inputData.getCurrentGML())) {
      readGMLFile(importer, inputData.getCurrentGML());
    }
    final ImportResult importResult = readGMLFile(importer, inputData.getProposedGML());
    scenario = CalculatedScenarioUtil.toCalculatedScenario(importResult);
    scenario.setOptions(PASUtil.getCalculationSetOptions(importResult.getImportedTemporaryPeriod(), importResult.getPermitCalculationRadiusType()));
    for (final Calculation calculation : scenario.getCalculations()) {
      calculation.setYear(importResult.getImportedYear());
    }
    scenario.getMetaData().setCorporation(inputData.getMeldingInformation().getBeneficiaryInformation().getOrganisationName());
    if (StringUtils.isEmpty(scenario.getMetaData().getDescription())) {
      scenario.getMetaData().setDescription(inputData.getMeldingInformation().getSubstantiation());
    }
    inputData.setReference(importResult.getMetaData().getReference());
    scenario.getMetaData().setReference(getReference(inputData));

    final ImportOutput output = new ImportOutput();
    output.setCalculatedScenario(scenario);
    output.setImportedYear(importResult.getImportedYear());
    output.setImportedTemporaryPeriod(importResult.getImportedTemporaryPeriod());
    output.setVersion(importResult.getVersion());
    output.setDatabaseVersion(importResult.getDatabaseVersion());
    output.setSrid(importResult.getSrid());
    output.setType(importResult.getType());
    return output;
  }

  private ImportResult readGMLFile(final Importer importer, final String gml) throws IOException, AeriusException {
    try (final InputStream inputStream = new ByteArrayInputStream(gml.getBytes(StandardCharsets.UTF_8))) {
      return importer.convertInputStream2ImportResult("file.gml", inputStream);
    }
  }

  protected void calculateScenario(final CalculatedScenario scenario, final String reference) throws Exception {
    LOG.info("Calculating and storing results for melding, reference: {}", reference);
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setScenario(scenario);
    inputData.setQueueName(QueueEnum.MELDING.getQueueName());

    calculatorBuildDirector.construct(inputData, reference, null).calculate();
    LOG.info("Finished calculating for melding, reference: {}", reference);
  }

  private int insertMelding(final Notice notice, final InsertRequestFile insertRequestFile, final String reference, final CalculatedScenario cs)
      throws AeriusException, SQLException {
    try (final Connection con = getPMF().getConnection()) {
      // Add the melding
      final UserProfile userProfile = getUserProfile(con);
      final int meldingId = NoticeModifyRepository.insertMelding(con, notice, userProfile, insertRequestFile);
      notice.setId(meldingId);
      LOG.info("Melding persisted in database as INITIALIZED: {}. Melding ID: {}", reference, meldingId);
      insertCalculatedSituations(con, notice, cs);
      RequestModifyRepository.updateRequestToQueued(con, notice, userProfile);
      LOG.info("Added calculations and changed status to QUEUED in database: {}. Melding ID: {}", reference, meldingId);
      return meldingId;
    }
  }

  private void insertCalculatedSituations(final Connection connection, final Notice notice, final CalculatedScenario cs) throws SQLException {
    if (cs instanceof CalculatedComparison) {
      RequestModifyRepository.insertSituationCalculation(connection, SituationType.CURRENT, ((CalculatedComparison) cs).getCalculationIdOne(),
          notice);
      RequestModifyRepository.insertSituationCalculation(connection, SituationType.PROPOSED, ((CalculatedComparison) cs).getCalculationIdTwo(),
          notice);
    } else if (cs instanceof CalculatedSingle) {
      RequestModifyRepository.insertSituationCalculation(connection, SituationType.PROPOSED, ((CalculatedSingle) cs).getCalculationId(), notice);
    }
  }

  private void assignMelding(final Notice notice, final String reference) throws SQLException, IOException, AeriusException {
    LOG.info("Assigning melding in database: {}", reference);

    try (final Connection con = getPMF().getConnection()) {
      // Add the melding
      NoticeModifyRepository.assignMelding(con, notice, getUserProfile(con));
      LOG.info("Melding assigned in database: {}. Melding ID: {}", reference, notice.getId());
      // After adding each melding, update the permit threshold values if needed. Same transaction!
      NoticeModifyRepository.updatePermitThresholdValues(con);
      LOG.info("Updated permit threshold values");
    }
  }

  private InsertRequestFile generatePDF(final CalculatedScenario scenario, final MeldingInputData meldingInputData, final String reference,
      final List<StringDataSource> gmls) throws IOException, SQLException, AeriusException {
    LOG.info("[reference:{}] Generating melding pdf", reference);
    byte[] fileContent;
    try (CloseableHttpClient client = createHttpClient()) {
      final PAAMeldingExport export =
          new PAAMeldingExport(getPMF(), client, scenario, meldingInputData, gmls, meldingInputData.getMeldingInformation());
      export.setWatermark(ConstantRepository.getEnum(getPMF(), ConstantsEnum.RELEASE, ReleaseType.class));
      fileContent = export.generatePDF();
    }
    LOG.info("[reference:{}] Melding pdf generated", reference);
    final InsertRequestFile insertRequestFile = new InsertRequestFile();
    insertRequestFile.setFileFormat(FileFormat.PDF);
    insertRequestFile.setRequestFileType(RequestFileType.APPLICATION);
    insertRequestFile.setFileContent(fileContent);
    return insertRequestFile;
  }

  protected CloseableHttpClient createHttpClient() {
    return HttpClientManager.getRetryingHttpClient(-1);
  }

  private static Point determineCentroid(final int srid, final CalculatedScenario scenario) throws AeriusException {
    final List<WKTGeometry> geometries = new ArrayList<>();
    for (final EmissionSource source : getProposedSources(scenario)) {
      geometries.add(source.getGeometry());
    }
    return GeometryUtil.determineCentroid(srid, geometries);
  }

  private static EmissionSourceList getProposedSources(final CalculatedScenario scenario) throws AeriusException {
    final EmissionSourceList list = NBWetCalculationUtil.getProposedSources(scenario);
    if (list == null) {
      LOG.error("Could not get proposed sources from scenario with class {}", scenario.getClass());
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    return list;
  }

  @Override
  protected MessagesEnum getMailSubject(final MeldingInputData inputData) {
    return MessagesEnum.MELDING_REGISTERED_USER_MAIL_SUBJECT;
  }

  @Override
  protected MessagesEnum getMailContent(final MeldingInputData inputData) {
    if (hasAnyAttachments(inputData)) {
      return MessagesEnum.MELDING_REGISTERED_USER_MAIL_WITH_ATTACHMENTS_CONTENT;
    } else {
      return MessagesEnum.MELDING_REGISTERED_USER_MAIL_CONTENT;
    }
  }

  @Override
  protected void setReplacementTokens(final MeldingInputData inputData, final MailMessageData mailMessageData) {
    final Authority authority = RegisterMailUtil.getAuthorityForRequest(getPMF(), inputData.getReference());
    setReplacementTokens(inputData, mailMessageData, authority);
    RegisterMailUtil.setAuthoritySignatureReplacementTokens(authority, mailMessageData);
  }

  private void setReplacementTokens(final MeldingInputData inputData, final MailMessageData mailMessageData, final Authority authority) {
    setDefaultReplacementTokens(inputData, mailMessageData);
    mailMessageData.setReplacement(ReplacementToken.PROJECT_NAME,
        inputData.getMeldingInformation().getBeneficiaryInformation().getOrganisationName());
    mailMessageData.setReplacement(ReplacementToken.AERIUS_REFERENCE, inputData.getReference());
    RegisterMailUtil.setAuthorityReplacementTokens(authority, mailMessageData);
  }

  @Override
  protected MailTo getMailTo(final MeldingInputData inputData) {
    final MailTo mailTo = new MailTo(inputData.getMeldingInformation().getBeneficiaryInformation().getOrganisationEmail());
    if (inputData.getMeldingInformation().isBenefactorEmailConfirmation()) {
      mailTo.addCc(inputData.getMeldingInformation().getBenefactorInformation().getOrganisationEmail());
    }
    if (inputData.getMeldingInformation().isExecutorEmailConfirmation()) {
      mailTo.addCc(inputData.getMeldingInformation().getExecutorInformation().getOrganisationEmail());
    }
    return mailTo;
  }

  private void informAuthority(final MeldingInputData inputData, final String downloadURL) throws SQLException, IOException {
    final Authority authority = RegisterMailUtil.getAuthorityForRequest(getPMF(), inputData.getReference());
    if (authority != null) {
      final MailMessageData mailMessageData = new MailMessageData(MessagesEnum.MELDING_AUTHORITY_MAIL_SUBJECT,
          MessagesEnum.MELDING_AUTHORITY_MAIL_CONTENT, getLocale(inputData), new MailTo(authority.getEmailAddress()));

      setReplacementTokens(inputData, mailMessageData, authority);
      mailMessageData.setReplacement(ReplacementToken.DOWNLOAD_LINK, downloadURL);

      sendMail(mailMessageData);
    }
  }

  private void sendDocumentsToAuthority(final MeldingInputData inputData, final String reference) throws IOException, AeriusException {
    final String authorityCentralMail = RegisterMailUtil.getAuthorityCentralMail(getPMF());

    LOG.info("Sending attachments from melding to reference {}", reference);
    final HashMap<String, Boolean> sendResult = sendDocumentsToAuthority(authorityCentralMail, inputData);
    sendOverviewToAuthority(authorityCentralMail, sendResult, inputData);
  }

  private void sendOverviewToAuthority(final String authorityCentralMail, final HashMap<String, Boolean> sendResult, final MeldingInputData inputData)
      throws IOException {
    final Authority authority = RegisterMailUtil.getAuthorityForRequest(getPMF(), inputData.getReference());
    final MailMessageData mailMessageData = new MailMessageData(MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_STATUS_MAIL_SUBJECT,
        MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_STATUS_MAIL_CONTENT, getLocale(inputData), getMailToAuthorities(authorityCentralMail, authority));
    setReplacementTokens(inputData, mailMessageData, authority);

    if (!sendResult.isEmpty()) {
      final StringBuilder filesStatus = buildFileStatusString(sendResult);
      mailMessageData.setReplacement(ReplacementToken.MELDING_ATTACHMENTS_RESULT_LIST, filesStatus.toString());
      mailMessageData.setDbReplacement(ReplacementToken.SEND_OK, MessagesEnum.MELDING_ATTACHMENTS_SEND_STATUS_OK);
      mailMessageData.setDbReplacement(ReplacementToken.SEND_FAIL, MessagesEnum.MELDING_ATTACHMENTS_SEND_STATUS_FAIL);
    }

    sendMail(mailMessageData);
  }

  private StringBuilder buildFileStatusString(final HashMap<String, Boolean> sendResult) {
    final StringBuilder filesStatus = new StringBuilder();
    int count = 1;
    for (final Entry<String, Boolean> entry : sendResult.entrySet()) {
      LOG.info("Attachment send {} status [{}]", entry.getKey(), entry.getValue());
      filesStatus.append("<p>").append(count).append(' ').append(entry.getKey()).append(" ([");
      filesStatus.append(entry.getValue() ? ReplacementToken.SEND_OK.name() : ReplacementToken.SEND_FAIL.name());
      filesStatus.append("])</p>");
      count++;
    }
    return filesStatus;
  }

  private HashMap<String, Boolean> sendDocumentsToAuthority(final String authorityCentralMail, final MeldingInputData inputData)
      throws AeriusException {
    final HashMap<String, Boolean> result = new HashMap<>();
    final Authority authority = RegisterMailUtil.getAuthorityForRequest(getPMF(), inputData.getReference());
    final MailTo mailTo = getMailToAuthorities(authorityCentralMail, authority);

    final int totalNrOfFiles = determineNumberOfFiles(inputData);

    try (final ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(inputData.getZipAttachment()))) {
      ZipEntry zipEntry = null;
      int currentFileNr = 1;
      while ((zipEntry = zipStream.getNextEntry()) != null) {
        final MailMessageData mailMessageData = new MailMessageData(MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_MAIL_SUBJECT,
            MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_MAIL_CONTENT, getLocale(inputData), mailTo);
        setReplacementTokens(inputData, mailMessageData, authority);

        addSubstantiation(inputData.getMeldingInformation().getSubstantiation(), mailMessageData);

        LOG.info("Add attachment: {}", zipEntry.getName());
        mailMessageData.getAttachments().add(new StringDataSource(IOUtils.toByteArray(zipStream), zipEntry.getName(), MIMETYPE_ZIP));

        mailMessageData.setReplacement(ReplacementToken.DOCUMENT_MAIL_NR, String.valueOf(currentFileNr));
        mailMessageData.setReplacement(ReplacementToken.MAX_DOCUMENT_MAIL_NR, String.valueOf(totalNrOfFiles));

        result.put(zipEntry.getName(), sendMail(mailMessageData));

        LOG.info("Document send to authority: {} of {}. Name {}. Size {}.", currentFileNr, totalNrOfFiles, zipEntry.getName(), zipEntry.getSize());
        currentFileNr++;
      }

    } catch (final IOException e) {
      LOG.error("Error sending melding attachments for melding {}.", getReference(inputData), e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    return result;
  }

  private MailTo getMailToAuthorities(final String authorityCentral, final Authority authority) {
    final MailTo mailTo = new MailTo(authorityCentral);
    if (authority != null) {
      mailTo.addBcc(authority.getEmailAddress());
    }
    return mailTo;
  }

  private void addSubstantiation(final String substantiation, final MailMessageData mailMessageData) {
    if (!StringUtils.isEmpty(substantiation)) {
      mailMessageData.setDbReplacement(ReplacementToken.MAIL_SUBSTANTIATION, MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_MAIL_SUBSTANTIATION);
      mailMessageData.setReplacement(ReplacementToken.MELDING_SUBSTANTIATION, substantiation);
    } else {
      mailMessageData.setReplacement(ReplacementToken.MAIL_SUBSTANTIATION, "");
    }
  }

  private int determineNumberOfFiles(final MeldingInputData inputData) {
    int maxFiles = 0; // count files in zipcontent
    try (final ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(inputData.getZipAttachment()))) {
      while (zipStream.getNextEntry() != null) {
        maxFiles++;
      }
    } catch (final IOException e) {
      LOG.error("Error determining number of user-supplied documents for melding {} ", getReference(inputData), e);
    }
    return maxFiles;
  }

  @Override
  protected DownloadInfo sendMailToUser(final MeldingInputData inputData, final String downloadURL,
      final ArrayList<StringDataSource> backupAttachments) throws IOException, SQLException, AeriusException {
    final DownloadInfo di;

    if (downloadURL == null) {
      LOG.error("Saving file to download directory failed after all the hard work done to generate it.. Sending error message instead");
      sendMailToUserOnError(inputData, null, backupAttachments);
      di = null;
    } else {
      final MailMessageData mailMessageData = getMailMessageData(inputData);
      setAttachtmentsReplaceTokens(inputData, mailMessageData);
      LOG.trace("Adding download link: {}", downloadURL);
      mailMessageData.setReplacement(ReplacementToken.DOWNLOAD_LINK, downloadURL);

      setReplacementTokens(inputData, mailMessageData);
      di = new DownloadInfo();
      di.setEmailSuccess(sendMail(mailMessageData));
      di.setUrl(downloadURL);
      di.setEmailAddress(inputData.getEmailAddress());
    }
    return di;

  }

  private void setAttachtmentsReplaceTokens(final MeldingInputData inputData, final MailMessageData mailMessageData) throws AeriusException {
    if (hasAnyAttachments(inputData)) {
      LOG.trace("Adding attachemts list count: {}", inputData.getZipAttachment().length);
      final StringBuilder fileList = new StringBuilder();
      try (final ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(inputData.getZipAttachment()))) {
        ZipEntry zipEntry = null;
        while ((zipEntry = zipStream.getNextEntry()) != null) {
          LOG.info("Add attachment: {}", zipEntry.getName());
          fileList.append("<p>- ").append(zipEntry.getName());
          fileList.append("</p>");
        }
        mailMessageData.setReplacement(ReplacementToken.MELDING_ATTACHMENTS_RESULT_LIST, fileList.toString());
      } catch (final IOException e) {
        LOG.error("Error sending melding attachments for melding {}.", getReference(inputData), e);
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
    }
  }

  @Override
  protected void sendMailToUserOnError(final MeldingInputData inputData, final Exception e, final ArrayList<StringDataSource> attachments)
      throws AeriusException {
    sendErrorMailToUser(inputData, e, attachments);
    sendErrorMailToAuthority(inputData, e, attachments);
  }

  private void sendErrorMailToUser(final MeldingInputData inputData, final Exception e, final ArrayList<StringDataSource> attachments)
      throws AeriusException {
    final MailMessageData mailMessageData = messageErrorMailToUser(inputData, e);
    setAttachtmentsReplaceTokens(inputData, mailMessageData);
    sendErrorMail(mailMessageData, inputData, e, attachments);
  }

  private MailMessageData messageErrorMailToUser(final MeldingInputData inputData, final Exception e) {
    final MailMessageData mailMessageData;

    final MessagesEnum meldingNotRegisteredContent;
    if (hasAnyAttachments(inputData)) {
      meldingNotRegisteredContent = MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_ATTACHMENTSLIST_CONTENT;
    } else {
      meldingNotRegisteredContent = MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_CONTENT;
    }
    if (e instanceof AeriusException && MELDING_NOT_REGISTERED.contains(((AeriusException) e).getReason())) {
      mailMessageData = new MailMessageData(MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_SUBJECT,
          meldingNotRegisteredContent, getLocale(inputData), getMailTo(inputData));
      mailMessageData.setReplacement(ReplacementToken.TECHNICAL_REASON_NOT_REGISTERED_MELDING, "");
    } else {
      mailMessageData = new MailMessageData(MessagesEnum.MELDING_NOT_REGISTERED_TECHNICAL_ISSUE_USER_MAIL_SUBJECT,
          meldingNotRegisteredContent, getLocale(inputData), getMailTo(inputData));
      mailMessageData.setDbReplacement(ReplacementToken.TECHNICAL_REASON_NOT_REGISTERED_MELDING,
          MessagesEnum.MELDING_NOT_REGISTERED_TECHNICAL_REASON);
    }
    return mailMessageData;
  }

  /*
   * Test if we have any attachments in inputData.
   * inputData the inputData object.
   * return boolean if we have attachments.
   */
  private boolean hasAnyAttachments(final MeldingInputData inputData) {
    return inputData.getZipAttachment() != null && inputData.getZipAttachment().length > 0;
  }

  private void sendErrorMailToAuthority(final MeldingInputData inputData, final Exception e, final ArrayList<StringDataSource> attachments) {
    try {
      final Authority authority = getAuthory(inputData);
      final MailMessageData mailMessageData = new MailMessageData(MessagesEnum.MELDING_NOT_REGISTERED_AUTHORITY_MAIL_SUBJECT,
          MessagesEnum.MELDING_NOT_REGISTERED_AUTHORITY_MAIL_CONTENT, getLocale(inputData), new MailTo(authority.getEmailAddress()));

      RegisterMailUtil.setAuthorityReplacementTokens(authority, mailMessageData);

      sendErrorMail(mailMessageData, inputData, e, attachments);
    } catch (final AeriusException | SQLException | IOException e1) {
      LOG.error("Error sending mail to authority on rejected melding. Reference {}", getReference(inputData), e1);
    }
  }

  private Authority getAuthory(final MeldingInputData inputData) throws SQLException, AeriusException, IOException {
    // at this point the notice isn't available in the database. Determine the authority based on the scenario.
    final CalculatedScenario scenario = getImportOutput(inputData).getCalculatedScenario();
    try (Connection connection = getPMF().getConnection()) {
      final int srid = ReceptorGridSettingsRepository.getSrid(connection);
      return NoticeRepository.getAuthorityForNotice(connection, determineCentroid(srid, scenario));
    }
  }

  private void sendErrorMail(final MailMessageData mailMessageData, final MeldingInputData inputData, final Exception e,
      final ArrayList<StringDataSource> attachments) {
    setErrorReplacementTokens(mailMessageData, inputData, e);
    mailMessageData.getAttachments().addAll(attachments);
    sendMail(mailMessageData);
  }

  @Override
  protected void setErrorReplacementTokens(final MailMessageData mailMessageData, final MeldingInputData inputData, final Exception e) {
    super.setErrorReplacementTokens(mailMessageData, inputData, e);
    mailMessageData.setReplacement(ReplacementToken.AERIUS_REFERENCE, inputData.getReference());
    mailMessageData.setReplacement(ReplacementToken.PROJECT_NAME,
        inputData.getMeldingInformation().getBeneficiaryInformation().getOrganisationName());
    mailMessageData.setDbReplacement(ReplacementToken.REASON_NOT_REGISTERED_MELDING, getErrorMessageEnum(e));
    try {
      RegisterMailUtil.setAuthoritySignatureReplacementTokens(getAuthory(inputData), mailMessageData);
    } catch (SQLException | AeriusException | IOException e1) {
      // add default signature
      mailMessageData.setReplacement(ReplacementToken.MAIL_SIGNATURE,
          MessageRepository.getString(getPMF(), MessagesEnum.MAIL_SIGNATURE_DEFAULT, LocaleUtils.getLocale(inputData.getLocale())));
    }

  }

  private MessagesEnum getErrorMessageEnum(final Exception e) {
    MessagesEnum message = null;

    if (e instanceof AeriusException) {
      switch (((AeriusException) e).getReason()) {
      case MELDING_DOES_NOT_FIT:
        message = MessagesEnum.MELDING_DOES_NOT_FIT;
        break;
      case MELDING_ABOVE_PERMIT_THRESHOLD:
        message = MessagesEnum.MELDING_ABOVE_PERMIT_THRESHOLD;
        break;
      default:
        message = MessagesEnum.MELDING_TECHNICAL_ISSUES;
        break;
      }
    } else {
      message = MessagesEnum.MELDING_TECHNICAL_ISSUES;
    }

    LOG.info("Melding reject message : {}.", message);
    return message;
  }

  @Override
  protected MailTo getMailToOnError(final MeldingInputData inputData) {
    final MailTo mailTo = getMailTo(inputData);
    mailTo.addBcc(ConstantRepository.getString(getPMF(), ConstantsEnum.MELDING_AUTHORITIES_CENTRAL_MAIL_ADDRESS));
    return mailTo;
  }

  protected UserProfile getUserProfile(final Connection connection) throws SQLException, AeriusException {
    return UserRepository.getUserProfileByName(connection, MELDING_WORKER_USERNAME);
  }

}