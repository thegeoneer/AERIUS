/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.paa.PAAImport;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public class RequestFileExporter {

  private static final Logger LOG = LoggerFactory.getLogger(RequestFileExporter.class);
  private static final int MAX_EXTACTED_FILES = 2;

  private enum ExportType {
    PERMIT(QueryBuilder.from(RequestRepository.REQUESTS)
        .join(new JoinClause("permits", RepositoryAttribute.REQUEST_ID))
        .select(RepositoryAttribute.REFERENCE, RepositoryAttribute.STATUS, RepositoryAttribute.INSERT_DATE, RepositoryAttribute.RECEIVED_DATE)
        .getQuery()),
    PRONOUNCEMENT(QueryBuilder.from(RequestRepository.REQUESTS)
        .join(new JoinClause("pronouncements", RepositoryAttribute.REQUEST_ID))
        .select(RepositoryAttribute.REFERENCE, RepositoryAttribute.STATUS, RepositoryAttribute.INSERT_DATE)
        .getQuery()),
    PRIORITYSUBPROJECT(QueryBuilder.from(RequestRepository.REQUESTS)
        .join(new JoinClause("priority_subprojects", RepositoryAttribute.REQUEST_ID))
        .select(RepositoryAttribute.REFERENCE, RepositoryAttribute.STATUS, RepositoryAttribute.INSERT_DATE)
        .getQuery());

    private final Query query;

    private ExportType(final Query query) {
      this.query = query;
    }

    public Query getQuery() {
      return query;
    }

    public static ExportType safeValueOf(final String type) {
      ExportType returnType = ExportType.PRONOUNCEMENT;
      for (final ExportType exportType : values()) {
        if (exportType.name().equalsIgnoreCase(type)) {
          returnType = exportType;
        }
      }
      return returnType;
    }
  }

  private enum RepositoryAttribute implements Attribute {

    REQUEST_ID,

    REFERENCE,
    STATUS,

    INSERT_DATE,
    RECEIVED_DATE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private final PMF pmf;
  private final ExportType exportType;
  private final File exportDir;

  public RequestFileExporter(final PMF pmf, final String type, final File exportDir) {
    this.pmf = pmf;
    if (!exportDir.exists() || !exportDir.isDirectory()) {
      throw new IllegalArgumentException("Expected an existing directory but got " + exportDir);
    }
    this.exportType = ExportType.safeValueOf(type);
    this.exportDir = exportDir;
  }

  public void export() {
    LOG.info("Exporting files for {} to {}", exportType, exportDir);
    try (final Connection con = pmf.getConnection()) {
      final List<RequestInfo> requests = getRequests(con, exportType.getQuery());
      LOG.info("Found references for {} requests, exporting files.", requests.size());
      for (final RequestInfo request : requests) {
        final byte[] content = RequestRepository.getRequestFileContent(con, request.reference, RequestFileType.APPLICATION);
        final RequestFile requestFile = RequestRepository.getSpecificRequestFile(con, request.reference, RequestFileType.APPLICATION);
        if (content == null) {
          LOG.warn("Empty request file encountered.. Skipping!");
        } else {
          writeFiles(request, requestFile, content);
        }
      }
      LOG.info("Files exported.");
    } catch (final Exception e) {
      LOG.error("Error while exporting", e);
    }
  }

  private void writeFiles(final RequestInfo request, final RequestFile requestFile, final byte[] content) {
    try {
      FileUtils.writeByteArrayToFile(new File(exportDir, getFileName(request, exportType.name(), requestFile.getFileFormat())), content);
    } catch (final IOException e) {
      LOG.error("Error while writing request file", e);
    }

    if (requestFile.getFileFormat() == FileFormat.PDF) {
      extractGMLsFromPDF(request, content);
    } else if (requestFile.getFileFormat() == FileFormat.ZIP) {
      extractGMLsFromZIP(request, content);
    }
  }

  private void extractGMLsFromPDF(final RequestInfo request, final byte[] content) {
    try (final ByteArrayInputStream bis = new ByteArrayInputStream(content)) {
      final ScenarioGMLs scenarioGMLs = PAAImport.importPAAFromStream(bis);
      FileUtils.writeStringToFile(new File(exportDir, getFileName(request, exportType.name() + "_proposed", FileFormat.GML)),
          scenarioGMLs.getProposedGML(), StandardCharsets.UTF_8);
      if (scenarioGMLs.getCurrentGML() != null) {
        FileUtils.writeStringToFile(new File(exportDir, getFileName(request, exportType.name() + "_current", FileFormat.GML)),
            scenarioGMLs.getCurrentGML(), StandardCharsets.UTF_8);
      }
    } catch (final AeriusException e) {
      LOG.error("Error while importing PDF to extract GMLs from", e);
    } catch (final IOException e) {
      LOG.error("Error while writing extracted GML(s) for a PDF request file", e);
    }
  }

  private void extractGMLsFromZIP(final RequestInfo request, final byte[] content) {

    try (final ByteArrayInputStream bis = new ByteArrayInputStream(content); final ZipInputStream zipStream = new ZipInputStream(bis)) {
      final List<File> outputFiles = new ArrayList<File>();
      ZipEntry zipEntry;
      int fileCount = 0;
      while ((zipEntry = zipStream.getNextEntry()) != null) {
        final String name = zipEntry.getName();
        final ImportType entryMatchType = ImportType.determineByFilename(name);
        if (entryMatchType != null && entryMatchType == ImportType.GML && fileCount < MAX_EXTACTED_FILES) {
          outputFiles.add(new File(exportDir, getFileName(request, exportType.name() + "_count_" + Integer.toString(fileCount), FileFormat.GML)));
          FileUtils.writeStringToFile(outputFiles.get(outputFiles.size() - 1), IOUtils.toString(zipStream), StandardCharsets.UTF_8);
          fileCount++;
        } else {
          // else skip this file, not supported.
          LOG.warn("Warning content of the zip file is not supported, content skipped");
        }

      }
      if (outputFiles.size() == 1) {
        renameFile(outputFiles.get(0), "count_0", "proposed");
      } else if (outputFiles.size() == 2) {
        renameFile(outputFiles.get(0), "count_0", "current");
        renameFile(outputFiles.get(1), "count_1", "proposed");
      }

    } catch (final IOException e) {
      LOG.error("Error while writing extracted GML(s) for a ZIP request file", e);
    }
  }

  private boolean renameFile(final File file, final String renameFrom, final String renameTo) {
    return file.renameTo(new File(exportDir, file.getName().replace(renameFrom, renameTo)));
  }

  private String getFileName(final RequestInfo request, final String prefix, final FileFormat extension) {
    return prefix + "_" + request.reference + "_" + request.state + "_" + request.date.getTime() + "." + extension.getExtension();
  }

  private List<RequestInfo> getRequests(final Connection con, final Query query) throws SQLException {
    final List<RequestInfo> requests = new ArrayList<>();
    try (final PreparedStatement selectPS = con.prepareStatement(query.get())) {

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        final Date date = query.equals(ExportType.PERMIT.getQuery())
            ? QueryUtil.getDate(rs, RepositoryAttribute.RECEIVED_DATE)
            : QueryUtil.getDate(rs, RepositoryAttribute.INSERT_DATE);
        requests.add(new RequestInfo(
            QueryUtil.getString(rs, RepositoryAttribute.REFERENCE),
            date,
            QueryUtil.getEnum(rs, RepositoryAttribute.STATUS, RequestState.class)));
      }
    }

    return requests;
  }

  private static class RequestInfo {

    private final String reference;
    private final Date date;
    private final RequestState state;

    RequestInfo(final String reference, final Date date, final RequestState state) {
      this.reference = reference;
      this.date = date;
      this.state = state;
    }

  }
}
