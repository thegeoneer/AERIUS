/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import java.io.IOException;

import nl.overheid.aerius.intercept.InterceptRedirect.InterceptRedirectCommand;
import nl.overheid.aerius.intercept.InterceptRemove.InterceptRemoveCommand;
import nl.overheid.aerius.intercept.InterceptRun.InterceptRunCommand;
import nl.overheid.aerius.intercept.InterceptSave.InterceptSaveCommand;
/**
 * Visitor pattern to run specific intercept commands.
 */
interface InterceptVisitor {
  /**
   * Runs the intercept redirect command.
   *
   * @param options command line options.
   * @throws IOException
   * @throws InterruptedException
   */
  void run(InterceptRedirectCommand options) throws IOException, InterruptedException;
  /**
   * Runs the intercept remove command.
   *
   * @param options command line options.
   * @throws Exception
   */
  void run(InterceptRemoveCommand options) throws Exception;
  /**
   * Runs the intercept execute command.
   *
   * @param options command line options.
   * @throws Exception
   */
  void run(InterceptRunCommand options) throws Exception;
  /**
   * Runs the intercept save command.
   *
   * @param options command line options.
   * @throws Exception
   */
  void run(InterceptSaveCommand options) throws Exception;
}
