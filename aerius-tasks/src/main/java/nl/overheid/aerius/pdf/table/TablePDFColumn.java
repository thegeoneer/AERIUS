/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table;

import com.lowagie.text.pdf.PdfPCell;

/**
 *
 */
public abstract class TablePDFColumn<T> extends AbstractPDFColumn<T> {

  public TablePDFColumn(final PDFColumnProperties columnProperties) {
    super(columnProperties);
  }

  @Override
  protected PdfPCell getCell(final T data) {
    final PDFSimpleTable table = getTable(data);
    format(table);
    return new PdfPCell(table.get());
  }

  protected abstract PDFSimpleTable getTable(T data);

  protected void format(final PDFSimpleTable table) {
    //default no-op
  }

}
