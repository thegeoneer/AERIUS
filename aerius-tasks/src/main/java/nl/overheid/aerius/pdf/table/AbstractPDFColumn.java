/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table;

import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.pdf.PdfPCell;

import nl.overheid.aerius.pdf.table.cell.CellProcessor;
import nl.overheid.aerius.pdf.table.cell.IsRowCellProcessor;
import nl.overheid.aerius.pdf.table.cell.TableRow;
import nl.overheid.aerius.pdf.table.cell.TableRowProcessor;

/**
 *
 */
public abstract class AbstractPDFColumn<T> implements IsRowCellProcessor {

  private final PDFColumnProperties columnProperties;

  private final List<TableRowProcessor> processors = new ArrayList<>();

  protected AbstractPDFColumn(final PDFColumnProperties columnProperties) {
    this.columnProperties = columnProperties;
  }

  protected abstract PdfPCell getCell(final T data);

  public float getRelativeWidth() {
    return columnProperties.getRelativeWidth();
  }

  public String getHeaderI18nKey() {
    return columnProperties.getHeaderI18nKey();
  }

  public void addProcessor(final TableRowProcessor processor) {
    processors.add(processor);
  }

  public void addProcessor(final CellProcessor processor) {
    addProcessor(new TableRowProcessor(processor));
  }

  @Override
  public void processCell(final PdfPCell cell, final TableRow row) {
    for (final TableRowProcessor processor : processors) {
      processor.apply(cell, row);
    }
  }

}
