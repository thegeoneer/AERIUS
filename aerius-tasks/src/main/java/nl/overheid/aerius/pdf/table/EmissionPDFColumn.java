/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table;

import java.util.Locale;

import nl.overheid.aerius.paa.common.PAAUtil;

/**
 * PDF column that can format an emission.
 */
public abstract class EmissionPDFColumn<T> extends TextPDFColumn<T> {

  public EmissionPDFColumn(final Locale locale, final PDFColumnProperties columnProperties) {
    super(locale, columnProperties);
  }

  @Override
  protected String getText(final T data) {
    final double emission = getEmission(data);

    final String result;
    if (Double.doubleToLongBits(emission) == 0) {
      result = "-";
    } else {
      final String emissionString = PAAUtil.getEmissionString(locale, emission);
      result = emissionString;
    }

    return result;
  }

  protected abstract double getEmission(T data);

}
