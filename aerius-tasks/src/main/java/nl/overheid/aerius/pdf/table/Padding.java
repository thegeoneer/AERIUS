/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table;

/**
 *
 */
public class Padding {

  public static final Padding NO_PADDING = new Padding(0);

  public static final float DEFAULT_PADDING = 2;

  private final float top;
  private final float bottom;
  private final float left;
  private final float right;

  public Padding(final float padding) {
    this(padding, padding, padding, padding);
  }

  public Padding(final float top, final float bottom, final float left, final float right) {
    this.top = top;
    this.bottom = bottom;
    this.left = left;
    this.right = right;
  }

  public float getTop() {
    return top;
  }

  public float getBottom() {
    return bottom;
  }

  public float getLeft() {
    return left;
  }

  public float getRight() {
    return right;
  }

}
