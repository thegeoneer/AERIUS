/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import java.awt.image.BufferedImage;
import java.io.IOException;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.worker.util.ImageMergeLayer;
import nl.overheid.aerius.worker.util.ImageUtils;

/**
 * A layer to be added to a PDF image. Additional convenience subclasses are available like {@link PDFImageWMSLayer} etc.
 */
public class PDFImageLayer {

  private BufferedImage image;
  private float opacity;
  private int posX;
  private int posY;

  /** Default constructor. */
  public PDFImageLayer() { /* No-op. */ }

  /**
   * Layer will consist of only this image.
   * @param image The image.
   */
  public PDFImageLayer(final BufferedImage image) {
    this.image = image;
  }

  /**
   * Layer will consist of this image and a posX/Y.
   * @param image The image.
   * @param posX x position
   * @param posY y position
   */
  public PDFImageLayer(final BufferedImage image, final int posX, final int posY) {
    this.image = image;
    this.posX = posX;
    this.posY = posY;
  }

  /**
   * Get the image to be added as layer to a PDF image. This will be the base image. It might be edited if needed, like to
   *  apply the opacity and such.
   * @param bbox The bbox.
   * @param width The width of the image.
   * @param height The height of the image.
   * @return layer image
   */
  protected BufferedImage getBaseImage(final BBox bbox, final int width, final int height) throws IOException {
    return image;
  }

  /**
   * Get the image to be added as layer to a PDF image.
   * @param bbox The bbox.
   * @param width The width of the image.
   * @param height The height of the image.
   * @return layer image
   */
  public BufferedImage getImage(final BBox bbox, final int width, final int height) throws IOException {
    BufferedImage img = getBaseImage(bbox, width, height);

    if (getOpacity() > 0) {
      img = ImageUtils.getImageWithOpacity(img, getOpacity());
    }

    return img;
  }

  /**
   * Convenience method to create an {@link ImageMergeLayer} out of this layer.
   * @param bbox The BBox to use.
   * @param width The width
   * @param height The height
   * @return {@link ImageMergeLayer}
   */
  public ImageMergeLayer createImageMergeLayer(final BBox bbox, final int width, final int height) throws IOException {
    final BufferedImage image = getImage(bbox, width, height);
    final ImageMergeLayer returnValue;
    if (image == null) {
      returnValue = null;
    } else {
      returnValue = new ImageMergeLayer(image, getPosX(), getPosY());
    }

    return returnValue;
  }

  /**
   * Set the opacity and return itself to allow chaining.
   * @param opacity The opacity to set.
   * @return Itself
   */
  public PDFImageLayer setOpacity(final float opacity) {
    this.opacity = opacity;

    return this;
  }

  public float getOpacity() {
    return opacity;
  }

  public int getPosX() {
    return posX;
  }

  public void setPosX(final int posX) {
    this.posX = posX;
  }

  public int getPosY() {
    return posY;
  }

  public void setPosY(final int posY) {
    this.posY = posY;
  }

}
