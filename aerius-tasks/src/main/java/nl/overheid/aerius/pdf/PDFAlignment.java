/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import com.lowagie.text.Element;

/**
 * The alignment to use in PDF.
 */
public enum PDFAlignment {

  /**
   *Align stuff to the left.
   */
  LEFT(Element.ALIGN_LEFT),
  /**
   *Align stuff to the right.
   */
  RIGHT(Element.ALIGN_RIGHT),
  /**
   *Align stuff to the middle (vertically).
   */
  MIDDLE(Element.ALIGN_MIDDLE),
  /**
   *Align stuff to the center (same amount of whitespace left and right).
   */
  CENTER(Element.ALIGN_CENTER);

  private final int alignOption;

  PDFAlignment(final int option) {
    this.alignOption = option;
  }

  public int getAlignOption() {
    return alignOption;
  }
}
