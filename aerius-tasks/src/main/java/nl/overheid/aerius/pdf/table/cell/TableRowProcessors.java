/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table.cell;

import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.pdf.PdfPCell;

/**
 * Will filter out the processors to be used on a specific cell and calls them one by one so the cell can be processed.
 * The processing can be changing styles/settings of a cell. Handy for Comic Sans day and what not.
 */
public class TableRowProcessors {

  // The processors to use.
  private final List<TableRowProcessor> processors = new ArrayList<>();

  /**
   * Process the given cell through all the matching processors present. Insertion order.
   *
   * @param cell The cell to process
   * @param row The type of row this cell belongs to.
   */
  public void processCell(final PdfPCell cell, final TableRow row) {
    processCell(cell, row, null);
  }

  /**
   * Process the given cell through all the matching processors present. Insertion order.
   * Column processors are handled AFTER the processors in this class (can override if needed).
   *
   * @param cell The cell to process
   * @param row The type of row this cell belongs to.
   * @param column The column this cell belongs to.
   */
  public void processCell(final PdfPCell cell, final TableRow row, final IsRowCellProcessor column) {
    // Find all the processor, matching the given row/column combination.
    for (final TableRowProcessor processor : processors) {
      processor.apply(cell, row);
    }
    if (column != null) {
      column.processCell(cell, row);
    }
  }

  /**
   * Add a processor which will be called when a cell is going to be processed.
   * Will be used for all columns in the table.
   * @param processor The processor to add.
   */
  public void addProcessor(final TableRowProcessor processor) {
    processors.add(processor);
  }

}
