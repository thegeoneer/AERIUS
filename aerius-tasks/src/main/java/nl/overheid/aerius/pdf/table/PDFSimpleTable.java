/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table;

import java.awt.image.BufferedImage;
import java.io.IOException;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

import nl.overheid.aerius.pdf.font.PDFFonts;

/**
 *
 */
public class PDFSimpleTable implements IsPDFTable {

  private final PdfPTable wrappedTable;

  public PDFSimpleTable(final int columns) {
    wrappedTable = new PdfPTable(columns);
    applyDefaultSettings();
  }

  public PDFSimpleTable(final PDFRelativeWidths relativeWidths) {
    wrappedTable = new PdfPTable(relativeWidths.getRelativeWidths());
    applyDefaultSettings();
  }

  @Override
  public PdfPTable get() {
    return wrappedTable;
  }

  /**
   * Convenience method to create a new cell with this table and default styling.
   * @return new cell with default styling and this table as content.
   */
  @Override
  public PdfPCell getAsCell() {
    return applyDefaultCellSettings(new PdfPCell(wrappedTable));
  }

  public void add(final PdfPCell cell) {
    wrappedTable.addCell(cell);
  }

  public void add(final PdfPCell cell, final Padding padding) {
    setPadding(cell, padding);
    wrappedTable.addCell(cell);
  }

  public void add(final String text, final PDFFonts font) {
    add(new Phrase(text, font.get()));
  }

  public void add(final Phrase phrase) {
    wrappedTable.addCell(phrase);
  }

  public void add(final Phrase phrase, final Padding padding) {
    final PdfPCell cell = new PdfPCell(phrase);
    applyDefaultCellSettings(cell);
    setPadding(cell, padding);
    wrappedTable.addCell(cell);
  }

  public void add(final PDFSimpleTable table) {
    wrappedTable.addCell(table.get());
  }

  public void add(final PDFSimpleTable table, final Padding padding) {
    final PdfPCell cell = table.getAsCell();
    setPadding(cell, padding);
    wrappedTable.addCell(cell);
  }

  public void add(final PDFColumnTable<?> table) {
    wrappedTable.addCell(table.get());
  }

  /**
   * Convenience method to add an empty cell to a table.
   * @param table The table to add empty cell to.
   */
  public void addEmptyCell() {
    addEmptyCell(-1);
  }

  /**
   * Convenience method to add an empty cell to a table with an extra padding.
   * @param paddingBottom Padding to add to the empty cell.
   */
  public void addEmptyCell(final float paddingBottom) {
    final Font fontVerySmall = FontFactory.getFont("RijksoverheidSansText");
    fontVerySmall.setSize(1);
    final PdfPCell cell = new PdfPCell(new Phrase(" ", fontVerySmall));
    applyDefaultCellSettings(cell);
    if (paddingBottom >= 0) {
      cell.setPaddingBottom(paddingBottom);
    }

    wrappedTable.addCell(cell);
  }

  /**
   * Convenience method to add an empty row to a table with an extra padding.
   * Completes the current row first, before adding the padding.
   * @param padding Padding to use for the empty row.
   */
  public void addPaddedEmptyRow(final float padding) {
    completeRow();
    addEmptyCell(padding);
    completeRow();
  }

  /**
   * Fills the current row with empty cells where nothing is filled in yet.
   */
  public void completeRow() {
    wrappedTable.completeRow();
  }

  /**
   * Add image. The image is placed with absolute positions in the cell, so it will not scale based on cell dimensions, but based
   *  on the width/height given (if different from the image dimensions).
   * @param bufImage The image to add.
   * @param width The width to add the image as.
   * @param height The height to add the image as.
   */
  public void addImage(final BufferedImage bufImage, final int width, final int height) {
    addImage(bufImage, width, height, 0);
  }

  /**
   * Add image with an extra padding at the top. The image is placed with absolute positions in the cell, so it will not scale
   *  based on cell dimensions, but based on the width/height given (if different from the image dimensions).
   * @param bufImage The image to add.
   * @param width The width to add the image as.
   * @param height The height to add the image as.
   * @param paddingTop The padding to add to the top.
   */
  public void addImage(final BufferedImage bufImage, final int width, final int height, final int paddingTop) {
    try {
      final Image image = Image.getInstance(bufImage, null);
      image.scaleAbsolute(width, height);

      final PdfPCell cell = getNewCellWithDefaultSettings();
      if (paddingTop > 0) {
        cell.setPaddingTop(paddingTop);
      }
      cell.setPhrase(new Phrase(new Chunk(image, 0, -1 * height)));

      wrappedTable.addCell(cell);
    } catch (final BadElementException | IOException e) {
      throw new IllegalArgumentException("Could not add image to table", e);
    }
  }

  /**
   * Add image to PDF table. The image is placed in the cell based on the height. The width of the image will scale along.
   * @param bufImage The image to add.
   * @param height The height to add the image as.
   */
  public void addImage(final BufferedImage bufImage, final int height) {
    try {
      final Image image = Image.getInstance(bufImage, null);
      image.scaleToFit((int) (bufImage.getWidth() * (height / (double) bufImage.getHeight())), height);
      wrappedTable.addCell(new Phrase(new Chunk(image, 0, -1 * height)));
    } catch (final BadElementException | IOException e) {
      throw new IllegalArgumentException("Could not add image to table", e);
    }
  }

  public void addImage(final BufferedImage bufImage) {
    addImageWithPadding(bufImage, null);
  }

  public void addImageWithPadding(final BufferedImage bufImage, final Padding padding) {
    final PdfPCell imageCell = getNewCellWithDefaultSettings();
    try {
      imageCell.setImage(Image.getInstance(bufImage, null));
      setPadding(imageCell, padding);
      wrappedTable.addCell(imageCell);
    } catch (final BadElementException | IOException e) {
      throw new IllegalArgumentException("Could not add image to table", e);
    }
  }

  public void addImage(final Image image) {
    final PdfPCell imageCell = getNewCellWithDefaultSettings();
    imageCell.setImage(image);
    wrappedTable.addCell(imageCell);
  }

  public void setKeepTogether(final boolean keepTogether) {
    wrappedTable.setKeepTogether(keepTogether);
  }

  public void setHeaderRows(final int headerRows) {
    wrappedTable.setHeaderRows(headerRows);
  }

  /**
   * Allow splitting of rows, needed for nested tables to be splitted correctly. Use for tables containing nested datatables.
   * @param table the table to set splitting on.
   */
  public void allowSplittingRows() {
    wrappedTable.setSplitRows(true);
    wrappedTable.setSplitLate(false);
  }

  public void setDefaultPadding(final Padding padding) {
    setPadding(wrappedTable.getDefaultCell(), padding);
  }

  public void setSpacingAfter(final float spacingAfter) {
    wrappedTable.setSpacingAfter(spacingAfter);
  }

  private void setPadding(final PdfPCell cell, final Padding padding) {
    if (padding != null) {
      cell.setPaddingTop(padding.getTop());
      cell.setPaddingBottom(padding.getBottom());
      cell.setPaddingLeft(padding.getLeft());
      cell.setPaddingRight(padding.getRight());
    }
  }

  private void applyDefaultSettings() {
    wrappedTable.setWidthPercentage(100);
    wrappedTable.setSpacingBefore(0);
    wrappedTable.setSpacingAfter(0);
    applyDefaultCellSettings(wrappedTable.getDefaultCell());
  }

  public static PdfPCell getNewCellWithDefaultSettings() {
    return applyDefaultCellSettings(new PdfPCell());
  }

  static PdfPCell applyDefaultCellSettings(final PdfPCell cell) {
    cell.setBorder(0);

    return cell;
  }

}
