/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.font;

import com.lowagie.text.Font;

import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * Base of a PDF font to be used by different report Font classes.
 */
public final class PDFFonts {

  private static final int PADDING_TO_ADD_FOR_NEXT_LINE = 10;

  private final String fontName;
  private final String color;
  private final int size;
  private final int style;
  private Font font;

  private PDFFonts(final String fontName, final String color, final int size) {
    this(fontName, color, size, Font.NORMAL);
  }

  private PDFFonts(final String fontName, final String color, final int size, final int style) {
    this.fontName = fontName;
    this.color = color;
    this.size = size;
    this.style = style;
  }

  /**
   * @param fontName The font name to use.
   * @param color The color to use.
   * @param size The size to use.
   * @return Create a PDF font with given settings.
   */
  public static PDFFonts create(final String fontName, final String color, final int size) {
    return new PDFFonts(fontName, color, size);
  }

  /**
   * @param fontName The font name to use.
   * @param color The color to use.
   * @param size The size to use.
   * @param style The style to use.
   * @return Create a PDF font with given settings.
   */
  public static PDFFonts create(final String fontName, final String color, final int size, final int style) {
    return new PDFFonts(fontName, color, size, style);
  }

  /**
   * @return Get and cache to font for future use.
   */
  public Font get() {
    if (font == null) {
      synchronized (this) {
        font = F.getFont(fontName);
      }
      if (color != null) {
        font.setColor(PDFUtils.getColor(color));
      }
      font.setSize(size);
      font.setStyle(style);
    }

    return font;
  }

  public int getNextLinePadding() {
    return getSize() + PADDING_TO_ADD_FOR_NEXT_LINE;
  }

  public String getFontName() {
    return fontName;
  }

  public String getColor() {
    return color;
  }

  public int getSize() {
    return size;
  }
}
