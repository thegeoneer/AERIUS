/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import nl.overheid.aerius.util.OSUtils;

/**
 *
 */
public final class PDFConstants {

  /** Newline (in a label for example) as used in the PDF framework. */
  public static final String NEWLINE = OSUtils.LNL;

  /** Tag as used to refer to an anchor reference as used in the PDF framework. */
  public static final String ANCHOR_TAG = "#";

  private PDFConstants() {
    // Default constructor utility class.
  }

}
