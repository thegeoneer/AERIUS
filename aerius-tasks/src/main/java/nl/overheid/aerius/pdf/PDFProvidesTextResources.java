/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

/**
 * Interface containing text resource methods.
 */
public interface PDFProvidesTextResources {

  /**
   * Get text for given key.
   * @param key The key to use.
   * @return Text for given key.
   */
  String getText(final String key);

  /**
   * Get text for given key and enum value.
   * @param key The key to use.
   * @param enumValue The enum value to use.
   * @return Text for given key and enum value.
   */
  String getText(final String key, final Enum<?> enumValue);

}
