/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.TiledImageCache;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.worker.util.ImageMergeLayer;
import nl.overheid.aerius.worker.util.ImageUtils;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 *
 */
public class PDFLayerStack {

  private static final Logger LOG = LoggerFactory.getLogger(PDFLayerStack.class);

  private static final float DEFAULT_BASE_OPACITY = 1.0f;

  private final TiledImageCache tiledImageCache;
  private final List<PDFImageLayer> layers = new ArrayList<>();

  private float baseOpacity = DEFAULT_BASE_OPACITY;
  private boolean determineOrientation = true;

  public PDFLayerStack(final TiledImageCache tiledImageCache) {
    this.tiledImageCache = tiledImageCache;
  }

  public void setBaseOpacity(final float baseOpacity) {
    this.baseOpacity = baseOpacity;
  }

  public void setDetermineOrientation(final boolean determineOrientation) {
    this.determineOrientation = determineOrientation;
  }

  public void addLayer(final PDFImageLayer imageLayer) {
    layers.add(imageLayer);
  }

  public BufferedImage getFeaturedImage(final BBox bbox, final int width, final int height, final float qualitySetting)
      throws AeriusException {
    final boolean isPortrait = !determineOrientation || bbox.getWidth() < bbox.getHeight();

    // Scale the images to improve quality.
    final int scaledWidth = Math.round((isPortrait ? width : height) * qualitySetting);
    final int scaledHeight = Math.round((isPortrait ? height : width) * qualitySetting);

    // We must expand/correct the BBox to adhere to width/height's ratio's.
    final BBox fixedBbox = GeometryUtil.getFixedBBox(bbox, scaledWidth, scaledHeight);

    final List<ImageMergeLayer> imageLayers = getLayers(fixedBbox, scaledWidth, scaledHeight);

    final BufferedImage buffImage = ImageUtils.mergeImages(imageLayers.toArray(new ImageMergeLayer[imageLayers.size()]));
    return isPortrait ? buffImage : ImageUtils.rotate(buffImage, ImageUtils.Rotation.COUNTERCLOCKWISE);
  }

  private List<ImageMergeLayer> getLayers(final BBox bbox, final int scaledWidth, final int scaledHeight) throws AeriusException {
    final List<ImageMergeLayer> imageLayers = new ArrayList<>();

    //Add the bottom layer (the pdok one usually)
    final PDFImageLayer locationLayer = new PDFImageLayer(tiledImageCache.getTiledImage(bbox, scaledWidth, scaledHeight));
    locationLayer.setOpacity(baseOpacity);
    layers.add(0, locationLayer);

    //now make ImageMergeLayers out of all of them.
    for (final PDFImageLayer layer : layers) {
      imageLayers.add(getImageLayer(layer, bbox, scaledWidth, scaledHeight));
    }

    //And last but not least, the scalebar.
    imageLayers.add(getScaleBar(bbox, scaledWidth, scaledHeight));
    return imageLayers;
  }

  private ImageMergeLayer getImageLayer(final PDFImageLayer layer, final BBox bbox, final int scaledWidth,
      final int scaledHeight) throws AeriusException {
    try {
      final ImageMergeLayer imageLayer = layer.createImageMergeLayer(bbox, scaledWidth, scaledHeight);
      if (imageLayer == null) {
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
      return imageLayer;
    } catch (final IOException e) {
      LOG.error("Error retrieving layer", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  private ImageMergeLayer getScaleBar(final BBox bbox, final int width, final int height) throws AeriusException {
    return PDFUtils.getScaleBarLayer(bbox, width, height, PAASharedConstants.QUALITY_SETTING);
  }

}
