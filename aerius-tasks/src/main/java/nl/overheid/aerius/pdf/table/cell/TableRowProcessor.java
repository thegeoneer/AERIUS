/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table.cell;

import com.lowagie.text.pdf.PdfPCell;

/**
 * Applies a specific setting to a column and/or row combination.
 */
public class TableRowProcessor {

  private final CellProcessor processor;
  private final TableRow row;

  /**
   * Use to specify the cellprocessor to use for all rows.
   * Uses TableRow.ANY as the default TableRow.
   */
  public TableRowProcessor(final CellProcessor processor) {
    this(processor, TableRow.ANY);
  }

  /**
   * Use to specify the cellprocessor to use for all rows, but for a specific TableRow section.
   */
  public TableRowProcessor(final CellProcessor processor, final TableRow row) {
    this.processor = processor;
    this.row = row;
  }

  public void apply(final PdfPCell cell, final TableRow row) {
    if (appliesTo(row)) {
      processor.processCell(cell);
    }
  }

  private boolean appliesTo(final TableRow row) {
    return this.row == TableRow.ANY || this.row == row;
  }

}
