/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import java.io.ByteArrayOutputStream;
import java.util.Random;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

import nl.overheid.aerius.pdf.table.IsPDFTable;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;

/**
 *
 */
public class PDFDocumentWrapper implements AutoCloseable {

  private static final int PDF_RANDOM_OWNER_PASSWORD_LENGTH = 128;

  private final Document document;
  private final PdfWriter pdfWriter;
  private final ByteArrayOutputStream bos = new ByteArrayOutputStream();

  private final byte[] ownerPassword;

  private PdfPageEventHelper headerFooterHelper;

  PDFDocumentWrapper(final Rectangle pageSize,
      final float pageMarginLeft, final float pageMarginRight, final float pageMarginTop, final float pageMarginBottom, final int permissions)
      throws DocumentException {
    document = new Document(pageSize, pageMarginLeft, pageMarginRight, pageMarginTop, pageMarginBottom);
    pdfWriter = PdfWriter.getInstance(document, bos);

    /* Generate an ownerPassword that will allow us the open the PDF and edit it i.e. for page numbering. */
    ownerPassword = new byte[PDF_RANDOM_OWNER_PASSWORD_LENGTH];
    new Random().nextBytes(ownerPassword);

    /* Set PDF permissions. */
    pdfWriter.setEncryption(null, ownerPassword, permissions, PdfWriter.DO_NOT_ENCRYPT_METADATA);

    document.open();
  }

  public void addElement(final Element element) throws DocumentException {
    document.add(element);
  }

  public void addElement(final IsPDFTable table) throws DocumentException {
    document.add(table.get());
  }

  /**
   * Adds a table cell with this padding to add white space between blocks.
   * @param padding The padding to add.
   * @throws DocumentException On PDF error
   */
  public void addPadding(final float padding) throws DocumentException {
    final PDFSimpleTable table = new PDFSimpleTable(1);

    table.addEmptyCell();
    table.setSpacingAfter(padding);

    addElement(table);
  }

  /**
   * Create a new page.
   * @return Whether a new page has been added, see {@link Document#newPage()} for more info.
   */
  public boolean newPage() {
    return document.newPage();
  }

  void finish() {
    document.close();
  }

  @Override
  public void close() throws Exception {
    if (document.isOpen()) {
      document.close();
    }
    bos.close();
  }

  PDFMetaData getMetaData() {
    return new PDFMetaData(pdfWriter, document);
  }

  byte[] getBytes() {
    return bos.toByteArray();
  }

  public int getCurrentPageNumber() {
    return pdfWriter.getCurrentPageNumber();
  }

  public Rectangle getPageSize() {
    return pdfWriter.getPageSize();
  }

  public void useHeaderFooterHelper(final PdfPageEventHelper headerFooterHelper) {
    if (headerFooterHelper != null) {
      this.headerFooterHelper = headerFooterHelper;
      pdfWriter.setPageEvent(headerFooterHelper);
    }
  }

  public float getVerticalPosition() {
    return pdfWriter.getVerticalPosition(true);
  }

  public byte[] getOwnerPassword() {
    return ownerPassword;
  }

  public PdfWriter getPdfWriter() {
    return pdfWriter;
  }

  public PdfPageEventHelper getHeaderFooterHelper() {
    return headerFooterHelper;
  }

  /**
   * Convenience method to check if the element with given height should
   *  fit on the remainder of the current page or not.
   * @param height The height of the element.
   * @return true if it should fit, false otherwise.
   */
  public boolean isThereSpaceForElement(final float height) {
    return getPdfWriter().getVerticalPosition(true) - document.bottomMargin() >= height;
  }

  public void add(final Image image, final float posX, final float posY) {
    PDFWriterUtil.addImage(pdfWriter, image, posX, posY);
  }
}
