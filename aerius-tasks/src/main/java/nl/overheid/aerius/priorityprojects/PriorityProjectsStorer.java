/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.priorityprojects;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Reads data files from the given path and stores the related data into the database in the "prioritaire projecten" tables.
 */
public class PriorityProjectsStorer {

  private static final Logger LOG = LoggerFactory.getLogger(PriorityProjectsStorer.class);

  private final PMF pmf;
  private final Locale locale;

  /**
   * @param locale
   * @throws SQLException
   */
  public PriorityProjectsStorer(final PMF pmf, final Locale locale) {
    this.pmf = pmf;
    this.locale = locale;
  }

  /**
   * Reads all valid files from the given path recursively and stores the data into the priority projects tables.
   * It uses the parent directory as the authority name.
   * @param path path to scan files from.
   * @param keys
   * @throws AeriusException
   * @throws IOException
   * @throws SQLException
   */
  public void storeFilesOnPath(final String path, final int originId, final int startSiteId, final int startSourceId,
      final List<Substance> substances) throws IOException {
    final List<FileData> files = scanFiles(path);
    final AtomicInteger nextSiteId = new AtomicInteger(startSiteId);
    final AtomicInteger nextSourceId = new AtomicInteger(startSourceId);
    for (final FileData file : files) {
      ImportResult result;
      try {
        result = getImportResult(file.getFile().getPath());
        if (result != null) {
          final String authority = file.getFile().getParentFile().getName();
          try (final Connection con = pmf.getConnection()) {
            PriorityProjectsRepository.insertPriorityProjectOrigin(con, originId, file.getFile().getName(), authority);
          }
          storeDocument(authority, file, result, originId, nextSiteId, nextSourceId, substances);
        }
      } catch (AeriusException | IOException | SQLException e) {
        LOG.error("Could not import file {}, reason", file, e);
      }
    }
  }

  /**
   * Returns a list of data files to be stored into the database. Recursively scans the directory.
   * @param path directory or file to scan.
   * @return
   * @throws IOException
   */
  List<FileData> scanFiles(final String path) throws IOException {
    final List<FileData> scannedFiles = new ArrayList<>();
    final File[] files = new File(path).listFiles();
    if (files == null) {
      LOG.error("No files found in {}", path);
    } else {
      for (final File file : files) {
        if (file.isDirectory()) {
          scannedFiles.addAll(scanFiles(file.getPath()));
        } else {
          final ImportType it = ImportType.determineByFilename(file.getName());
          if (it != null) {
            if (it == ImportType.BRN) {
              LOG.warn("OPS source files not supported, ignored file {}", file);
            } else {
              scannedFiles.add(new FileData(file, it));
            }
          }
        }
      }
    }
    return scannedFiles;
  }

  /**
   * Get the structured data from the file.
   * @param filename
   * @return
   * @throws AeriusException
   * @throws IOException
   * @throws SQLException
   */
  ImportResult getImportResult(final String filename) throws AeriusException, IOException, SQLException {
    try (final InputStream inputStream = new BufferedInputStream(new FileInputStream(new File(filename)))) {
      final Importer importer = new Importer(pmf, locale);
      final ImportResult result = importer.convertInputStream2ImportResult(filename, inputStream);

      if (!result.getExceptions().isEmpty()) {
        for (final Exception exception : result.getExceptions()) {
          LOG.error("Error reading file: {}", filename, exception);
        }
        return null;
      }
      return result;
    }
  }

  /**
   * Stores all priority projects data into the database.
   * @param authority
   * @param fileData
   * @param result
   * @param keys
   * @throws SQLException
   * @throws IOException
   */
  void storeDocument(final String authority, final FileData fileData, final ImportResult result, final int originId,
      final AtomicInteger nextSiteId, final AtomicInteger nextSourceId, final List<Substance> substances) throws SQLException, IOException {
    try (final Connection con = pmf.getConnection()) {
      final ScenarioMetaData md = result.getMetaData();
      final int siteId = nextSiteId.getAndIncrement();
      final int year = result.getImportedYear();

      final Set<EmissionValueKey> keys = getEmissionValueKeys(year, substances);

      LOG.info("Inserting site id {}, authority {} file: {}", siteId, authority, fileData.getFile());
      PriorityProjectsRepository.insertPriorityProject(con, siteId, year, authority,
          (StringUtils.defaultString(result.getSourceLists().get(0).getName()) + " " + StringUtils.defaultString(md.getProjectName())
              + " " + StringUtils.defaultString(md.getCorporation())).trim(),
          StringUtils.defaultString(md.getDescription()), StringUtils.defaultString(md.getReference()));
      PriorityProjectsRepository.insertPriorityProjectFile(con, siteId, fileData.getFile(), fileData.getType().getExtension(),
          fileData.getContent());
      for (final EmissionSource source : result.getSourceLists().get(0)) {
        final int sourceId = nextSourceId.getAndIncrement();
        PriorityProjectsRepository.insertPriorityProjectSource(con, siteId, sourceId, source.getSector().getSectorId(),
            originId, source.getLabel(), source.getGeometry());
        final Map<Substance, Double> emissions = new HashMap<>();

        for (final EmissionValueKey key : keys) {
          emissions.put(key.getSubstance(), source.getEmission(key));
        }
        PriorityProjectsRepository.insertPriorityProjectSourceEmissions(con, sourceId, emissions);
        PriorityProjectsRepository.insertPriorityProjectSourceCharacteristics(con, sourceId,
            source.getSourceCharacteristics());
      }
      LOG.info("Inserted site id {}, authority {} file: {}", siteId, authority, fileData.getFile());
    }
  }

  private Set<EmissionValueKey> getEmissionValueKeys(final int year, final List<Substance> substances) {
    //can't use EmissionValueKey.getEmissionValueKeys(year, substances), as that will add NO2 as well...
    final Set<EmissionValueKey> keys = new HashSet<>();
    for (final Substance substance : substances) {
      if (substance == Substance.NOXNH3) {
        keys.add(new EmissionValueKey(year, Substance.NH3));
        keys.add(new EmissionValueKey(year, Substance.NOX));
      } else {
        keys.add(new EmissionValueKey(year, substance));
      }
    }
    return keys;
  }

  /**
   * Data object to store filename, file extension and file content.
   */
  static class FileData {
    private final File file;
    private final ImportType type;
    private final byte[] content;

    public FileData(final File file, final ImportType type) throws IOException {
      this.file = file;
      this.type = type;
      content = FileUtils.readFileToByteArray(file);
    }

    public File getFile() {
      return file;
    }

    public ImportType getType() {
      return type;
    }

    public byte[] getContent() {
      return content;
    }

    @Override
    public String toString() {
      return file == null ? "null" : file.toString();
    }
  }
}
