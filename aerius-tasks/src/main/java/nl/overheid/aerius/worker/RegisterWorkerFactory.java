/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.register.PriorityProjectExportWorker;
import nl.overheid.aerius.register.PrioritySubProjectExportWorker;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.domain.export.PrioritySubProjectExportData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * DatabaseWorkerFactory for register database worker.
 */
public class RegisterWorkerFactory extends DatabaseWorkerFactory {

  private static final Logger LOG = LoggerFactory.getLogger(RegisterWorkerFactory.class);

  public RegisterWorkerFactory() {
    super(WorkerType.REGISTER, ProductType.REGISTER);
  }

  @Override
  protected WorkerHandler createWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final PMF pmf) {
    return new RegisterWorkerHandler(configuration, factory, pmf);
  }

  /**
   * Worker for the Database worker queue. This worker sends the data to the specific
   * database worker.
   */
  private static class RegisterWorkerHandler extends WorkerHandlerImpl<Serializable, Serializable> {

    private final BrokerConnectionFactory factory;

    private final DBWorkerConfiguration configuration;

    private final PMF pmf;

    /**
     * @param configuration The database configuration to use for setting up connections with the DB and such
     * @param factory broker connection factory
     */
    public RegisterWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final PMF pmf) {
      this.configuration = configuration;
      this.factory = factory;
      this.pmf = pmf;
    }

    @Override
    public Serializable run(final Serializable input, final WorkerIntermediateResultSender resultSender, String correlationId) throws Exception {
      final Serializable output;
      try {
        if (input instanceof PrioritySubProjectExportData) {
          output = handle((PrioritySubProjectExportData) input);
        } else if (input instanceof PriorityProjectExportData) {
          output = handle((PriorityProjectExportData) input);
        } else {
          throw new UnsupportedOperationException("Worker does not know how to handle the input:"
              + input.getClass().getName());
        }
      } catch (final SQLException e) {
        LOG.error("SQLException for " + input.getClass(), e);
        return new AeriusException(Reason.INTERNAL_ERROR);
      }
      return output;
    }

    private Serializable handle(final PrioritySubProjectExportData input) throws Exception {
      return new PrioritySubProjectExportWorker(pmf, configuration, factory).run(input, null);
    }

    private Serializable handle(final PriorityProjectExportData input) throws Exception {
      return new PriorityProjectExportWorker(pmf, configuration, factory).run(input, null);
    }
  }
}
