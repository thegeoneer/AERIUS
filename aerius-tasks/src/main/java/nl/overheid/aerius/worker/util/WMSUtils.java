/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.shared.SharedConstants;

/**
 * Various WMS helper methods.
 */
public final class WMSUtils {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(WMSUtils.class);

  /**
   * http request time out.
   */
  private static final int TIMEOUT = (int) TimeUnit.MINUTES.toMillis(5);

  /**
   * Possible parameters for WMS layers. Declared name is used as-is for the parameter (so be wary when changing them...)
   */
  public enum LayerParam {
    CALCULATION_ID, CALCULATION_A_ID, CALCULATION_B_ID, YEAR,
  }

  /**
   * Divider as used between layer parameters.
   */
  private static final char LAYER_DIVIDER_PARAMS = ';';
  /**
   * Divider as used between a layer's parameter key and it's value.
   */
  private static final char LAYER_DIVIDER_KEY_VALUE = ':';

  /**
   * The static parameters used to fetch a layer.
   */
  private static final Map<String, String> LAYER_FETCH_STATIC_PARAMS = new TreeMap<String, String>() {
    {
      put("FORMAT", "image/png");
      put("TRANSPARENT", "TRUE");
      put("SERVICE", "WMS");
      put("VERSION", "1.1.1");
      put("REQUEST", "GetMap");
      put("STYLES", "");
      // The dynamic parameters will be added by getLayerURL.
    }
  };

  private WMSUtils() {
    // Not to be extended or constructed.
  }

  /**
   * Get the URL as needed to fetch a WMS layer based on the arguments given. The URL will be properly encoded using UTF-8.
   *
   * @param dynamicWMS
   *          If true will use the dynamicWMS server instead of the staticWMS server.
   * @param layerName
   *          The layer to fetch from the WMS.
   * @param width
   *          The width of the layer to retrieve.
   * @param height
   *          The height of the layer to retrieve.
   * @param bbox
   *          the bounding box of the image
   * @param epsg
   *          the coordinate system of the layer.
   * @param layerParameters
   *          Send if set.
   * @param internalSLD
   *          The internal SLD to use (optional).
   * @return The URL to use.
   */
  public static String getLayerURL(final String baseURL, final boolean dynamicWMS, final String layerName, final int width, final int height,
      final BBox bbox, final EPSG epsg, final Map<LayerParam, String> layerParameters, final String internalSLD) {
    final List<NameValuePair> params = new ArrayList<>();
    // Add the static parameters.
    for (final Entry<String, String> entry : LAYER_FETCH_STATIC_PARAMS.entrySet()) {
      params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
    }
    // Add the dynamic parts.
    params.add(new BasicNameValuePair("SRS", epsg.getEpsgCode()));
    params.add(new BasicNameValuePair("LAYERS", layerName));
    params.add(new BasicNameValuePair("BBOX", new StringBuilder().append((int) bbox.getMinX()).append(',').append((int) bbox.getMinY()).append(',')
        .append((int) bbox.getMaxX()).append(',').append((int) bbox.getMaxY()).toString()));
    params.add(new BasicNameValuePair("WIDTH", Integer.toString(width)));
    params.add(new BasicNameValuePair("HEIGHT", Integer.toString(height)));
    if (!layerParameters.isEmpty()) {
      params.add(new BasicNameValuePair("viewparams", toLayerParamString(layerParameters)));
    }
    if (!StringUtils.isEmpty(internalSLD)) {
      params.add(new BasicNameValuePair(SharedConstants.PARAM_INTERNAL_SLD, internalSLD));
    }

    return baseURL + URLEncodedUtils.format(params, StandardCharsets.UTF_8);
  }

  /**
   * Get a WMS layer and returns it as an image. Returns null if somehow the data read is not a valid image.
   *
   * @param httpClient
   *          HTTP client
   * @param dynamicWMS
   *          If true will use the dynamicWMS server instead of the staticWMS server.
   * @param layerName
   *          The layer to fetch from the WMS.
   * @param width
   *          The width of the layer to retrieve.
   * @param height
   *          The height of the layer to retrieve.
   * @param bbox
   *          the bounding box of the image
   * @param epsg
   *          the coordinate system of the layer.
   * @param layerParameters
   *          Send if set.
   * @param internalSLD
   *          The internal SLD to use (optional).
   * @throws IOException
   *           On error.
   */
  public static BufferedImage getLayerAsImage(final String baseURL, final HttpClient httpClient, final boolean dynamicWMS, final String layerName,
      final int width, final int height, final BBox bbox, final EPSG epsg, final Map<LayerParam, String> layerParameters, final String internalSLD)
          throws IOException {
    final String layerURL = getLayerURL(baseURL, dynamicWMS, layerName, width, height, bbox, epsg, layerParameters, internalSLD);
    LOG.debug("GetLayerAsImage: {}", layerURL);

    // Create a HTTP GET request for the layer URL.
    final HttpGet request = new HttpGet(layerURL);
    final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(TIMEOUT).setSocketTimeout(TIMEOUT).build();
    request.setConfig(requestConfig);

    BufferedImage image = null;
    InputStream content = null;
    try {
      // Execute the request using the reusable HTTP client.
      final HttpResponse response = httpClient.execute(request);

      // Get the content as a stream and read it to an BufferedImage.
      content = response.getEntity().getContent();
      image = ImageIO.read(content);
    } finally {
      if (content != null) {
        content.close();
      }
      request.releaseConnection();
    }

    return image;
  }

  /**
   * Calculates the optimal resolution for the image based on the data given. Calculated so that the image will never be stretched.
   *
   * @param maxWidth
   *          The maximum width value for the resolution.
   * @param maxHeight
   *          The minimum height value for the resolution.
   * @param bboxWidth
   *          The bbox width.
   * @param bboxHeight
   *          The bbox height.
   * @return {@link Dimension}
   */
  public static Dimension getFixedDimension(final int maxWidth, final int maxHeight, final double bboxWidth, final double bboxHeight) {
    // Calculate the ratio height to ratio width.
    final double ratioHeightToWidth = bboxWidth / bboxHeight;

    // Calculate the default width/height, without the fixing part.
    double mapHeight = maxHeight;
    double mapWidth = maxHeight * ratioHeightToWidth;

    // Only try to fix if the width or height are higher than the max allowed.
    if (mapWidth > maxWidth) {
      // Calculate ratio width/height to max.
      final double ratioWidthToMax = maxWidth / mapWidth;

      // If the ratio width to max is higher than the ratio height to max,
      // divide both width and height by the ratio width to the max.
      mapWidth *= ratioWidthToMax;
      mapHeight *= ratioWidthToMax;
    }

    // Return the possibly new resolution.
    return new Dimension((int) Math.round(mapWidth), (int) Math.round(mapHeight));
  }

  /**
   * Calculates the bbox params given (max - min = distance) to meters.
   *
   * @param bboxMin
   *          the box minimum.
   * @param bboxMax
   *          the box maximum.
   * @return The distance in meters.
   */
  public static double calculateBboxToMeters(final double bboxMin, final double bboxMax) {
    return (bboxMax - bboxMin) / SharedConstants.GOOGLE_TO_RDNEW;
  }

  private static String toLayerParamString(final Map<LayerParam, String> layerParameters) {
    final List<String> params = new ArrayList<>();
    for (final Entry<LayerParam, String> entry : layerParameters.entrySet()) {
      params.add(entry.getKey().name() + WMSUtils.LAYER_DIVIDER_KEY_VALUE + entry.getValue());
    }
    return StringUtils.join(params, WMSUtils.LAYER_DIVIDER_PARAMS);
  }
}
