/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.conversion.EmissionsCalculator;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationInfoRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.gml.GMLBuilderFilter;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.ZipFileMaker;
import nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData;

/**
 * Worker converts sources and custom calculation points to GML.
 * When calculation ID supplied is not null, results for that calculation are also converted.
 * Sends generic mails containing the generated .gml file after it is done generating files (or when an error occurred).
 */
public class GMLExportWorker implements ExportWorker {

  private static final Logger LOG = LoggerFactory.getLogger(GMLExportWorker.class);

  private static final String FILENAME_GML_PREFIX = "AERIUS_gml";
  private static final String FILENAME_GML_COMPARISON = "comparison";
  private static final String FILENAME_GML_EXTENSION = ".zip";

  private final GMLWriter builder;
  private PMF pmf;
  private int filesCount; //files count in zip fle

  /**
   * @param pmf The Persistence Manager Factory to use.
   * @throws SQLException
   */
  public GMLExportWorker(final PMF pmf) throws SQLException {
    this.setPMF(pmf);
    builder = new GMLWriter(ReceptorGridSettingsRepository.getReceptorGridSettings(pmf));
  }

  @Override
  public void prepare(final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments)
      throws SQLException, AeriusException {
    final CalculatedScenario scenario = inputData.getScenario();
    ensureProperEmissions(scenario);
    try (Connection con = pmf.getConnection()) {
      //Make sure the calculation id's are not present, because they are recalculated by worker
      for (final Calculation cs : scenario.getCalculations()) {
        cs.setCalculationId(0);
      }
    }
    final CalculationPointList calculationPoints = scenario.getCalculationPoints();
//    if (Arrays.asList(ExportType.PAA_DEVELOPMENT_SPACES, ExportType.PAA_DEMAND).contains(inputData.getExportType()) && calculationPoints != null) {
//      //ensure PAA does not include calculation points in the GML.
//      calculationPoints.clear();
//    }
    // Generate metadata before calculating, because if this fails the calculations are useless.
    LOG.debug("Generating GML for project name: {}", scenario.getMetaData().getProjectName());
  }

  @Override
  public final void createFileContent(final File outputFile, final CalculationInputData inputData,
      final ArrayList<StringDataSource> backupAttachments, final String downloadDirectory, final String correlationId)
      throws IOException, AeriusException, SQLException {
    LOG.info("Generating ZIP for project, reference: {}", inputData.getScenario().getMetaData().getReference());
    final File tempDir = Files.createTempDirectory("GML_Export").toFile();
    final List<File> exportFiles = createGMLs(tempDir, inputData);
    ZipFileMaker.files2ZipStream(outputFile, exportFiles, tempDir, true);
    setFilesCount(exportFiles.size());
  }

  @Override
  public ImmediateExportData postProcess(final ImmediateExportData immediateExportData) {
    immediateExportData.setFilesInZip(getFileCount());
    return immediateExportData;
  }

  private List<File> createGMLs(final File tempDir, final CalculationInputData inputData) throws AeriusException, SQLException {
    final CalculatedScenario scenario = inputData.getScenario();
    final List<File> gmlFiles = new ArrayList<>();
    gmlFiles.addAll(defaultGmlExport(tempDir, scenario));

    // add individual sector result gml files
    if (inputData.isSectorOutput()) {
      gmlFiles.addAll(sectorsGmlExport(tempDir, scenario));
    }
    return gmlFiles;
  }

  private void ensureProperEmissions(final CalculatedScenario calculatedScenario) throws SQLException, AeriusException {
    for (final Calculation calculation : calculatedScenario.getCalculations()) {
      try (Connection con = getPMF().getConnection()) {
        EmissionsCalculator.setEmissions(con, calculation.getSources(), EmissionValueKey.getEmissionValueKeys(calculation.getYear(),
            Substance.EMISSION_SUBSTANCES));
      }
    }
  }

  private List<File> defaultGmlExport(final File tempDir, final CalculatedScenario scenario) throws AeriusException, SQLException {
    final ArrayList<ArrayList<AeriusResultPoint>> resultsPerCalculation = new ArrayList<>();
    for (final Calculation calculation : scenario.getCalculations()) {
      try (final Connection connection = getPMF().getConnection()) {
        final PartialCalculationResult result = CalculationInfoRepository.getCalculationResults(connection, calculation.getCalculationId());
        resultsPerCalculation.add(result.getResults());
      }
    }
    return exportGML(tempDir, scenario, resultsPerCalculation, null);
  }

  private List<File> sectorsGmlExport(final File tempDir, final CalculatedScenario scenario)
      throws AeriusException, SQLException {
    final List<File> generated = new ArrayList<>();
    for (final Sector sector : determineSectors(scenario)) {
      generated.addAll(sectorGmlExport(tempDir, scenario, sector));
    }
    return generated;
  }

  private List<Sector> determineSectors(final CalculatedScenario scenario) {
    final List<Sector> sectorList = new ArrayList<>();
    // create unique sector list
    for (final Calculation calculation : scenario.getCalculations()) {
      for (final EmissionSource emissionSource : scenario.getSources(calculation.getCalculationId())) {
        final Sector sector = emissionSource.getSector();
        if (!sectorList.contains(sector)) {
          sectorList.add(sector);
        }
      }
    }
    return sectorList;
  }

  private List<File> sectorGmlExport(final File tempDir, final CalculatedScenario scenario, final Sector sector)
      throws AeriusException, SQLException {
    final ArrayList<ArrayList<AeriusResultPoint>> resultsPerCalculation = new ArrayList<>();
    for (final Calculation calculation : scenario.getCalculations()) {
      try (final Connection connection = getPMF().getConnection()) {
        final PartialCalculationResult result = CalculationInfoRepository.getCalculationSectorResults(connection, calculation.getCalculationId(),
            sector.getSectorId());
        resultsPerCalculation.add(result.getResults());
      }
    }
    return exportGML(tempDir, scenario, resultsPerCalculation, new SectorFilter(sector));
  }

  private List<File> exportGML(final File tempDir, final CalculatedScenario scenario,
      final ArrayList<ArrayList<AeriusResultPoint>> resultsPerCalculation, final GMLBuilderFilter filter)
      throws AeriusException, SQLException {
    //ensure to clear results first: don't want leftovers from another export to conflict at this point.
    scenario.getScenario().clearResultPoints();
    for (final ArrayList<AeriusResultPoint> results : resultsPerCalculation) {
      scenario.getScenario().addResultPoints(results);
    }
    return builder.writeToFiles(tempDir, getPMF().getDatabaseVersion(), scenario, filter);
  }

  @Override
  public MessagesEnum getMailSubject(final CalculationInputData inputData) {
    return inputData.isNameEmpty() ? MessagesEnum.GML_MAIL_SUBJECT : MessagesEnum.GML_MAIL_SUBJECT_JOB;
  }

  @Override
  public void setReplacementTokens(final CalculationInputData inputData, final MailMessageData mailMessageData) {
    mailMessageData.setReplacement(ReplacementToken.AERIUS_REFERENCE, inputData.getScenario().getMetaData().getReference());
    mailMessageData.setReplacement(ReplacementToken.JOB, inputData.getName());
  }

  @Override
  public MessagesEnum getMailContent(final CalculationInputData inputData) {
    return MessagesEnum.GML_MAIL_CONTENT;
  }

  @Override
  public String getFileName(final CalculationInputData inputData) {
    return FileUtil.getFileName(FILENAME_GML_PREFIX, FILENAME_GML_EXTENSION,
        inputData.getScenario() instanceof CalculatedComparison ? FILENAME_GML_COMPARISON : null, null);
  }

  private PMF getPMF() {
    return pmf;
  }

  private void setPMF(final PMF pmf) {
    this.pmf = pmf;
  }

  private void setFilesCount(final int size) {
    this.filesCount = size;
  }

  private int getFileCount() {
    return filesCount;
  }

  private static class SectorFilter implements GMLBuilderFilter {

    private final Sector sector;

    SectorFilter(final Sector sector) {
      this.sector = sector;
    }

    @Override
    public String getOptionalFileName() {
      return sector.getName();
    }

    @Override
    public boolean include(final EmissionSource emissionSource) {
      return emissionSource.getSector().getSectorId() == sector.getSectorId();
    }

  }

}
