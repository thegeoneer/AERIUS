/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.importer.ImportInput;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.configuration.ImportWorkerConfiguration;

/**
 * DatabaseWorkerFactory for calculator database worker.
 */
public class ImportWorkerFactory extends DatabaseWorkerFactory {

  private static final Logger LOG = LoggerFactory.getLogger(ImportWorkerFactory.class);

  public ImportWorkerFactory(final WorkerType workerType) {
    super(workerType, determineProduct(workerType));
  }

  @Override
  public DBWorkerConfiguration createConfiguration(final Properties properties) {
    return new ImportWorkerConfiguration(properties, getProductType(), getWorkerType());
  }

  private static ProductType determineProduct(final WorkerType workerType) {
    if (workerType == WorkerType.IMPORT_REGISTER) {
      return ProductType.REGISTER;
    }
    throw new IllegalArgumentException("Invalid worker type for ImportWorker: " + workerType);
  }

  @Override
  protected WorkerHandler createWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final PMF pmf) {
    return new ImportWorkerHandler(pmf);
  }

  /**
   * Worker for the import worker queue. This worker sends the data to the specific
   * database worker.
   */
  private static class ImportWorkerHandler extends WorkerHandlerImpl<Serializable, Serializable> {

    private final PMF pmf;

    public ImportWorkerHandler(final PMF pmf) {
      this.pmf = pmf;
    }

    @Override
    public Serializable run(final Serializable input, final WorkerIntermediateResultSender resultSender, final String correlationId)
        throws Exception {
      final Serializable output;
      try {
        if (input instanceof ImportInput) {
          output = handle((ImportInput) input, resultSender);
        } else {
          throw new UnsupportedOperationException("Worker does not know how to handle the input:"
              + input.getClass().getName());
        }
      } catch (final SQLException e) {
        LOG.error("SQLException for " + input.getClass(), e);
        return new AeriusException(Reason.INTERNAL_ERROR);
      }
      return output;
    }

    private Serializable handle(final ImportInput input, final WorkerIntermediateResultSender resultSender) throws Exception {
      return new ImportWorker(pmf).run(input, resultSender, null);
    }
  }
}
