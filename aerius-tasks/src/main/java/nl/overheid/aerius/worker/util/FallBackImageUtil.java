/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Stores the fall back image used when external layer image services fail. This class must be initialized once during startup of the application by
 * calling {@link #initFallBackImage()}.
 */
public final class FallBackImageUtil {

  /**
   * fallback image if request timed out.
   */
  private static final String FALLBACK_IMAGE_FILENAME = "layer_fallback_image.png";

  private static BufferedImage fallBackImage;

  private FallBackImageUtil() {
    /* Not to be constructed or extended. */
  }

  /**
   * Should only be used once. It will cache a fall back image.
   * @throws IOException on I/O error
   */
  public static void initFallBackImage() throws IOException {
    synchronized (FallBackImageUtil.class) {
      fallBackImage = loadFallBackImage();
    }
  }

  /**
   * Returns a predefined fall back image.
   *
   * @return BufferedImage.
   * @throws IOException
   */
  private static BufferedImage loadFallBackImage() throws IOException {
    final Image img = ImageIO.read(FallBackImageUtil.class.getResource(FALLBACK_IMAGE_FILENAME));

    final BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
    final Graphics2D bGr = bimage.createGraphics();
    try {
      bGr.drawImage(img, 0, 0, null);
    } finally {
      bGr.dispose();
    }

    return bimage;
  }

  public static BufferedImage getFallBackImage() {
    return fallBackImage;
  }
}
