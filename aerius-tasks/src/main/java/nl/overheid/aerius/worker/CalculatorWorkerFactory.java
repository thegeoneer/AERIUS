/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.paa.PAAExportWorker;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.export.OPSExportData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * DatabaseWorkerFactory for calculator database worker.
 */
public class CalculatorWorkerFactory extends DatabaseWorkerFactory {

  private static final Logger LOG = LoggerFactory.getLogger(CalculatorWorkerFactory.class);

  public CalculatorWorkerFactory(final WorkerType workerType) {
    super(workerType, ProductType.CALCULATOR);
  }

  @Override
  protected WorkerHandler createWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final PMF pmf)
      throws SQLException {
    return new CalculatorWorkerHandler(configuration, factory, pmf);
  }

  /**
   * Worker for the Database worker queue. This worker sends the data to the specific
   * database worker.
   */
  private static class CalculatorWorkerHandler extends WorkerHandlerImpl<Serializable, Serializable> {
    private final BrokerConnectionFactory factory;
    private final DBWorkerConfiguration configuration;
    private final PMF pmf;
    private final CalculatorBuildDirector calculatorBuildDirector;

    /**
     * @param configuration The database configuration to use for setting up connections with the DB and such
     * @param factory broker connection factory
     * @throws SQLException
     */
    public CalculatorWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final PMF pmf)
        throws SQLException {
      this.configuration = configuration;
      this.factory = factory;
      this.pmf = pmf;
      calculatorBuildDirector = new CalculatorBuildDirector(pmf, factory);
    }

    @Override
    public Serializable run(final Serializable input, final WorkerIntermediateResultSender resultSender, final String correlationId)
        throws Exception {
      final Serializable output;
      try {
        if (input instanceof CalculationInputData) {
          output = handleCalculationExport((CalculationInputData) input, resultSender, correlationId);
        } else if (input instanceof OPSExportData) {
          output = handleOPSExport((OPSExportData) input, correlationId);
        } else {
          throw new UnsupportedOperationException("Worker does not know how to handle the input:"
              + input.getClass().getName());
        }
      } catch (final SQLException e) {
        LOG.error("SQLException for " + input.getClass(), e);
        return new AeriusException(Reason.INTERNAL_ERROR);
      }
      return output;
    }

    private Serializable handleCalculationExport(final CalculationInputData input, final WorkerIntermediateResultSender resultSender,
        final String correlationId) throws Exception {
      final Serializable output;
      if (input.getExportType() == ExportType.CALCULATION_UI) {
        output = handleCalculationUI(input, resultSender, correlationId);
      } else {
        output = handleExport(input, correlationId);
      }
      return output;
    }

    private Serializable handleCalculationUI(final CalculationInputData input, final WorkerIntermediateResultSender resultSender,
        final String correlationId)
            throws Exception {
      return new CalculationWorker(calculatorBuildDirector, pmf).run(input, resultSender, correlationId);
    }

    private Serializable handleExport(final CalculationInputData input, final String correlationId) throws Exception {
      ExportWorker worker;
      final ExportType exportType = input.getExportType();
      switch (exportType) {
//      case PAA_DEVELOPMENT_SPACES:
//      case PAA_DEMAND:
      case PAA_OWN_USE:
        worker = new PAAExportWorker(pmf);
        break;
      case GML_WITH_RESULTS:
      case GML_WITH_SECTORS_RESULTS:
      case GML_SOURCES_ONLY:
        worker = new GMLExportWorker(pmf);
        break;
      case CSV:
        worker = new CsvExportWorker();
        break;
      case CALCULATION_UI:
      case OPS:
      default:
        throw new UnsupportedOperationException("Worker does not know how to handle the export type: " + exportType);
      }
      return new CalculatorExportWorker(calculatorBuildDirector, pmf, factory, configuration, worker).run(input, correlationId);
    }

    private Serializable handleOPSExport(final OPSExportData input, final String correlationId) throws Exception {
      return new OPSExportWorker(pmf, configuration, factory).run(input, correlationId);
    }
  }
}
