/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.EngineSourceExpander;
import nl.overheid.aerius.calculation.conversion.OPSSourceConverter;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationInfoRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.ReceptorRepository;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.ops.OPSVersion;
import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.OPSFileWriter;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.export.OPSExportData;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.ZipFileMaker;
import nl.overheid.aerius.worker.base.AbstractExportWorker;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.DownloadDirectoryUtils.StorageLocation;

/**
 * Worker converts sources and custom calculation points to OPS sources and receptors.
 * Creates OPS files based on that input.
 * When calculation ID supplied is not null, results for that calculation are also converted.
 * Sends generic mails containing the generated files after it is done generating files (or when an error occurred).
 */
public class OPSExportWorker extends AbstractExportWorker<OPSExportData> {

  private static final Logger LOG = LoggerFactory.getLogger(OPSExportWorker.class);
  private static final String FILENAME_PREFIX = "AERIUS_ops";
  private static final String FILENAME_EXTENSION = ".zip";

  private static final File OPS_ROOT_PLACEHOLDER = new File("[OPS_ROOT]");

  private static final Substance[] SUBSTANCES = new Substance[] {
      Substance.NH3, Substance.NOX, Substance.PM10,
  };

  /**
   * @param pmf The Persistence Manager Factory to use
   * @param config The Database WorkerConfiguration to get any configuration from.
   */
  public OPSExportWorker(final PMF pmf, final DBWorkerConfiguration config, final BrokerConnectionFactory factory) throws IOException {
    super(pmf, config.getGMLDownloadDirectory(), factory);
  }

  @Override
  protected ImmediateExportData handleExport(final OPSExportData inputData, final ArrayList<StringDataSource> backupAttachments,
      final String correlationId) throws AeriusException, SQLException, IOException {
    LOG.info("OPS export worker kicked in for {}", inputData);

    final List<File> opsFiles = new ArrayList<>();
    //ensure all files are written to the same base directory.
    final File tempDir = Files.createTempDirectory("OPS_Export").toFile();

    //create a writer with emulate = false: we're not actually running it and the placeholder will cause an exception otherwise.
    final OPSConfiguration cfo = new OPSConfiguration();
    cfo.setOpsRoot(OPS_ROOT_PLACEHOLDER);
    cfo.setRunFilesDirectory(tempDir);
    final OPSFileWriter writer = new OPSFileWriter(cfo);
    for (final Calculation calculation : inputData.getScenario().getCalculations()) {
      opsFiles.addAll(getOPSFiles(writer, inputData.getScenario(), calculation));
    }

    //save the files to a zip and return the URL to download the zip.
    final StorageLocation storageLocation = createStorageLocation(inputData);
    ZipFileMaker.files2ZipStream(storageLocation.getOutputFile(), opsFiles, tempDir, true);
    return new ImmediateExportData(storageLocation.getUrl());
  }

  @Override
  protected String getFileName(final OPSExportData inputData) {
    return FileUtil.getFileName(FILENAME_PREFIX, FILENAME_EXTENSION, null, inputData.getCreationDate());
  }

  private List<File> getOPSFiles(final OPSFileWriter writer, final CalculatedScenario scenario, final Calculation calculation)
      throws SQLException, AeriusException, IOException {
    final List<File> files = new ArrayList<>();
    final OPSInputData opsInputData;
    try (Connection con = getPMF().getConnection()) {
      opsInputData = toOPSInputData(con, scenario, calculation);
    }
    //ensure the points have the right terrain data.
    ReceptorRepository.setTerrainData(getPMF(), opsInputData.getReceptors());
    for (final Substance substance : SUBSTANCES) {
      //next bit might cause conflicts when all sourcelists have the same name, but UI should take care of that.
      final String runId = FileUtil.getFileName(substance.name(), calculation.getSources().getName(), null);

      writer.writeFiles(runId, opsInputData.getEmissionSources(), opsInputData.getReceptors(), substance, calculation.getYear());
      files.addAll(Arrays.asList(writer.getRunDirectory(runId).listFiles()));
    }
    return files;
  }

  private OPSInputData toOPSInputData(final Connection con, final CalculatedScenario scenario, final Calculation calculation)
      throws SQLException, AeriusException, IOException {
    final ArrayList<EmissionValueKey> keys = EmissionValueKey.getEmissionValueKeys(calculation.getYear(), SUBSTANCES);

    final OPSInputData inputData = new OPSInputData(OPSVersion.VERSION, getSurfaceZoomLevel1(con));
    final EnumSet<EmissionResultKey> emissionResultKeys = EnumSet.noneOf(EmissionResultKey.class);

    for (final Substance substance : SUBSTANCES) {
      emissionResultKeys.add(EmissionResultKey.valueOf(substance));
    }
    inputData.setSubstances(Arrays.asList(SUBSTANCES));
    inputData.setEmissionResultKeys(emissionResultKeys);
    inputData.setYear(calculation.getYear());
    inputData.setEmissionSources(getSources(con, keys, calculation.getSources()));
    inputData.setReceptors(getReceptors(con, calculation, scenario));
    return inputData;
  }

  private List<OPSSource> getSources(final Connection con, final List<EmissionValueKey> keys, final List<EmissionSource> sourceList)
      throws SQLException, AeriusException {
    final List<OPSSource> sources = new ArrayList<>();
    final SourceConverter geometryExpander = new OPSSourceConverter(con);

    for (final EngineSource es : EngineSourceExpander.toEngineSources(con, geometryExpander, sourceList, keys)) {
      if (es instanceof OPSSource) {
        sources.add((OPSSource) es);
      }
    }
    return sources;
  }

  private List<OPSReceptor> getReceptors(final Connection con, final Calculation calculation,
      final CalculatedScenario scenario) throws SQLException {
    final List<OPSReceptor> receptors = new ArrayList<>();
    if (calculation == null || calculation.getCalculationId() == 0) {
      addReceptors(receptors, scenario.getCalculationPoints());
    } else {
      addReceptors(receptors, CalculationInfoRepository.getCalculationResults(con, calculation.getCalculationId()).getResults());
    }
    return receptors;
  }

  private <A extends AeriusPoint> void addReceptors(final List<OPSReceptor> receptors, final Collection<A> points) {
    for (final AeriusPoint aeriusPoint : points) {
      receptors.add(new OPSReceptor(aeriusPoint));
    }
  }

  @Override
  protected void setReplacementTokens(final OPSExportData inputData, final MailMessageData mailMessageData) {
    //NO-OP
  }

  private int getSurfaceZoomLevel1(final Connection con) throws SQLException {
    return ReceptorGridSettingsRepository.getZoomLevels(con).get(0).getSurfaceLevel1();
  }

}
