/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Various (AWT) Font helper methods.
 */
public final class FontUtils {

  private static final Logger LOG = LoggerFactory.getLogger(FontUtils.class);

  private static final String[] AWT_FONTS_TO_LOAD = {
    "fonts/rijksoverheidsanslf-bold.ttf",
    "fonts/rijksoverheidsanslf-bolditalic.ttf",
    "fonts/rijksoverheidsanslf-italic.ttf",
    "fonts/rijksoverheidsanslf-regular.ttf"
  };

  private static boolean fontsRegistered;

  // Not to be constructed or extended.
  private FontUtils() { }

  /**
   * Should only be used once, all AWT fonts configured will be loaded and registered.
   * @return true if everything is loaded and registered, false if it already was.
   * @throws IOException on I/O error
   */
  public static boolean initFonts() throws IOException {
    synchronized (FontUtils.class) {
      if (!fontsRegistered) {
        for (final String fontPath : AWT_FONTS_TO_LOAD) {
          try {
            final Font font = Font.createFont(java.awt.Font.TRUETYPE_FONT,
                Thread.currentThread().getContextClassLoader().getResourceAsStream(fontPath));

            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
          } catch (final FontFormatException e) {
            LOG.error("Invalid font format for file: {}", fontPath, e);
            throw new RuntimeException(e);
          }
        }

        fontsRegistered = true;
        return true;
      }

      return false;
    }
  }

  /**
   * Returns the width of the String in pixels, if drawn with the given Font and FontRenderContext.
   * Use {@link #getBoundsOfString(Font, FontRenderContext, String)} for efficiency reasons if you need
   *  the width and the height of a string.
   * @param font The font in which the string will be displayed.
   * @param fontRenderContext The context in which the string will be rendered.
   * @param string The string that will be displayed.
   * @return The width of the string.
   */
  public static double getWidthOfString(final Font font, final FontRenderContext fontRenderContext, final String string) {
      final Rectangle2D bounds = getBoundsOfString(font, fontRenderContext, string);
      return bounds.getMaxX() - bounds.getMinX();
  }

  /**
   * Returns the height of the String in pixels, if drawn with the given Font and FontRenderContext.
   * Use {@link #getBoundsOfString(Font, FontRenderContext, String)} for efficiency reasons if you need
   *  the width and the height of a string.
   * @param font The font in which the string will be displayed.
   * @param fontRenderContext The context in which the string will be rendered.
   * @param string The string that will be displayed.
   * @return The height of the string.
   */
  public static double getHeightOfString(final Font font, final FontRenderContext fontRenderContext, final String string) {
      final Rectangle2D bounds = getBoundsOfString(font, fontRenderContext, string);
      return bounds.getMaxY() - bounds.getMinY();
  }

  /**
   * Returns the bounds of the String in pixels.
   * @param font The font in which the string will be displayed.
   * @param fontRenderContext The context in which the string will be rendered.
   * @param string The string that will be displayed.
   * @return The rectangle containing the bounds of the string.
   */
  public static Rectangle2D getBoundsOfString(final Font font, final FontRenderContext fontRenderContext, final String string) {
      return font.getStringBounds(string, fontRenderContext);
  }

}
