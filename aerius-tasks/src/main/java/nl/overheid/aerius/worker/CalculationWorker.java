/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.CalculationSummaryFactory;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.NBWetCalculationUtil;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.worker.base.AbstractDBWorker;

/**
 * Worker to perform a complete calculation. It chunks the task, calls the calculation engine workers and collects the results in the database and
 * send the results back.
 */
public class CalculationWorker extends AbstractDBWorker<CalculationInputData, CalculationSummary> {

  private static final Logger LOG = LoggerFactory.getLogger(CalculationWorker.class);

  private final CalculatorBuildDirector calculatorBuildDirector;

  public CalculationWorker(final CalculatorBuildDirector calculatorBuildDirector, final PMF pmf) {
    super(pmf);
    this.calculatorBuildDirector = calculatorBuildDirector;
  }

  @Override
  public CalculationSummary run(final CalculationInputData inputData, final WorkerIntermediateResultSender resultSender,
      final String correlationId) throws Exception {
    final CalculationJob calculatorJob = calculatorBuildDirector.construct(inputData, correlationId, resultSender).calculate();
    //return summary info, if not return an empty object because that the tracker uses CalculationSummary object to know it's finished.
    return inputData.isReturnCalculationSummary() ? getSummaryInfo(calculatorJob) : new CalculationSummary();
  }

  private CalculationSummary getSummaryInfo(final CalculationJob calculatorJob) throws AeriusException {
    try (final Connection con = getPMF().getConnection()) {
      final CalculationType calculationType = calculatorJob.getCalculatedScenario().getOptions().getCalculationType();
      final int calculationId1 = NBWetCalculationUtil.getCurrentCalculationId(calculatorJob.getCalculatedScenario());
      final int calculationId2 = NBWetCalculationUtil.getProposedCalculationId(calculatorJob.getCalculatedScenario());
      final ArrayList<Substance> substances = calculatorJob.getCalculatedScenario().getOptions().getSubstances();
      return CalculationSummaryFactory.getSummary(con, calculationType, calculationId1, calculationId2, substances);
    } catch (final SQLException e) {
      LOG.error(e.getMessage(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }
}
