/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.FallBackImageUtil;
import nl.overheid.aerius.worker.util.PDFUtils;
import nl.overheid.aerius.worker.util.WorkerPMFFactory;

/**
 * Factory for database based workers.
 */
abstract class DatabaseWorkerFactory implements WorkerFactory<DBWorkerConfiguration> {

  private static final Logger LOG = LoggerFactory.getLogger(DatabaseWorkerFactory.class);
  private final WorkerType workerType;
  private final ProductType productType;

  protected DatabaseWorkerFactory(final WorkerType workerType, final ProductType productType) {
    this.workerType = workerType;
    this.productType = productType;
  }

  @Override
  public DBWorkerConfiguration createConfiguration(final Properties properties) {
    return new DBWorkerConfiguration(properties, productType, workerType);
  }

  @Override
  public final WorkerHandler createWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory)
      throws AeriusException, IOException, SQLException {
    try {
      PDFUtils.initFonts();
      FallBackImageUtil.initFallBackImage();
    } catch (final IOException e) {
      LOG.error("Error initializing fonts", e);
      throw e;
    }
    return createWorkerHandler(configuration, factory, WorkerPMFFactory.createPMF(configuration));
  }

  /**
   * Creates the worker type specific worker handler.
   * @param configuration
   * @param factory
   * @param createPMF
   * @return
   * @throws SQLException
   */
  protected abstract WorkerHandler createWorkerHandler(DBWorkerConfiguration configuration, BrokerConnectionFactory factory, PMF pmf)
      throws SQLException;

  @Override
  public WorkerType getWorkerType() {
    return workerType;
  }

  public ProductType getProductType() {
    return productType;
  }

}
