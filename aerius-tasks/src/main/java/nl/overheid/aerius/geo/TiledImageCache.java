/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.geo.shared.BBox;

/**
 * Wrapper around tiled layers images. It caches calls to the image server.
 * If only caches when enabled. Because cache is stored in memory the cache is actually not useful for production as it will cause the application to
 * run out of memory.
 */
public class TiledImageCache {

  private final Map<TiledCacheEntryKey, BufferedImage> cachedTiledImages = new HashMap<>();
  private final TiledLayer2Image tiledlayer2Image;
  private boolean useCache;

  public TiledImageCache(final TiledLayer2Image tiledLayer2Image) {
    this.tiledlayer2Image = tiledLayer2Image;
  }

  public boolean isUseCache() {
    return useCache;
  }

  public void setUseCache(final boolean useCache) {
    this.useCache = useCache;
  }

  public BufferedImage getTiledImage(final BBox bbox, final int width, final int height) {
    final BufferedImage image;
    final TiledCacheEntryKey tiledCacheEntryKey = new TiledCacheEntryKey(bbox, width, height);

    if (useCache && cachedTiledImages.containsKey(tiledCacheEntryKey)) {
      image = cachedTiledImages.get(tiledCacheEntryKey);
    } else {
      try {
        image = tiledlayer2Image.getImage(bbox, width, height);
        if (useCache) {
          cachedTiledImages.put(tiledCacheEntryKey, image);
        }
      } catch (final Exception e) {
        throw new IllegalArgumentException("Could not fetch tiled layer image", e);
      }
    }
    return image;
  }

  private static class TiledCacheEntryKey {
    private final BBox bbox;
    private final int width;
    private final int height;

    TiledCacheEntryKey(final BBox bbox, final int width, final int height) {
      this.bbox = bbox;
      this.width = width;
      this.height = height;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((bbox == null) ? 0 : bbox.hashCode());
      result = prime * result + height;
      return prime * result + width;
    }

    @Override
    public boolean equals(final Object obj) {
      boolean equal = false;
      if (obj != null && getClass() == obj.getClass()) {
        final TiledCacheEntryKey other = (TiledCacheEntryKey) obj;
        equal = bbox.equals(other.bbox)
            && height == other.height
            && width == other.width;
      }
      return equal;
    }

  }

}
