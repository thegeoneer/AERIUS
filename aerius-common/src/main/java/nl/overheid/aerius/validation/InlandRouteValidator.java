/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import java.util.List;

import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Validator for {@link InlandRouteEmissionSource} objects.
 */
public class InlandRouteValidator extends SourceValidator<InlandRouteEmissionSource> {

  InlandRouteValidator(final List<AeriusException> errors, final List<AeriusException> warnings) {
    super(errors, warnings);
  }

  @Override
  void validate(final InlandRouteEmissionSource source) {
    if (source.getWaterwayCategory() == null) {
      getWarnings().add(new AeriusException(Reason.GML_INLAND_WATERWAY_NOT_SET, source.getLabel()));
    }
  }
}
