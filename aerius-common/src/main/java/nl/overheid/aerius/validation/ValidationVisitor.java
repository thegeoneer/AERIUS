/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.validation;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public class ValidationVisitor implements EmissionSourceVisitor<Void> {

  private final List<AeriusException> errors;
  private final List<AeriusException> warnings;

  public ValidationVisitor(final List<AeriusException> errors, final List<AeriusException> warnings) {
    this.errors = errors;
    this.warnings = warnings;
  }

  public void visitSources(final ArrayList<EmissionSource> sources) {
    for (final EmissionSource source : sources) {
      try {
        source.accept(this);
      } catch (final AeriusException e) {
        errors.add(e);
      }
    }
  }

  @Override
  public Void visit(final FarmEmissionSource emissionSource) throws AeriusException {
    //NO-OP
    return null;
  }

  @Override
  public Void visit(final GenericEmissionSource emissionSource) throws AeriusException {
    //NO-OP
    return null;
  }

  @Override
  public Void visit(final InlandMooringEmissionSource emissionSource) throws AeriusException {
    new InlandMooringValidator(errors, warnings).validate(emissionSource);
    return null;
  }

  @Override
  public Void visit(final InlandRouteEmissionSource emissionSource) throws AeriusException {
    new InlandRouteValidator(errors, warnings).validate(emissionSource);
    return null;
  }

  @Override
  public Void visit(final MaritimeMooringEmissionSource emissionSource) throws AeriusException {
    //NO-OP
    return null;
  }

  @Override
  public Void visit(final MaritimeRouteEmissionSource emissionSource) throws AeriusException {
    //NO-OP
    return null;
  }

  @Override
  public Void visit(final OffRoadMobileEmissionSource emissionSource) throws AeriusException {
    //NO-OP
    return null;
  }

  @Override
  public Void visit(final PlanEmissionSource emissionSource) throws AeriusException {
    //NO-OP
    return null;
  }

  @Override
  public Void visit(final SRM2EmissionSource emissionSource) throws AeriusException {
    new SRM2Validator(errors, warnings).validate(emissionSource);
    return null;
  }

  @Override
  public Void visit(final SRM2NetworkEmissionSource emissionSource) throws AeriusException {
    for (final EmissionSource source : emissionSource.getEmissionSources()) {
      if (source instanceof SRM2EmissionSource) {
        visit((SRM2EmissionSource) source);
      }
    }
    return null;
  }

}
