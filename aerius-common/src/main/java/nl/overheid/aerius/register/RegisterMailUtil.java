/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.domain.user.Authority;

public final class RegisterMailUtil {

  private static final Logger LOG = LoggerFactory.getLogger(RegisterMailUtil.class);

  private RegisterMailUtil() {
    //util class.
  }

  public static String addAuthorityToSubject(final Authority authority, final String sendMailSubject) {
    return authority != null ? authority.getDescription() + " " + sendMailSubject : sendMailSubject;
  }

  public static void setAuthorityReplacementTokens(final Authority authority, final MailMessageData mailMessageData) {
    mailMessageData.setReplacement(ReplacementToken.AUTHORITY, authority == null ? "" : authority.getDescription());
  }

  public static void setAuthoritySignatureReplacementTokens(final Authority authority, final MailMessageData mailMessageData) {
    mailMessageData.setReplacement(ReplacementToken.MAIL_SIGNATURE, authority == null ? "" : authority.getDescription());
  }

  public static Authority getAuthorityForRequest(final PMF pmf, final String reference) {
    Authority authority = null;
    try (final Connection con = pmf.getConnection()) {
      authority = getAuthorityForRequest(con, reference);
    } catch (final SQLException e) {
      LOG.error("Error determining request for reference {}", reference, e);
    }
    return authority;
  }

  public static Authority getAuthorityForRequest(final Connection con, final String reference) throws SQLException {
    return RequestRepository.getAuthorityForRequestWithReference(con, reference);
  }

  public static String getAuthorityCentralMail(final PMF pmf) {
    return ConstantRepository.getString(pmf, ConstantsEnum.MELDING_AUTHORITIES_CENTRAL_MAIL_ADDRESS);
  }

  public static String getAuthorityCentralMail(final Connection con) throws SQLException {
    return ConstantRepository.getString(con, ConstantsEnum.MELDING_AUTHORITIES_CENTRAL_MAIL_ADDRESS);
  }

}
