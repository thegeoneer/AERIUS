/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.NBWetCalculationUtil;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestSituation;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.scenario.ScenarioSectorUtil;
import nl.overheid.aerius.shared.domain.sector.ScenarioSectorInformation;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.PriorityProjectValidationUtil;

/**
 * Util class to convert an {@link ImportOutput} to various {@link Request} objects.
 */
public final class RegisterImportUtil {

  private RegisterImportUtil() {
  }

  /**
   * Convert an {@link ImportOutput} to a {@link Notice} object.
   * @param output import result
   * @return Notice object
   * @throws AeriusException error in case input could not be converted
   */
  public static Notice convertToNotice(final ImportOutput output) throws AeriusException {
    final Notice notice = new Notice();

    //don't validate for notices, these should arrive through calculator/melding app
    fillRequest(output, notice);

    // No notice specific fields

    return notice;
  }

  /**
   * Convert an {@link ImportOutput} to a {@link Permit} object.
   * @param output import result
   * @return Permit object
   * @throws AeriusException error in case input could not be converted
   */
  public static Permit convertToPermit(final ImportOutput output) throws AeriusException {
    final Permit permit = new Permit();

    validateMetaData(output.getCalculatedScenario().getMetaData());
    fillRequest(output, permit);

    // Permit specific fields
    final DossierMetaData dossierMetaData = new DossierMetaData();
    dossierMetaData.setReceivedDate(new Date());
    permit.setDossierMetaData(dossierMetaData);

    return permit;
  }

  /**
   * Convert a {@link ImportOutput} to a {@link PriorityProject}.
   * @param output import result
   * @return PriorityProject object
   * @throws AeriusException error in case input could not be converted
   */
  public static PriorityProject convertToPriorityProject(final ImportOutput output) throws AeriusException {
    final PriorityProject pp = new PriorityProject();

    validateMetaData(output.getCalculatedScenario().getMetaData());

    PriorityProjectValidationUtil.checkNonAllowedSectors(output.getCalculatedScenario());
    fillRequest(output, pp);

    // PriorityProject specific fields
    final DossierMetaData dossierMetaData = new DossierMetaData();
    dossierMetaData.setReceivedDate(new Date());
    pp.setDossierMetaData(dossierMetaData);

    return pp;
  }

  /**
   * Convert a {@link ImportOutput} to a {@link PrioritySubProject}.
   * @param output import result
   * @return PrioritySubProject object
   * @throws AeriusException error in case input could not be converted
   */
  public static PrioritySubProject convertToPrioritySubProject(final ImportOutput output) throws AeriusException {
    final PrioritySubProject subProject = new PrioritySubProject();

    validateMetaData(output.getCalculatedScenario().getMetaData());
    PriorityProjectValidationUtil.checkNonAllowedSectors(output.getCalculatedScenario());
    fillRequest(output, subProject);

    // PrioritySubProject specific fields
    subProject.setReceivedDate(new Date());

    return subProject;
  }

  /**
   * @param result The result to convert.
   * @return The converted situationMap.
   */
  protected static HashMap<SituationType, RequestSituation> convertToSituations(final CalculatedScenario calculatedScenario, final int year) {
    // Extract emission values for the last available situation
    final HashMap<SituationType, RequestSituation> situationsMap = new HashMap<>();
    if (!calculatedScenario.getScenario().getSourceLists().isEmpty()) {
      situationsMap.put(SituationType.PROPOSED, convertToSituation(getProposedSources(calculatedScenario), year));
      final EmissionSourceList sourcesCurrent = getCurrentSources(calculatedScenario);
      if (sourcesCurrent != null) {
        situationsMap.put(SituationType.CURRENT, convertToSituation(sourcesCurrent, year));
      }
    }

    return situationsMap;
  }

  private static RequestSituation convertToSituation(final EmissionSourceList sources, final int year) {
    final RequestSituation situation = new RequestSituation();
    situation.setName(sources.getName());
    final EmissionValues emissionValues = new EmissionValues();
    for (final Substance substance : Substance.RESULT_SUBSTANCES) {
      final EmissionValueKey key = new EmissionValueKey(year, substance);
      if (sources.getTotalEmission(key) > 0 && substance != Substance.NOXNH3) {
        //add to the returning emission values with a year-less EVK.
        emissionValues.setEmission(new EmissionValueKey(substance), sources.getTotalEmission(key));
      }
    }
    situation.setTotalEmissionValues(emissionValues);
    return situation;
  }

  private static EmissionSourceList getProposedSources(final CalculatedScenario calculatedScenario) {
    return NBWetCalculationUtil.getProposedSources(calculatedScenario);
  }

  private static EmissionSourceList getCurrentSources(final CalculatedScenario calculatedScenario) {
    return NBWetCalculationUtil.getCurrentSources(calculatedScenario);
  }

  /**
   * Fill a {@link Request} object based on the {@link ImportResult}. Validations will also be triggered
   * @param importResult import result
   * @param request request to convert to.
   * @throws AeriusException error in case input could not be converted
   */
  private static void fillRequest(final ImportOutput output, final Request request) throws AeriusException {
    // Set the values that can't be changed by the user.
    request.setScenarioMetaData(output.getCalculatedScenario().getMetaData());
    request.setSituations(convertToSituations(output.getCalculatedScenario(), output.getImportedYear()));
    request.setPoint(getRequestImportPoint(output.getSrid(), output.getCalculatedScenario()));
    final ScenarioSectorInformation sectorInformation =
        ScenarioSectorUtil.determineSectorInformation(getProposedSources(output.getCalculatedScenario()), output.getImportedYear());
    request.setSector(sectorInformation.getMainSector());
    request.setMultipleSectors(sectorInformation.isMultipleSectors());

    request.setStartYear(output.getImportedYear());
    request.setTemporaryPeriod(output.getImportedTemporaryPeriod());
    request.setApplicationVersion(output.getVersion());
    request.setDatabaseVersion(output.getDatabaseVersion());
  }

  /**
   * @param result The result to determine the point to use for location determination.
   * @return The point to use for location determination.
   * @throws AeriusException If geometries in result couldn't be converted.
   */
  private static Point getRequestImportPoint(final int srid, final CalculatedScenario calculatedScenario) throws AeriusException {
    final List<WKTGeometry> geometries = new ArrayList<>();
    for (final EmissionSource source : getProposedSources(calculatedScenario)) {
      geometries.add(source.getGeometry());
    }
    return GeometryUtil.determineCentroid(srid, geometries);
  }

  /**
   * Validate for correct NBWet metadata.
   * This validation should be done on import with user-friendly error messages, this is a sanity check.
   * @param metaData The metadata to check. Should contain all required fields.
   * @throws AeriusException In case one of these is missing or empty.
   * @deprecated @TODO validation is also done with Import if enabled, but classes using it don't enabled it. The validation here should be removed.
   */
  @Deprecated
  protected static void validateMetaData(final ScenarioMetaData metaData) throws AeriusException {
    validateMetaData(metaData.getProjectName(), "projectName");
    validateMetaData(metaData.getReference(), "reference");
    validateMetaData(metaData.getDescription(), "description");
    validateMetaData(metaData.getStreetAddress(), "streetAddress");
    validateMetaData(metaData.getPostcode(), "postcode");
    validateMetaData(metaData.getCity(), "city");
  }

  private static void validateMetaData(final String value, final String elementName) throws AeriusException {
    if (StringUtils.isEmpty(value)) {
      throw new AeriusException(Reason.GML_METADATA_EMPTY, elementName);
    }
  }

}
