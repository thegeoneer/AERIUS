/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestionType;

/**
 * Wrapper for static MapSearchSuggestion factory methods.
 */
public final class JSON2SuggestionFactory {

  /**
   * Make sure this Object cannot be accidentally instantiated.
   */
  private JSON2SuggestionFactory() {
  }

  /**
   * Constructs a MapSearchSuggestion Object from a GSON ResultDoc Object that represents
   * one result from the query.
   * "address" results are excluded after postcode and short integer (<1000) searches to compensate for changed
   * behaviour of the new "locatieserver" compared to the old "geocoder".
   *
   * @param docObj the GSON ResultDoc Object.
   * @param searchStringAnalyzer container of various analysis strategies.
   */
  public static MapSearchSuggestion createMapSearchSuggestion(final ResultDoc docObj, final SearchStringAnalyzer searchStringAnalyzer) {
    final MapSearchSuggestion searchSuggestion;
    final SuggestionData data = new SuggestionData();
    switch (docObj.getType()) {
    case "gemeente":
      data.setName(docObj.getGemeentenaam());
      data.setSuggestionType(MapSearchSuggestionType.MUNICIPALITY_AREA);
      break;
    case "woonplaats":
      data.setName(docObj.getWoonplaatsnaam());
      data.setSuggestionType(MapSearchSuggestionType.TOWN_AREA);
      break;
    case "postcode":
      data.setName(doc2adress(docObj));
      setAddressType(data);
      break;
    case "adres":
      if (!searchStringAnalyzer.isShortIntegerMatch()) {
        data.setName(docObj.getWeergavenaam());
        setAddressType(data);
      }
      break;
    case "weg":
      if (searchStringAnalyzer.isSingleWordMatch()) {
        data.setName(docObj.getWeergavenaam());
        setAddressType(data);
      }
      break;
    default:
    }
    if (data.hasSuggestionType()) {
      searchSuggestion = instantiateAndFillMapSearchSuggestion(docObj, data);
    } else {
      searchSuggestion = null;
    }
    return searchSuggestion;
  }

  /**
   * @param data
   */
  private static void setAddressType(final SuggestionData data) {
    if (data.hasName()) {
      data.setSuggestionType(MapSearchSuggestionType.ADDRESS);
    }
  }

  /**
   * @param docObj
   * @param suggestionType
   * @param name
   * @return
   */
  private static MapSearchSuggestion instantiateAndFillMapSearchSuggestion(final ResultDoc docObj, final SuggestionData suggestionData) {
    final MapSearchSuggestion searchSuggestion = new MapSearchSuggestion(-1, suggestionData.getName(), suggestionData.getSuggestionType());
    addGeoPart(docObj, searchSuggestion);
    searchSuggestion.setLazySubItems(true);
    return searchSuggestion;
  }

  /**
   * @param docObj
   * @param searchSuggestion
   */
  private static void addGeoPart(final ResultDoc docObj, final MapSearchSuggestion searchSuggestion) {
    final String centroideRd = docObj.getCentroideRd();
    if (centroideRd == null) {
      return;
    }
    searchSuggestion.setPoint(JSON2SuggestionUtil.constructPoint(centroideRd));
    searchSuggestion.setZoomBox(new BBox(searchSuggestion.getPoint().getX(), searchSuggestion.getPoint().getY(), 0));
  }

  /**
   * Append address and postal code to the string buffer if present in address. If no address present false is returned.
   * @param docObj geocoded address
   * @return String containing the concatenated address parts from the ResultDoc.
   */
  private static String doc2adress(final ResultDoc docObj) {
    final StringBuilder sb = new StringBuilder();
    appendStringPlusSpacer(sb, docObj.getStraatnaam(), ", ");
    appendStringPlusSpacer(sb, docObj.getPostcode(), " ");
    appendStringPlusSpacer(sb, docObj.getWoonplaatsnaam(), "");
    return sb.toString();
  }

  /**
   * @param sb StringBuilder to append to.
   * @param contentString content to append.
   * @param spacer spacer String to add after content.
   */
  private static void appendStringPlusSpacer(final StringBuilder sb, final String contentString, final String spacer) {
    if (contentString == null || contentString.isEmpty()) {
      return;
    }
    sb.append(contentString);
    sb.append(spacer);
  }
}
