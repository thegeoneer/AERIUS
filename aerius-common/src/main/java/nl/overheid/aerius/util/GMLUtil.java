/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.gml.base.FeatureMember;
import nl.overheid.aerius.gml.v2_1.source.EmissionSource;
import nl.overheid.aerius.gml.v2_1.source.road.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

public final class GMLUtil {

  private static final Logger LOG = LoggerFactory.getLogger(GMLUtil.class);

  // Not to be constructed.
  private GMLUtil() {
  }

  @SuppressWarnings("rawtypes")
  public static FeatureMember toEmissionSource(final String feature) throws AeriusException {
    try {
      final XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(feature));
      if (xmlStreamReader.nextTag() != XMLStreamConstants.START_ELEMENT) {
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }

      final Class<? extends EmissionSource> clazz = getClassForStartElement(xmlStreamReader.getLocalName());
      final JAXBContext context = JAXBContext.newInstance(clazz);
      return context.createUnmarshaller().unmarshal(new StreamSource(new StringReader(feature)), clazz).getValue();
    } catch (XMLStreamException | FactoryConfigurationError | JAXBException e) {
      LOG.error("Error parsing IMAER feature to emission source", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  private static Class<? extends EmissionSource> getClassForStartElement(final String localName) {
    // TODO; use a more efficient lookup system. One that respects the IMAER version even!
    switch (localName) {
      case "SRM2RoadEmissionSource": return SRM2RoadEmissionSource.class;
      default:
        return null;
      }
  }
}
