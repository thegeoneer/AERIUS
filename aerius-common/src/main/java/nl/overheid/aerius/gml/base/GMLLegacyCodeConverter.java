/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base;

import java.util.HashMap;
import java.util.Map;

/**
 * Util class to convert old codes in previous versions of AERIUS gml files to the codes of the latest version.
 */
public class GMLLegacyCodeConverter {

  /**
   * The type of a legacy code.
   */
  public enum GMLLegacyCodeType {

    ON_ROAD_MOBILE_SOURCE;

  }

  private final Map<GMLLegacyCodeType, Map<String, Conversion>> codeMaps;

  /**
   * @param codeMaps The &lt;OldCode, NewCode&gt; maps to use for each legacy code type.
   */
  public GMLLegacyCodeConverter(final Map<GMLLegacyCodeType, Map<String, Conversion>> codeMaps) {
    this.codeMaps = codeMaps == null ? new HashMap<GMLLegacyCodeType, Map<String, Conversion>>() : codeMaps;
  }

  /**
   * Convert an old code to the right code.
   * In case the old code wasn't changed or can't be found, the same code is returned.
   * @param codeType The code type to find a code for.
   * @param oldCode The old code.
   * @return The proper code for the old code (or the supplied code if not found)
   */
  protected Conversion getConversion(final GMLLegacyCodeType codeType, final String oldCode) {
    return codeMaps.get(codeType) != null ? codeMaps.get(codeType).get(oldCode) : null;
  }

  /**
   * The conversion of a an old code.
   */
  public static class Conversion {

    private final String newValue;
    private final boolean issueWarning;

    /**
     * @param newValue The new value to use in the conversion.
     * @param issueWarning Boolean to indicate if the user should receive a warning about the conversion or not.
     */
    public Conversion(final String newValue, final boolean issueWarning) {
      this.newValue = newValue;
      this.issueWarning = issueWarning;
    }

    public String getNewValue() {
      return newValue;
    }

    public boolean isIssueWarning() {
      return issueWarning;
    }

  }
}
