/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v0_5.source.lodging;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.v0_5.source.EmissionProperty;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.source.FarmAdditionalLodgingSystem;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingCustomEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.domain.source.FarmReductiveLodgingSystem;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public class GML2Farm extends AbstractGML2Specific<FarmLodgingEmissionSource, FarmEmissionSource> {

  private static final Logger LOG = LoggerFactory.getLogger(GML2Farm.class);

  /**
   * @param conversionData The conversionData to use.
   */
  public GML2Farm(final GMLConversionData conversionData) {
    super(conversionData);
  }

  @Override
  public FarmEmissionSource convert(final FarmLodgingEmissionSource source) throws AeriusException {
    final FarmEmissionSource emissionValues = new FarmEmissionSource();
    for (final FarmLodgingProperty lodging : source.getFarmLodgings()) {
      emissionValues.getEmissionSubSources().add(getFarmLodging(lodging.getProperty(), source.getId()));
    }
    return emissionValues;
  }

  private FarmLodgingEmissions getFarmLodging(final AbstractFarmLodging lodging, final String sourceId) throws AeriusException {
    final FarmLodgingEmissions returnLodging;
    if (lodging instanceof CustomFarmLodging) {
      returnLodging = convertCustom((CustomFarmLodging) lodging);
    } else if (lodging instanceof FarmLodging) {
      returnLodging = convertStandard((FarmLodging) lodging, sourceId);
    } else {
      LOG.error("Don't know how to treat lodging type: " + lodging.getClass());
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    returnLodging.setAmount(lodging.getNumberOfAnimals());
    return returnLodging;
  }

  private FarmLodgingCustomEmissions convertCustom(final CustomFarmLodging customLodging) {
    final FarmLodgingCustomEmissions customEmissions = new FarmLodgingCustomEmissions();
    customEmissions.setDescription(customLodging.getDescription());
    for (final EmissionProperty emissionProperty : customLodging.getEmissionFactors()) {
      customEmissions.getEmissionFactors().setEmission(emissionProperty.getProperty().getSubstance(), emissionProperty.getProperty().getValue());
    }
    return customEmissions;
  }

  private FarmLodgingStandardEmissions convertStandard(final FarmLodging standardLodging, final String sourceId) throws AeriusException {
    final FarmLodgingStandardEmissions standardEmissions = new FarmLodgingStandardEmissions();
    final String categoryCode = standardLodging.getCode();
    final FarmLodgingCategory category = getCategories().determineFarmLodgingCategoryByCode(categoryCode);
    if (category == null) {
      //If we can't find the code, throw exception.
      //If it is a custom lodging type, it should have been specified as being one.
      throw new AeriusException(Reason.GML_UNKNOWN_RAV_CODE, sourceId, categoryCode);
    }
    standardEmissions.setCategory(category);
    handleLodgingSystems(standardLodging, sourceId, standardEmissions);

    return standardEmissions;
  }

  private void handleLodgingSystems(final FarmLodging lodging, final String sourceId,
      final FarmLodgingStandardEmissions standardEmissions) throws AeriusException {
    for (final LodgingSystemProperty lodgingSystem : lodging.getLodgingSystems()) {
      if (lodgingSystem.getProperty() instanceof AdditionalLodgingSystem) {
        final FarmAdditionalLodgingSystem resultSystem = getAdditionalSystem((AdditionalLodgingSystem) lodgingSystem.getProperty(), sourceId);
        standardEmissions.getLodgingAdditional().add(resultSystem);
      } else if (lodgingSystem.getProperty() instanceof ReductiveLodgingSystem) {
        final FarmReductiveLodgingSystem resultSystem = getReductiveSystem((ReductiveLodgingSystem) lodgingSystem.getProperty(), sourceId);
        standardEmissions.getLodgingReductive().add(resultSystem);
      }
    }

    //handle BWL code bit. Ït's OK if it's null.
    standardEmissions.setSystemDefinition(getSystemDefinition(lodging.getLodgingSystemDefinitionCode(),
        standardEmissions.getCategory().getFarmLodgingSystemDefinitions()));
  }

  private FarmAdditionalLodgingSystem getAdditionalSystem(final AdditionalLodgingSystem additionalSystem, final String sourceId)
      throws AeriusException {
    final FarmAdditionalLodgingSystem resultSystem = new FarmAdditionalLodgingSystem();
    resultSystem.setAmount(additionalSystem.getNumberOfAnimals());
    final FarmAdditionalLodgingSystemCategory category =
        getCategories().getFarmLodgingCategories().determineAdditionalLodgingSystemByCode(additionalSystem.getCode());
    if (category == null) {
      //If we can't find the code, throw exception.
      //If it is a custom lodging type, it should have been specified as being one.
      throw new AeriusException(Reason.GML_UNKNOWN_RAV_CODE, sourceId, additionalSystem.getCode());
    }
    resultSystem.setCategory(category);
    resultSystem.setSystemDefinition(getSystemDefinition(additionalSystem.getLodgingSystemDefinitionCode(),
        category.getFarmLodgingSystemDefinitions()));
    return resultSystem;
  }

  private FarmReductiveLodgingSystem getReductiveSystem(final ReductiveLodgingSystem reductiveSystem, final String sourceId)
      throws AeriusException {
    final FarmReductiveLodgingSystem resultSystem = new FarmReductiveLodgingSystem();
    final FarmReductiveLodgingSystemCategory category =
        getCategories().getFarmLodgingCategories().determineReductiveLodgingSystemByCode(reductiveSystem.getCode());
    if (category == null) {
      //If we can't find the code, throw exception.
      //If it is a custom lodging type, it should have been specified as being one.
      throw new AeriusException(Reason.GML_UNKNOWN_RAV_CODE, sourceId, reductiveSystem.getCode());
    }
    resultSystem.setCategory(category);
    resultSystem.setSystemDefinition(getSystemDefinition(reductiveSystem.getLodgingSystemDefinitionCode(),
            category.getFarmLodgingSystemDefinitions()));
    return resultSystem;
  }

  private FarmLodgingSystemDefinition getSystemDefinition(final String code, final List<FarmLodgingSystemDefinition> systemDefinitions) {
    FarmLodgingSystemDefinition rightDefinition = null;
    if (code != null) {
      for (final FarmLodgingSystemDefinition systemDefinition : systemDefinitions) {
        if (code.equals(systemDefinition.getCode())) {
          rightDefinition = systemDefinition;
        }
      }
    }
    return rightDefinition;
  }

}
