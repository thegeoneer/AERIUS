/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base.geo;

import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBElement;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;

import net.opengis.gml.v_3_2_1.AbstractGeometryType;
import net.opengis.gml.v_3_2_1.AbstractRingPropertyType;
import net.opengis.gml.v_3_2_1.LineStringType;
import net.opengis.gml.v_3_2_1.LinearRingType;
import net.opengis.gml.v_3_2_1.PointType;
import net.opengis.gml.v_3_2_1.PolygonType;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.gml.base.GMLSchema;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Class to convert gml geometry objects to data geometry objects.
 */
public final class Geo2GeometryUtil {

  private final int srid;
  private final GeometryFactory geometryFactory;

  public Geo2GeometryUtil(final int srid) {
    this.srid = srid;
    geometryFactory = new GeometryFactory(new PrecisionModel(), srid);
  }

  /**
   * Convert a GML-object to a WKTGeometry containing a POINT.
   * @param pointType point
   * @return The WKTGeometry containing POINT.
   * @throws AeriusException When no valid POINT could be made from the gml-object.
   */
  public WKTGeometry fromXMLPoint(final PointType pointType) throws AeriusException {
    validateSrs(srid, pointType);
    final List<Double> values = pointType.getPos().getValue();
    final Point point = geometryFactory.createPoint(new Coordinate(values.get(0), values.get(1)));
    return new WKTGeometry(point.toText());
  }

  /**
   * Convert a GML-object to a WKTGeometry containing a LINESTRING.
   * @param lineStringType lineStringType
   * @return The WKTGeometry containing LINESTRING.
   * @throws AeriusException When no valid LINESTRING could be made from the gml-object.
   */
  public WKTGeometry fromXMLLineString(final LineStringType lineStringType) throws AeriusException {
    validateSrs(srid, lineStringType);
    final Coordinate[] coordinates = lineString2Coordinates(lineStringType.getPosList().getValue());
    final String wktString = geometryFactory.createLineString(coordinates).toText();
    validateWKTGeometryString(wktString);
    final double length = GeometryUtil.determineLength(wktString);
    return new WKTGeometry(wktString, length);
  }

  /**
   * Convert a GML-object Polygon geometries.
   * @param polygonType polygon
   * @return The WKTGeometry containing POLYGON.
   * @throws AeriusException When no valid POLYGON could be made from the gml-object.
   */
  @SuppressWarnings("unchecked")
  public WKTGeometry fromXMLPolygon(final PolygonType polygonType) throws AeriusException {
    validateSrs(srid, polygonType);
    final JAXBElement<LinearRingType> exteriorRing = (JAXBElement<LinearRingType>) polygonType.getExterior().getAbstractRing();
    final List<Double> values = exteriorRing.getValue().getPosList().getValue();

    for (final Iterator<AbstractRingPropertyType> iterator = polygonType.getInterior().iterator(); iterator.hasNext();) {
      final JAXBElement<LinearRingType> innerRing = (JAXBElement<LinearRingType>) iterator.next().getAbstractRing();
      values.addAll(innerRing.getValue().getPosList().getValue());
    }
    final Coordinate[] coordinates = lineString2Coordinates(values);
    final String wktString = geometryFactory.createPolygon(coordinates).toText();
    validateWKTGeometryString(wktString);
    final double area = GeometryUtil.determineArea(wktString);
    return new WKTGeometry(wktString, area);
  }

  private static Coordinate[] lineString2Coordinates(final List<Double> values) {
    final Coordinate[] coordinates = new Coordinate[values.size() / 2];
    for (int i = 0; i < values.size(); i += 2) {
      coordinates[i / 2] = new Coordinate(values.get(i), values.get(i + 1));
    }
    return coordinates;
  }

  /**
   * Validates it the srs is the same as the default srs.
   * It won't trigger on empty srs. It's intended to make srs required in future IMAER versions, the converter then should still allow it for
   * older versions.
   *
   * @param type data to check.
   * @throws AeriusException exception when not the supported srs
   */
  private static void validateSrs(final int srid, final AbstractGeometryType type) throws AeriusException {
    final String expectedSrsName = GMLSchema.getSRSName(srid);
    final String srsName = type.getSrsName();
    if (srsName != null && !srsName.isEmpty() && !expectedSrsName.equalsIgnoreCase(srsName)) {
      throw new AeriusException(Reason.GML_SRS_NAME_UNSUPPORTED, srsName);
    }
  }

  private static void validateWKTGeometryString(final String wkt) throws AeriusException {
    if (!GeometryUtil.validWKT(wkt)) {
      throw new AeriusException(Reason.GML_GEOMETRY_INVALID, wkt);
    }
  }
}
