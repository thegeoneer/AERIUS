/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base.geo;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.gml.base.FeatureMember;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Utility class to convert GML to geometry objects.
 */
public final class GML2Geometry {

  private final Geo2GeometryUtil geo2GeometryUtil;

  public GML2Geometry(final int srid) {
    geo2GeometryUtil = new Geo2GeometryUtil(srid);
  }

  /**
   * Get the geometry.
   * @param gml2Geometry convert gml geometry to object
   * @return the Geometry of this featureMember.
   * @throws AeriusException When underlying geometry could not be parsed to a valid object or when it's not allowed for this feature member type.
   * (does not check for actual valid geometries)
   */
  public <E extends GmlEmissionSourceGeometry<?, ?, ?>> WKTGeometry getGeometry(final FeatureMember<E> fm) throws AeriusException {
    final E emissionSourceGeometry = fm.getEmissionSourceGeometry();
    final WKTGeometry geometry;
    try {
      geometry = constructWKTGeometry(fm.getId(), emissionSourceGeometry);
      isValidGeometry(fm, geometry);
    } catch (final AeriusException e) {
      if (e.getReason() == Reason.GML_GEOMETRY_INVALID) {
        throw new AeriusException(Reason.GML_GEOMETRY_INVALID, fm.getId());
      } else {
        throw e;
      }
    }
    return geometry;
  }

  private <E extends GmlEmissionSourceGeometry<?, ?, ?>> WKTGeometry constructWKTGeometry(final String id, final E emissionSourceGeometry)
      throws AeriusException {
    final WKTGeometry geometry;
    if (emissionSourceGeometry.getPolygon() != null) {
      geometry = fromXMLPolygon(emissionSourceGeometry.getPolygon());
    } else if (emissionSourceGeometry.getLineString() != null) {
      geometry = fromXMLLineString(emissionSourceGeometry.getLineString());
    } else if (emissionSourceGeometry.getPoint() != null) {
      geometry = fromXMLPoint(emissionSourceGeometry.getPoint());
    } else {
      throw new AeriusException(Reason.GML_GEOMETRY_UNKNOWN, id);
    }
    return geometry;
  }

  private <E extends GmlEmissionSourceGeometry<?, ?, ?>> void isValidGeometry(final FeatureMember<E> fm, final WKTGeometry geometry)
      throws AeriusException {
    // check if the geometry is actually valid for this type of feature member.
    if (!fm.isValidGeometry(geometry.getType())) {
      throw new AeriusException(Reason.GML_GEOMETRY_NOT_PERMITTED, fm.getId());
    }
  }

  /**
   * Convert a GML-object to a WKTGeometry containing a POINT.
   * @param gmlPoint The GML-object to convert to WKTGeometry
   * @return The WKTGeometry containing POINT.
   * @throws AeriusException When no valid POINT could be made from the gml-object.
   */
  public WKTGeometry fromXMLPoint(final GmlPoint gmlPoint) throws AeriusException {
    return geo2GeometryUtil.fromXMLPoint(gmlPoint.getGmlPoint());
  }

  /**
   * Convert a GML-object to a WKTGeometry containing a LINESTRING.
   * @param gmlString The GML-object to convert to WKTGeometry
   * @return The WKTGeometry containing LINESTRING.
   * @throws AeriusException When no valid LINESTRING could be made from the gml-object.
   */
  public WKTGeometry fromXMLLineString(final GmlLineString gmlString) throws AeriusException {
    return geo2GeometryUtil.fromXMLLineString(gmlString.getGMLLineString());
  }

  /**
   * Convert a GML-object to a WKTGeometry containing a POLYGON.
   * @param gmlPolygon The GML-object to convert to WKTGeometry
   * @return The WKTGeometry containing POLYGON.
   * @throws AeriusException When no valid POLYGON could be made from the gml-object.
   */
  public WKTGeometry fromXMLPolygon(final GmlPolygon gmlPolygon) throws AeriusException {
    return geo2GeometryUtil.fromXMLPolygon(gmlPolygon.getGmlPolygon());
  }
}
