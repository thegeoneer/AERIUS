/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.opengis.gml.v_3_2_1.ObjectFactory;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.FeatureCollection;
import nl.overheid.aerius.gml.base.FeatureMember;
import nl.overheid.aerius.gml.base.GMLSchema;
import nl.overheid.aerius.gml.base.GMLVersionWriter;
import nl.overheid.aerius.gml.base.MetaData;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.gml.v2_2.togml.GMLVersionWriterV22;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.ResearchAreaCalculationScenario;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.util.FileUtil;

/**
 * Builder class to create GML content from data objects.
 */
public final class GMLWriter {

  /**
   * Mimetype to be used for GML.
   */
  public static final String GML_MIMETYPE = "application/gml+xml";
  /**
   * @deprecated name of GML file should be determined base on context not a generic name.
   */
  @Deprecated
  public static final String CURRENT_GML = "Huidige_Situatie.gml";
  /**
   * @deprecated name of GML file should be determined base on context not a generic name.
   */
  @Deprecated
  public static final String PROPOSED_GML = "Nieuwe_Situatie.gml";


  /**
   * Current GML version data is exported to.
   */
  public static final AeriusGMLVersion LATEST_WRITER_VERSION = AeriusGMLVersion.V2_2;

  private static final Logger LOG = LoggerFactory.getLogger(GMLWriter.class);
  private static final String GML_EXTENSION = ".gml";
  private static final String GML_FILE_PREFIX = "AERIUS";
  private static final String FILE_PREFIX = "gml_";
  private static final String GML_ENCODING = StandardCharsets.UTF_8.name();

  // can't use the Context list of substances, as this includes NOXNH3.
  // IF YOU CHANGE THE ORDER A TEST WILL FAIL!
  private static final Substance[] EMISSION_SUBSTANCES = {Substance.NH3, Substance.NOX, Substance.PM10, Substance.NO2};
  //While PM25 isn't a valid emission substance (OPS at least can't handle it), it is a valid result substance.
  //NO2 can't be used by OPS, but should be usable for SMR2.
  // IF YOU CHANGE THE ORDER A TEST WILL FAIL!
  private static final Substance[] RESULT_SUBSTANCES = {Substance.NH3, Substance.NOX, Substance.PM10, Substance.NO2, Substance.PM25};

  private final GMLVersionWriter writer;
  private boolean overrideReference = true;

  public GMLWriter(final ReceptorGridSettings rgs) {
    writer = new GMLVersionWriterV22(rgs.getZoomLevel1(), GMLSchema.getSRSName(rgs.getEpsg().getSrid()));
  }

  public GMLVersionWriter getWriter() {
    return writer;
  }

  /**
   * Build GML's and convert them to Strings.
   * Be wary to use this method with large sets of results (or large sets of sources).
   * @param databaseVersion The database version used
   * @param scenario Scenario containing all source data
   * @return The objects representing one or more GMLs.
   * @throws AeriusException When exception occurred generating the GML.
   */
  public ArrayList<StringDataSource> writeToStrings(final String databaseVersion, final CalculatedScenario scenario) throws AeriusException {
    final ArrayList<StringDataSource> gmls = new ArrayList<>();
    for (final Calculation cs : scenario.getCalculations()) {
      final StringDataSource data;
      try (ByteArrayOutputStream boas = new ByteArrayOutputStream()) {
        writeToOutputStream(boas, databaseVersion, cs, scenario, null);
        data = new StringDataSource(boas.toString(GML_ENCODING), getFileName(cs.getSources(), null), GML_MIMETYPE);
      } catch (final IOException e) {
        //catch any exception and put them in a GML exception.
        LOG.error("Internal error occurred.", e);
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }

      if (LOG.isDebugEnabled()) {
        try {
          FileUtil.toFile(FILE_PREFIX, data.getFileName(), data.getData());
        } catch (final IOException e) {
          LOG.error("Problems creating gml file: " + data.getFileName(), e);
        }
      }
      gmls.add(data);

    }
    return gmls;
  }

  /**
   * Build GML's and write them directly to files.
   * The files will be generated in the supplied directory.
   * @param dir The directory to write the files to.
   * @param databaseVersion The database version used.
   * @param scenario Scenario containing all source data
   * @param filter The filter to use when constructing the GML, filtering out emission sources for instance. Can be null.
   * @return The list of files generated.
   * @throws AeriusException When exception occurred generating the GML.
   */
  public List<File> writeToFiles(final File dir, final String databaseVersion, final CalculatedScenario scenario, final GMLBuilderFilter filter)
      throws AeriusException {
    final List<File> generated = new ArrayList<>();
    for (final Calculation cs : scenario.getCalculations()) {
      final Path file = new File(dir, getFileName(cs.getSources(), filter)).toPath();
      try (final OutputStream outputStream = Files.newOutputStream(file)) {
        writeToOutputStream(outputStream, databaseVersion, cs, scenario, filter);
      } catch (final IOException e) {
        //catch any exception and put them in a GML exception.
        LOG.error("Internal error occurred.", e);
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }

      generated.add(file.toFile());
      LOG.info("File generated for {} in {} to {}. filter: {}", cs.getSources().getName(), scenario.getMetaData(), file.toString(),
          filter);
    }
    return generated;
  }

  private void writeToOutputStream(final OutputStream outputStream, final String databaseVersion, final Calculation cs,
      final CalculatedScenario scenario, final GMLBuilderFilter filter) throws AeriusException {
    final List<AeriusPoint> receptorPoints = getReceptors(cs.getCalculationId(), scenario);
    final MetaDataInput metaData = getMetaData(databaseVersion, cs, scenario);
    write(outputStream, cs.getSources(), receptorPoints, metaData, filter);
  }

  private MetaDataInput getMetaData(final String databaseVersion, final Calculation cs, final CalculatedScenario scenario) {
    final MetaDataInput metaData = new MetaDataInput();
    metaData.setScenarioMetaData(scenario.getMetaData());
    metaData.setYear(cs.getYear());
    metaData.setVersion(AeriusVersion.getVersionNumber());
    metaData.setDatabaseVersion(databaseVersion);
    metaData.setOptions(scenario.getOptions());
    metaData.setResultsIncluded(cs.getCalculationId() > 0);
    if (scenario instanceof ResearchAreaCalculationScenario) {
      metaData.setResearchArea(Boolean.TRUE);
    }
    return metaData;
  }

  /**
   * Write a list of emission sources to GML.
   * @param outputStream The outputstream to use when writing the GML.
   * @param sources Convert these sources to GML.
   * @param metaData Object containing the metadata like version, db-version and target year.
   * @throws AeriusException When the GML in the inputstream could not be converted to objects.
   */
  public void writeEmissionSources(final OutputStream outputStream, final List<EmissionSource> sources, final MetaDataInput metaDataInput)
      throws AeriusException {
    final MetaData metaData = writer.metaData2GML(metaDataInput);
    final List<FeatureMember> featureMembers = emissionSourcesToFeatures(sources, metaData.getYear(), null);
    toXMLString(outputStream, featureMembers, metaData);
  }

  /**
   * Convert an emission source to GML. Will always assume the source to yield one featureMember.
   * If it yields more (like with a SRM2Network will automatically use the first featureMember.
   * @param outputStream The outputstream to use when writing the GML.
   * @param source Convert source to GML.
   * @param year The year to use for emission calculations.
   * @throws AeriusException When the source could not be converted to GML.
   */
  public void writeEmissionSource(final OutputStream outputStream, final EmissionSource source, final int year) throws AeriusException {
    final FeatureMember featureMember = writer.source2GML(source, EMISSION_SUBSTANCES, year).get(0);

    toXMLString(outputStream, featureMember, true);
  }

  /**
   * Write a list of receptor points to GML.
   * @param outputStream The outputstream to use when writing the GML.
   * @param receptors Convert these receptor points to GML.
   * @param metaData Object containing the metadata like version, db-version and target year.
   * @throws AeriusException When the GML in the inputstream could not be converted to objects.
   */
  public void writeAeriusPoints(final OutputStream outputStream, final List<AeriusPoint> receptors, final MetaDataInput metaDataInput)
      throws AeriusException {
    final List<FeatureMember> featureMembers = aeriusResultPointsToFeatures(receptors);
    toXMLString(outputStream, featureMembers, metaDataInput == null ? null : writer.metaData2GML(metaDataInput));
  }

  /**
   * Get the receptors from the scenario: results if calculation id is valid, custom calculation point otherwise (and available).
   *
   * @param calculationId id of the calculation or null
   * @param scenario Scenario containing the points.
   * @return A list of points which could be AeriusResultPoints as well.
   */
  private static List<AeriusPoint> getReceptors(final Integer calculationId, final CalculatedScenario scenario) {
    final List<AeriusPoint> receptors = new ArrayList<>();
    if (calculationId == null || calculationId == 0) {
      receptors.addAll(scenario.getCalculationPoints());
    } else {
      receptors.addAll(scenario.getResultPoints(calculationId));
    }
    return receptors;
  }

  /**
   * Convert a list of emission sources and a list of AeriusPoints to GML.
   * @param outputStream The outputstream to use when writing the GML.
   * @param sources Convert these sources to GML.
   * @param aeriusPoints Convert these points to GML.
   * @param metaDataInput Object containing the information needed for GML metadata.
   * @throws AeriusException When the objects could not be converted to GML.
   */
  void write(final OutputStream outputStream, final EmissionSourceList sources, final List<AeriusPoint> aeriusPoints,
      final MetaDataInput metaDataInput) throws AeriusException {
    write(outputStream, sources, aeriusPoints, metaDataInput, null);
  }

  /**
   * Convert a list of emission sources and a list of AeriusPoints to GML.
   * @param outputStream The outputstream to use when writing the GML.
   * @param sources Convert these sources to GML.
   * @param aeriusPoints Convert these points to GML.
   * @param metaDataInput Object containing the information needed for GML metadata.
   * @param sectorId the sectorid, null for total results
   * @throws AeriusException When the objects could not be converted to GML.
   */
  void write(final OutputStream outputStream, final EmissionSourceList sources, final List<AeriusPoint> aeriusPoints,
      final MetaDataInput metaDataInput, final GMLBuilderFilter filter) throws AeriusException {
    final List<FeatureMember> featureMembers = emissionSourcesToFeatures(sources, metaDataInput.getYear(), filter);
    featureMembers.addAll(aeriusResultPointsToFeatures(aeriusPoints));
    metaDataInput.setName(sources.getName());
    final MetaData metaDataImpl = writer.metaData2GML(metaDataInput);
    if (overrideReference || StringUtils.isEmpty(metaDataImpl.getReference())) {
      final String newReference = ReferenceUtil.generatePermitReference();
      metaDataImpl.setReference(newReference);
      metaDataInput.getScenarioMetaData().setReference(newReference);
    }
    toXMLString(outputStream, featureMembers, metaDataImpl);
  }

  private List<FeatureMember> emissionSourcesToFeatures(final List<EmissionSource> sources, final Integer year, final GMLBuilderFilter filter)
      throws AeriusException {
    final List<FeatureMember> featureMembers = new ArrayList<>();
    for (final EmissionSource source : sources) {
      if (filter == null || filter.include(source)) {
        featureMembers.addAll(writer.source2GML(source, EMISSION_SUBSTANCES, year));
      }
    }
    return featureMembers;
  }

  private void toXMLString(final OutputStream outputStream, final List<FeatureMember> featureMembers, final MetaData metaData)
      throws AeriusException {
    final FeatureCollection collection = writer.createFeatureCollection();
    collection.setFeatureMembers(featureMembers);
    collection.setMetaData(metaData);

    toXMLString(outputStream, collection, false);
  }

  private void toXMLString(final OutputStream outputStream, final Object jaxbElement, final boolean fragment) throws AeriusException {
    try {
      //create context/marshaller based on our jaxbElement.
      final JAXBContext jaxbContext = JAXBContext.newInstance(jaxbElement.getClass(), ObjectFactory.class);
      final Marshaller marshaller = jaxbContext.createMarshaller();

      //assure line breaks and indentation for pretty output.
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

      //set the targetNameSpaceLocation (namespace and actual location separated by a space, can be multiple combinations)
      marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, writer.getNameSpace() + " " + writer.getPublicSchemaLocation());
      marshaller.setProperty(Marshaller.JAXB_ENCODING, GML_ENCODING);

      if (fragment) {
        marshaller.marshal(
            new JAXBElement(
                new QName(writer.getNameSpace(), jaxbElement.getClass().getSimpleName()),
                jaxbElement.getClass(), jaxbElement),
            outputStream);
      } else {
        marshaller.marshal(jaxbElement, outputStream);
      }
    } catch (final JAXBException e) {
      LOG.error("Building GML failed", e);
      throw new AeriusException(Reason.GML_CREATION_FAILED, e.getLinkedException().getMessage());
    }
  }

  private String getFileName(final EmissionSourceList sources, final GMLBuilderFilter filter) {
    //filename has to be unique in ZIP, so assure the filename is unique for comparison modes by using the sourcelist id.
    final List<String> fileParts = new ArrayList<>();
    fileParts.add(Integer.toString(sources.getId()));
    if (!StringUtils.isEmpty(sources.getName())) {
      fileParts.add(sources.getName());
    }
    if (filter != null && !StringUtils.isEmpty(filter.getOptionalFileName())) {
      fileParts.add(filter.getOptionalFileName());
    }
    return FileUtil.getFileName(GML_FILE_PREFIX, GML_EXTENSION, StringUtils.join(fileParts, "_"), null);
  }

  private List<FeatureMember> aeriusResultPointsToFeatures(final List<AeriusPoint> arrayList) throws AeriusException {
    final List<FeatureMember> featureMembers = new ArrayList<>();
    for (final AeriusPoint point : arrayList) {
      featureMembers.add(writer.result2GML(point, RESULT_SUBSTANCES));
    }
    return featureMembers;
  }

  public boolean isOverrideReference() {
    return overrideReference;
  }

  public void setOverrideReference(final boolean overrideReference) {
    this.overrideReference = overrideReference;
  }

}
