/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.Conversion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.GMLLegacyCodeType;
import nl.overheid.aerius.gml.base.source.ship.InlandShippingUtil;
import nl.overheid.aerius.shared.domain.ConsecutiveNamedList;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.HasShippingRoute;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Data object to keep track of conversion data.
 */
public class GMLConversionData {

  private final List<AeriusException> errors;
  private final List<AeriusException> warnings;
  private final Map<String, Map<Integer, String>> idMap = new HashMap<>();
  private final GMLLegacyCodeConverter legacyCodeConverter;
  private final int srid;
  private final ReceptorUtil receptorUtil;
  private final List<ShippingRoute> inlandRoutes = new ConsecutiveNamedList<>();
  private final EmissionSourceList emissionSourceList = new EmissionSourceList();
  private final SectorCategories categories;
  private final InlandShippingUtil inlandRouteUtil;

  /**
   * @param pmf database connection manager
   * @param legacyCodeConverter The converter for old category codes to use.
   * @param categories categories
   * @param rgs ReceptorGridSettings
   * @param errors
   * @param warnings
   */
  public GMLConversionData(final PMF pmf, final GMLLegacyCodeConverter legacyCodeConverter, final SectorCategories categories,
      final ReceptorGridSettings rgs, final List<AeriusException> errors, final List<AeriusException> warnings) {
    this.legacyCodeConverter = legacyCodeConverter;
    this.categories = categories;
    this.srid = rgs.getEpsg().getSrid();
    this.receptorUtil = new ReceptorUtil(rgs);
    this.errors = errors;
    this.warnings = warnings;
    inlandRouteUtil = new InlandShippingUtil(pmf, warnings);
  }

  public int getSrid() {
    return srid;
  }

  public ReceptorUtil getReceptorUtil() {
    return receptorUtil;
  }

  public SectorCategories getSectorCategories() {
    return categories;
  }

  /**
   * @return the errors
   */
  public List<AeriusException> getErrors() {
    return errors;
  }

  /**
   * @return the warnings
   */
  public List<AeriusException> getWarnings() {
    return warnings;
  }

  /**
   * Checks if given id is unique and if not adds it to the list of exceptions.
   * @param networkId
   * @param imaerId IMAER number id.
   * @param gmlId Feature Member String id
   */
  public void putAndTrack(final String networkId, final Integer imaerId, final String gmlId) {
    final String globalId = networkId == null ? "" : networkId;
    if (!idMap.containsKey(globalId)) {
      idMap.put(globalId, new HashMap<Integer, String>());
    }
    final Map<Integer, String> sMap = idMap.get(globalId);
    final String existingGmlId = sMap.get(imaerId);
    if (existingGmlId == null) {
      sMap.put(imaerId, gmlId);
    } else {
      errors.add(new AeriusException(Reason.GML_ID_NOT_UNIQUE, gmlId, existingGmlId));
    }
  }

  /**
   * @param codeType The code type to convert.
   * @param oldCode The old code to try and convert. Will be returned if not converted.
   * @param forSource The label of the source for which the code is converted, used for warnings.
   * @return The proper code to use.
   */
  public String getCode(final GMLLegacyCodeType codeType, final String oldCode, final String forSource) {
    String returnCode;
    final Conversion conversion = legacyCodeConverter.getConversion(codeType, oldCode);
    if (conversion != null) {
      returnCode = conversion.getNewValue();
      if (conversion.isIssueWarning()) {
        warnings.add(new AeriusException(getReason(codeType), oldCode, returnCode, forSource));
      }
    } else {
      returnCode = oldCode;
    }
    return returnCode;
  }

  private Reason getReason(final GMLLegacyCodeType codeType) {
    final Reason reason;
    switch (codeType) {
    case ON_ROAD_MOBILE_SOURCE:
      reason = Reason.GML_INVALID_ROAD_CATEGORY_MATCH;
      break;
    default:
      reason = Reason.GML_INVALID_CATEGORY_MATCH;
      break;
    }
    return reason;
  }

  public EmissionSourceList getEmissionSourceList() {
    return emissionSourceList;
  }

  public List<ShippingRoute> getInlandRoutes() {
    return inlandRoutes;
  }

  public List<ShippingRoute> getMaritimeRoutes() {
    return emissionSourceList.getMaritimeRoutes();
  }

  public void setOrForceWaterwayType(final String label, final HasShippingRoute route, final InlandWaterwayType inlandWaterwayType)
      throws AeriusException {
    inlandRouteUtil.setOrForceWaterwayType(categories, label, route, inlandWaterwayType);
  }
}
