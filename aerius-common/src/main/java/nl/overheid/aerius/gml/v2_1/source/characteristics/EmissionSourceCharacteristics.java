/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_1.source.characteristics;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import nl.overheid.aerius.gml.v2_1.base.CalculatorSchema;

/**
 *
 */
@XmlType(name = "EmissionSourceCharacteristicsType", namespace = CalculatorSchema.NAMESPACE,
    propOrder = {"heatContent", "emissionHeight", "buildingHeight", "spread", "diurnalVariation", "heatContentSpecificationProperty"})
public class EmissionSourceCharacteristics {

  private double heatContent;
  private double emissionHeight;
  private Double buildingHeight;
  private Double spread;
  private String diurnalVariation;
  private HeatContentSpecification heatContentSpecification;

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public double getHeatContent() {
    return heatContent;
  }

  public void setHeatContent(final double heatContent) {
    this.heatContent = heatContent;
  }

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public double getEmissionHeight() {
    return emissionHeight;
  }

  public void setEmissionHeight(final double emissionHeight) {
    this.emissionHeight = emissionHeight;
  }

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public Double getBuildingHeight() {
    return buildingHeight;
  }

  public void setBuildingHeight(final Double buildingHeight) {
    this.buildingHeight = buildingHeight;
  }

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public Double getSpread() {
    return spread;
  }

  public void setSpread(final Double spread) {
    this.spread = spread;
  }

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public String getDiurnalVariation() {
    return diurnalVariation;
  }

  public void setDiurnalVariation(final String diurnalVariation) {
    this.diurnalVariation = diurnalVariation;
  }

  @XmlTransient
  public HeatContentSpecification getHeatContentSpecification() {
    return heatContentSpecification;
  }

  public void setHeatContentSpecification(final HeatContentSpecification heatContentSpecification) {
    this.heatContentSpecification = heatContentSpecification;
  }

  @XmlElement(name = "heatContentSpecification", namespace = CalculatorSchema.NAMESPACE)
  public HeatContentSpecificationProperty getHeatContentSpecificationProperty() {
    return heatContentSpecification == null ? null : new HeatContentSpecificationProperty(heatContentSpecification);
  }

  public void setHeatContentSpecificationProperty(final HeatContentSpecificationProperty heatContentSpecification) {
    this.heatContentSpecification = heatContentSpecification == null ? null : heatContentSpecification.getProperty();
  }

}
