/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface to specific AERIUS GML version readers.
 */
public interface GMLVersionReader {

  /**
   * Get the {@link EmissionSource} objects from the GML data objects.
   * @param members The GML data objects to convert.
   * @return The list of EmissionSource objects
   * @throws AeriusException In case of a database exception.
   */
  ArrayList<EmissionSource> fromGML(final List<FeatureMember> members) throws AeriusException;

  /**
   * Get the {@link AeriusPoint} object form the GML data object. Implementing classes should check if the passed member actually is the member
   * the method is looking for.
   * @param member GML data object
   * @param includeReceptorPoint if true also read ReceptorPoints from GML.
   * @return new AeriusPoint object
   * @throws AeriusException in case of an error.
   */
  AeriusPoint fromGML(final FeatureMember member, boolean includeReceptorPoint) throws AeriusException;
}
