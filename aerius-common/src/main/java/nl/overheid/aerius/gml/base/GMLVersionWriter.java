/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base;

import java.util.List;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface to be implemented by all AERIUS GML versions that are supported to generate a GML from data.
 */
public interface GMLVersionWriter {

  /**
   * Returns the specific AERIUS GML version for the implementing class.
   * @return AERIUS GML version
   */
  AeriusGMLVersion getGMLVersion();

  /**
   * Public schema location for this AERIUS GML version.
   * @return schema location
   */
  String getPublicSchemaLocation();

  /**
   * Returns the name space related to this GML.
   * @return name space
   */
  String getNameSpace();

  /**
   * Creates a new instance of the implementing {@link FeatureCollection}.
   * @return new instance
   */
  FeatureCollection createFeatureCollection();

  /**
   * Converts object meta data to GML meta data object.
   * @param metaDataInput Object containing all information needed for the gml metadata object.
   * @return GML meta data object
   */
  MetaData metaData2GML(MetaDataInput metaDataInput);

  /**
   * Converts an emission source to one or more GML data objects that represent that emission source.
   * While an emission source can have emission values for multiple years and substances, the GML data object has emissions for one specific year.
   * @param source the emission source
   * @param substances the substances for which emissions must be copied.
   * @param year the year to get the emissions for
   * @return GML emission source data objects
   * @throws AeriusException internal or data error
   */
  List<FeatureMember> source2GML(EmissionSource source, Substance[] substances, Integer year) throws AeriusException;

  /**
   * Converts a receptor point to a GML data object of that point with results for the given substances.
   * @param point receptor point to convert
   * @param substances substances to get results for
   * @return GML receptor point
   * @throws AeriusException internal or data error
   */
  FeatureMember result2GML(AeriusPoint point, Substance[] substances) throws AeriusException;
}
