/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.export;

import java.io.IOException;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.OPSExportData;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.TaskResultCallback;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Utility class to send export tasks to the workers.
 */
public final class ExportTaskClient {

  private ExportTaskClient() {
    // utility class
  }

  /**
   * @param client The client to use to send the task.
   * @param properties The properties containing extra data like year and email address.
   * @param scenario The CalculatedScenario to export as OPS input files.
   * @param callback Callback method will be called when result is in.
   * @throws IOException In case putting task on queue failed.
   */
  public static void startOPSExport(final TaskManagerClient client, final ExportProperties properties,
      final CalculatedScenario scenario, final TaskResultCallback callback) throws IOException {
    final OPSExportData inputData = new OPSExportData();

    scenario.setYear(properties.getYear());
    inputData.setScenario(scenario);

    inputData.setEmailAddress(properties.getEmailAddress());
    inputData.setLocale(properties.getLocale());
    client.sendTask(inputData, callback, WorkerType.CALCULATOR, QueueEnum.OPS_EXPORT);
  }

  /**
   * @param client The client to use to send the task.
   * @param workerType the worker type to use for the queue.
   * @param queueName name of the queue to place the work on.
   * @param properties The properties containing extra data like year and emailAddress.
   * @param scenario The CalculatedScenario to export as GML file(s).
   * @param callback Callback method will be called when result is in.
   * @return a random unique task ID.
   * @throws IOException In case putting task on queue failed.
   */
  public static String startGMLExport(final TaskManagerClient client, final WorkerType workerType, final QueueEnum queueName,
      final ExportProperties properties, final CalculatedScenario scenario, final TaskResultCallback callback) throws IOException {
    final CalculationInputData inputData = new CalculationInputData();
    setScenarioData(queueName, properties, scenario, inputData);
    return client.sendTask(inputData, callback, workerType, queueName);
  }


  /**
   * @param client The client to use to send the task.
   * @param workerType the worker type to use for the queue.
   * @param queueName name of the queue to place the work on.
   * @param priorityProjectExportData The properties containing extra data like year and emailAddress.
   * @param callback Callback method will be called when result is in.
   * @return a random unique task ID.
   * @throws IOException In case putting task on queue failed.
   */
  public static String startPriorityProjectResultExport(final TaskManagerClient client, final WorkerType workerType, final QueueEnum queueName,
      final PriorityProjectExportData priorityProjectExportData, final TaskResultCallback callback) throws IOException {
    return client.sendTask(priorityProjectExportData, callback, workerType, queueName);
  }


  /**
   * Invoked when a PAAExport is to be started.
   * @param client the message bus client.
   * @param workerType the worker type to use for the queue.
   * @param queueName name of the queue to place the work on.
   * @param properties the export properties to use
   * @param scenario The scenario containing the data.
   * @param callback The callback that must be called when the export is finished.
   * @return a random unique task ID.
   * @throws IOException In case putting the task on the queue failed.
   */
  public static String startPAAExport(final TaskManagerClient client, final WorkerType workerType, final QueueEnum queueName,
      final ExportProperties properties, final CalculatedScenario scenario, final TaskResultCallback callback) throws IOException {
    final CalculationInputData inputData = new CalculationInputData();
    scenario.setMetaData(properties.getScenarioMetaData());
    final ScenarioMetaData metaData = scenario.getMetaData();
    final String description = metaData.getDescription();
    metaData.setDescription(description == null ? ""
        : description.substring(0, Math.min(description.length(), SharedConstants.PAA_DESCRIPTION_LENGTH)));
    setScenarioData(queueName, properties, scenario, inputData);
    return client.sendTask(inputData, callback, workerType, queueName);
  }

  private static void setScenarioData(final QueueEnum queueName, final ExportProperties properties, final CalculatedScenario scenario,
      final CalculationInputData inputData) {
    inputData.setExportType(properties.getExportType());
    inputData.setName(properties.getName());
    inputData.setEmailAddress(properties.getEmailAddress());
    inputData.setLocale(properties.getLocale());
    inputData.setQueueName(queueName.getQueueName());
    inputData.setScenario(scenario);
    inputData.setAdditionalOptions(properties.getAdditionalOptions());
    scenario.setYear(properties.getYear());
  }
}
