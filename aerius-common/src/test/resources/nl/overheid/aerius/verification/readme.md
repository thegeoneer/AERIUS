## verification purpose
Verification tests are a way to ensure verified results when converting from one file type to another (e.g. from .GML to OPS' .BRN) stay the same.
This is done to be more confident that AERIUS handles everything as expected and supplies the proper file to a (external) calculation model like OPS.
Currently most, if not all, files are supplied/verified by Diederik Metz.

## verification file name convention
name of verification test files should be: 

```
{sector_id}{sector_name}{number of other specific characteristic}.{extension}
```
