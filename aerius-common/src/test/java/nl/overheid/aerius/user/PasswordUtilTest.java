/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

/**
 * Helper class to generate a new password.
 */
public final class PasswordUtilTest {
  @Test
  public void testPasswordRandomness() {
    final String a = PasswordUtil.createPassword();
    final String b = PasswordUtil.createPassword();

    assertNotEquals("Assert password should not be the same", a, b);
  }

  @Test
  public void testPasswordValidity() {
    final String a = PasswordUtil.createPassword();

    assertEquals("Test passwowrd lenght", a.length(), 8);
  }
}
