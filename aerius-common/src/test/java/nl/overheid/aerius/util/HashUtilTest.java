/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Test;

public class HashUtilTest {

  private static final String TEST_SALT = "this\ris\r\nmy\ntest\nsalt";
  private static final String TEST_STRING = "this\ris\r\nmy\ntest\nstring";

  @Test
  public void testHash() {
    final String testHash = "J0bZEbqPFhnaTv8pvtKdRWHBMl0=";
    final ArrayList<String> data = new ArrayList<>();
    data.add(TEST_STRING);

    // Create and test hash1
    final String hash1 = HashUtil.generateSaltedHash(TEST_SALT, data);
    assertNotNull(hash1);
    assertEquals(testHash, hash1);

    // Create and test hash2
    final String hash2 = HashUtil.generateSaltedHash(TEST_SALT, data);
    assertNotNull(hash2);
    assertEquals(testHash, hash2);

    // Compare both hashes
    assertEquals(hash1, hash2);
  }

  @Test
  public void testHashMultipleStrings() {
    final String testString2 = "this'then'wouldarrrr<be>dfasdf>/another>";
    final String testHash = "z9iXhb9YbloOneMUExcNzuQ+VI4=";
    ArrayList<String> data = new ArrayList<>();
    data.add(TEST_STRING);
    data.add(testString2);

    // Create and test hash1
    final String hash1 = HashUtil.generateSaltedHash(TEST_SALT, data);
    assertNotNull(hash1);
    assertEquals(testHash, hash1);

    // Create and test hash2
    final String hash2 = HashUtil.generateSaltedHash(TEST_SALT, data);
    assertNotNull(hash2);
    assertEquals(testHash, hash2);

    // Compare both hashes
    assertEquals(hash1, hash2);

    //reverse order and try again. Hashes should NOT be equal now. (or should they?)
    data.clear();
    data.add(testString2);
    data.add(TEST_STRING);
    final String hash3 = HashUtil.generateSaltedHash(TEST_SALT, data);
    assertNotNull(hash3);
    assertNotEquals(testHash, hash3);

  }
}
