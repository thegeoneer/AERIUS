/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class to check how the GML importer handles invalid input.
 */
public class InvalidGMLTest extends BaseDBTest {

  private static final String LATEST_VALIDATE = "latest/validate/";

  private Importer importer;

  @Override
  @Before
  public void setUp() throws SQLException, AeriusException {
    importer = new Importer(getCalcPMF());
  }

  @Test(expected = AeriusException.class)
  public void testInvalidOpsValue() throws IOException, AeriusException, SQLException {
    assertResult("invalid_ops_values.gml", "Invalid ops input values", Reason.SOURCE_VALIDATION_FAILED);
  }

  @Test(expected = AeriusException.class)
  public void testInvalidSector() throws IOException, AeriusException, SQLException {
    assertResult("invalid_sector.gml", "Unknown sector code", Reason.GML_GENERIC_PARSE_ERROR);
  }

  @Test(expected = AeriusException.class)
  public void testInvalidSubstance() throws IOException, AeriusException, SQLException {
    try {
      assertResult("invalid_substance.gml", "Expected invalid substance", Reason.GML_VALIDATION_FAILED);
    } catch (final AeriusException e) {
      assertFalse("Should show list of possible options", e.getArgs()[0].isEmpty());
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testInvalidXML1() throws IOException, AeriusException, SQLException {
    try {
      assertResult("invalid_xml_1.gml", "Expected GML parse error, invalid tag", Reason.GML_PARSE_ERROR);
    } catch (final AeriusException e) {
      assertEquals("Row", "15", e.getArgs()[0]);
      assertEquals("Column", "19", e.getArgs()[1]);
      assertEquals("Tag in error", "imaer:namespace", e.getArgs()[2]);
      assertEquals("Tag expected", "</imaer:namespace>", e.getArgs()[3]);
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testInvalidXML2() throws IOException, AeriusException, SQLException {
    try {
      assertResult("invalid_xml_2.gml", "Expected GML parse error", Reason.GML_PARSE_ERROR);
    } catch (final AeriusException e) {
      assertEquals("Row", "13", e.getArgs()[0]);
      assertEquals("Column", "23", e.getArgs()[1]);
      assertEquals("Tag in error", "imaer:NEN3610ID", e.getArgs()[2]);
      assertEquals("Tag expected", "</imaer:NEN3610ID>", e.getArgs()[3]);
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testInvalidXML3() throws IOException, AeriusException, SQLException {
    assertResult("invalid_xml_3.gml", "Expected GML parse error", Reason.GML_VALIDATION_FAILED);
  }

  @Test(expected = AeriusException.class)
  public void testInvalidYear() throws IOException, AeriusException, SQLException {
    assertResult("invalid_year.gml", "Expected incorrect year message", Reason.GML_VALIDATION_FAILED);
  }

  @Test(expected = AeriusException.class)
  public void testUnsupportedAeriusGMLVersion() throws IOException, AeriusException, SQLException {
    assertResult("unsupported_aerius_gml_version.gml", "", Reason.GML_VERSION_NOT_SUPPORTED);
  }

  @Test(expected = AeriusException.class)
  public void testUnsupportedSrsName() throws IOException, AeriusException, SQLException {
    assertResult("unsupported_srs_name.gml", "Invalid SRS name for geometry", Reason.GML_SRS_NAME_UNSUPPORTED);
  }

  @Test(expected = AeriusException.class)
  public void testInvalidSRM2Network() throws IOException, AeriusException, SQLException {
    assertResult("invalid_srm2network.gml", "SRM2 network contains links to unexisting sources objects.", Reason.SOURCE_VALIDATION_FAILED);
  }

  private void assertResult(final String fileName, final String expectedReasonTxt, final Reason expectedReason)
      throws IOException, SQLException, AeriusException {
    try {
      final ImportResult result = getImportResult(LATEST_VALIDATE, fileName);
      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
    } catch (final AeriusException e) {
      assertSame(expectedReasonTxt, expectedReason, e.getReason());
      throw e;
    }
  }

  private File getFile(final String relativePath, final String fileName) throws FileNotFoundException {
    final URL url = getClass().getResource(relativePath + fileName);
    File file = null;
    if (url != null) {
      file = new File(url.getFile());
      if (!file.exists()) {
        file = null;
      }
    }
    return file;
  }

  private ImportResult getImportResult(final String relativePath, final String fileName)
      throws IOException, AeriusException, SQLException {
    ImportResult result = null;
    final File file = getFile(relativePath, fileName);
    if (file == null) {
      throw new FileNotFoundException(fileName);
    } else {
      try (final InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
        result = importer.convertInputStream2ImportResult(fileName, inputStream);
      }
    }
    return result;
  }
}
