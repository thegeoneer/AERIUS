/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class that reads an old version GML and compares it to the current version of that same GML.
 * Tests if old versions can be imported and if emissions are different based on different versions.
 */
@RunWith(Parameterized.class)
public class GMLOldVersionTest extends BaseDBTest {

  private static final Logger LOG = LoggerFactory.getLogger(GMLOldVersionTest.class);

  private enum TestFile {
    FARM,
    FARM_WITH_SYSTEMS,
    FARM_WITH_SYSTEMS_AND_FODDER_MEASURES,
    ROAD,
    ON_ROAD,
    OFF_ROAD,
    NETWORK_ROAD,
    PLAN,
    ROUTE_INLAND_SHIPPING,
    ROUTE_MARITIME_SHIPPING,
    MOORING_INLAND_SHIPPING,
    MOORING_MARITIME_SHIPPING,
    METADATA;

    private final List<AeriusGMLVersion> warningsIn;

    private TestFile(final AeriusGMLVersion... warningsIn) {
      this.warningsIn = Arrays.asList(warningsIn);
    }

    public String getFileName() {
      return name().toLowerCase() + ".gml";
    }

    public boolean expectWarning(final AeriusGMLVersion version) {
      return warningsIn.contains(version);
    }

  }

  private static final String TEST_FOLDER = "/complex/";
  private static final String CURRENT_VERSION = GMLWriter.LATEST_WRITER_VERSION.name().toLowerCase(Locale.ENGLISH);

  private final AeriusGMLVersion version;

  private static final EnumSet<Reason> VALID_WARNINGS = EnumSet.of(Reason.GML_VERSION_NOT_LATEST, Reason.GML_INLAND_WATERWAY_NOT_SET);

  private final TestFile testFile;

  /**
   * Initialize test with version and file to test.
   */
  public GMLOldVersionTest(final AeriusGMLVersion version, final TestFile file) {
    this.version = version;
    this.testFile = file;
  }

  @Parameters(name = "{0}: {1}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> files = new ArrayList<>();
    for (final AeriusGMLVersion version : AeriusGMLVersion.values()) {
      for (final TestFile file : TestFile.values()) {
        if (getFile(version.name().toLowerCase() + TEST_FOLDER, file.getFileName()) == null) {
          LOG.info("Ignoring GMLOldVersionTest for file '{}' for version {}. No old version of this file", file.getFileName(), version);
        } else {
          final Object[] f = new Object[2];
          f[0] = version;
          f[1] = file;
          files.add(f);
        }
      }
    }
    return files;
  }

  @Test
  public void testVersionGML() throws SQLException, IOException, AeriusException {
    final ImportResult oldResult = getImportResult(version.name().toLowerCase() + TEST_FOLDER, testFile);
    final ImportResult currentResult = getImportResult(CURRENT_VERSION + TEST_FOLDER, testFile);
    //if there is, there should be a current counter part.
    assertNotNull("Expected same file for current result " + testFile, currentResult);
    assertEquals("Number of sources", currentResult.getSources(0), oldResult.getSources(0));
    assertTrue("Expected no exceptions, got " + oldResult.getExceptions(), oldResult.getExceptions().isEmpty());
    if (testFile.expectWarning(version)) {
      assertFalse("Expected warnings, but got none", oldResult.getWarnings().isEmpty());
    } else {
      // warnings test on allowed
      for (final AeriusException warning : oldResult.getWarnings()) {
        if (!VALID_WARNINGS.contains(warning.getReason())) {
          fail("Not expected warning, got " + warning.getReason() + " " + warning.getMessage());
        }
      }
    }
    for (final EmissionSource source : oldResult.getSources(0)) {
      testSource(source, currentResult.getSources(0).get(oldResult.getSources(0).indexOf(source)));
    }
    //for good measure, try to export to a current-gen version
    final GMLWriter writer = new GMLWriter(RECEPTOR_GRID_SETTINGS);
    try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
      writer.writeEmissionSources(bos, oldResult.getSources(0),
          getMetaDataInput(oldResult));
    }
  }

  private MetaDataInput getMetaDataInput(final ImportResult oldResult) {
    final MetaDataInput metaDataInput = new MetaDataInput();
    metaDataInput.setScenarioMetaData(oldResult.getMetaData());
    metaDataInput.setYear(2020);
    metaDataInput.setName("name");
    metaDataInput.setVersion(CURRENT_VERSION);
    metaDataInput.setDatabaseVersion("version");
    return metaDataInput;
  }

  private void testSource(final EmissionSource oldSource, final EmissionSource currentSource) {
    assertEquals(getTestString("Label", currentSource.getLabel()), currentSource.getLabel(), oldSource.getLabel());
    assertEquals(getTestString("Sector", currentSource.getLabel()), currentSource.getSector(), oldSource.getSector());
    assertEquals(getTestString("Geometry", currentSource.getLabel()), currentSource.getGeometry(), oldSource.getGeometry());
    testEmissionValues(currentSource.getLabel(), oldSource, currentSource);
  }

  private void testEmissionValues(final String label, final EmissionSource oldValues, final EmissionSource currentValues) {
    assertEquals(getTestString("Class of EmissionValues", label), oldValues.getClass(), currentValues.getClass());
    assertEquals(getTestString("NH3 emission", label),
        currentValues.getEmission(new EmissionValueKey(2020, Substance.NH3)),
        oldValues.getEmission(new EmissionValueKey(2020, Substance.NH3)), 1E-3);
    assertEquals(getTestString("NOx emission", label),
        currentValues.getEmission(new EmissionValueKey(2020, Substance.NOX)),
        oldValues.getEmission(new EmissionValueKey(2020, Substance.NOX)), 1E-3);
  }

  private String getTestString(final String testPart, final String sourceLabel) {
    final StringBuilder builder = new StringBuilder(31);
    builder.append(testPart);
    builder.append(" for source ");
    builder.append(sourceLabel);
    return builder.toString();
  }

  private static File getFile(final String relativePath, final String fileName) throws FileNotFoundException {
    final URL url = GMLOldVersionTest.class.getResource(relativePath + fileName);
    File file = null;
    if (url != null) {
      file = new File(url.getFile());
      if (!file.exists()) {
        file = null;
      }
    }
    return file;
  }

  private ImportResult getImportResult(final String relativePath, final TestFile testFile)
      throws IOException, AeriusException, SQLException {
    ImportResult result = null;
    final String fileName = testFile.getFileName();
    final File file = getFile(relativePath, fileName);
    if (file == null) {
      LOG.info("Could not find file for test. relativePath: {}, filename: {}", relativePath, testFile);
    } else {
      final Importer importer = new Importer(getCalcPMF());
      try (final InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
        result = importer.convertInputStream2ImportResult(fileName, inputStream);
      }
    }
    return result;
  }
}
