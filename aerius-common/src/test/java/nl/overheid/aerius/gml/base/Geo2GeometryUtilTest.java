/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.math.BigInteger;
import java.util.Arrays;

import org.junit.Test;

import net.opengis.gml.v_3_2_1.DirectPositionType;
import net.opengis.gml.v_3_2_1.PointType;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.gml.base.geo.Geo2GeometryUtil;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link Geo2GeometryUtil}.
 */
public class Geo2GeometryUtilTest {

  private final Geo2GeometryUtil geo2GeometryUtil = new Geo2GeometryUtil(RDNew.SRID);

  @Test
  public void testPoint() throws AeriusException {
    final PointType pointType = createPoint();
    final WKTGeometry wktgeo = geo2GeometryUtil.fromXMLPoint(pointType);
    assertEquals("Not a point", TYPE.POINT, wktgeo.getType());
  }

  @Test(expected = AeriusException.class)
  public void testInvalidSRS() throws AeriusException {
    final PointType pointType = createPoint();
    pointType.setSrsName("unknown");
    pointType.setSrsDimension(BigInteger.TEN);
    try {
      geo2GeometryUtil.fromXMLPoint(pointType);
    } catch (final AeriusException e) {
      assertSame("Must be exception GML_SRS_NAME_UNSUPPORTED", Reason.GML_SRS_NAME_UNSUPPORTED, e.getReason());
      throw e;
    }
  }

  @Test
  public void testEmptySRS() throws AeriusException {
    final PointType pointType = createPoint();
    pointType.setSrsDimension(BigInteger.TEN);
    pointType.setSrsName(null);
    assertNotNull("Should just return the point no srs", geo2GeometryUtil.fromXMLPoint(pointType));
    pointType.setSrsName("");
    assertNotNull("Should just return the point on empty", geo2GeometryUtil.fromXMLPoint(pointType));
  }

  private PointType createPoint() {
    final PointType pointType = new PointType();
    final DirectPositionType value = new DirectPositionType();
    value.setValue(Arrays.asList(new Double[] {1.0, 1.0 }));
    pointType.setPos(value);
    return pointType;
  }
}
