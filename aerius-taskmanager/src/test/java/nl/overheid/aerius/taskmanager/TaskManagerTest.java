/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.codahale.metrics.MetricRegistry;

import nl.overheid.aerius.metrics.MetricFactory;
import nl.overheid.aerius.taskmanager.TaskScheduler.SchedulerFactory;
import nl.overheid.aerius.taskmanager.adaptor.AdaptorFactory;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.taskmanager.domain.configuration.TaskManagerConfiguration;
import nl.overheid.aerius.taskmanager.domain.configuration.TaskSchedulerConfiguration;

/**
 * Test class for {@link TaskManager}.
 */
public class TaskManagerTest {

  private static final String QUEUE_NAME = "test_queue";
  private static ExecutorService executor;

  @Before
  public void setUp() throws IOException, InterruptedException {
    executor = Executors.newCachedThreadPool();
    MetricFactory.init(new Properties(), "test");
  }

  @After
  public void after() throws InterruptedException {
    executor.shutdownNow();
    executor.awaitTermination(10, TimeUnit.MILLISECONDS);
    MetricFactory.getMetrics().removeMatching((name, metric) -> true);
  }

  @Test
  public void testAddScheduler() throws IOException, InterruptedException {
    final AdaptorFactory factory = new MockAdaptorFactory();
    final SchedulerFactory schedulerFactory = new FIFOTaskScheduler.FIFOSchedulerFactory();
    final TaskManager tm = new TaskManager(executor, factory, schedulerFactory);
    final TaskManagerConfiguration managerConfig = new TaskManagerConfiguration();
    final TaskSchedulerConfiguration schedulerConfiguration = new TaskSchedulerConfiguration(1, "description", managerConfig, WorkerType.TEST);
    assertTrue("TaskScheduler running", tm.addTaskScheduler(schedulerConfiguration));
    tm.removeTaskScheduler(WorkerType.TEST);
  }

  @Test
  public void testAddRemoveConsumer() throws IOException {
    final AdaptorFactory factory = new MockAdaptorFactory();
    final SchedulerFactory schedulerFactory = new FIFOTaskScheduler.FIFOSchedulerFactory();
    final TaskManager tm = new TaskManager(executor, factory, schedulerFactory);
    assertTrue("TaskConsumer running", tm.addTaskConsumer(WorkerType.TEST, QUEUE_NAME));
    tm.removeTaskConsumer(WorkerType.TEST, QUEUE_NAME);
  }

  @Test
  public void testMetricAvailable() throws IOException, InterruptedException {
    final AdaptorFactory factory = new MockAdaptorFactory();
    final SchedulerFactory schedulerFactory = new FIFOTaskScheduler.FIFOSchedulerFactory();
    final TaskManager tm = new TaskManager(executor, factory, schedulerFactory);

    final TaskManagerConfiguration managerConfig = new TaskManagerConfiguration();
    final TaskSchedulerConfiguration schedulerConfiguration = new TaskSchedulerConfiguration(1, "description", managerConfig, WorkerType.TEST);
    assertTrue("TaskScheduler running", tm.addTaskScheduler(schedulerConfiguration));
    tm.addTaskConsumer(WorkerType.TEST, QUEUE_NAME);

    final MetricRegistry metrics = MetricFactory.getMetrics();
    assertEquals("There should be 3 gauges in a scheduler.", 3, metrics.getGauges((name, metric) -> name.contains(WorkerType.TEST.name())).size());
  }
}
