/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.taskmanager.client.BrokerConnectionEnum;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.taskmanager.client.configuration.ConnectionConfiguration;
import nl.overheid.aerius.taskmanager.client.configuration.ConnectionConfigurationBean;
import nl.overheid.aerius.taskmanager.domain.configuration.TaskManagerConfiguration;
import nl.overheid.aerius.taskmanager.domain.configuration.TaskQueueConfiguration;
import nl.overheid.aerius.taskmanager.domain.configuration.TaskSchedulerConfiguration;

/**
 * Task manager database configuration repository.
 */
public final class ConfigurationRepository {

  private static final String GET_BROKER_CONFIGURATION =
      "SELECT key, value "
          + " FROM system.constants "
          + " WHERE key LIKE 'TASK_BROKER_%'";

  private static final String GET_TASK_SCHEDULER_CONFIGURATIONS =
      "SELECT task_scheduler_id, name, description"
          + " FROM system.task_schedulers";

  private static final String GET_TASK_CONFIGURATIONS_FOR_SCHEDULER =
      "SELECT name, description, priority, max_capacity_use "
          + " FROM system.task_scheduler_queues "
          + " WHERE task_scheduler_id = ? ";

  private ConfigurationRepository() {
    //don't allow to instantiate
  }

  /**
   * Get the full taskmanager configuration.
   * @param connection The connection to use for any required queries (will not be closed by the method)
   * @return The full taskmanager configuration (including schedulers/queues). Null when no manager found.
   * @throws SQLException When an exception occurred querying the database.
   */
  public static TaskManagerConfiguration getTaskManagerConfig(final Connection connection)
      throws SQLException {
    final TaskManagerConfiguration managerConfig = new TaskManagerConfiguration();
    managerConfig.setBrokerConfiguration(getBrokerConfiguration(connection));
    setTaskSchedulerConfigurationsForManager(connection, managerConfig);
    return managerConfig;
  }

  /**
   * Set the list of schedulers from the database that are assigned to a taskmanager.
   * @param connection The connection to use for any required queries (will not be closed by the method)
   * @param managerConfig The taskmanagerConfiguration to retrieve the scheduler configurations for.
   * @throws SQLException When an exception occurred querying the database.
   */
  public static void setTaskSchedulerConfigurationsForManager(final Connection connection,
      final TaskManagerConfiguration managerConfig) throws SQLException {
    try (final PreparedStatement taskSchedulerPS = connection
        .prepareStatement(GET_TASK_SCHEDULER_CONFIGURATIONS)) {
      final ResultSet rs = taskSchedulerPS.executeQuery();
      while (rs.next()) {
        final TaskSchedulerConfiguration schedulerConfig = new TaskSchedulerConfiguration(
            rs.getInt("task_scheduler_id"),
            rs.getString("description"),
            managerConfig,
            WorkerType.valueOf(rs.getString("name").toUpperCase()));

        schedulerConfig.setTaskManagerConfiguration(managerConfig);
        managerConfig.getTaskSchedulerConfigurations().add(schedulerConfig);
      }
    }
    for (final TaskSchedulerConfiguration tsc : managerConfig.getTaskSchedulerConfigurations()) {
      tsc.setTaskConfigurations(getTaskConfigurationsForScheduler(connection, tsc));
    }
  }

  /**
   * Get the broker configuration for a specific broker ID.
   * @param connection The connection to use for any required queries (will not be closed by the method)
   * @return The broker configuration that fits the broker ID. (Null if no broker with that ID)
   * @throws SQLException When an exception occurred querying the database.
   */
  public static ConnectionConfiguration getBrokerConfiguration(final Connection connection) throws SQLException {
    final ConnectionConfigurationBean brokerConfig = new ConnectionConfigurationBean();
    try (final PreparedStatement brokerPS = connection.prepareStatement(GET_BROKER_CONFIGURATION);
        final ResultSet rs = brokerPS.executeQuery()) {
      while (rs.next()) {
        final String value = rs.getString("value");
        switch (BrokerConnectionEnum.valueOf(rs.getString("key"))) {
        case TASK_BROKER_HOST:
          brokerConfig.setBrokerHost(value);
          break;
        case TASK_BROKER_PORT:
          brokerConfig.setBrokerPort(Integer.valueOf(value));
          break;
        case TASK_BROKER_USERNAME:
          brokerConfig.setBrokerUsername(value);
          break;
        case TASK_BROKER_PASSWORD:
          brokerConfig.setBrokerPassword(value);
          break;
        default:
        }
      }
    }
    return brokerConfig;
  }

  /**
   * Get a list of taskconfigurations from the database that are assigned to a scheduler.
   * @param connection The connection to use for any required queries (will not be closed by the method)
   * @param schedulerConfig The taskSchedulerConfiguration to retrieve the task configurations for.
   * @return A list of taskconfigurations configured for the scheduler.
   * @throws SQLException When an exception occurred querying the database.
   */
  public static List<TaskQueueConfiguration> getTaskConfigurationsForScheduler(final Connection connection,
      final TaskSchedulerConfiguration schedulerConfig) throws SQLException {
    final List<TaskQueueConfiguration> taskConfigs = new ArrayList<>();
    try (final PreparedStatement taskSchedulerPS = connection
        .prepareStatement(GET_TASK_CONFIGURATIONS_FOR_SCHEDULER)) {
      taskSchedulerPS.setInt(1, schedulerConfig.getId());
      final ResultSet rs = taskSchedulerPS.executeQuery();
      while (rs.next()) {
        final TaskQueueConfiguration taskConfig = new TaskQueueConfiguration(
            rs.getString("name"),
            rs.getString("description"),
            rs.getInt("priority"),
            Double.parseDouble(Float.toString(rs.getFloat("max_capacity_use"))),
            schedulerConfig);
        taskConfigs.add(taskConfig);
      }
    }
    return taskConfigs;
  }
}
