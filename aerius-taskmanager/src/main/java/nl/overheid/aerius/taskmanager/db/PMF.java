/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Persistence Manager Factory manages the database connection.
 */
public final class PMF {

  private PMF() {
  }

  /**
   * Returns a new connection to the AeriusDB database.
   * 
   * @param jdbcURL The (JDBC) url to the database.
   * @param dbUsername The username to use to connect to the database.
   * @param dbPassword The password to use to connect to the database.
   * @return a database connection
   * @throws SQLException 
   */
  public static Connection getConnection(String jdbcURL, String dbUsername, String dbPassword) throws SQLException {
    return DriverManager.getConnection(
        jdbcURL,
        dbUsername,
        dbPassword);
  }
}
