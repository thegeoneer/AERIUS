/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.exception;

import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Exception thrown when the same task was offered again to be sent to a worker.
 */
public class TaskAlreadySentException extends RuntimeException {

  private static final long serialVersionUID = 6483436844823532012L;

  private final WorkerType workerType;

  /**
   * TaskAlreadySentException.
   * @param workerType worker type this error occurred on
   */
  public TaskAlreadySentException(final WorkerType workerType) {
    this.workerType = workerType;
  }

  public WorkerType getWorkerType() {
    return workerType;
  }
}
