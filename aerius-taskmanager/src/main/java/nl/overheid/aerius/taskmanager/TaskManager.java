/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.metrics.MetricFactory;
import nl.overheid.aerius.taskmanager.TaskScheduler.SchedulerFactory;
import nl.overheid.aerius.taskmanager.adaptor.AdaptorFactory;
import nl.overheid.aerius.taskmanager.adaptor.WorkerProducer;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.taskmanager.domain.configuration.TaskSchedulerConfiguration;

/**
 * Main task manager class, manages all schedulers.
 */
class TaskManager {

  private static final Logger LOG = LoggerFactory.getLogger(TaskManager.class);

  private final ExecutorService executorService;
  private final Map<WorkerType, TaskDispatcher> dispatchers = new HashMap<>();
  private final Map<String, TaskConsumer> taskConsumers = new HashMap<>();
  private final SchedulerFactory schedulerFactory;
  private final AdaptorFactory factory;

  public TaskManager(final ExecutorService executorService, final AdaptorFactory factory, final SchedulerFactory schedulerFactory) {
    this.executorService = executorService;
    this.factory = factory;
    this.schedulerFactory = schedulerFactory;
  }

  /**
   * Add new task scheduler.
   *
   * @param scheduler scheduer configuration
   * @throws IOException
   * @throws InterruptedException
   */
  public boolean addTaskScheduler(final TaskSchedulerConfiguration scheduler) throws IOException, InterruptedException {
    // Set up scheduler with worker pool
    final TaskScheduler taskScheduler = schedulerFactory.createScheduler(scheduler);
    final WorkerType workerType = scheduler.getWorkerType();
    final WorkerProducer workerProducer = factory.createWorkerProducer(workerType);
    final WorkerPool workerPool = new WorkerPool(workerType, workerProducer, taskScheduler);
    workerProducer.start(executorService, workerPool);

    // Set up metrics
    WorkerPoolMetrics.setupMetrics(MetricFactory.getMetrics(), workerPool, workerType.name());

    // Set up dispatcher
    final TaskDispatcher dispatcher = new TaskDispatcher(workerType, taskScheduler, workerPool);
    executorService.execute(dispatcher);
    dispatchers.put(workerType, dispatcher);

    LOG.info("Started taskscheduler for {} of type {}", workerType, taskScheduler.getClass().getSimpleName());
    Thread.sleep(TimeUnit.SECONDS.toMillis(1)); // just wait a little second to make sure the process is actually running.
    return dispatcher.isRunning();
  }

  /**
   * Removed the scheduler for the given worker type.
   *
   * @param workerType worker type
   */
  public void removeTaskScheduler(final WorkerType workerType) {
    final TaskDispatcher dispatcher = dispatchers.remove(workerType);

    if (dispatcher != null) {
      dispatcher.shutdown();
      LOG.info("Stopped and removed taskscheduler for {}", workerType);
    }
  }

  /**
   * Adds a task consumer.
   *
   * @param workerType worker type to add consumer too
   * @param queueName queue name of the task consumer
   * @throws IOException
   */
  public boolean addTaskConsumer(final WorkerType workerType, final String queueName) throws IOException {
    final TaskConsumer taskConsumer = new TaskConsumer(workerType, queueName, dispatchers.get(workerType), factory);
    executorService.execute(taskConsumer);
    taskConsumers.put(workerType.getTaskQueueName(queueName), taskConsumer);
    LOG.info("Started task queue {} of worker type {}", queueName, workerType);
    return taskConsumer.isRunning();
  }

  /**
   * Removes a task worker with the given worker type and queue name.
   *
   * @param workerType worker type to remove the consumer from
   * @param queueName queue name of the task consumer
   */
  public void removeTaskConsumer(final WorkerType workerType, final String queueName) {
    final TaskConsumer taskConsumer = taskConsumers.get(workerType.getTaskQueueName(queueName));
    if (taskConsumer != null) {
      taskConsumer.shutdown();
      LOG.info("Stopped and removed task queue {} of worker type {}", queueName, workerType);
    }
  }

  /**
   * Shuts down all schedulers and consumers.
   */
  public void shutdown() {
    for (final Entry<String, TaskConsumer> taskConsumer : taskConsumers.entrySet()) {
      taskConsumer.getValue().shutdown();
    }
    for (final Entry<WorkerType, TaskDispatcher> dispatcher : dispatchers.entrySet()) {
      dispatcher.getValue().shutdown();
    }
  }
}
