/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.mq;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;

import nl.overheid.aerius.taskmanager.domain.Message;

/**
 * RabbitMQ Message object.
 */
public class RabbitMQMessage extends Message<RabbitMQMessageMetaData> {
  private final Channel channel;
  private final BasicProperties properties;
  private final byte[] body;

  /**
   * Constructor
   * @param channel
   * @param properties Basicproperties from the original message.
   * @param body The body of the original message.
   */
  public RabbitMQMessage(final Channel channel, final long deliveryTag, final BasicProperties properties, final byte[] body) {
    super(new RabbitMQMessageMetaData(deliveryTag));

    this.channel = channel;
    this.properties = properties;
    this.body = body == null ? new byte[0] : body.clone();
  }

  @Override
  public String getMessageId() {
    return properties.getMessageId();
  }

  public BasicProperties getProperties() {
    return properties;
  }

  public byte[] getBody() {
    return body.clone();
  }

  public Channel getChannel() {
    return channel;
  }
}
