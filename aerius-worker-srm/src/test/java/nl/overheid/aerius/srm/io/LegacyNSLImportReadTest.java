/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link LegacyNSLImportReader}.
 */
public class LegacyNSLImportReadTest extends BaseDBTest {

  @Test
  public void testImport() throws Exception {
    final String filename = "srm2_roads_example.csv";
    final File file = new File(getClass().getResource(filename).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportResult result = new ImportResult();
      new LegacyNSLImportReader().read(
          filename, inputStream, SectorRepository.getSectorCategories(getCalcPMF(), LocaleUtils.getDefaultLocale()), null, result);

      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
      assertEquals("Count nr. of networks", 1, result.getSources(0).size());
      assertTrue("It should be a network object", result.getSources(0).get(0) instanceof SRM2NetworkEmissionSource);
      assertEquals("Count nr. of roads", 160, (((SRM2NetworkEmissionSource) result.getSources(0).get(0))).getEmissionSources().size());
    }
  }
}
