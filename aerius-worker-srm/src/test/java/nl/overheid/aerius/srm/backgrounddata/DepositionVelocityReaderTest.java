/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Test class for {@link DepositionVelocityReader}.
 */
public class DepositionVelocityReaderTest {
  @Test
  public void testReadFile() throws IOException {
    Map<Substance, DepositionVelocityMap> data = null;

    try (final InputStream is = DepositionVelocityReader.class.getResourceAsStream("vd_zoomlevel-2_20150522.csv")) {
      data = DepositionVelocityReader.read(is, SRMConstants.HEX_HOR, SRMConstants.getReceptorUtil());
    }

    assertNotNull("Map should not be null.", data);
    assertEquals("Unexpected number of substances.", 2, data.size());

    final DepositionVelocityMap depositionVelocitiesNO2 = data.get(Substance.NO2);

    assertNotNull("NOX map should not be null.", depositionVelocitiesNO2);
    assertEquals("Invalid number of receptors.", 1694539, depositionVelocitiesNO2.size());

    final Double depositionVelocityNO2 = depositionVelocitiesNO2.getDepositionVelocityById(801827);
    assertNotNull("NOX value should not be null.", depositionVelocityNO2);
    assertEquals("Invalid NOX value.", 0.0008563, depositionVelocityNO2, 0);

    final DepositionVelocityMap depositionVelocitiesNH3 = data.get(Substance.NH3);

    assertNotNull("NH3 map should not be null.", depositionVelocitiesNO2);
    assertEquals("Invalid number of receptors.", 1694539, depositionVelocitiesNH3.size());

    final Double depositionVelocityNH3 = depositionVelocitiesNH3.getDepositionVelocityById(801827);
    assertNotNull("NH3 value should not be null.", depositionVelocityNH3);
    assertEquals("Invalid NH3 value.", 0.0066601, depositionVelocityNH3, 0);
  }
}
