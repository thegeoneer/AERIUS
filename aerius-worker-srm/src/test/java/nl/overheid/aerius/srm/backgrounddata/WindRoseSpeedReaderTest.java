/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import nl.overheid.aerius.io.LineReaderResult;

/**
 * Test class for {@link WindRoseSpeedReader}.
 */
public class WindRoseSpeedReaderTest {

  private static final String PRE_SRM_VERSION = "1.901";
  private static final String WINDROSE_TXT = "1.603/windrose.txt";

  @Test
  public void testReadFile() throws IOException {
    final WindRoseSpeedReader reader = new WindRoseSpeedReader(PRE_SRM_VERSION);
    final LineReaderResult<WindRoseSpeed> results = readResults(reader, WINDROSE_TXT);
    assertEquals("# rows", 43570, results.getObjects().size());
    final int row = 0;
    assertEquals("Windfactor sector 35", 0.01625, results.getObjects().get(row).getWindFactor(35), 0.0001);
    assertEquals("Avg windspeed sector 35", 3.014, results.getObjects().get(row).getAvgWindSpeed(35), 0.0001);
    assertEquals("X", 13500.0, results.getObjects().get(row).getX(), 0.0);
    assertEquals("Y", 370500.0, results.getObjects().get(row).getY(), 0.0);
  }

  private LineReaderResult<WindRoseSpeed> readResults(final WindRoseSpeedReader reader, final String filename) throws IOException {
    final LineReaderResult<WindRoseSpeed> results;
    try (final InputStream is = WindRoseSpeedReaderTest.class.getResourceAsStream(filename)) {
      results = reader.readObjects(is);
    }
    return results;
  }
}
