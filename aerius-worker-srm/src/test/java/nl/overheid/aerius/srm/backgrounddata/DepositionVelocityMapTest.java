/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Test class for {@link DepositionVelocityMap}.
 */
@RunWith(Parameterized.class)
public class DepositionVelocityMapTest {
  private static Map<Substance, DepositionVelocityMap> depositionVelocityMaps;

  private final Substance substance;
  private final int receptorId;
  private final double expectedValue;

  public DepositionVelocityMapTest(final Substance substance, final int receptorId, final double expectedValue) {
    this.substance = substance;
    this.receptorId = receptorId;
    this.expectedValue = expectedValue;
  }

  @BeforeClass
  public static void init() throws IOException {
    try (final InputStream is = DepositionVelocityReader.class.getResourceAsStream("vd_zoomlevel-2_20150522.csv")) {
      depositionVelocityMaps = DepositionVelocityReader.read(is, SRMConstants.HEX_HOR, SRMConstants.getReceptorUtil());
    }
  }

  @Parameters(name = "{0} - {1}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        {Substance.NO2, 1135584, 0.0007460},
        {Substance.NO2, 1135585, 0.0007446},
        {Substance.NO2, 7039267, 0.00119905},
        {Substance.NO2, 2200483, 0.00109615},
        {Substance.NH3, 1135584, 0.0042159},
        {Substance.NH3, 7039267, 0.00808455},
        {Substance.NH3, 2200483, 0.01512405}
    });
  }

  @Test
  public void testDepositionVelocity() throws IOException {
    final DepositionVelocityMap depositionVelocityMap = depositionVelocityMaps.get(substance);
    assertNotNull("No deposition velocity map found for substance.", depositionVelocityMap);

    final Double depositionVelocity = depositionVelocityMap.getDepositionVelocityById(receptorId);
    assertNotNull("No deposition velocity found for receptor.", depositionVelocity);
    assertEquals("Invalid deposition velocity.", expectedValue, depositionVelocity, 1E-8);
  }
}
