This tool generates the preprocessed pre-srm files for the AERIUS srm2 worker.
pre-srm is used to generate wind (air), O3 and NH3 background values per
1km area for a given coordinate set.

This tool is written in free pascal and can be build with the open source pascal
ide Lazarus.

To build the tool and interface files for pre-srm are needed. These are included
in this project because the original files can't be used directly as they are
written for delphi and not 100% compatible with free pascal. With a new version
of pre-srm it needs to be checked if there are any changes in the interface.

To run the tool the pre_srm.dll is needed. Place the latest pre_srm.dll in the
directoy of the srm2_preprocess.exe and adapt the AERIUS.ini file to point to
the correct location of the pre-srm data directory. To generate the files start
with the inifile as argument:
 srm2_preprocess.exe [iniFile]
