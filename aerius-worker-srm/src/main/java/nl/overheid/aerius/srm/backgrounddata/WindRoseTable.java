/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.index.strtree.GeometryItemDistance;
import com.vividsolutions.jts.index.strtree.STRtree;

/**
 * Table to fast search wind rose information.
 */
public class WindRoseTable<T extends Point> {
  private static final GeometryItemDistance ITEM_DIST = new GeometryItemDistance();

  private final GeometryFactory geometryFactory;
  private final STRtree tree = new STRtree();

  public WindRoseTable(final int srid) {
    geometryFactory = new GeometryFactory(new PrecisionModel(), srid);
  }

  public void addAll(final List<T> windroses) {
    for (final T windrose : windroses) {
      tree.insert(windrose.getEnvelopeInternal(), windrose);
    }
    tree.build();
  }

  @SuppressWarnings("unchecked")
  public T get(final int x, final int y) {
    final Point point = geometryFactory.createPoint(new Coordinate(x, y));
    return (T) tree.nearestNeighbour(point.getEnvelopeInternal(), point, ITEM_DIST);
  }

  public int size() {
    return tree.size();
  }

  public boolean isEmpty() {
    return tree.isEmpty();
  }
}
