/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSGProxy;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;

/**
 * Util class with constants.
 */
public final class SRMConstants {

  // ==================================================================================================================
  // PAS Profile Constants
  private static final class PasSRM2CalculationOptions extends SRM2CalculationOptions {
    private static final long serialVersionUID = 4006174066839722962L;

    private static final String PAS_PRESRM_VERSION = "1.901";
    /**
     * Max distance between source and receptor to include in calculations.
     * The value of 5000 is used in the PAS/WebNB as default.
     */
    private static final int PAS_MAX_SOURCE_DISTANCE = 5000;

    /**
     * For the PAS only prognose meteo data is used, also for historical years.
     */
    private static final int PAS_ALL_PROGNOSE_NO_YEAR = -1;

    PasSRM2CalculationOptions() {
      super(Profile.PAS, PAS_PRESRM_VERSION, PAS_MAX_SOURCE_DISTANCE, PAS_ALL_PROGNOSE_NO_YEAR,
          new HashSet<>(Arrays.asList(Option.CALCULATE_SUBRECEPTORS, Option.CALCULATE_DEPOSITION)));
    }
  }

  public static final SRM2CalculationOptions PAS_SRM_CALCULATION_OPTIONS = new PasSRM2CalculationOptions();

  // ==================================================================================================================

  /**
   * Molar mass for NO2.
   */
  public static final double MOLAR_MASS_NO2 = 46.0056;
  /**
   * Molar mass for NH3.
   */
  public static final double MOLAR_MASS_NH3 = 17.03;

  /**
   * Maximum length of a single road segment in meters.
   */
  public static final double MAX_SEGMENT_LENGTH = 2.0;

  /**
   * Number of rings to use for a sub receptor calculation.
   */
  public static final int SUB_RECEPTOR_RINGS = 11;

  /**
   * The number of bytes in a double.
   */
  public static final long DOUBLE_BYTES = 8;

  /**
   * The number of doubles for a single source.
   */
  public static final int SOURCE_DOUBLES = 8;

  /**
   * The number of doubles for a single receptor.
   */
  public static final int RECEPTOR_POS_DOUBLES = 2;

  /**
   * The number of double for a single receptor deposition velocity.
   */

  public static final int RECEPTOR_DEPOSITION_VELOCITY_DOUBLES = 1;

  /**
   * The number of wind sectors.
   */
  public static final int WIND_SECTORS = 36;

  /**
   * The number of doubles for the wind sector data. Per wind sector the wind factor and average wind speed.
   */
  public static final int RECEPTOR_WIND_SECTOR_DOUBLES = WIND_SECTORS * 2;

  /**
   * O3 per wind sector, only used for NO2 calculations.
   */
  public static final int RECEPTOR_WIND_SECTOR_O3_DOUBLES = WIND_SECTORS;

  /**
   * To convert from g/m/s. to μg/m/s.
   */
  public static final int EMISSION_CONVERSION = 1000 * 1000;

  /**
   * This is specific for the Dutch hexagon grid, but SRM2 is only supported for the dutch situation.
   */
  public static final int SRID = RDNew.SRID;

  /**
   * The hexagon zoom level 1 for the a 100 * 100 square hexagon.
   *
   * This is specific for the Dutch hexagon grid, but SRM2 is only supported for the dutch situation.
   */
  public static final HexagonZoomLevel ZOOM_LEVEL_1 = new HexagonZoomLevel(1, 10000);

  /**
   * Total number of hexagons in a row.
   *
   * This is specific for the Dutch hexagon grid, but SRM2 is only supported for the dutch situation.
   */
  public static final int HEX_HOR = 1529;

  /**
   * Lowest X-coordinate for hexagon mapping of the Netherlands.
   */
  private static final double X_MIN = 3604;
  /**
   * Highest X-coordinate for hexagon mapping of the Netherlands.
   */
  private static final double X_MAX = 287959;

  /**
   * Lowest Y-coordinate for hexagon mapping of the Netherlands.
   */
  private static final double Y_MIN = 296800;
  /**
   * Highest Y-coordinate for hexagon mapping of the Netherlands.
   */
  private static final double Y_MAX = 629300;

  // Constant to convert from microgram/m^2/s to gram/Ha/year
  public static final double DEPOSITION_UNIT_CONVERSION = SharedConstants.NUMBER_SECONDS_PER_YEAR * SharedConstants.M2_TO_HA
      / SharedConstants.MICROGRAM_TO_GRAM;

  //  Constants to convert
  public static final double DEPOSITION_CONVERSION_NO2 = DEPOSITION_UNIT_CONVERSION / SRMConstants.MOLAR_MASS_NO2;
  public static final double DEPOSITION_CONVERSION_NH3 = DEPOSITION_UNIT_CONVERSION / SRMConstants.MOLAR_MASS_NH3;


  private SRMConstants() {
  }

  /**
   *
   * @return
   */
  public static ReceptorUtil getReceptorUtil() {
    final ArrayList<HexagonZoomLevel> hexagonZoomLevels = new ArrayList<>();
    hexagonZoomLevels.add(ZOOM_LEVEL_1);
    return new ReceptorUtil(new ReceptorGridSettings(new BBox(X_MIN, Y_MIN, X_MAX, Y_MAX), EPSGProxy.getEPSG(SRID), HEX_HOR, hexagonZoomLevels));
  }
}
