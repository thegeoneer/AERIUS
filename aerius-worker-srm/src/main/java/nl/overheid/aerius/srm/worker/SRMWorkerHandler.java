/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.srm.backgrounddata.PreSRMDataBuilder;
import nl.overheid.aerius.srm.config.SRMWorkerConfiguration;
import nl.overheid.aerius.srm2.domain.SRM2InputData;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.worker.WorkerHandlerImpl;

/**
 * WorkHandler for SRM calculations that directs the calculation to the profile specific implementation.
 */
class SRMWorkerHandler extends WorkerHandlerImpl<SRM2InputData, CalculationResult> {
  private static final Logger LOGGER = LoggerFactory.getLogger(SRMWorkerHandler.class);

  private final PASWorkHandler pasWorkerHandler;

  SRMWorkerHandler(final SRMWorkerConfiguration config) throws IOException {
    final PreSRMDataBuilder builder = new PreSRMDataBuilder();

    if (config.hasProfileConfig(Profile.PAS)) {
      pasWorkerHandler = new PASWorkHandler(config, builder.build(config.getProfileConfiguration(Profile.PAS)));
    } else {
      pasWorkerHandler = null;
    }
  }

  @Override
  public CalculationResult run(final SRM2InputData input, final WorkerIntermediateResultSender resultSender, final String correlationId)
      throws Exception {
    final Profile profile = input.getProfile();

    if (profile == Profile.PAS && pasWorkerHandler != null) {
      return pasWorkerHandler.run(input, resultSender, correlationId);
    } else {
      LOGGER.error("Unsupported or unconfigured profile passed to srm2 worker: {}", profile);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }
}
