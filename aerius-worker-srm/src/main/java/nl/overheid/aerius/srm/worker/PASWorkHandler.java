/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.opencl.DefaultContext;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm.config.SRMWorkerConfiguration;
import nl.overheid.aerius.srm2.calculation.SRM2Calculator;
import nl.overheid.aerius.srm2.domain.SRM2InputData;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.worker.WorkerHandlerImpl;

/**
 * SRM WorkHandler for the {@link Profile#PAS}.
 */
class PASWorkHandler extends WorkerHandlerImpl<SRM2InputData, CalculationResult> {
  private static final Logger LOGGER = LoggerFactory.getLogger(PASWorkHandler.class);

  private final SRM2Calculator srm2Calculator;

  public PASWorkHandler(final SRMWorkerConfiguration config, final PreSRMData preSRMData) {
    srm2Calculator = new SRM2Calculator(new DefaultContext(true, config.isForceCpu()), preSRMData, config.getConnectionTimeout(),
        SRMConstants.PAS_SRM_CALCULATION_OPTIONS);
  }

  @Override
  public CalculationResult run(final SRM2InputData input, final WorkerIntermediateResultSender resultSender, final String correlationId)
      throws AeriusException {
    LOGGER.debug("Start calculation run for profile WetNB");
    final Collection<SRM2RoadSegment> sources = input.getEmissionSources();
    final Collection<AeriusPoint> receptors = input.getReceptors();
    final List<Substance> substances = input.getSubstances();
    final int year = input.getYear();
    final EnumSet<EmissionResultKey> erks = input.getEmissionResultKeys();

    return new CalculationResult(srm2Calculator.calculate(year, substances, erks, sources, receptors));
  }
}
