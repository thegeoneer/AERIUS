/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.List;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;


/**
 * Reader for pre-srm pre-processed O3 background per wind sector and NH3 background.
 */
class WindRoseConcentrationReader extends PreSrmXYReader<WindRoseConcentration> {
  // Separate on white space characters
  private static final String CO3 = "CO3";
  // Offset where windrose O3 data starts. Skip x-y coordinate data.
  private static final int CO3_OFFSET = 2;
  private static final int BACKGROUND_NH3_COLUMN_INDEX = CO3_OFFSET + SRMConstants.WIND_SECTORS;
  private static final String BACKGROUND_NH3_COLUMN = "Con_NH3";

  public WindRoseConcentrationReader(final String preSrmVersion, final int year) {
    super(preSrmVersion, YearPattern.PROGNOSE, year);
  }

  @Override
  protected WindRoseConcentration parseLine(final String line, final List<AeriusException> warnings) {
    final WindRoseConcentration windrose =
        new WindRoseConcentration(GeometryUtil.getCoordinateSequence(readX(), readY()), GeometryUtil.GEOMETRY_FACTORY);

    for (int i = 0; i < SRMConstants.WIND_SECTORS; i++) {
      windrose.setO3(i, getDouble(CO3 + i, CO3_OFFSET + i));
    }
    windrose.setBackgroundNH3(getDouble(BACKGROUND_NH3_COLUMN, BACKGROUND_NH3_COLUMN_INDEX));

    return windrose;
  }
}
