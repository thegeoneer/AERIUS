/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.domain;

import java.io.Serializable;
import java.util.Set;

import nl.overheid.aerius.shared.domain.calculation.Profile;

/**
 * SRM2 calculation options that determine how a SRM2 calculation should be performed.
 */
public class SRM2CalculationOptions implements Serializable {

  private static final long serialVersionUID = 7449216166416741839L;

  public enum Option {
    /**
     * When set enable sub receptor calculation for receptors close to a source point.
     */
    CALCULATE_SUBRECEPTORS,
    CALCULATE_DEPOSITION,
    CALCULATE_SRM1,
    METEO_ALL_PROGNOSE,
  }

  private final Profile profile;
  private final String preSRMVersion;
  /**
   * The distance sources should be present to perform a calculation.
   * If any source is present in the given distance from the given point the point will be calculated.
   * If no sources present the point result will be set to 0.
   */
  private final int maxSourceDistanceMeter;
  private final Set<Option> options;
  private final int prognoseYear;

  protected SRM2CalculationOptions(final Profile profile, final String preSRMVersion, final int maxSourceDistanceMeter, final int prognoseYear,
      final Set<Option> options) {
    this.profile = profile;
    this.preSRMVersion = preSRMVersion;
    this.maxSourceDistanceMeter = maxSourceDistanceMeter;
    this.prognoseYear = prognoseYear;
    this.options = options;
  }

  public Profile getProfile() {
    return profile;
  }

  public String getPreSRMVersion() {
    return preSRMVersion;
  }

  public int getMaxSourceDistanceMeter() {
    return maxSourceDistanceMeter;
  }

  public boolean isCalculateDeposition() {
    return options.contains(Option.CALCULATE_DEPOSITION);
  }

  public boolean isSubReceptorCalculation() {
    return options.contains(Option.CALCULATE_SUBRECEPTORS);
  }

  public boolean isCalculateSRM1() {
    return options.contains(Option.CALCULATE_SRM1);
  }

  public boolean isMeteoAllPrognose() {
    return options.contains(Option.METEO_ALL_PROGNOSE);
  }

  public int getPrognoseYear() {
    return prognoseYear;
  }

}

