/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadElevation;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSourceLinearReference;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Converter class to convert EmissionSource related objects to SRM2 road segment objects.
 */
public class EmissionSourceSRM2Converter {
  private static final double MIN_DISTANCE = 0.001;

  private static final Logger LOG = LoggerFactory.getLogger(EmissionSourceSRM2Converter.class);

  /**
   * Converts {@link EmissionSource}s to {@link EngineSource}s.
   * An {@link EmissionSource} is a source with a number of emissions: one emission per year and substance.
   * An {@link EngineSource} is a source with only one emission, agnostic of year and substance.
   * Therefore the {@link EngineSource} objects are mapped to year and substance, for each year and substance that their corresponding
   * {@link EmissionSource} has an emission. Sources with an emission <= 0 are not stored, because calculating them would have no effect.
   *
   * @param keys The years and substances to convert for.
   * @param emissionSources The {@link EmissionSource}s to convert.
   * @return The {@link EngineSource}s mapped to year and substance.
   * @throws AeriusException
   */

  public List<SRM2RoadSegment> convert(final List<EmissionSource> emissionSources, final List<EmissionValueKey> keys) throws AeriusException {
    final List<SRM2RoadSegment> engineSources = new ArrayList<>();

    if (emissionSources != null) {
      for (final EmissionSource emissionSource : emissionSources) {
        engineSources.addAll(convert(emissionSource, keys));
      }
    }
    return engineSources;
  }

  public List<SRM2RoadSegment> convert(final EmissionSource emissionSource, final List<EmissionValueKey> keys) throws AeriusException {
    final List<SRM2RoadSegment> converted = new ArrayList<>();

    if (emissionSource instanceof SRM2NetworkEmissionSource) {
      for (final EmissionSource srm2Source : ((SRM2NetworkEmissionSource) emissionSource).getEmissionSources()) {
        converted.addAll(convert(srm2Source.getGeometry(), srm2Source, keys));
      }
    } else {
      converted.addAll(convert(emissionSource.getGeometry(), emissionSource, keys));
    }

    return converted;
  }

  /**
   * Copies the {@link SRM2EmissionSource} object to {@link SRM2RoadSegment} objects for the given year emission. It
   * splits lines into segments with only start and an end point.
   *
   * @param geometry
   *          road geometry
   * @param source
   *          {@link SRM2EmissionSource} object to convert to {@link SRM2RoadSegment} objects
   * @param keys
   *          keys to get emission for
   * @param geometry
   * @return list of road segments
   * @throws AeriusException Thrown if the geometry is not a line.
   */
  public final List<SRM2RoadSegment> convert(final WKTGeometry geometry, final EmissionSource source, final List<EmissionValueKey> keys)
      throws AeriusException {
    if (geometry.getType() != TYPE.LINE) {
      throw new AeriusException(Reason.ROAD_GEOMETRY_NOT_ALLOWED);
    }

    final List<SRM2RoadSegment> segments = new ArrayList<>();

    final Geometry line = GeometryUtil.getGeometry(geometry.getWKT());

    final Node startNode = calculateSegmentNodes(line.getCoordinates());
    calculateDynamicSegments(source, startNode);
    addSegments(source, keys, segments, startNode);

    return segments;
  }

  private Node calculateSegmentNodes(final Coordinate[] coordinates) {
    Node startNode = null;
    Node previousNode = null;

    for (int i = 0; i < coordinates.length; i++) {
      final Node node = new Node(coordinates[i].x, coordinates[i].y);

      if (startNode == null) {
        startNode = node;
      } else {
        final Segment segment = new Segment(previousNode, node);
        previousNode.nextSegment = segment;
      }

      previousNode = node;
    }
    return startNode;
  }

  private void calculateDynamicSegments(final EmissionSource es, final Node startNode) {
    if (es instanceof SRM2EmissionSource) {
      final List<SRM2EmissionSourceLinearReference> dynamicSegments = ((SRM2EmissionSource) es).getDynamicSegments();
      if (!dynamicSegments.isEmpty()) {
        final double totalLength = startNode.totalLength();

        for (final SRM2EmissionSourceLinearReference dynamicSegment : dynamicSegments) {
          final Node segmentStart = startNode.addNode(dynamicSegment.getStart() * totalLength);
          final Node segmentEnd = startNode.addNode(dynamicSegment.getEnd() * totalLength);

          for (Node node = segmentStart; node != segmentEnd; node = node.next()) {
            node.nextSegment.applyReference(dynamicSegment);
          }
        }
      }
    }
  }

  private void addSegments(final EmissionSource source, final List<EmissionValueKey> keys, final List<SRM2RoadSegment> segments,
      final Node startNode) {
    if (source instanceof SRM2EmissionSource) {
      addSRM2Segments((SRM2EmissionSource) source, keys, segments, startNode);
    } else {
      addGenericSegments(source, keys, segments, startNode);
    }
  }

  private void addSRM2Segments(final SRM2EmissionSource source, final List<EmissionValueKey> keys, final List<SRM2RoadSegment> segments,
      final Node startNode) {
    if (source.getEmissionSubSources().isEmpty()) {
      LOG.trace("SRM2 source has no vehicles and is ignored: source:{} ", source);
    } else if (source.hasAnyEmissions()) {
      final Map<Substance, Double> emissionPerMeter = getEmissionPerMeter(source, keys);
      for (Segment segment = startNode.nextSegment; segment != null; segment = segment.next()) {
        final SRM2RoadSegment roadSegment = toRoadSegment(source, segment, emissionPerMeter);

        if (roadSegment != null) {
          if (roadSegment.getLineLength() <= 0) {
            LOG.trace("SRM2 segment line length is 0, coordinates: ({} {}), ({} {}), segment ignored ignored: {} ",
                roadSegment.getStartX(), roadSegment.getStartY(), roadSegment.getEndX(), roadSegment.getEndY(), source);
          } else {
            segments.add(roadSegment);
          }
        }
      }
    } else {
      LOG.trace("SRM2 source has no emissions and is ignored: source:{} ", source);
    }
  }

  private void addGenericSegments(final EmissionSource source, final List<EmissionValueKey> keys, final List<SRM2RoadSegment> segments,
      final Node startNode) {
    final boolean isFreeway  = source.getSector().getSectorId() == RoadType.FREEWAY.getSectorId();
    final Map<Substance, Double> emissionPerMeter = getEmissionPerMeter(source, keys);
    for (Segment segment = startNode.nextSegment; segment != null; segment = segment.next()) {
      final SRM2RoadSegment roadSegment = createObject(isFreeway, RoadElevation.NORMAL, 0, null, null);
      roadSegment.setStartX(segment.start.positionX);
      roadSegment.setStartY(segment.start.positionY);
      roadSegment.setEndX(segment.end.positionX);
      roadSegment.setEndY(segment.end.positionY);
      roadSegment.setSRM2(true);

      for (final Entry<Substance, Double> entry : emissionPerMeter.entrySet()) {
        roadSegment.setEmission(entry.getKey(), entry.getValue());
      }
      segments.add(roadSegment);
    }
  }

  /**
   * Gets the emissions per meter.
   * @param source
   * @param keys
   * @return
   */
  private Map<Substance, Double> getEmissionPerMeter(final EmissionSource source, final List<EmissionValueKey> keys) {
    final Map<Substance, Double> emissions = new HashMap<>();
    final boolean emissionPerUnit = source.isEmissionPerUnit();
    final double measure;

    if (emissionPerUnit) {
      // If emissionPerUnit set unit to false the returned emission is then by unit (=meter).
      source.setEmissionPerUnit(false);
      measure = 1.0;
    } else {
      // If not per meter divide the emission by the length to get the meter value.
      measure = source.getGeometry().getMeasure();
    }
    for (final EmissionValueKey key : keys) {
      final double emission = source.getEmission(key);

      if (emission > 0 && measure > 0) {
        emissions.put(key.getSubstance(), emission / (Substance.EMISSION_IN_G_PER_S_FACTOR * measure));
        LOG.trace("key:{}, emission: {}, measure:{}, new:{}", key.getSubstance(), emission, measure, emissions.get(key.getSubstance()));
      }
    }
    source.setEmissionPerUnit(emissionPerUnit);
    return emissions;
  }

  /**
   * Converts a segment to a SRM2RoadSegment.
   *
   * @param source
   *          The original source.
   * @param segment
   *          The segment of the source.
   * @param emissions
   *          The emissions per meter.
   * @return A new SRM2RoadSegment, or null if it has no emissions and can be omitted.
   */
  private SRM2RoadSegment toRoadSegment(final SRM2EmissionSource source, final Segment segment, final Map<Substance, Double> emissions) {
    RoadElevation elevation = null;
    Integer elevationHeight = null;
    Integer maxRoadSpeed = null;
    Boolean strictEnforcement = null;
    Boolean freeway = null;
    Double tunnelFactor = null;
    RoadSideBarrier barrierLeft = null;
    RoadSideBarrier barrierRight = null;

    for (final SRM2EmissionSourceLinearReference segmentOverride : segment.appliedReferences) {
      elevation = verifyOverride(segment, elevation, segmentOverride.getElevation());
      elevationHeight = verifyOverride(segment, elevationHeight, segmentOverride.getElevationHeight());
      maxRoadSpeed = verifyOverride(segment, maxRoadSpeed, segmentOverride.getMaxRoadSpeed());
      strictEnforcement = verifyOverride(segment, strictEnforcement, segmentOverride.getStrictEnforcement());
      freeway = verifyOverride(segment, freeway, segmentOverride.getFreeway());
      tunnelFactor = verifyOverride(segment, tunnelFactor, segmentOverride.getTunnelFactor());
      barrierLeft = verifyOverride(segment, barrierLeft, segmentOverride.getBarrierLeft());
      barrierRight = verifyOverride(segment, barrierRight, segmentOverride.getBarrierRight());
    }

    final SRM2RoadSegment roadSegment;

    if (source.getTunnelFactor() == 0 || (tunnelFactor != null && tunnelFactor == 0)) {
      LOG.trace("SRM2 source tunnel factor 0 and therefor is ignored: {}", source.toString());
      roadSegment = null;
    } else {
      final boolean segmentIsFreeway = freeway == null ? source.isFreeway() : freeway;
      final RoadElevation segmentElevation = elevation == null ? source.getElevation() : elevation;
      final int segmentElevationHeight = elevationHeight == null ? source.getElevationHeight() : elevationHeight;
      final RoadSideBarrier segmentBarrierLeft = barrierLeft == null ? source.getBarrierLeft() : barrierLeft;
      final RoadSideBarrier segmentBarrierRight = barrierRight == null ? source.getBarrierRight() : barrierRight;

      roadSegment = createObject(segmentIsFreeway, segmentElevation, segmentElevationHeight, segmentBarrierLeft, segmentBarrierRight);
      roadSegment.setSegmentId(source.getSegmentId());
      roadSegment.setStartX(segment.start.positionX);
      roadSegment.setStartY(segment.start.positionY);
      roadSegment.setEndX(segment.end.positionX);
      roadSegment.setEndY(segment.end.positionY);
      roadSegment.setSRM2(!source.isUrbanRoad());

      for (final Entry<Substance, Double> entry : emissions.entrySet()) {
        roadSegment.setEmission(entry.getKey(), entry.getValue());
      }
    }

    return roadSegment;
  }

  private SRM2RoadSegment createObject(final boolean isFreeway, final RoadElevation elevation, final int elevationHeight,
      final RoadSideBarrier barrierLeft, final RoadSideBarrier barrierRight) {
    final SRM2RoadSegment segment = new SRM2RoadSegment();
    segment.setElevationHeight(Sigma0Calculator.getElevationHeight(elevationHeight));
    segment.setSigma0(Sigma0Calculator.getSigma0(isFreeway, elevation, elevationHeight, barrierLeft, barrierRight));

    return segment;
  }

  private <S> S verifyOverride(final Segment segment, final S currentOverride, final S newOverride) {
    final S override;

    if (newOverride == null) {
      override = currentOverride;
    } else {
      if (currentOverride == null) {
        override = newOverride;
      } else {
        throw new IllegalArgumentException("Duplicate override specified for segment <" + segment.start.positionX + ", " + segment.start.positionY
            + "> - <" + segment.end.positionX + "," + segment.end.positionY + ">");
      }
    }

    return override;
  }

  private static class Node {
    private final double positionX;
    private final double positionY;

    private Segment nextSegment;

    public Node(final double positionX, final double positionY) {
      this.positionX = positionX;
      this.positionY = positionY;
    }

    public boolean hasNext() {
      return nextSegment != null;
    }

    public Node next() {
      return nextSegment == null ? null : nextSegment.end;
    }

    public double distanceToNext() {
      return nextSegment == null ? -1 : nextSegment.length();
    }

    public double totalLength() {
      double totalLength = 0;

      for (Node node = this; node.hasNext(); node = node.next()) {
        totalLength += node.distanceToNext();
      }

      return totalLength;
    }

    public Node addNode(final double distance) {
      Node returnNode = null;
      if (Math.abs(distance) <= MIN_DISTANCE) {
        returnNode = this;
      } else {
        double distanceToTravel = distance;

        for (Node node = this; node.hasNext(); node = node.next()) {
          final double distanceToNext = node.distanceToNext();

          if (Math.abs(distanceToTravel - distanceToNext) < MIN_DISTANCE) {
            returnNode = node.next();
            break;
          }
          if (distanceToTravel < distanceToNext) {
            final double factor = distanceToTravel / distanceToNext;

            returnNode = node.nextSegment.splitSegment(factor);
            break;
          } else {
            distanceToTravel -= distanceToNext;
          }
        }
      }
      return returnNode;
    }

    @Override
    public String toString() {
      return "Node [positionX=" + positionX + ", positionY=" + positionY + " ]";
    }
  }

  private static class Segment {
    private final Node start;
    private Node end;
    private final List<SRM2EmissionSourceLinearReference> appliedReferences = new ArrayList<>();

    public Segment(final Node start, final Node end) {
      this.start = start;
      this.end = end;
    }

    public void applyReference(final SRM2EmissionSourceLinearReference reference) {
      appliedReferences.add(reference);
    }

    public double length() {
      final double deltaX = end.positionX - start.positionX;
      final double deltaY = end.positionY - start.positionY;

      return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    public Node splitSegment(final double factor) {
      final double positionX = (end.positionX - start.positionX) * factor + start.positionX;
      final double positionY = (end.positionY - start.positionY) * factor + start.positionY;

      final Node newNode = new Node(positionX, positionY);
      final Segment newSegment = new Segment(newNode, end);
      newSegment.appliedReferences.addAll(appliedReferences);
      newNode.nextSegment = newSegment;
      end = newNode;

      return newNode;
    }

    public Segment next() {
      return end.nextSegment;
    }

    @Override
    public String toString() {
      return "Segment [start=" + start + ", end=" + end + "]";
    }
  }
}
