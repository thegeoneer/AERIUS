/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import nl.overheid.aerius.util.OSUtils;

/**
 * Implements an OpenCL source file that is loaded from a resource.
 */
public class FileSource extends AbstractSource {
  private static final Charset CHARSET = StandardCharsets.UTF_8;

  private final String resource;

  private String source;

  private final ReentrantReadWriteLock sourceLock = new ReentrantReadWriteLock();

  /**
   * Creates a FileSource that loads the source from the specified resource location.
   * @param resource The resource to be loaded.
   * @param constants Optional constant values that will be prefixed to the source.
   */
  public FileSource(final String resource, final Constant... constants) {
    super(constants);

    if (resource == null) {
      throw new NullPointerException("Resource cannot be null.");
    }

    this.resource = resource;
  }

  @Override
  protected String getBody() {
    sourceLock.readLock().lock();
    try {
      if (source == null) {
        source = readSource();
      }
    } finally {
      sourceLock.readLock().unlock();
    }
    return source;
  }

  private String readSource() {
    try (InputStreamReader isr = new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(resource), CHARSET);
        final BufferedReader reader = new BufferedReader(isr)) {
      final StringBuilder sb = new StringBuilder(2048);
      String line;

      while ((line = reader.readLine()) != null) {
        sb.append(line).append(OSUtils.LNL);
      }
      return sb.toString();
    } catch (final IOException e) {
      throw new RuntimeException("Error reading source file: " + e.getMessage(), e);
    }
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + resource.hashCode();
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if (!super.equals(obj)) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final FileSource other = (FileSource) obj;

    if (!resource.equals(other.resource)) {
      return false;
    }

    return true;
  }
}
