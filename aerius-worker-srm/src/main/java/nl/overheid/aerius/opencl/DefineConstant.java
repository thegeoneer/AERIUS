/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Wrapper class around an OpenCL #define statement.
 */
public class DefineConstant implements Constant {
  private static final String DOUBLE_FORMAT = "0.0#####";
  private static final String DOUBLE_FORMAT_E = "0.0#####E0";
  private static final double NORMAL_FORMAT_UPPER_LIMIT = 10.0;
  private static final double NORMAL_FORMAT_LOWER_LIMIT = 0.1;
  private final String value;

  /**
   * Creates a constant with the given name and value. The value is required to be in a format that will be accepted by
   * OpenCL.
   * <p>
   * <strong>Should not be used.</strong>
   *
   * @param name
   *          The name of the constant.
   * @param value
   *          The value of the constant.
   */
  public DefineConstant(final String name, final String value) {
    this.value = "#define " + name + " " + value;
  }

  /**
   * Creates a boolean constant.
   *
   * @param name
   *          The name of the constant.
   * @param value
   *          The value of the constant.
   */
  public DefineConstant(final String name, final boolean value) {
    this(name, format(value));
  }

  /**
   * Creates an integer constant.
   *
   * @param name
   *          The name of the constant.
   * @param value
   *          The value of the constant.
   */
  public DefineConstant(final String name, final int value) {
    this(name, format(value));
  }

  /**
   * Creates a double constant.
   *
   * @param name
   *          The name of the constant.
   * @param value
   *          The value of the constant.
   */
  public DefineConstant(final String name, final double value) {
    this(name, format(value));
  }

  /**
   * Creates a double2 constant.
   *
   * @param name
   *          The name of the constant.
   * @param value1
   *          The value of the constant.
   * @param value2
   *          The value of the constant.
   */
  public DefineConstant(final String name, final double value1, final double value2) {
    this(name, format(value1, value2));
  }

  /**
   * Creates a double4 constant.
   *
   * @param name
   *          The name of the constant.
   * @param value1
   *          The value of the constant.
   * @param value2
   *          The value of the constant.
   * @param value3
   *          The value of the constant.
   * @param value4
   *          The value of the constant.
   */
  public DefineConstant(final String name, final double value1, final double value2, final double value3, final double value4) {
    this(name, format(value1, value2, value3, value4));
  }

  /**
   * @return The define statement.
   */
  @Override
  public final String getValue() {
    return value;
  }

  /**
   * Formats a boolean value.
   *
   * @param value
   *          The value to be formatted.
   * @return The value formatted for OpenCL.
   */
  protected static String format(final boolean value) {
    return value ? "1" : "0";
  }

  /**
   * Formats an integer value.
   *
   * @param value
   *          The value to be formatted.
   * @return The value formatted for OpenCL.
   */
  protected static String format(final int value) {
    return String.valueOf(value);
  }

  /**
   * Formats a single double value.
   *
   * @param value
   *          The value to be formatted.
   * @return The value formatted for OpenCL.
   */
  protected static String format(final double value) {
    final String pattern = useENotation(value) ? DOUBLE_FORMAT_E : DOUBLE_FORMAT;

    return new DecimalFormat(pattern, new DecimalFormatSymbols(Locale.US)).format(value);
  }

  /**
   * Formats a double2 value.
   *
   * @param value1
   *          The first value to be formatted.
   * @param value2
   *          The second value to be formatted.
   * @return The values formatted for OpenCL as a double2.
   */
  protected static String format(final double value1, final double value2) {
    return "(double2) (" + format(value1) + ", " + format(value2) + ")";
  }

  /**
   * Formats a double4 value.
   *
   * @param value1
   *          The first value to be formatted.
   * @param value2
   *          The second value to be formatted.
   * @param value3
   *          The third value to be formatted.
   * @param value4
   *          The fourth value to be formatted.
   * @return The values formatted for OpenCL as a double4.
   */
  protected static String format(final double value1, final double value2, final double value3, final double value4) {
    return "(double4) (" + format(value1) + ", " + format(value2) + ", " + format(value3) + ", " + format(value4)
        + ")";
  }

  protected static boolean useENotation(final double value) {
    final double absValue = Math.abs(value);
    return absValue >= NORMAL_FORMAT_UPPER_LIMIT || absValue < NORMAL_FORMAT_LOWER_LIMIT;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    final int result = 1;
    return prime * result + ((value == null) ? 0 : value.hashCode());
  }

  @Override
  public boolean equals(final Object obj) {
    boolean equal = false;
    if (obj != null && obj.getClass() == getClass()) {
      final DefineConstant other = (DefineConstant) obj;
      equal = (value != null && value.equals(other.value))
          || (value == null && other.value == null);
    }
    return equal;
  }

  @Override
  public String toString() {
    return "Constant [" + value + "]";
  }
}
